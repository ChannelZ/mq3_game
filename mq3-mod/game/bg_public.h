/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// bg_public.h -- definitions shared by both the server game and client game modules

// because games can change separately from the main system version, we need a
// second version that must match between game and cgame

#include "bg_weapon.h"

#define GAME_VERSION		BASEGAME "-1"

#define DEFAULT_GRAVITY		800
#define GIB_HEALTH		-40 // -40
#define ARMOR_PROTECTION	0.66

#define MAX_ITEMS		256

#define RANK_TIED_FLAG		0x4000

/// item sizes are needed for client side pickup detection
#define ITEM_RADIUS		15

/// for the CS_SCORES[12] when only one player is present
#define SCORE_NOT_PRESENT	-9999
/// 30 seconds before vote times out
#define VOTE_TIME		20000

#define MINS_Z			-24
#define DEFAULT_VIEWHEIGHT	26
#define CROUCH_VIEWHEIGHT	12
#define DEAD_VIEWHEIGHT		-16
#define DEAD_BOXHEIGHT		-8

// defining hitboxes // referenced to the map coordinates (no rotations)
#define HITBOX_MIN_X -15
#define HITBOX_MAX_X 15
#define HITBOX_MIN_Y -15
#define HITBOX_MAX_Y 15
#define HITBOX_MIN_Z -24
#define HITBOX_MAX_Z 32
#define HITBOX_MIN_Z_CROUCH -16
#define HITBOX_MAX_Z_CROUCH 24

//
// config strings are a general means of communicating variable length strings
// from the server to all connected clients.
//

// CS_SERVERINFO and CS_SYSTEMINFO are defined in q_shared.h
#define CS_MUSIC		2
/// from the map worldspawn's message field
#define CS_MESSAGE		3
/// g_motd string for server message of the day
#define CS_MOTD			4
/// server time when the match will be restarted
#define CS_WARMUP		5
#define CS_SCORES1		6
#define CS_SCORES2		7
#define CS_VOTE_TIME		8
#define CS_VOTE_STRING		9
#define CS_VOTE_YES		10
#define CS_VOTE_NO		11

// if we are dueling, team1 and team2 are set to the player names
// in team games this can be set to the teamname
#define CS_TEAM1_NAME		12
#define CS_TEAM2_NAME		13

/// server time we want a "fight" to be player
#define CS_ANNOUNCER_TIME	19

#define CS_GAME_VERSION		20
/// when 1, fraglimit/timelimit has been hit and intermission will start in a second or two
#define CS_PLAYER_READY_MASK	21
#define CS_INTERMISSION		22
/// string indicating flag status in CTF
#define CS_FLAGSTATUS		23
//#define	CS_SHADERSTATE		24
#define CS_BOTINFO		25
/// string of 0's and 1's that tell which items are present
#define CS_ITEMS		27
/// let clients know if the game is paused
#define CS_GAME_PAUSED		28

#define CS_MODELS		32
#define CS_SOUNDS		(CS_MODELS+MAX_MODELS)
#define CS_PLAYERS		(CS_SOUNDS+MAX_SOUNDS)
#define CS_PLAYERS_STAT		(CS_PLAYERS+MAX_CLIENTS)
#define CS_LOCATIONS		(CS_PLAYERS_STAT+MAX_CLIENTS)
#define CS_PARTICLES		(CS_LOCATIONS+MAX_LOCATIONS)

#define CS_MAX			(CS_PARTICLES+MAX_LOCATIONS)

#if (CS_MAX) > MAX_CONFIGSTRINGS
#error overflow: (CS_MAX) > MAX_CONFIGSTRINGS
#endif

#define CS_SYSTEMINFO_CHEATS	"sv_cheats"

#define CS_SERVERINFO_MAPNAME	"mapname"
#define CS_SERVERINFO_HOSTNAME	"sv_hostname"
#define CS_SERVERINFO_TIMELIMIT	"g_timelimit"
#define CS_SERVERINFO_FRAGLIMIT	"g_fraglimit"
#define CS_SERVERINFO_CAPLIMIT	"g_capturelimit"
#define CS_SERVERINFO_RNDLIMIT	"g_roundlimit"

#define CS_PLAYER_MODEL		"model"
#define CS_PLAYER_HEADMODEL	"hmodel"
#define CS_PLAYER_NAME		"n"
#define CS_PLAYER_COLOR1	"c1"
#define CS_PLAYER_COLOR2	"c2"
#define CS_PLAYER_SKILL		"skill"
#define CS_PLAYER_HANDICAP	"hc"
#define CS_PLAYER_WINS		"w"
#define CS_PLAYER_LOSSES	"l"
#define CS_PLAYER_TEAM		"t"
#define CS_PLAYER_TEAMTASK	"tt"
#define CS_PLAYER_TEAMLEADER	"tl"
#define CS_PLAYER_MODELCOLOR	"mc"
#define CS_PLAYER_REDTEAM	"grt"
#define CS_PLAYER_BLUETEAM	"gbt"

typedef enum
  {
    /// free for all
    GT_FFA,
    /// one on one tournament
    GT_TOURNAMENT,
    /// single player ffa
    //GT_SINGLE_PLAYER,

    //-- team games go after this --

    /// team deathmatch
    GT_TEAM,
    /// capture the flag
    GT_CTF,
    /// team arena gametype (rounds)
    GT_ARENA,

    GT_MAX_GAME_TYPE
} gametype_t;

/*
================================================================================

PMOVE MODULE

The pmove code takes a player_state_t and a usercmd_t and generates a new player_state_t
and some other output data.  Used for local prediction on the client game and true
movement on the server game.
================================================================================
*/

typedef enum
  {
    /// can accelerate and turn
    PM_NORMAL,
    /// noclip movement
    PM_NOCLIP,
    /// still run into walls
    PM_SPECTATOR,
    /// no acceleration or turning, but free falling
    PM_DEAD,
    /// stuck in place with no control
    PM_FREEZE,
    /// no movement or status bar
    PM_INTERMISSION,
} pmtype_t;

typedef enum
  {
    WEAPON_READY,
    WEAPON_RAISING,
    WEAPON_DROPPING,
    WEAPON_FIRING
  } weaponstate_t;

/// pmove->ps->pm_flags
#define PMF_DUCKED		1
#define PMF_JUMP_HELD		2
/// go into backwards land
#define PMF_BACKWARDS_JUMP	8
/// coast down to backwards run
#define PMF_BACKWARDS_RUN	16
/// pm_time is time before rejump
#define PMF_TIME_LAND		32
/// pm_time is an air-accelerate only time
#define PMF_TIME_KNOCKBACK	64
/// pm_time is waterjump
#define PMF_TIME_WATERJUMP	256
/// clear after attack and jump buttons come up
#define PMF_RESPAWNED		512

#define PMF_USE_ITEM_HELD	1024

/// spectate following another player
#define PMF_FOLLOW		4096
/// spectate as a scoreboard
#define PMF_SCOREBOARD		8192

#define PMF_ALL_TIMES (PMF_TIME_WATERJUMP|PMF_TIME_LAND|PMF_TIME_KNOCKBACK)

#define MAXTOUCH 32

struct pmove_s
{
  /// state (in / out)
  playerState_t*ps;

  /// command (in)
  usercmd_t	cmd;
  /// collide against these types of surfaces
  int		tracemask;
  /// if the game is setup for no footsteps by the server
  bool	noFootsteps;
  /// true if a gauntlet attack would actually hit something
  bool	gauntletHit;

  // allowed player moves for the simulation
  // double jump, ramp jump, crouch slide, air move etc...
  int		plmoves_flags;

  int		framecount;

  /// results (out)
  int		numtouch;
  int		touchents[MAXTOUCH];

  /// bounding box size
  vec3_t	mins, maxs;

  int		watertype;
  int		waterlevel;

  float		xyspeed;

  /// if set, diagnostic output will be printed
  int		debugLevel;

  /// callbacks to test the world
  /// these will be different functions during game and cgame
  void		(*trace) (trace_t* results, const vec3_t start,
			  const vec3_t mins, const vec3_t maxs,
			  const vec3_t end, int passEntityNum, int contentMask);
  int		(*pointcontents) (const vec3_t point, int passEntityNum);
};
typedef struct pmove_s pmove_t;

/// if a full pmove isn't done on the client, you can just update the angles
void PM_UpdateViewAngles (playerState_t* ps, const usercmd_t* cmd);
void Pmove (pmove_t* pmove);

//===================================================================================

/// player_state->stats[] indexes
/// NOTE: may not have more than 16
enum statIndex_e
  {
    STAT_HEALTH,
    STAT_HOLDABLE_ITEM,
    // 16 bit fields
    STAT_WEAPONS,
    STAT_ARMOR,
    /// look this direction when dead (FIXME: get rid of?)
    STAT_DEAD_YAW,
    /// bit mask of clients wishing to exit the intermission (FIXME: configstring?)
    STAT_CLIENTS_READY,
    /// health / armor limit, changable by handicap
    STAT_MAX_HEALTH,
    /// game/match status of the party, this souldn't be needed,
    /// but inform bg_* and client of the server match status
    STAT_MATCHSTATUS,
    STAT_GAMEPLAY,
    /// style of protection the player currently has
    STAT_ARMOR_TYPE,
    /// last time we jumped
    STAT_LASTJUMPTIME,
    /// to get the number of fields
    STAT_NUM_STATS
  };
typedef enum statIndex_e statIndex_t;

//assert (STAT_NUM_STATS =< MAX_STATS)

// match status bitmasks
// to fill level.matchStatus and playerstate stats
#define MS_NONE		0x00000000
#define MS_WARMUP	0x00000001
#define MS_COUNTDOWN	0x00000002
#define MS_TIMESET	0x00000004
#define MS_LIVE		0x00000010
#define MS_PAUSED	0x00000100

// match status events
// the events correspond to transitory states from warmup -> live
// is a "start match" event
#define MSE_NONE	0x00000000
#define MSE_START	0x00000001
#define MSE_END		0x00000002
#define MSE_CDSTART	0x00000004	// starting countdown time to start recording
#define MSE_CDBREAK	0x00000008	// breaking countdown

// gameplay status bitmasks
#define GAMEPLAY_NOFIRE	0x00000001

/// player_state->persistant[] indexes
/// these fields are the only part of player_state that isn't
/// cleared on respawn
/// NOTE: may not have more than 16
enum persEnum_e
  {
    /// !!! MUST NOT CHANGE, SERVER AND GAME BOTH REFERENCE !!!
    PERS_SCORE,
    /// total points damage inflicted so damage beeps can sound on change
    PERS_HITS,
    /// player rank or team rank
    PERS_RANK,
    /// player team
    PERS_TEAM,
    /// incremented every respawn
    PERS_SPAWN_COUNT,
    /// 16 bits that can be flipped for events
    PERS_PLAYEREVENTS,
    /// clientnum of last damage inflicter
    PERS_ATTACKER,
    /// health/armor of last person we attacked
    PERS_ATTACKEE_ARMOR,
    /// count of the number of times you died
    PERS_KILLED,

    // player awards tracking
    /// two railgun hits in a row
    PERS_IMPRESSIVE_COUNT,
    /// two successive kills in a short amount of time
    PERS_EXCELLENT_COUNT,
    /// defend awards
    PERS_DEFEND_COUNT,
    /// assist awards
    PERS_ASSIST_COUNT,
    /// kills with the guantlet
    PERS_GAUNTLET_FRAG_COUNT,
    /// captures
    PERS_CAPTURES,
    //
    PERS_NUM_PERSISTANT
  };
typedef enum persEnum_e persEnum_t;

//assert (PERS_NUM_PERSISTANT <= MAX_PERSISTANT)


/// entityState_t->eFlags
/// don't draw a foe marker over players with EF_DEAD
#define EF_DEAD			0x00000001
#define EF_PLAYER_EVENT		0x00000002

/// toggled every time the origin abruptly changes
#define EF_TELEPORT_BIT		0x00000004
/// draw an excellent sprite
#define EF_AWARD_EXCELLENT	0x00000008
/// for missiles
#define EF_BOUNCE		0x00000010
/// for missiles
#define EF_BOUNCE_HALF		0x00000020
/// draw a gauntlet sprite
#define EF_AWARD_GAUNTLET	0x00000040
/// may have an event, but no model (unspawned items)
#define EF_NODRAW		0x00000080

/// for lightning gun
#define EF_FIRING		0x00000100

/// will push otherwise
#define EF_MOVER_STOP		0x00000400
/// draw the capture sprite
#define EF_AWARD_CAP		0x00000800
/// draw a talk balloon
#define EF_TALK			0x00001000
/// draw a connection trouble sprite
#define EF_CONNECTION		0x00002000
/// already cast a vote
#define EF_VOTED		0x00004000
/// draw an impressive sprite
#define EF_AWARD_IMPRESSIVE	0x00008000
/// draw a defend sprite
#define EF_AWARD_DEFEND		0x00010000
/// draw a assist sprite
#define EF_AWARD_ASSIST		0x00020000
/// denied
#define EF_AWARD_DENIED		0x00040000

/// shortcut to remove all awards
#define EF_CLEAR_AWARDS ~(EF_AWARD_IMPRESSIVE | EF_AWARD_EXCELLENT \
  | EF_AWARD_GAUNTLET | EF_AWARD_ASSIST | EF_AWARD_DEFEND | EF_AWARD_CAP)

// NOTE: may not have more than 16
enum powerup_e
  {
    PW_NONE,

    PW_QUAD,
    PW_BATTLESUIT,
    PW_HASTE,
    PW_INVIS,
    PW_REGEN,
    PW_FLIGHT,

    PW_REDFLAG,
    PW_BLUEFLAG,

    PW_NUM_POWERUPS
  };
typedef enum powerup_e powerup_t;

//assert (PW_NUM_POWERUPS <= MAX_POWERUPS)

typedef enum
  {
    HI_NONE,

    HI_TELEPORTER,
    HI_MEDKIT,

    HI_NUM_HOLDABLE
  } holdable_t;

// assume WP_NUM_WEAPONS <= 16
enum weapon_e
  {
    WP_NONE = 0,		//0

    WP_GAUNTLET,		// 1
    WP_MACHINEGUN,		// 2
    WP_SHOTGUN,			// 3
    WP_GRENADE_LAUNCHER,	// 4
    WP_ROCKET_LAUNCHER,		// 5
    WP_LIGHTNING,		// 6
    WP_RAILGUN,			// 7
    WP_PLASMAGUN,		// 8
    WP_BFG,			// 9

    WP_NUM_WEAPONS		//10
};
typedef enum weapon_e weapon_t;

//assert (WP_NUM_WEAPONS <= MAX_WEAPONS);

/// some useful tables
/// keep these names in sync with gametype_t
const char *gametypeNames[GT_MAX_GAME_TYPE];
const char *gametypeFullNames[GT_MAX_GAME_TYPE];

/// maximum amount of ammos
const int maxammo_table[WP_NUM_WEAPONS];

/// number of ammos in the weapon on pickup
const int weappicking_table[WP_NUM_WEAPONS];

/// number of ammos in ammos boxes
const int ammopicking_table [WP_NUM_WEAPONS];


/// reward sounds (stored in ps->persistant[PERS_PLAYEREVENTS])
#define PLAYEREVENT_DENIEDREWARD	0x0001
#define PLAYEREVENT_GAUNTLETREWARD	0x0002
#define PLAYEREVENT_HOLYSHIT		0x0004

// entityState_t->event values
// entity events are for effects that take place reletive
// to an existing entities origin.  Very network efficient.

// two bits at the top of the entityState->event field
// will be incremented with each change in the event so
// that an identical event started twice in a row can
// be distinguished.  And off the value with ~EV_EVENT_BITS
// to retrieve the actual event number
#define EV_EVENT_BIT1		0x00000100
#define EV_EVENT_BIT2		0x00000200
#define EV_EVENT_BITS		(EV_EVENT_BIT1|EV_EVENT_BIT2)

#define EVENT_VALID_MSEC	300

// use the 8th bit as an orientation flip
// to know if we climb stairs up (0) or down (1)
#define EVENT_STEP_ORIENTATION	(1 << 7)

enum entity_event_e
  {
    EV_NONE,

    EV_FOOTSTEP,
    EV_FOOTSTEP_METAL,
    EV_FOOTSPLASH,
    EV_FOOTWADE,
    EV_SWIM,

    EV_STEP_4,
    EV_STEP_8,
    EV_STEP_12,
    EV_STEP_16,
    EV_STEP_X,

    EV_FALL_SHORT,
    EV_FALL_MEDIUM,
    EV_FALL_FAR,

    EV_COLLIDE_X,

    EV_JUMP_PAD,	// boing sound at origin, jump sound on player

    EV_JUMP,
    EV_WATER_TOUCH,	// foot touches
    EV_WATER_LEAVE,	// foot leaves
    EV_WATER_UNDER,	// head touches
    EV_WATER_CLEAR,	// head leaves

    EV_ITEM_PICKUP,		// normal item pickups are predictable
    EV_GLOBAL_ITEM_PICKUP,	// powerup / team sounds are broadcast to everyone

    EV_NOWEAPON,
    EV_NOAMMO,
    EV_CHANGE_WEAPON,
    EV_FIRE_WEAPON,

    EV_USE_ITEM0,
    EV_USE_ITEM1,
    EV_USE_ITEM2,
    EV_USE_ITEM3,
    EV_USE_ITEM4,
    EV_USE_ITEM5,
    EV_USE_ITEM6,
    EV_USE_ITEM7,
    EV_USE_ITEM8,
    EV_USE_ITEM9,
    EV_USE_ITEM10,
    EV_USE_ITEM11,
    EV_USE_ITEM12,
    EV_USE_ITEM13,
    EV_USE_ITEM14,
    EV_USE_ITEM15,

    EV_ITEM_RESPAWN,
    EV_ITEM_POP,
    EV_PLAYER_TELEPORT_IN,
    EV_PLAYER_TELEPORT_OUT,
    /// eventParm will be the soundindex
    EV_GRENADE_BOUNCE,

    EV_GENERAL_SOUND,
    /// no attenuation
    EV_GLOBAL_SOUND,
    EV_GLOBAL_TEAM_SOUND,

    EV_BULLET_HIT_FLESH,
    EV_BULLET_HIT_WALL,
    EV_SG_BULLET_HIT_FLESH,
    EV_SG_BULLET_HIT_WALL,

    EV_MISSILE_HIT,
    EV_MISSILE_MISS,
    EV_MISSILE_MISS_METAL,
    EV_RAILTRAIL,
    EV_SHOTGUN,
    /// otherEntity is the shooter
    EV_BULLET,

    EV_PAIN,
    EV_DEATH1,
    EV_DEATH2,
    EV_DEATH3,
    EV_OBITUARY,

    EV_POWERUP_QUAD,
    EV_POWERUP_BATTLESUIT,
    EV_POWERUP_REGEN,

    /// gib a previously living player
    EV_GIB_PLAYER,
    /// score plum
    EV_SCOREPLUM,

    EV_DEBUG_LINE,
    EV_STOPLOOPINGSOUND,
    EV_TAUNT,
  };
typedef enum entity_event_e entity_event_t;

typedef enum
  {
    GTS_RED_CAPTURE,
    GTS_BLUE_CAPTURE,
    GTS_RED_RETURN,
    GTS_BLUE_RETURN,
    GTS_RED_TAKEN,
    GTS_BLUE_TAKEN,
    GTS_REDTEAM_SCORED,
    GTS_BLUETEAM_SCORED,
    GTS_REDTEAM_TOOK_LEAD,
    GTS_BLUETEAM_TOOK_LEAD,
    GTS_TEAMS_ARE_TIED,
  } global_team_sound_t;

/// animations
typedef enum
  {
    BOTH_DEATH1,
    BOTH_DEAD1,
    BOTH_DEATH2,
    BOTH_DEAD2,
    BOTH_DEATH3,
    BOTH_DEAD3,

    TORSO_GESTURE,

    TORSO_ATTACK,
    TORSO_ATTACK2,

    TORSO_DROP,
    TORSO_RAISE,

    TORSO_STAND,
    TORSO_STAND2,

    LEGS_WALKCR,
    LEGS_WALK,
    LEGS_RUN,
    LEGS_BACK,
    LEGS_SWIM,

    LEGS_JUMP,
    LEGS_LAND,

    LEGS_JUMPB,
    LEGS_LANDB,

    LEGS_IDLE,
    LEGS_IDLECR,

    LEGS_TURN,

    TORSO_GETFLAG,
    TORSO_GUARDBASE,
    TORSO_PATROL,
    TORSO_FOLLOWME,
    TORSO_AFFIRMATIVE,
    TORSO_NEGATIVE,

    MAX_ANIMATIONS,

    LEGS_BACKCR,
    LEGS_BACKWALK,
    FLAG_RUN,
    FLAG_STAND,
    FLAG_STAND2RUN,

    MAX_TOTALANIMATIONS
  } animNumber_t;

struct animation_s
{
  int firstFrame;
  int numFrames;
  /// 0 to numFrames
  int loopFrames;
  /// msec between frames
  int frameLerp;
  /// msec to get to first frame
  int initialLerp;
  /// true if animation is reversed
  int reversed;
  /// true if animation should flipflop back to base
  int flipflop;
};
typedef struct animation_s animation_t;


/// flip the togglebit every time an animation
/// changes so a restart of the same anim can be detected
#define ANIM_TOGGLEBIT		128

typedef enum
  {
    TEAM_FREE,
    TEAM_RED,
    TEAM_BLUE,
    TEAM_SPECTATOR,

    TEAM_NUM_TEAMS
  } team_t;

/// Time between location updates
#define TEAM_LOCATION_UPDATE_TIME 1000

/// How many players on the overlay
#define TEAM_MAXOVERLAY 8

/// team task
typedef enum
  {
    TEAMTASK_NONE,
    TEAMTASK_OFFENSE,
    TEAMTASK_DEFENSE,
    TEAMTASK_PATROL,
    TEAMTASK_FOLLOW,
    TEAMTASK_RETRIEVE,
    TEAMTASK_ESCORT,
    TEAMTASK_CAMP
  } teamtask_t;

/// means of death
enum meansOfDeath_e
  {
    MOD_UNKNOWN,
    MOD_SHOTGUN,
    MOD_GAUNTLET,
    MOD_MACHINEGUN,
    MOD_GRENADE,
    MOD_GRENADE_SPLASH,
    MOD_ROCKET,
    MOD_ROCKET_SPLASH,
    MOD_PLASMA,
    MOD_PLASMA_SPLASH,
    MOD_RAILGUN,
    MOD_LIGHTNING,
    MOD_BFG,
    MOD_BFG_SPLASH,
    MOD_WATER,
    MOD_SLIME,
    MOD_LAVA,
    MOD_CRUSH,
    MOD_TELEFRAG,
    MOD_FALLING,
    MOD_FACEPALM,
    MOD_SUICIDE,
    MOD_TARGET_LASER,
    MOD_TRIGGER_HURT,
  };
typedef enum meansOfDeath_e meansOfDeath_t;

//---------------------------------------------------------

/// gitem_t->giType
enum itemType_e
  {
    IT_BAD,
    /// EFX: rotate + upscale + minlight
    IT_WEAPON,
    /// EFX: rotate
    IT_AMMO,
    /// EFX: rotate + minlight
    IT_ARMOR,
    /// EFX: static external sphere + rotating internal
    IT_HEALTH,
    /// instant on, timer based
    /// EFX: rotate + external ring that rotates
    IT_POWERUP,

    /// single use, holdable item
    /// EFX: rotate + bob
    IT_HOLDABLE,

    IT_TEAM
  };
typedef enum itemType_e itemType_t;

#define MAX_ITEM_MODELS 4

struct gitem_s
{
  /// spawning name
  char		*classname;
  char		*pickup_sound;
  char		*world_model[MAX_ITEM_MODELS];

  char		*icon;
  /// for printing on pickup
  char		*pickup_name;

  /// for hud and chats
  char		*short_name;
  char		color;

  /// for ammo how much, or duration of powerup
  int		quantity;
  /// IT_* flags
  itemType_t	giType;

  unsigned int	giTag; // enums

  /// string of all models and images this item will use
  char		*precaches;
  /// string of all sounds this item will use
  char		*sounds;
};
typedef struct gitem_s gitem_t;

/// included in both the game dll and the client
extern const gitem_t bg_itemlist[];
extern const int bg_numItems;

const gitem_t* BG_FindItem (const char* pickupName);
const gitem_t* BG_FindItemForWeapon (weapon_t weapon);
const gitem_t* BG_FindItemForAmmo (weapon_t weapon);
const gitem_t* BG_FindItemForPowerup (powerup_t pw);
const gitem_t* BG_FindItemForHoldable (holdable_t pw);

#define ITEM_INDEX(x) ((x)-bg_itemlist)

bool BG_CanItemBeGrabbed (int gametype, const entityState_t* ent,
			      const playerState_t* ps, int parameter);


/// g_dmflags->integer flags
#define DF_NO_SELFDAMAGE		1
#define DF_NO_LIFEDECAY			2
#define DF_NO_ARMORDECAY		4
#define DF_NO_FALLING			8
#define DF_FIXED_FOV			16
#define DF_NO_FOOTSTEPS			32
#define DF_ALL_WEAPONS			64

/// g_plmflags->integer flags
/// this will setup the moves the players are able to perform
#define PLMOVES_DOUBLEJUMP		0x0001
#define PLMOVES_RAMPJUMP		0x0002
#define PLMOVES_AIRMOVE			0x0004
#define PLMOVES_INSTASWITCH		0x0008

/// content masks
#define MASK_ALL			(-1)
#define MASK_SOLID			(CONTENTS_SOLID)
#define MASK_PLAYERSOLID		(CONTENTS_SOLID|CONTENTS_PLAYERCLIP|CONTENTS_BODY)
#define MASK_DEADSOLID			(CONTENTS_SOLID|CONTENTS_PLAYERCLIP)
#define MASK_WATER			(CONTENTS_WATER|CONTENTS_LAVA|CONTENTS_SLIME)
#define MASK_OPAQUE			(CONTENTS_SOLID|CONTENTS_SLIME|CONTENTS_LAVA)
#define MASK_SHOT			(CONTENTS_SOLID|CONTENTS_BODY|CONTENTS_CORPSE)

//
/// entityState_t->eType
//
enum entityType_e
  {
    ET_GENERAL, // missile damage explosition or default entities
    ET_PLAYER,
    ET_ITEM,
    ET_MISSILE, // missile entity
    ET_MOVER,
    ET_BEAM, // target laser
    ET_PORTAL, // misc_portal_surface
    ET_SPEAKER, // target_speaker
    ET_PUSH_TRIGGER, // trigger_push
    ET_TELEPORT_TRIGGER, // trigger_teleport
    ET_DOOR_TRIGGER, // not used
    ET_INVISIBLE,

    ET_TEAM, // not used

    /// any of the EV_* events can be added freestanding
    /// by setting eType to ET_EVENTS + eventNum
    /// this avoids having to set eFlags and eventNum
    ET_EVENTS
  };
typedef enum entityType_e entityType_t;


void BG_EvaluateTrajectory (const trajectory_t* tr, int atTime, vec3_t result);
void BG_EvaluateTrajectoryDelta (const trajectory_t* tr,
				 int atTime, vec3_t result);

void BG_AddPredictableEventToPlayerstate (int newEvent, int eventParm,
					  playerState_t* ps);

void BG_TouchJumpPad (playerState_t* ps, entityState_t* jumppad);

void BG_PlayerStateToEntityState (playerState_t* ps, entityState_t* s,
				  bool snap);

bool BG_PlayerTouchesItem (playerState_t* ps, entityState_t* item,
			   int atTime);

bool BG_GametypeIsTeam (gametype_t gt);

#define MAX_ARENAS		1024
#define MAX_ARENAS_TEXT		8192

#define MAX_BOTS		1024
#define MAX_BOTS_TEXT		8192

/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// bg_local.h -- local definitions for the bg (both games) files

/// can't walk on very steep slopes
#define MIN_WALK_NORMAL 0.7f

#define STEPSIZE 18.0f

#define JUMP_VELOCITY 280.0f
#define DOUBLEJUMP_VELOCITY 100.0f
#define DOUBLEJUMP_TIME 400 // time interval we can rejump

#define TIMER_LAND 130
#define TIMER_GESTURE (34*66+50)

#define OVERCLIP 1.001f
#define GROUND_DISTANCE 0.25f

// avoid integer truncation, causing a lot of trouble (and OB)
#define PMOVE_SNAPVECTOR 0
// be sure to cliponcrash if this is to 1, otherwise expect OB
#define PMOVE_NORMALIZEMOVE 0
// good way to avoid OB when hitting the floor
#define PMOVE_CLIPONCRASH 1

#define PMOVE_AIRCROUCHING 1
#define PMOVE_CROUCHINGHITBOX 1

// simulate the integer truncation error, almost the same moves
#define TRUNCATION_ERROR_SIMULATION 0
#define ADDITIVE_ERROR_VALUE 0.05f	// 50.0f / 1000.0f
					// 50 units per second

// available special moves
#define PLM_ALLOW_RAMPJUMP 1
#define PLM_ALLOW_DOUBLEJUMP 1
#define PLM_ALLOW_AIRMOVE 1
#define PLM_ALLOW_INSTASWITCH 1

// all of the locals will be zeroed before each
// pmove, just to make damn sure we don't have
// any differences when running on client or server
struct pml_s
{
  vec3_t	forward, right, up;
  float		frametime;

  int		msec;

  bool	walking;
  bool	groundPlane;
  trace_t	groundTrace;

  float		impactSpeed;

  vec3_t	previous_origin;
  vec3_t	previous_velocity;
  int		previous_waterlevel;
};
typedef struct pml_s pml_t;

void PM_ClipVelocity (vec3_t in, vec3_t normal, vec3_t out, float overbounce);
void PM_AddTouchEnt (pmove_t* pm, int entityNum);
void PM_AddEvent (pmove_t* pm, int newEvent);

bool PM_SlideMove (pmove_t* pm, pml_t* pml, bool gravity, bool stepUp, bool stepDown);
void PM_StepSlideMove (pmove_t* pm, bool gravity);

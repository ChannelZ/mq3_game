/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// g_combat.c

#include "g_local.h"

/*
============
ScorePlum
============
*/
void
ScorePlum (gentity_t * ent, vec3_t origin, int score)
{
  gentity_t *plum;

  plum = G_TempEntity (origin, EV_SCOREPLUM);
  // only send this temp entity to a single client
  plum->sh.r.svFlags |= SVF_SINGLECLIENT;
  plum->sh.r.singleClient = ent->sh.s.number;
  //
  plum->sh.s.otherEntityNum = ent->sh.s.number;
  plum->sh.s.time = score;
}

/*
============
AddScore

Adds score to both the client and his team
============
*/
void
AddScore (gentity_t * ent, vec3_t origin, int score)
{
  if (!ent->client)
    {
      return;
    }
  // no scoring during pre-match warmup
  /*  if (level.warmupTime)
    {
      return;
    }
  */
  if (!(level.matchStatus & MS_LIVE))
    {
      return;
    }
  // show score plum
  ScorePlum (ent, origin, score);
  //
  ent->client->ps.persistant[PERS_SCORE] += score;
  if (g_gametype->integer == GT_TEAM)
    level.teamScores[ent->client->ps.persistant[PERS_TEAM]] += score;
  CalculateRanks ();
}

/*
=================
TossClientItems

Toss the weapon and powerups for the killed player
=================
*/
void
TossClientItems (gentity_t * self)
{
  const gitem_t *item;
  int weapon;
  float angle;
  int i;
  gentity_t *drop;

  // drop the weapon if not a gauntlet or machinegun
  weapon = self->sh.s.weapon;

  // make a special check to see if they are changing to a new
  // weapon that isn't the mg or gauntlet.  Without this, a client
  // can pick up a weapon, be killed, and not drop the weapon because
  // their weapon change hasn't completed yet and they are still holding the MG.
  if (weapon == WP_MACHINEGUN)
    {
      if (self->client->ps.weaponstate == WEAPON_DROPPING)
	{
	  weapon = self->client->pers.cmd.weapon;
	}
      if (!(self->client->ps.stats[STAT_WEAPONS] & (1 << weapon)))
	{
	  weapon = WP_NONE;
	}
    }

  if (weapon >= WP_MACHINEGUN && self->client->ps.ammo[weapon])
    {
      // find the item type for this weapon
      item = BG_FindItemForWeapon (weapon);

      // shouldn't ever happend
      if (item == NULL)
	return;

      // spawn the item
      // don't throw it forward on regular death
      Drop_Player_Item (self, item, 0, false);
      //Drop_Item (self, item, 0);
    }

  // drop all the powerups if not in teamplay
  if (g_gametype->integer != GT_TEAM)
    {
      angle = 45;
      for (i = 1; i < PW_NUM_POWERUPS; i++)
	{
	  if (self->client->ps.powerups[i] > level.time)
	    {
	      item = BG_FindItemForPowerup (i);
	      if (item == NULL)
		{
		  continue;
		}

	      // always drop flags strait forward
	      // using drop_player_item so flag dropps will be tracked by stats
	      if (item->giType == IT_TEAM)
		{
		  drop = Drop_Player_Item (self, item, 0, false);
		}
	      else
		{
		  drop = Drop_Item (self, item, angle);
		}

	      // decide how many seconds it has left
	      drop->count = (self->client->ps.powerups[i] - level.time) / 1000;
	      if (drop->count < 1)
		{
		  drop->count = 1;
		}
	      angle += 45;
	    }
	}
    }
}

/*
==================
LookAtKiller
==================
*/
void
LookAtKiller (gentity_t * self, gentity_t * inflictor, gentity_t * attacker)
{
  vec3_t dir;
  vec3_t angles;

  if (attacker && attacker != self)
    {
      VectorSubtract (attacker->sh.s.pos.trBase, self->sh.s.pos.trBase, dir);
    }
  else if (inflictor && inflictor != self)
    {
      VectorSubtract (inflictor->sh.s.pos.trBase, self->sh.s.pos.trBase, dir);
    }
  else
    {
      self->client->ps.stats[STAT_DEAD_YAW] = self->sh.s.angles[YAW];
      return;
    }

  self->client->ps.stats[STAT_DEAD_YAW] = vectoyaw (dir);

  angles[YAW] = vectoyaw (dir);
  angles[PITCH] = 0;
  angles[ROLL] = 0;
}

/*
==================
GibEntity
==================
*/
void
GibEntity (gentity_t * self, int killer)
{
  G_AddEvent (self, EV_GIB_PLAYER, killer);
  self->takedamage = false;
  self->sh.s.eType = ET_INVISIBLE;
  self->sh.r.contents = 0;
}

/*
==================
body_die
==================
*/
void
body_die (gentity_t * self, gentity_t * inflictor, gentity_t * attacker,
	  int damage, int meansOfDeath)
{
  if (self->health > GIB_HEALTH)
    {
      return;
    }
  // FIXME : don't like how this case is handled
  if (!g_blood->integer)
    {
      self->health = GIB_HEALTH + 1;
      return;
    }

  GibEntity (self, 0);
}

// these are just for logging, the client prints its own messages
char *modNames[] = {
  "MOD_UNKNOWN",
  "MOD_SHOTGUN",
  "MOD_GAUNTLET",
  "MOD_MACHINEGUN",
  "MOD_GRENADE",
  "MOD_GRENADE_SPLASH",
  "MOD_ROCKET",
  "MOD_ROCKET_SPLASH",
  "MOD_PLASMA",
  "MOD_PLASMA_SPLASH",
  "MOD_RAILGUN",
  "MOD_LIGHTNING",
  "MOD_BFG",
  "MOD_BFG_SPLASH",
  "MOD_WATER",
  "MOD_SLIME",
  "MOD_LAVA",
  "MOD_CRUSH",
  "MOD_TELEFRAG",
  "MOD_FALLING",
  "MOD_SUICIDE",
  "MOD_TARGET_LASER",
  "MOD_TRIGGER_HURT",
};

/*
================
G_ModToWeaponNumber
================
*/
static int
G_ModToWeaponNumber (int mod)
{
  int val = WP_NONE;
  switch (mod)
    {
    case MOD_GAUNTLET:
      val = WP_GAUNTLET;
      break;
    case MOD_MACHINEGUN:
      val = WP_MACHINEGUN;
      break;
    case MOD_SHOTGUN:
      val = WP_SHOTGUN;
      break;
    case MOD_GRENADE:
    case MOD_GRENADE_SPLASH:
      val = WP_GRENADE_LAUNCHER;
      break;
    case MOD_ROCKET:
    case MOD_ROCKET_SPLASH:
      val = WP_ROCKET_LAUNCHER;
      break;
    case MOD_PLASMA:
    case MOD_PLASMA_SPLASH:
      val = WP_PLASMAGUN;
      break;
    case MOD_RAILGUN:
      val = WP_RAILGUN;
      break;
    case MOD_LIGHTNING:
      val = WP_LIGHTNING;
      break;
    case MOD_BFG:
    case MOD_BFG_SPLASH:
      val = WP_BFG;
      break;
    default:
      break;
    }
  return val;
}

/*
==================
CheckAlmostCapture
==================
*/
void
CheckAlmostCapture (gentity_t * self, gentity_t * attacker)
{
  gentity_t *ent;
  vec3_t dir;
  char *classname;

  // if this player was carrying a flag, 2nd test should be implicit
  if ((self->client->ps.powerups[PW_REDFLAG])
      || (self->client->ps.powerups[PW_BLUEFLAG]))
    /*&& g_gametype->integer == GT_CTF */
    {
      // get the goal flag this player should have been going for
      if (self->client->sess.sessionTeam == TEAM_BLUE)
	{
	  classname = "team_CTF_blueflag";
	}
      else
	{
	  classname = "team_CTF_redflag";
	}

      ent = NULL;
      do
	{
	  ent = G_Find (ent, FOFS (classname), classname);
	}
      while (ent && (ent->flags & FL_DROPPED_ITEM));
      // if we found the destination flag and it's not picked up
      if (ent && !(ent->sh.r.svFlags & SVF_NOCLIENT))
	{
	  // if the player was *very* close
	  VectorSubtract (self->client->ps.origin, ent->sh.s.origin, dir);
	  if (VectorLength (dir) < 2000)
	    {
	      self->client->ps.persistant[PERS_PLAYEREVENTS] ^=
		PLAYEREVENT_HOLYSHIT;
	      if (attacker->client)
		{
		  attacker->client->ps.persistant[PERS_PLAYEREVENTS] ^=
		    PLAYEREVENT_HOLYSHIT;
		}
	    }
	}
    }
}

/*
==================
player_die
==================
*/
void
player_die (gentity_t * self, gentity_t * inflictor, gentity_t * attacker,
	    int damage, int meansOfDeath)
{
  gentity_t *ent;
  int anim;
  int contents;
  int killer;
  int i;
  char *killerName, *obit;

  if (self->client->ps.pm_type == PM_DEAD)
    {
      return;
    }

  if (level.match.end_flag >= END_INTERMISSION)
    {
      return;
    }

  // check for an almost capture
  CheckAlmostCapture (self, attacker);

  self->client->ps.pm_type = PM_DEAD;

  if (attacker)
    {
      killer = attacker->sh.s.number;
      if (attacker->client)
	{
	  killerName = attacker->client->pers.netname;
	}
      else
	{
	  killerName = "<non-client>";
	}
    }
  else
    {
      killer = ENTITYNUM_WORLD;
      killerName = "<world>";
    }

  if (killer < 0 || killer >= MAX_CLIENTS)
    {
      killer = ENTITYNUM_WORLD;
      killerName = "<world>";
    }

  if ((meansOfDeath < 0) || ((unsigned int) meansOfDeath >= (ARRAY_LEN (modNames))))
    {
      obit = "<bad obituary>";
    }
  else
    {
      obit = modNames[meansOfDeath];
    }

  G_LogPrintf ("Kill: %i %i %i: %s killed %s by %s\n",
	       killer, self->sh.s.number, meansOfDeath, killerName,
	       self->client->pers.netname, obit);

  // broadcast the death event to everyone
  ent = G_TempEntity (self->sh.r.currentOrigin, EV_OBITUARY);
  ent->sh.s.eventParm = meansOfDeath;
  ent->sh.s.otherEntityNum = self->sh.s.number;
  ent->sh.s.otherEntityNum2 = killer;
  ent->sh.r.svFlags = SVF_BROADCAST;	// send to everyone

  self->enemy = attacker;

  self->client->ps.persistant[PERS_KILLED]++;

  if (attacker && attacker->client)
    {
      attacker->client->cbt.lastkilled_client = self->sh.s.number;

      if (attacker == self)
	{
	  // the self client suicided or got killed by a teamate
	  AddScore (attacker, self->sh.r.currentOrigin, -1);

	  self->client->stats.suicidecounter++;
	}
      else if (OnSameTeam (self, attacker))
	{
	  int weap;

	  // the self client suicided or got killed by a teamate
	  AddScore (attacker, self->sh.r.currentOrigin, -1);

	  self->client->stats.totdeath++;
	  attacker->client->stats.totkill++;

	  attacker->client->stats.teamkill++;

	  // update weapon stats
	  weap = G_ModToWeaponNumber (meansOfDeath);
	  if ((weap > WP_NONE) && (weap < WP_NUM_WEAPONS))
	    {
	      self->client->stats.weap[weap].death++;
	      attacker->client->stats.weap[weap].kill++;
	    }
	}
      else
	{
	  // the self client got killed by a client attacker
	  int weap;

	  AddScore (attacker, self->sh.r.currentOrigin, 1);

	  self->client->stats.totdeath++;
	  attacker->client->stats.totkill++;

	  // update weapon stats
	  weap = G_ModToWeaponNumber (meansOfDeath);
	  if ((weap > WP_NONE) && (weap < WP_NUM_WEAPONS))
	    {
	      self->client->stats.weap[weap].death++;
	      attacker->client->stats.weap[weap].kill++;
	    }

	  if (meansOfDeath == MOD_GAUNTLET)
	    {
	      // play humiliation on player
	      attacker->client->ps.persistant[PERS_GAUNTLET_FRAG_COUNT]++;

	      // add the sprite over the player's head
	      // remove older sprites
	      attacker->client->ps.eFlags &= EF_CLEAR_AWARDS;
	      // set the new one
	      attacker->client->ps.eFlags |= EF_AWARD_GAUNTLET;
	      attacker->client->timer.rewardTime =
		level.time + REWARD_SPRITE_TIME;

	      // also play humiliation on target
	      self->client->ps.persistant[PERS_PLAYEREVENTS] ^=
		PLAYEREVENT_GAUNTLETREWARD;
	    }

	  // check for two kills in a short amount of time
	  // if this is close enough to the last kill, give a reward sound
	  if ((level.time - attacker->client->timer.lastKillTime)
	      < CARNAGE_REWARD_TIME)
	    {
	      // play excellent on player
	      attacker->client->ps.persistant[PERS_EXCELLENT_COUNT]++;

	      // add the sprite over the player's head
	      attacker->client->ps.eFlags &= EF_CLEAR_AWARDS;
	      attacker->client->ps.eFlags |= EF_AWARD_EXCELLENT;
	      attacker->client->timer.rewardTime =
		level.time + REWARD_SPRITE_TIME;
	    }
	  attacker->client->timer.lastKillTime = level.time;
	}
    }
  else
    {
      AddScore (self, self->sh.r.currentOrigin, -1);
    }

  // Add team bonuses
  Team_FragBonuses (self, inflictor, attacker);

  // if client is in a nodrop area, don't drop anything (but return CTF flags!)
  contents = gi->PointContents (self->sh.r.currentOrigin, -1);
  if (!(contents & CONTENTS_NODROP))
    {
      TossClientItems (self);
    }
  else
    {
      if (self->client->ps.powerups[PW_REDFLAG])
	{	// only happens in standard CTF
	  Team_ReturnFlag (&teamgame, TEAM_RED);
	  self->client->stats.ctf.flagtime +=
	    level.time - self->client->pers.teamState.flagsince;
	}
      else if (self->client->ps.powerups[PW_BLUEFLAG])
	{	// only happens in standard CTF
	  Team_ReturnFlag (&teamgame, TEAM_BLUE);
	  self->client->stats.ctf.flagtime +=
	    level.time - self->client->pers.teamState.flagsince;
	}
    }

  Cmd_Score_f (self);	// show scores
  // send updated scores to any clients that are following this one,
  // or they would get stale scoreboards
  for (i = 0; i < level.maxclients; i++)
    {
      gclient_t *client;

      client = &level.clients[i];
      if (client->pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      if (client->sess.sessionTeam != TEAM_SPECTATOR)
	{
	  continue;
	}
      if (client->sess.spectatorClient == self->sh.s.number)
	{
	  Cmd_Score_f (g_entities + i);
	}
    }

  self->takedamage = true;	// can still be gibbed

  self->sh.s.weapon = WP_NONE;
  self->sh.s.powerups = 0;
  self->sh.r.contents = CONTENTS_CORPSE;

  self->sh.s.angles[0] = 0;
  self->sh.s.angles[2] = 0;
  LookAtKiller (self, inflictor, attacker);

  VectorCopy (self->sh.s.angles, self->client->ps.viewangles);

  self->sh.s.loopSound = 0;

  self->sh.r.maxs[2] = -8;

  // don't allow respawn until the death anim is done
  // g_forcerespawn may force spawning at some later time
  // allow fast respawn default : +1700
  if (g_rapeMode->integer)
    self->client->timer.respawnTime = level.time + RESPAWN_MINIMAL_DELAY_RAPE;
  else
    self->client->timer.respawnTime = level.time + RESPAWN_MINIMAL_DELAY;

  // remove powerups
  Com_Memset (self->client->ps.powerups, 0, sizeof (self->client->ps.powerups));

  // never gib in a nodrop
  if ((self->health <= GIB_HEALTH) && !(contents & CONTENTS_NODROP)
      && ((g_blood->integer) || (meansOfDeath == MOD_SUICIDE)))
    {
      // gib death
      GibEntity (self, killer);
    }
  else
    {
      // normal death
      static int death_animation;

      switch (death_animation)
	{
	case 0:
	  anim = BOTH_DEATH1;
	  break;
	case 1:
	  anim = BOTH_DEATH2;
	  break;
	case 2:
	default:
	  anim = BOTH_DEATH3;
	  break;
	}

      // for the no-blood option, we need to prevent the health
      // from going to gib level
      if (self->health <= GIB_HEALTH)
	{
	  self->health = GIB_HEALTH + 1;
	}

      self->client->ps.legsAnim =
	((self->client->ps.legsAnim & ANIM_TOGGLEBIT) ^ ANIM_TOGGLEBIT) | anim;
      self->client->ps.torsoAnim =
	((self->client->ps.torsoAnim & ANIM_TOGGLEBIT) ^ ANIM_TOGGLEBIT) | anim;

      G_AddEvent (self, EV_DEATH1 + death_animation, killer);

      // the body can still be gibbed
      self->die = body_die;

      // globally cycle through the different death animations
      death_animation = (death_animation + 1) % 3;
    }

  gi->LinkEntity (&self->sh);
}

/*
================
CheckArmor
================
*/
int
CheckArmor (gentity_t * ent, int damage, int dflags)
{
  gclient_t *client;
  int save;
  int count;

  if (!damage)
    return 0;

  client = ent->client;

  if (client == NULL)
    return 0;

  if (dflags & DAMAGE_NO_ARMOR)
    return 0;

  // armor
  count = client->ps.stats[STAT_ARMOR];

  if (!g_armorSystem->integer)
    {
      save = ceil (damage * ARMOR_PROTECTION);
    }
  else
    {
      float armor_protect;
      int armortype;

      armortype = client->ps.stats[STAT_ARMOR_TYPE];

      if (armortype == 2)
	armor_protect = CPM_RAPROTECTION;
      else if (armortype == 1)
	armor_protect = CPM_YAPROTECTION;
      else
	armor_protect = CPM_LAPROTECTION;

      save = ceil (damage * armor_protect);
    }

  if (save >= count)
    save = count;

  if (!save)
    return 0;

  client->ps.stats[STAT_ARMOR] -= save;

  return save;
}

/*
=================
G_KnockbackFactor
=================
*/
static float
G_KnockbackFactor (meansOfDeath_t mod)
{
  float kscale;

  switch (mod)
    {
    case MOD_GAUNTLET:
      if (g_rapeMode->integer)
	kscale = GAUNTLET_KNOCKBACK_PRO;
      else
	kscale = GAUNTLET_KNOCKBACK;
      break;
    case MOD_SHOTGUN:
      kscale = SHOTGUN_KNOCKBACK;
      break;
    case MOD_ROCKET:
      if (g_rapeMode->integer)
	kscale = ROCKET_KNOCKBACK_PRO;
      else
	kscale = ROCKET_KNOCKBACK;
      break;
    case MOD_ROCKET_SPLASH:
      if (g_rapeMode->integer)
	kscale = ROCKET_SPLASH_KNOCKBACK_PRO;
      else
	kscale = ROCKET_SPLASH_KNOCKBACK;
      break;
    case MOD_LIGHTNING:
      if (g_rapeMode->integer)
	kscale = LIGHTNING_KNOCKBACK_PRO;
      else
	kscale = LIGHTNING_KNOCKBACK;
      break;

    default:
      kscale = 1.0f;
      break;
    }

  return kscale;
}

/*
===============
G_CrippleFactor
===============
*/
static float
G_CrippleFactor (meansOfDeath_t mod)
{
  float factor;

  switch (mod)
    {
    case MOD_MACHINEGUN:
      if (g_rapeMode->integer)
	factor = MACHINEGUN_CRIPPLE_PRO;
      else
	factor = MACHINEGUN_CRIPPLE;
      break;
    case MOD_RAILGUN:
      if (g_rapeMode->integer)
	factor = RAILGUN_CRIPPLE_PRO;
      else
	factor = RAILGUN_CRIPPLE;
      break;
    case MOD_LIGHTNING:
      if (g_rapeMode->integer)
	factor = LIGHTNING_CRIPPLE_PRO;
      else
	factor = LIGHTNING_CRIPPLE;
      break;

    default:
      factor = 1.0f;
      break;
    }

  return factor;
}

/*
============
T_Damage

targ		entity that is being damaged
inflictor	entity that is causing the damage
attacker	entity that caused the inflictor to damage targ
	example: targ=monster, inflictor=rocket, attacker=player

dir		direction of the attack for knockback
point		point at which the damage is being inflicted, used for headshots
damage		amount of damage being inflicted
knockback	force to be applied against targ as a result of the damage

inflictor, attacker, dir, and point can be NULL for environmental effects

dflags		these flags are used to control how T_Damage works
	DAMAGE_RADIUS			damage was indirect (from a nearby explosion)
	DAMAGE_NO_ARMOR			armor does not protect from this damage
	DAMAGE_NO_KNOCKBACK		do not affect velocity, just view angles
	DAMAGE_NO_PROTECTION	kills godmode, armor, everything
============
*/
void
G_Damage (gentity_t * targ, gentity_t * inflictor, gentity_t * attacker,
	  vec3_t dir, vec3_t point, int damage, int dflags, int mod)
{
  gclient_t *client;
  int take;
  int asave;
  int knockback;
  int max;

  if (!targ->takedamage)
    {
      return;
    }

  // the intermission has allready been qualified for, so don't
  // allow any extra scoring
  if (level.match.end_flag >= END_CALLED)
    {
      return;
    }

  // don't allow new damages to be registered
  if (level.noDamage)
    {
      return;
    }

  if (inflictor == NULL)
    {
      inflictor = &g_entities[ENTITYNUM_WORLD];
    }
  if (attacker == NULL)
    {
      attacker = &g_entities[ENTITYNUM_WORLD];
    }

  // shootable doors / buttons don't actually have any health
  if (targ->sh.s.eType == ET_MOVER)
    {
      if (targ->use && targ->moverState == MOVER_POS1)
	{
	  targ->use (targ, inflictor, attacker);
	}
      return;
    }

  // reduce damage by the attacker's handicap value
  // unless they are rocket jumping
  if (attacker->client && attacker != targ)
    {
      max = attacker->client->ps.stats[STAT_MAX_HEALTH];

      damage = damage * max / 100;
    }

  client = targ->client;

  if (client != NULL && client->misc.noclip)
    {
      return;
    }

  if (!dir)
    {
      dflags |= DAMAGE_NO_KNOCKBACK;
    }
  else
    {
      VectorNormalize (dir);
    }

  knockback = damage;
  if (knockback > 200)
    {
      knockback = 200;
    }
  if (targ->flags & FL_NO_KNOCKBACK)
    {
      knockback = 0;
    }
  if (dflags & DAMAGE_NO_KNOCKBACK)
    {
      knockback = 0;
    }

  // figure momentum add, even if the damage won't be taken
  if (knockback && targ->client)
    {
      vec3_t kvel;
      float kscale = 1.0f;

      // don't change knockback on rocket jumping
      if ((targ == attacker) && (mod == MOD_ROCKET_SPLASH))
	{
	}
      else
	{
	  kscale = G_KnockbackFactor (mod);
	}

      VectorScale (dir, kscale * g_knockback->value * (float) knockback, kvel);
      VectorAdd (targ->client->ps.velocity, kvel, targ->client->ps.velocity);

      // set the timer so that the other client can't cancel
      // out the movement immediately
      if (!targ->client->ps.pm_time)
	{
	  int t;
	  float factor;

	  t = knockback * 2;
	  if (t < 50)
	    {
	      t = 50;
	    }
	  if (t > 200)
	    {
	      t = 200;
	    }

	  factor = G_CrippleFactor (mod);

	  targ->client->ps.pm_time = (int) ((float) t * factor);
	  targ->client->ps.pm_flags |= PMF_TIME_KNOCKBACK;
	}
    }

  // check for completely getting out of the damage
  if (!(dflags & DAMAGE_NO_PROTECTION))
    {
      if ((targ == attacker) && (g_dmflags->integer & DF_NO_SELFDAMAGE))
	{
	  return;
	}

      // if friendlyFire is not set, don't do damage to the target
      // if the attacker was on the same team
      if (targ != attacker && OnSameTeam (targ, attacker))
	{
	  if (!g_friendlyFire->integer)
	    {
	      return;
	    }
	}

      // check for godmode
      if (targ->flags & FL_GODMODE)
	{
	  return;
	}
    }

  // battlesuit protects from all radius damage (but takes knockback)
  // and protects 50% against all damage (updated to 75%)
  if (client != NULL && client->ps.powerups[PW_BATTLESUIT])
    {
      G_AddEvent (targ, EV_POWERUP_BATTLESUIT, 0);
      if ((dflags & DAMAGE_RADIUS) || (mod == MOD_FALLING))
	{
	  return;
	}
      damage *= 0.25; // 0.5
    }

  // always give half damage if hurting self
  // calculated after knockback, so rocket jumping works
  if (targ == attacker)
    {
      damage *= 0.5;
    }

  if (damage < 1)
    {
      damage = 1;
    }
  take = damage;

  // save some from armor
  asave = CheckArmor (targ, take, dflags);
  take -= asave;

  // don't take damages if paused
  if (game.paused)
    {
      take = 0;
      asave = 0;
    }

  if (g_debugDamage->integer)
    {
      G_Printf ("%i: client:%i health:%i damage:%i armor:%i\n", level.time,
		targ->sh.s.number, targ->health, take, asave);
    }

  if (attacker->client && client != NULL
      && targ != attacker && targ->health > 0
      && targ->sh.s.eType != ET_MISSILE && targ->sh.s.eType != ET_GENERAL)
    {
      int weap;

      // legacy stat tracker
      // add to the attacker's hit counter
      // (if the target isn't a general entity like a prox mine)
      if (OnSameTeam (targ, attacker))
	{
	  attacker->client->ps.persistant[PERS_HITS]--;
	  attacker->client->stats.teamdamage += damage;
	}
      else
	{
	  attacker->client->ps.persistant[PERS_HITS]++;
	}
      attacker->client->ps.persistant[PERS_ATTACKEE_ARMOR] =
	(targ->health << 8) | (client->ps.stats[STAT_ARMOR]);

      // advanced one (new one)
      // switch by mod, it is easier than looking for eType,
      // then split regarding the result
      weap = G_ModToWeaponNumber (mod);
      if ((weap > WP_NONE) && (weap < WP_NUM_WEAPONS))
	{
	  attacker->client->stats.weap[weap].damagegiven += damage;
	  attacker->client->stats.weap[weap].hit++;
	  attacker->client->stats.totdamage_done += damage;
	  client->stats.weap[weap].damagereceived += damage;
	}
    }

  // add to the damage inflicted on a player this frame
  // the total will be turned into screen blends and view angle kicks
  // at the end of the frame
  if (client != NULL)
    {
      if (attacker != NULL)
	{
	  client->ps.persistant[PERS_ATTACKER] = attacker->sh.s.number;
	}
      else
	{
	  client->ps.persistant[PERS_ATTACKER] = ENTITYNUM_WORLD;
	}
      client->cbt.damage_armor += asave;
      client->cbt.damage_blood += take;
      client->cbt.damage_knockback += knockback;
      if (dir)
	{
	  VectorCopy (dir, client->cbt.damage_from);
	  client->cbt.damage_fromWorld = false;
	}
      else
	{
	  VectorCopy (targ->sh.r.currentOrigin, client->cbt.damage_from);
	  client->cbt.damage_fromWorld = true;
	}
    }

  if (targ->client != NULL)
    {
      // set the last client who damaged the target
      targ->client->cbt.lasthurt_client = attacker->sh.s.number;
      targ->client->cbt.lasthurt_mod = mod;
    }

  // do the damage
  if (take)
    {
      targ->health -= take;
      if (targ->client != NULL)
	{
	  targ->client->ps.stats[STAT_HEALTH] = targ->health;
	  // exclude telefrags and suicides
	  if (mod == MOD_TELEFRAG)
	    {
	      targ->client->stats.telefragcounter++;
	    }
	  else if (mod == MOD_SUICIDE)
	    {
	      targ->client->stats.suicidecounter++;
	    }
	  else
	    {
	      targ->client->stats.totdamage_taken += damage;
	    }
	}

      // killed the target
      if (targ->health <= 0)
	{
	  if (targ->client != NULL)
	    targ->flags |= FL_NO_KNOCKBACK;

	  if (targ->health < -999)
	    targ->health = -999;

	  targ->enemy = attacker;
	  targ->die (targ, inflictor, attacker, take, mod);
	  return;
	}
      else if (targ->pain != NULL)
	{
	  targ->pain (targ, attacker, take);
	}
    }
}

/*
============
CanDamage

Returns true if the inflictor can directly damage the target.  Used for
explosions and melee attacks.
============
*/
bool
CanDamage (gentity_t * targ, vec3_t origin)
{
  vec3_t dest;
  trace_t tr;
  vec3_t midpoint;

  // use the midpoint of the bounds instead of the origin, because
  // bmodels may have their origin is 0,0,0
  VectorAdd (targ->sh.r.absmin, targ->sh.r.absmax, midpoint);
  VectorScale (midpoint, 0.5, midpoint);

  gi->Trace (&tr, origin, vec3_origin, vec3_origin, midpoint, ENTITYNUM_NONE,
	     MASK_SOLID);
  if (tr.fraction == 1.0 || tr.entityNum == targ->sh.s.number)
    return true;

  // this should probably check in the plane of projection,
  // rather than in world coordinate, and also include Z
  // FIXME
  // currently :
  // y
  // ^            z axis is up/down : this a from top or bottom view
  // o > x
  //
  //      .       .
  //
  //          o
  //
  //      .       .
  /*
     // check xy plan
     VectorCopy (midpoint, dest);
     dest[0] += 15.0;
     dest[1] += 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     VectorCopy (midpoint, dest);
     dest[0] += 15.0;
     dest[1] -= 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     VectorCopy (midpoint, dest);
     dest[0] -= 15.0;
     dest[1] += 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     VectorCopy (midpoint, dest);
     dest[0] -= 15.0;
     dest[1] -= 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     // check xz plan
     VectorCopy (midpoint, dest);
     dest[0] += 15.0;
     dest[2] += 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     VectorCopy (midpoint, dest);
     dest[0] += 15.0;
     dest[2] -= 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     VectorCopy (midpoint, dest);
     dest[0] -= 15.0;
     dest[2] += 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     VectorCopy (midpoint, dest);
     dest[0] -= 15.0;
     dest[2] -= 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     // check yz plan
     VectorCopy (midpoint, dest);
     dest[1] += 15.0;
     dest[2] += 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     VectorCopy (midpoint, dest);
     dest[1] += 15.0;
     dest[2] -= 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     VectorCopy (midpoint, dest);
     dest[1] -= 15.0;
     dest[2] += 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;

     VectorCopy (midpoint, dest);
     dest[1] -= 15.0;
     dest[2] -= 15.0;
     gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE, MASK_SOLID);
     if (tr.fraction == 1.0)
     return true;
  */

  // --------------------------------------------------------------------------
  // new model - should avoid stairs unhit but creates others
  // check the 8 edges of a cube
  VectorCopy (midpoint, dest);
  dest[0] -= 15.0;
  dest[1] -= 15.0;
  dest[2] -= 15.0;
  gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE,
	     MASK_SOLID);
  if (tr.fraction == 1.0)
    return true;

  dest[1] += 30.0;
  gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE,
	     MASK_SOLID);
  if (tr.fraction == 1.0)
    return true;

  dest[0] += 30.0;
  gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE,
	     MASK_SOLID);
  if (tr.fraction == 1.0)
    return true;

  dest[1] -= 30.0;
  gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE,
	     MASK_SOLID);
  if (tr.fraction == 1.0)
    return true;

  dest[2] += 30.0;
  gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE,
	     MASK_SOLID);
  if (tr.fraction == 1.0)
    return true;

  dest[1] += 30.0;
  gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE,
	     MASK_SOLID);
  if (tr.fraction == 1.0)
    return true;

  dest[0] -= 30.0;
  gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE,
	     MASK_SOLID);
  if (tr.fraction == 1.0)
    return true;

  dest[1] -= 30.0;
  gi->Trace (&tr, origin, vec3_origin, vec3_origin, dest, ENTITYNUM_NONE,
	     MASK_SOLID);
  if (tr.fraction == 1.0)
    return true;
  // --------------------------------------------------------------------------

  return false;
}

/*
============
G_RadiusDamage
============
*/
bool
G_RadiusDamage (vec3_t origin, gentity_t * attacker, float damage,
		float radius, gentity_t * ignore, int mod)
{
  float points, dist;
  gentity_t *ent;
  int entityList[MAX_GENTITIES];
  int numListedEntities;
  vec3_t mins, maxs;
  vec3_t v;
  vec3_t dir;
  int i, e;
  bool hitClient = false;

  if (radius < 1)
    {
      radius = 1;
    }

  for (i = 0; i < 3; i++)
    {
      mins[i] = origin[i] - radius;
      maxs[i] = origin[i] + radius;
    }

  numListedEntities = gi->EntitiesInBox (mins, maxs, entityList, MAX_GENTITIES);

  for (e = 0; e < numListedEntities; e++)
    {
      ent = &g_entities[entityList[e]];

      if (ent == ignore)
	continue;
      if (!ent->takedamage)
	continue;

      // find the distance from the edge of the bounding box
      for (i = 0; i < 3; i++)
	{
	  if (origin[i] < ent->sh.r.absmin[i])
	    {
	      v[i] = ent->sh.r.absmin[i] - origin[i];
	    }
	  else if (origin[i] > ent->sh.r.absmax[i])
	    {
	      v[i] = origin[i] - ent->sh.r.absmax[i];
	    }
	  else
	    {
	      v[i] = 0;
	    }
	}

      dist = VectorLength (v);
      if (dist >= radius)
	{
	  continue;
	}

      // UPDATEME
      // make it non linear ?
      // default
      points = damage * (1.0 - dist / radius);
      // increases the damage value radius (from default), slow drop (1-x^2)
      //points = damage * ( 1.0 - (dist*dist) / (radius*radius) );

      if (CanDamage (ent, origin))
	{
	  if (LogAccuracyHit (ent, attacker))
	    {
	      hitClient = true;
	    }
	  VectorSubtract (ent->sh.r.currentOrigin, origin, dir);
	  // push the center of mass higher than the origin so players
	  // get knocked into the air more
	  if (ent == attacker)
	    // for rocket jumping behavior
	    dir[2] += 24;	// default 24
	  else
	    dir[2] += 40; // not really useful, mainly for splashes against walls
	  G_Damage (ent, NULL, attacker, dir, origin, (int) points,
		    DAMAGE_RADIUS, mod);
	}
    }

  return hitClient;
}

/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

level_locals_t level;
game_locals_t game;
teamgame_t teamgame;

gentity_t g_entities[MAX_GENTITIES];
gclient_t g_clients[MAX_CLIENTS];

// hackish to main
cvar_t* g_listEntity;

gamelib_import_t* gi;
gamelib_export_t ge;

void G_InitGame (int levelTime, int randomSeed, int restart);
void G_RunFrame (int levelTime);
void G_ShutdownGame (int restart);
void CheckExitRules (void);

/*
==========================
For dynamic loading

Give informations on the library
to the server
==========================
*/
char *
lib_info (void)
{
  return "Game Library alpha 0.00001337";
}

/*
============
G_InitGame

TODO : on restart we shouldn't need to reinitialize
a few things maybe (cvar ? but we need to redo the tracking)
============
*/
void
G_InitGame (int levelTime, int randomSeed, int restart)
{
  int i;

  G_Printf ("------- Game Initialization -------\n");
  G_Printf ("gamename: %s\n", GAMEVERSION);
  G_Printf ("gamedate: %s\n", __DATE__);

  srand (randomSeed);

  // cvars initialisation g_cvars.c
  G_InitCvars ();

  // hackish cvar proper to g_main.c
  g_listEntity = gCvar_Get ("g_listEntity", "0", 0);

  // init g_mem.c
  G_InitMemory ();

  // set some level and game globals
  Com_Memset (&level, 0, sizeof (level));
  Com_Memset (&game, 0, sizeof (game));

  level.time = 0;
  game.time = levelTime;

  // FIXME standing in lava / slime
  level.snd_fry = G_SoundIndex ("sound/player/fry.wav");

  G_LogFileInit (&level);

  G_InitWorldSession ();

  // initialize all entities for this game
  Com_Memset (g_entities, 0, MAX_GENTITIES * sizeof (g_entities[0]));
  level.gentities = g_entities;

  // initialize all clients for this game
  level.maxclients = g_maxclients->integer;
  Com_Memset (g_clients, 0, MAX_CLIENTS * sizeof (g_clients[0]));
  level.clients = g_clients;

  // set client fields on player ents
  for (i = 0; i < level.maxclients; i++)
    {
      g_entities[i].client = g_clients + i;
    }

  // always leave room for the max number of clients,
  // even if they aren't all used, so numbers inside that
  // range are NEVER anything but clients
  level.num_entities = MAX_CLIENTS;

  // let the server system know where the entites are
  gi->LocateGameData (&level.gentities->sh, level.num_entities,
		      sizeof (gentity_t), &level.clients[0].ps,
		      sizeof (level.clients[0]));

  // reserve some spots for dead player bodies
  InitBodyQue ();

  // some maps don't have some items, no need to use memory for them
  ClearRegisteredItems ();

  // parse the key/value pairs and spawn gentities
  G_SpawnEntitiesFromString ();

  // general initialization
  G_FindTeams ();

  // make sure we have flags for CTF, etc
  G_CheckTeamItems ();

  SaveRegisteredItems ();

  // initializing bots
  if (gi->Cvar_VariableIntegerValue ("bot_enable"))
    {
      G_Printf ("Loading game bots\n");

      G_Printf ("BotAISetup : %d\n", BotAISetup (restart));
      G_Printf ("BotAILoadMap : %d\n", BotAILoadMap (restart));
      G_InitBots (restart);
    }

  G_AddCommands ();

  G_Printf ("-----------------------------------\n");
}

/*
=================
G_ShutdownGame
=================
*/
void
G_ShutdownGame (int restart)
{
  G_Printf ("==== ShutdownGame ====\n");

  // write all the client session data so we can get it back
  G_WriteSessionData ();

  // stop bots
  BotAIShutdown (restart);

  G_LogFileClose (&level);

  G_FreeCommands ();

  G_FreeCvars ();
}

/*
================
G_SimulationTime
================
*/
int
G_SimulationTime (void)
{
  return level.time;
}

//===================================================================

/*
========================================================================

FUNCTIONS CALLED EVERY FRAME

========================================================================
*/

/*
==================
G_CheckVote
==================
*/
void
G_CheckVote (void)
{
  if (level.voteExecuteTime && (level.voteExecuteTime < game.time))
    {
      level.voteExecuteTime = 0;
      gi->SendConsoleCommand (EXEC_APPEND, va ("%s\n", level.voteString));
    }

  if (!level.voteTime)
    {
      return;
    }

  if ((game.time - level.voteTime) >= VOTE_TIME)
    {
      gi->SendServerCommand (-1, "print \"Vote failed.\n\"");
    }
  else
    {
      // ATVI Q3 1.32 Patch #9, WNF
      if (level.voteYes > level.numVotingClients / 2)
	{
	  char *msg;

	  msg = va ("print \"Vote passed. ("
		    S_COLOR_GREEN "YES" S_COLOR_WHITE ":%i - "
		    S_COLOR_RED "NO" S_COLOR_WHITE ":%i)\n\"",
		    level.voteYes, level.voteNo);
	  // execute the command, then remove the vote
	  gi->SendServerCommand (-1, msg);
	  level.voteExecuteTime = game.time + 2000;
	}
      else if (level.voteNo >= level.numVotingClients / 2)
	{
	  // same behavior as a timeout
	  gi->SendServerCommand (-1, "print \"Vote failed.\n\"");
	}
      else
	{
	  // still waiting for a majority
	  return;
	}
    }

  // we just did the wanted action; reset
  level.voteTime = 0;
  gi->SetConfigstring (CS_VOTE_TIME, "");
}

/*
==================
G_CheckTimeout
==================
*/
void
G_CheckTimeout (void)
{
  // game isn't paused or we don't plan to resume it
  if (!game.paused || !game.timeinTime)
    return;

  if (game.time > game.timeinTime)
    {
      game.paused = false;
      // shouldn't be needed
      game.timeoutTime = 0;
      game.timeinTime = 0;
      game.timeoutAnnoncerTime = 0;

      gi->SetConfigstring (CS_GAME_PAUSED, "0");

      gi->SendServerCommand (-1, "print \"The game has resumed.\n\"");
      gi->SendServerCommand (-1, "cp \"The game has resumed.\n\"");
      return;
    }

  if (game.time > game.timeoutAnnoncerTime)
    {
      // S_COLOR_WHITE
      gi->SendServerCommand (-1, va ("cp \"Resuming game\n in %i seconds.\n\"",
				     ((game.timeinTime - game.time)/1000) + 1));

      game.timeoutAnnoncerTime = game.time + 1000;
    }
}

/*
==================
G_CheckCvars
==================
*/
void
G_CheckCvars (void)
{
  static int lastMod = -1;

  if (g_password->modificationCount != lastMod)
    {
      lastMod = g_password->modificationCount;
      if (*g_password->string && Q_stricmp (g_password->string, "none"))
	{
	  gCvar_Set (g_needpass, "1");
	}
      else
	{
	  gCvar_Set (g_needpass, "0");
	}
    }
}

/*
=============
G_RunThink

Runs thinking code for this frame if necessary
=============
*/
void
G_RunThink (gentity_t * ent)
{
  assert (ent != NULL);

  float thinktime;

  thinktime = ent->nextthink;
  if (thinktime <= 0)
    {
      return;
    }
  if (thinktime > level.time)
    {
      return;
    }

  ent->nextthink = 0;
  if (!ent->think)
    {
      G_Error ("NULL ent->think");
    }
  ent->think (ent);
}

/*
=====================
G_Display_AllEntities

if g_listEntity dump
all entities to console
=====================
*/
void
G_Display_AllEntities (void)
{
  int i;

  if (g_listEntity->integer)
    {
      for (i = 0; i < MAX_GENTITIES; i++)
	{
	  G_Printf ("%4i: %s\n", i, g_entities[i].classname);
	}

      gCvar_Set (g_listEntity, "0");
    }
}

/*
================
G_RunEntities

Advances all entities/objects in the world
================
*/
void
G_RunEntities (gentity_t * ent, lvl_t * level)
{
  int i;

  for (i = 0; i < level->num_entities; i++, ent++)
    {
      if (!ent->inuse)
	{
	  continue;
	}

      // this one shouldn't happend
      if (level->time - ent->eventTime < 0)
	{
	  G_Printf ("%d : level->time - ent->eventTime is negative !!!\n",
		    ent - g_entities);
	}
      // clear events that are too old
      if ((level->time - ent->eventTime) > EVENT_VALID_MSEC)
	{
	  if (ent->sh.s.event)
	    {
	      ent->sh.s.event = 0;	// &= EV_EVENT_BITS;
	      if (ent->client)
		{
		  ent->client->ps.externalEvent = 0;
		  // predicted events should never be set to zero
		  //ent->client->ps.events[0] = 0;
		  //ent->client->ps.events[1] = 0;
		}
	    }
	  if (ent->freeAfterEvent)
	    {
	      // tempEntities or dropped items completely go away after their event
	      G_FreeEntity (ent);
	      continue;
	    }
	  else if (ent->unlinkAfterEvent)
	    {
	      // items that will respawn will hide themselves after their pickup event
	      ent->unlinkAfterEvent = false;
	      gi->UnlinkEntity (&ent->sh);
	    }
	}

      // temporary entities don't think
      if (ent->freeAfterEvent)
	{
	  continue;
	}

      if (!ent->sh.r.linked && ent->neverFree)
	{
	  continue;
	}

      if ((ent->sh.s.eType == ET_ITEM) || ent->physicsObject)
	{
	  G_RunItem (ent);
	  continue;
	}

      if (ent->sh.s.eType == ET_MOVER)
	{
	  G_RunMover (ent);
	  continue;
	}

      if (ent->sh.s.eType == ET_MISSILE)
	{
	  G_RunMissile (ent);
	  continue;
	}

      if (i <= level->clientHighestID)
	{
	  G_RunClient (ent);
	  continue;
	}

      G_RunThink (ent);
    }
}


/*
================
G_RunFrame

Advances the entities in the world,
update server and player status.
================
*/
void
G_RunFrame (int levelTime)
{
  int i;
  gentity_t *ent;
  const int dotiming = 0;
  int start, end;

  // if we are waiting for the level to restart, do nothing
  if (level.restarted)
    {
      return;
    }

  game.framenum++;
  game.previousTime = game.time;
  game.time = levelTime;
  game.msec = game.time - game.previousTime;

  if (!game.paused)
    {
      level.framenum++;
      level.previousTime = level.time;
      level.time += game.msec;
    }

  // TODO : get any cvar changes ?

  //
  // go through all allocated objects
  //
  if (dotiming)
    start = gi->Milliseconds ();

  ent = &g_entities[0];

  if (game.paused)
    {
      for (i = 0; i <= level.clientHighestID; i++, ent++)
	{
	  G_RunClient (ent);
	}
    }
  else
    {
      G_RunEntities (ent, &level);
    }

  if (dotiming)
    end = gi->Milliseconds ();

  if (dotiming)
    start = gi->Milliseconds ();

  // perform final fixups on the players
  ent = &g_entities[0];
  for (i = 0; i <= level.clientHighestID; i++, ent++)
    {
      if (ent->inuse)
	{
	  ClientEndFrame (ent);
	}
    }

  if (dotiming)
    end = gi->Milliseconds ();

  // run all the match related logic
  // (warmup/live/end/intermission)
  G_Match_Frame (&game, &level);

  // see if it is time to do a tournement restart
  //G_CheckTournament ();

  // see if it is time to end the level
  CheckExitRules ();

  // update to team status?
  CheckTeamStatus ();

  // cancel vote if timed out
  G_CheckVote ();

  // see if we will resume the next frame or display infos to players
  G_CheckTimeout ();

  // for tracking changes
  G_CheckCvars ();

  // if g_listEntity, display all entities for this frame
  G_Display_AllEntities ();
}

/*
==================================
GetGameAPI

retrieve and export
functions with the server/
==================================
*/
gamelib_export_t *
GetGameAPI (int version, gamelib_import_t * gimp)
{
  if (!gimp)
    {
      G_Error ("GetGameAPI () : No server functions interface. Exiting\n");
    }
  gi = gimp;
  G_Printf ("GetGameAPI () : Loaded Server API in Game\n");

  // setup the print/error functions for qshared
  q_shared_import_t q_shared_imp;
  q_shared_imp.print = gi->Printf;
  q_shared_imp.error = gi->Error;

  Q_Set_Interface (&q_shared_imp);

  Com_Memset (&ge, 0, sizeof (ge));

  ge.init = G_InitGame;
  ge.shutdown = G_ShutdownGame;
  ge.run_frame = G_RunFrame;
  ge.game_simulation_time = G_SimulationTime;
  ge.client_connect = ClientConnect;
  ge.client_disconnect = ClientDisconnect;
  ge.client_think = ClientThink;
  ge.client_userinfo_changed = ClientUserinfoChanged;
  ge.client_begin = ClientBegin;
  ge.client_command = ClientCommand;
  ge.console_command = ConsoleCommand;

  ge.botai_start_frame = BotAIStartFrame;

  return &ge;
}

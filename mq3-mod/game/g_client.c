/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

// g_client.c -- client functions that don't happen every frame

//static vec3_t playerMins = { -15, -15, -24 };
//static vec3_t playerMaxs = { 15, 15, 32 };

// removing static because of bots
vec3_t playerMins = { -15, -15, -24 };
vec3_t playerMaxs = { 15, 15, 32 };

/*QUAKED info_player_deathmatch (1 0 1) (-16 -16 -24) (16 16 32) initial
potential spawning position for deathmatch games.
The first time a player enters the game, they will be at an 'initial' spot.
Targets will be fired when someone spawns in on them.
"nobots" will prevent bots from using this spot.
"nohumans" will prevent non-bots from using this spot.
*/
void
SP_info_player_deathmatch (gentity_t * ent)
{
  int i;

  G_SpawnInt ("nobots", "0", &i);
  if (i)
    {
      ent->flags |= FL_NO_BOTS;
    }
  G_SpawnInt ("nohumans", "0", &i);
  if (i)
    {
      ent->flags |= FL_NO_HUMANS;
    }
}

/*QUAKED info_player_start (1 0 0) (-16 -16 -24) (16 16 32)
equivelant to info_player_deathmatch
*/
void
SP_info_player_start (gentity_t * ent)
{
  ent->classname = "info_player_deathmatch";
  SP_info_player_deathmatch (ent);
}

/*QUAKED info_player_intermission (1 0 1) (-16 -16 -24) (16 16 32)
The intermission will be viewed from this point.  Target an info_notnull for the view direction.
*/
void
SP_info_player_intermission (gentity_t * ent)
{

}

/*
=======================================================================

  Client Helper Functions

=======================================================================
*/

/*
=================
G_Client_IsActive

Helper function, check for the client
to be playing ingame
=================
*/
bool
G_Client_IsPlaying (gclient_t * cl)
{
  if (cl->sess.sessionTeam == TEAM_SPECTATOR)
    {
      return false;
    }
  if (cl->pers.connected != CON_CONNECTED)
    {
      return false;
    }

  return true;
}

/*
=================
G_Client_IsIngame

Check for the client to have finished
connecting to the game (player ingame)
=================
*/
bool
G_Client_IsIngame (gclient_t * cl)
{
  if (cl->pers.connected != CON_CONNECTED)
    {
      return false;
    }

  return true;
}

/*
====================
G_Client_ValidEntity

Check if this gentity is used
by a valid client
====================
*/
bool
G_Client_ValidEntity (gentity_t * client)
{
  if (!client->inuse)
    {
      return false;
    }

  return true;
}

/*
=======================================================================

  SelectSpawnPoint

=======================================================================
*/

/*
================
SpotWouldTelefrag

================
*/
bool
SpotWouldTelefrag (gentity_t * spot)
{
  int i, num;
  int touch[MAX_GENTITIES];
  gentity_t *hit;
  vec3_t mins, maxs;

  VectorAdd (spot->sh.s.origin, playerMins, mins);
  VectorAdd (spot->sh.s.origin, playerMaxs, maxs);
  num = gi->EntitiesInBox (mins, maxs, touch, MAX_GENTITIES);

  for (i = 0; i < num; i++)
    {
      hit = &g_entities[touch[i]];
      //if ( hit->client && hit->client->ps.stats[STAT_HEALTH] > 0 ) {
      if (hit->client)
	{
	  return true;
	}
    }

  return false;
}

/*
================
SelectNearestDeathmatchSpawnPoint

Find the spot that we DON'T want to use
================
*/
#define MAX_SPAWN_POINTS 128
gentity_t *
SelectNearestDeathmatchSpawnPoint (vec3_t from)
{
  gentity_t *spot;
  gentity_t *nearestSpot;
  vec3_t delta;
  float dist, nearestDist;

  nearestDist = 999999;
  nearestSpot = NULL;
  spot = NULL;

  while ((spot = G_Find (spot, FOFS (classname), "info_player_deathmatch")) != NULL)
    {
      VectorSubtract (spot->sh.s.origin, from, delta);
      dist = VectorLength (delta);
      if (dist < nearestDist)
	{
	  nearestDist = dist;
	  nearestSpot = spot;
	}
    }

  return nearestSpot;
}

/*
================
SelectRandomDeathmatchSpawnPoint

go to a random point that doesn't telefrag
================
*/
gentity_t *
SelectRandomDeathmatchSpawnPoint (vec3_t origin, vec3_t angles, bool isBot)
{
  gentity_t *spot;
  gentity_t *spots[MAX_SPAWN_POINTS];
  int count;
  int selection;

  count = 0;
  spot = NULL;

  while (((spot = G_Find (spot, FOFS (classname), "info_player_deathmatch")) != NULL)
	  && (count < MAX_SPAWN_POINTS))
    {
      if (SpotWouldTelefrag (spot))
	{
	  continue;
	}

      if (((spot->flags & FL_NO_BOTS) && isBot)
	  || ((spot->flags & FL_NO_HUMANS) && !isBot))
	{
	  // spot is not for this human/bot player
	  continue;
	}

      spots[count] = spot;
      count++;
    }

  if (!count)
    {	// no spots that won't telefrag
      spot = G_Find (NULL, FOFS (classname), "info_player_deathmatch");
      if (spot == NULL)
	G_Error ("Couldn't find a spawn point");

      // fill the origin and angles field
      VectorCopy (spot->sh.s.origin, origin);
      VectorCopy (spot->sh.s.angles, angles);

      return spot;
    }

  selection = rand () % count;

  // fill the origin and angles field
  VectorCopy (spots[selection]->sh.s.origin, origin);
  VectorCopy (spots[selection]->sh.s.angles, angles);

  return spots[selection];
}

/*
===========
SelectRandomFurthestSpawnPoint

Chooses a player start, deathmatch start, etc
============
*/
#define MAX_FSP 64
gentity_t *
SelectRandomFurthestSpawnPoint (vec3_t avoidPoint, vec3_t origin,
				vec3_t angles, bool isBot)
{
  gentity_t *spot;
  vec3_t delta;
  float dist;
  float list_dist[MAX_FSP];
  gentity_t *list_spot[MAX_FSP];
  int numSpots, rnd, i, j;

  numSpots = 0;
  spot = NULL;

  Com_Memset (list_dist, 0, sizeof (list_dist));
  Com_Memset (list_spot, 0, sizeof (list_spot));

  while ((spot = G_Find (spot, FOFS (classname), "info_player_deathmatch")) != NULL)
    {
      if (SpotWouldTelefrag (spot))
	{
	  continue;
	}

      if (((spot->flags & FL_NO_BOTS) && isBot)
	  || ((spot->flags & FL_NO_HUMANS) && !isBot))
	{
	  // spot is not for this human/bot player
	  continue;
	}

      VectorSubtract (spot->sh.s.origin, avoidPoint, delta);
      dist = VectorLength (delta);

      // insert sort
      for (i = 0; i < numSpots; i++)
	{
	  if (dist > list_dist[i])
	    {
	      if (numSpots >= MAX_FSP)
		numSpots = MAX_FSP - 1;

	      for (j = numSpots; j > i; j--)
		{
		  list_dist[j] = list_dist[j - 1];
		  list_spot[j] = list_spot[j - 1];
		}

	      list_dist[i] = dist;
	      list_spot[i] = spot;

	      numSpots++;

	      break;
	    }
	}

      if ((i >= numSpots) && (numSpots < MAX_FSP))
	{
	  list_dist[numSpots] = dist;
	  list_spot[numSpots] = spot;
	  numSpots++;
	}
    }

  // no valid spawn point
  if (!numSpots)
    {
      spot = G_Find (NULL, FOFS (classname), "info_player_deathmatch");
      if (spot == NULL)
	G_Error ("Couldn't find a spawn point");

      // fill the origin and angles field
      VectorCopy (spot->sh.s.origin, origin);
      VectorCopy (spot->sh.s.angles, angles);

      return spot;
    }

  // select a random spot from the spawn points furthest away
  rnd = random () * (numSpots / 2);

  // fill the origin and angles field
  VectorCopy (list_spot[rnd]->sh.s.origin, origin);
  VectorCopy (list_spot[rnd]->sh.s.angles, angles);

  return list_spot[rnd];
}

/*
===========
SelectSpawnPoint

Chooses a player start, deathmatch start, etc
============
*/
gentity_t *
SelectSpawnPoint (vec3_t avoidPoint, vec3_t origin, vec3_t angles, bool isBot)
{
  gentity_t * spawnpoint;

  spawnpoint = SelectRandomFurthestSpawnPoint (avoidPoint, origin, angles, isBot);

  return spawnpoint;
}

/*
===========
SelectInitialSpawnPoint

Try to find a spawn point marked 'initial', otherwise
use normal spawn selection.
============
*/
gentity_t *
SelectInitialSpawnPoint (vec3_t origin, vec3_t angles, bool isBot)
{
  gentity_t *spot;

  spot = NULL;
  while ((spot = G_Find (spot, FOFS (classname), "info_player_deathmatch")) != NULL)
    {
      if (((spot->flags & FL_NO_BOTS) && isBot)
	  || ((spot->flags & FL_NO_HUMANS) && !isBot))
	{
	  continue;
	}

      if ((spot->spawnflags & 0x01))
	{
	  break;
	}
    }

  if ((spot == NULL) || SpotWouldTelefrag (spot))
    {
      return SelectSpawnPoint (vec3_origin, origin, angles, isBot);
    }

  // fill the origin and angles field
  VectorCopy (spot->sh.s.origin, origin);
  VectorCopy (spot->sh.s.angles, angles);

  return spot;
}

/*
===========
SelectSpectatorSpawnPoint

============
*/
// from g_match_intermission
gentity_t * G_Intermission_FindPoint (vec3_t origin, vec3_t angle);

gentity_t *
SelectSpectatorSpawnPoint (vec3_t origin, vec3_t angles)
{
  gentity_t *spot;
  spot = G_Intermission_FindPoint (origin, angles);

  return spot;
}

/*
=======================================================================

BODYQUE

=======================================================================
*/

/*
===============
InitBodyQue
===============
*/
void
InitBodyQue (void)
{
  int i;
  gentity_t *ent;

  level.bodyQueIndex = 0;
  for (i = 0; i < BODY_QUEUE_SIZE; i++)
    {
      ent = G_Spawn ();
      ent->classname = "bodyque";
      ent->neverFree = true;
      level.bodyQue[i] = ent;
    }
}

/*
=============
BodySink

After sitting around for five seconds, fall into the ground and dissapear
=============
*/
void
BodySink (gentity_t * ent)
{
  if (level.time - ent->timestamp > 6500)
    {
      // the body ques are never actually freed, they are just unlinked
      gi->UnlinkEntity (&ent->sh);
      ent->physicsObject = false;
      return;
    }
  ent->nextthink = level.time + 100;
  ent->sh.s.pos.trBase[2] -= 1;
}

/*
=============
CopyToBodyQue

A player is respawning, so make an entity that looks
just like the existing corpse to leave behind.
=============
*/
void
CopyToBodyQue (gentity_t * ent)
{
  gentity_t *body;
  int contents;

  gi->UnlinkEntity (&ent->sh);

  // if client is in a nodrop area, don't leave the body
  contents = gi->PointContents (ent->sh.s.origin, -1);
  if (contents & CONTENTS_NODROP)
    {
      return;
    }

  // grab a body que and cycle to the next one
  body = level.bodyQue[level.bodyQueIndex];
  level.bodyQueIndex = (level.bodyQueIndex + 1) % BODY_QUEUE_SIZE;

  gi->UnlinkEntity (&(body->sh));

  body->sh.s = ent->sh.s;
  body->sh.s.eFlags = EF_DEAD;	// clear EF_TALK, etc

  body->sh.s.powerups = 0;	// clear powerups
  body->sh.s.loopSound = 0;	// clear lava burning
  body->sh.s.number = body - g_entities;
  body->timestamp = level.time;
  body->physicsObject = true;
  body->physicsBounce = 0;	// don't bounce
  if (body->sh.s.groundEntityNum == ENTITYNUM_NONE)
    {
      body->sh.s.pos.trType = TR_GRAVITY;
      body->sh.s.pos.trTime = level.time;
      VectorCopy (ent->client->ps.velocity, body->sh.s.pos.trDelta);
    }
  else
    {
      body->sh.s.pos.trType = TR_STATIONARY;
    }
  body->sh.s.event = 0;

  // change the animation to the last-frame only, so the sequence
  // doesn't repeat anew for the body
  switch (body->sh.s.legsAnim & ~ANIM_TOGGLEBIT)
    {
    case BOTH_DEATH1:
    case BOTH_DEAD1:
      body->sh.s.torsoAnim = body->sh.s.legsAnim = BOTH_DEAD1;
      break;
    case BOTH_DEATH2:
    case BOTH_DEAD2:
      body->sh.s.torsoAnim = body->sh.s.legsAnim = BOTH_DEAD2;
      break;
    case BOTH_DEATH3:
    case BOTH_DEAD3:
    default:
      body->sh.s.torsoAnim = body->sh.s.legsAnim = BOTH_DEAD3;
      break;
    }

  body->sh.r.svFlags = ent->sh.r.svFlags;
  VectorCopy (ent->sh.r.mins, body->sh.r.mins);
  VectorCopy (ent->sh.r.maxs, body->sh.r.maxs);
  VectorCopy (ent->sh.r.absmin, body->sh.r.absmin);
  VectorCopy (ent->sh.r.absmax, body->sh.r.absmax);

  body->clipmask = CONTENTS_SOLID | CONTENTS_PLAYERCLIP;
  body->sh.r.contents = CONTENTS_CORPSE;
  body->sh.r.ownerNum = ent->sh.s.number;

  body->nextthink = level.time + 5000;
  body->think = BodySink;

  body->die = body_die;

  // don't take more damage if already gibbed
  if (ent->health <= GIB_HEALTH)
    {
      body->takedamage = false;
    }
  else
    {
      body->takedamage = true;
    }

  VectorCopy (body->sh.s.pos.trBase, body->sh.r.currentOrigin);
  gi->LinkEntity (&(body->sh));
}

//======================================================================

/*
==================
SetClientViewAngle

==================
*/
void
SetClientViewAngle (gentity_t * ent, vec3_t angle)
{
  int i;

  // set the delta angle
  for (i = 0; i < 3; i++)
    {
      int cmdAngle;

      cmdAngle = ANGLE2SHORT (angle[i]);
      ent->client->ps.delta_angles[i] =
	cmdAngle - ent->client->pers.cmd.angles[i];
    }
  VectorCopy (angle, ent->sh.s.angles);
  VectorCopy (ent->sh.s.angles, ent->client->ps.viewangles);
}

/*
================
respawn
================
*/
void
respawn (gentity_t * ent)
{
  gentity_t *tent;

  CopyToBodyQue (ent);
  ClientSpawn (ent);

  // add a teleportation effect
  tent = G_TempEntity (ent->client->ps.origin, EV_PLAYER_TELEPORT_IN);
  tent->sh.s.clientNum = ent->sh.s.clientNum;
}

/*
================
TeamCount

Returns number of players on a team
================
*/
team_t
TeamCount (int ignoreClientNum, team_t team)
{
  int i;
  int count = 0;

  for (i = 0; i < level.maxclients; i++)
    {
      if (i == ignoreClientNum)
	{
	  continue;
	}
      // if the player isn't connected, skip, he won't have a "valid" team though
      if (level.clients[i].pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      if (level.clients[i].sess.sessionTeam == team)
	{
	  count++;
	}
    }

  return count;
}

/*
================
TeamLeader

Returns the client number of the team leader
================
*/
int
TeamLeader (team_t team)
{
  int i;

  for (i = 0; i < level.maxclients; i++)
    {
      if (level.clients[i].pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      if (level.clients[i].sess.sessionTeam == team)
	{
	  if (level.clients[i].sess.teamLeader)
	    return i;
	}
    }

  return -1;
}

/*
================
PickTeam

================
*/
team_t
PickTeam (int ignoreClientNum)
{
  int counts[TEAM_NUM_TEAMS];

  counts[TEAM_BLUE] = TeamCount (ignoreClientNum, TEAM_BLUE);
  counts[TEAM_RED] = TeamCount (ignoreClientNum, TEAM_RED);

  if (counts[TEAM_BLUE] > counts[TEAM_RED])
    {
      return TEAM_RED;
    }
  if (counts[TEAM_RED] > counts[TEAM_BLUE])
    {
      return TEAM_BLUE;
    }
  // equal team count, so join the team with the lowest score
  if (level.teamScores[TEAM_BLUE] > level.teamScores[TEAM_RED])
    {
      return TEAM_RED;
    }
  return TEAM_BLUE;
}

/*
===========
ClientCheckName
============
*/
static void
ClientCleanName (const char *in, char *out, int outSize)
{
  int outpos = 0, colorlessLen = 0, spaces = 0;

  // discard leading spaces
  for (; *in == ' '; in++);

  for (; *in && (outpos < outSize - 1); in++)
    {
      out[outpos] = *in;

      if (*in == ' ')
	{
	  // don't allow too many consecutive spaces
	  if (spaces > 2)
	    continue;

	  spaces++;
	}
      else if (outpos > 0 && out[outpos - 1] == Q_COLOR_ESCAPE)
	{
	  if (Q_IsColorString (&out[outpos - 1]))
	    {
	      colorlessLen--;

	      if (ColorIndex (*in) == 0)
		{
		  // Disallow color black in names to prevent players
		  // from getting advantage playing in front of black backgrounds
		  outpos--;
		  continue;
		}
	    }
	  else
	    {
	      spaces = 0;
	      colorlessLen++;
	    }
	}
      else
	{
	  spaces = 0;
	  colorlessLen++;
	}

      outpos++;
    }

  out[outpos] = '\0';

  // don't allow empty names
  if (*out == '\0' || colorlessLen == 0)
    Q_strncpyz (out, "UnnamedPlayer", outSize);
}

/*
===========
ClientUserInfoChanged

Called from ClientConnect when the player first connects and
directly by the server system when the player updates a userinfo variable.

The game can override any of the settings and call trap_SetUserinfo
if desired.
============
*/
void
ClientUserinfoChanged (int clientNum)
{
  gentity_t *ent;
  gclient_t *client;
  int teamTask, teamLeader, team, health;
  char *s;
  char model[MAX_QPATH];
  char headModel[MAX_QPATH];
  char oldname[MAX_STRING_CHARS];
  char c1[MAX_INFO_STRING];
  char c2[MAX_INFO_STRING];
  // model color
  char modelcolor[MAX_INFO_STRING];
  char userinfo[MAX_INFO_STRING];

  /*
     char         redTeam[MAX_INFO_STRING];
     char         blueTeam[MAX_INFO_STRING];
   */

  ent = g_entities + clientNum;
  client = ent->client;

  gi->GetUserinfo (clientNum, userinfo, sizeof (userinfo));

  // this is not the userinfo, more like the configstring actually
  G_LogPrintf ("Received client %i userinfo : %s\n", clientNum, userinfo);

  // check for malformed or illegal info strings
  if (!Info_Validate (userinfo))
    {
      strcpy (userinfo, "\\name\\badinfo");
      gi->DropClient (clientNum, "Invalid userinfo");
    }

  // check for local client
  s = Info_ValueForKey (userinfo, "ip");
  if (!strcmp (s, "localhost"))
    {
      client->pers.localClient = true;
    }

  // set name
  Q_strncpyz (oldname, client->pers.netname, sizeof (oldname));
  s = Info_ValueForKey (userinfo, "name");
  ClientCleanName (s, client->pers.netname, sizeof (client->pers.netname));

  if (client->sess.sessionTeam == TEAM_SPECTATOR)
    {
      if (client->sess.spectatorState == SPECTATOR_SCOREBOARD)
	{
	  Q_strncpyz (client->pers.netname, "scoreboard",
		      sizeof (client->pers.netname));
	}
    }

  if (client->pers.connected == CON_CONNECTED)
    {
      if (strcmp (oldname, client->pers.netname))
	{
	  gi->SendServerCommand (-1, va ("print \"%s" S_COLOR_WHITE
					 " renamed to %s\n\"",
					 oldname, client->pers.netname));
	}
    }

  // set max health
  health = atoi (Info_ValueForKey (userinfo, "ci_handicap"));
  client->pers.maxHealth = health;
  if (client->pers.maxHealth < 1 || client->pers.maxHealth > 100)
    {
      client->pers.maxHealth = 100;
    }
  client->ps.stats[STAT_MAX_HEALTH] = client->pers.maxHealth;

  // set model
  if (BG_GametypeIsTeam (g_gametype->integer))
    {
      Q_strncpyz (model, Info_ValueForKey (userinfo, "ci_team_model"),
		  sizeof (model));
      Q_strncpyz (headModel, Info_ValueForKey (userinfo, "ci_team_headmodel"),
		  sizeof (headModel));
    }
  else
    {
      Q_strncpyz (model, Info_ValueForKey (userinfo, "ci_model"),
		  sizeof (model));
      Q_strncpyz (headModel, Info_ValueForKey (userinfo, "ci_headmodel"),
		  sizeof (headModel));
    }

  // bots set their team a few frames later
  if (BG_GametypeIsTeam (g_gametype->integer)
      && g_entities[clientNum].sh.r.svFlags & SVF_BOT)
    {
      s = Info_ValueForKey (userinfo, "team");
      if (!Q_stricmp (s, "red") || !Q_stricmp (s, "r"))
	{
	  team = TEAM_RED;
	}
      else if (!Q_stricmp (s, "blue") || !Q_stricmp (s, "b"))
	{
	  team = TEAM_BLUE;
	}
      else
	{
	  // pick the team with the least number of players
	  team = PickTeam (clientNum);
	}
    }
  else
    {
      team = client->sess.sessionTeam;
    }

  // teamInfo
  s = Info_ValueForKey (userinfo, "teamoverlay");
  if (!*s || atoi (s) != 0)
    {
      client->pers.teamInfo = true;
    }
  else
    {
      client->pers.teamInfo = false;
    }

  // team task (0 = none, 1 = offence, 2 = defence)
  teamTask = atoi (Info_ValueForKey (userinfo, "teamtask"));
  // team Leader (1 = leader, 0 is normal player)
  teamLeader = client->sess.teamLeader;

  // colors
  strcpy (c1, Info_ValueForKey (userinfo, "ci_color1"));
  strcpy (c2, Info_ValueForKey (userinfo, "ci_color2"));

  // model color
  strcpy (modelcolor, Info_ValueForKey (userinfo, "ci_modelcolor"));
  //Com_Printf("modelcolor string : %s\n", modelcolor);

  // send over a subset of the userinfo keys so other clients can
  // print scoreboards, display models, and play custom sounds
  if (ent->sh.r.svFlags & SVF_BOT)
    {
      s = va (CS_PLAYER_NAME "\\%s\\" CS_PLAYER_TEAM "\\%i\\" CS_PLAYER_MODEL
	      "\\%s\\" CS_PLAYER_HEADMODEL "\\%s\\" CS_PLAYER_COLOR1 "\\%s\\"
	      CS_PLAYER_COLOR2 "\\%s\\" CS_PLAYER_MODELCOLOR "\\%s\\"
	      CS_PLAYER_HANDICAP "\\%i\\" CS_PLAYER_WINS "\\%i\\"
	      CS_PLAYER_LOSSES "\\%i\\" CS_PLAYER_SKILL "\\%s\\"
	      CS_PLAYER_TEAMTASK "\\%i\\" CS_PLAYER_TEAMLEADER "\\%i",
	      client->pers.netname, team, model, headModel, c1, c2, modelcolor,
	      client->pers.maxHealth, client->sess.wins, client->sess.losses,
	      Info_ValueForKey (userinfo, "skill"), teamTask, teamLeader);
    }
  else
    {
      s = va (CS_PLAYER_NAME "\\%s\\" CS_PLAYER_TEAM "\\%i\\" CS_PLAYER_MODEL
	      "\\%s\\" CS_PLAYER_HEADMODEL "\\%s\\" CS_PLAYER_COLOR1 "\\%s\\"
	      CS_PLAYER_COLOR2 "\\%s\\" CS_PLAYER_MODELCOLOR "\\%s\\"
	      CS_PLAYER_HANDICAP "\\%i\\" CS_PLAYER_WINS "\\%i\\"
	      CS_PLAYER_LOSSES "\\%i\\", client->pers.netname,
	      client->sess.sessionTeam, model, headModel, c1, c2, modelcolor,
	      client->pers.maxHealth, client->sess.wins, client->sess.losses);
    }

  gi->SetConfigstring (CS_PLAYERS + clientNum, s);

  // this is not the userinfo, more like the configstring actually
  G_LogPrintf ("ClientUserinfoChanged: %i %s\n", clientNum, s);
}


static void
ClientSaveHighestAdd (int clientNum)
{
  if (clientNum > level.clientHighestID)
    {
      level.clientHighestID = clientNum;
    }
}

static void
ClientSaveHighestDel (int clientNum)
{
  int i;
  gentity_t* ent;

  /*
  for (i = clientNum; i < level.clientHighestID; i++)
    {
      ent = g_entities + i;

      // looking for a valid client
      if (G_Client_ValidEntity (ent))
	{
	  return;
	}
    }
  */
  // we have to find the new highest clientNum
  // otherwise nothing to do
  if (clientNum == level.clientHighestID)
    {
      for (i = clientNum; i >= 0; i--)
	{
	  ent = g_entities + i;

	  // looking for a valid client
	  if (G_Client_ValidEntity (ent))
	    {
	      level.clientHighestID = i;
	      break;
	    }
	}
    }
}

/*
===========
ClientConnect

Called when a player begins connecting to the server.
Called again for every map change or tournement restart.

The session information will be valid after exit.

Return NULL if the client should be allowed, otherwise return
a string with the reason for denial.

Otherwise, the client will be sent the current gamestate
and will eventually get to ClientBegin.

firstTime will be true the very first time a client connects
to the server machine, but false on map changes and tournement
restarts.
============
*/
char *
ClientConnect (int clientNum, bool firstTime, bool isBot)
{
  char *value;
  // char *areabits;
  gclient_t *client;
  char userinfo[MAX_INFO_STRING];
  gentity_t *ent;

  ent = &g_entities[clientNum];

  gi->GetUserinfo (clientNum, userinfo, sizeof (userinfo));

  // we don't check password for bots
  if (!isBot)
    {
      // check for a password
      value = Info_ValueForKey (userinfo, "password");
      if (g_password->string[0] && Q_stricmp (g_password->string, "none") &&
	  strcmp (g_password->string, value) != 0)
	{
	  return "Invalid password";
	}
    }
  // if a player reconnects quickly after a disconnect,
  // the client disconnect may never be called,
  // thus flag can get lost in the ether
  if (ent->inuse)
    {
      G_LogPrintf ("Forcing disconnect on active client: %i\n", clientNum);
      // so lets just fix up anything that should happen on a disconnect
      ClientDisconnect (clientNum);
    }

  // they can connect
  ent->client = level.clients + clientNum;
  client = ent->client;

  Com_Memset (client, 0, sizeof (*client));

  client->pers.connected = CON_CONNECTING;

  // read or initialize the session data
  if (firstTime || level.newSession)
    {
      G_InitSessionData (client, userinfo);
    }
  else
    {
      G_ReadSessionData (client);
    }

  if (isBot)
    {
      ent->sh.r.svFlags |= SVF_BOT;
      ent->inuse = true;
      if (!G_BotConnect (clientNum, !firstTime))
	{
	  return "BotConnectFailed";
	}
    }

  // get and distribute relevent paramters
  G_LogPrintf ("ClientConnect: %i\n", clientNum);
  ClientUserinfoChanged (clientNum);

  // don't do the "xxx connected" messages if they were caried over from previous level
  if (firstTime)
    {
      gi->SendServerCommand (-1, va ("print \"%s" S_COLOR_WHITE " connected\n\"",
				     client->pers.netname));
    }

  if ((BG_GametypeIsTeam (g_gametype->integer))
      && (client->sess.sessionTeam != TEAM_SPECTATOR))
    {
      BroadcastTeamChange (client, -1);
    }

  // count current clients and rank for scoreboard
  CalculateRanks ();

  ClientSaveHighestAdd (clientNum);

  return NULL;
}

/*
===========
ClientBegin

called when a client has finished connecting, and is ready
to be placed into the level.  This will happen every level load,
and on transition between teams, but doesn't happen on respawns
============
*/
void
ClientBegin (int clientNum)
{
  gentity_t *ent;
  gclient_t *client;
  gentity_t *tent;
  int flags;

  ent = g_entities + clientNum;
  client = level.clients + clientNum;

  if (ent->sh.r.linked)
    {
      gi->UnlinkEntity (&ent->sh);
    }
  G_InitGentity (ent);
  ent->touch = 0;
  ent->pain = 0;
  ent->client = client;

  client->pers.connected = CON_CONNECTED;
  client->pers.enterTime = game.time;
  client->pers.teamState.state = TEAM_BEGIN;

  // save eflags around this, because changing teams will
  // cause this to happen with a valid entity, and we
  // want to make sure the teleport bit is set right
  // so the viewpoint doesn't interpolate through the
  // world to the new position
  flags = client->ps.eFlags;
  Com_Memset (&client->ps, 0, sizeof (client->ps));
  client->ps.eFlags = flags;

  // locate ent at a spawn point
  ClientSpawn (ent);

  if (client->sess.sessionTeam != TEAM_SPECTATOR)
    {
      // send event
      tent = G_TempEntity (ent->client->ps.origin, EV_PLAYER_TELEPORT_IN);
      tent->sh.s.clientNum = ent->sh.s.clientNum;

      if (g_gametype->integer != GT_TOURNAMENT)
	{
	  gi->SendServerCommand (-1, va ("print \"%s" S_COLOR_WHITE
					 " entered the game\n\"",
					 client->pers.netname));
	}
    }
  G_LogPrintf ("ClientBegin: %i\n", clientNum);

  // count current clients and rank for scoreboard
  CalculateRanks ();
}

/*
===========
ClientSpawn

Called every time a client is placed fresh in the world:
after the first ClientBegin, and after each respawn
Initializes all non-persistant parts of playerState
============
*/
void
ClientSpawn (gentity_t * ent)
{
  int index;
  vec3_t spawn_origin, spawn_angles;
  gclient_t *client;
  gentity_t *spawnPoint;
  int i;
  int persistant[MAX_PERSISTANT];
  int flags;
  int savedPing;
  int accuracy_hits, accuracy_shots;
  char userinfo[MAX_INFO_STRING];

  index = ent - g_entities;
  client = ent->client;

  // find a spawn point
  // do it before setting health back up, so farthest
  // ranging doesn't count this client
  if (client->sess.sessionTeam == TEAM_SPECTATOR)
    {
      spawnPoint = SelectSpectatorSpawnPoint (spawn_origin, spawn_angles);
    }
  else if (g_gametype->integer == GT_CTF)
    {
      // all base oriented team games use the CTF spawn points
      spawnPoint = SelectCTFSpawnPoint (client->sess.sessionTeam,
					client->pers.teamState.state,
					spawn_origin, spawn_angles,
					ent->sh.r.svFlags & SVF_BOT);

      // spawnpoint is non NULL, lift the player a bit above the ground
      spawn_origin[2] += RESPAWN_HEIGHT;
    }
  else
    {
      // the first spawn should be at a good looking spot
      if (!client->pers.initialSpawn && client->pers.localClient)
	{
	  client->pers.initialSpawn = true;
	  spawnPoint = SelectInitialSpawnPoint (spawn_origin, spawn_angles,
						ent->sh.r.svFlags & SVF_BOT);

	  // spawnpoint is non NULL, lift the player a bit above the ground
	  spawn_origin[2] += RESPAWN_HEIGHT;
	}
      else
	{
	  // don't spawn near existing origin if possible
	  spawnPoint = SelectSpawnPoint (client->ps.origin, spawn_origin,
					 spawn_angles, ent->sh.r.svFlags & SVF_BOT);

	  // spawnpoint is non NULL, lift the player a bit above the ground
	  spawn_origin[2] += RESPAWN_HEIGHT;
	}
    }

  client->pers.teamState.state = TEAM_ACTIVE;

  // toggle the teleport bit so the client knows to not lerp
  // and never clear the voted flag
  flags = ent->client->ps.eFlags & (EF_TELEPORT_BIT | EF_VOTED);
  flags ^= EF_TELEPORT_BIT;

  // clear everything but the persistant data (pers, sess)
  savedPing = client->ps.ping;
  accuracy_hits = client->cbt.accuracy_hits;
  accuracy_shots = client->cbt.accuracy_shots;
  for (i = 0; i < MAX_PERSISTANT; i++)
    {
      persistant[i] = client->ps.persistant[i];
    }

  Com_Memset (&(client->ps), 0, sizeof (client->ps));
  Com_Memset (&(client->cbt), 0, sizeof (client->cbt));
  Com_Memset (&(client->timer), 0, sizeof (client->timer));
  Com_Memset (&(client->misc), 0, sizeof (client->misc));

  client->ps.ping = savedPing;
  client->cbt.accuracy_hits = accuracy_hits;
  client->cbt.accuracy_shots = accuracy_shots;
  client->cbt.lastkilled_client = -1;

  for (i = 0; i < MAX_PERSISTANT; i++)
    {
      client->ps.persistant[i] = persistant[i];
    }

  // increment the spawncount so the client will detect the respawn
  client->ps.persistant[PERS_SPAWN_COUNT]++;
  client->ps.persistant[PERS_TEAM] = client->sess.sessionTeam;

  client->misc.airOutTime = level.time + 12000;

  gi->GetUserinfo (index, userinfo, sizeof (userinfo));
  // set max health; i variable isn't used here
  i = atoi (Info_ValueForKey (userinfo, "ci_handicap"));
  if ((i < 1) || (i > 100))
    {
      i = 100;
    }
  // multiplying the handicap value with the g_spawnLife one
  client->pers.maxHealth = (g_spawnLife->integer * i) / 100;

  // clear entity values
  client->ps.stats[STAT_MAX_HEALTH] = client->pers.maxHealth;
  client->ps.eFlags = flags;

  ent->sh.s.groundEntityNum = ENTITYNUM_NONE;
  ent->client = &level.clients[index];
  ent->takedamage = true;
  ent->inuse = true;
  ent->classname = "player";
  ent->sh.r.contents = CONTENTS_BODY;
  ent->clipmask = MASK_PLAYERSOLID;
  ent->die = player_die;
  ent->waterlevel = 0;
  ent->watertype = 0;
  ent->flags = 0;

  VectorCopy (playerMins, ent->sh.r.mins);
  VectorCopy (playerMaxs, ent->sh.r.maxs);

  client->ps.clientNum = index;

  client->ps.stats[STAT_WEAPONS] = (1 << WP_MACHINEGUN);
  if (g_gametype->integer == GT_TEAM)
    {
      client->ps.ammo[WP_MACHINEGUN] = 50;
    }
  else
    {
      client->ps.ammo[WP_MACHINEGUN] = 100;
    }

  client->ps.stats[STAT_WEAPONS] |= (1 << WP_GAUNTLET);
  client->ps.ammo[WP_GAUNTLET] = -1;

  // we are in warmup, give all available weapons
  if ((level.matchStatus & MS_WARMUP) && !(level.matchStatus & MS_LIVE))
    {
      weapon_t weap;

      for (weap = WP_SHOTGUN; weap < WP_NUM_WEAPONS; weap++)
	{
	  if (IsItemRegistered (BG_FindItemForWeapon (weap)))
	    {
	      client->ps.stats[STAT_WEAPONS] |= (1 << weap);
	      client->ps.ammo[weap] = maxammo_table[weap];
	    }
	}
    }

  // health will count down towards max_health 	// default gives +25 heath
  ent->health = client->ps.stats[STAT_HEALTH] = client->ps.stats[STAT_MAX_HEALTH];
  client->ps.stats[STAT_ARMOR] = g_spawnArmor->integer;

  // add some more life during warmup
  if ((level.matchStatus & MS_WARMUP) && !(level.matchStatus & MS_LIVE))
    {
      client->ps.stats[STAT_ARMOR] += 100;
    }

  G_SetOrigin (ent, spawn_origin);
  VectorCopy (spawn_origin, client->ps.origin);

  // the respawned flag will be cleared after the attack and jump keys come up
  client->ps.pm_flags |= PMF_RESPAWNED;

  gi->GetUsercmd (client - level.clients, &ent->client->pers.cmd);
  SetClientViewAngle (ent, spawn_angles);

  if (ent->client->sess.sessionTeam == TEAM_SPECTATOR)
    {
    }
  else
    {
      G_KillBox (ent);
      gi->LinkEntity (&ent->sh);

      // force the base weapon up
      client->ps.weapon = WP_MACHINEGUN;
      client->ps.weaponstate = WEAPON_READY;
    }

  // don't allow full run speed for a bit
  client->ps.pm_flags |= PMF_TIME_KNOCKBACK;
  client->ps.pm_time = RESPAWN_KNOCKBACK_TIME;	// respawn knockbacktime

  client->timer.respawnTime = level.time;
  client->timer.inactivityTime = level.time + g_inactivity->integer * 1000;
  client->misc.latched_buttons = 0;

  // set default animations
  client->ps.torsoAnim = TORSO_STAND;
  client->ps.legsAnim = LEGS_IDLE;

  if (level.match.end_flag >= END_INTERMISSION)
    {
      //G_MoveClientToIntermission (&level, ent);
    }
  else
    {
      // fire the targets of the spawn point
      G_UseTargets (spawnPoint, ent);

      // select the highest weapon number available, after any
      // spawn given items have fired
      // we don't want that in warmup, start with machinegun
      if ((level.matchStatus & MS_WARMUP) && !(level.matchStatus & MS_LIVE))
	{

	}
      else
	{
	  client->ps.weapon = 1;
	  for (i = WP_NUM_WEAPONS - 1; i > 0; i--)
	    {
	      if (client->ps.stats[STAT_WEAPONS] & (1 << i))
		{
		  client->ps.weapon = i;
		  break;
		}
	    }
	}
    }

  // run a client frame to drop exactly to the floor,
  // initialize animations and other things
  // this should trick pmove to get a faster start
  client->ps.commandTime = game.time - 250; // gives some boost at strart
  ent->client->pers.cmd.serverTime = game.time; // ?
  ClientThink (ent - g_entities);

  /*
  // positively link the client, even if the command times are weird
  // this should be to avoid extrapolated packets after repsawn ?
  // does it really change something ? ClientThink runs PlayerStateToEntityState
  if (ent->client->sess.sessionTeam != TEAM_SPECTATOR)
    {
      BG_PlayerStateToEntityState (&client->ps, &ent->sh.s, true);
      VectorCopy (ent->client->ps.origin, ent->sh.r.currentOrigin);
      gi->LinkEntity (&ent->sh);
    }
  */

  // run the presend to set anything else
  ClientEndFrame (ent);

  // clear entity state values
  BG_PlayerStateToEntityState (&client->ps, &ent->sh.s, true);
}

/*
===========
ClientDisconnect

Called when a player drops from the server.
Will not be called between levels.

This should NOT be called directly by any game logic,
call trap_DropClient(), which will call this and do
server system housekeeping.
============
*/
void
ClientDisconnect (int clientNum)
{
  gentity_t *ent;
  gentity_t *tent;
  int i;

  // cleanup if we are kicking a bot that
  // hasn't spawned yet
  if (gi->Cvar_VariableValue ("bot_enable"))
    {
      G_RemoveQueuedBotBegin (clientNum);
    }

  ent = g_entities + clientNum;
  if (!ent->client || (ent->client->pers.connected == CON_DISCONNECTED))
    {
      return;
    }

  // stop any following clients
  for (i = 0; i < level.maxclients; i++)
    {
      if (level.clients[i].sess.sessionTeam == TEAM_SPECTATOR
	  && level.clients[i].sess.spectatorState == SPECTATOR_FOLLOW
	  && level.clients[i].sess.spectatorClient == clientNum)
	{
	  StopFollowing (&g_entities[i]);
	}
    }

  // send effect if they were completely connected
  if (ent->client->pers.connected == CON_CONNECTED
      && ent->client->sess.sessionTeam != TEAM_SPECTATOR)
    {
      tent = G_TempEntity (ent->client->ps.origin, EV_PLAYER_TELEPORT_OUT);
      tent->sh.s.clientNum = ent->sh.s.clientNum;

      // They don't get to take powerups with them!
      // Especially important for stuff like CTF flags
      TossClientItems (ent);
    }

  G_LogPrintf ("ClientDisconnect: %i\n", clientNum);

  gi->UnlinkEntity (&ent->sh);
  ent->sh.s.modelindex = 0;
  ent->inuse = false;
  ent->classname = "disconnected";
  ent->client->pers.connected = CON_DISCONNECTED;
  ent->client->ps.persistant[PERS_TEAM] = TEAM_FREE;
  ent->client->sess.sessionTeam = TEAM_FREE;

  gi->SetConfigstring (CS_PLAYERS + clientNum, "");

  CalculateRanks ();

  ClientSaveHighestDel (clientNum);

  if (ent->sh.r.svFlags & SVF_BOT)
    {
      BotAIShutdownClient (clientNum, false);
    }
}

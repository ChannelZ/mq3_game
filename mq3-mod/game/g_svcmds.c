/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

// this file holds commands that can be executed by the server console, but not remote clients

#include "g_local.h"

/*
===================
Svcmd_EntityList_f
===================
*/
void
Svcmd_EntityList_f (void)
{
  int e;
  gentity_t * check;

  check = g_entities;
  for (e = 0; e < level.num_entities; e++, check++)
    {
      if (!check->inuse)
	{
	  continue;
	}

      G_Printf ("%3i:", e);
      switch (check->sh.s.eType)
	{
	case ET_GENERAL:
	  G_Printf ("ET_GENERAL          ");
	  break;
	case ET_PLAYER:
	  G_Printf ("ET_PLAYER           ");
	  break;
	case ET_ITEM:
	  G_Printf ("ET_ITEM             ");
	  break;
	case ET_MISSILE:
	  G_Printf ("ET_MISSILE          ");
	  break;
	case ET_MOVER:
	  G_Printf ("ET_MOVER            ");
	  break;
	case ET_BEAM:
	  G_Printf ("ET_BEAM             ");
	  break;
	case ET_PORTAL:
	  G_Printf ("ET_PORTAL           ");
	  break;
	case ET_SPEAKER:
	  G_Printf ("ET_SPEAKER          ");
	  break;
	case ET_PUSH_TRIGGER:
	  G_Printf ("ET_PUSH_TRIGGER     ");
	  break;
	case ET_TELEPORT_TRIGGER:
	  G_Printf ("ET_TELEPORT_TRIGGER ");
	  break;
	case ET_INVISIBLE:
	  G_Printf ("ET_INVISIBLE        ");
	  break;
	default:
	  G_Printf ("%3i                 ", check->sh.s.eType);
	  break;
	}

      if (check->classname)
	{
	  G_Printf ("%s", check->classname);
	}
      G_Printf("\n");
    }
}

gclient_t *
ClientForString (const char *s)
{
  gclient_t * cl;
  int i;
  int idnum;

  // numeric values are just slot numbers
  if (s[0] >= '0' && s[0] <= '9')
    {
      idnum = atoi(s);
      if (idnum < 0 || idnum >= level.maxclients)
	{
	  G_Printf ("Bad client slot: %i\n", idnum);
	  return NULL;
	}

      cl = &level.clients[idnum];
      if (cl->pers.connected == CON_DISCONNECTED)
	{
	  G_Printf ("Client %i is not connected\n", idnum);
	  return NULL;
	}
      return cl;
    }

  // check for a name match
  for (i = 0; i <= level.clientHighestID; i++)
    {
      cl = &level.clients[i];
      if (cl->pers.connected == CON_DISCONNECTED)
	{
	  continue;
	}
      if (!Q_stricmp (cl->pers.netname, s))
	{
	  return cl;
	}
    }

  G_Printf ("User %s is not on the server\n", s);

  return NULL;
}

/*
===================
Svcmd_ForceTeam_f

forceteam <player> <team>
===================
*/
void
Svcmd_ForceTeam_f (void)
{
  gclient_t * cl;
  const char *str;

  // find the player
  str = gi->Argv (1);
  cl = ClientForString (str);
  if (cl == NULL)
    {
      return;
    }

  // set the team
  str = gi->Argv (2);
  SetTeam (&g_entities[cl - level.clients], str);
}

/*
=================
ConsoleCommand

=================
*/
bool
ConsoleCommand (void)
{
  char cmd[MAX_TOKEN_CHARS];

  gi->ArgvBuffer (0, cmd, sizeof(cmd));

  if (Q_stricmp (cmd, "addbot") == 0)
    {
      Svcmd_AddBot_f();
      return true;
    }

  if (Q_stricmp (cmd, "list_bot") == 0)
    {
      Svcmd_BotList_f();
      return true;
    }

  if (Q_stricmp (cmd, "entitylist") == 0)
    {
      Svcmd_EntityList_f ();
      return true;
    }

  if (Q_stricmp (cmd, "forceteam") == 0)
    {
      Svcmd_ForceTeam_f();
      return true;
    }

  if (Q_stricmp (cmd, "game_memory") == 0)
    {
      Svcmd_GameMem_f();
      return true;
    }

  if (Q_stricmp (cmd, "ai_debug") == 0)
    {
      BotAIDebug ();
      return true;
    }

  // everything else (unknown commands) will be printed as a say command
  // FIXME : adding the ending "\n" shouldn't be needed here ?
  gi->SendServerCommand (-1, va("print \"server: %s\n\"", gi->ArgsFrom (0)));
  return true;
}

/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

// cvars and local cmds (admin console)

cvar_t * g_gametype;
cvar_t * g_dmflags;
cvar_t * g_plmflags;
cvar_t * g_fraglimit;
cvar_t * g_timelimit;
cvar_t * g_capturelimit;
cvar_t * g_roundlimit;
cvar_t * g_friendlyFire;
cvar_t * g_password;
cvar_t * g_needpass;
cvar_t * g_maxclients;
cvar_t * g_maxGameClients;
cvar_t * g_speed;
cvar_t * g_gravity;
cvar_t * g_knockback;
cvar_t * g_quadfactor;
cvar_t * g_forcerespawn;
cvar_t * g_inactivity;
cvar_t * g_debugMove;
cvar_t * g_debugDamage;
cvar_t * g_debugAlloc;
cvar_t * g_weaponRespawn;
cvar_t * g_weaponTeamRespawn;
cvar_t * g_motd;
cvar_t * g_synchronousClients;
cvar_t * g_warmuptime;
cvar_t * g_doWarmup;
cvar_t * g_tournamentStyle;
cvar_t * g_restarted;
cvar_t * g_logfile;
cvar_t * g_logfileSync;
cvar_t * g_blood;
cvar_t * g_allowVote;
cvar_t * g_allowSpectatorChat;
cvar_t * g_teamAutoJoin;
cvar_t * g_teamForceBalance;
cvar_t * g_smoothClients;

// gameplay tunning

cvar_t * g_overtimetype;
cvar_t * g_overtime;

cvar_t * g_spawnLife;
cvar_t * g_spawnArmor;
cvar_t * g_item_powerup_respawn;

cvar_t * g_armorSystem;
cvar_t * g_itemSystem;

cvar_t * g_gauntlet_damage;
cvar_t * g_machinegun_damage;
cvar_t * g_shotgun_damage;
cvar_t * g_grenade_damage;
cvar_t * g_rocket_damage;
cvar_t * g_lightning_damage;
cvar_t * g_railgun_damage;
cvar_t * g_plasma_damage;
cvar_t * g_bfg_damage;
cvar_t * g_grenadesplash_damage;
cvar_t * g_rocketsplash_damage;
cvar_t * g_plasmasplash_damage;
cvar_t * g_grenade_speed;
cvar_t * g_rocket_speed;
cvar_t * g_plasma_speed;

cvar_t * g_rapeMode;

/*
===============
G_CvarTrackInfo

Let clients know when the server changed some important cvars
===============
*/
void
G_CvarTrackInfo (cvar_t* var)
{
  gi->SendServerCommand (-1, va ("print \"Server: %s changed"
				 " to %s\n\"", var->name, var->string));
}

/*
===============
G_CvarTrackInfoLatched

Let clients know when the server is going to chang some important cvars
on the next game restart (map change, map_restart)
===============
*/
void
G_CvarTrackInfoLatched (cvar_t* var)
{
  gi->SendServerCommand (-1, va ("print \"Server: %s will be changed upon restarting"
				 " to %s\n\"", var->name, var->latchedString));
}

/*
===============
G_CvarTrackRapeMode

Let clients know when the server changed some important cvars
===============
*/
void
G_CvarTrackRapeMode (cvar_t* var)
{
  gi->SendServerCommand (-1, va ("print \"Server: %s changed"
				 " to %s\n\"", var->name, var->string));

  if (g_rapeMode->integer)
    {
      // promode settings
      // no armor decay; no footsteps
      gCvar_Set (g_dmflags, "36");
      // every special moves (double/ramp jump, airmove, instaswitch)
      gCvar_Set (g_plmflags, "15");

      gCvar_Set (g_spawnArmor, "0");
      gCvar_Set (g_armorSystem, "1");
      gCvar_Set (g_itemSystem, "1");
      gCvar_Set (g_shotgun_damage, XSTRING (SHOTGUN_DAMAGE_PRO));
      gCvar_Set (g_railgun_damage, XSTRING (RAILGUN_DAMAGE_PRO));
      gCvar_Set (g_lightning_damage, XSTRING (LIGHTNING_DAMAGE_PRO));
      gCvar_Set (g_rocket_speed, XSTRING (ROCKET_SPEED_PRO));
    }
  else
    {
      // vq3
      gclient_t* gclient;
      int i;

      gCvar_Set (g_dmflags, "0");
      gCvar_Set (g_plmflags, "0");

      gCvar_Set (g_spawnArmor, "25");
      gCvar_Set (g_armorSystem, "0");
      gCvar_Set (g_itemSystem, "1");
      gCvar_Set (g_shotgun_damage, XSTRING (SHOTGUN_DAMAGE));
      gCvar_Set (g_railgun_damage, XSTRING (RAILGUN_DAMAGE));
      gCvar_Set (g_lightning_damage, XSTRING (LIGHTNING_DAMAGE));
      gCvar_Set (g_rocket_speed, XSTRING (ROCKET_SPEED));

      // restore armor type to 0
      for (i = 0; i <= level.clientHighestID; i++)
	{
	  gclient = &level.clients[i];
	  if (gclient->pers.connected != CON_CONNECTED)
	    {
	      continue;
	    }

	  gclient->ps.stats[STAT_ARMOR_TYPE] = 0;
	}
    }
}

/*
================
G_InitCvars
================
*/
void
G_InitCvars (void)
{
  // server needs to know the maximum number of clients, as well as the game code
  // use "sv_maxclients" cvar name for this purpose
  g_maxclients = gCvar_Get ("sv_maxclients", "10",
			    (CVAR_SERVERINFO | CVAR_LATCH | CVAR_ARCHIVE));

  // we really don't want to keep track of these
  gCvar_Get ("gamename", GAMEVERSION, CVAR_SERVERINFO | CVAR_ROM);
  gCvar_Get ("gamedate", __DATE__, CVAR_ROM);

  // regular cvars
  // if you plan to keep track of these cvars,
  // be sure to stop tracking as the game module closes
  // (when the server goes to sleep) so you won't have a invalid pointer
  g_gametype = gCvar_Get ("g_gametype", "0", (CVAR_SERVERINFO | CVAR_LATCH));
  gCvar_Range (g_gametype, 0, GT_MAX_GAME_TYPE - 1, true);
  // need another way to do this if the cvar is latched
  gCvar_Track (g_gametype, G_CvarTrackInfoLatched);

  g_dmflags = gCvar_Get ("g_dmflags", "0", (CVAR_SERVERINFO | CVAR_ARCHIVE)); // track me
  gCvar_Track (g_dmflags, G_CvarTrackInfo);

  g_plmflags = gCvar_Get ("g_plmflags", "0", (CVAR_SERVERINFO | CVAR_ARCHIVE));
  gCvar_Track (g_plmflags, G_CvarTrackInfo);

  g_fraglimit = gCvar_Get ("g_fraglimit", "20", (CVAR_SERVERINFO | CVAR_ARCHIVE));
  gCvar_Track (g_fraglimit, G_CvarTrackInfo);

  g_timelimit = gCvar_Get ("g_timelimit", "0", (CVAR_SERVERINFO | CVAR_ARCHIVE));
  gCvar_Track (g_timelimit, G_CvarTrackInfo);

  g_capturelimit = gCvar_Get ("g_capturelimit", "8", (CVAR_SERVERINFO | CVAR_ARCHIVE));
  gCvar_Track (g_capturelimit, G_CvarTrackInfo);

  g_roundlimit = gCvar_Get ("g_roundlimit", "15", (CVAR_SERVERINFO | CVAR_ARCHIVE));
  gCvar_Track (g_roundlimit, G_CvarTrackInfo);

  g_friendlyFire = gCvar_Get ("g_friendlyFire", "0", CVAR_ARCHIVE);
  gCvar_Track (g_friendlyFire, G_CvarTrackInfo);

  g_maxGameClients = gCvar_Get ("g_maxGameClients", "0",
				(CVAR_SERVERINFO | CVAR_LATCH | CVAR_ARCHIVE));

  g_speed = gCvar_Get ("g_speed", "320", 0);
  gCvar_Track (g_speed, G_CvarTrackInfo);

  g_gravity = gCvar_Get ("g_gravity", "800", 0);
  gCvar_Track (g_gravity, G_CvarTrackInfo);

  g_knockback = gCvar_Get ("g_knockback", "5", 0);
  gCvar_Track (g_knockback, G_CvarTrackInfo);

  g_quadfactor = gCvar_Get ("g_quadfactor", "3", 0);
  gCvar_Track (g_quadfactor, G_CvarTrackInfo);

  g_forcerespawn = gCvar_Get ("g_forcerespawn", "15", 0);
  gCvar_Track (g_forcerespawn, G_CvarTrackInfo);

  // 5 mins
  g_inactivity = gCvar_Get ("g_inactivity", "320", 0);
  gCvar_Track (g_inactivity, G_CvarTrackInfo);

  g_weaponRespawn = gCvar_Get ("g_weaponRespawn", "5", 0);
  gCvar_Track (g_weaponRespawn, G_CvarTrackInfo);

  g_weaponTeamRespawn = gCvar_Get ("g_weaponTeamRespawn", "30", 0);
  gCvar_Track (g_weaponTeamRespawn, G_CvarTrackInfo);

  g_warmuptime = gCvar_Get ("g_warmuptime", "20", CVAR_ARCHIVE);
  gCvar_Track (g_warmuptime, G_CvarTrackInfo);

  g_doWarmup = gCvar_Get ("g_doWarmup", "0", CVAR_ARCHIVE);
  gCvar_Track (g_doWarmup, G_CvarTrackInfo);

  g_tournamentStyle = gCvar_Get ("g_tournamentStyle", "0", CVAR_ARCHIVE);
  gCvar_Track (g_tournamentStyle, G_CvarTrackInfo);

  g_overtimetype = gCvar_Get ("g_overtimetype", "1", (CVAR_ARCHIVE | CVAR_SERVERINFO));
  gCvar_Track (g_overtimetype, G_CvarTrackInfo);

  g_overtime = gCvar_Get ("g_overtime", "120", (CVAR_ARCHIVE | CVAR_SERVERINFO));
  gCvar_Track (g_overtime, G_CvarTrackInfo);

  // When it comes to gameplay features/cvars,
  // be sure they are set explicitely in the config file otherwise default

  g_spawnLife = gCvar_Get ("g_spawnLife", "100", (CVAR_SERVERINFO | CVAR_TEMP));
  // setting an acceptable life range
  gCvar_Range (g_spawnLife, 1, 200, true);
  gCvar_Track (g_spawnLife, G_CvarTrackInfo);

  g_spawnArmor = gCvar_Get ("g_spawnArmor", "25", (CVAR_SERVERINFO | CVAR_TEMP));
  // setting an acceptable armor range
  gCvar_Range (g_spawnArmor, 0, 200, true);
  gCvar_Track (g_spawnArmor, G_CvarTrackInfo);

  g_item_powerup_respawn = gCvar_Get ("g_item_powerup_respawn", "120",
				      (CVAR_SERVERINFO | CVAR_TEMP));
  // setting an acceptable time range
  gCvar_Range (g_item_powerup_respawn, 30, 300, true);
  gCvar_Track (g_item_powerup_respawn, G_CvarTrackInfo);

  // gameplay tunning
  g_armorSystem = gCvar_Get ("g_armorSystem", "0", CVAR_TEMP);
  // setting an acceptable range (0 or 1)
  gCvar_Range (g_armorSystem, 0, 1, true);
  gCvar_Track (g_armorSystem, G_CvarTrackInfo);

  g_itemSystem = gCvar_Get ("g_itemSystem", "0", CVAR_ARCHIVE);
  // setting an acceptable range (0 or 1)
  gCvar_Range (g_itemSystem, 0, 1, true);
  gCvar_Track (g_itemSystem, G_CvarTrackInfo);

  g_gauntlet_damage = gCvar_Get ("g_gauntlet_damage", XSTRING (GAUNTLET_DAMAGE),
				 CVAR_TEMP);
  // setting an acceptable damage range (0 to 200)
  gCvar_Range (g_gauntlet_damage, 0, 200, true);
  gCvar_Track (g_gauntlet_damage, G_CvarTrackInfo);

  g_machinegun_damage = gCvar_Get ("g_machinegun_damage", XSTRING (MACHINEGUN_DAMAGE),
				   CVAR_TEMP);
  gCvar_Range (g_machinegun_damage, 0, 200, true);
  gCvar_Track (g_machinegun_damage, G_CvarTrackInfo);

  g_shotgun_damage = gCvar_Get ("g_shotgun_damage", XSTRING (SHOTGUN_DAMAGE),
				CVAR_TEMP);
  gCvar_Range (g_shotgun_damage, 0, 200, true);
  gCvar_Track (g_shotgun_damage, G_CvarTrackInfo);

  g_grenade_damage = gCvar_Get ("g_grenade_damage", XSTRING (GRENADE_DAMAGE),
				CVAR_TEMP);
  gCvar_Range (g_grenade_damage, 0, 200, true);
  gCvar_Track (g_grenade_damage, G_CvarTrackInfo);

  g_rocket_damage = gCvar_Get ("g_rocket_damage", XSTRING (ROCKET_DAMAGE),
			       CVAR_TEMP);
  gCvar_Range (g_rocket_damage, 0, 200, true);
  gCvar_Track (g_rocket_damage, G_CvarTrackInfo);

  g_lightning_damage = gCvar_Get ("g_lightning_damage", XSTRING (LIGHTNING_DAMAGE),
				  CVAR_TEMP);
  gCvar_Range (g_lightning_damage, 0, 200, true);
  gCvar_Track (g_lightning_damage, G_CvarTrackInfo);

  g_railgun_damage = gCvar_Get ("g_railgun_damage", XSTRING (RAILGUN_DAMAGE),
				CVAR_TEMP);
  gCvar_Range (g_railgun_damage, 0, 200, true);
  gCvar_Track (g_railgun_damage, G_CvarTrackInfo);

  g_plasma_damage = gCvar_Get ("g_plasma_damage", XSTRING (PLASMA_DAMAGE),
			       CVAR_TEMP);
  gCvar_Range (g_plasma_damage, 0, 200, true);
  gCvar_Track (g_plasma_damage, G_CvarTrackInfo);

  g_bfg_damage = gCvar_Get ("g_bfg_damage", XSTRING (BFG_DAMAGE),
			    CVAR_TEMP);
  gCvar_Range (g_bfg_damage, 0, 200, true);
  gCvar_Track (g_bfg_damage, G_CvarTrackInfo);

  g_grenadesplash_damage = gCvar_Get ("g_grenadesplash_damage",
				      XSTRING (GRENADE_SPLASH_DAMAGE), CVAR_TEMP);
  gCvar_Range (g_grenadesplash_damage, 0, 200, true);
  gCvar_Track (g_grenadesplash_damage, G_CvarTrackInfo);

  g_rocketsplash_damage = gCvar_Get ("g_rocketsplash_damage",
				     XSTRING (ROCKET_SPLASH_DAMAGE), CVAR_TEMP);
  gCvar_Range (g_rocketsplash_damage, 0, 200, true);
  gCvar_Track (g_rocketsplash_damage, G_CvarTrackInfo);

  g_plasmasplash_damage = gCvar_Get ("g_plasmasplash_damage",
				     XSTRING (PLASMA_SPLASH_DAMAGE), CVAR_TEMP);
  gCvar_Range (g_plasmasplash_damage, 0, 200, true);
  gCvar_Track (g_plasmasplash_damage, G_CvarTrackInfo);

  g_grenade_speed = gCvar_Get ("g_grenade_speed",
			       XSTRING (GRENADE_SPEED), CVAR_TEMP);
  gCvar_Range (g_grenade_speed, 0, 4000, true);
  gCvar_Track (g_grenade_speed, G_CvarTrackInfo);

  g_rocket_speed = gCvar_Get ("g_rocket_speed",
			      XSTRING (ROCKET_SPEED), CVAR_TEMP);
  gCvar_Range (g_rocket_speed, 0, 4000, true);
  gCvar_Track (g_rocket_speed, G_CvarTrackInfo);

  g_plasma_speed = gCvar_Get ("g_plasma_speed",
			      XSTRING (PLASMA_SPEED), CVAR_TEMP);
  gCvar_Range (g_plasma_speed, 0, 4000, true);
  gCvar_Track (g_plasma_speed, G_CvarTrackInfo);

  // rapemode
  g_rapeMode = gCvar_Get ("g_rapeMode", "0", CVAR_ARCHIVE);
  gCvar_Track (g_rapeMode, G_CvarTrackRapeMode);

  g_teamAutoJoin = gCvar_Get ("g_teamAutoJoin", "0", CVAR_ARCHIVE);
  g_teamForceBalance = gCvar_Get ("g_teamForceBalance", "0", CVAR_ARCHIVE);

  g_allowSpectatorChat = gCvar_Get ("g_allowSpectatorChat", "1", CVAR_ARCHIVE);
  gCvar_Track (g_allowSpectatorChat, G_CvarTrackInfo);

  g_password = gCvar_Get ("g_password", "", 0);
  g_needpass = gCvar_Get ("g_needpass", "0", (CVAR_SERVERINFO | CVAR_ROM));
  g_motd = gCvar_Get ("g_motd", "", 0);
  g_restarted = gCvar_Get ("g_restarted", "0", CVAR_ROM);
  g_logfile = gCvar_Get ("g_logfile", "games.log", CVAR_ARCHIVE);
  g_logfileSync = gCvar_Get ("g_logfileSync", "0", CVAR_ARCHIVE);
  g_blood = gCvar_Get ("g_blood", "1", 0);
  g_allowVote = gCvar_Get ("g_allowVote", "1", CVAR_ARCHIVE);
  g_synchronousClients = gCvar_Get ("g_synchronousClients", "0", CVAR_SYSTEMINFO);
  g_smoothClients = gCvar_Get ("g_smoothClients", "0", 0);

  g_debugMove = gCvar_Get ("g_debugMove", "0", 0);
  g_debugDamage = gCvar_Get ("g_debugDamage", "0", 0);
  g_debugAlloc = gCvar_Get ("g_debugAlloc", "0", 0);

  level.warmupModificationCount = g_warmuptime->modificationCount;
}

void
G_FreeCvars (void)
{
  // clear cvar tracking for the game module

  gCvar_StopTrack (g_gametype);
  gCvar_StopTrack (g_dmflags);
  gCvar_StopTrack (g_plmflags);
  gCvar_StopTrack (g_fraglimit);
  gCvar_StopTrack (g_timelimit);
  gCvar_StopTrack (g_capturelimit);
  gCvar_StopTrack (g_roundlimit);
  gCvar_StopTrack (g_friendlyFire);
  gCvar_StopTrack (g_speed);
  gCvar_StopTrack (g_gravity);
  gCvar_StopTrack (g_knockback);
  gCvar_StopTrack (g_quadfactor);
  gCvar_StopTrack (g_forcerespawn);
  gCvar_StopTrack (g_inactivity);
  gCvar_StopTrack (g_weaponRespawn);
  gCvar_StopTrack (g_weaponTeamRespawn);
  gCvar_StopTrack (g_warmuptime);
  gCvar_StopTrack (g_doWarmup);
  gCvar_StopTrack (g_tournamentStyle);
  gCvar_StopTrack (g_overtimetype);
  gCvar_StopTrack (g_overtime);
  gCvar_StopTrack (g_spawnLife);
  gCvar_StopTrack (g_spawnArmor);
  gCvar_StopTrack (g_item_powerup_respawn);
  gCvar_StopTrack (g_armorSystem);
  gCvar_StopTrack (g_itemSystem);
  gCvar_StopTrack (g_gauntlet_damage);
  gCvar_StopTrack (g_machinegun_damage);
  gCvar_StopTrack (g_shotgun_damage);
  gCvar_StopTrack (g_grenade_damage);
  gCvar_StopTrack (g_rocket_damage);
  gCvar_StopTrack (g_lightning_damage);
  gCvar_StopTrack (g_railgun_damage);
  gCvar_StopTrack (g_plasma_damage);
  gCvar_StopTrack (g_bfg_damage);
  gCvar_StopTrack (g_grenadesplash_damage);
  gCvar_StopTrack (g_rocketsplash_damage);
  gCvar_StopTrack (g_plasmasplash_damage);
  gCvar_StopTrack (g_grenade_speed);
  gCvar_StopTrack (g_rocket_speed);
  gCvar_StopTrack (g_plasma_speed);
  gCvar_StopTrack (g_rapeMode);
  gCvar_StopTrack (g_allowSpectatorChat);
}

void
G_AddCommands (void)
{
  // adding server cmds for admins console

  gi->AddCommand ("remove", Cmd_Remove_f);
  gi->AddCommand ("random", Cmd_Random_f);
}

void
G_FreeCommands (void)
{
  // removing console commands

  gi->RemoveCommand ("remove");
  gi->RemoveCommand ("random");
}

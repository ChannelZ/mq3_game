// Some portions Copyright (C) 1999-2000 Id Software, Inc.
// All other portions Copyright (C) 2002-2007 Ted Vessenes

/*****************************************************************************
 * ai_vars.c
 *
 * Functions to manage bot variables
 *****************************************************************************/

#include "ai_main.h"
/*#include "ai_vars.h"

#include "ai_client.h"
#include "ai_view.h"
#include "ai_weapon.h"
*/
// NOTE: These variables do not use cvars because cvars can be updated during
// gameplay.  These variables are fixed whenever the game is reloaded.
int gametype;
int game_style;
int maxclients;

// Bot states
bot_state_t * bot_states[MAX_CLIENTS];

// Timestamp of the last executed server frame in seconds and milliseconds
//
// NOTE: server_time_ms is just a copy of level.time.  It's only purpose is
// to standardize the time interface, at least as far as the AI code is concerned.
float server_time;
int server_time_ms;

// Timestamps for the current AI frame in seconds and milliseconds
//
// NOTE: These timestamps are only loosely related to other timestamps used
// in the code.  Avoid using these whenever possible.
float ai_time;
int ai_time_ms;

// Default bot skill
cvar_t * g_spSkill;

// Variables for internal information
cvar_t * bot_thinktime;
cvar_t * bot_memorydump;
cvar_t * bot_saveroutingcache;

// Variables generating extra information
cvar_t * bot_report;
cvar_t * bot_testsolid;
cvar_t * bot_testclusters;

// Variables modifying chat behavior
cvar_t * bot_fastchat;
cvar_t * bot_nochat;
cvar_t * bot_testrchat;

// Variables modifying movement behavior
cvar_t * bot_grapple;		// False if bots should never grapple
cvar_t * bot_rocketjump;	// False if bots should never rocket jump

// Variables modifying dodging
cvar_t * bot_dodge_rate;	// Percent of time to spend dodging when going somewhere and being attacked
cvar_t * bot_dodge_min;		// Minimum amount of time to continue dodging in one direction
cvar_t * bot_dodge_max;		// Maximum amount of time to continue dodging in one direction

// Variables modifying perception
cvar_t * bot_lag_min;		// The minimum amount of lag a bot can have relative to other targets

// Variables modifying item selection
cvar_t * bot_item_path_neighbor_weight;	// The weighting between starting and ending regions when computing path
// neighbors of an item cluster.  0.0 means only consider the start; 1.0
// means only consider the end.
cvar_t * bot_item_predict_time_min;	// The bot will predict for at least this many seconds in its final
// location when considering a set of item pickups
cvar_t * bot_item_change_penalty_time;	// Estimate item pickup will take this much extra time when selecting
// a different item from last frame.  (Changing movement direction
// requires extra acceleration and deceleration the travel time estimates
cvar_t * bot_item_change_penalty_factor;	// Only select a new cluster if it's this many times as valuable
// as the currently selected cluster
cvar_t * bot_item_autopickup_time;	// Always pickup any item this many seconds or closer

// Variables modifying awareness
cvar_t * bot_aware_duration;	// How many seconds the most aware bot remains aware of things
cvar_t * bot_aware_skill_factor;	// The least aware bot's awareness is this many times as good as the best
cvar_t * bot_aware_refresh_factor;	// Bot may be this many times further away from a target it's already
// aware of and still refresh its awareness.

// Variables modifying reaction time
cvar_t * bot_reaction_min;	// The fastest a bot will start reacting to a change
cvar_t * bot_reaction_max;	// The slowest a bot will start reacting to a change

// Variables modifying the focus of the bot's view
cvar_t * bot_view_focus_head_dist;	// Bot focuses on the heads of player targets closer than this distance
cvar_t * bot_view_focus_body_dist;	// Bot focuses on the bodies of player targets farther than this distance

// Variables modifying the ideal view state's behavior
// NOTE: Changes in bot_view_ideal_error_min/max don't seem to have much effect
cvar_t * bot_view_ideal_error_min;	// Minimum ideal view error value as a percentage of target's velocity
cvar_t * bot_view_ideal_error_max;	// Maximum ideal view error value as a percentage of target's velocity
cvar_t * bot_view_ideal_correct_factor;	// Multiplied by bot's reaction time to produce time to delay between ideal view corrections

// Variables modifying the actual view state's behavior
cvar_t * bot_view_actual_accel_min;	// Minimum actual view acceleration in degrees per second
cvar_t * bot_view_actual_accel_max;	// Maximum actual view acceleration in degrees per second
cvar_t * bot_view_actual_error_min;	// Minimum actual view error value as a percentage of velocity change
cvar_t * bot_view_actual_error_max;	// Maximum actual view error value as a percentage of velocity change
cvar_t * bot_view_actual_correct_factor;	// Multiplied by bot's reaction time to produce time to delay between actual view corrections

// Variables defining how the bot attacks
cvar_t * bot_attack_careless_reload;	// Bots are careless when firing weapons with reload times no greater than this value
cvar_t * bot_attack_careless_factor;	// Bots scale targets' bounding boxes by this percent when aiming carelessly
cvar_t * bot_attack_careful_factor_min;	// The best bots scale targets' bounding boxes by this percent when aiming carefully
cvar_t * bot_attack_careful_factor_max;	// The worst bots scale targets' bounding boxes by this percent when aiming carefully
cvar_t * bot_attack_continue_factor;	// Once a bot stops attacking, it continues firing for this many times their reaction time
cvar_t * bot_attack_lead_time_full;	// Bots will lead the full distance when the amount of time they need to lead is no more than this
cvar_t * bot_attack_lead_time_scale;	// The percentage of time beyond bot_attack_lead_time_full that the bot actually leads

#ifdef DEBUG_AI
// Variables generating debug output
cvar_t * bot_debug_path;	// Describe obstacle and path planning setup during start up
cvar_t * bot_debug_item;	// Describe item region setup during start up
cvar_t * bot_debug_predict_time;	// The amount of time ahead to test predicted player movement
#endif	/*  */

/*
  ==================
  BotAIVariableSetup
  ==================
*/
void
BotAIVariableSetup (void)
{
  bot_thinktime =  gi->Cvar_Get ("bot_thinktime", "100", CVAR_CHEAT);
  bot_memorydump = gi->Cvar_Get ("bot_memorydump", "0", CVAR_CHEAT);
  bot_saveroutingcache = gi->Cvar_Get ("bot_saveroutingcache", "0",
				       CVAR_CHEAT);
  bot_report = gi->Cvar_Get ("bot_report", "0", CVAR_CHEAT);
  bot_testsolid = gi->Cvar_Get ("bot_testsolid", "0", CVAR_CHEAT);
  bot_testclusters = gi->Cvar_Get ("bot_testclusters", "0",
				   CVAR_CHEAT);
}

/*
  ================
  LevelSetGametype

  Sets the gametype value and some
  other values which depend on it.
  ================
*/
void
LevelSetGametype (int type)
{

  // Only record changes
  if (type == gametype)
    return;

  // Set the type
  gametype = type;

  // Create a bitmask of game information
  switch (gametype)

    {
    default:
    case GT_FFA:
    case GT_TOURNAMENT:
      //case GT_SINGLE_PLAYER:
      game_style = 0x0000;
      break;
    case GT_TEAM:
      game_style = GS_TEAM;
      break;

#ifdef MISSIONPACK
    case GT_OBELISK:
      game_style = (GS_TEAM | GS_BASE | GS_DESTROY);
      break;
    case GT_HARVESTER:
      game_style = (GS_TEAM | GS_BASE | GS_CARRIER);
      break;
    case GT_1FCTF:

#endif	/*  */
    case GT_CTF:
      game_style = (GS_TEAM | GS_BASE | GS_CARRIER | GS_FLAG);
      break;
    }

  // Some weapons work differently in different game modes
  LevelWeaponUpdateGametype ();
}


/*
  ===================
  LevelSetupVariables
  ===================
*/
void
LevelSetupVariables (void)
{
  LevelSetGametype (gi->Cvar_VariableIntegerValue ("g_gametype"));
  maxclients = gi->Cvar_VariableIntegerValue ("sv_maxclients");
  g_spSkill = gi->Cvar_Get ("g_spSkill", "3", 0);
  bot_fastchat = gi->Cvar_Get ("bot_fastchat", "0", 0);
  bot_nochat = gi->Cvar_Get ("bot_nochat", "0", 0);
  bot_testrchat = gi->Cvar_Get ("bot_testrchat", "0", 0);
  bot_grapple = gi->Cvar_Get ("bot_grapple", "0", CVAR_CHEAT);
  bot_rocketjump = gi->Cvar_Get ("bot_rocketjump", "1", CVAR_CHEAT);
  bot_dodge_rate = gi->Cvar_Get ("bot_dodge_rate", "0.35",
				 CVAR_CHEAT);
  bot_dodge_min = gi->Cvar_Get ("bot_dodge_min", "0.60", CVAR_CHEAT);
  bot_dodge_max = gi->Cvar_Get ("bot_dodge_max", "1.00", CVAR_CHEAT);
  bot_lag_min = gi->Cvar_Get ("bot_lag_min", "0.050", CVAR_CHEAT);
  bot_item_path_neighbor_weight = gi->Cvar_Get ("bot_item_path_neighbor_weight",
						"0.35", CVAR_CHEAT);
  bot_item_predict_time_min = gi->Cvar_Get ("bot_item_predict_time_min",
					    "20.0", CVAR_CHEAT);
  bot_item_change_penalty_time = gi->Cvar_Get ("bot_item_change_penalty_time",
					       "1.0", CVAR_CHEAT);
  bot_item_change_penalty_factor = gi->Cvar_Get ("bot_item_change_penalty_factor",
						 "1.2", CVAR_CHEAT);
  bot_item_autopickup_time = gi->Cvar_Get ("bot_item_autopickup_time",
					   "1.0", CVAR_CHEAT);
  bot_aware_duration = gi->Cvar_Get ("bot_aware_duration", "5.0",
				     CVAR_CHEAT);
  bot_aware_skill_factor = gi->Cvar_Get ("bot_aware_skill_factor",
					 "0.5", CVAR_CHEAT);
  bot_aware_refresh_factor = gi->Cvar_Get ("bot_aware_refresh_factor",
					   "2.0", CVAR_CHEAT);

  // All of these constants are very touchy.  Modify at your own risk!
  // NOTE: Changing reaction time and acceleration min/max (how fast the
  // bot moves its virtual mouse) will have the biggest impact on accuracy.
  // Reaction time
  bot_reaction_min = gi->Cvar_Get ("bot_reaction_min", "0.120",
				   CVAR_CHEAT);
  bot_reaction_max = gi->Cvar_Get ("bot_reaction_max", "0.280",
				   CVAR_CHEAT);

  // View focus
  bot_view_focus_head_dist = gi->Cvar_Get ("bot_view_focus_head_dist",
					   "256.0", CVAR_CHEAT);
  bot_view_focus_body_dist = gi->Cvar_Get ("bot_view_focus_body_dist",
					   "512.0", CVAR_CHEAT);

  // Ideal view modification
  bot_view_ideal_error_min = gi->Cvar_Get ("bot_view_ideal_error_min",
					   "0.0", CVAR_CHEAT);
  bot_view_ideal_error_max = gi->Cvar_Get ("bot_view_ideal_error_max",
					   "0.5", CVAR_CHEAT);
  bot_view_ideal_correct_factor =gi->Cvar_Get ("bot_view_ideal_correct_factor",
					       "3.0", CVAR_CHEAT);

  // Actual view modification
  bot_view_actual_accel_min = gi->Cvar_Get ("bot_view_actual_accel_min",
					    "800.0", CVAR_CHEAT);
  bot_view_actual_accel_max = gi->Cvar_Get ("bot_view_actual_accel_max",
					    "1500.0", CVAR_CHEAT);
  bot_view_actual_error_min = gi->Cvar_Get ("bot_view_actual_error_min",
					    "0.00", CVAR_CHEAT);
  bot_view_actual_error_max = gi->Cvar_Get ("bot_view_actual_error_max",
					    "1.00", CVAR_CHEAT);
  bot_view_actual_correct_factor = gi->Cvar_Get ("bot_view_actual_correct_factor",
						 "1.0", CVAR_CHEAT);

  // Attack
  bot_attack_careless_reload = gi->Cvar_Get ("bot_attack_careless_reload",
					     "0.5", CVAR_CHEAT);
  bot_attack_careless_factor = gi->Cvar_Get ("bot_attack_careless_factor",
					     "5.0", CVAR_CHEAT);
  bot_attack_careful_factor_min = gi->Cvar_Get ("bot_attack_careful_factor_min",
						"1.0", CVAR_CHEAT);
  bot_attack_careful_factor_max = gi->Cvar_Get ("bot_attack_careful_factor_max",
						"2.0", CVAR_CHEAT);
  bot_attack_continue_factor = gi->Cvar_Get ("bot_attack_continue_factor",
					     "2.5", CVAR_CHEAT);
  bot_attack_lead_time_full = gi->Cvar_Get ("bot_attack_lead_time_full",
					    "0.50", CVAR_CHEAT);
  bot_attack_lead_time_scale = gi->Cvar_Get ("bot_attack_lead_time_scale",
					     "0.20", CVAR_CHEAT);

#ifdef DEBUG_AI
  // Turn these before startup to see precomputed data structure information
  bot_debug_path =  gi->Cvar_Get ("bot_debug_path", "0", CVAR_CHEAT);
  bot_debug_item =  gi->Cvar_Get ("bot_debug_item", "0", CVAR_CHEAT);
  bot_debug_predict_time =  gi->Cvar_Get ("bot_debug_predict_time",
					  "0.0", CVAR_CHEAT);
#endif	/*  */
}

/*
  ====================
  LevelUpdateVariables

  Updates any variables that may have
  changed since last frame
  ====================
*/
void
LevelUpdateVariables (void)
{
  /*
  // First reread a whole ton of variables
  trap_Cvar_Update (&bot_thinktime);
  trap_Cvar_Update (&bot_memorydump);
  trap_Cvar_Update (&bot_saveroutingcache);
  trap_Cvar_Update (&bot_fastchat);
  trap_Cvar_Update (&bot_nochat);
  trap_Cvar_Update (&bot_testrchat);
  trap_Cvar_Update (&bot_report);
  trap_Cvar_Update (&bot_grapple);
  trap_Cvar_Update (&bot_rocketjump);
  trap_Cvar_Update (&bot_dodge_rate);
  trap_Cvar_Update (&bot_dodge_min);
  trap_Cvar_Update (&bot_dodge_max);
  trap_Cvar_Update (&bot_lag_min);
  trap_Cvar_Update (&bot_item_path_neighbor_weight);
  trap_Cvar_Update (&bot_item_predict_time_min);
  trap_Cvar_Update (&bot_item_change_penalty_time);
  trap_Cvar_Update (&bot_item_change_penalty_factor);
  trap_Cvar_Update (&bot_item_autopickup_time);
  trap_Cvar_Update (&bot_aware_duration);
  trap_Cvar_Update (&bot_aware_skill_factor);
  trap_Cvar_Update (&bot_aware_refresh_factor);
  trap_Cvar_Update (&bot_reaction_min);
  trap_Cvar_Update (&bot_reaction_max);
  trap_Cvar_Update (&bot_view_focus_head_dist);
  trap_Cvar_Update (&bot_view_focus_body_dist);
  trap_Cvar_Update (&bot_view_ideal_error_min);
  trap_Cvar_Update (&bot_view_ideal_error_max);
  trap_Cvar_Update (&bot_view_ideal_correct_factor);
  trap_Cvar_Update (&bot_view_actual_accel_min);
  trap_Cvar_Update (&bot_view_actual_accel_max);
  trap_Cvar_Update (&bot_view_actual_error_min);
  trap_Cvar_Update (&bot_view_actual_error_max);
  trap_Cvar_Update (&bot_view_actual_correct_factor);
  trap_Cvar_Update (&bot_attack_careless_reload);
  trap_Cvar_Update (&bot_attack_careless_factor);
  trap_Cvar_Update (&bot_attack_careful_factor_min);
  trap_Cvar_Update (&bot_attack_careful_factor_max);
  trap_Cvar_Update (&bot_attack_continue_factor);
  trap_Cvar_Update (&bot_attack_lead_time_full);
  trap_Cvar_Update (&bot_attack_lead_time_scale);

#ifdef DEBUG_AI
  trap_Cvar_Update (&bot_debug_path);
  trap_Cvar_Update (&bot_debug_item);
  trap_Cvar_Update (&bot_debug_predict_time);
  #endif	*//*  */

  // Handle some internal AI variable sets
  if (bot_memorydump->integer)
    {
      gi->botlib->BotLibVarSet ("memorydump", "1");
      gi->Cvar_Set ("bot_memorydump", "0");
    }
  if (bot_saveroutingcache->integer)
    {
      gi->botlib->BotLibVarSet ("saveroutingcache", "1");
      gi->Cvar_Set ("bot_saveroutingcache", "0");
    }

  // Handle anything required by think time changes
  LevelUpdateThinkTime ();

  // Cache bot reaction times if the reaction time min or max changed
  LevelCacheReactionTimes ();
}

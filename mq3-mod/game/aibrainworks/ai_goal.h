// Some portions Copyright (C) 1999-2000 Id Software, Inc.
// All other portions Copyright (C) 2002-2007 Ted Vessenes

/*****************************************************************************
 * ai_goal.h
 *
 * Includes used for bot goal algorithms
 *****************************************************************************/
void GoalName (bot_goal_t * goal, char *name, size_t size);
char *GoalNameFast (bot_goal_t * goal);
gentity_t * GoalPlayer (bot_goal_t * goal);
void GoalReset (bot_goal_t * goal);
bool GoalLocationArea (bot_goal_t * goal, vec3_t origin, int area);
bool GoalLocation (bot_goal_t * goal, vec3_t origin);
bool GoalEntityArea (bot_goal_t * goal, gentity_t * ent, int area);
bool GoalEntity (bot_goal_t * goal, gentity_t * ent);
bool GoalFromName (bot_goal_t * goal, char *goalname, bot_state_t * bs);

// Some portions Copyright (C) 1999-2000 Id Software, Inc.
// All other portions Copyright (C) 2002-2007 Ted Vessenes

/*****************************************************************************
 * ai_vars.h
 *
 * Includes for accessing bot variables
 *****************************************************************************/
extern int gametype;
extern int game_style;
extern int maxclients;

// Different styles of gameplay
#define GS_TEAM		0x0001	// Game has teamplay
#define GS_BASE		0x0002	// Game has bases of some kind
#define GS_CARRIER	0x0004	// Game supports carriers of some kind
#define GS_FLAG		0x0008	// Game includes flags
#define GS_DESTROY	0x0010	// Game includes destructable objects
extern bot_state_t *bot_states[MAX_CLIENTS];
extern float server_time;
extern int server_time_ms;
extern float ai_time;
extern int ai_time_ms;
extern cvar_t * g_spSkill;
extern cvar_t * bot_thinktime;
extern cvar_t * bot_memorydump;
extern cvar_t * bot_saveroutingcache;
extern cvar_t * bot_report;
extern cvar_t * bot_testsolid;
extern cvar_t * bot_testclusters;
extern cvar_t * bot_fastchat;
extern cvar_t * bot_nochat;
extern cvar_t * bot_testrchat;
extern cvar_t * bot_grapple;
extern cvar_t * bot_rocketjump;
extern cvar_t * bot_dodge_rate;
extern cvar_t * bot_dodge_min;
extern cvar_t * bot_dodge_max;
extern cvar_t * bot_lag_min;
extern cvar_t * bot_item_path_neighbor_weight;
extern cvar_t * bot_item_predict_time_min;
extern cvar_t * bot_item_change_penalty_time;
extern cvar_t * bot_item_change_penalty_factor;
extern cvar_t * bot_item_autopickup_time;
extern cvar_t * bot_aware_duration;
extern cvar_t * bot_aware_skill_factor;
extern cvar_t * bot_aware_refresh_factor;
extern cvar_t * bot_reaction_min;
extern cvar_t * bot_reaction_max;
extern cvar_t * bot_view_focus_head_dist;
extern cvar_t * bot_view_focus_body_dist;
extern cvar_t * bot_view_ideal_error_min;
extern cvar_t * bot_view_ideal_error_max;
extern cvar_t * bot_view_ideal_correct_factor;
extern cvar_t * bot_view_actual_accel_min;
extern cvar_t * bot_view_actual_accel_max;
extern cvar_t * bot_view_actual_error_min;
extern cvar_t * bot_view_actual_error_max;
extern cvar_t * bot_view_actual_correct_factor;
extern cvar_t * bot_attack_careless_reload;
extern cvar_t * bot_attack_careless_factor;
extern cvar_t * bot_attack_careful_factor_min;
extern cvar_t * bot_attack_careful_factor_max;
extern cvar_t * bot_attack_continue_factor;
extern cvar_t * bot_attack_lead_time_full;
extern cvar_t * bot_attack_lead_time_scale;

#ifdef DEBUG_AI
extern cvar_t * bot_debug_path;
extern cvar_t * bot_debug_item;
extern cvar_t * bot_debug_predict_time;

#endif	/*  */
void BotAIVariableSetup (void);
void LevelSetupVariables (void);
void LevelUpdateVariables (void);

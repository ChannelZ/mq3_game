/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

/*
=============
G_LogFileInit
=============
*/
void
G_LogFileInit (lvl_t * level)
{
  if (g_logfile->string[0])
    {
      if (g_logfileSync->integer)
	{
	  gi->FS_FOpenFile (g_logfile->string, &level->logFile, FS_APPEND_SYNC);
	}
      else
	{
	  gi->FS_FOpenFile (g_logfile->string, &level->logFile, FS_APPEND);
	}

      if (!level->logFile)
	{
	  G_Printf ("WARNING: Couldn't open logfile: %s\n", g_logfile->string);
	}
      else
	{
	  char serverinfo[MAX_INFO_STRING];

	  gi->GetServerinfo (serverinfo, sizeof (serverinfo));

	  G_LogPrintf
	    ("------------------------------------------------------------\n");
	  G_LogPrintf ("InitGame: %s\n", serverinfo);
	}
    }
  else
    {
      G_Printf ("Not logging to disk.\n");
    }
}

/*
==============
G_LogFileClose
==============
*/
void
G_LogFileClose (lvl_t * level)
{
  if (level->logFile)
    {
      G_LogPrintf ("ShutdownGame:\n");
      G_LogPrintf
	("------------------------------------------------------------\n");
      gi->FS_FCloseFile (level->logFile);
    }
}

/*
=================
G_LogPrintf

Print to the logfile with a time stamp if it is open
=================
*/
void
G_LogPrintf (const char *fmt, ...)
{
  va_list argptr;
  char string[1024];
  int min, tens, sec;

  // also print the game time ?
  sec = level.time / 1000;
  min = sec / 60;
  sec -= min * 60;
  tens = sec / 10;
  sec -= tens * 10;

  Com_sprintf (string, sizeof (string), "%3i:%1i%1i ", min, tens, sec);

  va_start (argptr, fmt);
  Q_vsnprintf (string + 7, sizeof (string) - 7, fmt, argptr);
  va_end (argptr);

  G_Printf ("%s", string + 7);

  if (!level.logFile)
    {
      return;
    }

  gi->FS_Write (string, strlen (string), level.logFile);
}

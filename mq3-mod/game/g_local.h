/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

// g_local.h -- local definitions for game module

#include "qcommon/q_shared.h"
#include "qcommon/qcom_extern.h"

#include "botlib/botlib.h"

#include "bg_public.h"
// all available functions from server
#include "server/sv_game_API.h"
#include "g_types.h" // structures and typedefs
#include "g_team.h" // teamplay specific stuff

// botlibs are loaded in files that need it

#define G_Printf(s, args...) gi->Printf (s, ##args)
#define G_Error(s, args...) gi->Error (ERR_DROP, s, ##args)

// bunch of useful macros for cvar manipulation
// no other cvar function should be needed, others are very specific
// and hackish on the sides.
#define gCvar_Get(cvarname, value, flags) gi->Cvar_Get (cvarname, value, flags)
#define gCvar_Set(cvarptr, value) gi->Cvar_SetNew (cvarptr, value)
#define gCvar_Range(cvarptr, min, max, integertype)	\
  gi->Cvar_CheckRange (cvarptr, min, max, integertype)
#define gCvar_Track(cvarptr, func) gi->Cvar_TrackChange (cvarptr, func)
#define gCvar_StopTrack(cvarptr) gi->Cvar_TrackChange (cvarptr, NULL)


//==================================================================

/// the "gameversion" client command will print this plus compile date
#define GAMEVERSION BASEGAME

// ****************************************
// MOVE THIS IN THE APPROPRIATE FILE
/// don't spawn on the floor
#define RESPAWN_HEIGHT 9
/// time before we can respawn
#define RESPAWN_MINIMAL_DELAY 800 // 1700
#define RESPAWN_MINIMAL_DELAY_RAPE 300
#define RESPAWN_KNOCKBACK_TIME 50 // 100

/// msec
// this sould be removed a the "live" frametime taken.
#define FRAMETIME		100

#define CARNAGE_REWARD_TIME	3000
#define REWARD_SPRITE_TIME	2000

#define INTERMISSION_DELAY_TIME	1000
#define INTERMISSION_STATS_TIME	500
// ****************************************

// COMMENT ME
#define FOFS(x) ((size_t)&(((gentity_t* )0)->x))

// declared in g_main for gameinit ()
extern gamelib_import_t * gi;
//gamelib_export_t ge // this is local to g_main.c

extern level_locals_t level;
extern game_locals_t game;
extern teamgame_t teamgame;

extern gentity_t g_entities[MAX_GENTITIES];

extern cvar_t * g_gametype;
/// allow this many total, including spectators
extern cvar_t * g_maxclients;
/// allow this many active
extern cvar_t * g_maxGameClients;
extern cvar_t * g_restarted;

extern cvar_t * g_dmflags;
extern cvar_t * g_plmflags;
extern cvar_t * g_fraglimit;
extern cvar_t * g_timelimit;
extern cvar_t * g_capturelimit;
extern cvar_t * g_roundlimit;
extern cvar_t * g_friendlyFire;
extern cvar_t * g_password;
extern cvar_t * g_needpass;
extern cvar_t * g_gravity;
extern cvar_t * g_speed;
extern cvar_t * g_knockback;
extern cvar_t * g_quadfactor;
extern cvar_t * g_forcerespawn;
extern cvar_t * g_inactivity;
extern cvar_t * g_debugMove;
extern cvar_t * g_debugAlloc;
extern cvar_t * g_debugDamage;
extern cvar_t * g_weaponRespawn;
extern cvar_t * g_weaponTeamRespawn;
extern cvar_t * g_synchronousClients;
extern cvar_t * g_motd;
extern cvar_t * g_warmuptime;
extern cvar_t * g_doWarmup;
extern cvar_t * g_tournamentStyle;
extern cvar_t * g_blood;
extern cvar_t * g_allowVote;
extern cvar_t * g_allowSpectatorChat;
extern cvar_t * g_teamAutoJoin;
extern cvar_t * g_teamForceBalance;
extern cvar_t * g_smoothClients;
extern cvar_t * g_logfile;
extern cvar_t * g_logfileSync;
extern cvar_t * g_overtimetype;
extern cvar_t * g_overtime;

extern cvar_t * g_spawnLife;
extern cvar_t * g_spawnArmor;
extern cvar_t * g_item_powerup_respawn;

extern cvar_t * g_armorSystem;
extern cvar_t * g_itemSystem;

extern cvar_t * g_gauntlet_damage;
extern cvar_t * g_machinegun_damage;
extern cvar_t * g_shotgun_damage;
extern cvar_t * g_grenade_damage;
extern cvar_t * g_rocket_damage;
extern cvar_t * g_lightning_damage;
extern cvar_t * g_railgun_damage;
extern cvar_t * g_plasma_damage;
extern cvar_t * g_bfg_damage;
extern cvar_t * g_grenadesplash_damage;
extern cvar_t * g_rocketsplash_damage;
extern cvar_t * g_plasmasplash_damage;
extern cvar_t * g_grenade_speed;
extern cvar_t * g_rocket_speed;
extern cvar_t * g_plasma_speed;

extern cvar_t * g_rapeMode;


//============================================================================

//
// g_spawn.c
//

/// spawn string returns a temporary reference, you must CopyString() if you want to keep it
bool	G_SpawnString (const char* key, const char* defaultString, char** out);
bool	G_SpawnFloat (const char* key, const char* defaultString, float* out);
bool	G_SpawnInt (const char* key, const char* defaultString, int* out);
bool	G_SpawnVector (const char* key, const char* defaultString, float* out);
void		G_SpawnEntitiesFromString (void);
char*		G_NewString (const char* string);
void G_FindTeams (void);

//
// g_cmds.c
//
void	Cmd_Score_f (gentity_t* ent);
void	StopFollowing (gentity_t* ent);
void	BroadcastTeamChange (gclient_t* client, int oldTeam);
void	SetTeam (gentity_t* ent, const char* s);
void	Cmd_FollowCycle_f (gentity_t* ent, int dir);
void	G_ConsoleStats (lvl_t * level, gentity_t* ent, gentity_t* target, bool finalmsg);
void	G_CTFConsoleStats (lvl_t * level, gentity_t* ent, gentity_t* target);
void	G_TDMConsoleStats (lvl_t * level, gentity_t* ent, gentity_t* target);
void	Cmd_Remove_f (void);
void	Cmd_Random_f (void);

//
// g_items.c
//
void	G_CheckTeamItems (void);
void	G_RunItem (gentity_t* ent);
void	RespawnItem (gentity_t* ent);

void	UseHoldableItem (gentity_t* ent);
void	PrecacheItem (gitem_t* it);
gentity_t* Drop_Item (gentity_t* ent, const gitem_t* item, float angle);
gentity_t* LaunchItem (const gitem_t* item, vec3_t origin, vec3_t velocity,
		       int quantity, int clientNum);
gentity_t* Drop_Player_Item (gentity_t* ent, const gitem_t* item, float angle, bool throw);
void	SetRespawn (gentity_t* ent, float delay);
void	G_SpawnItem (gentity_t* ent, const gitem_t* item);
void	FinishSpawningItem (gentity_t* ent);
void	ItemDefaultConfiguration (gentity_t* ent);
void	Think_Weapon (gentity_t* ent);
int	ArmorIndex (gentity_t* ent);
void	Add_Ammo (gentity_t* ent, int weapon, int count);
void	Touch_Item (gentity_t* ent, gentity_t* other, trace_t* trace);

const gitem_t*  G_FindNearestItem (vec3_t origin, int flag);

void	ClearRegisteredItems (void);
void	RegisterItem (const gitem_t* item);
bool IsItemRegistered (const gitem_t* item);
void	SaveRegisteredItems (void);

//
// g_utils.c
//
int	G_ModelIndex (char* name);
int	G_SoundIndex (char* name);
void	G_TeamCommand (team_t team, char* cmd);
void	G_KillBox (gentity_t* ent);
gentity_t* G_Find (gentity_t* from, int fieldofs, const char* match);
gentity_t* G_PickTarget (char* targetname);
void	G_UseTargets (gentity_t* ent, gentity_t* activator);
void	G_SetMovedir (vec3_t angles, vec3_t movedir);

void	G_InitGentity (gentity_t* e);
gentity_t* G_Spawn (void);
gentity_t* G_TempEntity (vec3_t origin, entity_event_t event);
void	G_Sound (gentity_t* ent, int channel, int soundIndex);
void	G_FreeEntity (gentity_t* e);
bool G_EntitiesFree (void);

void	G_TouchTriggers (gentity_t* ent);
void	G_TouchSolids (gentity_t* ent);

float*	tv (float x, float y, float z);
char*	vtos (const vec3_t v);
float	vectoyaw (const vec3_t vec);

void	G_AddPredictableEvent (gentity_t* ent, int event, int eventParm);
void	G_AddEvent (gentity_t* ent, int event, int eventParm);
void	G_SetOrigin (gentity_t* ent, vec3_t origin);

//
// g_combat.c
//
bool	CanDamage (gentity_t* targ, vec3_t origin);
void		G_Damage (gentity_t* targ, gentity_t* inflictor,
			  gentity_t* attacker, vec3_t dir, vec3_t point,
			  int damage, int dflags, int mod);
bool	G_RadiusDamage (vec3_t origin, gentity_t* attacker,
				float damage, float radius,
				gentity_t* ignore, int mod);
int		G_InvulnerabilityEffect (gentity_t* targ, vec3_t dir,
					 vec3_t point, vec3_t impactpoint,
					 vec3_t bouncedir);
void		body_die (gentity_t* self, gentity_t* inflictor,
			  gentity_t* attacker, int damage, int meansOfDeath);
void		TossClientItems (gentity_t* self);
void		TossClientCubes (gentity_t* self);

// damage flags
/// damage was indirect
#define DAMAGE_RADIUS			0x00000001
/// armour does not protect from this damage
#define DAMAGE_NO_ARMOR			0x00000002
/// do not affect velocity, just view angles
#define DAMAGE_NO_KNOCKBACK		0x00000004
/// armor, shields, invulnerability, and godmode have no effect
#define DAMAGE_NO_PROTECTION		0x00000008

//
// g_missile.c
//
void G_RunMissile (gentity_t* ent);

gentity_t* fire_plasma (gentity_t* self, vec3_t start, vec3_t aimdir);
gentity_t* fire_grenade (gentity_t* self, vec3_t start, vec3_t aimdir);
gentity_t* fire_rocket (gentity_t* self, vec3_t start, vec3_t dir);
gentity_t* fire_bfg (gentity_t* self, vec3_t start, vec3_t dir);

//
// g_mover.c
//
void	G_RunMover (gentity_t* ent);
void	Touch_DoorTrigger (gentity_t* ent, gentity_t* other, trace_t* trace);

//
// g_trigger.c
//
void	trigger_teleporter_touch (gentity_t* self, gentity_t* other,
				  trace_t* trace);


//
// g_misc.c
//
void	TeleportPlayer (gentity_t* player, vec3_t origin, vec3_t angles);

//
// g_weapon.c
//
bool LogAccuracyHit (gentity_t* target, gentity_t* attacker);
void	CalcMuzzlePoint (gentity_t* ent, vec3_t forward, vec3_t right,
			 vec3_t up, vec3_t muzzlePoint);
void	SnapVectorTowards (vec3_t v, vec3_t to);
bool CheckGauntletAttack (gentity_t* ent);


//
// g_client.c
//
team_t	TeamCount (int ignoreClientNum, team_t team);
int	TeamLeader (team_t team);
team_t	PickTeam (int ignoreClientNum);
void	SetClientViewAngle (gentity_t* ent, vec3_t angle);
gentity_t* SelectSpawnPoint (vec3_t avoidPoint, vec3_t origin,
			     vec3_t angles, bool isBot);
void	CopyToBodyQue (gentity_t* ent);
void	respawn (gentity_t* ent);
void	InitClientPersistant (gclient_t* client);
void	InitClientResp (gclient_t* client);
void	InitBodyQue (void);
void	ClientSpawn (gentity_t* ent);
void	player_die (gentity_t* self, gentity_t* inflictor, gentity_t* attacker,
		    int damage, int mod);
void	AddScore (gentity_t* ent, vec3_t origin, int score);
void	CalculateRanks (void);
bool SpotWouldTelefrag (gentity_t* spot);

//
// g_svcmds.c
//
bool ConsoleCommand (void);

//
// g_weapon.c
//
void	FireWeapon (gentity_t* ent);

//
// p_hud.c
//
void	G_SetStats (gentity_t* ent);

//
// g_cmds.c
//
void SendScoreboardMessageToAllClients (lvl_t * level);

void DeathmatchScoreboardMessage (lvl_t * level, gentity_t * client);

//
// g_pweapon.c
//


//
// g_main.c
//
//void	SetLeader (team_t team, int client);
//void	CheckTeamLeader (team_t team);
void	G_RunThink (gentity_t* ent);

// g_log.c
void G_LogFileInit (lvl_t * level);
void G_LogFileClose (lvl_t * level);
void G_LogPrintf (const char * fmt, ...);

// g_match.c
void G_Match_Start (lvl_t * level, const char * msg);
void G_Match_End (lvl_t * level, const char * msg);
void G_Match_Frame (game_locals_t * game, lvl_t * level);
void SendConsoleStatsToAllClients (lvl_t * level);
void SendConsoleMatchSummaryToAllClients (lvl_t * level);
void G_BeginIntermission (void);
//void G_CheckTournament (void);
//gentity_t* G_FindIntermissionPoint (vec3_t origin, vec3_t angles);
//void G_MoveClientToIntermission (lvl_t * level, gentity_t* client);

//
// g_match_intermission.c
//
void G_Intermission_MoveClient (lvl_t * level, gentity_t * ent);
gentity_t * G_Intermission_FindPoint (vec3_t origin, vec3_t angle);
void G_Intermission_CheckExit (game_locals_t * game, lvl_t * level);


//
// g_client.c
//
char*	ClientConnect (int clientNum, bool firstTime, bool isBot);
void	ClientUserinfoChanged (int clientNum);
void	ClientDisconnect (int clientNum);
void	ClientBegin (int clientNum);
void	ClientCommand (int clientNum);
// player is "live" in the arena
bool G_Client_IsPlaying (gclient_t * cl);
// spectators are also ingame clients but no playing
bool G_Client_IsIngame (gclient_t * cl);
bool G_Client_ValidEntity (gentity_t * client);

//
// g_active.c
//
void	ClientThink (int clientNum);
void	ClientEndFrame (gentity_t* ent);
void	G_RunClient (gentity_t* ent);

//
// g_team.c
//

//
// g_mem.c
//
void*	G_Alloc (int size);
void	G_InitMemory (void);
void	Svcmd_GameMem_f (void);

//
// g_session.c
//
void	G_ReadSessionData (gclient_t* client);
void	G_InitSessionData (gclient_t* client, char* userinfo);

void	G_InitWorldSession (void);
void	G_WriteSessionData (void);

//
// g_bot.c
//
void G_InitBots (bool restart);
char* G_GetBotInfoByNumber (int num);
char* G_GetBotInfoByName (const char* name);
void G_CheckBotSpawn (void);
void G_RemoveQueuedBotBegin (int clientNum);
bool G_BotConnect (int clientNum, bool restart);
void Svcmd_AddBot_f (void);
void Svcmd_BotList_f (void);
void BotInterbreedEndMatch (void);

//
// g_cmdcvar.c
//
void G_InitCvars (void);
void G_FreeCvars (void);
void G_AddCommands (void);
void G_FreeCommands (void);

// ai_main.c
#define MAX_FILEPATH 144

//bot settings
typedef struct bot_settings_s
{
  char characterfile[MAX_FILEPATH];
  float skill;
  char team[MAX_FILEPATH];
} bot_settings_t;

bool BotAISetup (int restart);
bool BotAIShutdown (int restart);
bool BotAILoadMap (int restart);
int BotAISetupClient (int client, struct bot_settings_s* settings, bool restart);
int BotAIShutdownClient (int client, bool restart);
int BotAIStartFrame (int time);
void BotAIDebug (void);
void BotTestAAS (vec3_t origin);

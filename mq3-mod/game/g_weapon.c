/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

// g_weapon.c
// perform the server side effects of a weapon firing

#include "g_local.h"

static float s_quadFactor;
// this is set for the current entity we're working on
static vec3_t forward, right, up;
static vec3_t muzzle;

/*
======================================================================

GAUNTLET

======================================================================
*/
void
Weapon_Gauntlet (gentity_t * ent)
{

}

/*
===============
CheckGauntletAttack
===============
*/
bool
CheckGauntletAttack (gentity_t * ent)
{
  trace_t tr;
  vec3_t end;
  gentity_t *tent;
  gentity_t *traceEnt;
  int damage;

  // set aiming directions
  AngleVectors (ent->client->ps.viewangles, forward, right, up);

  CalcMuzzlePoint (ent, forward, right, up, muzzle);

  VectorMA (muzzle, 32, forward, end);

  gi->Trace (&tr, muzzle, NULL, NULL, end, ent->sh.s.number, MASK_SHOT);
  if (tr.surfaceFlags & SURF_NOIMPACT)
    {
      return false;
    }

  traceEnt = &g_entities[tr.entityNum];

  // send blood impact
  if (traceEnt->takedamage && traceEnt->client)
    {
      tent = G_TempEntity (tr.endpos, EV_MISSILE_HIT);
      tent->sh.s.otherEntityNum = traceEnt->sh.s.number;
      tent->sh.s.eventParm = DirToByte (tr.plane.normal);
      tent->sh.s.weapon = ent->sh.s.weapon;
    }

  if (!traceEnt->takedamage)
    {
      return false;
    }

  if (ent->client->ps.powerups[PW_QUAD])
    {
      G_AddEvent (ent, EV_POWERUP_QUAD, 0);
      s_quadFactor = g_quadfactor->value;
    }
  else
    {
      s_quadFactor = 1;
    }

  damage = g_gauntlet_damage->integer * s_quadFactor;
  G_Damage (traceEnt, ent, ent, forward, tr.endpos, damage, 0, MOD_GAUNTLET);

  return true;
}

/*
======================================================================

MACHINEGUN

======================================================================
*/

/*
======================
SnapVectorTowards

Round a vector to integers for more efficient network
transmission, but make sure that it rounds towards a given point
rather than blindly truncating.  This prevents it from truncating 
into a wall.
======================
*/
// TODO : move this to q_math_vector.c ?
void
SnapVectorTowards (vec3_t v, vec3_t to)
{
  int i;

  for (i = 0; i < 3; i++)
    {
      if (to[i] <= v[i])
	{
	  v[i] = nearbyintf (v[i]);
	}
      else
	{
	  v[i] = nearbyintf (v[i] + 1);
	}
    }
}

/*
===============
Bullet_Fire

muzzle point was caculated before calling this function
===============
*/
void
Bullet_Fire (gentity_t * ent, float spread, int damage)
{
  trace_t tr;
  vec3_t end;
  float r;
  float u;
  gentity_t *tent;
  gentity_t *traceEnt;

  damage *= s_quadFactor;

  r = random () * M_PI * 2.0f;
  u = sin (r) * crandom () * spread * 16;
  r = cos (r) * crandom () * spread * 16;
  VectorMA (muzzle, 8192 * 16, forward, end);
  VectorMA (end, r, right, end);
  VectorMA (end, u, up, end);

  gi->Trace (&tr, muzzle, NULL, NULL, end, ENTITYNUM_NONE, MASK_SHOT);
  if (tr.surfaceFlags & SURF_NOIMPACT)
    {
      return;
    }

  traceEnt = &g_entities[tr.entityNum];

  // snap the endpos to integers, but nudged towards the line
  SnapVectorTowards (tr.endpos, muzzle);

  // send bullet impact
  if (traceEnt->takedamage && traceEnt->client)
    {
      tent = G_TempEntity (tr.endpos, EV_BULLET_HIT_FLESH);
      tent->sh.s.eventParm = traceEnt->sh.s.number;
      if (LogAccuracyHit (traceEnt, ent))
	{
	  ent->client->cbt.accuracy_hits++;
	}
    }
  else
    {
      tent = G_TempEntity (tr.endpos, EV_BULLET_HIT_WALL);
      tent->sh.s.eventParm = DirToByte (tr.plane.normal);
    }
  tent->sh.s.otherEntityNum = ent->sh.s.number;

  if (traceEnt->takedamage)
    {
      G_Damage (traceEnt, ent, ent, forward, tr.endpos,
		damage, 0, MOD_MACHINEGUN);
    }
}

/*
======================================================================

BFG

======================================================================
*/
void
BFG_Fire (gentity_t * ent)
{
  gentity_t *m;

  m = fire_bfg (ent, muzzle, forward);
  m->damage *= s_quadFactor;
  m->splashDamage *= s_quadFactor;

  //VectorAdd( m->sh.s.pos.trDelta, ent->client->ps.velocity, m->sh.s.pos.trDelta );        // "real" physics
}

/*
======================================================================

SHOTGUN

======================================================================
*/
void
weapon_supershotgun_fire2 (gentity_t * ent)
{
  gentity_t *tent;
  gentity_t *traceEnt;
  trace_t tr;

  int i;
  float r, u;
  float offsetx, offsety;
  vec3_t origin;
  vec3_t end;

  bool hitClient = false;
  int damage;
  int seed = rand () & 0xffff;

  damage = g_shotgun_damage->integer * s_quadFactor;

  // generate a EV_SHOTGUN event
  tent = G_TempEntity (muzzle, EV_SHOTGUN);
  VectorScale (forward, 4096, tent->sh.s.origin2);
  gi->SnapVector (tent->sh.s.origin2);
  tent->sh.s.otherEntityNum = ent->sh.s.number;

  // generate the "random" spread pattern
  for (i = 0; i < SHOTGUN_COUNT; i++)
    {
      // a bit of randomness for pellets directions (changing end point)
      r = Q_crandom (&seed) * SHOTGUN_SPREAD * 16;
      u = Q_crandom (&seed) * SHOTGUN_SPREAD * 16;
      // don't spawn everything from the center of the player
      offsetx = Q_crandom (&seed) * 16;
      offsety = Q_crandom (&seed) * 12;

      VectorCopy (muzzle, origin);
      VectorMA (origin, offsetx, right, origin);
      VectorMA (origin, offsety, up, origin);

      // snap to integer coordinates for more efficient network bandwidth usage
      gi->SnapVector (origin);

      VectorMA (origin, 8192 * 16, forward, end);
      VectorMA (end, r, right, end);
      VectorMA (end, u, up, end);

      gi->Trace (&tr, origin, NULL, NULL, end, ENTITYNUM_NONE, MASK_SHOT);
      // it might occur we shot ourselves
      //gi->Trace (&tr, origin, NULL, NULL, end, ent->sh.s.number, MASK_SHOT);
      if (tr.surfaceFlags & SURF_NOIMPACT)
	{
	  return;
	}

      traceEnt = &g_entities[tr.entityNum];

      // snap the endpos to integers, but nudged towards the line
      SnapVectorTowards (tr.endpos, origin);

      // send bullet impact
      if (traceEnt->takedamage && traceEnt->client)
	{
	  tent = G_TempEntity (tr.endpos, EV_SG_BULLET_HIT_FLESH);
	  tent->sh.s.eventParm = traceEnt->sh.s.number;
	  if (LogAccuracyHit (traceEnt, ent) && (hitClient == false))
	    {
	      hitClient = true;
	    }
	}
      else
	{
	  tent = G_TempEntity (tr.endpos, EV_SG_BULLET_HIT_WALL);
	  tent->sh.s.eventParm = DirToByte (tr.plane.normal);
	}
      // send shotgun blast
      tent->sh.s.otherEntityNum = ent->sh.s.number;

      if (traceEnt->takedamage)
	{
	  G_Damage (traceEnt, ent, ent, forward, tr.endpos,
		    damage, 0, MOD_SHOTGUN);
	}
    }

  // register a hit if a pellet touched someone
  if (hitClient)
    ent->client->cbt.accuracy_hits++;
}

/*
======================================================================

GRENADE LAUNCHER

======================================================================
*/
void
weapon_grenadelauncher_fire (gentity_t * ent)
{
  gentity_t *m;

  // extra vertical velocity
  forward[2] += 0.2f;
  VectorNormalize (forward);

  m = fire_grenade (ent, muzzle, forward);
  m->damage *= s_quadFactor;
  m->splashDamage *= s_quadFactor;

  //VectorAdd( m->sh.s.pos.trDelta, ent->client->ps.velocity, m->sh.s.pos.trDelta );        // "real" physics
}

/*
======================================================================

ROCKET

======================================================================
*/
void
Weapon_RocketLauncher_Fire (gentity_t * ent)
{
  gentity_t *m;

  m = fire_rocket (ent, muzzle, forward);
  m->damage *= s_quadFactor;
  m->splashDamage *= s_quadFactor;

  //VectorAdd( m->sh.s.pos.trDelta, ent->client->ps.velocity, m->sh.s.pos.trDelta );        // "real" physics
}

/*
======================================================================

PLASMA GUN

======================================================================
*/
void
Weapon_Plasmagun_Fire (gentity_t * ent)
{
  gentity_t *m;

  m = fire_plasma (ent, muzzle, forward);
  m->damage *= s_quadFactor;
  m->splashDamage *= s_quadFactor;

  //VectorAdd( m->sh.s.pos.trDelta, ent->client->ps.velocity, m->sh.s.pos.trDelta );        // "real" physics
}

/*
======================================================================

RAILGUN

======================================================================
*/

/*
=================
weapon_railgun_fire
=================
*/
#define MAX_RAIL_HITS 4
void
weapon_railgun_fire (gentity_t * ent)
{
  vec3_t end;

  trace_t trace;
  gentity_t *tent;
  gentity_t *traceEnt;
  int damage;
  int i;
  int hits;
  int unlinked;
  gentity_t *unlinkedEntities[MAX_RAIL_HITS];

  damage = g_railgun_damage->integer * s_quadFactor;

  VectorMA (muzzle, 8192, forward, end);

  // trace only against the solids, so the railgun will go through people
  unlinked = 0;
  hits = 0;
  do
    {
      gi->Trace (&trace, muzzle, NULL, NULL, end, ENTITYNUM_NONE, MASK_SHOT);
      if (trace.entityNum >= ENTITYNUM_MAX_NORMAL)
	{
	  break;
	}
      traceEnt = &g_entities[trace.entityNum];
      if (traceEnt->takedamage)
	{
	  if (LogAccuracyHit (traceEnt, ent))
	    {
	      hits++;
	    }
	  G_Damage (traceEnt, ent, ent, forward, trace.endpos, damage, 0,
		    MOD_RAILGUN);
	}
      if (trace.contents & CONTENTS_SOLID)
	{
	  break;		// we hit something solid enough to stop the beam
	}
      // unlink this entity, so the next trace will go past it
      gi->UnlinkEntity (&traceEnt->sh);
      unlinkedEntities[unlinked] = traceEnt;
      unlinked++;
    }
  while (unlinked < MAX_RAIL_HITS);

  // link back in any entities we unlinked
  for (i = 0; i < unlinked; i++)
    {
      gi->LinkEntity (&unlinkedEntities[i]->sh);
    }

  // the final trace endpos will be the terminal point of the rail trail
  // snap the endpos to integers to save net bandwidth,
  // but nudged towards the line
  SnapVectorTowards (trace.endpos, muzzle);

  // send railgun beam effect
  tent = G_TempEntity (trace.endpos, EV_RAILTRAIL);

  // set the weapon ??
  tent->sh.s.weapon = ent->sh.s.weapon;

  // set player number for custom colors on the railtrail
  tent->sh.s.clientNum = ent->sh.s.clientNum;

  VectorCopy (muzzle, tent->sh.s.origin2);
  // move origin a bit to come closer to the drawn gun muzzle
  // let the client handle repositioning
  //VectorMA (tent->sh.s.origin2, 4, right, tent->sh.s.origin2);
  VectorMA (tent->sh.s.origin2, -1, up, tent->sh.s.origin2);

  // no explosion at end if SURF_NOIMPACT, but still make the trail
  if (trace.surfaceFlags & SURF_NOIMPACT)
    {
      tent->sh.s.eventParm = 255;	// don't make the explosion at the end
    }
  else
    {
      tent->sh.s.eventParm = DirToByte (trace.plane.normal);
    }
  tent->sh.s.clientNum = ent->sh.s.clientNum;

  // give the shooter a reward sound if they have made two railgun hits in a row
  if (hits == 0)
    {
      // complete miss
      ent->client->cbt.accurateCount = 0;
    }
  else
    {
      // check for "impressive" reward sound
      ent->client->cbt.accurateCount += hits;
      if (ent->client->cbt.accurateCount >= 2)
	{
	  ent->client->cbt.accurateCount -= 2;
	  ent->client->ps.persistant[PERS_IMPRESSIVE_COUNT]++;
	  // add the sprite over the player's head
	  ent->client->ps.eFlags &= EF_CLEAR_AWARDS;
	  ent->client->ps.eFlags |= EF_AWARD_IMPRESSIVE;
	  ent->client->timer.rewardTime = level.time + REWARD_SPRITE_TIME;
	}
      ent->client->cbt.accuracy_hits += hits;
    }
}

/*
======================================================================

LIGHTNING GUN

======================================================================
*/
void
Weapon_LightningFire (gentity_t * ent)
{
  trace_t tr;
  vec3_t end;
  gentity_t *traceEnt, *tent;
  int damage;

  damage = g_lightning_damage->integer * s_quadFactor;

  VectorMA (muzzle, LIGHTNING_RANGE, forward, end);

  gi->Trace (&tr, muzzle, NULL, NULL, end, ENTITYNUM_NONE, MASK_SHOT);

  if (tr.entityNum == ENTITYNUM_NONE)
    {
      return;
    }
  traceEnt = &g_entities[tr.entityNum];

  if (traceEnt->takedamage)
    {
      G_Damage (traceEnt, ent, ent, forward, tr.endpos, damage, 0,
		MOD_LIGHTNING);
    }

  if (traceEnt->takedamage && traceEnt->client)
    {
      tent = G_TempEntity (tr.endpos, EV_MISSILE_HIT);
      tent->sh.s.otherEntityNum = traceEnt->sh.s.number;
      tent->sh.s.eventParm = DirToByte (tr.plane.normal);
      tent->sh.s.weapon = ent->sh.s.weapon;
      if (LogAccuracyHit (traceEnt, ent))
	{
	  ent->client->cbt.accuracy_hits++;
	}
    }
  else if (!(tr.surfaceFlags & SURF_NOIMPACT))
    {
      tent = G_TempEntity (tr.endpos, EV_MISSILE_MISS);
      tent->sh.s.eventParm = DirToByte (tr.plane.normal);
    }
}

//======================================================================

/*
===============
LogAccuracyHit
===============
*/
bool
LogAccuracyHit (gentity_t * target, gentity_t * attacker)
{
  if (!target->takedamage)
    {
      return false;
    }

  if (target == attacker)
    {
      return false;
    }

  if (!target->client)
    {
      return false;
    }

  if (!attacker->client)
    {
      return false;
    }

  if (target->client->ps.stats[STAT_HEALTH] <= 0)
    {
      return false;
    }

  if (OnSameTeam (target, attacker))
    {
      return false;
    }

  return true;
}

/*
===============
CalcMuzzlePoint

set muzzle location relative to pivoting eye
===============
*/
void
CalcMuzzlePoint (gentity_t * ent, vec3_t forward, vec3_t right, vec3_t up,
		 vec3_t muzzlePoint)
{
  VectorCopy (ent->sh.s.pos.trBase, muzzlePoint);
  muzzlePoint[2] += ent->client->ps.viewheight;
  VectorMA (muzzlePoint, 14, forward, muzzlePoint);
  // snap to integer coordinates for more efficient network bandwidth usage
  gi->SnapVector (muzzlePoint);
}

/*
===============
CalcMuzzlePointOrigin

set muzzle location relative to pivoting eye
===============
*/
void
CalcMuzzlePointOrigin (gentity_t * ent, vec3_t origin, vec3_t forward,
		       vec3_t right, vec3_t up, vec3_t muzzlePoint)
{
  VectorCopy (ent->sh.s.pos.trBase, muzzlePoint);
  muzzlePoint[2] += ent->client->ps.viewheight;
  VectorMA (muzzlePoint, 14, forward, muzzlePoint);
  // snap to integer coordinates for more efficient network bandwidth usage
  gi->SnapVector (muzzlePoint);
}

/*
===============
FireWeapon
===============
*/
void
FireWeapon (gentity_t * ent)
{
  if (ent->client->ps.powerups[PW_QUAD])
    {
      s_quadFactor = g_quadfactor->value;
    }
  else
    {
      s_quadFactor = 1;
    }

  if ((ent->sh.s.weapon < WP_NONE) || (ent->sh.s.weapon >= WP_NUM_WEAPONS))
    {
      G_Error ("Bad ent->sh.s.weapon in FireWeapon ()");
      return;
    }

  // track shots taken for accuracy tracking.  Grapple is not a weapon and gauntet is just not tracked
  if (ent->sh.s.weapon != WP_GAUNTLET)
    {
      ent->client->cbt.accuracy_shots++;
      // faster than tracking each created projectile
      if (ent->sh.s.weapon == WP_SHOTGUN)
	ent->client->stats.weap[WP_SHOTGUN].fired += SHOTGUN_COUNT;
      else
	ent->client->stats.weap[ent->sh.s.weapon].fired++;
    }

  // set aiming directions
  AngleVectors (ent->client->ps.viewangles, forward, right, up);

  //CalcMuzzlePointOrigin (ent, ent->client->misc.oldOrigin, forward, right, up,
  //			 muzzle);
  CalcMuzzlePoint (ent, forward, right, up, muzzle);

  // fire the specific weapon
  switch (ent->sh.s.weapon)
    {
    case WP_GAUNTLET:
      Weapon_Gauntlet (ent);
      break;
    case WP_LIGHTNING:
      Weapon_LightningFire (ent);
      break;
    case WP_SHOTGUN:
      weapon_supershotgun_fire2 (ent);
      break;
    case WP_MACHINEGUN:
      // move this in a function called once on gametype selection
      if (g_gametype->integer != GT_TEAM)
	{
	  Bullet_Fire (ent, MACHINEGUN_SPREAD, g_machinegun_damage->integer);
	}
      else
	{
	  Bullet_Fire (ent, MACHINEGUN_SPREAD, MACHINEGUN_TEAM_DAMAGE);
	}
      break;
    case WP_GRENADE_LAUNCHER:
      weapon_grenadelauncher_fire (ent);
      break;
    case WP_ROCKET_LAUNCHER:
      Weapon_RocketLauncher_Fire (ent);
      break;
    case WP_PLASMAGUN:
      Weapon_Plasmagun_Fire (ent);
      break;
    case WP_RAILGUN:
      weapon_railgun_fire (ent);
      break;
    case WP_BFG:
      BFG_Fire (ent);
      break;
    case WP_NONE:
      G_Printf ("No weapon to fire with for ent: %d!\n", ent->sh.s.clientNum);
      break;
    default:
      // FIXME - this should never happend
      G_Error ("Bad ent->sh.s.weapon");
      break;
    }
}

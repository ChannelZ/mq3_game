/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// bg_misc.c -- both games misc functions, all completely stateless

#include "qcommon/q_shared.h"
#include "bg_public.h"


// temporary, find a better way to do this
// can't use (const int) for table init

// keep theses in sync with gametype_t enumeration
// names used when parsing the entitystring
const char *
gametypeNames[GT_MAX_GAME_TYPE] =
  {
    "ffa",
    "tournament",
    "team",
    "ctf"
  };

const char *
gametypeFullNames[GT_MAX_GAME_TYPE] =
  {
    "Free For All",
    "Tournament",
    "Team Deathmatch",
    "Capture the Flag",
  };

const int
maxammo_table[WP_NUM_WEAPONS] =
  { 0,				// WP_NONE
    0,				// WP_GAUNTLET
    MACHINEGUN_MAX_AMMO,	// WP_MACHINEGUN
    SHOTGUN_MAX_AMMO,		// WP_SHOTGUN
    GRENADE_LAUNCHER_MAX_AMMO,	// WP_GRENADE_LAUNCHER
    ROCKET_LAUNCHER_MAX_AMMO,	// WP_ROCKET_LAUNCHER
    LIGHTNING_MAX_AMMO,		// WP_LIGHTNING
    RAILGUN_MAX_AMMO,		// WP_RAILGUN
    PLASMAGUN_MAX_AMMO,		// WP_PLASMAGUN
    BFG_MAX_AMMO		// WP_BFG
  };


const int
weappicking_table[WP_NUM_WEAPONS] =
  { 0,				// WP_NONE
    0,				// WP_GAUNTLET
    MACHINEGUN_PICKING_AMMO,	// WP_MACHINEGUN
    SHOTGUN_PICKING_AMMO,	// WP_SHOTGUN
    GRENADE_LAUNCHER_PICKING_AMMO,	// WP_GRENADE_LAUNCHER
    ROCKET_LAUNCHER_PICKING_AMMO,	// WP_ROCKET_LAUNCHER
    LIGHTNING_PICKING_AMMO,	// WP_LIGHTNING
    RAILGUN_PICKING_AMMO,	// WP_RAILGUN
    PLASMAGUN_PICKING_AMMO,	// WP_PLASMAGUN
    BFG_PICKING_AMMO		// WP_BFG
  };

const int
ammopicking_table [WP_NUM_WEAPONS] =
  { 0,				// WP_NONE
    0,				// WP_GAUNTLET
    MACHINEGUN_AMMO_BOX,	// WP_MACHINEGUN
    SHOTGUN_AMMO_BOX,		// WP_SHOTGUN
    GRENADE_LAUNCHER_AMMO_BOX,	// WP_GRENADE_LAUNCHER
    ROCKET_LAUNCHER_AMMO_BOX,	// WP_ROCKET_LAUNCHER
    LIGHTNING_AMMO_BOX,		// WP_LIGHTNING
    RAILGUN_AMMO_BOX,		// WP_RAILGUN
    PLASMAGUN_AMMO_BOX,		// WP_PLASMAGUN
    BFG_AMMO_BOX		// WP_BFG
  };


/*QUAKED item_***** ( 0 0 0 ) (-16 -16 -16) (16 16 16) suspended
DO NOT USE THIS CLASS, IT JUST HOLDS GENERAL INFORMATION.
The suspended flag will allow items to hang in the air, otherwise they are dropped to the next surface.

If an item is the target of another entity, it will not spawn in until fired.

An item fires all of its targets when it is picked up.  If the toucher can't carry it, the targets won't be fired.

"notfree" if set to 1, don't spawn in free for all games
"notteam" if set to 1, don't spawn in team games
"notsingle" if set to 1, don't spawn in single player games
"wait"	override the default wait before respawning.  -1 = never respawn automatically, which can be used with targeted spawning.
"random" random number of plus or minus seconds varied from the respawn time
"count" override quantity or duration on most items.
*/

/*
typedef struct gitem_s
{
/// spawning name
char		*classname;
char		*pickup_sound;
char		*world_model[MAX_ITEM_MODELS];

char		*icon;
/// for printing on pickup
char		*pickup_name;

char		*shortname;
char		color;

/// for ammo how much, or duration of powerup
int		quantity;

/// IT_* flags
itemType_t	giType;

int		giTag;

/// string of all models and images this item will use
char		*precaches;
/// string of all sounds this item will use
char		*sounds;
} gitem_t;
*/

const gitem_t
bg_itemlist[] =
{
  {
    NULL,
    NULL,
    {NULL,
     NULL,
     NULL, NULL} ,
    /* icon */
    NULL,
    /* pickup */
    NULL,
    NULL,
    0,
    0,
    0,
    0,
    /* precache */
    "",
    /* sounds */
    ""
  },	// leave index 0 alone

//
// ARMOR
//

/*QUAKED item_armor_shard (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  { /* classname */
    "item_armor_shard",
    /* pickup sound */
    "sound/misc/ar1_pkup.wav",
    /* world model list */
    {"models/powerups/armor/shard.md3",
     "models/powerups/armor/shard_sphere.md3",
     NULL, NULL},
    /* icon */
    "icons/iconr_shard",
    /* pickup name */
    "Armor Shard",
    /* shortname */
    "5 AM",
    /* color */
    ITEM_TA_COLOR,
    /* quantity */
    ARMOR_SHARDS,
    /* giType */
    IT_ARMOR,
    /* giTag */
    0,
    /* precache */
    "",
    /* sounds */
    ""
  },
  // bu add to make some maps compatible
  {
    /* classname */
    "item_armor_jacket",
    /* pickup sound */
    "sound/misc/ar2_pkup.wav",
    /* models */
    {"models/powerups/armor/armor_yel.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconr_green",
    /* pickup */
    "Light Armor",
    /* shotname */
    "LA",
    /* color tag */
    ITEM_LA_COLOR,
    /* quantity */
    ARMOR_LOW,
    /* giType */
    IT_ARMOR,
    /* giTag */
    0,
    /* precaches */
    /* models */
    "",
    /* sounds */
    "",
  },
  // bu end

/*QUAKED item_armor_combat (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_armor_combat",
    "sound/misc/ar2_pkup.wav",
    {"models/powerups/armor/armor_yel.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconr_yellow",
    /* pickup */
    "Medium Armor",
    "MA",
    ITEM_MA_COLOR,
    ARMOR_MEDIUM,
    IT_ARMOR,
    0,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED item_armor_body (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_armor_body",
    "sound/misc/ar2_pkup.wav",
    {"models/powerups/armor/armor_red.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconr_red",
    /* pickup */
    "Heavy Armor",
    "HA",
    ITEM_HA_COLOR,
    ARMOR_HIGH,
    IT_ARMOR,
    0,
    /* precache */
    "",
    /* sounds */
    ""
  },

//
// health
//
/*QUAKED item_health_small (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_health_small",
    "sound/items/s_health.wav",
    {"models/powerups/health/small_cross.md3",
     "models/powerups/health/small_sphere.md3",
     NULL, NULL},
    /* icon */
    "icons/iconh_green",
    /* pickup */
    "5 Health",
    "5 HLT",
    ITEM_TH_COLOR,
    5,
    IT_HEALTH,
    0,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED item_health (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_health",
    "sound/items/n_health.wav",
    {"models/powerups/health/medium_cross.md3",
     "models/powerups/health/medium_sphere.md3",
     NULL, NULL},
    /* icon */
    "icons/iconh_yellow",
    /* pickup */
    "25 Health",
    "25 HLT",
    ITEM_LH_COLOR,
    25,
    IT_HEALTH,
    0,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED item_health_large (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_health_large",
    "sound/items/l_health.wav",
    {"models/powerups/health/large_cross.md3",
     "models/powerups/health/large_sphere.md3",
     NULL, NULL},
    /* icon */
    "icons/iconh_red",
    /* pickup */
    "50 Health",
    "50 HLT",
    ITEM_MDH_COLOR,
    50,
    IT_HEALTH,
    0,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED item_health_mega (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_health_mega",
    "sound/items/m_health.wav",
    {"models/powerups/health/mega_cross.md3",
     "models/powerups/health/mega_sphere.md3",
     NULL, NULL},
    /* icon */
    "icons/iconh_mega",
    /* pickup */
    "Mega Health",
    "MH",
    ITEM_MGH_COLOR,
    100,
    IT_HEALTH,
    0,
    /* precache */
    "",
    /* sounds */
    ""
  },

//
// WEAPONS
//

/*QUAKED weapon_gauntlet (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "weapon_gauntlet",
    "sound/misc/w_pkup.wav",
    {"models/weapons2/gauntlet/gauntlet.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconw_gauntlet",
    /* pickup */
    GAUNTLET_NAME,
    GAUNTLET_SHORT,
    GAUNTLET_COLOR,
    0,
    IT_WEAPON,
    WP_GAUNTLET,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED weapon_machinegun (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "weapon_machinegun",
    "sound/misc/w_pkup.wav",
    {"models/weapons2/machinegun/machinegun.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconw_machinegun",
    /* pickup */
    MACHINEGUN_NAME,
    MACHINEGUN_SHORT,
    MACHINEGUN_COLOR,
    MACHINEGUN_PICKING_AMMO,
    IT_WEAPON,
    WP_MACHINEGUN,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED weapon_shotgun (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "weapon_shotgun",
    "sound/misc/w_pkup.wav",
    {"models/weapons2/shotgun/shotgun.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconw_shotgun",
    /* pickup */
    SHOTGUN_NAME,
    SHOTGUN_SHORT,
    SHOTGUN_COLOR,
    SHOTGUN_PICKING_AMMO,
    IT_WEAPON,
    WP_SHOTGUN,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED weapon_grenadelauncher (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "weapon_grenadelauncher",
    "sound/misc/w_pkup.wav",
    {"models/weapons2/grenadel/grenadel.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconw_grenade",
    /* pickup */
    GRENADE_NAME,
    GRENADE_SHORT,
    GRENADE_COLOR,
    GRENADE_LAUNCHER_PICKING_AMMO,
    IT_WEAPON,
    WP_GRENADE_LAUNCHER,
    /* precache */
    "",
    /* sounds */
    "sound/weapons/grenade/hgrenb1a.wav sound/weapons/grenade/hgrenb2a.wav"
  },

/*QUAKED weapon_rocketlauncher (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "weapon_rocketlauncher",
    "sound/misc/w_pkup.wav",
    {"models/weapons2/rocketl/rocketl.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconw_rocket",
    /* pickup */
    ROCKET_NAME,
    ROCKET_SHORT,
    ROCKET_COLOR,
    ROCKET_LAUNCHER_PICKING_AMMO,
    IT_WEAPON,
    WP_ROCKET_LAUNCHER,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED weapon_lightning (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "weapon_lightning",
    "sound/misc/w_pkup.wav",
    {"models/weapons2/lightning/lightning.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconw_lightning",
    /* pickup */
    LIGHTNING_NAME,
    LIGHTNING_SHORT,
    LIGHTNING_COLOR,
    LIGHTNING_PICKING_AMMO,
    IT_WEAPON,
    WP_LIGHTNING,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED weapon_railgun (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "weapon_railgun",
    "sound/misc/w_pkup.wav",
    {"models/weapons2/railgun/railgun.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconw_railgun",
    /* pickup */
    RAILGUN_NAME,
    RAILGUN_SHORT,
    RAILGUN_COLOR,
    RAILGUN_PICKING_AMMO,
    IT_WEAPON,
    WP_RAILGUN,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED weapon_plasmagun (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "weapon_plasmagun",
    "sound/misc/w_pkup.wav",
    {"models/weapons2/plasma/plasma.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconw_plasma",
    /* pickup */
    PLASMA_NAME,
    PLASMA_SHORT,
    PLASMA_COLOR,
    PLASMAGUN_PICKING_AMMO,
    IT_WEAPON,
    WP_PLASMAGUN,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED weapon_bfg (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "weapon_bfg",
    "sound/misc/w_pkup.wav",
    {"models/weapons2/bfg/bfg.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconw_bfg",
    /* pickup */
    BFG_NAME,
    BFG_SHORT,
    BFG_COLOR,
    BFG_PICKING_AMMO,
    IT_WEAPON,
    WP_BFG,
    /* precache */
    "",
    /* sounds */
    ""
  },

//
// AMMO ITEMS
//

/*QUAKED ammo_bullets (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "ammo_bullets",
    "sound/misc/am_pkup.wav",
    {"models/powerups/ammo/machinegunam.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/icona_machinegun",
    /* pickup */
    "Bullets",
    "MG_AM",
    '3',
    MACHINEGUN_AMMO_BOX,
    IT_AMMO,
    WP_MACHINEGUN,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED ammo_shells (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "ammo_shells",
    "sound/misc/am_pkup.wav",
    {"models/powerups/ammo/shotgunam.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/icona_shotgun",
    /* pickup */
    "Shells",
    "SG_AM",
    'h',
    SHOTGUN_AMMO_BOX,
    IT_AMMO,
    WP_SHOTGUN,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED ammo_grenades (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "ammo_grenades",
    "sound/misc/am_pkup.wav",
    {"models/powerups/ammo/grenadeam.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/icona_grenade",
    /* pickup */
    "Grenades",
    "GL_AM",
    'v',
    GRENADE_LAUNCHER_AMMO_BOX,
    IT_AMMO,
    WP_GRENADE_LAUNCHER,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED ammo_rockets (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "ammo_rockets",
    "sound/misc/am_pkup.wav",
    {"models/powerups/ammo/rocketam.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/icona_rocket",
    /* pickup */
    "Rockets",
    "RL_AM",
    '1',
    ROCKET_LAUNCHER_AMMO_BOX,
    IT_AMMO,
    WP_ROCKET_LAUNCHER,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED ammo_lightning (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "ammo_lightning",
    "sound/misc/am_pkup.wav",
    {"models/powerups/ammo/lightningam.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/icona_lightning",
    /* pickup */
    "Lightnings",
    "LG_AM",
    'V',
    LIGHTNING_AMMO_BOX,
    IT_AMMO,
    WP_LIGHTNING,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED ammo_slugs (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "ammo_slugs",
    "sound/misc/am_pkup.wav",
    {"models/powerups/ammo/railgunam.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/icona_railgun",
    /* pickup */
    "Slugs",
    "RL_AM",
    '2',
    RAILGUN_AMMO_BOX,
    IT_AMMO,
    WP_RAILGUN,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED ammo_cells (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "ammo_cells",
    "sound/misc/am_pkup.wav",
    {"models/powerups/ammo/plasmaam.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/icona_plasma",
    /* pickup */
    "Cells",
    "PG_AM",
    'T',
    PLASMAGUN_AMMO_BOX,
    IT_AMMO,
    WP_PLASMAGUN,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED ammo_bfg (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "ammo_bfg",
    "sound/misc/am_pkup.wav",
    {"models/powerups/ammo/bfgam.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/icona_bfg",
    /* pickup */
    "Bfg Ammo",
    "BFG_AM",
    'O',
    BFG_AMMO_BOX,
    IT_AMMO,
    WP_BFG,
    /* precache */
    "",
    /* sounds */
    ""
  },

//
// HOLDABLE ITEMS
//
/*QUAKED holdable_teleporter (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "holdable_teleporter",
    "sound/items/holdable.wav",
    {"models/powerups/holdable/teleporter.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/teleporter",
    /* pickup */
    "Personal Teleporter",
    "TELE",
    'W',
    60,
    IT_HOLDABLE,
    HI_TELEPORTER,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED holdable_medkit (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "holdable_medkit",
    "sound/items/holdable.wav",
    {"models/powerups/holdable/medkit.md3",
     "models/powerups/holdable/medkit_sphere.md3",
     NULL, NULL},
    /* icon */
    "icons/medkit",
    /* pickup */
    "Medkit",
    "MED",
    'b',
    60,
    IT_HOLDABLE,
    HI_MEDKIT,
    /* precache */
    "",
    /* sounds */
    "sound/items/use_medkit.wav"
  },

//
// POWERUP ITEMS
//
/*QUAKED item_quad (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_quad",
    "sound/items/quaddamage.wav",
    {"models/powerups/instant/quad.md3",
     "models/powerups/instant/quad_ring.md3",
     NULL, NULL},
    /* icon */
    "icons/quad",
    /* pickup */
    "Quad Damage",
    "QUAD",
    'L',
    30,
    IT_POWERUP,
    PW_QUAD,
    /* precache */
    "",
    /* sounds */
    "sound/items/damage2.wav sound/items/damage3.wav"
  },

/*QUAKED item_enviro (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_enviro",
    "sound/items/protect.wav",
    {"models/powerups/instant/enviro.md3",
     "models/powerups/instant/enviro_ring.md3",
     NULL, NULL},
    /* icon */
    "icons/envirosuit",
    /* pickup */
    "Battle Suit",
    "BS",
    'p',
    30,
    IT_POWERUP,
    PW_BATTLESUIT,
    /* precache */
    "",
    /* sounds */
    "sound/items/airout.wav sound/items/protect3.wav"
  },

/*QUAKED item_haste (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_haste",
    "sound/items/haste.wav",
    {"models/powerups/instant/haste.md3",
     "models/powerups/instant/haste_ring.md3",
     NULL, NULL},
    /* icon */
    "icons/haste",
    /* pickup */
    "Speed",
    "SPD",
    'h',
    30,
    IT_POWERUP,
    PW_HASTE,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED item_invis (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_invis",
    "sound/items/invisibility.wav",
    {"models/powerups/instant/invis.md3",
     "models/powerups/instant/invis_ring.md3",
     NULL, NULL},
    /* icon */
    "icons/invis",
    /* pickup */
    "Invisibility",
    "INVIS",
    '7',
    30,
    IT_POWERUP,
    PW_INVIS,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED item_regen (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_regen",
    "sound/items/regeneration.wav",
    {"models/powerups/instant/regen.md3",
     "models/powerups/instant/regen_ring.md3",
     NULL, NULL},
    /* icon */
    "icons/regen",
    /* pickup */
    "Regeneration",
    "REGEN",
    '1',
    30,
    IT_POWERUP,
    PW_REGEN,
    /* precache */
    "",
    /* sounds */
    "sound/items/regen.wav"
  },

/*QUAKED item_flight (.3 .3 1) (-16 -16 -16) (16 16 16) suspended
*/
  {
    "item_flight",
    "sound/items/flight.wav",
    {"models/powerups/instant/flight.md3",
     "models/powerups/instant/flight_ring.md3",
     NULL, NULL},
    /* icon */
    "icons/flight",
    /* pickup */
    "Flight",
    "FLIGHT",
    'Y',
    60,
    IT_POWERUP,
    PW_FLIGHT,
    /* precache */
    "",
    /* sounds */
    "sound/items/flight.wav"
  },

/*QUAKED team_CTF_redflag (1 0 0) (-16 -16 -16) (16 16 16)
Only in CTF games
*/
  {
    "team_CTF_redflag",
    NULL,
    {"models/flags/r_flag.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconf_red1",
    /* pickup */
    "Red Flag",
    "Red FL",
    '1',
    0,
    IT_TEAM,
    PW_REDFLAG,
    /* precache */
    "",
    /* sounds */
    ""
  },

/*QUAKED team_CTF_blueflag (0 0 1) (-16 -16 -16) (16 16 16)
Only in CTF games
*/
  {
    "team_CTF_blueflag",
    NULL,
    {"models/flags/b_flag.md3",
     NULL, NULL, NULL},
    /* icon */
    "icons/iconf_blu1",
    /* pickup */
    "Blue Flag",
    "Blue FL",
    '4',
    0,
    IT_TEAM,
    PW_BLUEFLAG,
    /* precache */
    "",
    /* sounds */
    ""
  },

  // end of list marker
  {
    NULL,
    NULL,
    {NULL, NULL, NULL, NULL},
    NULL,
    NULL,
    NULL,
    0,
    0,
    0,
    0,
    NULL,
    NULL
  }
};

// cg_local is using a macro BG_ITEMLIST with the number of items, keep track of this
const int bg_numItems = ARRAY_LEN (bg_itemlist) - 1;

/*
==============
BG_FindItemForPowerup
==============
*/
const gitem_t *
BG_FindItemForPowerup (powerup_t pw)
{
  const gitem_t * it;

  for (it = bg_itemlist + 1; it->classname; it++)
    {
      if ((it->giType == IT_POWERUP || (it->giType == IT_TEAM))
	  && (it->giTag == pw))
	{
	  return it;
	}
    }

  SHD_ERROR (ERR_DROP, "Couldn't find item for powerup %i", pw);
  return NULL;
}

/*
==============
BG_FindItemForHoldable
==============
*/
const gitem_t *
BG_FindItemForHoldable (holdable_t pw)
{
  const gitem_t * it;

  for (it = bg_itemlist + 1; it->classname; it++)
    {
      if (it->giType == IT_HOLDABLE && it->giTag == pw)
	{
	  return it;
	}
    }

  SHD_ERROR (ERR_DROP, "HoldableItem not found : %i", pw);
  return NULL;
}

/*
===============
BG_FindItemForWeapon

===============
*/
const gitem_t *
BG_FindItemForWeapon (weapon_t weapon)
{
  const gitem_t * it;

  for (it = bg_itemlist + 1; it->classname; it++)
    {
      if (it->giType == IT_WEAPON && it->giTag == weapon)
	{
	  return it;
	}
    }

  SHD_ERROR (ERR_DROP, "Couldn't find item for weapon %i", weapon);
  return NULL;
}

/*
===============
BG_FindItemForAmmo

===============
*/
const gitem_t *
BG_FindItemForAmmo (weapon_t weapon)
{
  const gitem_t * it;

  for (it = bg_itemlist + 1; it->classname; it++)
    {
      if (it->giType == IT_AMMO && it->giTag == weapon)
	{
	  return it;
	}
    }

  SHD_ERROR (ERR_DROP, "Couldn't find item for ammo %i", weapon);
  return NULL;
}

/*
===============
BG_FindItem

===============
*/
const gitem_t *
BG_FindItem (const char *pickupName)
{
  const gitem_t * it;

  for (it = bg_itemlist + 1; it->classname; it++)
    {
      if (!Q_stricmp (it->pickup_name, pickupName)
	  || !Q_stricmp (it->classname, pickupName))
	return it;
    }

  // this is used by cmd_give, and can be wrong
  // FIXME : add a SHD_WARNING ?
  //SHD_ERROR (ERR_DROP, "Couldn't find item %s", pickupName);
  return NULL;
}

/*
=================
BG_GametypeIsTeam
=================
*/
bool
BG_GametypeIsTeam (gametype_t gt)
{
  if (gt >= GT_TEAM)
    {
      return true;
    }
  else
    {
      return false;
    }
}

/**
============
BG_PlayerTouchesItem

Items can be picked up without actually touching their physical bounds to make
grabbing them easier
============
*/
bool
BG_PlayerTouchesItem (playerState_t * ps, entityState_t * item, int atTime)
{
  vec3_t origin;

  BG_EvaluateTrajectory (&item->pos, atTime, origin);

  // we are ignoring ducked differences here
  // NOTE (bu) : can't understand why x and y haven't the same values for check
  if ((ps->origin[0] - origin[0] > 36)	// 44
      || (ps->origin[0] - origin[0] < -36)	// -50
      || (ps->origin[1] - origin[1] > 36)	// 36
      || (ps->origin[1] - origin[1] < -36)	// -36
      || (ps->origin[2] - origin[2] > 64)	// 36
      || (ps->origin[2] - origin[2] < -36)) // -36
    {
      return false;
    }

  return true;
}

/**
================
BG_CanItemBeGrabbed

Returns false if the item should not be picked up.
This needs to be the same for client side prediction and server use.
================
*/
bool
BG_CanItemBeGrabbed (int gametype, const entityState_t * ent,
		     const playerState_t * ps, int parameter)
{
  const gitem_t * item;

  if ((ent->modelindex < 1) || (ent->modelindex >= bg_numItems))
    {
      SHD_ERROR (ERR_DROP, "BG_CanItemBeGrabbed: index out of range");
    }

  item = &bg_itemlist[ent->modelindex];

  switch (item->giType)
    {
    case IT_WEAPON:
      {
	// always pick weapon, a least for strategical reasons ?
	/*
	  if (ps->ammo[ item->giTag ] >= item->quantity) {
	  return false;
	  }
	*/
	// check if player still has the weapon
	/*
	  if ( (ps->stats[STAT_WEAPONS]  & (1 << item->giTag)) && !ent->modelindex2 ) {
	  return false;
	  }
	*/

	return true;		// weapons are always picked up
      }

    case IT_AMMO:
      {
	// get max ammo value, don't pick if we are maxed
	if (ps->ammo[item->giTag] >= maxammo_table [item->giTag])
	  {
	    return false;	// can't hold any more
	  }
	return true;
      }

    case IT_ARMOR:
      {
	if (!parameter)
	  {
	    if (ps->stats[STAT_ARMOR] >= ps->stats[STAT_MAX_HEALTH] * 2)
	      {
		return false;
	      }
	    return true;
	  }
	else
	  {
	    // pro mode system
	    if (item->quantity == 100) // RA
	      {
		return (ps->stats[STAT_ARMOR] >= CPM_RAMAXARMOR) ? false : true;
	      }
	    else if (item->quantity == 50) // YA
	      {
		if (ps->stats[STAT_ARMOR_TYPE] <= 1)
		  {
		    return (ps->stats[STAT_ARMOR] >= CPM_YAMAXARMOR) ? false : true;
		  }
		return (ps->stats[STAT_ARMOR] > CPM_RABREAKPOINT) ? false : true;
	      }
	    else if (item->quantity == 25) // LA
	      {
		if (ps->stats[STAT_ARMOR_TYPE] == 0)
		  {
		    return (ps->stats[STAT_ARMOR] >= CPM_LAMAXARMOR) ? false : true;
		  }
		return (ps->stats[STAT_ARMOR] > CPM_YABREAKPOINT) ? false : true;
	      }
	    return true; // you can _always_ get shards in cpm
	  }
      }

    case IT_HEALTH:
      {
	// small and mega healths will go over the max, otherwise
	// don't pick up if already at max
	if ((item->quantity == 5) || (item->quantity == 100))
	  {
	    if (ps->stats[STAT_HEALTH] >= (ps->stats[STAT_MAX_HEALTH] * 2))
	      {
		return false;
	      }
	    return true;
	  }

	if (ps->stats[STAT_HEALTH] >= ps->stats[STAT_MAX_HEALTH])
	  {
	    return false;
	  }
	return true;
      }

    case IT_POWERUP:
      return true;	// powerups are always picked up

    case IT_TEAM:	// team items, such as flags
      {
	if (gametype == GT_CTF)
	  {
	    // ent->modelindex2 is non-zero on items if they are dropped
	    // we need to know this because we can pick up our dropped flag (and return it)
	    // but we can't pick up our flag at base
	    if (ps->persistant[PERS_TEAM] == TEAM_RED)
	      {
		if ((item->giTag == PW_BLUEFLAG)
		    || ((item->giTag == PW_REDFLAG) && (ent->modelindex2))
		    || ((item->giTag == PW_REDFLAG) && (ps->powerups[PW_BLUEFLAG])))
		  return true;
	      }
	    else if (ps->persistant[PERS_TEAM] == TEAM_BLUE)
	      {
		if ((item->giTag == PW_REDFLAG)
		    || ((item->giTag == PW_BLUEFLAG) && (ent->modelindex2))
		    || ((item->giTag == PW_BLUEFLAG) && (ps->powerups[PW_REDFLAG])))
		  return true;
	      }
	  }

	return false;
      }

    case IT_HOLDABLE:
      {
	// can only hold one item at a time
	if (ps->stats[STAT_HOLDABLE_ITEM])
	  {
	    return false;
	  }
	return true;
      }

    case IT_BAD:
      SHD_ERROR (ERR_DROP, "BG_CanItemBeGrabbed: IT_BAD");
    default:
#ifndef NDEBUG
      SHD_PRINT ("BG_CanItemBeGrabbed: unknown enum %d\n", item->giType);
#endif
      break;
    }

  return false;
}

//======================================================================

/*
================
BG_EvaluateTrajectory

================
*/
void
BG_EvaluateTrajectory (const trajectory_t *tr, int atTime, vec3_t result)
{
  float deltaTime;
  float phase;

  switch (tr->trType)
    {
    case TR_STATIONARY:
    case TR_INTERPOLATE:
      VectorCopy (tr->trBase, result);
      break;
    case TR_LINEAR:
      deltaTime = (atTime - tr->trTime) * 0.001f;	// milliseconds to seconds
      VectorMA (tr->trBase, deltaTime, tr->trDelta, result);
      break;
    case TR_SINE:
      deltaTime = (atTime - tr->trTime) / (float) tr->trDuration;
      phase = sin (deltaTime * M_PI * 2.0f);
      VectorMA (tr->trBase, phase, tr->trDelta, result);
      break;
    case TR_LINEAR_STOP:
      if (atTime > (tr->trTime + tr->trDuration))
	{
	  atTime = tr->trTime + tr->trDuration;
	}
      deltaTime = (atTime - tr->trTime) * 0.001f;	// milliseconds to seconds
      if (deltaTime < 0.0f)
	{
	  deltaTime = 0.0f;
	}
      VectorMA (tr->trBase, deltaTime, tr->trDelta, result);
      break;
    case TR_GRAVITY:
      deltaTime = (atTime - tr->trTime) * 0.001f;	// milliseconds to seconds
      VectorMA (tr->trBase, deltaTime, tr->trDelta, result);
      result[2] -= 0.5f * DEFAULT_GRAVITY * deltaTime * deltaTime;	// FIXME: local gravity...
      break;
    default:
      SHD_ERROR (ERR_DROP, "BG_EvaluateTrajectory: unknown trType: %i", tr->trTime);
      break;
    }
}

/*
================
BG_EvaluateTrajectoryDelta

For determining velocity at a given time
================
*/
void
BG_EvaluateTrajectoryDelta (const trajectory_t *tr, int atTime, vec3_t result)
{
  float deltaTime;
  float phase;

  switch (tr->trType)
    {
    case TR_STATIONARY:
    case TR_INTERPOLATE:
      VectorClear (result);
      break;
    case TR_LINEAR:
      VectorCopy (tr->trDelta, result);
      break;
    case TR_SINE:
      deltaTime = (atTime - tr->trTime) / (float) tr->trDuration;
      phase = cos (deltaTime * M_PI * 2.0f);	// derivative of sin = cos
      //phase *= 0.5f; // bu : dunno why ?
      VectorScale (tr->trDelta, phase, result);
      break;
    case TR_LINEAR_STOP:
      if (atTime > (tr->trTime + tr->trDuration))
	{
	  VectorClear (result);
	  return;
	}
      VectorCopy (tr->trDelta, result);
      break;
    case TR_GRAVITY:
      deltaTime = (atTime - tr->trTime) * 0.001f;	// milliseconds to seconds
      VectorCopy (tr->trDelta, result);
      result[2] -= DEFAULT_GRAVITY * deltaTime;	// FIXME: local gravity...
      break;
    default:
      SHD_ERROR (ERR_DROP, "BG_EvaluateTrajectoryDelta: unknown trType: %i", tr->trType);
      break;
    }
}


// this is used for debug infos, be sure it's in sync with declarations
char *
eventnames[] =
  {
    "EV_NONE",

    "EV_FOOTSTEP",
    "EV_FOOTSTEP_METAL",
    "EV_FOOTSPLASH",
    "EV_FOOTWADE",
    "EV_SWIM",

    "EV_STEP_4",
    "EV_STEP_8",
    "EV_STEP_12",
    "EV_STEP_16",

    "EV_FALL_SHORT",
    "EV_FALL_MEDIUM",
    "EV_FALL_FAR",
    /// boing sound at origin", jump sound on player
    "EV_JUMP_PAD",
    "EV_JUMP",
    /// foot touches
    "EV_WATER_TOUCH",
    /// foot leaves
    "EV_WATER_LEAVE",
    /// head touches
    "EV_WATER_UNDER",
    /// head leaves
    "EV_WATER_CLEAR",

    /// normal item pickups are predictable
    "EV_ITEM_PICKUP",
    /// powerup / team sounds are broadcast to everyone
    "EV_GLOBAL_ITEM_PICKUP",

    "EV_NOWEAPON",
    "EV_NOAMMO",
    "EV_CHANGE_WEAPON",
    "EV_FIRE_WEAPON",

    "EV_USE_ITEM0",
    "EV_USE_ITEM1",
    "EV_USE_ITEM2",
    "EV_USE_ITEM3",
    "EV_USE_ITEM4",
    "EV_USE_ITEM5",
    "EV_USE_ITEM6",
    "EV_USE_ITEM7",
    "EV_USE_ITEM8",
    "EV_USE_ITEM9",
    "EV_USE_ITEM10",
    "EV_USE_ITEM11",
    "EV_USE_ITEM12",
    "EV_USE_ITEM13",
    "EV_USE_ITEM14",
    "EV_USE_ITEM15",

    "EV_ITEM_RESPAWN",
    "EV_ITEM_POP",
    "EV_PLAYER_TELEPORT_IN",
    "EV_PLAYER_TELEPORT_OUT",

    /// eventParm will be the soundindex
    "EV_GRENADE_BOUNCE",

    "EV_GENERAL_SOUND",
    /// no attenuation
    "EV_GLOBAL_SOUND",
    "EV_GLOBAL_TEAM_SOUND",

    "EV_BULLET_HIT_FLESH",
    "EV_BULLET_HIT_WALL",

    "EV_MISSILE_HIT",
    "EV_MISSILE_MISS",
    "EV_MISSILE_MISS_METAL",
    "EV_RAILTRAIL",
    "EV_SHOTGUN",
    /// otherEntity is the shooter
    "EV_BULLET",

    "EV_PAIN",
    "EV_DEATH1",
    "EV_DEATH2",
    "EV_DEATH3",
    "EV_OBITUARY",

    "EV_POWERUP_QUAD",
    "EV_POWERUP_BATTLESUIT",
    "EV_POWERUP_REGEN",

    /// gib a previously living player
    "EV_GIB_PLAYER",
    /// score plum
    "EV_SCOREPLUM",

    "EV_DEBUG_LINE",
    "EV_STOPLOOPINGSOUND",
    "EV_TAUNT"
  };

/*
===============
BG_AddPredictableEventToPlayerstate

Handles the sequence numbers
===============
*/
void
BG_AddPredictableEventToPlayerstate (int newEvent, int eventParm,
				     playerState_t *ps)
{
#ifdef _DEBUG
#ifdef QAGAME
  SHD_PRINT (" game event svt %5d -> %5d: num = %20s parm %d\n",
	     /*ps->pmove_framecount*//*ps->commandTime*/, ps->eventSequence,
	      eventnames[newEvent], eventParm);
#else
  SHD_PRINT ("Cgame event svt %5d -> %5d: num = %20s parm %d\n",
	     /*ps->pmove_framecount*//*ps->commandTime*/, ps->eventSequence,
	      eventnames[newEvent], eventParm);
#endif
#endif
  ps->events[ps->eventSequence & (MAX_PS_EVENTS-1)] = newEvent;
  ps->eventParms[ps->eventSequence & (MAX_PS_EVENTS-1)] = eventParm;
  ps->eventSequence++;
}

/*
========================
BG_TouchJumpPad
========================
*/
void
BG_TouchJumpPad (playerState_t *ps, entityState_t *jumppad)
{
  vec3_t angles;
  float p;
  int effectNum;

  // spectators don't use jump pads
  if (ps->pm_type != PM_NORMAL)
    {
      return;
    }

  // flying characters don't hit bounce pads
  if (ps->powerups[PW_FLIGHT])
    {
      return;
    }

  // if we didn't hit this same jumppad the previous frame
  // then don't play the event sound again if we are in a fat trigger
  if (1/*ps->jumppad_ent != jumppad->number*/)
    {
      vectoangles (jumppad->origin2, angles);
      p = fabs (AngleNormalize180 (angles[PITCH]));
      if (p < 45)
	{
	  effectNum = 0;
	}
      else
	{
	  effectNum = 1;
	}
      BG_AddPredictableEventToPlayerstate (EV_JUMP_PAD, effectNum, ps);
    }
  // remember hitting this jumppad this frame
  ps->jumppad_ent = jumppad->number;
  //ps->jumppad_frame = ps->pmove_framecount;
  // give the player the velocity from the jumppad + current speed
  // this is quite funny, players can "jump"
  // the same frame they walk on the bumper
  /*
  if (ps->velocity[2] < 0)
    ps->velocity[2] = 0;
  VectorAdd (jumppad->origin2, ps->velocity, ps->velocity);
  */
  VectorCopy (jumppad->origin2, ps->velocity);
}

/*
========================
BG_PlayerStateToEntityState

This is done after each set of usercmd_t on the server,
and after local prediction on the client
========================
*/
void
BG_PlayerStateToEntityState (playerState_t *ps, entityState_t *s, bool snap)
{
  int i;

#define PMOVE_FIXED
#ifdef PMOVE_FIXED
  snap = false;
#endif

  if ((ps->pm_type == PM_INTERMISSION) || (ps->pm_type == PM_SPECTATOR))
    {
      s->eType = ET_INVISIBLE;
    }
  else if (ps->stats[STAT_HEALTH] <= GIB_HEALTH)
    {
      s->eType = ET_INVISIBLE;
    }
  else
    {
      s->eType = ET_PLAYER;
    }

  s->number = ps->clientNum;

  s->pos.trType = TR_INTERPOLATE;
  VectorCopy (ps->origin, s->pos.trBase);
  if (snap)
    {
      SnapVector (s->pos.trBase);
    }
  // set the trDelta for flag direction
  VectorCopy (ps->velocity, s->pos.trDelta);

  s->apos.trType = TR_INTERPOLATE;
  VectorCopy (ps->viewangles, s->apos.trBase);
  if (snap)
    {
      SnapVector (s->apos.trBase);
    }

  s->angles2[YAW] = ps->movementDir;
  s->legsAnim = ps->legsAnim;
  s->torsoAnim = ps->torsoAnim;
  // ET_PLAYER looks here instead of at number
  // so corpses can also reference the proper config
  s->clientNum = ps->clientNum;
  s->eFlags = ps->eFlags;
  if (ps->stats[STAT_HEALTH] <= 0)
    {
      s->eFlags |= EF_DEAD;
    }
  else
    {
      s->eFlags &= ~EF_DEAD;
    }

  if (ps->externalEvent)
    {
      s->event = ps->externalEvent;
      s->eventParm = ps->externalEventParm;
    }
  else if (ps->entityEventSequence < ps->eventSequence)
    {
      int seq;

      if (ps->entityEventSequence < (ps->eventSequence - MAX_PS_EVENTS))
	{
	  ps->entityEventSequence = ps->eventSequence - MAX_PS_EVENTS;
	}
      seq = ps->entityEventSequence & (MAX_PS_EVENTS-1);
      s->event = ps->events[seq] | ((ps->entityEventSequence & 3) << 8);
      s->eventParm = ps->eventParms[seq];
      ps->entityEventSequence++;
    }

  s->weapon = ps->weapon;
  s->groundEntityNum = ps->groundEntityNum;

  s->powerups = 0;
  for (i = 0; i < MAX_POWERUPS; i++)
    {
      if (ps->powerups[i])
	{
	  s->powerups |= 1 << i;
	}
    }

  s->loopSound = ps->loopSound;
  s->generic1 = ps->generic1;
}

/*
========================
BG_PlayerStateToEntityStateExtraPolate

This is done after each set of usercmd_t on the server,
and after local prediction on the client
========================
*/
/*
void
BG_PlayerStateToEntityStateExtraPolate (playerState_t *ps, entityState_t *s,
					int time, bool snap)
{
  int i;

#ifdef PMOVE_FIXED
  snap = false;
#endif

  if (ps->pm_type == PM_INTERMISSION || ps->pm_type == PM_SPECTATOR)
    {
      s->eType = ET_INVISIBLE;
    }
  else if (ps->stats[STAT_HEALTH] <= GIB_HEALTH)
    {
      s->eType = ET_INVISIBLE;
    }
  else
    {
      s->eType = ET_PLAYER;
    }

  s->number = ps->clientNum;

  s->pos.trType = TR_LINEAR_STOP;
  VectorCopy (ps->origin, s->pos.trBase);
  if (snap)
    {
      SnapVector (s->pos.trBase);
    }
  // set the trDelta for flag direction and linear prediction
  VectorCopy (ps->velocity, s->pos.trDelta);
  // set the time for linear prediction
  s->pos.trTime = time;
  // set maximum extrapolation time
  s->pos.trDuration = 50; // 1000 / sv_fps (default = 20)

  s->apos.trType = TR_INTERPOLATE;
  VectorCopy (ps->viewangles, s->apos.trBase);
  if (snap)
    {
      SnapVector (s->apos.trBase);
    }

  s->angles2[YAW] = ps->movementDir;
  s->legsAnim = ps->legsAnim;
  s->torsoAnim = ps->torsoAnim;
  // ET_PLAYER looks here instead of at number
  // so corpses can also reference the proper config
  s->clientNum = ps->clientNum;
  s->eFlags = ps->eFlags;
  if (ps->stats[STAT_HEALTH] <= 0)
    {
      s->eFlags |= EF_DEAD;
    }
  else
    {
      s->eFlags &= ~EF_DEAD;
    }

  if (ps->externalEvent)
    {
      s->event = ps->externalEvent;
      s->eventParm = ps->externalEventParm;
    }
  else if (ps->entityEventSequence < ps->eventSequence)
    {
      int seq;

      if (ps->entityEventSequence < ps->eventSequence - MAX_PS_EVENTS)
	{
	  ps->entityEventSequence = ps->eventSequence - MAX_PS_EVENTS;
	}
      seq = ps->entityEventSequence & (MAX_PS_EVENTS-1);
      s->event = ps->events[seq] | ((ps->entityEventSequence & 3) << 8);
      s->eventParm = ps->eventParms[seq];
      ps->entityEventSequence++;
    }

  s->weapon = ps->weapon;
  s->groundEntityNum = ps->groundEntityNum;

  s->powerups = 0;
  for (i = 0; i < MAX_POWERUPS; i++)
    {
      if (ps->powerups[i])
	{
	  s->powerups |= 1 << i;
	}
    }

  s->loopSound = ps->loopSound;
  s->generic1 = ps->generic1;
}
*/

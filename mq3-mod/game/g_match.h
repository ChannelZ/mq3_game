/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

// basically g_local.h + local stuff for g_match* files
#include "g_local.h"

void G_Match_CheckTournament (void);

void G_Match_RoundFrame (lvl_t * level);

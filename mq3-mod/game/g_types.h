/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

/// movers are things like doors, plats, buttons, etc
typedef enum
{
    MOVER_POS1,
    MOVER_POS2,
    MOVER_1TO2,
    MOVER_2TO1
} moverState_t;

/// gentity->flags
#define FL_GODMODE		0x00000010
#define FL_NOTARGET		0x00000020
/// not the first on the team
#define FL_TEAMSLAVE		0x00000400
#define FL_NO_KNOCKBACK		0x00000800
#define FL_DROPPED_ITEM		0x00001000
/// spawn point not for bot use
#define FL_NO_BOTS		0x00002000
/// spawn point just for bots
#define FL_NO_HUMANS		0x00004000
/// force gesture on client
#define FL_FORCE_GESTURE	0x00008000

struct gentity_s
{
  /*
    /// communicated by server to clients
    entityState_t	s;
    /// shared by both the server system and game
    entityShared_t	r;
  */
  /// structure made of entityState_t and entity_Shared_t
  /// entityState_t is communicated by server to clients
  /// entityShared_t is shared by both server system and game
  sharedEntity_t sh;

  // DO NOT MODIFY ANYTHING ABOVE THIS, THE SERVER
  // EXPECTS THE FIELDS IN THAT ORDER!
  //================================

  /// NULL if not a client
  struct gclient_s * client;

  bool		inuse;

  /// set in QuakeEd
  char *	classname;
  /// set in QuakeEd
  int		spawnflags;
  /// if true, FreeEntity will only unlink
  bool		neverFree;
  // bodyque uses this
  /// FL_* variables
  int		flags;

  char		*model;
  char		*model2;
  /// level.time when the object was freed
  int		freetime;

  /// events will be cleared EVENT_VALID_MSEC after set
  int		eventTime;
  bool		freeAfterEvent;
  bool		unlinkAfterEvent;

  /// if true, it can be pushed by movers and fall off edges
  bool		physicsObject;
  // all game items are physicsObjects,
  /// 1.0 = continuous bounce, 0.0 = no bounce
  float		physicsBounce;
  /// brushes with this content value will be collided against
  int		clipmask;
  // when moving.  items and corpses do not collide against
  // players, for instance

  /// movers
  moverState_t	moverState;
  int		soundPos1;
  int		sound1to2;
  int		sound2to1;
  int		soundPos2;
  struct gentity_s* parent;
  struct gentity_s* nextTrain;
  struct gentity_s* prevTrain;
  vec3_t	pos1, pos2;

  char *	message;
  /// body queue sinking, etc
  int		timestamp;

  /// set in editor, -1 = up, -2 = down
  float		angle;
  char *	target;
  char *	targetname;
  char *	team;

  float		speed;
  vec3_t	movedir;

  int		nextthink;
  void (*think) (struct gentity_s* self);
  /// movers call this when hitting endpoint
  void (*reached) (struct gentity_s* self);
  void (*blocked) (struct gentity_s* self, struct gentity_s* other);
  void (*touch) (struct gentity_s* self, struct gentity_s* other, trace_t* trace);
  void (*use) (struct gentity_s* self, struct gentity_s* other, struct gentity_s* activator);
  void (*pain) (struct gentity_s* self, struct gentity_s* attacker, int damage);
  void (*die) (struct gentity_s* self, struct gentity_s* inflictor,
	       struct gentity_s* attacker, int damage, int mod);

  int		pain_debounce_time;
  /// wind tunne
  int		fly_sound_debounce_time;

  int		health;

  bool		takedamage;

  int		damage;
  /// quad will increase this without increasing radius
  int		splashDamage;
  int		splashRadius;
  int		methodOfDeath;
  int		splashMethodOfDeath;

  int		count;

  struct gentity_s * chain;
  struct gentity_s * enemy;
  struct gentity_s * activator;
  /// next entity in team
  struct gentity_s * teamchain;
  /// master of the team
  struct gentity_s * teammaster;

  int		watertype;
  int		waterlevel;

  int		noise_index;

  /// timing variables
  float		wait;
  float		random;
  /// for bonus items
  const gitem_t	* item;
};
typedef struct gentity_s gentity_t;



typedef enum
{
    CON_DISCONNECTED,
    CON_CONNECTING,
    CON_CONNECTED
} clientConnected_t;

typedef enum
{
    SPECTATOR_NOT,
    SPECTATOR_FREE,
    SPECTATOR_FOLLOW,
    SPECTATOR_SCOREBOARD
} spectatorState_t;

typedef enum
{
    /// Beginning a team game, spawn at base
    TEAM_BEGIN,
    /// Now actively playing
    TEAM_ACTIVE
} playerTeamStateState_t;

typedef struct
{
  playerTeamStateState_t state;

  int		location;

  int		captures;
  int		basedefense;
  int		carrierdefense;
  int		flagrecovery;
  int		fragcarrier;
  int		assists;

  int		flagsince;
  int		lastreturnedflag;
  int		lastfraggedcarrier;
} playerTeamState_t;

// the auto following clients don't follow a specific client
// number, but instead follow the first two active players
#define FOLLOW_ACTIVE1	-1
#define FOLLOW_ACTIVE2	-2

/// client data that stays across multiple levels or tournament restarts
/// this is achieved by writing all the data to cvar strings at game shutdown
/// time and reading them back at connection time.  Anything added here
/// MUST be dealt with in G_InitSessionData() / G_ReadSessionData() / G_WriteSessionData()
typedef struct
{
  team_t	sessionTeam;
  /// for determining next-in-line to play
  int		spectatorTime;
  spectatorState_t spectatorState;
  /// for chasecam and follow mode
  int		spectatorClient;
  /// tournament stats
  int		wins, losses;
  /// true when this client is a team leader
  bool		teamLeader;
} clientSession_t;

//
//  STATS !
//
// structures to handle stats

// handle a weapon caracteristic
// consolestats assume all fields are 4bits long...
struct gStatWeapon_s
{
  int	fired;
  int	hit;
  int	damagegiven;
  int	damagereceived;
  int	kill;
  int	death;
  int	picked;
  int	dropped;
};
typedef struct gStatWeapon_s gStatWeapon_t;

// TODO : change the names, don't let them start with STAT_
//        it can be confused with STAT_ from bg_public.h
enum item_stat_life
  {
    ITEM_STAT_MH,
    ITEM_STAT_MDH,
    ITEM_STAT_LH,
    ITEM_STAT_TH,
    ITEM_STAT_RA,
    ITEM_STAT_YA,
    ITEM_STAT_LA,
    ITEM_STAT_TA,
    ITEM_STAT_NUM_LIFE
  };

enum item_stat_misc
  {
    STAT_QUAD,
    STAT_REGEN,
    STAT_HASTE,
    STAT_MEDKIT,
    ITEM_STAT_REDFLAG,
    ITEM_STAT_BLUEFLAG,
    ITEM_STAT_NUM_MISC
  };

// to track special items pickup (MH, RA etc...)
struct gStatItem_s
{
  int	totlife;
  int	totarmor;
  enum item_stat_life life[ITEM_STAT_NUM_LIFE];
  enum item_stat_misc other[ITEM_STAT_NUM_MISC];
};
typedef struct gStatItem_s gStatItem_t;

struct gStatItemPowerup_s
{
  int	count;
  int	time;
};
typedef struct gStatItemPowerup_s gStatItemPowerup_t;

// ctf specific player stats
struct gStatPlayerCtf_s
{
  int	picks;
  int	captures;
  int	returns;
  int	flagtime;
  int	defends;
  int	assists;
};
typedef struct gStatPlayerCtf_s gStatPlayerCtf_t;

// tdm specific player stats
struct gStatPlayerTdm_s
{

};
typedef struct gStatPlayerTdm_s gStatPlayerTdm_t;

// handle other player stats
struct gStatPlayer_s
{
  int	totdamage_done;
  int	totdamage_taken; // this includes damage taken from "world", falling etc...
			 // but exclude telefrags and suicides (they are tracked apart)
  int	totkill;
  int	totdeath;
  int	teamkill;
  int	teamdamage;
  int	telefragcounter;
  int	suicidecounter;
  gStatWeapon_t weap[WP_NUM_WEAPONS];
  gStatItem_t items;
  union
  {
    gStatPlayerCtf_t ctf;
    gStatPlayerTdm_t tdm;
  };
  gStatItemPowerup_t powerups[PW_NUM_POWERUPS];
  const gitem_t * lastTakenItem;
};
typedef struct gStatPlayer_s gStatPlayer_t;

// sum of all team players stats
struct gStatTeam_s
{
  gStatWeapon_t weap[WP_NUM_WEAPONS];
  gStatItem_t items;
  gStatItemPowerup_t powerups[PW_NUM_POWERUPS];
};
typedef struct gStatTeam_s gStatTeam_t;

//
#define MAX_NETNAME	36
#define MAX_VOTE_COUNT	3

/// client data that stays across multiple respawns, but is cleared
/// on each level change or team change at ClientBegin()
struct clientPersistant_s
{
  clientConnected_t connected;
  /// we would lose angles if not persistant
  usercmd_t	cmd;
  /// true if "ip" info key is "localhost"
  bool		localClient;
  /// the first spawn should be at a cool location
  bool		initialSpawn;
  ///
  char		netname[MAX_NETNAME];
  /// for handicapping
  int		maxHealth;
  /// level.time the client entered the game
  int		enterTime;
  /// status in teamplay games
  playerTeamState_t teamState;
  /// to prevent people from constantly calling votes
  int		voteCount;
  /// send team overlay updates?
  bool		teamInfo;
  /// client ready status false or true
  bool		readyClient;
};
typedef struct clientPersistant_s clientPersistant_t;


struct gStatLevel_s
{
  enum item_stat_life spawned[ITEM_STAT_NUM_LIFE];
};
typedef struct gStatLevel_s gStatLevel_t;


struct gclienttimers_s
{
  /// timers
  /// can respawn when time > this, force after g_forcerespwan
  int		respawnTime;
  /// kick players when time > this
  int		inactivityTime;
  /// true if the five seoond warning has been given
  bool		inactivityWarning;
  /// clear the EF_AWARD_IMPRESSIVE, etc when time > this
  /// level.time of last usercmd_t, for EF_CONNECTION
  int		lastCmdTime;
  /// for multiple kill rewards
  int		lastKillTime;

  /// time the player switched teams
  int		switchTeamTime;
  int		rewardTime;
};

struct gclientcombat_s
{
  /// sum up damage over an entire frame, so
  /// shotgun blasts give a single big kick

  /// damage absorbed by armor
  int		damage_armor;
  /// damage taken out of health
  int		damage_blood;
  /// impact damage
  int		damage_knockback;
  /// origin for vector calculation
  vec3_t	damage_from;
  /// if true, don't use the damage_from vector
  bool		damage_fromWorld;
  /// for "impressive" reward sound
  int		accurateCount;
  /// total number of shots
  int		accuracy_shots;
  /// total number of hits
  int		accuracy_hits;

  ///
  /// last client that this client killed
  int		lastkilled_client;
  /// last client that damaged this client
  int		lasthurt_client;
  /// type of damage the client did
  int		lasthurt_mod;
};

struct gclientmisc_s
{
  /// wishes to leave the intermission
  bool		readyToExit;

  bool		noclip;

  /// we can't just use pers.lastCommand.time, because
  /// of the g_sycronousclients case
  int		buttons;
  int		oldbuttons;
  int		latched_buttons;

  vec3_t	oldOrigin;

  int		airOutTime;

  /// timeResidual is used to handle events that happen every second
  /// like health / armor countdowns and regeneration
  int		timeResidual;
};

/// this structure is cleared on each ClientSpawn(),
/// except for 'client->pers' and 'client->sess'
struct gclient_s
{
  /// ps MUST be the first element, because the server expects it
  /// communicated by server to clients
  playerState_t		ps;

  /// the rest of the structure is private to game
  /// theses 2 next won't be reset on respawn
  clientPersistant_t	pers;
  clientSession_t	sess;

  /// handle a player stats structure which holds weapon, items infos
  /// not reset on respawn
  gStatPlayer_t		stats;

  /// related to damage calculation and rewards
  struct gclientcombat_s cbt;

  /// timers structure
  struct gclienttimers_s timer;

  /// various things to know about the client
  struct gclientmisc_s	misc;
};
typedef struct gclient_s gclient_t;

// related to the gamemodule runtime
struct game_locals_s
{
  bool			paused;

  /// game time; this is the forwarding of the server time, it won't ever stop
  int			time;
  int			previousTime;
  int			framenum;
  int			msec;

  /// game.time when the timeout was called
  int			timeoutTime;
  /// game.time we gonna resume the party
  int			timeinTime;
  /// this is used to inform clients about the timeout status
  int			timeoutAnnoncerTime;
};
typedef struct game_locals_s game_locals_t;

// match structure
// everything related to the ingame handling should use these variables
// each game can be split into (1)warmup/livematch/endmatch -> (2)warmup/...

enum game_match_flags_e
  {
    END_NONE = 0,
    END_CALLED,
    END_INTERMISSION,
    END_PROCEED,
    END_EXIT
  };
typedef enum game_match_flags_e game_match_flags_t;

struct game_match_s
{
  enum game_match_flags_e end_flag;
  int end_time;
};
typedef struct game_match_s game_match_t;

// internal ctf handling structure
struct teamgame_s
{
  flagStatus_t redStatus;	// CTF
  flagStatus_t blueStatus;	// CTF
  int redTakenSoundTime;
  int blueTakenSoundTime;
  int redLastPickTime;
  int blueLastPickTime;
  int redLastBasePickTime;
  int blueLastBasePickTime;
};
typedef struct teamgame_s teamgame_t;


#define MAX_SPAWN_VARS		64
#define MAX_SPAWN_VARS_CHARS	4096
#define BODY_QUEUE_SIZE		8

///
/// this structure is cleared as each map is entered
///
struct level_locals_s
{
  /// [maxclients]
  struct gclient_s *	clients;

  struct gentity_s *	gentities;
  int			gentitySize;
  /// current number, <= MAX_GENTITIES
  int			num_entities;
  /// restart match at this time
  int			warmupTime;

  fileHandle_t		logFile;

  /// store latched cvars here that we want to get at often
  int			maxclients;

  int			framenum;
  /// in msec, this one may be paused, it reflect the state of the simulated world
  int			time;
  /// so movers can back up when blocked
  int			previousTime;

  int			teamScores[TEAM_NUM_TEAMS];
  char			teamNames[2][MAX_NETNAME]; // team1(red) and team2(blue) names
  gStatTeam_t		teamStatsRed;
  gStatTeam_t		teamStatsBlue;

  /// last time of client team location update
  int			lastTeamLocationTime;
  /// don't use any old session data, because
  bool			newSession;
  /// we changed gametype
  /// waiting for a map_restart to fire
  bool			restarted;

  int			numConnectedClients;
  /// includes connecting clients
  int			numNonSpectatorClients;
  /// connected, non-spectators
  int			numPlayingClients;
  /// sorted by score
  int			sortedClients[MAX_CLIENTS];
  /// clientNums for auto-follow spectators
  int			follow1, follow2;
  /// sound index for standing in lava
  int			snd_fry;
  /// for detecting if g_warmup is changed
  int			warmupModificationCount;

  /// match status (live, warmup, ready, notready, pause ? etc..)
  int			matchStatus;
  /// for time extention overtime mode
  int			numberovertime;
  /// display the overtime message once when entering suddendeath
  bool			overtimeDisplayed;

  /// the game won't allow clients to fire (like during countdown ?)
  bool			noWeaponFire;
  /// don't register anymore damages
  /// TODO : need cleaning with intermission with might be diplicated in some cases
  bool			noDamage;

  /// level stats (items spawns)
  struct gStatLevel_s	stats;

  /// voting state
  char			voteString[MAX_STRING_CHARS];
  char			voteDisplayString[MAX_STRING_CHARS];
  /// level.time vote was called
  int			voteTime;
  /// time the vote is executed
  int			voteExecuteTime;
  int			voteYes;
  int			voteNo;
  /// set by CalculateRanks
  int			numVotingClients;

  /// spawn variables
  /// the G_Spawn*() functions are valid
  bool			spawning;
  int			numSpawnVars;
  /// key / value pairs
  char *		spawnVars[MAX_SPAWN_VARS][2];
  int			numSpawnVarChars;
  char			spawnVarChars[MAX_SPAWN_VARS_CHARS];

  struct game_match_s	match;
  /// intermission state
  /// intermission was qualified, but
  /// wait INTERMISSION_DELAY_TIME before
  /// actually going there so the last
  /// frag can be watched.  Disable future
  /// kills during this delay
  //int			intermissionQueued;
  /// time the intermission was started
  //int			intermissiontime;
  char *		changemap;
  /// at least one client wants to exit
  bool			readyToExit;
  int			exitTime;
  /// also used for spectator spawns
  vec3_t		intermission_origin;
  vec3_t		intermission_angle;
  /// target_locations get linked
  bool			locationLinked;
  /// head of the location list
  gentity_t *		locationHead;
  /// dead bodies
  int			bodyQueIndex;
  gentity_t *		bodyQue[BODY_QUEUE_SIZE];
  /// keep track of the highest connected client number
  int			clientHighestID;
};
typedef struct level_locals_s level_locals_t;
// trying with a shorter name
typedef struct level_locals_s lvl_t;

/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

// ======================================

// later in this file
void Team_SetFlagStatus (teamgame_t * teamgame, team_t team, flagStatus_t status);

/*
==============
Team_InitGame
==============
*/
void
Team_InitGame (teamgame_t * teamgame)
{
  // reset teamgame structure
  Com_Memset (&(*teamgame), 0, sizeof (*teamgame));

  // and set default values
  switch (g_gametype->integer)
    {
      // nothing to do for tdm
    case GT_TEAM:
      break;
    case GT_CTF:
      teamgame->redStatus = -1; // Invalid to force update
      Team_SetFlagStatus (teamgame, TEAM_RED, FLAG_ATBASE);
      teamgame->blueStatus = -1; // Invalid to force update
      Team_SetFlagStatus (teamgame, TEAM_BLUE, FLAG_ATBASE);
      break;
    case GT_ARENA:
      break;
    default:
      break;
    }
}

/*
======================
Some utility functions
to ease printing
======================
*/
team_t
OtherTeam (team_t team)
{
  if (team == TEAM_RED)
    return TEAM_BLUE;
  else if (team == TEAM_BLUE)
    return TEAM_RED;
  return team;
}

const char *
TeamName (team_t team)
{
  if (team == TEAM_RED)
    return "RED";
  else if (team == TEAM_BLUE)
    return "BLUE";
  else if (team == TEAM_SPECTATOR)
    return "SPECTATOR";
  return "FREE";
}

const char *
OtherTeamName (team_t team)
{
  if (team == TEAM_RED)
    return "BLUE";
  else if (team == TEAM_BLUE)
    return "RED";
  else if (team == TEAM_SPECTATOR)
    return "SPECTATOR";
  return "FREE";
}

const char *
TeamColorString (team_t team)
{
  if (team == TEAM_RED)
    return S_COLOR_RED;
  else if (team == TEAM_BLUE)
    return S_COLOR_BLUE;
  else if (team == TEAM_SPECTATOR)
    return S_COLOR_YELLOW;
  return S_COLOR_WHITE;
}

const char *
OtherTeamColorString (team_t team)
{
  if (team == TEAM_RED)
    return S_COLOR_BLUE;
  else if (team == TEAM_BLUE)
    return S_COLOR_RED;
  else if (team == TEAM_SPECTATOR)
    return S_COLOR_YELLOW;
  return S_COLOR_WHITE;
}

// ============================================

// NULL for everyone
void
PrintMsg (gentity_t * ent, const char *fmt, ...)
{
  char msg[1024];
  va_list argptr;
  char *p;

  va_start (argptr, fmt);
  if ((unsigned int) Q_vsnprintf (msg, sizeof (msg), fmt, argptr) >
      sizeof (msg))
    {
      G_Error ("PrintMsg overrun");
    }
  va_end (argptr);

  // double quotes are bad
  while ((p = strchr (msg, '"')) != NULL)
    *p = '\'';

  gi->SendServerCommand (((ent == NULL) ? -1 : ent - g_entities),
			 va ("print \"%s\"", msg));
}

/*
==============
AddTeamScore

 used for gametype > GT_TEAM
 for gametype GT_TEAM the level.teamScores is updated in AddScore in g_combat.c
==============
*/
void
AddTeamScore (vec3_t origin, team_t team, int score)
{
  gentity_t *te;

  te = G_TempEntity (origin, EV_GLOBAL_TEAM_SOUND);
  te->sh.r.svFlags |= SVF_BROADCAST;

  if (team == TEAM_RED)
    {
      if ((level.teamScores[TEAM_RED] + score) == level.teamScores[TEAM_BLUE])
	{
	  //teams are tied sound
	  te->sh.s.eventParm = GTS_TEAMS_ARE_TIED;
	}
      else if ((level.teamScores[TEAM_RED] <= level.teamScores[TEAM_BLUE])
	       && ((level.teamScores[TEAM_RED] + score) > level.teamScores[TEAM_BLUE]))
	{
	  // red took the lead sound
	  te->sh.s.eventParm = GTS_REDTEAM_TOOK_LEAD;
	}
      else
	{
	  // red scored sound
	  te->sh.s.eventParm = GTS_REDTEAM_SCORED;
	}
    }
  else
    {
      if ((level.teamScores[TEAM_BLUE] + score) == level.teamScores[TEAM_RED])
	{
	  //teams are tied sound
	  te->sh.s.eventParm = GTS_TEAMS_ARE_TIED;
	}
      else if ((level.teamScores[TEAM_BLUE] <= level.teamScores[TEAM_RED])
	       && ((level.teamScores[TEAM_BLUE] + score) > level.teamScores[TEAM_RED]))
	{
	  // blue took the lead sound
	  te->sh.s.eventParm = GTS_BLUETEAM_TOOK_LEAD;
	}
      else
	{
	  // blue scored sound
	  te->sh.s.eventParm = GTS_BLUETEAM_SCORED;
	}
    }

  level.teamScores[team] += score;
}

/*
==============
OnSameTeam
==============
*/
bool
OnSameTeam (gentity_t * ent1, gentity_t * ent2)
{
  if (!ent1->client || !ent2->client)
    {
      return false;
    }

  if (!BG_GametypeIsTeam (g_gametype->integer))
    {
      return false;
    }

  if (ent1->client->sess.sessionTeam == ent2->client->sess.sessionTeam)
    {
      return true;
    }

  return false;
}

/*
===================
Team_SetFlagStatus
===================
*/
// just some stupid offset to avoid adding the '\0' char
// anyvalue different from 0 is Ok
// it must be the same as in cg_servercmds.c
static char flagStatusOffsetTrick = '0';

void
Team_SetFlagStatus (teamgame_t * teamgame, team_t team, flagStatus_t status)
{
  bool modified = false;

  switch (team)
    {
    case TEAM_RED:		// CTF
      if (teamgame->redStatus != status)
	{
	  teamgame->redStatus = status;
	  modified = true;
	}
      break;

    case TEAM_BLUE:		// CTF
      if (teamgame->blueStatus != status)
	{
	  teamgame->blueStatus = status;
	  modified = true;
	}
      break;
    default:
      break;
    }

  if (modified)
    {
      char st[3];

      st[0] = (char)teamgame->redStatus + flagStatusOffsetTrick;
      st[1] = (char)teamgame->blueStatus + flagStatusOffsetTrick;
      st[2] = '\0';

      gi->SetConfigstring (CS_FLAGSTATUS, st);
    }
}

/*
=====================
Team_CheckDroppedItem
=====================
*/
void
Team_CheckDroppedItem (teamgame_t * teamgame, gentity_t * dropped)
{
  if (dropped->item->giTag == PW_REDFLAG)
    {
      Team_SetFlagStatus (teamgame, TEAM_RED, FLAG_DROPPED);
    }
  else if (dropped->item->giTag == PW_BLUEFLAG)
    {
      Team_SetFlagStatus (teamgame, TEAM_BLUE, FLAG_DROPPED);
    }
}

/*
=================
Team_ForceGesture

forcing players in the given team to "gesture"
=================
*/
void
Team_ForceGesture (team_t team)
{
  int i;
  gentity_t *ent;

  for (i = 0; i < MAX_CLIENTS; i++)
    {
      ent = &g_entities[i];

      if (!ent->inuse)
	continue;

      if (!ent->client)
	continue;

      if (ent->client->sess.sessionTeam != team)
	continue;

      ent->flags |= FL_FORCE_GESTURE;
    }
}

/*
================
Team_FragBonuses

Calculate the bonuses for flag defense, flag carrier defense, etc.
Note that bonuses are not cumulative. You get one, they are in importance
order.
================
*/
void
Team_FragBonuses (gentity_t * targ, gentity_t * inflictor,
		  gentity_t * attacker)
{
  int i;
  int flag_pw, enemy_flag_pw;
  gentity_t *flag, *carrier = NULL;
  char *c;
  vec3_t v1, v2;
  team_t team;
  //team_t otherteam;
  char *msg;

  // no bonus for fragging yourself or team mates
  if ((targ->client == NULL) || (attacker->client == NULL)
      || (targ == attacker) || OnSameTeam (targ, attacker))
    return;

  team = targ->client->sess.sessionTeam;
  //otherteam = OtherTeam (targ->client->sess.sessionTeam);

  // same team, if the flag at base, check to he has the enemy flag
  if (team == TEAM_RED)
    {
      flag_pw = PW_REDFLAG;
      enemy_flag_pw = PW_BLUEFLAG;
    }
  else
    {
      flag_pw = PW_BLUEFLAG;
      enemy_flag_pw = PW_REDFLAG;
    }

  // did the attacker frag the flag carrier?
  if (targ->client->ps.powerups[enemy_flag_pw])
    {
      attacker->client->pers.teamState.lastfraggedcarrier = level.time;
      AddScore (attacker, targ->sh.r.currentOrigin, CTF_FRAG_CARRIER_BONUS);
      attacker->client->pers.teamState.fragcarrier++;

      // broadcast to everyone
      msg = va ("print \"%s" S_COLOR_WHITE
		" fragged %s%s" S_COLOR_WHITE "'s flag carrier!\n\"",
		attacker->client->pers.netname,
		TeamColorString (team), TeamName (team));
      gi->SendServerCommand (-1, msg);

      return;
    }

  // nothing more to do if we team killed
  if (attacker->client->sess.sessionTeam != targ->client->sess.sessionTeam)
    return;

  // check whether we killed near the flag spot; defended the flag
  // find the flag
  switch (attacker->client->sess.sessionTeam)
    {
    case TEAM_RED:
      c = "team_CTF_redflag";
      break;
    case TEAM_BLUE:
      c = "team_CTF_blueflag";
      break;
    default:
      return;
    }

  flag = NULL;
  while ((flag = G_Find (flag, FOFS (classname), c)) != NULL)
    {
      if (!(flag->flags & FL_DROPPED_ITEM))
	break;
    }

  if (!flag)
    return;	// can't find team attacker's flag

  // check to see if we are defending the base's flag
  VectorSubtract (targ->sh.r.currentOrigin, flag->sh.r.currentOrigin, v1);
  VectorSubtract (attacker->sh.r.currentOrigin, flag->sh.r.currentOrigin, v2);

  // not on the same team;
  // one of both is in a radius near the flag spot and can see it
  bool see1, see2;

  // target was near the flag
  see1 = ((VectorLength (v1) < CTF_TARGET_PROTECT_RADIUS)
	  && gi->InPVS (flag->sh.r.currentOrigin, targ->sh.r.currentOrigin));

  // attacker was near the flag
  see2 = ((VectorLength (v2) < CTF_TARGET_PROTECT_RADIUS)
	  && gi->InPVS (flag->sh.r.currentOrigin, attacker->sh.r.currentOrigin));

  if (see1 || see2)
    {
      // we defended the base flag
      AddScore (attacker, targ->sh.r.currentOrigin, CTF_FLAG_DEFENSE_BONUS);
      attacker->client->pers.teamState.basedefense++;

      attacker->client->stats.ctf.defends++;
      attacker->client->ps.persistant[PERS_DEFEND_COUNT]++;
      // add the sprite over the player's head
      attacker->client->ps.eFlags &= EF_CLEAR_AWARDS;
      attacker->client->ps.eFlags |= EF_AWARD_DEFEND;
      attacker->client->timer.rewardTime = level.time + REWARD_SPRITE_TIME;

      return;
    }

  // check if we are protecting the flag carrier
  // find attacker's team's flag carrier
  for (i = 0; i <= level.clientHighestID; i++)
    {
      carrier = g_entities + i;
      if (!carrier->inuse)
	continue;

      if (carrier->client->ps.powerups[flag_pw])
	break;

      carrier = NULL;
    }

  if ((carrier != NULL) && (carrier != attacker))
    {
      VectorSubtract (targ->sh.r.currentOrigin, carrier->sh.r.currentOrigin, v1);
      VectorSubtract (attacker->sh.r.currentOrigin, carrier->sh.r.currentOrigin, v2);

      // target was near the carrier
      see1 = ((VectorLength (v1) < CTF_ATTACKER_PROTECT_RADIUS)
	      && gi->InPVS (carrier->sh.r.currentOrigin, targ->sh.r.currentOrigin));

      // attacker was near the carrier
      see2 = ((VectorLength (v2) < CTF_ATTACKER_PROTECT_RADIUS)
	      && gi->InPVS (carrier->sh.r.currentOrigin, attacker->sh.r.currentOrigin));

      if (see1 || see2)
	{
	  // defended the flag carrier
	  AddScore (attacker, targ->sh.r.currentOrigin, CTF_CARRIER_PROTECT_BONUS);
	  attacker->client->pers.teamState.carrierdefense++;

	  attacker->client->stats.ctf.defends++;
	  attacker->client->ps.persistant[PERS_DEFEND_COUNT]++;
	  // add the sprite over the player's head
	  attacker->client->ps.eFlags &= EF_CLEAR_AWARDS;
	  attacker->client->ps.eFlags |= EF_AWARD_DEFEND;
	  attacker->client->timer.rewardTime = level.time + REWARD_SPRITE_TIME;

	  return;
	}
    }
}

/*
==============
Team_ResetFlag
==============
*/
gentity_t *
Team_ResetFlag (teamgame_t * teamgame, team_t team)
{
  char *c;
  gentity_t *ent, *rent;

  switch (team)
    {
    case TEAM_RED:
      c = "team_CTF_redflag";
      break;
    case TEAM_BLUE:
      c = "team_CTF_blueflag";
      break;
    default:
      return NULL;
    }

  // this is done in 2 passes
  // first we want to find the current dropped flag (if there is)
  // and free the entity.
  // then we will find the flag spot and respawn the item on the spot
  ent = NULL;
  rent = NULL;
  while ((ent = G_Find (ent, FOFS (classname), c)) != NULL)
    {
      if (ent->flags & FL_DROPPED_ITEM)
	G_FreeEntity (ent);
      else
	{
	  rent = ent;
	  RespawnItem (ent);
	}
    }

  Team_SetFlagStatus (teamgame, team, FLAG_ATBASE);

  return rent;
}

/*
===============
Team_ResetFlags
===============
*/
void
Team_ResetFlags (teamgame_t * teamgame)
{
  if (g_gametype->integer == GT_CTF)
    {
      Team_ResetFlag (teamgame, TEAM_RED);
      Team_ResetFlag (teamgame, TEAM_BLUE);
    }
}

/*
====================
Team_ReturnFlagSound
====================
*/
void
Team_ReturnFlagSound (gentity_t * ent, team_t team)
{
  gentity_t *te;

  if (ent == NULL)
    {
      G_Printf ("Warning:  NULL passed to Team_ReturnFlagSound\n");
      return;
    }

  te = G_TempEntity (ent->sh.s.pos.trBase, EV_GLOBAL_TEAM_SOUND);
  if (team == TEAM_BLUE)
    {
      te->sh.s.eventParm = GTS_RED_RETURN;
    }
  else
    {
      te->sh.s.eventParm = GTS_BLUE_RETURN;
    }

  te->sh.r.svFlags |= SVF_BROADCAST;
}

/*
==================
Team_TakeFlagSound
==================
*/
void
Team_TakeFlagSound (teamgame_t * teamgame, gentity_t * ent, team_t team)
{
  gentity_t *te;

  if (ent == NULL)
    {
      G_Printf ("Warning:  NULL passed to Team_TakeFlagSound\n");
      return;
    }

  // only play sound when the flag was at the base
  // or not picked up the last 10 seconds
  switch (team)
    {
    case TEAM_RED:
      if ((teamgame->blueStatus != FLAG_ATBASE)
	  && (teamgame->blueTakenSoundTime + 10000 > level.time))
	{
	  return;
	}
      teamgame->blueTakenSoundTime = level.time;
      break;

    case TEAM_BLUE:
      if ((teamgame->redStatus != FLAG_ATBASE)
	  && (teamgame->redTakenSoundTime + 10000 > level.time))
	{
	  return;
	}
      teamgame->redTakenSoundTime = level.time;
      break;

    default:
      break;
    }

  te = G_TempEntity (ent->sh.s.pos.trBase, EV_GLOBAL_TEAM_SOUND);
  if (team == TEAM_BLUE)
    {
      te->sh.s.eventParm = GTS_RED_TAKEN;
    }
  else
    {
      te->sh.s.eventParm = GTS_BLUE_TAKEN;
    }

  te->sh.r.svFlags |= SVF_BROADCAST;
}
/*
=====================
Team_CaptureFlagSound
=====================
*/
void
Team_CaptureFlagSound (gentity_t * ent, team_t team)
{
  gentity_t *te;

  if (ent == NULL)
    {
      G_Printf ("Warning:  NULL passed to Team_CaptureFlagSound\n");
      return;
    }

  te = G_TempEntity (ent->sh.s.pos.trBase, EV_GLOBAL_TEAM_SOUND);
  if (team == TEAM_BLUE)
    {
      te->sh.s.eventParm = GTS_BLUE_CAPTURE;
    }
  else
    {
      te->sh.s.eventParm = GTS_RED_CAPTURE;
    }

  te->sh.r.svFlags |= SVF_BROADCAST;
}

/*
===============
Team_ReturnFlag
===============
*/
void
Team_ReturnFlag (teamgame_t * teamgame, team_t team)
{
  Team_ReturnFlagSound (Team_ResetFlag (teamgame, team), team);

  PrintMsg (NULL, "The %s%s" S_COLOR_WHITE " flag has returned!\n",
	    TeamColorString (team), TeamName (team));
}

/*
===============
Team_FreeEntity
===============
*/
void
Team_FreeEntity (teamgame_t * teamgame, gentity_t * ent)
{
  if (ent->item->giTag == PW_REDFLAG)
    {
      Team_ReturnFlag (teamgame, TEAM_RED);
    }
  else if (ent->item->giTag == PW_BLUEFLAG)
    {
      Team_ReturnFlag (teamgame, TEAM_BLUE);
    }
}

/*
==============
Team_DroppedFlagThink

Automatically set in Launch_Item if the item is one of the flags

Flags are unique in that if they are dropped, the base flag must be respawned when they time out
==============
*/
void
Team_DroppedFlagThink (teamgame_t * teamgame, gentity_t * ent)
{
  team_t team;

  if (ent->item->giTag == PW_REDFLAG)
    {
      team = TEAM_RED;
    }
  else if (ent->item->giTag == PW_BLUEFLAG)
    {
      team = TEAM_BLUE;
    }
  else
    {
      G_Printf ("Error : team_droppedflagthink(), no flag team found");
      return;
    }

  // Reset Flag will delete this entity
  Team_ReturnFlagSound (Team_ResetFlag (teamgame, team), team);
}

/*
==============
Team_TouchOurFlag

Everything performed when touching the team flag
ent : flag entity
other : player entity
team : the flag's color
==============
*/
int
Team_TouchOurFlag (teamgame_t * teamgame, gentity_t * ent, gentity_t * other, team_t team)
{
  int i;
  gentity_t *player;
  gclient_t *cl = other->client;
  powerup_t enemy_flag;
  int capturetime;
  int capture_ms;
  int totaltime;
  int total_ms;

  if (cl->sess.sessionTeam == TEAM_RED)
    {
      enemy_flag = PW_BLUEFLAG;
    }
  else
    {
      enemy_flag = PW_REDFLAG;
    }

  // touched the missing flag
  // hey, it's not home. return it by teleporting it back
  if (ent->flags & FL_DROPPED_ITEM)
    {
      int time, ms;

      if (team == TEAM_RED)
	{
	  time = level.time - teamgame->redLastBasePickTime;
	  ms = time - (time / 1000) * 1000;
	  time = (time / 1000);
	}
      else
	{
	  time = level.time - teamgame->blueLastBasePickTime;
	  ms = time - (time / 1000) * 1000;
	  time = (time / 1000);
	}

      // forward a message to all players
      PrintMsg (NULL, "%s" S_COLOR_WHITE " returned the %s%s"
		S_COLOR_WHITE " flag (%d.%03d)!\n",
		cl->pers.netname, TeamColorString (team),
		TeamName (team), time, ms);

      // updating scores and stats
      AddScore (other, ent->sh.r.currentOrigin, CTF_RECOVERY_BONUS);
      other->client->pers.teamState.flagrecovery++;
      other->client->pers.teamState.lastreturnedflag = level.time;

      // updating player stats
      other->client->stats.ctf.returns++;

      //ResetFlag will remove this entity! We must return zero
      Team_ReturnFlagSound (Team_ResetFlag (teamgame, team), team);
      return 0;
    }

  // the flag is at home base. if the player has the enemy
  // flag, he's just won!
  if (!cl->ps.powerups[enemy_flag])
    return 0;		// We don't have the flag

  // we have the enemy flag and touched the home flag
  // the player just scored !
  // calculating run times
  if (enemy_flag == PW_BLUEFLAG)
   {
      capturetime = level.time - teamgame->blueLastPickTime;
      capture_ms = capturetime - (capturetime / 1000) * 1000;
      totaltime = level.time - teamgame->blueLastBasePickTime;
      total_ms = totaltime - (totaltime / 1000) * 1000;
    }
  else if (enemy_flag == PW_REDFLAG)
    {
      capturetime = level.time - teamgame->redLastPickTime;
      capture_ms = capturetime - (capturetime / 1000) * 1000;
      totaltime = level.time - teamgame->redLastBasePickTime;
      total_ms = totaltime - (totaltime / 1000) * 1000;
    }

  // updating total flagtime for the carrier
  cl->stats.ctf.flagtime += level.time - cl->pers.teamState.flagsince;

  // if the runtime is different from total time (flag was dropped)
  // print the flag capture announce with run times
  if (totaltime != capturetime)
    {
      PrintMsg (NULL, "%s" S_COLOR_WHITE " captured the %s%s" S_COLOR_WHITE
		" flag " S_COLOR_GREEN "(%d.%03d) " S_COLOR_ORANGE2
		"(total %d.%03d)" S_COLOR_WHITE "!\n",
		cl->pers.netname, OtherTeamColorString (team),
		TeamName (OtherTeam (team)),
		(capturetime / 1000), capture_ms, (totaltime / 1000), total_ms);
    }
  else
    {
      PrintMsg (NULL, "%s" S_COLOR_WHITE " captured the %s%s" S_COLOR_WHITE
		" flag " S_COLOR_GREEN "(%d.%03d)" S_COLOR_WHITE "!\n",
		cl->pers.netname, OtherTeamColorString (team),
		TeamName (OtherTeam (team)),
		(capturetime / 1000), capture_ms);
    }

  // Increase the team's score
  AddTeamScore (ent->sh.s.pos.trBase, other->client->sess.sessionTeam, 1);

  // and force players to gesture
  Team_ForceGesture (other->client->sess.sessionTeam);

  // update the capturer stats
  other->client->pers.teamState.captures++;
  other->client->stats.ctf.captures++;

  // add the sprite over the player's head
  other->client->ps.eFlags &= EF_CLEAR_AWARDS;
  other->client->ps.eFlags |= EF_AWARD_CAP;
  other->client->timer.rewardTime = level.time + REWARD_SPRITE_TIME;
  other->client->ps.persistant[PERS_CAPTURES]++;

  // other gets another 10 frag bonus
  AddScore (other, ent->sh.r.currentOrigin, CTF_CAPTURE_BONUS);

  // make capture sound announce
  Team_CaptureFlagSound (ent, team);

  // Ok, let's do the player loop, hand out the bonuses
  for (i = 0; i <= level.clientHighestID; i++)
    {
      player = &g_entities[i];

      // also make sure we don't award assist bonuses to the flag carrier himself.
      if (!player->inuse || (player == other))
	continue;

      // only to teamates
      if (player->client->sess.sessionTeam == cl->sess.sessionTeam)
	{
	  // team bonus to all team members
	  if (player != other)
	    AddScore (player, ent->sh.r.currentOrigin, CTF_TEAM_BONUS);

	  // award extra points for capture assists
	  // if a teamate returned the base flag recently
	  if ((player->client->pers.teamState.lastreturnedflag +
	       CTF_RETURN_FLAG_ASSIST_TIMEOUT) > level.time)
	    {
	      // give some assist points
	      AddScore (player, ent->sh.r.currentOrigin,
			CTF_RETURN_FLAG_ASSIST_BONUS);

	      // updating internal stats structure
	      player->client->pers.teamState.assists++;
	      player->client->stats.ctf.assists++;

	      player->client->ps.persistant[PERS_ASSIST_COUNT]++;

	      // add the sprite over the player's head
	      player->client->ps.eFlags &= EF_CLEAR_AWARDS;
	      player->client->ps.eFlags |= EF_AWARD_ASSIST;
	      player->client->timer.rewardTime = level.time + REWARD_SPRITE_TIME;
	    }
	  // if a teamate killed the enemy flag carrier recently
	  else if ((player->client->pers.teamState.lastfraggedcarrier +
		    CTF_FRAG_CARRIER_ASSIST_TIMEOUT) > level.time)
	    {
	      // add some points
	      AddScore (player, ent->sh.r.currentOrigin,
			CTF_FRAG_CARRIER_ASSIST_BONUS);

	      // update stats
	      player->client->pers.teamState.assists++;
	      player->client->stats.ctf.assists++;

	      player->client->ps.persistant[PERS_ASSIST_COUNT]++;

	      // add the sprite over the player's head
	      player->client->ps.eFlags &= EF_CLEAR_AWARDS;
	      player->client->ps.eFlags |= EF_AWARD_ASSIST;
	      player->client->timer.rewardTime = level.time + REWARD_SPRITE_TIME;
	    }
	}
    }

  // player doesn't have the enemy flag anymore
  cl->ps.powerups[enemy_flag] = 0;

  // respawn it in its base
  Team_ResetFlag (teamgame, OtherTeam (team));

  // update players ranking
  CalculateRanks ();

  return 0;	// Do not respawn the flag automatically
}

/*
==============
Team_TouchEnemyFlag

ent : the flag entity
other : the player
team : the flag's color
==============
*/
int
Team_TouchEnemyFlag (teamgame_t * teamgame, gentity_t * ent, gentity_t * other, team_t team)
{
  bool flagwasbase = false; // track if it was picked from its slot
  gclient_t *cl = other->client;

  // this is pretty easy to handle, if a player touches the enemy flag,
  // it just takes it and the game makes an announce.
  PrintMsg (NULL, "%s" S_COLOR_WHITE " got the %s%s"
	    S_COLOR_WHITE " flag!\n",
	    other->client->pers.netname, TeamColorString (team), TeamName (team));

  // player carries the flag
  if (team == TEAM_RED)
    cl->ps.powerups[PW_REDFLAG] = INT_MAX;	// flags never expire
  else
    cl->ps.powerups[PW_BLUEFLAG] = INT_MAX;	// flags never expire

  // start counters to know when the flag was picked
  // if the flag was in base start the basepick counter
  if (team == TEAM_RED)
    {
      teamgame->redLastPickTime = level.time;
      cl->stats.ctf.picks++;

      if (teamgame->redStatus == FLAG_ATBASE)
	{
	  teamgame->redLastBasePickTime = level.time;
	  flagwasbase = true;
	}
    }
  else
    {
      teamgame->blueLastPickTime = level.time;
      cl->stats.ctf.picks++;

      if (teamgame->blueStatus == FLAG_ATBASE)
	{
	  teamgame->blueLastBasePickTime = level.time;
	  flagwasbase = true;
	}
    }

  // update player flag info
  cl->pers.teamState.flagsince = level.time;

  // give some points to the attacker if the flag was base
  // avoid getting points from dropping/picking combination
  if (flagwasbase)
    AddScore (other, ent->sh.r.currentOrigin, CTF_FLAG_BONUS);

  // set the flag status and update the configstring
  Team_SetFlagStatus (teamgame, team, FLAG_TAKEN);

  // make some nasty sound announce
  Team_TakeFlagSound (teamgame, ent, team);

  // Do not respawn this automatically, but do delete it if it was FL_DROPPED
  return -1;
}

/*
=============
Pickup_Team

ent is the item entity
other is the client entity
=============
*/
int
Pickup_Team (teamgame_t * teamgame, gentity_t * ent, gentity_t * other)
{
  team_t team;
  gclient_t *cl = other->client;

  // figure out what team this flag is
  if (!strcmp (ent->classname, "team_CTF_redflag"))
    {
      team = TEAM_RED;
    }
  else if (!strcmp (ent->classname, "team_CTF_blueflag"))
    {
      team = TEAM_BLUE;
    }
  else
    {
      PrintMsg (other, "Don't know what team the flag is on.\n");
      return 0;
    }

  // GT_CTF
  if (team == cl->sess.sessionTeam)
    {
      return Team_TouchOurFlag (teamgame, ent, other, team);
    }
  else
    {
      return Team_TouchEnemyFlag (teamgame, ent, other, team);
    }
}

/*
===========
Team_GetLocation

Report a location for the player. Uses placed nearby target_location entities
============
*/
static gentity_t *
Team_GetLocation (gentity_t * ent)
{
  gentity_t *eloc, *best;
  float bestlen, len;
  vec3_t origin;

  best = NULL;
  bestlen = 3 * 8192.0 * 8192.0;

  VectorCopy (ent->sh.r.currentOrigin, origin);

  for (eloc = level.locationHead; eloc; eloc = eloc->nextTrain)
    {
      // get vector length
      len = DistanceSquared (eloc->sh.r.currentOrigin, origin);

      if (len > bestlen)
	{
	  continue;
	}

      // the player must be in the same area than the location entity
      if (!gi->InPVS (origin, eloc->sh.r.currentOrigin))
	{
	  continue;
	}

      bestlen = len;
      best = eloc;
    }

  return best;
}

/*
===========
Team_GetLocationMsg

Report a location for the player. Uses placed nearby target_location entities
============
*/
bool
Team_GetLocationMsg (gentity_t * ent, char *loc, int loclen)
{
  gentity_t *best;
  char color;

  // grab the nearest connected location point
  best = Team_GetLocation (ent);

  if (best == NULL)
    return false;

  // NOTE bu : the location coloring really needs to be reworked
  // normally colors could be put in the location message ?
  // so that we can display multi colored string.

  // the count field represents a color
  if (best->count)
    {
      // get a valid ColorIndex argument
      color = best->count % MAX_DEFINED_COLORS;

      Com_sprintf (loc, loclen, "%c%c%s" S_COLOR_WHITE, Q_COLOR_ESCAPE,
		   ColorIndexToChar (color), best->message);
    }
  else
    Com_sprintf (loc, loclen, "%s", best->message);

  return true;
}

/*---------------------------------------------------------------------------*/
/*
================
SelectRandomTeamSpawnPoint

go to a random point that doesn't telefrag
================
*/
// is this defined somewhere else ?
#define MAX_TEAM_SPAWN_POINTS 32

static gentity_t *
SelectRandomTeamSpawnPoint (int teamstate, team_t team, vec3_t origin,
			    vec3_t angles, bool isBot)
{
  gentity_t *spot;
  int count;
  int selection;
  gentity_t *spots[MAX_TEAM_SPAWN_POINTS];
  char *classname;

  if (teamstate == TEAM_BEGIN)
    {
      if (team == TEAM_RED)
	classname = "team_CTF_redplayer";
      else if (team == TEAM_BLUE)
	classname = "team_CTF_blueplayer";
      else
	return NULL;
    }
  else
    {
      if (team == TEAM_RED)
	classname = "team_CTF_redspawn";
      else if (team == TEAM_BLUE)
	classname = "team_CTF_bluespawn";
      else
	return NULL;
    }
  count = 0;

  spot = NULL;

  while ((spot = G_Find (spot, FOFS (classname), classname)) != NULL)
    {
      // avoid spawn telefragging
      if (SpotWouldTelefrag (spot))
	{
	  continue;
	}

      if (((spot->flags & FL_NO_BOTS) && isBot)
	  || ((spot->flags & FL_NO_HUMANS) && !isBot))
	{
	  // spot is not for this human/bot player
	  continue;
	}

      spots[count] = spot;
      if (++count == MAX_TEAM_SPAWN_POINTS)
	break;
    }

  if (!count)
    {
      // no spots that won't telefrag
      return NULL;
    }

  selection = rand () % count;

  // fill the origin and angles field
  VectorCopy (spots[selection]->sh.s.origin, origin);
  VectorCopy (spots[selection]->sh.s.angles, angles);

  return spots[selection];
}

/*
===========
SelectCTFSpawnPoint

============
*/
gentity_t *
SelectCTFSpawnPoint (team_t team, int teamstate, vec3_t origin,
		     vec3_t angles, bool isBot)
{
  gentity_t *spot;

  spot = SelectRandomTeamSpawnPoint (teamstate, team, origin, angles, isBot);

  // backup on any valid spawn point
  if (spot == NULL)
    {
      return SelectSpawnPoint (vec3_origin, origin, angles, isBot);
    }

  return spot;
}

/*---------------------------------------------------------------------------*/

/*
==================
TeamplayLocationsMessage

Format:
	clientNum location health armor weapon powerups

==================
*/
static void
TeamplayInfoMessage (gentity_t * ent)
{
  char entry[MAX_STRING_CHARS];
  char string[MAX_STRING_CHARS];
  unsigned int stringlength;
  int i, j;
  gentity_t *player;
  int cnt, num;
  int h, a;
  int clients[TEAM_MAXOVERLAY];

  // the players doesn't ask teaminfo
  if (!ent->client->pers.teamInfo)
    return;

  // figure out what client should be on the display
  // we are limited to 8, but we want to use the top eight players
  // but in client order (so they don't keep changing position on the overlay)
  for (i = 0, cnt = 0; (i <= level.clientHighestID) && (cnt < TEAM_MAXOVERLAY); i++)
    {
      player = g_entities + level.sortedClients[i];

      // valid player and in the same team
      if (player->inuse
	  && (player->client->sess.sessionTeam == ent->client->sess.sessionTeam))
	{
	  clients[cnt++] = level.sortedClients[i];
	}
    }

  num = cnt;

  // send the latest information on all clients
  string[0] = 0;
  stringlength = 0;

  // send infos about the top8 players, they'll be sorted by client num
  for (i = 0, cnt = 0; (i <= level.clientHighestID) && (cnt < TEAM_MAXOVERLAY); i++)
    {
      player = g_entities + i;

      if (!player->inuse)
	continue;

      if (player->client->sess.sessionTeam != ent->client->sess.sessionTeam)
	continue;

      // check if the player is one of the top8 player
      j = 0;
      while (j < num)
	{
	  // is it in the list ?
	  if (i == clients[j])
	    break;

	  j++;
	}

      // seems that it isn't skip
      if (j == num)
	continue;

      // ok we have a valid one, send infos
      // health, armor, location, weapon, powerups
      h = player->client->ps.stats[STAT_HEALTH];
      a = player->client->ps.stats[STAT_ARMOR];
      if (h < 0)
	h = 0;
      if (a < 0)
	a = 0;

      Com_sprintf (entry, sizeof (entry), " %i %i %i %i %i %i",
		   i, player->client->pers.teamState.location, h, a,
		   player->client->ps.weapon, player->sh.s.powerups);

      j = strlen (entry);

      // we would overflow or get a truncated message (keep room for '\0')
      if ((stringlength + j) >= sizeof (string))
	break;

      strcpy (string + stringlength, entry);
      stringlength += j;
      cnt++;
    }

  gi->SendServerCommand (ent - g_entities, va ("tinfo %i %s", cnt, string));
}

/*
===============
CheckTeamStatus

Called every frame by G_RunFrame
===============
*/
void
CheckTeamStatus (void)
{
  int i;
  gentity_t *loc, *ent;

  // don't update too often
  if ((level.time - level.lastTeamLocationTime) > TEAM_LOCATION_UPDATE_TIME)
    {
      level.lastTeamLocationTime = level.time;

      // get the location of each player
      for (i = 0; i <= level.clientHighestID; i++)
	{
	  ent = g_entities + i;

	  if (ent->client->pers.connected != CON_CONNECTED)
	    {
	      continue;
	    }

	  if (ent->inuse && (ent->client->sess.sessionTeam == TEAM_RED
			     || ent->client->sess.sessionTeam == TEAM_BLUE))
	    {
	      loc = Team_GetLocation (ent);
	      if (loc)
		ent->client->pers.teamState.location = loc->health;
	      else
		ent->client->pers.teamState.location = 0;
	    }
	}

      // send teamate locations to everyplayer
      for (i = 0; i <= level.clientHighestID; i++)
	{
	  ent = g_entities + i;

	  if (ent->client->pers.connected != CON_CONNECTED)
	    {
	      continue;
	    }

	  if (ent->inuse && (ent->client->sess.sessionTeam == TEAM_RED
			     || ent->client->sess.sessionTeam == TEAM_BLUE))
	    {
	      TeamplayInfoMessage (ent);
	    }
	}
    }
}

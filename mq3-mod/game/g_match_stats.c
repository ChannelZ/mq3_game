/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_match.h"

/*
========================
SendScoreboardMessageToAllClients

Do this at BeginIntermission time and whenever ranks are recalculated
due to enters/exits/forced team changes during intermission
========================
*/
void
SendScoreboardMessageToAllClients (lvl_t * level)
{
  int i;

  for (i = 0; i < level->maxclients; i++)
    {
      if (level->clients[i].pers.connected == CON_CONNECTED)
	{
	  DeathmatchScoreboardMessage (level, g_entities + i);
	}
    }
}

/*
========================
SendConsoleStatsToAllClients

Do this at the end of a match
========================
*/
void
SendConsoleStatsToAllClients (lvl_t * level)
{
  int i;
  int clientNum;

  for (i = 0; i < level->numConnectedClients; i++)
    {
      clientNum = level->sortedClients[i];

      // only send to connected clients
      if (level->clients[clientNum].pers.connected != CON_CONNECTED)
	continue;

      // if we were in tournament send both players infos to everyone
      if (g_gametype->integer == GT_TOURNAMENT)
	{
	  // be sure they did'nt disconnect
	  if (level->clients[level->sortedClients[0]].pers.connected == CON_CONNECTED)
	    {
	      G_ConsoleStats (level, g_entities + clientNum,
			      g_entities + level->sortedClients[0], true);
	    }
	  // hum...
	  gi->SendServerCommand (i, "print \"\n\"");

	  if (level->clients[level->sortedClients[1]].pers.connected == CON_CONNECTED)
	    {
	      // send both to each player even spectators
	      G_ConsoleStats (level, g_entities + clientNum,
			      g_entities + level->sortedClients[1], true);
	    }
	}
      else // other gametypes
	{
	  // only send to connected and playing players their own infos
	  if (level->clients[clientNum].sess.sessionTeam < TEAM_SPECTATOR)
	    {
	      // draw stats in console
	      G_ConsoleStats (level, g_entities + clientNum,
			      g_entities + clientNum, true);

	      switch (g_gametype->integer)
		{
		case GT_CTF:
		  {
		    G_CTFConsoleStats (level, g_entities + clientNum,
				       g_entities + clientNum);
		    break;
		  }
		case GT_TEAM:
		  {
		    G_TDMConsoleStats (level, g_entities + clientNum,
				       g_entities + clientNum);
		    break;
		  }
		default:
		  break;
		}
	    }
	}
    }
}

/*
========================
SendConsoleMatchSummaryToAllClients

Send a short summary of the just finished match
to all clients (even spectators) with all the needed
infos about the match (team scores, team item management etc...)
========================
*/
void
SendConsoleMatchSummaryToAllClients (lvl_t * level)
{
  char *msg;

  if (g_gametype->integer == GT_TOURNAMENT)
    {

    }
  else if (g_gametype->integer == GT_TEAM)
    {
      if (level->teamScores[TEAM_RED] > level->teamScores[TEAM_BLUE])
	{
	  msg = va ("print \"" S_COLOR_RED "Red  " S_COLOR_WHITE
		    "wins %d to %d\n\"", level->teamScores[TEAM_RED],
		    level->teamScores[TEAM_BLUE]);

	  gi->SendServerCommand (-1, msg);
	}
      else
	{
	  msg = va ("print \"" S_COLOR_BLUE "Blue " S_COLOR_WHITE
		    "wins %d to %d\n\"", level->teamScores[TEAM_BLUE],
		    level->teamScores[TEAM_RED]);

	  gi->SendServerCommand (-1, msg);
	}
    }
  else if (g_gametype->integer == GT_CTF)
    {
      if (level->teamScores[TEAM_RED] > level->teamScores[TEAM_BLUE])
	{
	  msg = va ("print \"" S_COLOR_RED "Red  " S_COLOR_WHITE
		    "wins %d to %d\n\"", level->teamScores[TEAM_RED],
		    level->teamScores[TEAM_BLUE]);

	  gi->SendServerCommand (-1, msg);
	}
      else
	{
	  msg = va ("print \"" S_COLOR_BLUE "Blue " S_COLOR_WHITE
		    "wins %d to %d\n\"", level->teamScores[TEAM_BLUE],
		    level->teamScores[TEAM_RED]);

	  gi->SendServerCommand (-1, msg);
	}
    }
}

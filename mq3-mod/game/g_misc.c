/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// g_misc.c

#include "g_local.h"


/*QUAKED func_group (0 0 0) ?
Used to group brushes together just for editor convenience.  They are turned into normal brushes by the utilities.
*/

/*QUAKED info_camp (0 0.5 0) (-4 -4 -4) (4 4 4)
Used as a positional target for calculations in the utilities (spotlights, etc), but removed during gameplay.
*/
void
SP_info_camp (gentity_t * self)
{
  G_SetOrigin (self, self->sh.s.origin);
}

/*QUAKED info_null (0 0.5 0) (-4 -4 -4) (4 4 4)
Used as a positional target for calculations in the utilities (spotlights, etc), but removed during gameplay.
*/
void
SP_info_null (gentity_t * self)
{
  G_FreeEntity (self);
}

/*QUAKED info_notnull (0 0.5 0) (-4 -4 -4) (4 4 4)
Used as a positional target for in-game calculation, like jumppad targets.
target_position does the same thing
*/
void
SP_info_notnull (gentity_t * self)
{
  G_SetOrigin (self, self->sh.s.origin);
}

/*QUAKED light (0 1 0) (-8 -8 -8) (8 8 8) linear
Non-displayed light.
"light" overrides the default 300 intensity.
Linear checbox gives linear falloff instead of inverse square
Lights pointed at a target will be spotlights.
"radius" overrides the default 64 unit radius of a spotlight at the target point.
*/
void
SP_light (gentity_t * self)
{
  G_FreeEntity (self);
}

/*
=================================================================================

TELEPORTERS

=================================================================================
*/
void
TeleportPlayer (gentity_t *player, vec3_t origin, vec3_t angles)
{
  gentity_t *tent;
  vec3_t forward;

  // use temp events at source and destination to prevent the effect
  // from getting dropped by a second player event
  if (player->client->sess.sessionTeam != TEAM_SPECTATOR)
    {
      tent = G_TempEntity (player->client->ps.origin, EV_PLAYER_TELEPORT_OUT);
      tent->sh.s.clientNum = player->sh.s.clientNum;

      tent = G_TempEntity (origin, EV_PLAYER_TELEPORT_IN);
      tent->sh.s.clientNum = player->sh.s.clientNum;
    }

  // unlink to make sure it can't possibly interfere with G_KillBox
  gi->UnlinkEntity (&player->sh);

  VectorCopy (origin, player->client->ps.origin);
  // REMOVEME : don't get stuck in the floor if the teleporter is placed on it
  player->client->ps.origin[2] += 1;

  // spit the player out
  // grab the direction
  AngleVectors (angles, forward, NULL, NULL);
  // give some momentum to the player
  VectorScale (forward, 400, player->client->ps.velocity);
  //
  player->client->ps.pm_time = 160;	// hold time
  player->client->ps.pm_flags |= PMF_TIME_KNOCKBACK;

  // toggle the teleport bit so the client knows to not lerp
  player->client->ps.eFlags ^= EF_TELEPORT_BIT;

  // set angles
  SetClientViewAngle (player, angles);

  // kill anything at the destination
  if (player->client->sess.sessionTeam != TEAM_SPECTATOR)
    {
      G_KillBox (player);
    }

  // save results of pmove
  BG_PlayerStateToEntityState (&player->client->ps, &player->sh.s, true);

  // use the precise origin for linking
  VectorCopy (player->client->ps.origin, player->sh.r.currentOrigin);

  if (player->client->sess.sessionTeam != TEAM_SPECTATOR)
    {
      gi->LinkEntity (&player->sh);
    }
}

/*QUAKED misc_teleporter_dest (1 0 0) (-32 -32 -24) (32 32 -16)
Point teleporters at these.
Now that we don't have teleport destination pads, this is just
an info_notnull
*/
void
SP_misc_teleporter_dest (gentity_t * ent)
{
}

//===========================================================

/*QUAKED misc_model (1 0 0) (-16 -16 -16) (16 16 16)
"model"		arbitrary .md3 file to display
*/
void
SP_misc_model (gentity_t * ent)
{
#if 0
  ent->sh.s.modelindex = G_ModelIndex (ent->model);
  VectorSet (ent->mins, -16, -16, -16);
  VectorSet (ent->maxs, 16, 16, 16);
  gi->LinkEntity (&ent->sh);

  G_SetOrigin (ent, ent->sh.s.origin);
  VectorCopy (ent->sh.s.angles, ent->sh.s.apos.trBase);
#else
  G_FreeEntity (ent);
#endif
}

//===========================================================

void
locateCamera (gentity_t * ent)
{
  vec3_t dir;
  gentity_t *target;
  gentity_t *owner;

  owner = G_PickTarget (ent->target);
  if (!owner)
    {
      G_Printf ("Couldn't find target for misc_partal_surface\n");
      G_FreeEntity (ent);
      return;
    }
  ent->sh.r.ownerNum = owner->sh.s.number;

  // frame holds the rotate speed
  if (owner->spawnflags & 1)
    {
      ent->sh.s.frame = 25;
    }
  else if (owner->spawnflags & 2)
    {
      ent->sh.s.frame = 75;
    }

  // swing camera ?
  if (owner->spawnflags & 4)
    {
      // set to 0 for no rotation at all
      ent->sh.s.powerups = 0;
    }
  else
    {
      ent->sh.s.powerups = 1;
    }

  // clientNum holds the rotate offset
  ent->sh.s.clientNum = owner->sh.s.clientNum;

  VectorCopy (owner->sh.s.origin, ent->sh.s.origin2);

  // see if the portal_camera has a target
  target = G_PickTarget (owner->target);
  if (target)
    {
      VectorSubtract (target->sh.s.origin, owner->sh.s.origin, dir);
      VectorNormalize (dir);
    }
  else
    {
      G_SetMovedir (owner->sh.s.angles, dir);
    }

  ent->sh.s.eventParm = DirToByte (dir);
}

/*QUAKED misc_portal_surface (0 0 1) (-8 -8 -8) (8 8 8)
The portal surface nearest this entity will show a view from the targeted misc_portal_camera, or a mirror view if untargeted.
This must be within 64 world units of the surface!
*/
void
SP_misc_portal_surface (gentity_t * ent)
{
  VectorClear (ent->sh.r.mins);
  VectorClear (ent->sh.r.maxs);
  gi->LinkEntity (&ent->sh);

  ent->sh.r.svFlags = SVF_PORTAL;
  ent->sh.s.eType = ET_PORTAL;

  if (!ent->target)
    {
      VectorCopy (ent->sh.s.origin, ent->sh.s.origin2);
    }
  else
    {
      ent->think = locateCamera;
      //ent->nextthink = level.time + 100;
      ent->nextthink = -1;
    }
}

/*QUAKED misc_portal_camera (0 0 1) (-8 -8 -8) (8 8 8) slowrotate fastrotate noswing
The target for a misc_portal_director.  You can set either angles or target another entity to determine the direction of view.
"roll" an angle modifier to orient the camera around the target vector;
*/
void
SP_misc_portal_camera (gentity_t * ent)
{
  float roll;

  VectorClear (ent->sh.r.mins);
  VectorClear (ent->sh.r.maxs);
  gi->LinkEntity (&ent->sh);

  G_SpawnFloat ("roll", "0", &roll);

  ent->sh.s.clientNum = roll / 360.0 * 256;
}

/*
======================================================================

  SHOOTERS

======================================================================
*/
void
Use_Shooter (gentity_t * ent, gentity_t * other, gentity_t * activator)
{
  vec3_t dir;
  float deg;
  vec3_t up, right;

  // see if we have a target
  if (ent->enemy)
    {
      VectorSubtract (ent->enemy->sh.r.currentOrigin, ent->sh.s.origin, dir);
      VectorNormalize (dir);
    }
  else
    {
      VectorCopy (ent->movedir, dir);
    }

  // randomize a bit
  PerpendicularVector (up, dir);
  CrossProduct (up, dir, right);

  deg = crandom () * ent->random;
  VectorMA (dir, deg, up, dir);

  deg = crandom () * ent->random;
  VectorMA (dir, deg, right, dir);

  VectorNormalize (dir);

  switch (ent->sh.s.weapon)
    {
    case WP_GRENADE_LAUNCHER:
      fire_grenade (ent, ent->sh.s.origin, dir);
      break;
    case WP_ROCKET_LAUNCHER:
      fire_rocket (ent, ent->sh.s.origin, dir);
      break;
    case WP_PLASMAGUN:
      fire_plasma (ent, ent->sh.s.origin, dir);
      break;
    }

  G_AddEvent (ent, EV_FIRE_WEAPON, 0);
}

static void
InitShooter_Finish (gentity_t * ent)
{
  ent->enemy = G_PickTarget (ent->target);
  ent->think = 0;
  ent->nextthink = 0;
}

void
InitShooter (gentity_t * ent, int weapon)
{
  ent->use = Use_Shooter;
  ent->sh.s.weapon = weapon;

  RegisterItem (BG_FindItemForWeapon (weapon));

  G_SetMovedir (ent->sh.s.angles, ent->movedir);

  if (!ent->random)
    {
      ent->random = 1.0;
    }
  ent->random = sin (M_PI * ent->random / 180);
  // target might be a moving object, so we can't set movedir for it
  if (ent->target)
    {
      ent->think = InitShooter_Finish;
      ent->nextthink = level.time + 500;
    }
  gi->LinkEntity (&ent->sh);
}

/*QUAKED shooter_rocket (1 0 0) (-16 -16 -16) (16 16 16)
Fires at either the target or the current direction.
"random" the number of degrees of deviance from the taget. (1.0 default)
*/
void
SP_shooter_rocket (gentity_t * ent)
{
  InitShooter (ent, WP_ROCKET_LAUNCHER);
}

/*QUAKED shooter_plasma (1 0 0) (-16 -16 -16) (16 16 16)
Fires at either the target or the current direction.
"random" is the number of degrees of deviance from the taget. (1.0 default)
*/
void
SP_shooter_plasma (gentity_t * ent)
{
  InitShooter (ent, WP_PLASMAGUN);
}

/*QUAKED shooter_grenade (1 0 0) (-16 -16 -16) (16 16 16)
Fires at either the target or the current direction.
"random" is the number of degrees of deviance from the taget. (1.0 default)
*/
void
SP_shooter_grenade (gentity_t * ent)
{
  InitShooter (ent, WP_GRENADE_LAUNCHER);
}

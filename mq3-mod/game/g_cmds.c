/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

/*
==================
DeathmatchScoreboardMessage

==================
*/
void
DeathmatchScoreboardMessage (lvl_t * level, gentity_t * ent)
{
  char entry[1024];
  char string[1400];
  int stringlength;
  int i, j;
  gclient_t *cl;
  int numSorted, accuracy, perfect;
  int ping;
  int netscore;

  // send the latest information on all clients
  string[0] = 0;
  stringlength = 0;

  numSorted = level->numConnectedClients;

  for (i = 0; i < numSorted; i++)
    {
      cl = &level->clients[level->sortedClients[i]];

      if (cl->pers.connected == CON_CONNECTING)
	{
	  ping = -1;
	}
      else
	{
	  ping = cl->ps.ping < 999 ? cl->ps.ping : 999;
	}

      if (cl->cbt.accuracy_shots)
	{
	  accuracy = cl->cbt.accuracy_hits * 100 / cl->cbt.accuracy_shots;
	}
      else
	{
	  accuracy = 0;
	}
      perfect = (cl->ps.persistant[PERS_RANK] == 0
		 && cl->ps.persistant[PERS_KILLED] == 0) ? 1 : 0;


      netscore = (cl->stats.totkill - cl->stats.totdeath
		  - cl->stats.teamkill - cl->stats.suicidecounter);

      Com_sprintf (entry, sizeof (entry),
		   " %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i",
		   level->sortedClients[i], cl->ps.persistant[PERS_SCORE],
		   ping, (game.time - cl->pers.enterTime) / 60000,
		   g_entities[level->sortedClients[i]].sh.s.powerups, accuracy,
		   cl->ps.persistant[PERS_IMPRESSIVE_COUNT],
		   cl->ps.persistant[PERS_EXCELLENT_COUNT],
		   cl->ps.persistant[PERS_GAUNTLET_FRAG_COUNT],
		   cl->ps.persistant[PERS_DEFEND_COUNT],
		   cl->ps.persistant[PERS_ASSIST_COUNT], perfect,
		   cl->ps.persistant[PERS_CAPTURES],
		   cl->stats.totdamage_done,
		   cl->stats.totdamage_taken,
		   netscore);

      j = strlen (entry);
      if (stringlength + j >= sizeof (string))
	break;
      strcpy (string + stringlength, entry);
      stringlength += j;
    }

  gi->SendServerCommand (ent - g_entities, va ("scores %i %i %i%s", i,
					       level->teamScores[TEAM_RED],
					       level->teamScores[TEAM_BLUE],
					       string));
}

/*
==================
Cmd_Score_f

Request current scoreboard information
Used by +scores
==================
*/
void
Cmd_Score_f (gentity_t * ent)
{
  DeathmatchScoreboardMessage (&level, ent);
}

/*
==================
CheatsOk
==================
*/
bool
CheatsOk (gentity_t * ent)
{
  if (!gi->Cvar_VariableIntegerValue ("sv_cheats"))
    {
      gi->SendServerCommand (ent - g_entities,
			     va ("print \"Cheats are not enabled"
				 "on this server.\n\""));
      return false;
    }
  if (ent->health <= 0)
    {
      gi->SendServerCommand (ent - g_entities,
			     va ("print \"You must be alive to use"
				 "this command.\n\""));
      return false;
    }
  return true;
}

/*
==================
ClientNumberFromString

Returns a player number for either a number or name string
Returns -1 if invalid
==================
*/
int
ClientNumberFromString (gentity_t * to, const char *s)
{
  gclient_t *cl;
  int idnum;
  char cleanName[MAX_STRING_CHARS];

  // numeric values are just slot numbers
  if (s[0] >= '0' && s[0] <= '9')
    {
      idnum = atoi (s);
      if (((idnum < 0) || (idnum >= level.maxclients)) && (to != NULL))
	{
	  gi->SendServerCommand (to - g_entities,
				 va ("print \"Bad client slot: %i\n\"", idnum));
	  return -1;
	}

      cl = &level.clients[idnum];
      if ((cl->pers.connected != CON_CONNECTED) && (to != NULL))
	{
	  gi->SendServerCommand (to - g_entities,
				 va ("print \"Client %i is not active\n\"",
				     idnum));
	  return -1;
	}
      return idnum;
    }

  // check for a name match
  for (idnum = 0, cl = level.clients; idnum < level.maxclients; idnum++, cl++)
    {
      if (cl->pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      Q_strncpyz (cleanName, cl->pers.netname, sizeof (cleanName));
      Q_CleanStr (cleanName);
      if (!Q_stricmp (cleanName, s))
	{
	  return idnum;
	}
    }

  gi->SendServerCommand (to - g_entities,
			 va ("print \"User %s is not on the server\n\"", s));
  return -1;
}

/*
==================
Cmd_Give_f

Give items to a client
==================
*/
void
Cmd_Give_f (gentity_t * ent)
{
  char *name;
  const gitem_t *it;
  int i;
  bool give_all;
  gentity_t *it_ent;
  trace_t trace;

  // cheat protected
  if (!CheatsOk (ent))
    {
      return;
    }

  // no classname contains spaces atm
  name = gi->ArgsFrom (1);

  // give every thing listed here ?
  if (!Q_stricmp (name, "all"))
    give_all = true;
  else
    give_all = false;

  if (give_all || !Q_stricmp (name, "health"))
    {
      ent->health = ent->client->ps.stats[STAT_MAX_HEALTH];
      if (!give_all)
	return;
    }

  if (give_all || !Q_stricmp (name, "weapons"))
    {
      ent->client->ps.stats[STAT_WEAPONS] =
	(1 << WP_NUM_WEAPONS) - 1 - (1 << WP_NONE);
      if (!give_all)
	return;
    }

  if (give_all || !Q_stricmp (name, "ammo"))
    {
      for (i = 0; i < MAX_WEAPONS; i++)
	{
	  ent->client->ps.ammo[i] = 999;
	}
      if (!give_all)
	return;
    }

  if (give_all || !Q_stricmp (name, "armor"))
    {
      ent->client->ps.stats[STAT_ARMOR] = 200;

      if (!give_all)
	return;
    }

  if (!Q_stricmp (name, "excellent"))
    {
      ent->client->ps.persistant[PERS_EXCELLENT_COUNT]++;
      return;
    }
  if (!Q_stricmp (name, "impressive"))
    {
      ent->client->ps.persistant[PERS_IMPRESSIVE_COUNT]++;
      return;
    }
  if (!Q_stricmp (name, "gauntletaward"))
    {
      ent->client->ps.persistant[PERS_GAUNTLET_FRAG_COUNT]++;
      return;
    }
  if (!Q_stricmp (name, "defend"))
    {
      ent->client->ps.persistant[PERS_DEFEND_COUNT]++;
      return;
    }
  if (!Q_stricmp (name, "assist"))
    {
      ent->client->ps.persistant[PERS_ASSIST_COUNT]++;
      return;
    }

  // spawn a specific item right on the player
  if (!give_all)
    {
      it = BG_FindItem (name);
      if (it == NULL)
	{
	  char * msg = va ("print \"Can't give/find item : %s\n\"", name);
	  // we didn't find anything valid
	  gi->SendServerCommand (ent - g_entities, msg);

	  return;
	}

      it_ent = G_Spawn ();
      VectorCopy (ent->sh.r.currentOrigin, it_ent->sh.s.origin);
      it_ent->classname = it->classname;
      G_SpawnItem (it_ent, it);
      FinishSpawningItem (it_ent);
      Com_Memset (&trace, 0, sizeof (trace));
      Touch_Item (it_ent, ent, &trace);
      if (it_ent->inuse)
	{
	  G_FreeEntity (it_ent);
	}
    }
}

/*
==================
Cmd_God_f

Sets client to godmode
==================
*/
void
Cmd_God_f (gentity_t * ent)
{
  char *msg;

  // cheat protected
  if (!CheatsOk (ent))
    {
      return;
    }

  ent->flags ^= FL_GODMODE;
  if (!(ent->flags & FL_GODMODE))
    msg = "godmode OFF\n";
  else
    msg = "godmode ON\n";

  gi->SendServerCommand (ent - g_entities, va ("print \"%s\"", msg));
}

/*
==================
Cmd_Notarget_f

Sets client to notarget
==================
*/
void
Cmd_Notarget_f (gentity_t * ent)
{
  char *msg;

  // cheat protected
  if (!CheatsOk (ent))
    {
      return;
    }

  ent->flags ^= FL_NOTARGET;
  if (!(ent->flags & FL_NOTARGET))
    msg = "notarget OFF\n";
  else
    msg = "notarget ON\n";

  gi->SendServerCommand (ent - g_entities, va ("print \"%s\"", msg));
}

/*
==================
Cmd_Noclip_f
==================
*/
void
Cmd_Noclip_f (gentity_t * ent)
{
  char *msg;

  // cheat protected
  if (!CheatsOk (ent))
    {
      return;
    }

  if (ent->client->misc.noclip)
    {
      msg = "noclip OFF\n";
    }
  else
    {
      msg = "noclip ON\n";
    }
  ent->client->misc.noclip = !ent->client->misc.noclip;

  gi->SendServerCommand (ent - g_entities, va ("print \"%s\"", msg));
}

/*
==================
Cmd_LevelShot_f

This is just to help generate the level pictures
for the menus.  It goes to the intermission immediately
and sends over a command to the client to resize the view,
hide the scoreboard, and take a special screenshot
==================
*/
/*
void
Cmd_LevelShot_f (gentity_t * ent)
{
  // cheat protected
  if (!CheatsOk (ent))
    {
      return;
    }

  // doesn't work in single player
  if (g_gametype->integer != 0)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"Must be in g_gametype 0 for levelshot\n\"");
      return;
    }

  BeginIntermission ();
  gi->SendServerCommand (ent - g_entities, "clientLevelShot");
}
*/

/*
=================
Cmd_Kill_f
=================
*/
void
Cmd_Kill_f (gentity_t * ent)
{
  if (ent->client->sess.sessionTeam == TEAM_SPECTATOR)
    {
      return;
    }
  if (ent->health <= 0)
    {
      return;
    }
  ent->flags &= ~FL_GODMODE;
  ent->client->ps.stats[STAT_HEALTH] = ent->health = -999;
  player_die (ent, ent, ent, 100000, MOD_SUICIDE);
}

/*
=================
Cmd_Remove_f
=================
*/
void
Cmd_Remove_f (void)
{
  int num;
  const char *arg;

  arg = gi->Argv (1);

  num = ClientNumberFromString (NULL, arg);
  if (num == -1)
    {
      return;
    }

  SetTeam (g_entities + num, "s");
}

/*
=================
Cmd_RemoveFromEnt_f
=================
*/
void
Cmd_RemoveFromEnt_f (gentity_t * ent)
{
  int num;
  const char *arg;

  if (ent->client->sess.sessionTeam == TEAM_SPECTATOR)
    {
      return;
    }

  if (gi->Argc() != 2)
    {
      char *msg;

      msg = va ("print \"Usage: remove <player name>\nremove <player id>\n\"");
      gi->SendServerCommand (ent - g_entities, msg);
      return;
    }

  arg = gi->Argv (1);

  num = ClientNumberFromString (ent, arg);
  if (num == -1)
    {
      return;
    }

  SetTeam (g_entities + num, "s");
}

void
Cmd_Random_f (void)
{
  int num;
  const char *arg;
  int res;
  char *msg;

  arg = gi->Argv (1);

  num = atoi (arg);

  if (num <= 0)
    {
      return;
    }

  res = rand () % num;

  msg = va ("print \"" S_COLOR_YELLOW "Random " S_COLOR_GREEN "%d" S_COLOR_WHITE
	    " : " S_COLOR_CYAN "%d \n\"", num, res);

  gi->SendServerCommand (-1, msg);
}

/*
=================
BroadCastTeamChange

Let everyone know about a team change
=================
*/
void
BroadcastTeamChange (gclient_t * client, int oldTeam)
{
  char * msg = NULL;
  // set the message
  if (client->sess.sessionTeam == TEAM_RED)
    {
      msg = va ("cp \"%s" S_COLOR_WHITE " joined the red team.\n\"",
		client->pers.netname);
    }
  else if (client->sess.sessionTeam == TEAM_BLUE)
    {
      msg = va ("cp \"%s" S_COLOR_WHITE " joined the blue team.\n\"",
		client->pers.netname);
    }
  else if ((client->sess.sessionTeam == TEAM_SPECTATOR)
	   && (oldTeam != TEAM_SPECTATOR))
    {
      msg = va ("cp \"%s" S_COLOR_WHITE " joined the spectators.\n\"",
		client->pers.netname);
    }
  else if (client->sess.sessionTeam == TEAM_FREE)
    {
      msg = va ("cp \"%s" S_COLOR_WHITE " joined the battle.\n\"",
		client->pers.netname);
    }
  // broadcast if there is to
  if (msg != NULL)
    gi->SendServerCommand (-1, msg);
}

/*
=================
SetTeam
=================
*/
void
SetTeam (gentity_t * ent, const char *s)
{
  int team, oldTeam;
  gclient_t *client;
  int clientNum;
  spectatorState_t specState;
  int specClient;
  int spectatorTime;

  //
  // see what change is requested
  //
  client = ent->client;

  clientNum = client - level.clients;
  specClient = 0;
  specState = SPECTATOR_NOT;
  spectatorTime = game.time; // if -1 we aren't on the waiting list

  oldTeam = client->sess.sessionTeam;

  if (!Q_stricmp (s, "scoreboard") || !Q_stricmp (s, "score"))
    {
      team = TEAM_SPECTATOR;
      specState = SPECTATOR_SCOREBOARD;
      spectatorTime = -1; // scoreboard specs won't spawn in the game
    }
  else if (!Q_stricmp (s, "speconly"))
    {
      if ((oldTeam == TEAM_SPECTATOR) && (client->sess.spectatorTime == -1))
	{
	  spectatorTime = game.time;
	  gi->SendServerCommand (clientNum,
				 "print \"Waiting to play.\n\"");
	}
      else
	{
	  spectatorTime = -1;
	  gi->SendServerCommand (clientNum,
				 "print \"Going spectator only.\n\"");
	}

      team = TEAM_SPECTATOR;
      specState = SPECTATOR_FREE;
    }
  else if (!Q_stricmp (s, "follow1"))
    {
      team = TEAM_SPECTATOR;
      specState = SPECTATOR_FOLLOW;
      specClient = -1;
      spectatorTime = -1;
    }
  else if (!Q_stricmp (s, "follow2"))
    {
      team = TEAM_SPECTATOR;
      specState = SPECTATOR_FOLLOW;
      specClient = -2;
      spectatorTime = -1;
    }
  else if (!Q_stricmp (s, "s") || !Q_stricmp (s, "spectator"))
    {
      team = TEAM_SPECTATOR;
      specState = SPECTATOR_FREE;
    }
  else if (BG_GametypeIsTeam (g_gametype->integer))
    {
      // if running a team game, assign player to one of the teams
      specState = SPECTATOR_NOT;
      if (!Q_stricmp (s, "r") || !Q_stricmp (s, "red"))
	{
	  team = TEAM_RED;
	}
      else if (!Q_stricmp (s, "b") || !Q_stricmp (s, "blue"))
	{
	  team = TEAM_BLUE;
	}
      else
	{
	  // pick the team with the least number of players
	  team = PickTeam (clientNum);
	}

      if (g_teamForceBalance->integer)
	{
	  int counts[TEAM_NUM_TEAMS];

	  counts[TEAM_BLUE] = TeamCount (clientNum, TEAM_BLUE);
	  counts[TEAM_RED] = TeamCount (clientNum, TEAM_RED);

	  // We allow a spread of two
	  if ((team == TEAM_RED) && (counts[TEAM_RED] - counts[TEAM_BLUE] > 1))
	    {
	      gi->SendServerCommand (clientNum,
				     "cp \"Red team has too many players.\n\"");
	      return;	// ignore the request
	    }
	  if (team == TEAM_BLUE && counts[TEAM_BLUE] - counts[TEAM_RED] > 1)
	    {
	      gi->SendServerCommand (clientNum,
				     "cp \"Blue team has too many players.\n\"");
	      return;	// ignore the request
	    }
	  // It's ok, the team we are switching to has less
	  // or same number of players
	}
    }
  else // < GT_TEAM; ffa or tourney
    {
      // force them to spectators if there aren't any spots free
      team = TEAM_FREE;
    }

  // override decision if limiting the players
  if ((g_gametype->integer == GT_TOURNAMENT)
      && (level.numNonSpectatorClients >= 2))
    {
      team = TEAM_SPECTATOR;
      specState = SPECTATOR_FREE;
    }
  else if ((g_maxGameClients->integer > 0)
	   && (level.numNonSpectatorClients >= g_maxGameClients->integer))
    {
      team = TEAM_SPECTATOR;
      specState = SPECTATOR_FREE;
    }

  //
  // decide if we will allow the change
  //
  if ((team == oldTeam) && (team != TEAM_SPECTATOR))
    {
      return;
    }

  //
  // execute the team change
  //

  // if the player was dead leave the body
  if (client->ps.stats[STAT_HEALTH] <= 0)
    {
      CopyToBodyQue (ent);
    }

  if (oldTeam != TEAM_SPECTATOR)
    {
      // Kill him (makes sure he loses flags, etc)
      ent->flags &= ~FL_GODMODE;
      ent->client->ps.stats[STAT_HEALTH] = ent->health = 0;
      player_die (ent, ent, ent, 100000, MOD_SUICIDE);
    }

  // they go to the end of the line for tournements
  if (team == TEAM_SPECTATOR)
    {
      client->sess.spectatorTime = spectatorTime;
    }

  client->sess.sessionTeam = team;
  client->sess.spectatorState = specState;
  client->sess.spectatorClient = specClient;

  // remove me
  client->sess.teamLeader = false;

  BroadcastTeamChange (client, oldTeam);

  // get and distribute relevent paramters
  ClientUserinfoChanged (clientNum);

  ClientBegin (clientNum);
}

/*
=================
StopFollowing

If the client being followed leaves the game, or you just want to drop
to free floating spectator mode
=================
*/
void
StopFollowing (gentity_t * ent)
{
  ent->client->ps.persistant[PERS_TEAM] = TEAM_SPECTATOR;
  ent->client->sess.sessionTeam = TEAM_SPECTATOR;
  ent->client->sess.spectatorState = SPECTATOR_FREE;
  ent->client->ps.pm_flags &= ~PMF_FOLLOW;
  ent->sh.r.svFlags &= ~SVF_BOT;
  ent->client->ps.clientNum = ent - g_entities;
}

/*
=================
Cmd_Team_f
=================
*/
void
Cmd_Team_f (gentity_t * ent)
{
  int oldTeam;
  const char *s;
  const char *msg;

  if (gi->Argc () != 2)
    {
      // some general infos about this command
      msg = va ("print \"" S_COLOR_GREEN "Change team :\n"
		"" S_COLOR_YELLOW "free" S_COLOR_WHITE
		" or " S_COLOR_YELLOW "f" S_COLOR_WHITE " : to join the game\n"
		"" S_COLOR_YELLOW "red" S_COLOR_WHITE
		" or " S_COLOR_YELLOW "r" S_COLOR_WHITE " : to join red team in team games\n"
		"" S_COLOR_YELLOW "blue" S_COLOR_WHITE
		" or " S_COLOR_YELLOW "b" S_COLOR_WHITE " : to join blue team in team games\n"
		"" S_COLOR_YELLOW "spectator" S_COLOR_WHITE
		" or " S_COLOR_YELLOW "s" S_COLOR_WHITE " : to join spectator\n"
		"" S_COLOR_YELLOW "speconly" S_COLOR_WHITE " : to join spectator"
		" and not the waiting list\n"
		"" S_COLOR_YELLOW "follow1" S_COLOR_WHITE " : autofollow first ranked\n"
		"" S_COLOR_YELLOW "follow2" S_COLOR_WHITE " : autofollow second ranked\n"
		"" S_COLOR_YELLOW "scoreboard" S_COLOR_WHITE " : print the scoreboard\n\n\"");

      gi->SendServerCommand (ent - g_entities, msg);

      oldTeam = ent->client->sess.sessionTeam;
      switch (oldTeam)
	{
	case TEAM_BLUE:
	  gi->SendServerCommand (ent - g_entities, "print \"Playing blue team\n\"");
	  break;
	case TEAM_RED:
	  gi->SendServerCommand (ent - g_entities, "print \"Playing red team\n\"");
	  break;
	case TEAM_FREE:
	  gi->SendServerCommand (ent - g_entities, "print \"Playing free team\n\"");
	  break;
	case TEAM_SPECTATOR:
	  {
	    if (ent->client->sess.spectatorTime != -1)
	      gi->SendServerCommand (ent - g_entities,
				     "print \"Spectator team. Waiting to play.\n\"");
	    else
	      gi->SendServerCommand (ent - g_entities,
				     "print \"Spectator team. Spectator only.\n\"");
	  }
	  break;
	}
      return;
    }

  if (ent->client->timer.switchTeamTime > game.time)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"May not switch teams more"
			     " than once per 5 seconds.\n\"");
      return;
    }

  s = gi->Argv (1);

  SetTeam (ent, s);

  ent->client->timer.switchTeamTime = game.time + 5000;
}

/*
=================
Cmd_SetTeamName_f
=================
*/
void
Cmd_SetTeamName_f (gentity_t * ent)
{
  const char *msg;
  team_t team;
  int clientNum = ent - g_entities;
  int index;

  if (gi->Argc () != 2)
    {
      msg = "print \"setteamname <name>\n"
	"Change the name of the player's team in team games\n\"";

      gi->SendServerCommand (clientNum, msg);
      return;
    }

  if (!BG_GametypeIsTeam (g_gametype->integer))
    {
      gi->SendServerCommand (clientNum, "print \"Not playing a team game\n\"");
      return;
    }

  team = ent->client->sess.sessionTeam;

  if (team >= TEAM_SPECTATOR)
    {
      gi->SendServerCommand (clientNum, "print \"Spectators can't change team name\n\"");
      return;
    }

  if (team == TEAM_RED)
    {
      index = 0;
    }
  else if (team == TEAM_BLUE)
    {
      index = 1;
    }
  else
    {
      gi->SendServerCommand (clientNum, "print \"You are in an unknown team !\n\"");
      return;
    }

  // do the work
  msg = gi->Argv (1);

  Q_strncpyz (level.teamNames[index], msg, sizeof (level.teamNames[0]));

  // broadcast team name change and update the config string
  if (index == 0)
    {
      msg = va ("print \"Red Team name changed to %s\n\"", level.teamNames[0]);
      gi->SetConfigstring (CS_TEAM1_NAME, level.teamNames[0]);
    }
  else
    {
      msg = va ("print \"Blue Team name changed to %s\n\"", level.teamNames[1]);
      gi->SetConfigstring (CS_TEAM2_NAME, level.teamNames[1]);
    }

  gi->SendServerCommand (-1, msg);
}

/*
=================
Cmd_Follow_f
=================
*/
void
Cmd_Follow_f (gentity_t * ent)
{
  int i;
  const char *arg;

  if (gi->Argc () != 2)
    {
      if (ent->client->sess.spectatorState == SPECTATOR_FOLLOW)
	{
	  StopFollowing (ent);
	}
      return;
    }

  arg = gi->Argv (1);
  i = ClientNumberFromString (ent, arg);
  if (i == -1)
    {
      return;
    }

  // can't follow self
  if (&level.clients[i] == ent->client)
    {
      return;
    }

  // can't follow another spectator
  if (level.clients[i].sess.sessionTeam == TEAM_SPECTATOR)
    {
      return;
    }

  // first set them to spectator
  if (ent->client->sess.sessionTeam != TEAM_SPECTATOR)
    {
      SetTeam (ent, "s");
    }

  ent->client->sess.spectatorState = SPECTATOR_FOLLOW;
  ent->client->sess.spectatorClient = i;
}

/*
=================
Cmd_FollowCycle_f
=================
*/
void
Cmd_FollowCycle_f (gentity_t * ent, int dir)
{
  int clientnum;
  int original;

  // first set them to spectator
  if (ent->client->sess.spectatorState == SPECTATOR_NOT)
    {
      SetTeam (ent, "s");
    }

  if (dir != 1 && dir != -1)
    {
      G_Error ("Cmd_FollowCycle_f: bad dir %i", dir);
    }

  clientnum = ent->client->sess.spectatorClient;
  original = clientnum;
  do
    {
      clientnum += dir;
      if (clientnum >= level.maxclients)
	{
	  clientnum = 0;
	}
      if (clientnum < 0)
	{
	  clientnum = level.maxclients - 1;
	}

      // can only follow connected clients
      if (level.clients[clientnum].pers.connected != CON_CONNECTED)
	{
	  continue;
	}

      // can't follow another spectator
      if (level.clients[clientnum].sess.sessionTeam == TEAM_SPECTATOR)
	{
	  continue;
	}

      // this is good, we can use it
      ent->client->sess.spectatorClient = clientnum;
      ent->client->sess.spectatorState = SPECTATOR_FOLLOW;
      return;
    }
  while (clientnum != original);

  // leave it where it was
}

/*
=================
G_FindNearestPlayer
=================
*/
gentity_t *
G_FindNearestPlayer (gentity_t * origin, int flag)
{
  gentity_t *ent;
  gentity_t *resplayer = NULL;
  vec3_t res;
  vec_t distance;
  vec_t saved = 9999999.0;
  int i;

  ent = &g_entities[0];

  // FFA/Tourney will return NULL
  if (!BG_GametypeIsTeam (g_gametype->integer))
    return NULL;

  // can we use numconnected and sortedclients ?
  for (i = 0; i < level.maxclients; i++, ent++)
    {
      if (!ent->inuse)
	continue;

      // do we have a player ?
      if (/*ent->sh.s.eType != ET_PLAYER || */!ent->client)
	continue;

      // exclude self; because i'm always near of myself -> predictible result
      if (ent == origin)
	continue;

      // check if the client is connected
      if (ent->client->pers.connected != CON_CONNECTED)
	continue;

      // check if playing
      if (ent->client->sess.sessionTeam >= TEAM_SPECTATOR)
	continue;

      // we have now a connected playing client

      // display nearest player if flag == 0
      // otherwise nearest team player (flag == 1)
      if ((flag == 1) && !OnSameTeam (origin, ent))
	continue;

      // do the job
      VectorSubtract (origin->sh.r.currentOrigin, ent->sh.r.currentOrigin, res);
      distance = VectorLength (res);

      if (distance < saved)
	{
	  saved = distance;
	  resplayer = ent;
	}
    }

  return resplayer;
}

/*
=================
G_ChatTokenString
=================
*/
int
G_ChatTokenString (gentity_t * ent, char tok, char *buffer, int buffersize)
{
  int len;
  switch (tok)
    {
      // armor
    case 'a':
      {
	len = Com_sprintf (buffer, buffersize, "%d",
			   ent->client->ps.stats[STAT_ARMOR]);

	// buffer was too small, string got cut
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // armor colored output
    case 'b':
      {
	break;
      }
      // health
    case 'h':
      {
	len = Com_sprintf (buffer, buffersize, "%d",
			   ent->client->ps.stats[STAT_HEALTH]);

	// buffer was too small, string got cut
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // health colored output
    case 'g':
      {
	break;
      }
      // name (add a nick variable for short names?)
    case 'n':
      {
	len = Com_sprintf (buffer, buffersize, "%s", ent->client->pers.netname);

	// buffer was too small, string got cut
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // current weapon
    case 'w':
      {
	// grab item
	// ent->sh.s.weapon is the same
	int weap = ent->client->ps.weapon;

	// nothing to display when dead
	if (weap == WP_NONE)
	  {
	    Q_strncpyz (buffer, "(none)", buffersize);
	    return 6;
	  }

	const gitem_t *res;
	res = BG_FindItemForWeapon (weap);
	// this shouldn't happend !
	if (res == NULL)
	  return 0;

	len = Com_sprintf (buffer, buffersize, "^%c%s",
			   res->color, res->short_name);

	// buffer was too small, string go cut
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // last taken item
    case 't':
      {
	const gitem_t *res = ent->client->stats.lastTakenItem;
	// we have a valid item
	if (res != NULL)
	  {
	    len = Com_sprintf (buffer, buffersize, "^%c%s",
			       res->color, res->pickup_name);

	    // buffer was too small, string got cut
	    if (len >= buffersize - 1)
	      len = buffersize - 1;	// skipping the ending '\0'
	    return len;
	  }
	else
	  {
	    return 0;
	  }
      }
      // nearest item
    case 'i':
      {
	const gitem_t *res = G_FindNearestItem (ent->sh.r.currentOrigin, 0);
	// no result
	if (res == NULL)
	  return 0;
	len = Com_sprintf (buffer, buffersize, "^%c%s", res->color,
			   res->pickup_name);

	// buffer was too small, string got cut
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // nearest, but excluding ammo, shards and 5hp
    case 'j':
      {
	const gitem_t *res = G_FindNearestItem (ent->sh.r.currentOrigin, 3);
	if (res == NULL)
	  return 0;
	len = Com_sprintf (buffer, buffersize, "^%c%s", res->color,
			   res->pickup_name);

	// buffer was too small
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // nearest crosshair item
    case 'k':
      {
	vec3_t start;
	vec3_t end;
	trace_t tr;
	vec3_t forward;

	// grabbing player angles
	AngleVectors (ent->client->ps.viewangles, forward, NULL, NULL);

	VectorCopy (ent->sh.r.currentOrigin, start);
	VectorNormalize (forward);
	// looking forward at the client viewheight
	start[2] += ent->client->ps.viewheight;
	VectorMA (start, 131072, forward, end);	// 131072 = 2^17
	// stop on first wall or player we hit
	gi->Trace (&tr, start, vec3_origin, vec3_origin, end,
		   ent->sh.s.number, CONTENTS_SOLID | CONTENTS_BODY);

	// checks for trace validity ?

	// look for items near the spotted point
	const gitem_t *res = G_FindNearestItem (tr.endpos, 0);
	if (res == NULL)
	  return 0;
	len = Com_sprintf (buffer, buffersize, "^%c%s", res->color,
			   res->pickup_name);

	// buffer was too small
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // same as 'k' but skip ammo, shards and low health
    case 'l':
      {
	vec3_t start;
	vec3_t end;
	trace_t tr;
	vec3_t forward;

	// grabbing player angles
	AngleVectors (ent->client->ps.viewangles, forward, NULL, NULL);

	VectorCopy (ent->sh.r.currentOrigin, start);
	VectorNormalize (forward);
	// looking forward at the client viewheight
	start[2] += ent->client->ps.viewheight;
	VectorMA (start, 131072, forward, end);	// 131072 = 2^17
	// stop on first wall or player we hit
	gi->Trace (&tr, start, vec3_origin, vec3_origin, end,
		   ent->sh.s.number, CONTENTS_SOLID | CONTENTS_BODY);

	// look for items near the spotted point
	const gitem_t *res = G_FindNearestItem (tr.endpos, 3);
	if (res == NULL)
	  return 0;
	len = Com_sprintf (buffer, buffersize, "^%c%s", res->color,
			   res->pickup_name);

	// buffer was too small
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // nearest player
    case 'p':
      {
	gentity_t *player = G_FindNearestPlayer (ent, 0);
	if (player == NULL)
	  return 0;
	len = Com_sprintf (buffer, buffersize, "%s", player->client->pers.netname);

	// buffer was too small
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // nearest team player
    case 'q':
      {
	gentity_t *player = G_FindNearestPlayer (ent, 1);
	if (player == NULL)
	  return 0;
	len = Com_sprintf (buffer, buffersize, "%s", player->client->pers.netname);

	// buffer was too small
	if (len >= buffersize - 1)
	  len = buffersize - 1;	// skipping the ending '\0'
	return len;
      }
      // power up status, quad, regen, flags
      // the algo is quite slow
    case 'P':
      {
	int *powerups = ent->client->ps.powerups;
	const gitem_t *item;
	int i;
	bool dospace = false;
	// len = 0; // we should compute the len on generation (avoiding strlen)

	Com_Memset (buffer, 0, buffersize);

	// go through all the powerups and look if they are active
	for (i = PW_QUAD; i < PW_NUM_POWERUPS; i++)
	  {
	    if (powerups[i] > level.time)
	      {
		// grab the associated item
		item = BG_FindItemForPowerup (i);
		if (item != NULL)
		  {
		    // separate arguments with a space if there are many
		    if (dospace)
		      {
			Q_strcat (buffer, buffersize, " ");
		      }
		    else
		      {
			// for the next loop
			dospace = true;
		      }

		    // fill the buffer with the color and shortname
		    char *msg = va ("^%c%s", item->color, item->short_name);
		    Q_strcat (buffer, buffersize, msg);
		  }
	      }
	  }
	return strlen (buffer);
      }
      // return the player's location
    case 'L':
      {
	bool found;
	found = Team_GetLocationMsg (ent, buffer, buffersize);

	if (!found)
	  return 0;

	return strlen (buffer);
      }
    default:
      break;
    }
  return 0;
}

/*
==================
G_ParseChatTokens
==================
*/
void
G_ParseChatTokens (gentity_t * ent, char *text, const char *chatText,
		   size_t textsize, size_t chatTextlength)
{
  char *dest;
  const char *src;
  unsigned int i = 0;
  unsigned int total = 0;

  dest = text;
  src = chatText;

  // ent is supposed valid as well as chatText

  G_Printf ("chattext : %s ([%i] - size : %i)\n", chatText, chatTextlength, textsize);

  while (i <= chatTextlength)
    {
      // checking for src != NULL seems useless
      if (*src == '\0')
	{
	  *dest = *src;
	  break;
	}

      // check for chat token #
      if (*src == '#')
	{
	  // be sure we won't miss a \0
	  if (*(src + 1) == '\0')
	    {
	      if (total < (textsize - 1))
		*(dest) = '\0';

	      break;
	    }

	  char string[32];
	  int len;

	  //Com_Memset (&string, 0, sizeof (string));

	  len = G_ChatTokenString (ent, *(src + 1), string, sizeof (string));

	  // not found, nothing to copy or an error
	  if (len <= 0)
	    {
	      src += 2;
	      i += 2;
	      continue;
	    }

	  // check if we'are going too fat, -1 because we can jump 2 characters
	  if ((total + len) >= (textsize - 1))
	    break;

	  // copy to the chat line
	  strncpy (dest, string, len);

	  src += 2;
	  i += 2;
	  dest += len;
	  total += len;
	}
      else
	{
	  *dest = *src;

	  dest++;
	  src++;
	  i++;
	  total++;
	}
    }
  // for security
  text[textsize - 1] = '\0';
}

/*
==================
G_SayTo

private talk to "other"
==================
*/
static void
G_SayTo (gentity_t * ent, gentity_t * other, int mode, int color,
	 const char *name, const char *message)
{
  char * msg;

  if (!other)
    {
      return;
    }
  if (!other->inuse)
    {
      return;
    }
  if (!other->client)
    {
      return;
    }
  if (other->client->pers.connected != CON_CONNECTED)
    {
      return;
    }
  if ((mode == SAY_TEAM) && !OnSameTeam (ent, other))
    {
      return;
    }
  // no chatting to players in tournements if not wanted
  if (!g_allowSpectatorChat->integer
      && (g_gametype->integer == GT_TOURNAMENT)
      && (other->client->sess.sessionTeam == TEAM_FREE)
      && (ent->client->sess.sessionTeam != TEAM_FREE))
    {
      return;
    }

  msg = va ("%s \"%s%c%c%s\"", mode == SAY_TEAM ? "tchat" : "chat",
	    name, Q_COLOR_ESCAPE, color, message);

  gi->SendServerCommand (other - g_entities, msg);
}

/*
==================
G_Say
==================
*/
#define EC "\x19"
void
G_Say (gentity_t * ent, gentity_t * target, int mode, const char *chatText)
{
  int j;
  gentity_t *other;
  int color;
  char name[64];
  // don't let text be too long for malicious reasons
  char text[MAX_SAY_TEXT];
  char location[64];

  // no team in FFA or Tourney, talk to everybody
  if ((!BG_GametypeIsTeam (g_gametype->integer)) && (mode == SAY_TEAM))
    {
      mode = SAY_ALL;
    }

  switch (mode)
    {
    default:
    case SAY_ALL:
      G_LogPrintf ("say: %s: %s\n", ent->client->pers.netname, chatText);
      Com_sprintf (name, sizeof (name), "%s%c%c" EC ": ",
		   ent->client->pers.netname, Q_COLOR_ESCAPE, COLOR_WHITE);
      color = COLOR_GREEN;
      break;
    case SAY_TEAM:
      G_LogPrintf ("sayteam: %s: %s\n", ent->client->pers.netname, chatText);
      if (Team_GetLocationMsg (ent, location, sizeof (location)))
	{
	  Com_sprintf (name, sizeof (name), EC "(%s%c%c" EC ") (%s)" EC ": ",
		       ent->client->pers.netname, Q_COLOR_ESCAPE, COLOR_WHITE,
		       location);
	}
      else
	{
	  Com_sprintf (name, sizeof (name), EC "(%s%c%c" EC ")" EC ": ",
		       ent->client->pers.netname, Q_COLOR_ESCAPE, COLOR_WHITE);
	}
      color = COLOR_CYAN;
      break;
    case SAY_TELL:
      G_LogPrintf ("tell: %s to %s: %s\n", ent->client->pers.netname,
		   target->client->pers.netname, chatText);
      if ((target != NULL) && (BG_GametypeIsTeam (g_gametype->integer))
	  && (target->client->sess.sessionTeam == ent->client->sess.sessionTeam)
	  && (Team_GetLocationMsg (ent, location, sizeof (location))))
	{
	  Com_sprintf (name, sizeof (name), EC "[%s%c%c" EC "] (%s)" EC ": ",
		       ent->client->pers.netname, Q_COLOR_ESCAPE, COLOR_WHITE,
		       location);
	}
      else
	{
	  Com_sprintf (name, sizeof (name), EC "[%s%c%c" EC "]" EC ": ",
		       ent->client->pers.netname, Q_COLOR_ESCAPE, COLOR_WHITE);
	}
      color = COLOR_MAGENTA;
      break;
    }

  G_ParseChatTokens (ent, text, chatText, sizeof (text), strlen (chatText));

  if (target)
    {
      G_SayTo (ent, target, mode, color, name, text);
      return;
    }

  // echo the text to the console
  G_Printf ("%s%s\n", name, text);

  // send it to all the apropriate clients
  for (j = 0; j < level.maxclients; j++)
    {
      other = &g_entities[j];
      G_SayTo (ent, other, mode, color, name, text);
    }
}

/*
==================
Cmd_Say_f
==================
*/
static void
Cmd_Say_f (gentity_t * ent, int mode, bool arg0)
{
  char *p;

  if (gi->Argc () < 2 && !arg0)
    {
      return;
    }

  if (arg0)
    {
      p = gi->ArgsFrom (0);
    }
  else
    {
      p = gi->ArgsFrom (1);
    }

  G_Say (ent, NULL, mode, p);
}

/*
==================
Cmd_Tell_f
==================
*/
static void
Cmd_Tell_f (gentity_t * ent)
{
  int targetNum;
  gentity_t *target;
  char *p;
  const char *arg;

  if (gi->Argc () < 2)
    {
      return;
    }

  arg = gi->Argv (1);
  targetNum = atoi (arg);
  if (targetNum < 0 || targetNum >= level.maxclients)
    {
      return;
    }

  target = &g_entities[targetNum];
  if (!target || !target->inuse || !target->client)
    {
      return;
    }

  p = gi->ArgsFrom (2);

  G_Say (ent, target, SAY_TELL, p);
  // get an echo for the sender
  // don't tell to the player self if it was already directed to this player
  // also don't send the chat back to a bot
  if (ent != target && !(ent->sh.r.svFlags & SVF_BOT))
    {
      G_Say (ent, ent, SAY_TELL, p);
    }
}

/*
==================
Cmd_Where_f
==================
*/
void
Cmd_Where_f (gentity_t * ent)
{
  gi->SendServerCommand (ent - g_entities,
			 va ("print \"%s\n\"", vtos (ent->sh.s.origin)));
}

/*
============================
Callvote related stuff

============================
*/

static void
default_vote_action (gentity_t * ent, const char * arg1, const char * arg2)
{
  Com_sprintf (level.voteString, sizeof (level.voteString), "%s \"%s\"",
	       arg1, arg2);
  Com_sprintf (level.voteDisplayString, sizeof (level.voteDisplayString),
	       "%s", level.voteString);
}

static void
map_vote_action (gentity_t * ent, const char * arg1, const char * arg2)
{
  // special case for map changes, we want to reset the nextmap setting
  // this allows a player to change maps, but not upset the map rotation
  char s[MAX_STRING_CHARS];

  gi->Cvar_VariableStringBuffer ("nextmap", s, sizeof (s));
  if (*s)
    {
      Com_sprintf (level.voteString, sizeof (level.voteString),
		   "%s %s; set nextmap \"%s\"", arg1, arg2, s);
    }
  else
    {
      Com_sprintf (level.voteString, sizeof (level.voteString), "%s %s",
		   arg1, arg2);
    }
  Com_sprintf (level.voteDisplayString, sizeof (level.voteDisplayString),
	       "%s", level.voteString);
}

static void
nextmap_vote_action (gentity_t * ent, const char * arg1, const char * arg2)
{
  char s[MAX_STRING_CHARS];

  gi->Cvar_VariableStringBuffer ("nextmap", s, sizeof (s));
  if (!*s)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"nextmap not set.\n\"");
      return;
    }
  Com_sprintf (level.voteString, sizeof (level.voteString),
	       "vstr nextmap");
  Com_sprintf (level.voteDisplayString, sizeof (level.voteDisplayString),
	       "%s", level.voteString);
}

static void
gametype_vote_action (gentity_t * ent, const char * arg1, const char * arg2)
{
  int i;
  // special case for g_gametype, check for bad values
  if (!Q_stricmp (arg1, "g_gametype"))
    {
      i = atoi (arg2);
      if (/*i == GT_SINGLE_PLAYER || */i < GT_FFA || i >= GT_MAX_GAME_TYPE)
	{
	  gi->SendServerCommand (ent - g_entities,
				 "print \"Invalid gametype.\n\"");
	  return;
	}

      Com_sprintf (level.voteString, sizeof (level.voteString), "%s %d", arg1, i);
      Com_sprintf (level.voteDisplayString, sizeof (level.voteDisplayString),
		   "%s %s", arg1, gametypeFullNames[i]);
    }
}

struct vote_command_s
{
  char * displayname;
  char * cmdname;
  void (* func) (gentity_t * ent, const char * arg1, const char * arg2);
  bool isCvar;
};
typedef struct vote_command_s vote_command_t;


vote_command_t vote_command_list [] =
  {
    {"map_restart", "map_restart", default_vote_action, false},
    {"nextmap", "nextmap", nextmap_vote_action, false},
    {"map", "map", map_vote_action, false},
    {"gametype", "g_gametype", gametype_vote_action, true},
    {"rapemode", "g_rapeMode", default_vote_action, true},
    {"kick", "kick", default_vote_action, false},
    {"remove", "remove", default_vote_action, false},
    {"doWarmup", "g_doWarmup", default_vote_action, true},
    {"timelimit", "g_timelimit", default_vote_action, true},
    {"fraglimit", "g_fraglimit", default_vote_action, true},
    {"capturelimit", "g_capturelimit", default_vote_action, true},
    {"roundlimit", "g_roundlimit", default_vote_action, true},
    {"allowspectatorchat", "g_allowSpectatorChat", default_vote_action, true},
    {"dmflags", "g_dmflags", default_vote_action, true},
    {"plmflags", "g_plmflags", default_vote_action, true},
    {"random", "random", default_vote_action, false},
    {"spawnLife", "g_spawnLife", default_vote_action, true},
    {"spawnArmor", "g_spawnArmor", default_vote_action, true},
    {" ", " ", NULL, false} // sentinel
  };

int vote_number_commands = ARRAY_LEN (vote_command_list) - 1;

/*
==================
Cmd_CallVote_f
==================
*/
void
Cmd_CallVote_f (gentity_t * ent)
{
  const char *c;
  int i;
  char arg1[MAX_STRING_TOKENS];
  const char *arg2;
  char votemsg [1024];
  bool valid;

  if (!g_allowVote->integer)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"Voting not allowed here.\n\"");
      return;
    }

  else if (level.voteTime)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"A vote is already in progress.\n\"");
      return;
    }

  else if (ent->client->pers.voteCount >= MAX_VOTE_COUNT)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"You have called the maximum"
			     " number of votes.\n\"");
      return;
    }
  else if (ent->client->sess.sessionTeam == TEAM_SPECTATOR)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"Not allowed to call a vote"
			     " as spectator.\n\"");
      return;
    }

  // make sure it is a valid command to vote on
  gi->ArgvBuffer (1, arg1, sizeof(arg1)); // we will replace this one
  arg2 = gi->Argv (2);

  // check for command separators in arg2
  // avoid exploits when the commandstring is executed
  for (c = arg2; *c; ++c)
    {
      switch (*c)
	{
	case '\n':
	case '\r':
	case ';':
	  gi->SendServerCommand (ent - g_entities,
				 "print \"Invalid vote string.\n\"");
	  return;
	  break;
	}
    }

  // list of available commands
  valid = false;

  //G_Printf ("number commands : %i\n", vote_number_commands);

  for (i = 0; i < vote_number_commands; i++)
    {
      if (!Q_stricmp (arg1, vote_command_list[i].displayname))
	{
	  valid = true; // we got a valid one
	  break;
	}
    }

  // command wasn't valid
  if (!valid)
    {
      int j, len;
      int tlen;
      gi->SendServerCommand (ent - g_entities,
			     "print \"Invalid vote string.\n\"");

      Q_strncpyz (votemsg, "print \"Vote commands are: ", sizeof (votemsg));
      len = strlen (votemsg);

      for (j = 0; j < vote_number_commands; j++)
	{
	  tlen = strlen (vote_command_list[j].displayname);

	  // keep a decent formating
	  if ((len + tlen) >= 70)
	    {
	      // we arbitrary offset available commands
	      Q_strcat (votemsg, sizeof(votemsg), "\n");
	      Q_strcat (votemsg, sizeof(votemsg), "                   ");
	      len = 19;
	    }
	  Q_strcat (votemsg, sizeof(votemsg), vote_command_list[j].displayname);
	  Q_strcat (votemsg, sizeof(votemsg), " "); // could add an if len==0
	  len += tlen + 1;
	}
      // and don't forget to jump line
      Q_strcat (votemsg, sizeof(votemsg), "\n");

      gi->SendServerCommand (ent - g_entities, votemsg);
      return;
    }

  // the command is valid be has no arguments, we don't want to vote
  // but to know its current value (if it's a cvar)
  if (!*arg2 && vote_command_list[i].isCvar)
    {
      char * msg;
      char * cvarval;

      cvarval = gi->Cvar_VariableString (vote_command_list[i].cmdname);

      // the cvar isn't set and/or not existing, report ?
      // if (!*cvarval)

      msg = va ("print \"%s : current value is %s.\n\"",
		vote_command_list[i].displayname, cvarval);
      gi->SendServerCommand (ent - g_entities, msg);

      return;
    }

  // if there is still a vote to be executed
  if (level.voteExecuteTime)
    {
      level.voteExecuteTime = 0;
      gi->SendConsoleCommand (EXEC_APPEND, va ("%s\n", level.voteString));
    }

  // we have the indice of the valid function
  // replace arg1 with the game command name
  Q_strncpyz (arg1, vote_command_list[i].cmdname, sizeof(arg1));
  // call the associated function (set the vote string)
  vote_command_list[i].func (ent, arg1, arg2);

  // display the votestring in console
  gi->SendServerCommand (-1, va ("print \"%s called a vote : %s %s\n\"",
				 ent->client->pers.netname, arg1, arg2));

  // start the voting, the caller autoamtically votes yes
  level.voteTime = game.time;
  level.voteYes = 1;
  level.voteNo = 0;

  for (i = 0; i < level.maxclients; i++)
    {
      level.clients[i].ps.eFlags &= ~EF_VOTED;
    }
  ent->client->ps.eFlags |= EF_VOTED;

  gi->SetConfigstring (CS_VOTE_TIME, va ("%i", level.voteTime));
  gi->SetConfigstring (CS_VOTE_STRING, level.voteDisplayString);
  gi->SetConfigstring (CS_VOTE_YES, va ("%i", level.voteYes));
  gi->SetConfigstring (CS_VOTE_NO, va ("%i", level.voteNo));
}

/*
==================
Cmd_Vote_f
==================
*/
void
Cmd_Vote_f (gentity_t * ent)
{
  const char *msg;

  if (!level.voteTime)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"No vote in progress.\n\"");
      return;
    }
  if (ent->client->ps.eFlags & EF_VOTED)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"Vote already cast.\n\"");
      return;
    }
  if (ent->client->sess.sessionTeam == TEAM_SPECTATOR)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"Not allowed to vote as spectator.\n\"");
      return;
    }

  gi->SendServerCommand (ent - g_entities, "print \"Vote cast.\n\"");

  ent->client->ps.eFlags |= EF_VOTED;

  msg = gi->Argv (1);

  if (msg[0] == 'y' || msg[0] == 'Y' || msg[0] == '1')
    {
      level.voteYes++;
      gi->SetConfigstring (CS_VOTE_YES, va ("%i", level.voteYes));
    }
  else
    {
      level.voteNo++;
      gi->SetConfigstring (CS_VOTE_NO, va ("%i", level.voteNo));
    }
  // a majority will be determined in CheckVote, which will also account
  // for players entering or leaving
}

/*
=================
Cmd_Ready_f
=================
*/
void
Cmd_Ready_f (gentity_t * ent)
{
  int clientNum;
  char * msg;

  clientNum = ent - g_entities;

  if (g_doWarmup->integer != 1)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"No warmup in this game\n\"");
      return;
    }

  if (level.matchStatus & MS_LIVE)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"Match already in progress.\n\"");
      return;
    }

  if (level.clients[clientNum].sess.sessionTeam >= TEAM_SPECTATOR)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"You must be in the game to get ready.\n\"");
      return;
    }

  level.clients[clientNum].pers.readyClient ^= 1;

  if (level.clients[clientNum].pers.readyClient == true)
    {
      msg = va ("print \"%s %sis ready\n\"", ent->client->pers.netname,
		S_COLOR_YELLOW);
      gi->SendServerCommand (-1, msg);

      msg = va ("cp \"%s %sis ready\n\"", ent->client->pers.netname,
		S_COLOR_YELLOW);
      gi->SendServerCommand (-1, msg);

    }
  else
    {
      msg = va ("print \"%s %sis not ready\n\"", ent->client->pers.netname,
		S_COLOR_YELLOW);
      gi->SendServerCommand (-1, msg);

      msg = va ("cp \"%s %sis not ready\n\"", ent->client->pers.netname,
		S_COLOR_YELLOW);
      gi->SendServerCommand (-1, msg);
    }
}

/*
=================
Cmd_SetViewpos_f
=================
*/
void
Cmd_SetViewpos_f (gentity_t * ent)
{
  vec3_t origin, angles;
  const char *arg;
  int i;
  char * msg;

  if (!gi->Cvar_VariableIntegerValue ("sv_cheats"))
    {
      msg = va ("print \"Cheats are not enabled on this server.\n\"");
      gi->SendServerCommand (ent - g_entities, msg);
      return;
    }

  if (gi->Argc () != 5)
    {
      gi->SendServerCommand (ent - g_entities,
			     va ("print \"usage: setviewpos x y z yaw\n\""));
      return;
    }

  VectorClear (angles);
  for (i = 0; i < 3; i++)
    {
      arg = gi->Argv (i + 1);
      origin[i] = atof (arg);
    }

  arg = gi->Argv (4);
  angles[YAW] = atof (arg);

  TeleportPlayer (ent, origin, angles);
}

/*
=================
Cmd_Drop_f
=================
*/
void
Cmd_Drop_f (gentity_t * ent)
{
  const char *str;
  const gitem_t *item;
  gentity_t *drop;
  int weapon;
  int nbargc, i;
  char * msg;

  nbargc = gi->Argc ();

  if (nbargc <= 1)
    {
      msg = "print \"Command drop. Arguments are : ammo, weapon and flag\n"
	"They can be combined : drop ammo weapon\n\"";
      gi->SendServerCommand (ent - g_entities, msg);
      return;
    }
  if (ent->client->sess.sessionTeam == TEAM_SPECTATOR)
    {
      msg = "print \"You must be an active player to drop items.\n\"";
      gi->SendServerCommand (ent - g_entities, msg);
      return;
    }
  if (ent->health < 1)
    {
      msg = "print \"You must be alive to drop something\n\"";
      gi->SendServerCommand (ent - g_entities, msg);
      return;
    }
  if (!BG_GametypeIsTeam (g_gametype->integer))
    {
      msg = "print \"This gametype doesn't allow dropping\n\"";
      gi->SendServerCommand (ent - g_entities, msg);
      return;
    }

  for (i = 1; i < nbargc; i++)
    {
      str = gi->Argv (i);

      if (!Q_strcmp ("weapon", str))
	{
	  weapon = ent->sh.s.weapon;
	  // check validity
	  if (weapon <= WP_NONE || weapon >= WP_NUM_WEAPONS)
	    {
	      //Com_Printf ("Not a valid weapon number : Cmd_Drop_f, weapon\n");
	      continue;
	    }
	  if (weapon == WP_GAUNTLET)
	    {
	      //gi->SendServerCommand (ent-g_entities,
	      //                      "print \"Can't drop this weapon\n\"");
	      continue;
	    }
	  if (!(ent->client->ps.stats[STAT_WEAPONS] & (1 << weapon)))
	    {
	      //gi->SendServerCommand (ent-g_entities,
	      //                      "print \"You must have the"
	      //                      "weapon to drop it\n\"");
	      continue;
	    }

	  item = BG_FindItemForWeapon (weapon);
	  drop = Drop_Player_Item (ent, item, 0, false);
	}
      else if (!Q_strcmp ("ammo", str))
	{
	  weapon = ent->sh.s.weapon;
	  // check validity
	  if (weapon <= WP_NONE || weapon >= WP_NUM_WEAPONS)
	    {
	      //Com_Printf ("Not a valid weapon number : Cmd_Drop_f, ammo\n");
	      continue;
	    }
	  if (weapon == WP_GAUNTLET)
	    {
	      //gi->SendServerCommand (ent-g_entities,
	      //                      "print \"Can't drop ammos for"
	      //                      "this weapon\n\"");
	      continue;
	    }
	  item = BG_FindItemForAmmo (weapon);
	  drop = Drop_Player_Item (ent, item, 0, false);
	}
      else if (!Q_strcmp ("flag", str))
	{
	  if (g_gametype->integer != GT_CTF)
	    continue;

	  item = NULL;
	  if (ent->client->ps.powerups[PW_REDFLAG])
	    {
	      item = BG_FindItemForPowerup (PW_REDFLAG);
	    }
	  else if (ent->client->ps.powerups[PW_BLUEFLAG])
	    {
	      item = BG_FindItemForPowerup (PW_BLUEFLAG);
	    }
	  else
	    {
	      //gi->SendServerCommand (ent-g_entities, "print \"You must"
	      //                       "have the flag to drop it\n\"");
	    }
	  if (item != NULL)
	    {
	      drop = Drop_Player_Item (ent, item, 0, true);
	    }
	}
    }
  return;
}

// ==============================================================

/*
void Q_strcat( char *dest, int destsize, const char *src );
*/
static char *
stat_weaponnames [] =
  {
    "NONE      ",
    "Gauntlet  ",
    "Machinegun",
    "Shotgun   ",
    "Grenade L ",
    "Rocket L  ",
    "Lightning ",
    "Railgun   ",
    "Plasmagun ",
    "BFG10k    "
  };

static char *
stat_itemnames [] =
  {
    "MH",
    "50H",
    "25H",
    "5H",
    "HA",
    "MA",
    "LA",
    "TA"
  };

static char
stat_itemcolor [] =
  {
    'K',
    'z',
    'p',
    'c',
    'K',
    'z',
    'p',
    'c'
  };

/*
=============
G_FillConsoleStats

Fill the given buffer with target stats
fullstats provides more infos, but can give hints during play
=============
*/
static void
G_FillConsoleStats (gentity_t * target, bool fullstats,
		    char * string, unsigned int stringsize,
		    char * string2, unsigned int string2size)
{
  gStatPlayer_t *stat;
  gStatWeapon_t *wstat;
  char *msg;
  int infoexpanded;
  int i;
  unsigned int j;
  int *ptr;

  stat = &(target->client->stats);

  //Com_Memset (string, 0, stringsize);
  //Com_Memset (string2, 0, string2size);
  //Q_strcat (string, stringsize, "print \"");

  // cannot use \t, quake console doesn't recognize it
  // player name
  Q_strcat (string, stringsize,
	    va (S_COLOR_GREEN "Statistics " S_COLOR_WHITE
		"for %-.30s" S_COLOR_WHITE " (id : %i)\n",
		target->client->pers.netname, target - g_entities));

  // header
  Q_strcat (string, stringsize,
	    "Weapon        Hit/Fired Ratio  Gvn/Rcvd  Kill Death Take Drop\n");
  Q_strcat (string, stringsize,
	    "--------------------------------------------------------------\n");

  // WEAPONS

  wstat = &(stat->weap[WP_GAUNTLET]);
  msg = va (S_COLOR_YELLOW "Gauntlet   : " S_COLOR_WHITE
	    "%4d/-       -  "
	    S_COLOR_LIGHTBLUE "%5d/%-5d " S_COLOR_WHITE
	    S_COLOR_YELLOW "%4d  " S_COLOR_RED "%4d    "
	    S_COLOR_WHITE "-    -\n", wstat->hit,
	    wstat->damagegiven, wstat->damagereceived,
	    wstat->kill, wstat->death);
  Q_strcat (string, stringsize, msg);

  // loop for all other weapons, skip if the weapon is not available
  for (i = WP_MACHINEGUN; i < WP_NUM_WEAPONS; i++)
    {
      wstat = &(stat->weap[i]);

      // don't print if there's nothing to see (structure is empty)
      if (Q_IsMemNull (wstat, sizeof (wstat)))
	continue;

      msg = va (S_COLOR_YELLOW "%s : " S_COLOR_WHITE
		S_COLOR_LIGHTGREEN "%4d" S_COLOR_WHITE "/" S_COLOR_LIGHTPURPLE "%-4d "
		S_COLOR_ORANGE2 "%5.1f "
		S_COLOR_LIGHTBLUE "%5d/%-5d "
		S_COLOR_GREEN "%4d  " S_COLOR_RED "%4d "
		S_COLOR_GREEN "%4d " S_COLOR_RED "%4d\n",
		stat_weaponnames[i], wstat->hit, wstat->fired,
		(wstat->fired ? ((float)wstat->hit/wstat->fired) : 0.0f)* 100.0f,
		wstat->damagegiven, wstat->damagereceived,
		wstat->kill, wstat->death,
		wstat->picked, wstat->dropped);
      Q_strcat (string, stringsize, msg);
    }

  // GENERAL INFOS
  // we will output these one in the second buffer

  Q_strcat (string2, string2size, "\n");

  msg = va (S_COLOR_WHITE "Damage Given/Received : "
	    S_COLOR_GREEN "%d" S_COLOR_WHITE " / " S_COLOR_RED "%-d   "
	    S_COLOR_WHITE "(ratio : " S_COLOR_YELLOW "%4.1f" S_COLOR_WHITE ")\n",
	    stat->totdamage_done, stat->totdamage_taken,
	    (stat->totdamage_taken ?
	     ((float)stat->totdamage_done / stat->totdamage_taken) : 0.0f));

  Q_strcat (string2, string2size, msg);

  // ITEMS

  // health
  Q_strcat (string2, string2size,
	    va (S_COLOR_YELLOW "Health taken : " S_COLOR_WHITE "%d", stat->items.totlife));

  infoexpanded = 0;
  // look if we took health
  for (i = ITEM_STAT_MH; i <= ITEM_STAT_TH; i++)
    {
      if (stat->items.life[i])
	{
	  float ratio;

	  if (!infoexpanded)
	    {
	      Q_strcat (string2, string2size, " (");
	    }
	  else
	    {
	      Q_strcat (string2, string2size, " - ");
	    }

	  if (fullstats)
	    {
	      ratio = ((float)stat->items.life[i]
		       / (float)level.stats.spawned[i]);

	      Q_strcat (string2, string2size,
			va ("%d ^%c%s" S_COLOR_WHITE "[%3.2f]",
			    stat->items.life[i], stat_itemcolor[i],
			    stat_itemnames[i], ratio));
	    }
	  else
	    {
	      Q_strcat (string2, string2size,
			va ("%d ^%c%s" S_COLOR_WHITE, stat->items.life[i],
			    stat_itemcolor[i], stat_itemnames[i]));
	    }

	  infoexpanded = 1;
	}
    }

  // close and endline
  if (infoexpanded)
    Q_strcat (string2, string2size, ")");
  Q_strcat (string2, string2size, "\n");

  // armor
  Q_strcat (string2, string2size,
	    va (S_COLOR_YELLOW "Armor  taken : " S_COLOR_WHITE "%d", stat->items.totarmor));

  infoexpanded = 0;
  // look if we took armor
  for (i = ITEM_STAT_RA; i <= ITEM_STAT_TA; i++)
    {
      if (stat->items.life[i])
	{
	  float ratio;

	  if (!infoexpanded)
	    {
	      Q_strcat (string2, string2size, " (");
	      infoexpanded = 1;
	    }
	  else
	    {
	      Q_strcat (string2, string2size, " - ");
	    }

	  if (fullstats)
	    {
	      ratio = ((float)stat->items.life[i]
		       / (float)level.stats.spawned[i]);

	      Q_strcat (string2, string2size,
			va ("%d ^%c%s" S_COLOR_WHITE "[%3.2f]",
			    stat->items.life[i], stat_itemcolor[i],
			    stat_itemnames[i], ratio));
	    }
	  else
	    {
	      Q_strcat (string2, string2size,
			va ("%d ^%c%s" S_COLOR_WHITE, stat->items.life[i],
			    stat_itemcolor[i], stat_itemnames[i]));
	    }

	  infoexpanded = 1;
	}
    }

  // close and endline
  if (infoexpanded)
    Q_strcat (string2, string2size, ")");
  Q_strcat (string2, string2size, "\n");

  // if we play TDM also show the amount of team damage
  if (g_gametype->integer == GT_TEAM)
    {
      Q_strcat (string2, string2size,
		va (S_COLOR_RED "Team Damage  " S_COLOR_WHITE ": %d\n",
		    stat->teamdamage));
    }

  // end of the message
  //Q_strcat (string, stringsize, "\"");
}

/*
=============
G_FillItemsCTFConsoleStats

Fills a buffer with the powerup stats
of the target player
=============
*/
static void
G_FillItemsCTFConsoleStats (lvl_t * level, gentity_t * target,
			    char * string, unsigned int stringsize)
{
  gStatItemPowerup_t *powerups;
  char *msg;
  const gitem_t *item;
  int i;
  int ftsec, ftmin;

  powerups = target->client->stats.powerups;

  // jump a line before starting the printings
  Q_strcat (string, stringsize, "\n"
	    "Items run     Cnt    Time\n"
	    "-------------------------\n");

  // print flag status
  if (target->client->stats.ctf.picks)
    {
      ftsec = (target->client->stats.ctf.flagtime / 1000) % 60;
      ftmin = (target->client->stats.ctf.flagtime / 60000);

      if (ftmin)
	{
	  msg = va (S_COLOR_YELLOW "Flag  " S_COLOR_WHITE
		    "        %3d  %3d:%02d\n",
		    target->client->stats.ctf.picks, ftmin, ftsec);
	}
      else
	{
	  msg = va (S_COLOR_YELLOW "Flag  " S_COLOR_WHITE
		    "        %3d    0:%02d\n",
		    target->client->stats.ctf.picks, ftsec);
	}
      Q_strcat (string, stringsize, msg);
    }

  for (i = PW_QUAD; i < PW_NUM_POWERUPS; i++)
    {
      if (powerups[i].count)
	{
	  item = BG_FindItemForPowerup (i);

	  ftmin = (powerups[i].time / 60000);
	  ftsec = (powerups[i].time / 1000) % 60;

	  if (ftmin)
	    {
	      msg = va (S_COLOR_YELLOW "%-12s " S_COLOR_WHITE
			" %3d   %3d:%02d\n", item->pickup_name,
			powerups[i].count, ftmin, ftsec);
	    }
	  else
	    {
	      msg = va (S_COLOR_YELLOW "%-12s " S_COLOR_WHITE
			" %3d    0:%02d\n", item->pickup_name,
			powerups[i].count, ftsec);
	    }
	  Q_strcat (string, stringsize, msg);
	}
    }
}

/*
=============
G_FillCTFConsoleStats

fills the buffer with a summary of both ctf team stats
=============
*/
static void
G_FillCTFConsoleStats (lvl_t * level, gentity_t * targ, char * string, unsigned int stringsize)
{
  gStatPlayer_t *stat;
  gStatPlayerCtf_t *ctfstat;
  gclient_t *cl;
  char *msg;
  int i, k;
  team_t j;
  int numSorted;
  int ftsec, ftmin;
  int totkill, totdeath, totrun, totcap, totast, totdef, totrtn, totscore;
  int tottime;
  int plen;
  char *p;
  bool containColors;

  numSorted = level->numConnectedClients;

  Q_strcat (string, stringsize, "\n"
	    "Team Player          Kll Dth Run  Held " S_COLOR_YELLOW
	    "Cap" S_COLOR_WHITE " Ast Def Rtn  Score\n"
	    "-------------------------------------------------------------\n");

  for (j = TEAM_RED; j <= TEAM_BLUE; j++)
    {
      totkill = totdeath = totrun = totcap = 0;
      totast = totdef = totrtn = totscore = 0;
      tottime = 0;

      // on the second loop add one more line as a separator
      if (j == TEAM_BLUE)
	Q_strcat (string, stringsize, "\n");

      for (i = 0; i < numSorted; i++)
	{
	  cl = &level->clients[level->sortedClients[i]];

	  // not playing
	  if (cl->sess.sessionTeam >= TEAM_SPECTATOR)
	    continue;

	  // not displaying this team
	  if (cl->sess.sessionTeam != j)
	    continue;

	  // stats structures
	  stat = &(cl->stats);
	  ctfstat = &(cl->stats.ctf);

	  if (cl->sess.sessionTeam == TEAM_RED)
	    Q_strcat (string, stringsize, S_COLOR_RED "Red " S_COLOR_WHITE);
	  else
	    Q_strcat (string, stringsize, S_COLOR_BLUE "Blue" S_COLOR_WHITE);

	  // mark the targeted player
	  if (cl == targ->client)
	    Q_strcat (string, stringsize, S_COLOR_GREEN "*");
	  else
	    Q_strcat (string, stringsize, " ");

	  // handle colorized names
	  // first look if the string is colorized
	  // then if we will have to cut it
	  // if, properly truncate to the good displayed char
	  // otherwise print the string and pad with spaces for alignment
	  p = cl->pers.netname;
	  containColors = false;

	  while (*p)
	    {
	      if (Q_IsColorString (p))
		{
		  containColors = true;
		  break;
		}
	      p++;
	    }

	  // we have colors
	  if (containColors)
	    {
	      plen = Q_PrintStrlen (cl->pers.netname);

	      // string is too long
	      if (plen > 15)
		{
		  char cleanName[MAX_NETNAME];
		  int index;

		  Q_strncpyz (cleanName, cl->pers.netname, sizeof (cleanName));

		  // we want to print 15 characters
		  index = Q_PrintStrPos (cleanName, 15);
		  // end the string here
		  cleanName[index] = '\0';

		  msg = va (S_COLOR_WHITE "%-s " S_COLOR_WHITE, cleanName);
		  Q_strcat (string, stringsize, msg);
		}
	      else
		{
		  msg = va (S_COLOR_WHITE "%-s " S_COLOR_WHITE, cl->pers.netname);
		  Q_strcat (string, stringsize, msg);

		  // pad with spaces
		  for (k = 15; k > plen; k--)
		    {
		      Q_strcat (string, stringsize, " ");
		    }
		}
	    }
	  else // regular display
	    {
	      msg = va ("%-15s " S_COLOR_WHITE, cl->pers.netname);
	      Q_strcat (string, stringsize, msg);
	    }

	  ftmin = (ctfstat->flagtime / 60000);
	  ftsec = (ctfstat->flagtime / 1000) % 60;

	  msg = va ("%3d %3d %3d %2d:%02d %3d %3d %3d %3d  "
		    S_COLOR_YELLOW "  %3d" S_COLOR_WHITE "\n",
		    stat->totkill, stat->totdeath,
		    ctfstat->picks, ftmin, ftsec,
		    ctfstat->captures, ctfstat->assists,
		    ctfstat->defends, ctfstat->returns,
		    cl->ps.persistant[PERS_SCORE]);

	  Q_strcat (string, stringsize, msg);

	  // backup all the values to make the sum for the team
	  // NOTE : the best would be to keep track of these during
	  // the game, so that an eventual disconnect wont bend results
	  totkill += stat->totkill;
	  totdeath += stat->totdeath;
	  totrun += ctfstat->picks;
	  totcap += ctfstat->captures;
	  totast += ctfstat->assists;
	  totdef += ctfstat->defends;
	  totrtn += ctfstat->returns;
	  totscore += cl->ps.persistant[PERS_SCORE];

	  tottime += ctfstat->flagtime;
	}

      Q_strcat (string, stringsize, "---------------------------------"
		"----------------------------\n");

      if (j == TEAM_RED)
	Q_strcat (string, stringsize, S_COLOR_RED "Red "
		  S_COLOR_YELLOW " Total           " S_COLOR_CYAN);
      else
	Q_strcat (string, stringsize, S_COLOR_BLUE "Blue"
		  S_COLOR_YELLOW " Total           " S_COLOR_CYAN);

      ftmin = (tottime / 60000);
      ftsec = (tottime / 1000) % 60;

      msg = va ("%3d %3d %3d %2d:%02d " S_COLOR_GREEN "%3d" S_COLOR_CYAN
		" %3d %3d %3d  " S_COLOR_YELLOW "  %3d" S_COLOR_WHITE "\n",
		totkill, totdeath, totrun, ftmin, ftsec,
		totcap, totast, totdef, totrtn, totscore);

      Q_strcat (string, stringsize, msg);
    }
}

/*
=============
G_FillItemsTDMConsoleStats

Fills a buffer with the powerup stats
of the target player
=============
*/
static void
G_FillItemsTDMConsoleStats (lvl_t * level, gentity_t * target,
			    char * string, unsigned int stringsize)
{
  gStatItemPowerup_t *powerups;
  char *msg;
  const gitem_t *item;
  int i;
  int ftsec, ftmin;

  powerups = target->client->stats.powerups;

  // jump a line before starting the printings
  Q_strcat (string, stringsize, "\n"
	    "Items run     Cnt    Time\n"
	    "-------------------------\n");

  for (i = PW_QUAD; i < PW_NUM_POWERUPS; i++)
    {
      if (powerups[i].count)
	{
	  item = BG_FindItemForPowerup (i);

	  ftmin = (powerups[i].time / 60000);
	  ftsec = (powerups[i].time / 1000) % 60;

	  if (ftmin)
	    {
	      msg = va (S_COLOR_YELLOW "%-12s " S_COLOR_WHITE
			" %3d   %3d:%02d\n", item->pickup_name,
			powerups[i].count, ftmin, ftsec);
	    }
	  else
	    {
	      msg = va (S_COLOR_YELLOW "%-12s " S_COLOR_WHITE
			" %3d    0:%02d\n", item->pickup_name,
			powerups[i].count, ftsec);
	    }
	  Q_strcat (string, stringsize, msg);
	}
    }
}

/*
=============
G_FillTDMConsoleStats

fills the buffer with a summary of both tdm team stats
=============
*/
static void
G_FillTDMConsoleStats (lvl_t * level, gentity_t * targ,
		       char * string, unsigned int stringsize)
{
  gStatPlayer_t *stat;
  gclient_t *cl;
  char *msg;
  int i, k;
  team_t j;
  int numSorted;
  int totkill, totdeath, totscore;
  int totnet, tottk, totsuic, totgiven, totreceived;
  int netscore;
  int plen;
  char *p;
  bool containColors;

  numSorted = level->numConnectedClients;

  Q_strcat (string, stringsize, "\n"
	    "Team Player          Kll Dth  Net  Tk Sui   "
	    "Gvn    Rvd  Score\n"
	    "-------------------------------------------------------------\n");

  for (j = TEAM_RED; j <= TEAM_BLUE; j++)
    {
      totkill = totdeath = 0;
      totnet = tottk = totsuic = totgiven = totreceived = 0;
      totscore = 0;

      // on the second loop add one more line as a separator
      if (j == TEAM_BLUE)
	Q_strcat (string, stringsize, "\n");

      for (i = 0; i < numSorted; i++)
	{
	  cl = &level->clients[level->sortedClients[i]];

	  // not playing
	  if (cl->sess.sessionTeam >= TEAM_SPECTATOR)
	    continue;

	  // not displaying this team
	  if (cl->sess.sessionTeam != j)
	    continue;

	  // stats structures
	  stat = &(cl->stats);

	  if (cl->sess.sessionTeam == TEAM_RED)
	    Q_strcat (string, stringsize, S_COLOR_RED "Red " S_COLOR_WHITE);
	  else
	    Q_strcat (string, stringsize, S_COLOR_BLUE "Blue" S_COLOR_WHITE);

	  // mark the targeted player
	  if (cl == targ->client)
	    Q_strcat (string, stringsize, S_COLOR_GREEN "*");
	  else
	    Q_strcat (string, stringsize, " ");

	  // handle colorized names
	  // first look if the string is colorized
	  // then if we will have to cut it
	  // if, properly truncate to the good displayed char
	  // otherwise print the string and pad with spaces for alignment
	  p = cl->pers.netname;
	  containColors = false;

	  while (*p)
	    {
	      if (Q_IsColorString (p))
		{
		  containColors = true;
		  break;
		}
	      p++;
	    }

	  // we have colors
	  if (containColors)
	    {
	      plen = Q_PrintStrlen (cl->pers.netname);

	      // string is too long
	      if (plen > 15)
		{
		  char cleanName[MAX_NETNAME];
		  int index;

		  Q_strncpyz (cleanName, cl->pers.netname, sizeof (cleanName));

		  // we want to print 15 characters
		  index = Q_PrintStrPos (cleanName, 15);
		  // end the string here
		  cleanName[index] = '\0';

		  msg = va (S_COLOR_WHITE "%-s " S_COLOR_WHITE, cleanName);
		  Q_strcat (string, stringsize, msg);
		}
	      else
		{
		  msg = va (S_COLOR_WHITE "%-s " S_COLOR_WHITE, cl->pers.netname);
		  Q_strcat (string, stringsize, msg);

		  // pad with spaces
		  for (k = 15; k > plen; k--)
		    {
		      Q_strcat (string, stringsize, " ");
		    }
		}
	    }
	  else // regular display
	    {
	      msg = va ("%-15s " S_COLOR_WHITE, cl->pers.netname);
	      Q_strcat (string, stringsize, msg);
	    }

	  netscore = (stat->totkill - stat->totdeath
		      - stat->teamkill - stat->suicidecounter);

	  msg = va ("%3d %3d  " S_COLOR_MAGENTA "%3d" S_COLOR_WHITE
		    " %3d %3d %5d  %5d " S_COLOR_YELLOW
		    "   %3d" S_COLOR_WHITE "\n",
		    stat->totkill, stat->totdeath, netscore,
		    stat->teamkill, stat->suicidecounter,
		    stat->totdamage_done, stat->totdamage_taken,
		    cl->ps.persistant[PERS_SCORE]);

	  Q_strcat (string, stringsize, msg);

	  // backup all the values to make the sum for the team
	  // NOTE : the best would be to keep track of these during
	  // the game, so that an eventual disconnect wont bend results
	  totkill += stat->totkill;
	  totdeath += stat->totdeath;
	  totnet += netscore;
	  tottk += stat->teamkill;
	  totsuic += stat->suicidecounter;
	  totgiven += stat->totdamage_done;
	  totreceived += stat->totdamage_taken;
	  totscore += cl->ps.persistant[PERS_SCORE];
	}

      Q_strcat (string, stringsize, "---------------------------------"
		"----------------------------\n");

      if (j == TEAM_RED)
	Q_strcat (string, stringsize, S_COLOR_RED "Red "
		  S_COLOR_YELLOW " Total           " S_COLOR_CYAN);
      else
	Q_strcat (string, stringsize, S_COLOR_BLUE "Blue"
		  S_COLOR_YELLOW " Total           " S_COLOR_CYAN);

      msg = va ("%3d %3d  %3d %3d %3d %5d  %5d    %3d\n",
		totkill, totdeath, totnet, tottk, totsuic,
		totgiven, totreceived, totscore);

      Q_strcat (string, stringsize, msg);
    }

  // display a teamstat summary ?
  // from level.teamStatsRed/Blue structure
}

/*
===================
G_ConsoleStats

use this one to call from the code
===================
*/
void
G_ConsoleStats (lvl_t * level, gentity_t * ent, gentity_t * target, bool finalmsg)
{
  int offset;
  char string[MAX_STRING_CHARS];
  char string2[MAX_STRING_CHARS];

  // doing this way avoid doing a full buffer copy
  Q_strncpyz (string, "print \"", sizeof (string));
  Q_strncpyz (string2, "print \"", sizeof (string2));

  // 'print "'
  offset = 7;

  // keep space for the ending \" and 0
  G_FillConsoleStats (target, finalmsg,
		      string + offset, sizeof (string) - (offset + 2),
		      string2 + offset, sizeof (string2) - (offset + 2));

  Q_strcat (string, sizeof (string), "\"");
  Q_strcat (string2, sizeof (string2), "\"");

  // send it
  gi->SendServerCommand (ent - g_entities, string);
  gi->SendServerCommand (ent - g_entities, string2);
}

/*
===================
G_CTFConsoleStats

ctf specifics player stats
ent is the entity to send to
target is the entity we want the infos
===================
*/
void
G_CTFConsoleStats (lvl_t * level, gentity_t * ent, gentity_t * target)
{
  int offset;
  char string[MAX_STRING_CHARS];
  char string2[MAX_STRING_CHARS];

  // we will progressivly cat on the buffer
  Q_strncpyz (string, "print \"", sizeof (string));
  Q_strncpyz (string2, "print \"", sizeof (string2));

  // 'print "'
  offset = 7;

  // keep space for the ending \" and 0
  G_FillItemsCTFConsoleStats (level, target, string + offset,
			      sizeof (string) - (offset + 2));
  // summary of both teams
  G_FillCTFConsoleStats (level, target, string2 + offset,
			 sizeof (string2) - (offset + 2));

  // ending the string
  Q_strcat (string, sizeof (string), "\"");
  Q_strcat (string2, sizeof (string2), "\"");

  // send it
  gi->SendServerCommand (ent - g_entities, string);
  gi->SendServerCommand (ent - g_entities, string2);
}

/*
===================
G_TDMConsoleStats

ctf specifics player stats
ent is the entity to send to
target is the entity we want the infos
===================
*/
void
G_TDMConsoleStats (lvl_t * level, gentity_t * ent, gentity_t * target)
{
  int offset;
  char string[MAX_STRING_CHARS];
  char string2[MAX_STRING_CHARS];

  // we will progressivly cat on the buffer
  Q_strncpyz (string, "print \"", sizeof (string));
  Q_strncpyz (string2, "print \"", sizeof (string2));

  // 'print "'
  offset = 7;

  // keep space for the ending \" and 0
  G_FillItemsTDMConsoleStats (level, target, string + offset,
			      sizeof (string) - (offset + 2));
  // summary of both teams
  G_FillTDMConsoleStats (level, target, string2 + offset,
			 sizeof (string2) - (offset + 2));

  // ending the string
  Q_strcat (string, sizeof (string), "\"");
  Q_strcat (string2, sizeof (string2), "\"");

  // send it
  gi->SendServerCommand (ent - g_entities, string);
  gi->SendServerCommand (ent - g_entities, string2);
}

/*
===================
Cmd_ConsoleStats_f

===================
*/
void
Cmd_ConsoleStats_f (gentity_t * ent)
{
  int clientNum;
  int nbargc;
  gentity_t *target = NULL;

  nbargc = gi->Argc ();

  // we might have received a malformed command
  if ((nbargc != 1) && (nbargc != 2))
    return;

  // no more info take stats from calling entity
  // this is BAD, it shouldn't be done server side
  if (nbargc == 1)
    {
      // we are spectating
      if (ent->client->sess.sessionTeam >= TEAM_SPECTATOR)
	{
	  // and following someone
	  if ((ent->client->sess.spectatorState == SPECTATOR_FOLLOW)
	      && (ent->client->sess.spectatorClient >= 0))
	    {
	      target = g_entities + ent->client->sess.spectatorClient;
	    }
	}
      else // 1st POV playing player
	{
	  target = ent;
	}
    }
  else // 2 args, the good way
    {
      // we want a specific player, be sure its state is valid
      clientNum = atoi (gi->Argv (1));

      // out of bounds
      if ((clientNum < 0) || (clientNum >= level.maxclients))
	return;

      // perfom some checks, is this enough ?
      target = g_entities + clientNum;
    }

  // if we aren't following a player or its state is not valid stop here
  if ((target == NULL) || (target->client == NULL) || (!target->inuse))
    {
      return;	// not fully in game yet
    }

  G_ConsoleStats (&level, ent, target, false);
}

/*
=============
Cmd_BoxStats_f

=============
*/
void
Cmd_BoxStats_f (gentity_t * ent)
{
  char string[MAX_STRING_CHARS];
  char string2[MAX_STRING_CHARS];
  gentity_t *target = NULL;
  int nbargc;
  int clientNum;
  int offset, offset2;

  nbargc = gi->Argc ();

  // we might have received a malformed command
  if ((nbargc != 1) && (nbargc != 2))
    return;

  // no argument (should'nt happend)
  // return the caller's stats
  if (nbargc == 1)
    {
      target = ent;
    }
  else
    {
      // we want a specific player, be sure its state is valid
      clientNum = atoi (gi->Argv (1));

      // out of bounds
      if ((clientNum < 0) || (clientNum >= level.maxclients))
	return;

      // perfom some checks, is this enough ?
      target = g_entities + clientNum;
    }

  // if we aren't following a player or its state is not valid stop here
  if ((target == NULL) || (target->client == NULL) || (!target->inuse))
    {
      return;	// not fully in game yet
    }

  Q_strncpyz (string, "boxprint \"", sizeof (string));
  Q_strncpyz (string2, "boxprint2 \"", sizeof (string2));

  // 'boxprint "'
  offset = 10;
  // 'boxprint2 "'
  offset2 = 11;

  // keep space for the ending \" and 0
  G_FillConsoleStats (target, false,
		      string + offset, sizeof (string) - (offset + 2),
		      string2 + offset2, sizeof (string) - (offset + 2));

  Q_strcat (string, sizeof (string), "\"");

  gi->SendServerCommand (ent - g_entities, string);
  gi->SendServerCommand (ent - g_entities, string2);
}

/*
=============
Cmd_Timeout_f

calling a timeout to pause the game
=============
*/
void
Cmd_Timeout_f (gentity_t * ent)
{
  if (!(level.matchStatus & MS_LIVE))
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"No match in progress.\n\"");
      return;
    }

  if (game.paused)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"The game is already in pause.\n\"");
      return;
    }

  game.paused = true;

  game.timeoutTime = game.time;
  game.timeinTime = 0;

  gi->SetConfigstring (CS_GAME_PAUSED, "1");

  gi->SendServerCommand (-1, va ("print \"%s " S_COLOR_WHITE "called a timeout.\n\"",
				 ent->client->pers.netname));

  gi->SendServerCommand (-1, va ("cp \"%s\n" S_COLOR_WHITE "called a timeout.\n\n"
				 "Game is paused.\"", ent->client->pers.netname));
}

// resume 5 seconds after the announce
#define TIMEIN_DELAY 5000
// i guess you're old enough not to make this bigger than TIMEIN_DELAY
#define TIMEIN_ANNOUNCER_TIME 3000

/*
=============
Cmd_Timein_f

timein, ask to resume the game
=============
*/
void
Cmd_Timein_f (gentity_t * ent)
{
  if (!(level.matchStatus & MS_LIVE))
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"No match in progress.\n\"");
      return;
    }

  if (!game.paused)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"The game isn't paused.\n\"");
      return;
    }

  if (game.timeinTime)
    {
      gi->SendServerCommand (ent - g_entities,
			     "print \"Timein has already be called\n\"");
      return;
    }

  // time we want the party to resume
  game.timeinTime = game.time + TIMEIN_DELAY;
  // time we start informing player the party is gonna resume
  game.timeoutAnnoncerTime = game.timeinTime - TIMEIN_ANNOUNCER_TIME;

  // inform someone called a timein
  gi->SendServerCommand (-1, va ("print \"%s " S_COLOR_WHITE "called a timein.\n\"",
				 ent->client->pers.netname));
  gi->SendServerCommand (-1, va ("cp \"%s\n" S_COLOR_WHITE "called a timein.\n\"",
				 ent->client->pers.netname));
}

/*
=================
ClientCommand
=================
*/
void
ClientCommand (int clientNum)
{
  gentity_t *ent;
  const char *cmd;

  ent = g_entities + clientNum;
  if (!ent->client || (ent->client->pers.connected != CON_CONNECTED))
    {
      return;		// not fully in game yet
    }

  cmd = gi->Argv (0);

  if (Q_stricmp (cmd, "say") == 0)
    {
      Cmd_Say_f (ent, SAY_ALL, false);
      return;
    }
  if (Q_stricmp (cmd, "say_team") == 0)
    {
      Cmd_Say_f (ent, SAY_TEAM, false);
      return;
    }
  if (Q_stricmp (cmd, "ready") == 0)
    {
      Cmd_Ready_f (ent);
      return;
    }
  if (Q_stricmp (cmd, "tell") == 0)
    {
      Cmd_Tell_f (ent);
      return;
    }
  if (Q_stricmp (cmd, "score") == 0)
    {
      Cmd_Score_f (ent);
      return;
    }
  if (Q_stricmp (cmd, "constats") == 0)
    {
      Cmd_ConsoleStats_f (ent);
      return;
    }
  if (Q_stricmp (cmd, "boxstats") == 0)
    {
      Cmd_BoxStats_f (ent);
      return;
    }

  // ignore all other commands when at intermission
  if (level.match.end_flag >= END_INTERMISSION)
    {
      Cmd_Say_f (ent, false, true);
      return;
    }

  if (Q_stricmp (cmd, "give") == 0)
    Cmd_Give_f (ent);
  else if (Q_stricmp (cmd, "god") == 0)
    Cmd_God_f (ent);
  else if (Q_stricmp (cmd, "notarget") == 0)
    Cmd_Notarget_f (ent);
  else if (Q_stricmp (cmd, "noclip") == 0)
    Cmd_Noclip_f (ent);
  else if (Q_stricmp (cmd, "kill") == 0)
    Cmd_Kill_f (ent);
  /*
  else if (Q_stricmp (cmd, "levelshot") == 0)
    Cmd_LevelShot_f (ent);
  */
  else if (Q_stricmp (cmd, "follow") == 0)
    Cmd_Follow_f (ent);
  else if (Q_stricmp (cmd, "follownext") == 0)
    Cmd_FollowCycle_f (ent, 1);
  else if (Q_stricmp (cmd, "followprev") == 0)
    Cmd_FollowCycle_f (ent, -1);
  else if (Q_stricmp (cmd, "team") == 0)
    Cmd_Team_f (ent);
  else if (Q_stricmp (cmd, "where") == 0)
    Cmd_Where_f (ent);
  else if (Q_stricmp (cmd, "callvote") == 0)
    Cmd_CallVote_f (ent);
  else if (Q_stricmp (cmd, "vote") == 0)
    Cmd_Vote_f (ent);
  else if (Q_stricmp (cmd, "setviewpos") == 0)
    Cmd_SetViewpos_f (ent);
  else if (Q_stricmp (cmd, "drop") == 0)
    Cmd_Drop_f (ent);
  else if (Q_stricmp (cmd, "timeout") == 0)
    Cmd_Timeout_f (ent);
  else if (Q_stricmp (cmd, "timein") == 0)
    Cmd_Timein_f (ent);
  else if (Q_stricmp (cmd, "setteamname") == 0)
    Cmd_SetTeamName_f (ent);
  else
    gi->SendServerCommand (clientNum, va ("print \"unknown cmd %s\n\"", cmd));
}

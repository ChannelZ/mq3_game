/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

/*
  Items are any object that a player can touch to gain some effect.

  Pickup will return the number of seconds until they should respawn.

  all items should pop when dropped in lava or slime

  Respawnable items don't actually go away when picked up, they are
  just made invisible and untouchable.  This allows them to ride
  movers and respawn apropriately.
*/

#define RESPAWN_ARMOR		25
#define RESPAWN_HEALTH		35
#define RESPAWN_AMMO		30
#define RESPAWN_HOLDABLE	60
#define RESPAWN_MEGAHEALTH	35
#define RESPAWN_POWERUP		120

//======================================================================

int
Pickup_Powerup (gentity_t * ent, gentity_t * other)
{
  int quantity;
  int i;
  gclient_t *client;
  int respawntime;

  if (!other->client->ps.powerups[ent->item->giTag])
    {
      // round timing to seconds to make multiple powerup timers
      // count in sync
      other->client->ps.powerups[ent->item->giTag] =
	level.time - (level.time % 1000);
    }

  if (ent->count)
    {
      quantity = ent->count;
    }
  else
    {
      quantity = ent->item->quantity;
    }

  other->client->ps.powerups[ent->item->giTag] += quantity * 1000;
  other->client->stats.powerups[ent->item->giTag].count++;

  if (other->client->sess.sessionTeam == TEAM_RED)
    level.teamStatsRed.powerups[ent->item->giTag].count++;
  else if (other->client->sess.sessionTeam == TEAM_BLUE)
    level.teamStatsBlue.powerups[ent->item->giTag].count++;

  // give any nearby players a "denied" anti-reward
  for (i = 0; i < level.maxclients; i++)
    {
      vec3_t delta;
      float len;
      vec3_t forward;
      trace_t tr;

      client = &level.clients[i];
      if (client == other->client)
	{
	  continue;
	}
      // not connected to game
      if (client->pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      // dead
      if (client->ps.stats[STAT_HEALTH] <= 0)
	{
	  continue;
	}

      // if same team in team game, no sound
      // cannot use OnSameTeam as it expects to g_entities, not clients
      if ((BG_GametypeIsTeam (g_gametype->integer))
	  && (other->client->sess.sessionTeam == client->sess.sessionTeam))
	{
	  continue;
	}

      // if too far away, no sound
      VectorSubtract (ent->sh.s.pos.trBase, client->ps.origin, delta);
      len = VectorNormalize (delta);
      if (len > 192)
	{
	  continue;
	}

      // if not facing, no sound
      /*
      AngleVectors (client->ps.viewangles, forward, NULL, NULL);
      if (DotProduct (delta, forward) < 0.4)
	{
	  continue;
	}
      */

      // if not line of sight, no sound
      gi->Trace (&tr, client->ps.origin, NULL, NULL, ent->sh.s.pos.trBase,
		 ENTITYNUM_NONE, CONTENTS_SOLID);
      if (tr.fraction != 1.0)
	{
	  continue;
	}

      // anti-reward
      client->ps.persistant[PERS_PLAYEREVENTS] ^= PLAYEREVENT_DENIEDREWARD;
    }

  if (g_item_powerup_respawn->integer)
    respawntime = g_item_powerup_respawn->integer;
  else
    respawntime = RESPAWN_POWERUP;

  return respawntime;
}

int
Pickup_Holdable (gentity_t * ent, gentity_t * other)
{
  other->client->ps.stats[STAT_HOLDABLE_ITEM] = ent->item - bg_itemlist;

  return RESPAWN_HOLDABLE;
}

//======================================================================

void
Add_Ammo (gentity_t * ent, int weapon, int count)
{
  ent->client->ps.ammo[weapon] += count;
  if (ent->client->ps.ammo[weapon] > maxammo_table[weapon])
    {
      ent->client->ps.ammo[weapon] = maxammo_table[weapon];
    }
}

int
Remove_Ammo (gentity_t * ent, int weapon, int count)
{
  int quantity;

  quantity = ent->client->ps.ammo[weapon] - count;
  if (quantity < 0)
    {
      quantity = ent->client->ps.ammo[weapon];
      ent->client->ps.ammo[weapon] = 0;
    }
  else
    {
      quantity = count;
      ent->client->ps.ammo[weapon] -= count;
    }
  return quantity;
}

int
Pickup_Ammo (gentity_t * ent, gentity_t * other)
{
  int quantity;

  if (ent->count)
    {
      quantity = ent->count;
    }
  else
    {
      quantity = ent->item->quantity;
    }

  Add_Ammo (other, ent->item->giTag, quantity);

  return RESPAWN_AMMO;
}

//======================================================================

int
Pickup_Weapon (gentity_t * ent, gentity_t * other)
{
  int quantity;

  // assume count is positive. take care of dropped items without ammo
  if (ent->count || (!ent->count && (ent->flags & FL_DROPPED_ITEM)))
    {
      quantity = ent->count;
      // add an upper limit to the pickage ammos quantity
      // like the regular ammo boxes ? (ammopicking_table)
      if (quantity > ent->item->quantity)
	{
	  quantity = ent->item->quantity;
	}
    }
  else
    {
      quantity = ent->item->quantity;
    }

  // teamplay and dropped weapons always have full ammo
  if (!(ent->flags & FL_DROPPED_ITEM) && (g_gametype->integer != GT_TEAM))
    {
      // respawning rules
      // drop the quantity if we already have over the minimum
      if (other->client->ps.ammo[ent->item->giTag] < quantity)
	{
	  quantity = quantity - other->client->ps.ammo[ent->item->giTag];
	}
      else
	{
	  quantity = 0;		// only add a single shot
	}
    }

  // add the weapon
  other->client->ps.stats[STAT_WEAPONS] |= (1 << ent->item->giTag);

  Add_Ammo (other, ent->item->giTag, quantity);

  // update stats, picked this item
  other->client->stats.weap[ent->item->giTag].picked++;

  if (other->client->sess.sessionTeam == TEAM_RED)
    level.teamStatsRed.weap[ent->item->giTag].picked++;
  else if (other->client->sess.sessionTeam == TEAM_BLUE)
    level.teamStatsBlue.weap[ent->item->giTag].picked++;

  // team deathmatch has slow weapon respawns
  if (g_gametype->integer == GT_TEAM)
    {
      return g_weaponTeamRespawn->integer;
    }

  return g_weaponRespawn->integer;
}

//======================================================================

int
Pickup_Health (gentity_t * ent, gentity_t * other)
{
  int max;
  int quantity;

  // regular bubbles can't make you over max_health (typically 100)
  if (ent->item->quantity != 5 && ent->item->quantity != 100)
    {
      max = other->client->ps.stats[STAT_MAX_HEALTH];
    }
  // but 5 and mega health can
  else
    {
      max = other->client->ps.stats[STAT_MAX_HEALTH] * 2;
    }

  if (ent->count)
    {
      quantity = ent->count;
    }
  else
    {
      quantity = ent->item->quantity;
    }

  other->health += quantity;

  if (other->health > max)
    {
      other->health = max;
    }
  other->client->ps.stats[STAT_HEALTH] = other->health;

  // health stats; client is valid and alive
  other->client->stats.items.totlife += quantity;

  // stats; not suited for upgrades
  switch (quantity)
    {
      // only mh tracked atm
    case 100:
      other->client->stats.items.life[ITEM_STAT_MH]++;
      break;
    case 50:
      other->client->stats.items.life[ITEM_STAT_MDH]++;
      break;
    case 25:
      other->client->stats.items.life[ITEM_STAT_LH]++;
      break;
    case 5:
      other->client->stats.items.life[ITEM_STAT_TH]++;
      break;
    default:
      break;
    }

  if (ent->item->quantity == 100)
    {	// mega health respawns slow
      return RESPAWN_MEGAHEALTH;
    }

  return RESPAWN_HEALTH;
}

//======================================================================

int
Pickup_Armor (gentity_t * ent, gentity_t * other)
{
  if (!g_armorSystem->integer)
    {
      other->client->ps.stats[STAT_ARMOR] += ent->item->quantity;
      if ((other->client->ps.stats[STAT_ARMOR]) >
	  (other->client->ps.stats[STAT_MAX_HEALTH] * 2))
	{
	  other->client->ps.stats[STAT_ARMOR] =
	    other->client->ps.stats[STAT_MAX_HEALTH] * 2;
	}
    }
  else
    {
      // cpm armor system
      if (ent->item->quantity == 100) // RA
	{
	  other->client->ps.stats[STAT_ARMOR] += CPM_RAPICK;
	  if (other->client->ps.stats[STAT_ARMOR] > CPM_RAMAXARMOR)
	    other->client->ps.stats[STAT_ARMOR] = CPM_RAMAXARMOR;

	  other->client->ps.stats[STAT_ARMOR_TYPE] = CPM_RAARMORTYPE;
	}
      else if (ent->item->quantity == 50) // YA
	{
	  if (other->client->ps.stats[STAT_ARMOR_TYPE] == CPM_RAARMORTYPE)
	    other->client->ps.stats[STAT_ARMOR] *= CPM_RAMULTIPLIER;

	  other->client->ps.stats[STAT_ARMOR] += CPM_YAPICK;
	  if (other->client->ps.stats[STAT_ARMOR] > CPM_YAMAXARMOR)
	    other->client->ps.stats[STAT_ARMOR] = CPM_YAMAXARMOR;

	  other->client->ps.stats[STAT_ARMOR_TYPE] = CPM_YAARMORTYPE;
	}
      else if (ent->item->quantity == 25) // YA
	{
	  if (other->client->ps.stats[STAT_ARMOR_TYPE] == CPM_YAARMORTYPE)
	    other->client->ps.stats[STAT_ARMOR] *= CPM_YAMULTIPLIER;

	  other->client->ps.stats[STAT_ARMOR] += CPM_LAPICK;
	  if (other->client->ps.stats[STAT_ARMOR] > CPM_LAMAXARMOR)
	    other->client->ps.stats[STAT_ARMOR] = CPM_LAMAXARMOR;

	  other->client->ps.stats[STAT_ARMOR_TYPE] = CPM_LAARMORTYPE;
	}
      else // Shard
	{
	  if (other->client->ps.stats[STAT_ARMOR] <= 0)
	    other->client->ps.stats[STAT_ARMOR_TYPE] = CPM_YAARMORTYPE; // YA protection

	  other->client->ps.stats[STAT_ARMOR] += 5; // otherwise add to the current protection
	  if (other->client->ps.stats[STAT_ARMOR] > CPM_RAMAXARMOR)
	    other->client->ps.stats[STAT_ARMOR] = CPM_RAMAXARMOR;
	}
    }

  // armor stats; client exists and is alive
  other->client->stats.items.totarmor += ent->item->quantity;

  // stats; not a portable way if armor values are changed
  switch (ent->item->quantity)
    {
    case 100:
      other->client->stats.items.life[ITEM_STAT_RA]++;
      break;
    case 50:
      other->client->stats.items.life[ITEM_STAT_YA]++;
      break;
    case 25:
      other->client->stats.items.life[ITEM_STAT_LA]++;
      break;
    case 5:
      other->client->stats.items.life[ITEM_STAT_TA]++;
      break;
    default:
      break;
    }

  return RESPAWN_ARMOR;
}

//======================================================================

/*
===============
RespawnItem
===============
*/
void
RespawnItem (gentity_t * ent)
{
  assert (ent != NULL);

  // randomly select from teamed entities
  if (ent->team)
    {
      gentity_t *master;
      int count;
      int choice;

      if (!ent->teammaster)
	{
	  G_Error ("RespawnItem: bad teammaster");
	}
      master = ent->teammaster;

      for (count = 0, ent = master; ent; ent = ent->teamchain, count++)
	;

      choice = rand () % count;

      for (count = 0, ent = master; count < choice;
	   ent = ent->teamchain, count++)
	;
    }

  ent->sh.r.contents = CONTENTS_TRIGGER;
  ent->sh.s.eFlags &= ~EF_NODRAW;
  ent->sh.r.svFlags &= ~SVF_NOCLIENT;
  gi->LinkEntity (&ent->sh);

  if (ent->item->giType == IT_POWERUP)
    {
      // play powerup spawn sound to all clients
      gentity_t *te;

      // if the powerup respawn sound should Not be global
      if (ent->speed)
	{
	  te = G_TempEntity (ent->sh.s.pos.trBase, EV_GENERAL_SOUND);
	}
      else
	{
	  te = G_TempEntity (ent->sh.s.pos.trBase, EV_GLOBAL_SOUND);
	}
      te->sh.s.eventParm = G_SoundIndex ("sound/items/poweruprespawn.wav");
      te->sh.r.svFlags |= SVF_BROADCAST;
    }
  // tracking item spawning for stats purpose
  else if (ent->item->giType == IT_ARMOR)
    {
      switch (ent->item->quantity)
	{
	case 100:
	  level.stats.spawned[ITEM_STAT_RA]++;
	  break;
	case 50:
	  level.stats.spawned[ITEM_STAT_YA]++;
	  break;
	case 25:
	  level.stats.spawned[ITEM_STAT_LA]++;
	  break;
	case 5:
	  level.stats.spawned[ITEM_STAT_TA]++;
	  break;
	}
    }
  else if (ent->item->giType == IT_HEALTH)
    {
      switch (ent->item->quantity)
	{
	case 100:
	  level.stats.spawned[ITEM_STAT_MH]++;
	  break;
	case 50:
	  level.stats.spawned[ITEM_STAT_MDH]++;
	  break;
	case 25:
	  level.stats.spawned[ITEM_STAT_LH]++;
	  break;
	case 5:
	  level.stats.spawned[ITEM_STAT_TH]++;
	  break;
	}
    }

  // play the normal respawn sound only to nearby clients
  G_AddEvent (ent, EV_ITEM_RESPAWN, 0);

  ent->nextthink = 0;
}

/*
===============
Touch_Item

ent : the touched item
other : the client touching the item
===============
*/
void
Touch_Item (gentity_t * ent, gentity_t * other, trace_t * trace)
{
  int respawn;
  gStatPlayer_t *stat;

  if (!other->client)
    return;
  if (other->health < 1)
    return;		// dead people can't pickup

  if ((ent->sh.s.clientNum == other->client->ps.clientNum)
      && (ent->sh.s.time2 > level.time))
    return;		// can't pick self dropped items during 2 sec

  // the same pickup rules are used for client side and server side
  if (!BG_CanItemBeGrabbed (g_gametype->integer,
			    &ent->sh.s, &other->client->ps, g_armorSystem->integer))
    {
      return;
    }

  G_LogPrintf ("Item: %i %s\n", other->sh.s.number, ent->item->classname);

  // call the item-specific pickup function
  switch (ent->item->giType)
    {
    case IT_WEAPON:
      respawn = Pickup_Weapon (ent, other);
      break;
    case IT_AMMO:
      respawn = Pickup_Ammo (ent, other);
      break;
    case IT_ARMOR:
      respawn = Pickup_Armor (ent, other);
      break;
    case IT_HEALTH:
      respawn = Pickup_Health (ent, other);
      break;
    case IT_POWERUP:
      respawn = Pickup_Powerup (ent, other);
      break;
    case IT_TEAM:
      respawn = Pickup_Team (&teamgame, ent, other);
      break;
    case IT_HOLDABLE:
      respawn = Pickup_Holdable (ent, other);
      break;
    default:
      return;
    }

  // keep track of the last taken item by the player
  stat = &other->client->stats;
  stat->lastTakenItem = ent->item;

  if (!respawn)
    {
      return;
    }

  // play the normal pickup sound
  G_AddEvent (other, EV_ITEM_PICKUP, ent->sh.s.modelindex);

  // powerup pickups are global broadcasts
  if (ent->item->giType == IT_POWERUP || ent->item->giType == IT_TEAM)
    {
      // if we want the global sound to play
      if (!ent->speed)
	{
	  gentity_t *te;

	  te = G_TempEntity (ent->sh.s.pos.trBase, EV_GLOBAL_ITEM_PICKUP);
	  te->sh.s.eventParm = ent->sh.s.modelindex;
	  te->sh.r.svFlags |= SVF_BROADCAST;
	}
      else
	{
	  gentity_t *te;

	  te = G_TempEntity (ent->sh.s.pos.trBase, EV_GLOBAL_ITEM_PICKUP);
	  te->sh.s.eventParm = ent->sh.s.modelindex;
	  // only send this temp entity to a single client
	  te->sh.r.svFlags |= SVF_SINGLECLIENT;
	  te->sh.r.singleClient = other->sh.s.number;
	}
    }

  // fire item targets
  G_UseTargets (ent, other);

  // wait of -1 will not respawn
  if (ent->wait == -1)
    {
      ent->sh.r.svFlags |= SVF_NOCLIENT;
      ent->sh.s.eFlags |= EF_NODRAW;
      ent->sh.r.contents = 0;
      ent->unlinkAfterEvent = true;
      return;
    }

  // non zero wait overrides respawn time
  if (ent->wait)
    {
      respawn = ent->wait;
    }

  // random can be used to vary the respawn time
  if (ent->random)
    {
      respawn += crandom () * ent->random;
      if (respawn < 1)
	{
	  respawn = 1;
	}
    }

  // dropped items will not respawn
  if (ent->flags & FL_DROPPED_ITEM)
    {
      ent->freeAfterEvent = true;
    }

  // picked up items still stay around, they just don't
  // draw anything.  This allows respawnable items
  // to be placed on movers.
  ent->sh.r.svFlags |= SVF_NOCLIENT;
  ent->sh.s.eFlags |= EF_NODRAW;
  ent->sh.r.contents = 0;

  // ZOID
  // A negative respawn times means to never respawn this item (but don't
  // delete it).  This is used by items that are respawned by third party
  // events such as ctf flags
  if (respawn <= 0)
    {
      ent->nextthink = 0;
      ent->think = 0;
    }
  else
    {
      ent->nextthink = level.time + respawn * 1000;
      ent->think = RespawnItem;
    }
  gi->LinkEntity (&ent->sh);
}

//======================================================================

/*
================
LaunchItem

Spawns an item and tosses it forward
================
*/
gentity_t *
LaunchItem (const gitem_t * item, vec3_t origin, vec3_t velocity,
	    int quantity, int clientNum)
{
  gentity_t *dropped;
  trace_t tr;

  dropped = G_Spawn ();

  dropped->sh.s.eType = ET_ITEM;
  // store item number in modelindex
  dropped->sh.s.modelindex = item - bg_itemlist;
  // This is non-zero is it's a dropped item
  dropped->sh.s.modelindex2 = 1;
  // time2 is only used for particles,
  // i assume it's safe to use for weapons and ammos
  // you can't grab back your own drop immediatly
  dropped->sh.s.time2 = level.time + 2000;
  dropped->classname = item->classname;
  dropped->item = item;
  if (quantity > 0)
    {
      dropped->count = quantity;
    }

  VectorSet (dropped->sh.r.mins, -ITEM_RADIUS, -ITEM_RADIUS, -ITEM_RADIUS);
  VectorSet (dropped->sh.r.maxs, ITEM_RADIUS, ITEM_RADIUS, ITEM_RADIUS);
  dropped->sh.r.contents = CONTENTS_TRIGGER;

  dropped->touch = Touch_Item;

  G_SetOrigin (dropped, origin);
  dropped->sh.s.pos.trType = TR_GRAVITY;
  dropped->sh.s.pos.trTime = level.time;
  VectorCopy (velocity, dropped->sh.s.pos.trDelta);

  dropped->sh.s.eFlags |= EF_BOUNCE_HALF;
  dropped->physicsBounce = 0.50;	// items are bouncy

  if ((g_gametype->integer == GT_CTF) && (item->giType == IT_TEAM))
    {		// Special case for CTF flags
      dropped->think = Team_DroppedFlagThink;
      dropped->nextthink = level.time + 30000;
      Team_CheckDroppedItem (&teamgame, dropped);
    }
  else
    {		// auto-remove after 30 seconds
      dropped->think = G_FreeEntity;
      dropped->nextthink = level.time + 30000;
    }

  dropped->flags = FL_DROPPED_ITEM;

  gi->LinkEntity (&dropped->sh);

  // run an item frame
  dropped->sh.r.ownerNum = clientNum;
  dropped->sh.s.clientNum = clientNum;

  gi->Trace (&tr, origin, dropped->sh.r.mins, dropped->sh.r.maxs,
	     origin, clientNum, MASK_PLAYERSOLID & ~CONTENTS_BODY/*MASK_SOLID*/);

  if (tr.allsolid)
    {
      vec3_t nmins, nmaxs;
      vec3_t endp;
      trace_t tr2;
      int mask = MASK_PLAYERSOLID & ~CONTENTS_BODY;
      int i;

      // item might get stucked when it's dropped by a player against a wall
      // unblock the position !
      // cut size by 2 and move the item around

      VectorCopy (dropped->sh.r.mins, nmins);
      VectorCopy (dropped->sh.r.maxs, nmaxs);

      // make the bounding boxes smaller than player model
      VectorScale (nmins, 0.9f, nmins);
      VectorScale (nmaxs, 0.9f, nmaxs);

      VectorCopy (dropped->sh.r.currentOrigin, endp);

      // preliminary test, if already stuck here, stop, don't waste time
      gi->Trace (&tr, dropped->sh.r.currentOrigin, nmins, nmaxs,
		 endp, dropped->sh.r.ownerNum, mask);

      if (tr.startsolid)
	{
	  // stay stucked no way the algo will resolve the situation
	  return dropped;
	}

      // bubu unblocking algo, moving around the origin, 6 directions
      for (i = 0; i < 6; i++)
	{
	  float sgn = (i < 3) ? -1.0f : 1.0f;

	  endp[i % 3] += sgn * (ITEM_RADIUS + 1);

	  gi->Trace (&tr2, dropped->sh.r.currentOrigin, nmins, nmaxs,
		     endp, dropped->sh.r.ownerNum, mask);

	  if (!tr2.startsolid)
	    {
	      VectorCopy (tr.endpos, endp);

	      if (tr2.fraction != 1.0f)
		{
		  // we did collide, take the collision point as new one
		  VectorAdd (endp, tr2.plane.normal, endp);
		}

	      // move back to (adjusted, or not) origin point
	      endp[i % 3] -= sgn * (ITEM_RADIUS + 1);
	      VectorCopy (endp, dropped->sh.r.currentOrigin);

	      // perform a final trace with default bounding boxes
	      gi->Trace (&tr2, dropped->sh.r.currentOrigin, dropped->sh.r.mins,
			 dropped->sh.r.maxs, endp,
			 dropped->sh.r.ownerNum, mask);

	      if (!tr2.startsolid)
		{
		  // we're not anymore stucked
		  return dropped;
		}
	    }
	}
    }

  return dropped;
}

/*
================
Drop_Item

Spawns an item and tosses it forward
================
*/
gentity_t *
Drop_Item (gentity_t * ent, const gitem_t * item, float angle)
{
  vec3_t velocity;
  vec3_t angles;

  VectorCopy (ent->sh.s.apos.trBase, angles);
  angles[YAW] += angle;
  angles[PITCH] = 0;	// always forward

  AngleVectors (angles, velocity, NULL, NULL);
  VectorScale (velocity, 150, velocity);
  velocity[2] += 200 + crandom () * 50;

  return LaunchItem (item, ent->sh.s.pos.trBase, velocity, -1, -1);
}

/*
================
Drop_Player_Item

Spawns an item and tosses it forward
================
*/
// defined in bg_misc
extern const int ammopicking_table[WP_NUM_WEAPONS];

// Same as Drop_Item but gives the player a bit more control on dropping
gentity_t *
Drop_Player_Item (gentity_t * ent, const gitem_t * item, float angle, bool throw)
{
  vec3_t velocity;
  vec3_t angles;
  vec_t speed, dotprod;
  gentity_t *drop;
  int weapon, quantity, nb;
  vec3_t velcal;

  drop = NULL;
  VectorCopy (ent->sh.s.apos.trBase, angles);
  angles[YAW] += angle;
  angles[PITCH] = 0;		// always forward

  if (throw)
    {
      VectorCopy (ent->client->ps.velocity, velcal);
      speed = VectorNormalize (velcal); // player speed

      AngleVectors (angles, velocity, NULL, NULL);
      VectorNormalize (velocity);
      dotprod = DotProduct (velcal, velocity); // dotprod (0.0 - 1.0)
      VectorScale (velocity, 150 + dotprod * speed, velocity);
      velocity[2] += 200 + crandom () * 50;
    }
  else
    {
      AngleVectors (angles, velocity, NULL, NULL);
      VectorScale (velocity, 150, velocity);
      velocity[2] += 200 + crandom () * 50;
    }

  if (item->giType == IT_WEAPON)
    {
      weapon = item->giTag;

      // remove weapon from dropper, returns number of removed ammos (>=0)
      nb = Remove_Ammo (ent, weapon, ammopicking_table[weapon]);

      //trough the weapon
      drop = LaunchItem (item, ent->sh.s.pos.trBase, velocity, nb,
			 ent->client->ps.clientNum);

      // weapon is valid as it was checked by the calling function
      // remove the weapon
      ent->client->ps.stats[STAT_WEAPONS] ^= (1 << weapon);

      // update stats
      ent->client->stats.weap[weapon].dropped++;

      if (ent->client->sess.sessionTeam == TEAM_RED)
	level.teamStatsRed.weap[weapon].dropped++;
      else if (ent->client->sess.sessionTeam == TEAM_BLUE)
	level.teamStatsBlue.weap[weapon].dropped++;

      // set the client to "no weapon"
      ent->client->ps.weapon = WP_NONE;
    }
  else if (item->giType == IT_AMMO)
    {
      weapon = item->giTag;
      nb = Remove_Ammo (ent, weapon, ammopicking_table[weapon]);
      if (nb != 0)
	{
	  drop = LaunchItem (item, ent->sh.s.pos.trBase, velocity, nb,
			     ent->client->ps.clientNum);
	}
    }
  else if (item->giType == IT_TEAM)
    {
      drop = LaunchItem (item, ent->sh.s.pos.trBase, velocity, -1,
			 ent->client->ps.clientNum);

      ent->client->stats.ctf.flagtime +=
	level.time - ent->client->pers.teamState.flagsince;

      // decide how many seconds it has left
      drop->count = (ent->client->ps.powerups[item->giTag] - level.time) / 1000;
      if (drop->count < 1)
	{
	  drop->count = 1;
	}
      ent->client->ps.powerups[item->giTag] = 0;
    }

  return drop;
}

/*
================
Use_Item

Respawn the item
================
*/
void
Use_Item (gentity_t * ent, gentity_t * other, gentity_t * activator)
{
  RespawnItem (ent);
}

//======================================================================

void
ItemDefaultConfiguration (gentity_t *ent)
{
  // team slaves and targeted items aren't present at start
  if ((ent->flags & FL_TEAMSLAVE) || ent->targetname)
    {
      ent->sh.s.eFlags |= EF_NODRAW;
      ent->sh.r.contents = 0;
      return;
    }

  // powerups don't spawn in for a while
  if (ent->item->giType == IT_POWERUP)
    {
      float respawn;

      respawn = 30 + crandom () * 15; // 45
      ent->sh.s.eFlags |= EF_NODRAW;
      ent->sh.r.contents = 0;
      ent->nextthink = level.time + respawn * 1000;
      ent->think = RespawnItem;
      return;
    }

  // do respawn all the item even for the 1st spawn
  // so we can keep track of them
  // but don't change position or anything else
  ent->sh.s.eFlags |= EF_NODRAW;
  ent->sh.r.contents = 0;
  ent->nextthink = level.time + 1; // spawn them on the next frame (avoid level.time == 0)
  ent->think = RespawnItem;
}

/*
================
FinishSpawningItem

Traces down to find where an item should rest, instead of letting them
free fall from their spawn points
================
*/
void
FinishSpawningItem (gentity_t * ent)
{
  trace_t tr;
  vec3_t dest;

  VectorSet (ent->sh.r.mins, -ITEM_RADIUS, -ITEM_RADIUS, -ITEM_RADIUS);
  VectorSet (ent->sh.r.maxs, ITEM_RADIUS, ITEM_RADIUS, ITEM_RADIUS);

  ent->sh.s.eType = ET_ITEM;
  // store item number in modelindex
  ent->sh.s.modelindex = ent->item - bg_itemlist;
  // zero indicates this isn't a dropped item
  ent->sh.s.modelindex2 = 0;
  ent->sh.r.contents = CONTENTS_TRIGGER;
  ent->touch = Touch_Item;
  // using an item causes it to respawn
  ent->use = Use_Item;

  if (ent->spawnflags & 1)
    {
      // suspended
      G_SetOrigin (ent, ent->sh.s.origin);
    }
  else
    {
      // drop to floor
      VectorSet (dest, ent->sh.s.origin[0], ent->sh.s.origin[1],
		 ent->sh.s.origin[2] - 4096);
      gi->Trace (&tr, ent->sh.s.origin, ent->sh.r.mins, ent->sh.r.maxs, dest,
		 ent->sh.s.number, MASK_SOLID);
      if (tr.startsolid)
	{
	  G_Printf ("FinishSpawningItem: %s startsolid at %s\n",
		    ent->classname, vtos (ent->sh.s.origin));
	  G_FreeEntity (ent);
	  return;
	}

      // allow to ride movers
      ent->sh.s.groundEntityNum = tr.entityNum;

      G_SetOrigin (ent, tr.endpos);
    }

  ItemDefaultConfiguration (ent);

  gi->LinkEntity (&ent->sh);
}

/*
==================
G_FindNearestItem
==================
*/
const gitem_t *
G_FindNearestItem (vec3_t origin, int flag)
{
  gentity_t *ent;
  const gitem_t *resitem = NULL;
  vec3_t res;
  vec_t distance;
  vec_t saved = 9999999.0;
  int i;
  ent = &g_entities[0];

  for (i = 0; i < level.num_entities; i++, ent++)
    {
      if (!ent->inuse)
	continue;

      if (ent->sh.s.eType != ET_ITEM)
	continue;

      // item respawned (hop it works the same for flags)
      if (ent->nextthink > 0)
	continue;

      // skip ammos
      if (flag >= 2)
	{
	  if (ent->item->giType == IT_AMMO)
	    continue;
	}

      // skip shards and 5 health
      if (flag >= 3)
	{
	  if (ent->item->giType == IT_ARMOR && ent->item->quantity <= 5)
	    continue;
	  if (ent->item->giType == IT_HEALTH && ent->item->quantity <= 25)
	    continue;
	}

      // no need to use Trace function
      VectorSubtract (origin, ent->sh.r.currentOrigin, res);
      distance = VectorLength (res);

      if (distance < saved)
	{
	  saved = distance;
	  resitem = ent->item;
	}
    }
  return resitem;
}

/*
==================
G_CheckTeamItems
==================
*/
bool itemRegistered[MAX_ITEMS];

void
G_CheckTeamItems (void)
{
  if (BG_GametypeIsTeam (g_gametype->integer))
    {
      // Set up team stuff
      Team_InitGame (&teamgame);

      if (g_gametype->integer == GT_CTF)
	{
	  const gitem_t *item;

	  // check for the two flags
	  item = BG_FindItem ("Red Flag");
	  if (!item || !itemRegistered[item - bg_itemlist])
	    {
	      G_Printf (S_COLOR_YELLOW "WARNING: No team_CTF_redflag in map");
	    }
	  item = BG_FindItem ("Blue Flag");
	  if (!item || !itemRegistered[item - bg_itemlist])
	    {
	      G_Printf (S_COLOR_YELLOW "WARNING: No team_CTF_blueflag in map");
	    }
	}
    }
}

/*
==============
ClearRegisteredItems
==============
*/
void
ClearRegisteredItems (void)
{
  Com_Memset (itemRegistered, 0, sizeof (itemRegistered));

  // players always start with the base weapon
  RegisterItem (BG_FindItemForWeapon (WP_MACHINEGUN));
  RegisterItem (BG_FindItemForWeapon (WP_GAUNTLET));

  // we might want to drop MACHINEGUN, even on maps which don't have it
  if ((g_gametype->integer == GT_TEAM) || (g_gametype->integer == GT_CTF))
    {
      RegisterItem (BG_FindItemForAmmo (WP_MACHINEGUN));
    }

  // if the all_wepaons flag was up on map load, register all weapons
  if (g_dmflags->integer & DF_ALL_WEAPONS)
    {
      RegisterItem (BG_FindItemForWeapon (WP_SHOTGUN));
      RegisterItem (BG_FindItemForWeapon (WP_GRENADE_LAUNCHER));
      RegisterItem (BG_FindItemForWeapon (WP_ROCKET_LAUNCHER));
      RegisterItem (BG_FindItemForWeapon (WP_LIGHTNING));
      RegisterItem (BG_FindItemForWeapon (WP_RAILGUN));
      RegisterItem (BG_FindItemForWeapon (WP_PLASMAGUN));
      RegisterItem (BG_FindItemForWeapon (WP_BFG));

      // also register ammos, we might drop them
      if ((g_gametype->integer == GT_TEAM) || (g_gametype->integer == GT_CTF))
	{
	  RegisterItem (BG_FindItemForAmmo (WP_SHOTGUN));
	  RegisterItem (BG_FindItemForAmmo (WP_GRENADE_LAUNCHER));
	  RegisterItem (BG_FindItemForAmmo (WP_ROCKET_LAUNCHER));
	  RegisterItem (BG_FindItemForAmmo (WP_LIGHTNING));
	  RegisterItem (BG_FindItemForAmmo (WP_RAILGUN));
	  RegisterItem (BG_FindItemForAmmo (WP_PLASMAGUN));
	  RegisterItem (BG_FindItemForAmmo (WP_BFG));
	}
    }
}

/*
===============
RegisterItem

The item will be added to the precache list
===============
*/
void
RegisterItem (const gitem_t * item)
{
  if (item == NULL)
    {
      G_Error ("RegisterItem: NULL");
    }
  itemRegistered[item - bg_itemlist] = true;
}

/*
===============
IsItemRegistered

returns true if the item is registered
===============
*/
bool
IsItemRegistered (const gitem_t * item)
{
  if (item == NULL)
    {
      G_Error ("IsItemRegistered: NULL");
    }
  return itemRegistered[item - bg_itemlist];
}

/*
===============
SaveRegisteredItems

Write the needed items to a config string
so the client will know which ones to precache
===============
*/
void
SaveRegisteredItems (void)
{
  char string[MAX_ITEMS + 1];
  int i;
  int count;

  count = 0;
  for (i = 0; i < bg_numItems; i++)
    {
      if (itemRegistered[i])
	{
	  count++;
	  string[i] = '1';
	}
      else
	{
	  string[i] = '0';
	}
    }
  string[bg_numItems] = 0;

  G_Printf ("%i items registered\n", count);
  gi->SetConfigstring (CS_ITEMS, string);
}

/*
============
G_ItemDisabled
============
*/
int
G_ItemDisabled (const gitem_t * item)
{
  char name[128];

  Com_sprintf (name, sizeof (name), "disable_%s", item->classname);
  return gi->Cvar_VariableIntegerValue (name);
}

/*
============
G_SpawnItem

Sets the clipping size and plants the object on the floor.

Items can't be immediately dropped to floor, because they might
be on an entity that hasn't spawned yet.
============
*/
void
G_SpawnItem (gentity_t * ent, const gitem_t * item)
{
  G_SpawnFloat ("random", "0", &ent->random);
  G_SpawnFloat ("wait", "0", &ent->wait);

  RegisterItem (item);
  if (G_ItemDisabled (item))
    return;

  ent->item = item;
  // some movers spawn on the second frame, so delay item
  // spawns until the third frame so they can ride trains
  //ent->nextthink = level.time + FRAMETIME * 2;
  ent->nextthink = -2;
  ent->think = FinishSpawningItem;

  ent->physicsBounce = 0.50;	// items are bouncy

  if (item->giType == IT_POWERUP)
    {
      G_SoundIndex ("sound/items/poweruprespawn.wav");
      G_SpawnFloat ("noglobalsound", "0", &ent->speed);
    }

  // if the item is a weapon and we are in a gametype that allows dropping
  // be sure also have registered the ammos
  if ((g_gametype->integer == GT_TEAM)
      || (g_gametype->integer == GT_CTF))
    {
      if ((item->giType == IT_WEAPON)
	  && (item->giTag > WP_MACHINEGUN)
	  && (item->giTag < WP_NUM_WEAPONS))
	{
	  RegisterItem (BG_FindItemForAmmo (item->giTag));
	}
    }
}

/*
================
G_BounceItem

================
*/
void
G_BounceItem (gentity_t * ent, trace_t * trace)
{
  vec3_t velocity;
  float dot;
  int hitTime;

  // reflect the velocity on the trace plane
  hitTime =
    level.previousTime + (level.time - level.previousTime) * trace->fraction;
  BG_EvaluateTrajectoryDelta (&ent->sh.s.pos, hitTime, velocity);
  dot = DotProduct (velocity, trace->plane.normal);
  VectorMA (velocity, -2 * dot, trace->plane.normal, ent->sh.s.pos.trDelta);

  // cut the velocity to keep from bouncing forever
  VectorScale (ent->sh.s.pos.trDelta, ent->physicsBounce,
	       ent->sh.s.pos.trDelta);

  // check for stop
  if (trace->plane.normal[2] > 0 && ent->sh.s.pos.trDelta[2] < 40)
    {
      trace->endpos[2] += 1.0;	// make sure it is off ground
      gi->SnapVector (trace->endpos);
      G_SetOrigin (ent, trace->endpos);
      ent->sh.s.groundEntityNum = trace->entityNum;
      return;
    }

  VectorAdd (ent->sh.r.currentOrigin, trace->plane.normal,
	     ent->sh.r.currentOrigin);
  VectorCopy (ent->sh.r.currentOrigin, ent->sh.s.pos.trBase);
  ent->sh.s.pos.trTime = level.time;
}

/*
================
G_RunItem

================
*/
void
G_RunItem (gentity_t * ent)
{
  vec3_t origin;
  trace_t tr;
  int contents;
  int mask;

  // if groundentity has been set to -1, it may have been pushed off an edge
  if (ent->sh.s.groundEntityNum == -1)
    {
      if (ent->sh.s.pos.trType != TR_GRAVITY)
	{
	  ent->sh.s.pos.trType = TR_GRAVITY;
	  ent->sh.s.pos.trTime = level.time;
	}
    }

  if (ent->sh.s.pos.trType == TR_STATIONARY)
    {
      // check think function
      G_RunThink (ent);
      return;
    }

  // get current position
  BG_EvaluateTrajectory (&ent->sh.s.pos, level.time, origin);

  // trace a line from the previous position to the current position
  if (ent->clipmask)
    {
      mask = ent->clipmask;
    }
  else
    {
      mask = MASK_PLAYERSOLID & ~CONTENTS_BODY;	//MASK_SOLID;
    }
  gi->Trace (&tr, ent->sh.r.currentOrigin, ent->sh.r.mins, ent->sh.r.maxs,
	     origin, ent->sh.r.ownerNum, mask);

  VectorCopy (tr.endpos, ent->sh.r.currentOrigin);

  if (tr.startsolid)
    {
      tr.fraction = 0;
    }

  gi->LinkEntity (&ent->sh);	// FIXME: avoid this for stationary?

  // check think function
  G_RunThink (ent);

  if (tr.fraction == 1)
    {
      return;
    }

  // if it is in a nodrop volume, remove it
  contents = gi->PointContents (ent->sh.r.currentOrigin, -1);
  if (contents & CONTENTS_NODROP)
    {
      if (ent->item && ent->item->giType == IT_TEAM)
	{
	  Team_FreeEntity (&teamgame, ent);
	}
      else
	{
	  G_FreeEntity (ent);
	}
      return;
    }

  G_BounceItem (ent, &tr);
}

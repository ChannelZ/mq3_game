/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

#define MISSILE_PRESTEP_TIME 50

/*
================
G_BounceMissile

================
*/
void
G_BounceMissile (gentity_t *ent, trace_t *trace)
{
  int hitTime;
  float dot;
  vec3_t velocity;

  // reflect the velocity on the trace plane
  hitTime = level.previousTime + (level.time - level.previousTime) * trace->fraction;
  BG_EvaluateTrajectoryDelta (&ent->sh.s.pos, hitTime, velocity);
  dot = DotProduct (velocity, trace->plane.normal);
  VectorMA (velocity, -2*dot, trace->plane.normal, ent->sh.s.pos.trDelta);

  if (ent->sh.s.eFlags & EF_BOUNCE_HALF)
    {
      VectorScale (ent->sh.s.pos.trDelta, 0.65, ent->sh.s.pos.trDelta);
      // check for stop
      if (trace->plane.normal[2] > 0.2
	  && VectorLength (ent->sh.s.pos.trDelta) < 40)
	{
	  G_SetOrigin (ent, trace->endpos);
	  return;
	}
    }

  VectorAdd (ent->sh.r.currentOrigin, trace->plane.normal, ent->sh.r.currentOrigin);
  VectorCopy (ent->sh.r.currentOrigin, ent->sh.s.pos.trBase);
  ent->sh.s.pos.trTime = level.time;
}

/*
================
G_ExplodeMissile

Explode a missile without an impact
================
*/
void
G_ExplodeMissile (gentity_t *ent)
{
  vec3_t dir;
  vec3_t origin;

  BG_EvaluateTrajectory (&ent->sh.s.pos, level.time, origin);

  gi->SnapVector (origin);
  G_SetOrigin (ent, origin);

  // we don't have a valid direction, so just point straight up
  dir[0] = dir[1] = 0;
  dir[2] = 1;

  ent->sh.s.eType = ET_GENERAL;
  G_AddEvent (ent, EV_MISSILE_MISS, DirToByte(dir));

  ent->freeAfterEvent = true;

  // splash damage
  if (ent->splashDamage)
    {
      if (G_RadiusDamage (ent->sh.r.currentOrigin, ent->parent,
			  ent->splashDamage, ent->splashRadius,
			  ent, ent->splashMethodOfDeath))
	{
	  g_entities[ent->sh.r.ownerNum].client->cbt.accuracy_hits++;
	}
    }

  gi->LinkEntity (&ent->sh);
}

/*
================
G_MissileImpact
================
*/
#define EXTRA_KNOCKBACK_OFFSET 16

void
G_MissileImpact (gentity_t *ent, trace_t *trace)
{
  gentity_t * other;
  bool hitClient = false;

  other = &g_entities[trace->entityNum];

  // check for bounce
  if (!other->takedamage && (ent->sh.s.eFlags & (EF_BOUNCE | EF_BOUNCE_HALF)))
    {
      G_BounceMissile (ent, trace);
      G_AddEvent (ent, EV_GRENADE_BOUNCE, 0);
      return;
    }

  // impact damage
  if (other->takedamage)
    {
      // FIXME: wrong damage direction?
      if (ent->damage)
	{
	  float res;
	  vec3_t velocity;

	  if (LogAccuracyHit (other, &g_entities[ent->sh.r.ownerNum]))
	    {
	      g_entities[ent->sh.r.ownerNum].client->cbt.accuracy_hits++;
	      hitClient = true;
	    }
	  BG_EvaluateTrajectoryDelta (&ent->sh.s.pos, level.time, velocity);
	  if (VectorLength (velocity) == 0)
	    {
	      velocity[2] = 1;	// stepped on a grenade
	    }

	  res = (trace->endpos[2] - (other->sh.r.currentOrigin[2] + other->sh.r.mins[2]));
	  // if we are under the gravity center make a linear extra push up
	  // no need to check res > 0, we know there is a direct hit
	  // res might be slightly negative sometimes

	  if (res < /*(-other->sh.r.mins[2])*/EXTRA_KNOCKBACK_OFFSET)
	    {
	      // extra knockback
	      float factor = (float)(EXTRA_KNOCKBACK_OFFSET - res)/(float)EXTRA_KNOCKBACK_OFFSET;

	      factor *= 14.0f;
	      velocity[2] += ent->damage * factor;
	    }
	  G_Damage (other, ent, &g_entities[ent->sh.r.ownerNum],
		    velocity, ent->sh.s.origin, ent->damage,
		    0, ent->methodOfDeath);
	}
    }

  // is it cheaper in bandwidth to just remove this ent and create a new
  // one, rather than changing the missile into the explosion?

  if (other->takedamage && other->client)
    {
      G_AddEvent (ent, EV_MISSILE_HIT, DirToByte (trace->plane.normal));
      ent->sh.s.otherEntityNum = other->sh.s.number;
    }
  else if (trace->surfaceFlags & SURF_METALSTEPS)
    {
      G_AddEvent (ent, EV_MISSILE_MISS_METAL, DirToByte (trace->plane.normal));
    }
  else
    {
      G_AddEvent (ent, EV_MISSILE_MISS, DirToByte (trace->plane.normal));
    }

  ent->freeAfterEvent = true;

  // change over to a normal entity right at the point of impact
  ent->sh.s.eType = ET_GENERAL;

  SnapVectorTowards (trace->endpos, ent->sh.s.pos.trBase); // save net bandwidth

  G_SetOrigin (ent, trace->endpos);

  // splash damage (doesn't apply to person directly hit)
  if (ent->splashDamage)
    {
      vec3_t hitpoint;

      // HACK : we will move a bit from the impact point to avoid trace errors
      VectorMA (trace->endpos, 1.0f, trace->plane.normal, hitpoint);
      //VectorCopy (trace->endpos, hitpoint);

      if (G_RadiusDamage (hitpoint, ent->parent,
			  ent->splashDamage, ent->splashRadius, 
			  other, ent->splashMethodOfDeath))
	{
	  if (!hitClient)
	    {
	      g_entities[ent->sh.r.ownerNum].client->cbt.accuracy_hits++;
	    }
	}
    }

  gi->LinkEntity (&ent->sh);
}

/*
================
G_RunMissile
================
*/
void
G_RunMissile (gentity_t *ent)
{
  vec3_t origin;
  trace_t tr;
  int passent;

  // get current position
  BG_EvaluateTrajectory (&ent->sh.s.pos, level.time, origin);

  // ignore interactions with the missile owner
  passent = ent->sh.r.ownerNum;

  // trace a line from the previous position to the current position
  gi->Trace (&tr, ent->sh.r.currentOrigin, ent->sh.r.mins,
	     ent->sh.r.maxs, origin, passent, ent->clipmask);

  if (tr.startsolid || tr.allsolid)
    {
      // make sure the tr.entityNum is set to the entity we're stuck in
      gi->Trace (&tr, ent->sh.r.currentOrigin, ent->sh.r.mins,
		 ent->sh.r.maxs, ent->sh.r.currentOrigin, passent, ent->clipmask);
      tr.fraction = 0;
    }
  else
    {
      VectorCopy (tr.endpos, ent->sh.r.currentOrigin);
    }

  gi->LinkEntity (&ent->sh);

  if (tr.fraction != 1)
    {
      // never explode or bounce on sky
      if (tr.surfaceFlags & SURF_NOIMPACT)
	{
	  G_FreeEntity (ent);
	  return;
	}
      G_MissileImpact (ent, &tr);
      if (ent->sh.s.eType != ET_MISSILE)
	{
	  return;		// exploded
	}
    }

  // check think function after bouncing
  G_RunThink (ent);
}

//=============================================================================

/*
=================
fire_plasma

=================
*/
gentity_t *
fire_plasma (gentity_t *self, vec3_t start, vec3_t dir)
{
  gentity_t * bolt;

  VectorNormalize (dir);

  bolt = G_Spawn ();
  bolt->classname = "plasma";
  bolt->nextthink = level.time + 10000;
  bolt->think = G_ExplodeMissile;
  bolt->sh.s.eType = ET_MISSILE;
  bolt->sh.r.svFlags = SVF_USE_CURRENT_ORIGIN;
  bolt->sh.s.weapon = WP_PLASMAGUN;
  bolt->sh.r.ownerNum = self->sh.s.number;
  bolt->parent = self;
  bolt->damage = g_plasma_damage->integer;
  bolt->splashDamage = g_plasmasplash_damage->integer;
  bolt->splashRadius = PLASMA_SPLASH_RADIUS;
  bolt->methodOfDeath = MOD_PLASMA;
  bolt->splashMethodOfDeath = MOD_PLASMA_SPLASH;
  bolt->clipmask = MASK_SHOT;

  bolt->sh.s.pos.trType = TR_LINEAR;
  bolt->sh.s.pos.trTime = level.time - MISSILE_PRESTEP_TIME;	// move a bit on the very first frame
  VectorCopy (start, bolt->sh.s.pos.trBase);
  VectorScale (dir, g_plasma_speed->integer, bolt->sh.s.pos.trDelta);
  gi->SnapVector (bolt->sh.s.pos.trDelta);	// save net bandwidth

  VectorCopy (start, bolt->sh.r.currentOrigin);

  return bolt;
}	

//=============================================================================

/*
=================
fire_grenade
=================
*/
gentity_t *
fire_grenade (gentity_t *self, vec3_t start, vec3_t dir)
{
  gentity_t * bolt;

  VectorNormalize (dir);

  bolt = G_Spawn ();
  bolt->classname = "grenade";
  bolt->nextthink = level.time + 2500; // + 2500
  bolt->think = G_ExplodeMissile;
  bolt->sh.s.eType = ET_MISSILE;
  bolt->sh.r.svFlags = SVF_USE_CURRENT_ORIGIN;
  bolt->sh.s.weapon = WP_GRENADE_LAUNCHER;
  bolt->sh.s.eFlags = EF_BOUNCE_HALF;
  bolt->sh.r.ownerNum = self->sh.s.number;
  bolt->parent = self;
  bolt->damage = g_grenade_damage->integer;
  bolt->splashDamage = g_grenadesplash_damage->integer;
  bolt->splashRadius = GRENADE_SPLASH_RADIUS;
  bolt->methodOfDeath = MOD_GRENADE;
  bolt->splashMethodOfDeath = MOD_GRENADE_SPLASH;
  bolt->clipmask = MASK_SHOT;

  bolt->sh.s.pos.trType = TR_GRAVITY;
  bolt->sh.s.pos.trTime = level.time - MISSILE_PRESTEP_TIME;	// move a bit on the very first frame
  VectorCopy (start, bolt->sh.s.pos.trBase);
  VectorScale (dir, g_grenade_speed->integer, bolt->sh.s.pos.trDelta); // 800
  gi->SnapVector (bolt->sh.s.pos.trDelta);	// save net bandwidth

  VectorCopy (start, bolt->sh.r.currentOrigin);

  return bolt;
}

//=============================================================================

/*
=================
fire_bfg
=================
*/
gentity_t *
fire_bfg (gentity_t *self, vec3_t start, vec3_t dir)
{
  gentity_t * bolt;

  VectorNormalize (dir);

  bolt = G_Spawn ();
  bolt->classname = "bfg";
  bolt->nextthink = level.time + 10000;
  bolt->think = G_ExplodeMissile;
  bolt->sh.s.eType = ET_MISSILE;
  bolt->sh.r.svFlags = SVF_USE_CURRENT_ORIGIN;
  bolt->sh.s.weapon = WP_BFG;
  bolt->sh.r.ownerNum = self->sh.s.number;
  bolt->parent = self;
  bolt->damage = g_bfg_damage->integer;
  bolt->splashDamage = BFG_SPLASH_DAMAGE;
  bolt->splashRadius = BFG_SPLASH_RADIUS;
  bolt->methodOfDeath = MOD_BFG;
  bolt->splashMethodOfDeath = MOD_BFG_SPLASH;
  bolt->clipmask = MASK_SHOT;

  bolt->sh.s.pos.trType = TR_LINEAR;
  bolt->sh.s.pos.trTime = level.time - MISSILE_PRESTEP_TIME;	// move a bit on the very first frame
  VectorCopy (start, bolt->sh.s.pos.trBase);
  VectorScale (dir, 2000, bolt->sh.s.pos.trDelta);
  gi->SnapVector (bolt->sh.s.pos.trDelta);	// save net bandwidth
  VectorCopy (start, bolt->sh.r.currentOrigin);

  return bolt;
}

//=============================================================================

/*
=================
fire_rocket
=================
*/
gentity_t *
fire_rocket (gentity_t *self, vec3_t start, vec3_t dir)
{
  gentity_t * bolt;

  VectorNormalize (dir);

  bolt = G_Spawn ();
  bolt->classname = "rocket";
  bolt->nextthink = level.time + 15000;
  bolt->think = G_ExplodeMissile;
  bolt->sh.s.eType = ET_MISSILE;
  bolt->sh.r.svFlags = SVF_USE_CURRENT_ORIGIN;
  bolt->sh.s.weapon = WP_ROCKET_LAUNCHER;
  bolt->sh.r.ownerNum = self->sh.s.number;
  bolt->parent = self;
  bolt->damage = g_rocket_damage->integer;
  bolt->splashDamage = g_rocketsplash_damage->integer;
  bolt->splashRadius = ROCKET_SPLASH_RADIUS;
  bolt->methodOfDeath = MOD_ROCKET;
  bolt->splashMethodOfDeath = MOD_ROCKET_SPLASH;
  bolt->clipmask = MASK_SHOT;

  bolt->sh.s.pos.trType = TR_LINEAR;
  bolt->sh.s.pos.trTime = level.time - MISSILE_PRESTEP_TIME;	// move a bit on the very first frame
  VectorCopy (start, bolt->sh.s.pos.trBase);
  VectorScale (dir, g_rocket_speed->integer, bolt->sh.s.pos.trDelta);	// 900
  gi->SnapVector (bolt->sh.s.pos.trDelta);		// save net bandwidth
  VectorCopy (start, bolt->sh.r.currentOrigin);

  return bolt;
}

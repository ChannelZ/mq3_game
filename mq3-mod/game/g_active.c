/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

/*
===============
G_DamageFeedback

Called just before a snapshot is sent to the given player.
Totals up all damage and generates both the player_state_t
damage values to that client for pain blends and kicks, and
global pain sound events for all clients.
===============
*/
void
P_DamageFeedback (gentity_t * player)
{
  gclient_t *client;
  float count;
  vec3_t angles;

  client = player->client;
  if (client->ps.pm_type == PM_DEAD)
    {
      return;
    }

  // total points of damage shot at the player this frame
  count = client->cbt.damage_blood + client->cbt.damage_armor;
  if (count == 0)
    {
      return;		// didn't take any damage
    }

  // limit damages (not a technical restriction)
  if (count > 255)
    {
      count = 255;
    }

  // send the information to the client

  // world damage (falling, slime, etc) uses a special code
  // to make the blend blob centered instead of positional
  if (client->cbt.damage_fromWorld)
    {
      client->ps.damagePitch = 255;
      client->ps.damageYaw = 255;

      client->cbt.damage_fromWorld = false;
    }
  else
    {
      vectoangles (client->cbt.damage_from, angles);
      client->ps.damagePitch = angles[PITCH] / 360.0 * 256;
      client->ps.damageYaw = angles[YAW] / 360.0 * 256;
    }

  // play an apropriate pain sound
  if ((level.time > player->pain_debounce_time)
      && !(player->flags & FL_GODMODE))
    {
      player->pain_debounce_time = level.time + 700;
      G_AddEvent (player, EV_PAIN, player->health);
      client->ps.damageEvent++;
    }

  client->ps.damageCount = count;

  //
  // clear totals
  //
  client->cbt.damage_blood = 0;
  client->cbt.damage_armor = 0;
  client->cbt.damage_knockback = 0;
}

/*
=============
P_WorldEffects

Check for lava / slime contents and drowning
=============
*/
void
P_WorldEffects (gentity_t * ent)
{
  bool envirosuit;
  int waterlevel;

  if (ent->client->misc.noclip)
    {
      ent->client->misc.airOutTime = level.time + 12000;	// don't need air
      return;
    }

  waterlevel = ent->waterlevel;

  envirosuit = (ent->client->ps.powerups[PW_BATTLESUIT] > level.time);

  //
  // check for drowning
  //
  if (waterlevel == 3)
    {
      // envirosuit give air
      if (envirosuit)
	{
	  ent->client->misc.airOutTime = level.time + 10000;
	}

      // if out of air, start drowning
      if (ent->client->misc.airOutTime < level.time)
	{
	  // drown!
	  ent->client->misc.airOutTime += 1000;
	  if (ent->health > 0)
	    {
	      // take more damage the longer underwater
	      ent->damage += 2;
	      if (ent->damage > 15)
		ent->damage = 15;

	      // play a gurp sound instead of a normal pain sound
	      if (ent->health <= ent->damage)
		{
		  G_Sound (ent, CHAN_VOICE, G_SoundIndex ("*drown.wav"));
		}
	      else if (rand () & 1)
		{
		  G_Sound (ent, CHAN_VOICE,
			   G_SoundIndex ("sound/player/gurp1.wav"));
		}
	      else
		{
		  G_Sound (ent, CHAN_VOICE,
			   G_SoundIndex ("sound/player/gurp2.wav"));
		}

	      // don't play a normal pain sound
	      ent->pain_debounce_time = level.time + 200;

	      G_Damage (ent, NULL, NULL, NULL, NULL,
			ent->damage, DAMAGE_NO_ARMOR, MOD_WATER);
	    }
	}
    }
  else
    {
      ent->client->misc.airOutTime = level.time + 12000;
      ent->damage = 2; // base amount of health loss when drowing
    }

  //
  // check for sizzle damage (move to pmove?)
  //
  if (waterlevel && (ent->watertype & (CONTENTS_LAVA | CONTENTS_SLIME)))
    {
      if ((ent->health > 0) && (ent->pain_debounce_time <= level.time))
	{
	  if (envirosuit)
	    {
	      G_AddEvent (ent, EV_POWERUP_BATTLESUIT, 0);
	    }
	  else
	    {
	      if (ent->watertype & CONTENTS_LAVA)
		{
		  G_Damage (ent, NULL, NULL, NULL, NULL,
			    30 * waterlevel, 0, MOD_LAVA);
		}
	      if (ent->watertype & CONTENTS_SLIME)
		{
		  G_Damage (ent, NULL, NULL, NULL, NULL,
			    10 * waterlevel, 0, MOD_SLIME);
		}
	    }
	}
    }
}

/*
===============
G_SetClientSound
===============
*/
void
G_SetClientSound (gentity_t * ent)
{
  if (ent->waterlevel && (ent->watertype & (CONTENTS_LAVA | CONTENTS_SLIME)))
    {
      ent->client->ps.loopSound = level.snd_fry;
    }
  else
    {
      ent->client->ps.loopSound = 0;
    }
}

//==============================================================

/*
==============
ClientImpacts
==============
*/
void
ClientImpacts (gentity_t * ent, pmove_t * pm)
{
  int i, j;
  trace_t trace;
  gentity_t *other;

  Com_Memset (&trace, 0, sizeof (trace));
  for (i = 0; i < pm->numtouch; i++)
    {
      // avoid touching twice the same ent (how may this happend ?)
      for (j = 0; j < i; j++)
	{
	  if (pm->touchents[j] == pm->touchents[i])
	    {
	      break;
	    }
	}
      if (j != i)
	{
	  continue;	// duplicated
	}

      other = &g_entities[pm->touchents[i]];

      // for bots
      if ((ent->sh.r.svFlags & SVF_BOT) && (ent->touch))
	{
	  ent->touch (ent, other, &trace);
	}

      if (!other->touch)
	{
	  continue;
	}

      other->touch (other, ent, &trace);
    }
}

/*
============
G_TouchTriggers

Find all trigger entities that ent's current position touches.
Spectators will only interact with teleporters.
============
*/
void
G_TouchTriggers (gentity_t * ent)
{
  int i, num;
  int touch[MAX_GENTITIES];
  gentity_t *hit;
  trace_t trace;
  vec3_t mins, maxs;
  static vec3_t range = { 40, 40, 52 };

  if (!ent->client)
    {
      return;
    }

  // dead clients don't activate triggers!
  if (ent->client->ps.stats[STAT_HEALTH] <= 0)
    {
      return;
    }

  VectorSubtract (ent->client->ps.origin, range, mins);
  VectorAdd (ent->client->ps.origin, range, maxs);

  num = gi->EntitiesInBox (mins, maxs, touch, MAX_GENTITIES);

  // can't use ent->absmin, because that has a one unit pad
  VectorAdd (ent->client->ps.origin, ent->sh.r.mins, mins);
  VectorAdd (ent->client->ps.origin, ent->sh.r.maxs, maxs);

  for (i = 0; i < num; i++)
    {
      hit = &g_entities[touch[i]];

      if (!hit->touch && !ent->touch)
	{
	  continue;
	}
      if (!(hit->sh.r.contents & CONTENTS_TRIGGER))
	{
	  continue;
	}

      // ignore most entities if a spectator
      if (ent->client->sess.sessionTeam == TEAM_SPECTATOR)
	{
	  if (hit->sh.s.eType != ET_TELEPORT_TRIGGER &&
	      // this is ugly but adding a new ET_? type will
	      // most likely cause network incompatibilities
	      (hit->touch != Touch_DoorTrigger
	       || hit->sh.s.eType != ET_DOOR_TRIGGER))
	    {
	      continue;
	    }
	}

      // use seperate code for determining if an item is picked up
      // so you don't have to actually contact its bounding box
      if (hit->sh.s.eType == ET_ITEM)
	{
	  if (!BG_PlayerTouchesItem (&ent->client->ps, &hit->sh.s, level.time))
	    {
	      continue;
	    }
	}
      else
	{
	  if (!gi->EntityContact (mins, maxs, &hit->sh))
	    {
	      continue;
	    }
	}

      Com_Memset (&trace, 0, sizeof (trace));

      if (hit->touch)
	{
	  hit->touch (hit, ent, &trace);
	}

      // let the bots act
      if ((ent->sh.r.svFlags & SVF_BOT) && (ent->touch))
	{
	  ent->touch (ent, hit, &trace);
	}
    }

  // if we didn't touch a jump pad this pmove frame
  /*
  if (ent->client->ps.jumppad_frame != ent->client->ps.pmove_framecount)
    {
      ent->client->ps.jumppad_frame = 0;
      ent->client->ps.jumppad_ent = 0;
    }
  */
}

/*
=================
SpectatorThink
=================
*/
void
SpectatorThink (gentity_t * ent, usercmd_t * ucmd)
{
  pmove_t pm;
  gclient_t *client;

  client = ent->client;

  if (client->sess.spectatorState != SPECTATOR_FOLLOW)
    {
      client->ps.pm_type = PM_SPECTATOR;
      client->ps.speed = 400;	// faster than normal

      // set up for pmove
      Com_Memset (&pm, 0, sizeof (pm));
      pm.ps = &client->ps;
      pm.cmd = *ucmd;
      pm.tracemask = MASK_PLAYERSOLID & ~CONTENTS_BODY;	// spectators can fly through bodies
      pm.trace = gi->Trace;
      pm.pointcontents = gi->PointContents;

      // perform a pmove
      Pmove (&pm);
      // save results of pmove
      VectorCopy (client->ps.origin, ent->sh.s.origin);

      G_TouchTriggers (ent);
      gi->UnlinkEntity (&ent->sh);
    }

  client->misc.oldbuttons = client->misc.buttons;
  client->misc.buttons = ucmd->buttons;

  // attack button cycles through spectators
  if ((client->misc.buttons & BUTTON_ATTACK)
      && !(client->misc.oldbuttons & BUTTON_ATTACK))
    {
      Cmd_FollowCycle_f (ent, 1);
    }
}

/*
=================
ClientInactivityTimer

Returns false if the client is dropped
=================
*/
bool
ClientInactivityTimer (gclient_t * client)
{
  if (!g_inactivity->integer)
    {
      // give everyone some time, so if the operator sets g_inactivity during
      // gameplay, everyone isn't kicked
      client->timer.inactivityTime = level.time + 60 * 1000;
      client->timer.inactivityWarning = false;
    }
  else if (client->pers.cmd.forwardmove || client->pers.cmd.rightmove
	   || client->pers.cmd.upmove
	   || (client->pers.cmd.buttons & BUTTON_ATTACK))
    {
      client->timer.inactivityTime = level.time + g_inactivity->integer * 1000;
      client->timer.inactivityWarning = false;
    }
  else if (!client->pers.localClient)
    {
      if (level.time > client->timer.inactivityTime)
	{
	  gi->DropClient (client - level.clients,
			  "Dropped due to inactivity");
	  return false;
	}
      if ((level.time > client->timer.inactivityTime - 10000)
	  && !client->timer.inactivityWarning)
	{
	  client->timer.inactivityWarning = true;
	  gi->SendServerCommand (client - level.clients,
				 "cp \"Ten seconds until inactivity drop!\n\"");
	}
    }
  return true;
}

/*
==================
ClientTimerActions

Actions that happen once a second
==================
*/
void
ClientTimerActions (gentity_t * ent, int msec)
{
  gclient_t *client;
  int i;

  client = ent->client;
  client->misc.timeResidual += msec;

  while (client->misc.timeResidual >= 1000)
    {
      client->misc.timeResidual -= 1000;

      // regenerate
      if (client->ps.powerups[PW_REGEN])
	{
	  if (ent->health < client->ps.stats[STAT_MAX_HEALTH])
	    {
	      ent->health += 15;
	      if (ent->health > (client->ps.stats[STAT_MAX_HEALTH] * 1.1))
		{
		  ent->health = client->ps.stats[STAT_MAX_HEALTH] * 1.1;
		}
	      G_AddEvent (ent, EV_POWERUP_REGEN, 0);
	    }
	  else if (ent->health < (client->ps.stats[STAT_MAX_HEALTH] * 2))
	    {
	      ent->health += 5;
	      if (ent->health > (client->ps.stats[STAT_MAX_HEALTH] * 2))
		{
		  ent->health = client->ps.stats[STAT_MAX_HEALTH] * 2;
		}
	      G_AddEvent (ent, EV_POWERUP_REGEN, 0);
	    }
	}
      else
	{
	  if (!(g_dmflags->integer & DF_NO_LIFEDECAY))
	    {
	      // count down health when over max
	      if (ent->health > client->ps.stats[STAT_MAX_HEALTH])
		{
		  ent->health--;
		}
	    }
	}

      if (!(g_dmflags->integer & DF_NO_ARMORDECAY))
	{
	  // count down armor when over max
	  // create a STAT_MAX_ARMOR ?
	  if (client->ps.stats[STAT_ARMOR] > client->ps.stats[STAT_MAX_HEALTH])
	    {
	      client->ps.stats[STAT_ARMOR]--;
	    }
	}

      // update the time powerups are carried in a stats structures.
      // NOTE : the current code structure is too complicated
      // to be able to make everything "event" driven and then get accurate times
      // instead we will be second accurate for non critical items.
      for (i = PW_QUAD; i < PW_NUM_POWERUPS; i++)
	{
	  if (client->ps.powerups[i])
	    {
	      client->stats.powerups[i].time += 1000;
	    }
	}
    }
}

/*
====================
ClientIntermissionThink
====================
*/
void
ClientIntermissionThink (gclient_t * client)
{
  client->ps.eFlags &= ~EF_TALK;
  client->ps.eFlags &= ~EF_FIRING;

  // the level will exit when everyone wants to or after timeouts

  // swap and latch button actions
  client->misc.oldbuttons = client->misc.buttons;
  client->misc.buttons = client->pers.cmd.buttons;
  /*
    if (((client->misc.buttons & BUTTON_ATTACK)
      || (client->misc.buttons & BUTTON_USE_HOLDABLE))
      && (client->misc.oldbuttons != client->misc.buttons))
   */
  if (client->misc.buttons & (BUTTON_ATTACK | BUTTON_USE_HOLDABLE)
      & (client->misc.oldbuttons ^ client->misc.buttons))
    {
      // this used to be an ^1 but once a player says ready, it should stick
      client->misc.readyToExit = 1;
    }
}

/*
================
ClientEvents

Events will be passed on to the clients for presentation,
but any server game effects are handled here
================
*/
void
ClientEvents (gentity_t * ent, int oldEventSequence)
{
  int i, j;
  int event;
  int eventParm;
  gclient_t *client;
  int damage;
  vec3_t dir;
  vec3_t origin, angles;
  const gitem_t *item;
  gentity_t *drop;

  client = ent->client;

  if (oldEventSequence < (client->ps.eventSequence - MAX_PS_EVENTS))
    {
      oldEventSequence = client->ps.eventSequence - MAX_PS_EVENTS;
    }
  for (i = oldEventSequence; i < client->ps.eventSequence; i++)
    {
      event = client->ps.events[i & (MAX_PS_EVENTS - 1)];
      eventParm = client->ps.eventParms[i & (MAX_PS_EVENTS - 1)];

      switch (event)
	{
	case EV_COLLIDE_X:
	  {
	    if (ent->sh.s.eType != ET_PLAYER)
	      {
		break;		// not in the player model
	      }
	    G_Damage (ent, NULL, NULL, NULL, NULL, eventParm, 0, MOD_FACEPALM);
	    break;
	  }
	case EV_FALL_MEDIUM:
	case EV_FALL_FAR:
	  {
	    if (ent->sh.s.eType != ET_PLAYER)
	      {
		break;		// not in the player model
	      }
	    if (g_dmflags->integer & DF_NO_FALLING)
	      {
		break;
	      }
	    if (event == EV_FALL_FAR)
	      {
		damage = 10;
	      }
	    else
	      {
		damage = 5;
	      }
	    VectorSet (dir, 0, 0, 1);
	    ent->pain_debounce_time = level.time + 200;	// no normal pain sound
	    G_Damage (ent, NULL, NULL, NULL, NULL, damage, 0, MOD_FALLING);
	    break;
	  }
	case EV_FIRE_WEAPON:
	  {
	    FireWeapon (ent);
	    break;
	  }
	  // teleporter
	case EV_USE_ITEM1:
	  {
	    // drop flags in CTF
	    item = NULL;
	    j = 0;

	    if (ent->client->ps.powerups[PW_REDFLAG])
	      {
		item = BG_FindItemForPowerup (PW_REDFLAG);
		j = PW_REDFLAG;
		ent->client->stats.ctf.flagtime +=
		  level.time - ent->client->pers.teamState.flagsince;
	      }
	    else if (ent->client->ps.powerups[PW_BLUEFLAG])
	      {
		item = BG_FindItemForPowerup (PW_BLUEFLAG);
		j = PW_BLUEFLAG;
		ent->client->stats.ctf.flagtime +=
		  level.time - ent->client->pers.teamState.flagsince;
	      }

	    if (item)
	      {
		drop = Drop_Item (ent, item, 0);
		// decide how many seconds it has left
		drop->count = (ent->client->ps.powerups[j] - level.time) / 1000;
		if (drop->count < 1)
		  {
		    drop->count = 1;
		  }

		ent->client->ps.powerups[j] = 0;
	      }

	    SelectSpawnPoint (ent->client->ps.origin, origin, angles, false);
	    TeleportPlayer (ent, origin, angles);
	    break;
	  }
	  // medkit
	case EV_USE_ITEM2:
	  {
	    ent->health = ent->client->ps.stats[STAT_MAX_HEALTH] + 25;
	    break;
	  }
	default:
	  break;
	}
    }
}

/*
==============
SendPendingPredictableEvents
==============
*/
void
SendPendingPredictableEvents (playerState_t * ps)
{
  gentity_t *t;
  int event, seq;
  int extEvent, number;

  // if there are still events pending
  if (ps->entityEventSequence < ps->eventSequence)
    {
      // create a temporary entity for this event which is sent to everyone
      // except the client who generated the event
      seq = ps->entityEventSequence & (MAX_PS_EVENTS - 1);
      event = ps->events[seq] | ((ps->entityEventSequence & 3) << 8);
      // set external event to zero before calling BG_PlayerStateToEntityState
      extEvent = ps->externalEvent;
      ps->externalEvent = 0;
      // create temporary entity for event
      t = G_TempEntity (ps->origin, event);
      number = t->sh.s.number;
      BG_PlayerStateToEntityState (ps, &t->sh.s, true);
      t->sh.s.number = number;
      t->sh.s.eType = ET_EVENTS + event;
      t->sh.s.eFlags |= EF_PLAYER_EVENT;
      t->sh.s.otherEntityNum = ps->clientNum;
      // send to everyone except the client who generated the event
      t->sh.r.svFlags |= SVF_NOTSINGLECLIENT;
      t->sh.r.singleClient = ps->clientNum;
      // set back external event
      ps->externalEvent = extEvent;
    }
}

/*
==============
ClientThink

This will be called once for each client frame, which will
usually be a couple times for each server frame on fast clients.

If "g_synchronousClients 1" is set, this will be called exactly
once for each server frame, which makes for smooth demo recording.
==============
*/
void
ClientThink_real (gentity_t * ent)
{
  gclient_t *client;
  pmove_t pm;
  int oldEventSequence;
  int msec;
  usercmd_t *ucmd;

  client = ent->client;

  // don't think if the client is not yet connected (and thus not yet spawned in)
  if (client->pers.connected != CON_CONNECTED)
    {
      return;
    }
  // mark the time, so the connection sprite can be removed
  ucmd = &ent->client->pers.cmd;

  // sanity check the command time to prevent speedup cheating
  if (ucmd->serverTime > game.time + 200)
    {
      ucmd->serverTime = game.time + 200;
      // G_Printf("serverTime <<<<<\n" );
    }
  if (ucmd->serverTime < game.time - 1000)
    {
      ucmd->serverTime = game.time - 1000;
      // G_Printf("serverTime >>>>>\n" );
    }

  msec = ucmd->serverTime - client->ps.commandTime;
  // following others may result in bad times, but we still want
  // to check for follow toggles
  if ((msec < 1) && (client->sess.spectatorState != SPECTATOR_FOLLOW))
    {
      return;
    }
  if (msec > 200)
    {
      msec = 200;
    }

  //client->ps.stats[STAT_MATCHSTATUS] = level.matchStatus;
  // check for pausing
  if (game.paused)
    {
      client->ps.stats[STAT_MATCHSTATUS] |= MS_PAUSED;
    }
  else
    {
      client->ps.stats[STAT_MATCHSTATUS] &= ~MS_PAUSED;
    }

  if (level.noWeaponFire)
    {
      client->ps.stats[STAT_GAMEPLAY] |= GAMEPLAY_NOFIRE;
    }
  else
    {
      client->ps.stats[STAT_GAMEPLAY] &= ~GAMEPLAY_NOFIRE;
    }

  //
  // check for exiting intermission
  //
  if (level.match.end_flag >= END_INTERMISSION)
    {
      ClientIntermissionThink (client);
      return;
    }

  // spectators don't do much
  if (client->sess.sessionTeam == TEAM_SPECTATOR)
    {
      if (client->sess.spectatorState == SPECTATOR_SCOREBOARD)
	{
	  return;
	}
      SpectatorThink (ent, ucmd);
      return;
    }

  // check for inactivity timer, but never drop the local client of a non-dedicated server
  if (!ClientInactivityTimer (client))
    {
      return;
    }

  // clear the rewards if time
  if (level.time > client->timer.rewardTime)
    {
      client->ps.eFlags &= EF_CLEAR_AWARDS;
    }

  if (client->misc.noclip)
    {
      client->ps.pm_type = PM_NOCLIP;
    }
  else if (client->ps.stats[STAT_HEALTH] <= 0)
    {
      client->ps.pm_type = PM_DEAD;
    }
  else
    {
      client->ps.pm_type = PM_NORMAL;
    }

  client->ps.gravity = g_gravity->integer;

  // set speed
  client->ps.speed = g_speed->integer;

  if (client->ps.powerups[PW_HASTE])
    {
      client->ps.speed *= 1.3;
    }

  // set up for pmove
  oldEventSequence = client->ps.eventSequence;

  Com_Memset (&pm, 0, sizeof (pm));

  // check for the hit-scan gauntlet, don't let the action
  // go through as an attack unless it actually hits something
  if (client->ps.weapon == WP_GAUNTLET && !(ucmd->buttons & BUTTON_TALK) &&
      (ucmd->buttons & BUTTON_ATTACK) && client->ps.weaponTime <= 0)
    {
      pm.gauntletHit = CheckGauntletAttack (ent);
    }

  if (ent->flags & FL_FORCE_GESTURE)
    {
      ent->flags &= ~FL_FORCE_GESTURE;
      ent->client->pers.cmd.buttons |= BUTTON_GESTURE;
    }

  pm.ps = &client->ps;
  pm.cmd = *ucmd;
  if (pm.ps->pm_type == PM_DEAD)
    {
      pm.tracemask = MASK_PLAYERSOLID & ~CONTENTS_BODY;
    }
  else if (ent->sh.r.svFlags & SVF_BOT)
    {
      pm.tracemask = MASK_PLAYERSOLID | CONTENTS_BOTCLIP;
    }
  else
    {
      pm.tracemask = MASK_PLAYERSOLID;
    }
  pm.trace = gi->Trace;
  pm.pointcontents = gi->PointContents;
  pm.debugLevel = g_debugMove->integer;
  pm.noFootsteps = (g_dmflags->integer & DF_NO_FOOTSTEPS) > 0;
  pm.plmoves_flags = g_plmflags->integer;

  VectorCopy (client->ps.origin, client->misc.oldOrigin);

  Pmove (&pm);

  // save results of pmove
  if (ent->client->ps.eventSequence != oldEventSequence)
    {
      ent->eventTime = level.time;
    }

  BG_PlayerStateToEntityState (&ent->client->ps, &ent->sh.s, true);

  SendPendingPredictableEvents (&ent->client->ps);

  // use the snapped origin for linking so it matches client predicted versions
  VectorCopy (ent->sh.s.pos.trBase, ent->sh.r.currentOrigin);

  VectorCopy (pm.mins, ent->sh.r.mins);
  VectorCopy (pm.maxs, ent->sh.r.maxs);

  ent->waterlevel = pm.waterlevel;
  ent->watertype = pm.watertype;

  // we don't want to grab item or do actions with the level
  if (game.paused)
    {
      // swap and latch button actions
      client->misc.oldbuttons = client->misc.buttons;
      client->misc.buttons = ucmd->buttons;
      client->misc.latched_buttons |= client->misc.buttons & ~client->misc.oldbuttons;

      return;
    }

  // execute client events
  ClientEvents (ent, oldEventSequence);

  // link entity now, after any personal teleporters have been used
  gi->LinkEntity (&ent->sh);
  if (!ent->client->misc.noclip)
    {
      G_TouchTriggers (ent);
    }

  // NOTE: now copy the exact origin over otherwise clients can be snapped into solid
  VectorCopy (ent->client->ps.origin, ent->sh.r.currentOrigin);

  // test for solid areas in the AAS file
  BotTestAAS (ent->sh.r.currentOrigin);

  // touch other objects
  ClientImpacts (ent, &pm);

  // save results of triggers and client events
  if (ent->client->ps.eventSequence != oldEventSequence)
    {
      ent->eventTime = level.time;
    }

  // swap and latch button actions
  client->misc.oldbuttons = client->misc.buttons;
  client->misc.buttons = ucmd->buttons;
  client->misc.latched_buttons |= client->misc.buttons & ~client->misc.oldbuttons;

  // check for respawning
  if (client->ps.stats[STAT_HEALTH] <= 0)
    {
      // wait for the attack button to be pressed
      if (level.time > client->timer.respawnTime)
	{
	  // forcerespawn is to prevent users from waiting out powerups
	  if (g_forcerespawn->integer > 0 &&
	      (level.time - client->timer.respawnTime) >
	      (g_forcerespawn->integer * 1000))
	    {
	      respawn (ent);
	      return;
	    }

	  // pressing attack or use is the normal respawn method
	  if (ucmd->buttons & (BUTTON_ATTACK | BUTTON_USE_HOLDABLE))
	    {
	      respawn (ent);
	    }
	}
      return;
    }

  // perform once-a-second actions
  ClientTimerActions (ent, msec);
}

/*
==================
ClientThink

A new command has arrived from the client
==================
*/
void
ClientThink (int clientNum)
{
  gentity_t *ent;

  ent = g_entities + clientNum;
  gi->GetUsercmd (clientNum, &ent->client->pers.cmd);

  // mark the time we got info, so we can display the
  // phone jack if they don't get any for a while
  ent->client->timer.lastCmdTime = game.time;

  if (!(ent->sh.r.svFlags & SVF_BOT) && !g_synchronousClients->integer)
    {
      ClientThink_real (ent);
    }
}

/*
==================
G_RunClient
==================
*/
void
G_RunClient (gentity_t * ent)
{
  if (!(ent->sh.r.svFlags & SVF_BOT) && !g_synchronousClients->integer)
    {
      return;
    }
  ent->client->pers.cmd.serverTime = game.time;
  ClientThink_real (ent);
}

/*
==================
SpectatorClientEndFrame

==================
*/
void
SpectatorClientEndFrame (gentity_t * ent)
{
  gclient_t *cl;

  // if we are doing a chase cam or a remote view, grab the latest info
  if (ent->client->sess.spectatorState == SPECTATOR_FOLLOW)
    {
      int clientNum, flags;

      clientNum = ent->client->sess.spectatorClient;

      // team follow1 and team follow2 go to whatever clients are playing
      if (clientNum == -1)
	{
	  clientNum = level.follow1;
	}
      else if (clientNum == -2)
	{
	  clientNum = level.follow2;
	}
      if (clientNum >= 0)
	{
	  cl = &level.clients[clientNum];
	  if ((cl->pers.connected == CON_CONNECTED)
	      && (cl->sess.sessionTeam != TEAM_SPECTATOR))
	    {
	      flags = (cl->ps.eFlags & ~(EF_VOTED))
		       | (ent->client->ps.eFlags & (EF_VOTED));
	      ent->client->ps = cl->ps;
	      ent->client->ps.pm_flags |= PMF_FOLLOW;
	      ent->client->ps.eFlags = flags;
	      return;
	    }
	  else
	    {
	      // drop them to free spectators unless they
	      // are dedicated camera followers
	      if (ent->client->sess.spectatorClient >= 0)
		{
		  ent->client->sess.spectatorState = SPECTATOR_FREE;
		  ClientBegin (ent->client - level.clients);
		}
	    }
	}
    }

  if (ent->client->sess.spectatorState == SPECTATOR_SCOREBOARD)
    {
      ent->client->ps.pm_flags |= PMF_SCOREBOARD;
    }
  else
    {
      ent->client->ps.pm_flags &= ~PMF_SCOREBOARD;
    }
}

/*
==============
ClientEndFrame

Called at the end of each server frame for each connected client
A fast client will have multiple ClientThink for each ClientEdFrame,
while a slow client may have multiple ClientEndFrame between ClientThink.
==============
*/
void
ClientEndFrame (gentity_t * ent)
{
  int i;

  if (ent->client->sess.sessionTeam == TEAM_SPECTATOR)
    {
      SpectatorClientEndFrame (ent);
      return;
    }

  // turn off any expired powerups
  for (i = 0; i < MAX_POWERUPS; i++)
    {
      if (ent->client->ps.powerups[i] < level.time)
	{
	  ent->client->ps.powerups[i] = 0;
	}
    }

  //
  // If the end of unit layout is displayed, don't give
  // the player any normal movement attributes
  //
  if (level.match.end_flag >= END_INTERMISSION)
    {
      return;
    }

  // burn from lava, etc
  P_WorldEffects (ent);

  // apply all the damage taken this frame
  P_DamageFeedback (ent);

  // add the EF_CONNECTION flag if we haven't gotten commands recently
  if ((game.time - ent->client->timer.lastCmdTime) > 1000)
    {
      ent->sh.s.eFlags |= EF_CONNECTION;
    }
  else
    {
      ent->sh.s.eFlags &= ~EF_CONNECTION;
    }

  // used to remove health if > 100 (1 hp per sec)
  ent->client->ps.stats[STAT_HEALTH] = ent->health;	// FIXME: get rid of ent->health...

  G_SetClientSound (ent);

  // set the latest infor
  BG_PlayerStateToEntityState (&ent->client->ps, &ent->sh.s, true);

  SendPendingPredictableEvents (&ent->client->ps);

  // set the bit for the reachability area the client is currently in
  //    i = trap_AAS_PointReachabilityAreaIndex( ent->client->ps.origin );
  //    ent->client->areabits[i >> 3] |= 1 << (i & 7);
}

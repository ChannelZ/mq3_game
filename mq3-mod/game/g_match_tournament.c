/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_match.h"

// g_match.c
void CheckExitRules (void);

/*
========================================================================

PLAYER COUNTING / SCORE SORTING

========================================================================
*/

/*
=============
AddTournamentPlayer

If there are less than two tournament players, put a
spectator in the game and restart
=============
*/
void
AddTournamentPlayer (void)
{
  int i;
  gclient_t* client;
  gclient_t* nextInLine;

  if (level.numPlayingClients >= 2)
    {
      return;
    }

  // don't autoadd players
  if (g_tournamentStyle->integer)
    {
      return;
    }

  nextInLine = NULL;

  for (i = 0; i < level.maxclients; i++)
    {
      client = &level.clients[i];
      if (client->pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      if (client->sess.sessionTeam != TEAM_SPECTATOR)
	{
	  continue;
	}
      // never select the dedicated follow, scoreboard clients or speconly
      if ((client->sess.spectatorTime == -1)
	  || (client->sess.spectatorState == SPECTATOR_SCOREBOARD))
	{
	  continue;
	}

      if (!nextInLine
	  || (client->sess.spectatorTime < nextInLine->sess.spectatorTime))
	{
	  nextInLine = client;
	}
    }

  if (!nextInLine)
    {
      return;
    }

  level.warmupTime = -1;
  level.matchStatus |= MS_WARMUP;
  gi->SetConfigstring (CS_WARMUP, va ("%i", level.warmupTime));

  // set them to free-for-all team
  SetTeam (&g_entities[nextInLine - level.clients], "f");
}

/*
=======================
RemoveTournamentLoser

Make the loser a spectator at the back of the line
=======================
*/
void
RemoveTournamentLoser (void)
{
  int clientNum;

  if (level.numPlayingClients != 2)
    {
      return;
    }

  if (g_tournamentStyle->integer)
    {
      return;
    }

  clientNum = level.sortedClients[1];

  if (level.clients[clientNum].pers.connected != CON_CONNECTED)
    {
      return;
    }

  // make them a spectator
  SetTeam (&g_entities[clientNum], "s");
}

/*
=======================
RemoveTournamentWinner
=======================
*/
void
RemoveTournamentWinner (void)
{
  int clientNum;

  if (level.numPlayingClients != 2)
    {
      return;
    }

  if (g_tournamentStyle->integer)
    {
      return;
    }

  clientNum = level.sortedClients[0];

  if (level.clients[clientNum].pers.connected != CON_CONNECTED)
    {
      return;
    }

  // make them a spectator
  SetTeam (&g_entities[clientNum], "s");
}

/*
=======================
AdjustTournamentScores
=======================
*/
void
AdjustTournamentScores (lvl_t * level)
{
  int clientNum;

  if (level->numPlayingClients < 1)
    return;

  clientNum = level->sortedClients[0];
  if (level->clients[clientNum].pers.connected == CON_CONNECTED)
    {
      level->clients[clientNum].sess.wins++;
      ClientUserinfoChanged (clientNum);
    }

  if (level->numPlayingClients < 2)
    return;

  clientNum = level->sortedClients[1];
  if (level->clients[clientNum].pers.connected == CON_CONNECTED)
    {
      level->clients[clientNum].sess.losses++;
      ClientUserinfoChanged (clientNum);
    }
}

/*
=============
SortRanks

=============
*/
int
SortRanks (const void *a, const void *b)
{
  gclient_t *ca, *cb;

  ca = &level.clients[*(int *) a];
  cb = &level.clients[*(int *) b];

  // sort special clients last
  if ((ca->sess.spectatorState == SPECTATOR_SCOREBOARD)
      || (ca->sess.spectatorClient < 0))
    {
      return 1;
    }
  if ((cb->sess.spectatorState == SPECTATOR_SCOREBOARD)
      || (cb->sess.spectatorClient < 0))
    {
      return -1;
    }

  // then connecting clients
  if (ca->pers.connected == CON_CONNECTING)
    {
      return 1;
    }
  if (cb->pers.connected == CON_CONNECTING)
    {
      return -1;
    }

  // then spectators
  if ((ca->sess.sessionTeam == TEAM_SPECTATOR)
      && (cb->sess.sessionTeam == TEAM_SPECTATOR))
    {
      if (ca->sess.spectatorTime < cb->sess.spectatorTime)
	{
	  return -1;
	}
      if (ca->sess.spectatorTime > cb->sess.spectatorTime)
	{
	  return 1;
	}
      return 0;
    }
  if (ca->sess.sessionTeam == TEAM_SPECTATOR)
    {
      return 1;
    }
  if (cb->sess.sessionTeam == TEAM_SPECTATOR)
    {
      return -1;
    }

  // then sort by score
  if (ca->ps.persistant[PERS_SCORE] > cb->ps.persistant[PERS_SCORE])
    {
      return -1;
    }
  if (ca->ps.persistant[PERS_SCORE] < cb->ps.persistant[PERS_SCORE])
    {
      return 1;
    }
  return 0;
}

/*
============
CalculateRanks

Recalculates the score ranks of all players
This will be called on every client connect, begin, disconnect, death,
and team change.
============
*/
void
CalculateRanks (void)
{
  int i;
  int rank;
  int score;
  int newScore;
  gclient_t *cl;

  level.follow1 = -1;
  level.follow2 = -1;
  level.numConnectedClients = 0;
  level.numNonSpectatorClients = 0;
  level.numPlayingClients = 0;
  level.numVotingClients = 0;	// don't count bots

  for (i = 0; i < level.maxclients; i++)
    {
      // consider CON_CONNECTING, so it can be displayed on scoreboard
      if (level.clients[i].pers.connected != CON_DISCONNECTED)
	{
	  level.sortedClients[level.numConnectedClients] = i;
	  level.numConnectedClients++;

	  if (level.clients[i].sess.sessionTeam != TEAM_SPECTATOR)
	    {
	      level.numNonSpectatorClients++;

	      // decide if this should be auto-followed
	      if (level.clients[i].pers.connected == CON_CONNECTED)
		{
		  level.numPlayingClients++;
		  if (!(g_entities[i].sh.r.svFlags & SVF_BOT))
		    {
		      level.numVotingClients++;
		    }
		  // follow the first one valid
		  if (level.follow1 == -1)
		    {
		      level.follow1 = i;
		    }
		  else if (level.follow2 == -1)
		    {
		      level.follow2 = i;
		    }
		}
	    }
	}
    }
  // do the sorting
  qsort (level.sortedClients, level.numConnectedClients,
	 sizeof (level.sortedClients[0]), SortRanks);

  // set the rank value for all clients that are connected and not spectators
  if (BG_GametypeIsTeam (g_gametype->integer))
    {
      // in team games, rank is just the order of the teams, 0=red, 1=blue, 2=tied
      for (i = 0; i < level.numConnectedClients; i++)
	{
	  cl = &level.clients[level.sortedClients[i]];
	  if (level.teamScores[TEAM_RED] == level.teamScores[TEAM_BLUE])
	    {
	      cl->ps.persistant[PERS_RANK] = 2;
	    }
	  else if (level.teamScores[TEAM_RED] > level.teamScores[TEAM_BLUE])
	    {
	      cl->ps.persistant[PERS_RANK] = 0;
	    }
	  else
	    {
	      cl->ps.persistant[PERS_RANK] = 1;
	    }
	}
    }
  else
    {
      rank = -1;
      score = 0;
      for (i = 0; i < level.numPlayingClients; i++)
	{
	  cl = &level.clients[level.sortedClients[i]];
	  newScore = cl->ps.persistant[PERS_SCORE];
	  if (i == 0 || newScore != score)
	    {
	      rank = i;
	      // assume we aren't tied until the next client is checked
	      level.clients[level.sortedClients[i]].ps.persistant[PERS_RANK] =
		rank;
	    }
	  else
	    {
	      // we are tied with the previous client
	      level.clients[level.sortedClients[i - 1]].ps.
		persistant[PERS_RANK] = rank | RANK_TIED_FLAG;
	      level.clients[level.sortedClients[i]].ps.persistant[PERS_RANK] =
		rank | RANK_TIED_FLAG;
	    }
	  score = newScore;
	  /*
	  if (g_gametype->integer == GT_SINGLE_PLAYER
	      && level.numPlayingClients == 1)
	    {
	      level.clients[level.sortedClients[i]].ps.persistant[PERS_RANK]
		= rank | RANK_TIED_FLAG;
	    }
	  */
	}
    }

  // set the CS_SCORES1/2 configstrings, which will be visible to everyone
  if (BG_GametypeIsTeam (g_gametype->integer))
    {
      gi->SetConfigstring (CS_SCORES1, va ("%i", level.teamScores[TEAM_RED]));
      gi->SetConfigstring (CS_SCORES2, va ("%i", level.teamScores[TEAM_BLUE]));
    }
  else
    {
      if (level.numConnectedClients == 0)
	{
	  gi->SetConfigstring (CS_SCORES1, va ("%i", SCORE_NOT_PRESENT));
	  gi->SetConfigstring (CS_SCORES2, va ("%i", SCORE_NOT_PRESENT));
	}
      else if (level.numConnectedClients == 1)
	{
	  gi->SetConfigstring (CS_SCORES1,
			       va ("%i", level.clients[level.sortedClients[0]].
				   ps.persistant[PERS_SCORE]));
	  gi->SetConfigstring (CS_SCORES2, va ("%i", SCORE_NOT_PRESENT));
	}
      else
	{
	  gi->SetConfigstring (CS_SCORES1,
			       va ("%i", level.clients[level.sortedClients[0]].
				   ps.persistant[PERS_SCORE]));
	  gi->SetConfigstring (CS_SCORES2,
			       va ("%i", level.clients[level.sortedClients[1]].
				   ps.persistant[PERS_SCORE]));
	}
    }

  // after we calculated the new ranks at the end of ClientBegin
  // if we are in tournament game, update team1, team2 names
  if (g_gametype->integer == GT_TOURNAMENT)
    {
      if (level.numPlayingClients == 2)
	{
	  const char *msg;

	  msg = level.clients[level.sortedClients[0]].pers.netname;
	  gi->SetConfigstring (CS_TEAM1_NAME, msg);

	  msg = level.clients[level.sortedClients[1]].pers.netname;
	  gi->SetConfigstring (CS_TEAM2_NAME, msg);
	}
      else if (level.numPlayingClients == 1)
	{
	  // reset the configstrings
	  const char *msg;

	  msg = level.clients[level.sortedClients[0]].pers.netname;
	  gi->SetConfigstring (CS_TEAM1_NAME, msg);

	  gi->SetConfigstring (CS_TEAM1_NAME, msg);
	  gi->SetConfigstring (CS_TEAM2_NAME, "");
	}
      else
	{
	  gi->SetConfigstring (CS_TEAM1_NAME, "");
	  gi->SetConfigstring (CS_TEAM2_NAME, "");
	}
    }

  // see if it is time to end the level
  CheckExitRules ();

  // if we are at the intermission, send the new info to everyone
  if (level.match.end_flag >= END_INTERMISSION)
    {
      SendScoreboardMessageToAllClients (&level);
    }
}

/*
==================
CheckReady
==================
*/
static bool
CheckReady (void)
{
  int i;
  gclient_t *cl;
  int readyCount;
  bool onlyBots = true;
  int mask;

  mask = 0;
  readyCount = 0;

  // if we don't do any warmup or match is live or not enought players, skip
  if (!g_doWarmup->integer || (level.matchStatus & MS_LIVE)
      || (level.numConnectedClients < 2))
    return false;

  // don't start match with only bots connected
  for (i = 0; i < level.numConnectedClients; i++)
    {
      cl = level.clients + level.sortedClients[i];
      // a connected human !
      if (!(g_entities[(level.sortedClients[i])].sh.r.svFlags & SVF_BOT)
	  && (cl->pers.connected == CON_CONNECTED))
	{
	  onlyBots = false;
	  break;
	}
    }
  if (onlyBots)
    return false;

  for (i = 0; i < level.numConnectedClients; i++)
    {
      cl = level.clients + level.sortedClients[i];
      if (cl->pers.connected != CON_CONNECTED
	  || cl->sess.sessionTeam >= TEAM_SPECTATOR)
	{
	  continue;
	}
      // bots are always ready
      else if ((cl->pers.readyClient)
	       || (g_entities[(level.sortedClients[i])].sh.r.svFlags & SVF_BOT))
	{
	  readyCount++;
	  mask |= 1 << level.sortedClients[i];
	}
    }

  gi->SetConfigstring (CS_PLAYER_READY_MASK, va ("%i", mask));

  if (readyCount >= level.numPlayingClients)
    {
      return true;
    }
  else
    {
      return false;
    }
}

/*
=============
G_Match_CheckTournament

Once a frame, check for changes in tournement player state
=============
*/
#define MATCH_COUNTDOWN_TIME 10000
#define COUNT_RED 0
#define COUNT_BLUE 1

// from g_match_tournament.c
void AddTournamentPlayer (void);

void
G_Match_CheckTournament (void)
{
  bool notEnough = false;
  int counts[2];
  bool startNow = false;

  // check because we run 3 game frames before calling Connect and/or ClientBegin
  // for clients on a map_restart
  if (level.numPlayingClients == 0)
    {
      return;
    }

  if (level.match.end_flag >= END_CALLED)
    {
      return;
    }

  // if we are live, don't do anything, if a tournament player raged quit
  // don't change warmupTime as the intermission will appear
  if (level.matchStatus & MS_LIVE)
    {
      if (level.warmupTime)
	{
	  char *msg;

	  level.warmupTime = 0;

	  msg = va ("%i", level.warmupTime);
	  gi->SetConfigstring (CS_WARMUP, msg);
	}
      return;
    }

  if (level.numPlayingClients < 2)
    {
      notEnough = true;

      // handle duel case and be sure we do not add a player
      // just before intermission
      if (g_gametype->integer == GT_TOURNAMENT && !(level.match.end_flag >= END_CALLED))
	{
	  // pull in a spectator if needed
	  AddTournamentPlayer ();
	  return;
	}
    }

  if (BG_GametypeIsTeam (g_gametype->integer))
    {
      counts[COUNT_RED] = TeamCount (-1, TEAM_RED);
      counts[COUNT_BLUE] = TeamCount (-1, TEAM_BLUE);

      if ((counts[COUNT_RED] < 1) || (counts[COUNT_BLUE] < 1))
	{
	  notEnough = true;
	}
    }

  // if we don't have two players, go back to "waiting for players"
  if (notEnough)
    {
      if (level.warmupTime != -1)
	{
	  level.warmupTime = -1;
	  level.matchStatus = MS_NONE;
	  gi->SetConfigstring (CS_WARMUP, va ("%i", level.warmupTime));
	  G_LogPrintf ("Waiting for players:\n");
	}
      return;		// still waiting for players
    }

  // assume we have enough players here

  // if we came from waiting for players, start warump
  if (level.matchStatus & MS_NONE)
    {
      level.matchStatus |= MS_WARMUP;
      level.matchStatus &= ~MS_NONE;
    }

  // if the warmup changed in the console, restart it
  if (g_warmuptime->modificationCount != level.warmupModificationCount)
    {
      level.warmupModificationCount = g_warmuptime->modificationCount;
      //level.warmupTime = -1;
      level.matchStatus |= MS_WARMUP;
      return;
    }

  switch (g_doWarmup->integer)
    {
      // we don't wait, start when possible
    default:
      // we wait for g_warmuptime seconds after map start to begin
      // if negative, start immediatly
    case 0:
      {
	// not yet set up
	if (level.matchStatus & MS_WARMUP
	    && !(level.matchStatus & MS_TIMESET))
	  {
	    char *msg;
	    int delay = g_warmuptime->integer - 1;

	    // we want the legacy behaviour
	    if (delay < 0)
	      {
		delay = -1;
		level.warmupTime = 0;
	      }
	    else
	      {
		level.warmupTime = level.time + delay * 1000;
	      }

	    // fudge by -1 to account for extra delays
	    level.matchStatus |= MS_TIMESET;
	    msg = va ("%i", level.warmupTime);
	    gi->SetConfigstring (CS_WARMUP, msg);
	    msg = va ("%i", game.time + delay * 1000);
	    gi->SetConfigstring (CS_ANNOUNCER_TIME, msg);
	  }
	else
	  {
	    // if announcer time is set and elapsed, time to start
	    if (level.time >= level.warmupTime)
	      {
		startNow = true;
	      }
	  }
	break;
      }
      // we wait for all the players to be ready to begin
    case 1:
      {
	bool readyToStart = CheckReady ();

	// players not ready
	if (!readyToStart && (level.matchStatus & MS_WARMUP))
	  {
	    // tell the client we are waiting for players to ready
	    if (!(level.matchStatus & MS_COUNTDOWN))
	      {
		level.warmupTime = -2;
		gi->SetConfigstring (CS_WARMUP, va ("%i", level.warmupTime));
	      }

	    // abord countdown if a player unready at the last second
	    else if ((level.matchStatus & MS_COUNTDOWN))
	      {
		gi->SendServerCommand (-1, "print \"" S_COLOR_RED
				       "Countdown Stopped !\n\"");
		gi->SendServerCommand (-1, "cp \"" S_COLOR_RED
				       "Countdown Stopped !\n\"");
		level.warmupTime = -2;
		level.matchStatus &= ~MS_COUNTDOWN;

		// restore firing after countdown break
		level.noWeaponFire = false;
		level.noDamage = false;

		gi->SetConfigstring (CS_WARMUP, va ("%i", level.warmupTime));
		gi->SetConfigstring (CS_ANNOUNCER_TIME, "0");
		// send the new matchstatus to everyone
		gi->SendServerCommand (-1, va ("matchstatus %i", MSE_CDBREAK));
		return;		// forward to client
	      }
	  }

	// players ready
	// MS_WARMUP check is not really needed
	else if (readyToStart && (level.matchStatus & MS_WARMUP)
		 && !(level.matchStatus & MS_COUNTDOWN))
	  {
	    char *msg;
	    level.warmupTime = level.time + MATCH_COUNTDOWN_TIME;
	    // start countdown
	    level.matchStatus |= MS_COUNTDOWN;

	    msg = va ("%i", level.warmupTime);
	    gi->SetConfigstring (CS_WARMUP, msg);
	    msg = va ("%i", game.time + MATCH_COUNTDOWN_TIME);
	    gi->SetConfigstring (CS_ANNOUNCER_TIME, msg);

	    // send the new matchstatus to everyone
	    gi->SendServerCommand (-1, va ("matchstatus %i", MSE_CDSTART));

	    // stop firing during countdown
	    level.noWeaponFire = true;
	    level.noDamage = true;

	    return;		// let the client know
	  }
	// start game
	if ((level.time >= level.warmupTime)
	    && (level.matchStatus & MS_COUNTDOWN))
	  {
	    startNow = true;
	  }
	break;
      }
    }

  if (startNow == true)
    {
      G_Match_Start (&level, "Lets get ready to Rumble !!");
    }
}

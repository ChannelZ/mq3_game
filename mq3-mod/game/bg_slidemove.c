/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// bg_slidemove.c -- part of bg_pmove functionality

#include "qcommon/q_shared.h"
#include "bg_public.h"
#include "bg_local.h"

/*
============
PM_StepEvent
============
*/
void
PM_StepEvent (pmove_t* pm, int delta)
{
  if (delta > 2)
    {
      if (delta < 7)
	{
	  PM_AddEvent (pm, EV_STEP_4);
	}
      else if (delta < 11)
	{
	  PM_AddEvent (pm, EV_STEP_8);
	}
      else if (delta < 15)
	{
	  PM_AddEvent (pm, EV_STEP_12);
	}
      else if (delta < 19)
	{
	  PM_AddEvent (pm, EV_STEP_16);
	}
    }
  // take negative and big positive values into account
  else // if (delta < 2)
    {
      // check for boundary values
      // use STEPSIZE instead ?
      if (delta > 127)
	{
	  delta = 127;
	}
      else if (delta < -127)
	{
	  delta = -127;
	}
      // determine sign
      if (delta > 0)
	{
	  // obvious from previous lines
	  // delta = delta & ~EVENT_STEP_ORIENTATION;
	}
      else
	{
	  // tag this delta step as "negative"
	  delta = (abs (delta)) | EVENT_STEP_ORIENTATION;
	}

      BG_AddPredictableEventToPlayerstate (EV_STEP_X, delta, pm->ps);
    }
}

/*

input: origin, velocity, bounds, groundPlane, trace function

output: origin, velocity, impacts, stairup boolean

*/

/*
==================
PM_SlideMove

Returns true if the velocity was clipped in some way
==================
*/
#define	MAX_CLIP_PLANES	5
bool
PM_SlideMove (pmove_t* pm, pml_t* pml, bool gravity, bool stepUp, bool stepDown)
{
  int bumpcount, numbumps;
  int i, j, k;
  vec3_t dir;
  float d;
  int numplanes;
  vec3_t planes[MAX_CLIP_PLANES];
  vec3_t primal_velocity;
  vec3_t clipVelocity;
  trace_t trace;
  vec3_t end;
  float time_left;
  float into;
  vec3_t endVelocity;
  vec3_t endClipVelocity;
  float delta;
  vec3_t stepEnd;
  trace_t downtrace;
  trace_t steptrace;
  float maxStepHeight = STEPSIZE;

  numbumps = 4;

  VectorCopy (pm->ps->velocity, primal_velocity);

  if (gravity)
    {
      VectorCopy (pm->ps->velocity, endVelocity);
      endVelocity[2] -= pm->ps->gravity * pml->frametime;
      pm->ps->velocity[2] = (pm->ps->velocity[2] + endVelocity[2]) * 0.5;
      primal_velocity[2] = endVelocity[2];
      if (pml->groundPlane)
	{
	  // slide along the ground plane
	  PM_ClipVelocity (pm->ps->velocity, pml->groundTrace.plane.normal,
			   pm->ps->velocity, OVERCLIP);
	}
    }

  time_left = pml->frametime;

  // never turn against the ground plane
  if (pml->groundPlane)
    {
      numplanes = 1;
      VectorCopy (pml->groundTrace.plane.normal, planes[0]);
    }
  else
    {
      numplanes = 0;
    }

  // never turn against original velocity
  VectorNormalize2 (pm->ps->velocity, planes[numplanes]);
  numplanes++;

  for (bumpcount = 0; bumpcount < numbumps; bumpcount++)
    {
      // calculate position we are trying to move to
      VectorMA (pm->ps->origin, time_left, pm->ps->velocity, end);

      // see if we can make it there
      pm->trace (&trace, pm->ps->origin, pm->mins, pm->maxs,
		 end, pm->ps->clientNum, pm->tracemask);

      if (trace.allsolid)
	{
	  // entity is completely trapped in another solid
	  pm->ps->velocity[2] = 0;	// don't build up falling damage, but allow sideways acceleration
	  return true;
	}

      if (trace.fraction > 0)
	{
	  // actually covered some distance
	  VectorCopy (trace.endpos, pm->ps->origin);
	}

      if (trace.fraction == 1)
	{
	  break;	// moved the entire distance
	}

      // save entity for contact
      PM_AddTouchEnt (pm, trace.entityNum);

      time_left -= time_left * trace.fraction;

      // ---------------------------------------------------------------

      if (stepUp)
	{
	  int nearGround = pml->groundPlane;

	  if (!nearGround)
	    {
	      VectorCopy (pm->ps->origin, stepEnd);
	      stepEnd[2] -= maxStepHeight;

	      pm->trace (&downtrace, pm->ps->origin, pm->mins, pm->maxs,
			 stepEnd, pm->ps->clientNum, pm->tracemask);

	      nearGround = ((downtrace.fraction < 1.0f)
			    && (downtrace.plane.normal[2] > MIN_WALK_NORMAL));
	    }

	  // may only step up if near the ground or on a ladder
	  if (nearGround)
	    {
	      // step up
	      VectorCopy (pm->ps->origin, stepEnd);
	      stepEnd[2] += maxStepHeight;
	      pm->trace (&downtrace, pm->ps->origin, pm->mins, pm->maxs,
			 stepEnd, pm->ps->clientNum, pm->tracemask);

	      // trace along velocity
	      VectorMA (downtrace.endpos, time_left, pm->ps->velocity, stepEnd);
	      pm->trace (&steptrace, downtrace.endpos, pm->mins, pm->maxs,
			 stepEnd, pm->ps->clientNum, pm->tracemask);

	      // step down
	      VectorCopy (steptrace.endpos, stepEnd);
	      stepEnd[2] -= maxStepHeight;
	      pm->trace (&downtrace, steptrace.endpos, pm->mins, pm->maxs,
			 stepEnd, pm->ps->clientNum, pm->tracemask);

	      if ((downtrace.fraction == 1.0f)
		  || (downtrace.plane.normal[2] > MIN_WALK_NORMAL))
		{
		  if (steptrace.fraction == 1.0f)
		    {
		      //time_left = 0; // unused

		      delta = (downtrace.endpos[2] - pm->ps->origin[2]);
		      VectorCopy (downtrace.endpos, pm->ps->origin);

		      PM_StepEvent (pm, (int) delta);

		      break;
		    }

		  // if the move is further when stepping up
		  if (steptrace.fraction > trace.fraction)
		    {
		      time_left -= time_left * steptrace.fraction;

		      delta = (downtrace.endpos[2] - pm->ps->origin[2]);
		      VectorCopy (downtrace.endpos, pm->ps->origin);

		      trace = steptrace;

		      PM_StepEvent (pm, (int) delta);
		    }
		}
	    }
	}

      // ---------------------------------------------------------------

      if (numplanes >= MAX_CLIP_PLANES)
	{
	  // this shouldn't really happen
	  VectorClear (pm->ps->velocity);
	  return true;
	}

      //
      // if this is the same plane we hit before, nudge velocity
      // out along it, which fixes some epsilon issues with
      // non-axial planes
      //
      for (i = 0; i < numplanes; i++)
	{
	  if (DotProduct (trace.plane.normal, planes[i]) > 0.99)
	    {
	      VectorAdd (trace.plane.normal, pm->ps->velocity, pm->ps->velocity);
	      break;
	    }
	}
      if (i < numplanes)
	{
	  continue;
	}
      VectorCopy (trace.plane.normal, planes[numplanes]);
      numplanes++;

      //
      // modify velocity so it parallels all of the clip planes
      //

      // find a plane that it enters
      for (i = 0; i < numplanes; i++)
	{
	  into = DotProduct (pm->ps->velocity, planes[i]);
	  if (into >= 0.1)
	    {
	      continue;		// move doesn't interact with the plane
	    }

	  // see how hard we are hitting things
	  if (-into > pml->impactSpeed)
	    {
	      pml->impactSpeed = -into;
	    }

	  // slide along the plane
	  PM_ClipVelocity (pm->ps->velocity, planes[i], clipVelocity, OVERCLIP);

	  // slide along the plane
	  PM_ClipVelocity (endVelocity, planes[i], endClipVelocity, OVERCLIP);

	  // see if there is a second plane that the new move enters
	  for (j = 0 ; j < numplanes ; j++)
	    {
	      if (j == i)
		{
		  continue;
		}
	      if (DotProduct (clipVelocity, planes[j]) >= 0.1)
		{
		  continue;	// move doesn't interact with the plane
		}

	      // try clipping the move to the plane
	      PM_ClipVelocity (clipVelocity, planes[j], clipVelocity, OVERCLIP);
	      PM_ClipVelocity (endClipVelocity, planes[j], endClipVelocity, OVERCLIP);

	      // see if it goes back into the first clip plane
	      if (DotProduct (clipVelocity, planes[i]) >= 0)
		{
		  continue;
		}

	      // slide the original velocity along the crease
	      CrossProduct (planes[i], planes[j], dir);
	      VectorNormalize (dir);
	      d = DotProduct (dir, pm->ps->velocity);
	      VectorScale (dir, d, clipVelocity);

	      CrossProduct (planes[i], planes[j], dir);
	      VectorNormalize (dir);
	      d = DotProduct (dir, endVelocity);
	      VectorScale (dir, d, endClipVelocity);

	      // see if there is a third plane the the new move enters
	      for (k = 0 ; k < numplanes ; k++)
		{
		  if (k == i || k == j)
		    {
		      continue;
		    }
		  if (DotProduct (clipVelocity, planes[k]) >= 0.1)
		    {
		      continue;		// move doesn't interact with the plane
		    }

		  // stop dead at a tripple plane interaction
		  VectorClear (pm->ps->velocity);
		  return true;
		}
	    }

	  // if we have fixed all interactions, try another move
	  VectorCopy (clipVelocity, pm->ps->velocity);
	  VectorCopy (endClipVelocity, endVelocity);
	  break;
	}
    }

  // ---------------------------------------------------------------

  // step down
  if (stepDown && pml->groundPlane)
    {
      if ((pm->cmd.buttons & BUTTON_WALKING)
	  && (VectorLength (pm->ps->velocity) < pm->ps->speed))
	{
	  VectorCopy (pm->ps->origin, stepEnd);
	  stepEnd[2] -= maxStepHeight;

	  pm->trace (&downtrace, pm->ps->origin, pm->mins, pm->maxs,
		     stepEnd, pm->ps->clientNum, pm->tracemask);

	  const float trace_min = 0.0f; // 1e-4f
	  if ((downtrace.fraction > trace_min) && (downtrace.fraction < 1.0f))
	    {
	      delta = (downtrace.endpos[2] - pm->ps->origin[2]);
	      VectorCopy (downtrace.endpos, pm->ps->origin);

	      PM_StepEvent (pm, delta);
	    }
	}
    }

  // ---------------------------------------------------------------

  if (gravity)
    {
      VectorCopy (endVelocity, pm->ps->velocity);
    }

  // don't change velocity if in a timer (FIXME: is this correct?)
  if (pm->ps->pm_time)
    {
      VectorCopy (primal_velocity, pm->ps->velocity);
    }

  return (bumpcount != 0);
}

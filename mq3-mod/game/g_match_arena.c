/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_match.h"

// Arena gametype works by playing successive rounds

void
G_Match_RoundFrame (lvl_t * level)
{
  // rounds are only available within arena gametype
  if (g_gametype->integer != GT_ARENA)
    {
      return;
    }

  // check for round status
}

void
G_Match_RoundWarmup (lvl_t * level)
{

}

void
G_Match_RoundLive (lvl_t * level)
{

}

void
G_Match_RoundEnd (lvl_t * level)
{

}

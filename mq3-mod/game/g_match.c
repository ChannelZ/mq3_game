/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_match.h"

//void G_MoveClientToIntermission (lvl_t * level, gentity_t * ent);
void AdjustTournamentScores (lvl_t * level);

/*
===========
G_Clean_Entity_Events

Clean all events assiociated with the given entity
This will clean/reset non player entities
===========
*/
// do me a proper way
extern void MatchTeam (gentity_t * teamLeader, int moverState, int time);

static void
G_Clean_Entity_Events (lvl_t * level, gentity_t * gent)
{
  // clean all events if the entity is active
  if (gent->inuse)
    {
      // the entity is active; reset all events eventTime
      gent->eventTime = 0;

      // entity shared events
      if (gent->sh.s.event)
	{
	  gent->sh.s.event = 0;	// &= EV_EVENT_BITS;
	  // gent->sh.s.eventParm = 0; // ???

	  // cleaning client events
	  if (gent->client)
	    {
	      gent->client->ps.externalEvent = 0;
	      // predicted events should never be set to zero
	      //ent->client->ps.events[0] = 0;
	      //ent->client->ps.events[1] = 0;
	      // clear rewards
	      gent->client->ps.eFlags &= EF_CLEAR_AWARDS;
	      gent->client->timer.rewardTime = 0;
	    }
	}
      // if the entity is to get unlinked after the event, unlink it now
      else if (gent->unlinkAfterEvent)
	{
	  // items that will respawn will hide themselves after their pickup event
	  gent->unlinkAfterEvent = false;
	  gi->UnlinkEntity (&gent->sh);
	}
      // temp entities, free them
      if (gent->sh.s.eType >= ET_EVENTS)
	{
	  G_FreeEntity (gent);
	  //continue;
	  return;
	}

      // respawning/reseting all other non player entities
      switch (gent->sh.s.eType)
	{
	case ET_ITEM:
	  {
	    if (gent->item != NULL)
	      {
		gent->nextthink = FRAMETIME; // don't do it this frame
		gent->think = ItemDefaultConfiguration;
	      }
	    // dropped items will not respawn
	    if (gent->flags & FL_DROPPED_ITEM)
	      {
		G_FreeEntity (gent);
	      }
	    break;
	  }
	  // reactivating triggers
	case ET_MOVER:
	  {
	    // setting movers to their initial position
	    MatchTeam (gent, MOVER_POS1, level->time);
	    break;
	  }
	case ET_GENERAL: // default so it can be unused
	  {
	    // free the entity ? or let some time so the event is run
	    break;
	  }
	case ET_MISSILE:
	  {
	    // free the entity
	    G_FreeEntity (gent);
	    break;
	  }
	case ET_BEAM:
	  {
	    // nothing to do
	    break;
	  }
	case ET_PORTAL:
	  {
	    // nothing to do
	    break;
	  }
	case ET_SPEAKER:
	  {
	    // nothing to do
	    break;
	  }
	case ET_PUSH_TRIGGER:
	  {
	    // something to do ?
	    break;
	  }
	case ET_TELEPORT_TRIGGER:
	  {
	    // nothing to do
	    break;
	  }
	case ET_DOOR_TRIGGER:
	  {
	    // nothing to do
	    break;
	  }
	case ET_INVISIBLE:
	  {
	    // nothing to do
	    break;
	  }
	case ET_TEAM:
	  {
	    // nothing to do
	    break;
	  }
	  // this shouldn't happend
	case ET_PLAYER:
	  {
	    int num;
	    // we might have ET_PLAYER entity in the body_queue
	    num = gent - g_entities;
	    if ((num >= MAX_CLIENTS) && (num < MAX_CLIENTS + BODY_QUEUE_SIZE))
	      {
		gi->UnlinkEntity (&gent->sh);
		gent->physicsObject = false;
	      }
	    else
	      {
		G_Error ("G_Clean_Entity_Events () : parsing entities found an ET_PLAYER : %d\n",
			 gent - g_entities);
	      }
	    break;
	  }

	default:
	  {
	    G_Error ("G_Clean_Entity_Events () : unknown entity type : %d\n",
		     gent - g_entities);
	    break;
	  }
	}
    }
}

/*
========================
G_Clean_Entity_EventsAll

Clean All non player entities
========================
*/
static void
G_Clean_Entity_EventsAll (lvl_t * level)
{
  int i;
  gentity_t* gent;

  gent = &g_entities[MAX_CLIENTS];

  for (i = MAX_CLIENTS; i < /*MAX_GENTITIES*/level->num_entities; i++, gent++)
    {
      G_Clean_Entity_Events (level, gent);
    }
}

/*
======================
G_Match_Start_SpawnAll
======================
*/
void
G_Match_Start_SpawnAll (lvl_t * level)
{
  int i;
  gentity_t* gent;
  gclient_t* cl;

  // doing the client stuff now
  for (i = 0; i < /*MAX_CLIENTS*/level->maxclients; i++)
    {
      gent = &g_entities[i];

      if (G_Client_ValidEntity (gent))
	{
	  cl = gent->client;

	  // next spawn will at a special spawn if needed
	  // we didn't get initial spawn yet
	  cl->pers.initialSpawn = false;
	  cl->pers.teamState.state = TEAM_BEGIN; // for ctf

	  // wipe gclient fields; most are by ClientSpawn; also reset stats
	  Com_Memset (&cl->stats, 0, sizeof (cl->stats));

	  // let the client know the match started
	  gi->SendServerCommand (i, va ("matchstatus %i", MSE_START));

	  // respawn the player
	  ClientSpawn (gent);
	}
    }
}

/*
=============
G_Match_Start

=============
*/
void
G_Match_Start (lvl_t * level, const char * msg)
{
  char* msgtime;

  G_LogPrintf ("match : started\n");

  // change match status to live here; client respawn won't be in warmup mode
  level->matchStatus = MS_LIVE; // no other flags are needed for now

  // wipe level fields
  Com_Memset (&level->stats, 0, sizeof (level->stats));
  level->numberovertime = 0;
  level->overtimeDisplayed = false;
  //level.restarted = false; // should'nt be set to true here (restart game module)
  level->lastTeamLocationTime = 0;
  level->teamScores[TEAM_FREE] = 0;
  level->teamScores[TEAM_RED] = 0;
  level->teamScores[TEAM_BLUE] = 0;
  level->teamScores[TEAM_SPECTATOR] = 0;

  // take damage and fire weapons
  level->noWeaponFire = false;
  level->noDamage = false;

  level->framenum = 0;
  level->time = 0;
  level->previousTime = 0;

  level->warmupTime = 0;

  msgtime = va ("%i", level->warmupTime);
  gi->SetConfigstring (CS_WARMUP, msgtime);

  // reset warmup / prematch variables
  gi->SetConfigstring (CS_PLAYER_READY_MASK, va ("%i", 0));

  // reset internal teamgame structure
  Team_InitGame (&teamgame);

  // reseting all entities except clients
  G_Clean_Entity_EventsAll (level);
  G_Printf ("wiped all non client entities\n");

  // Clean and spawn all valid client entities
  G_Match_Start_SpawnAll (level);
}

/*
===========
G_Match_End

===========
*/
void
G_Match_End (lvl_t * level, const char * msg)
{
  int i;
  int numSorted;
  int ping;
  gclient_t* cl;

  // log a few informations
  G_LogPrintf ("Match ended: %s\n", msg);

  // log a summary of the match
  if (BG_GametypeIsTeam (g_gametype->integer))
    {
      G_LogPrintf ("red:%i  blue:%i\n",
		   level->teamScores[TEAM_RED], level->teamScores[TEAM_BLUE]);
    }

  // and rankings
  numSorted = level->numConnectedClients;
  for (i = 0; i < numSorted; i++)
    {
      cl = &level->clients[level->sortedClients[i]];

      if (!G_Client_IsPlaying (cl))
	continue;

      ping = cl->ps.ping < 999 ? cl->ps.ping : 999;

      G_LogPrintf ("score: %i  ping: %i  client: %i %s\n",
		   cl->ps.persistant[PERS_SCORE], ping,
		   level->sortedClients[i], cl->pers.netname);
    }

  // call to end the match and prepare intermission
  // we need a few seconds after the limit where players
  // wont move and take no damage and then only fire
  // intermission, dump stats, take screenshots etc...
  level->match.end_time = game.time;
  level->match.end_flag = END_CALLED;
}

/*
===========================
G_Match_EndedToIntermission
===========================
*/
void
G_Match_EndedToIntermission (lvl_t * level)
{
  int i;
  gclient_t* cl;
  gentity_t* client;

  // the match has ended and isn't live anymore, stop recording the demo
  //level.matchStatus &= ~MS_LIVE;
  level->matchStatus = MS_NONE;

  // this will keep the clients from playing any voice sounds
  // that will get cut off when the queued intermission starts
  gi->SetConfigstring (CS_INTERMISSION, "1");

  // reset ready status too
  for (i = 0, cl = level->clients; i < level->maxclients; i++, cl++)
    {
      // not fully in game
      if (!G_Client_IsIngame (cl))
	continue;

      // we could use -1 also to send the info to everyone
      gi->SendServerCommand (cl - level->clients, va ("matchstatus %i", MSE_END));

      // reset all events eventTime for the active entity
      g_entities[i].eventTime = 0;
    }

  // if in tournement mode, change the wins / losses
  if (g_gametype->integer == GT_TOURNAMENT)
    {
      AdjustTournamentScores (level);
    }

  G_Intermission_FindPoint (level->intermission_origin, level->intermission_angle);

  // move all clients to the intermission point
  for (i = 0; i < level->maxclients; i++)
    {
      client = g_entities + i;

      if (!G_Client_ValidEntity (client))
	continue;

      // respawn if dead
      if (client->health <= 0)
	{
	  //respawn (client);
	  CopyToBodyQue (client);
	  ClientSpawn (client);
	}

      G_Intermission_MoveClient (level, client);
    }

  // send the current scoring to all clients
  SendScoreboardMessageToAllClients (level);
}

void
G_Match_Frame (game_locals_t * game, lvl_t * level)
{
  // see if it is time to do a tournement restart
  G_Match_CheckTournament ();

  // update the arena round status if needed
  G_Match_RoundFrame (level);
}

/*
=============
ScoreIsTied
=============
*/
static bool
ScoreIsTied (void)
{
  int a, b;

  if (level.numPlayingClients < 2)
    {
      return false;
    }

  if (BG_GametypeIsTeam (g_gametype->integer))
    {
      return level.teamScores[TEAM_RED] == level.teamScores[TEAM_BLUE];
    }

  a = level.clients[level.sortedClients[0]].ps.persistant[PERS_SCORE];
  b = level.clients[level.sortedClients[1]].ps.persistant[PERS_SCORE];

  return a == b;
}

#define MAX_OVERTIME 5 * 60

/*
============================
CheckLiveTimelimitExitRules
============================
*/
static void
CheckLiveTimelimitExitRules (void)
{
  // do we play a timelimit game ?
  if (!g_timelimit->integer)
    return;

  if (level.time >= ((g_timelimit->integer * 60000) + level.numberovertime))
    {
      if (ScoreIsTied ())
	{
	  switch (g_overtimetype->integer)
	    {
	      // suddendeath
	    case 1:
	      {
		// this is reset when game module is reloaded
		if (!level.overtimeDisplayed)
		  {
		    // display overtime once for everyone
		    gi->SendServerCommand (-1, "print \"SuddenDeath"
					   " overtime.\n\"");
		    gi->SendServerCommand (-1, "cp \"" S_COLOR_RED
					   "Sudden Death\n\"");
		    level.overtimeDisplayed = true;
		  }
		break;
	      }
	      // add some extra time
	    case 2:
	      {
		if ((g_overtime->integer < 0)
		    || (g_overtime->integer > MAX_OVERTIME))
		  {
		    g_overtime->integer = 0;
		  }

		level.numberovertime += g_overtime->integer * 1000;
		gi->SendServerCommand (-1, va ("cp \"" S_COLOR_YELLOW
					       "%i seconds added\n\"",
					       g_overtime->integer));
		break;
	      }
	      // just stop tied
	    default:
	    case 0:
	      {
		gi->SendServerCommand (-1,
				       "print \"Timelimit hit."
				       " Game is tied.\n\"");
		G_Match_End (&level, "Timelimit hit. Game is tied.");
		break;
	      }
	    }
	}
      else
	{
	  if (level.numberovertime)
	    {
	      gi->SendServerCommand (-1, va ("print \"Timelimit hit after"
					     " %i sec overtime.\n\"",
					     level.numberovertime / 1000));
	      G_Match_End (&level, va ("Timelimit hit with %i sec overtime",
			   level.numberovertime / 1000));
	    }
	  else if (level.overtimeDisplayed)
	    {
	      gi->SendServerCommand (-1, "print \""
				     "Sudden Death ended !\n\"");
	      G_Match_End (&level, "Timelimit hit after SuddenDeath overtime.");
	    }
	  else
	    {
	      gi->SendServerCommand (-1, "print \"Timelimit hit.\n\"");
	      G_Match_End (&level, "Timelimit hit.");
	    }
	}
    }
}

/*
============================
CheckLiveFraglimitExitRules
============================
*/
static void
CheckLiveFraglimitExitRules (void)
{
  int i;
  gclient_t *cl;

  // we have a deathmatch (FFA, Duel, TDM) fraglimit match
  // fraglimit in CTF is non sense
  if (/*g_gametype->integer == GT_CTF) || */!g_fraglimit->integer)
    return;

  // TODO : add some checks if red and blue register a frag
  // on the very same frame.
  // the current code would give red team winning.
  // assume we are in TDM
  if (level.teamScores[TEAM_RED] >= g_fraglimit->integer)
    {
      gi->SendServerCommand (-1, "print \"Red hit the fraglimit.\n\"");
      G_Match_End (&level, "Fraglimit hit.");
      return;
    }

  if (level.teamScores[TEAM_BLUE] >= g_fraglimit->integer)
    {
      gi->SendServerCommand (-1, "print \"Blue hit the fraglimit.\n\"");
      G_Match_End (&level, "Fraglimit hit.");
      return;
    }

  // assume we play FFA or Duel
  for (i = 0; i < g_maxclients->integer; i++)
    {
      cl = level.clients + i;

      if (cl->pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      if (cl->sess.sessionTeam != TEAM_FREE)
	{
	  continue;
	}

      if (cl->ps.persistant[PERS_SCORE] >= g_fraglimit->integer)
	{
	  gi->SendServerCommand (-1, va ("print \"%s" S_COLOR_WHITE
					 " hit the fraglimit.\n\"",
					 cl->pers.netname));
	  G_Match_End (&level, "Fraglimit hit.");
	  return;
	}
    }
}

/*
===============================
CheckLiveCapturelimitExitRules
===============================
*/
static void
CheckLiveCapturelimitExitRules (void)
{
  // do we play capturelimit and ctf
  if ((g_gametype->integer != GT_CTF) || !g_capturelimit->integer)
    return;

  // TODO : same as for TDM the code don't handle simultaneous capture
  if (level.teamScores[TEAM_RED] >= g_capturelimit->integer)
    {
      gi->SendServerCommand (-1, "print \"Red hit the capturelimit.\n\"");
      G_Match_End (&level, "Capturelimit hit.");
      return;
    }

  if (level.teamScores[TEAM_BLUE] >= g_capturelimit->integer)
    {
      gi->SendServerCommand (-1, "print \"Blue hit the capturelimit.\n\"");
      G_Match_End (&level, "Capturelimit hit.");
      return;
    }
}

/*
===============================
CheckLiveCapturelimitExitRules
===============================
*/
static void
CheckLiveRoundlimitExitRules (void)
{
  // do we play capturelimit and ctf
  if ((g_gametype->integer != GT_ARENA) || !g_roundlimit->integer)
    return;

  // TODO : same as for TDM the code don't handle simultaneous capture
  if (level.teamScores[TEAM_RED] >= g_roundlimit->integer)
    {
      gi->SendServerCommand (-1, "print \"Red hit the roundlimit.\n\"");
      G_Match_End (&level, "Roundlimit hit.");
      return;
    }

  if (level.teamScores[TEAM_BLUE] >= g_roundlimit->integer)
    {
      gi->SendServerCommand (-1, "print \"Blue hit the roundlimit.\n\"");
      G_Match_End (&level, "Roundlimit hit.");
      return;
    }
}

/*
========================
CheckLiveMatchExitRules
========================
*/
static void
CheckLiveMatchExitRules (void)
{
  // we are live
  if (level.matchStatus & MS_LIVE)
    {
      // live but alone, someone left game ?
      // FIXME :the last condition sux
      //	but we need to avoid the first server frames after a
      //	map_restart because it will have numplayeingCl < 2
      if (level.numPlayingClients < 2)
	{
	  // NOTE : the last condition should be removed now (not needed anymore)
	  if ((g_gametype->integer == GT_TOURNAMENT)
	      && (level.time > 3000))
	    {
	      G_Match_End (&level, "A player left the game.");
	      return;
	    }

	  return;
	}

      // Timelimit (all gametypes)
      CheckLiveTimelimitExitRules ();

      // Fraglimit (FFA, Duel, TDM)
      CheckLiveFraglimitExitRules ();

      // Capturelimit (CTF) this can be combined with timelimit
      CheckLiveCapturelimitExitRules ();

      // Arena roundlimit hit (can be combined with timelimit or fraglimit)
      CheckLiveRoundlimitExitRules ();
    }
}

/*
=================
CheckExitRules

There will be a delay between the time the exit is qualified for
and the time everyone is moved to the intermission spot, so you
can see the last frag.
=================
*/
void
CheckExitRules (void)
{
  if (level.match.end_flag == END_PROCEED)
    {
      G_Intermission_CheckExit (&game, &level);
      return;
    }

  // if at the intermission, wait for all non-bots to
  // signal ready, then go to next level
  // move from end_intermission -> end_proceed
  if (level.match.end_flag == END_INTERMISSION)
    {
      // let the client take screenshots and handle match end before
      // spamming all the stats
      if ((game.time - level.match.end_time) >= INTERMISSION_STATS_TIME)
	{
	  // and dump stats in the console
	  SendConsoleStatsToAllClients (&level);
	  // make a quick summary of the scores (tournaments, tdm, ctf, ffa etc...)
	  // this is published to every one even spectators
	  SendConsoleMatchSummaryToAllClients (&level);

	  level.match.end_flag = END_PROCEED;
	  level.match.end_time = game.time;
	  return;
	}
    }

  // move from end_called -> end_intermission
  if (level.match.end_flag == END_CALLED)
    {
      if ((game.time - level.match.end_time) >= INTERMISSION_DELAY_TIME)
	{
	  level.match.end_flag = END_INTERMISSION;
	  level.match.end_time = game.time;

	  //G_BeginIntermission ();
	  G_Match_EndedToIntermission (&level);
	}
      return;
    }

  CheckLiveMatchExitRules ();
}

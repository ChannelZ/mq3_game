/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

/// what you get for capture
#define CTF_CAPTURE_BONUS		5
/// what your team gets for capture
#define CTF_TEAM_BONUS			1
/// what you get for recovery
#define CTF_RECOVERY_BONUS		1
/// what you get for picking up enemy flag
#define CTF_FLAG_BONUS			1
/// what you get for fragging enemy flag carrier
#define CTF_FRAG_CARRIER_BONUS		2
/// (milli)seconds until auto return
#define CTF_FLAG_RETURN_TIME		40000

/// bonus for fraggin someone while either you or your target are near your flag carrier
#define CTF_CARRIER_PROTECT_BONUS		1
/// bonus for fraggin someone while either you or your target are near your flag
#define CTF_FLAG_DEFENSE_BONUS			1
/// awarded for returning a flag that causes a capture to happen almost immediately
#define CTF_RETURN_FLAG_ASSIST_BONUS		1
/// award for fragging a flag carrier if a capture happens almost immediately
#define CTF_FRAG_CARRIER_ASSIST_BONUS		2

/// the radius around an object being defended where a target will be worth extra frags
#define CTF_TARGET_PROTECT_RADIUS		1000
/// the radius around an object being defended where an attacker will get extra frags when making kills
#define CTF_ATTACKER_PROTECT_RADIUS		1000

#define CTF_FRAG_CARRIER_ASSIST_TIMEOUT		10000
#define CTF_RETURN_FLAG_ASSIST_TIMEOUT		10000


// Prototypes
bool OnSameTeam (gentity_t * ent1, gentity_t * ent2);

void Team_DroppedFlagThink (teamgame_t * teamgame, gentity_t * ent);
void Team_FragBonuses (gentity_t * targ, gentity_t * inflictor, gentity_t * attacker);
void Team_InitGame (teamgame_t * teamgame);
void Team_ReturnFlag (teamgame_t * teamgame, team_t team);
void Team_FreeEntity (teamgame_t * teamgame, gentity_t * ent);
void Team_CheckDroppedItem (teamgame_t * teamgame, gentity_t * dropped);

gentity_t *SelectCTFSpawnPoint (team_t team, int teamstate,
				vec3_t origin, vec3_t angles, bool isBot);

// used by g_cmds.c
bool Team_GetLocationMsg (gentity_t *ent, char *loc, int loclen);

// called every frame by g_runframe, in g_main.c
void CheckTeamStatus (void);

// used in g_item when touching an IT_TEAM object
int Pickup_Team (teamgame_t * teamgame, gentity_t *ent, gentity_t *other);

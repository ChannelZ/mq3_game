/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

// This file will contain all the #defines related to weapons
// reload time, weapon color, ammos etc...

// THESE VALUES ARE NOT NEEDED TO BE SHARED
// so they could be put in another file (like g_weapon.h and remain server side only)
// but for convenience atm we won't do this (but this requiere a lot of recompiling
// if there are changes but the idea was to have all infos about weapons in a file

// weapon reloadtime
#define GAUNTLET_RELOADTIME	400	// 400
#define LIGHTNING_RELOADTIME	20	// 50
#define SHOTGUN_RELOADTIME	800	// 1000
#define MACHINEGUN_RELOADTIME	80	// 100
#define GRENADE_LAUNCHER_RELOADTIME 700 // 800
#define ROCKET_LAUNCHER_RELOADTIME 800	// 800
#define PLASMA_RELOADTIME	100	// 100
#define RAILGUN_RELOADTIME	1500	// 1500
#define BFG_RELOADTIME		200	// 200

// weapon change time
#define WEAPONCHANGE_TIMEBEFORE	200	// 200
#define WEAPONCHANGE_TIMEAFTER	250	// 250

// weapon damage
#define GAUNTLET_DAMAGE		50	// 50
#define MACHINEGUN_DAMAGE	4	// 7
#define MACHINEGUN_TEAM_DAMAGE	4	// wimpier MG in teamplay

#define SHOTGUN_DAMAGE		4	// 10 // other values are in bg_public.h
#define SHOTGUN_DAMAGE_PRO	5

#define GRENADE_DAMAGE		100	// 100
#define GRENADE_SPLASH_DAMAGE	100	// 100
#define GRENADE_SPLASH_RADIUS	150	// 150

#define ROCKET_DAMAGE		100	// 100
#define ROCKET_SPLASH_DAMAGE	100	// 100
#define ROCKET_SPLASH_RADIUS	120	// 120

#define PLASMA_DAMAGE		20	// 20
#define PLASMA_SPLASH_DAMAGE	15	// 15
#define PLASMA_SPLASH_RADIUS	20	// 20

#define BFG_DAMAGE		100	// 100
#define BFG_SPLASH_DAMAGE	100	// 100
#define BFG_SPLASH_RADIUS	120	// 120

#define LIGHTNING_DAMAGE	3	// 8
#define LIGHTNING_DAMAGE_PRO	5	// 10

#define RAILGUN_DAMAGE		80	// 100
#define RAILGUN_DAMAGE_PRO	100	// 100

// projectile speed
#define ROCKET_SPEED		900	// 900
#define ROCKET_SPEED_PRO	900	// 900
#define GRENADE_SPEED		800	// 700
#define PLASMA_SPEED		2000	// 2000

// weapon spread
#define MACHINEGUN_SPREAD	200	// 200

// weapon knockback factor (used in g_combat.c; G_Damage())
#define GAUNTLET_KNOCKBACK	1.0f
#define GAUNTLET_KNOCKBACK_PRO	0.5f
#define MACHINEGUN_KNOCKBACK	1.0f
#define SHOTGUN_KNOCKBACK	1.8f
#define GRENADE_KNOCKBACK	1.0f
#define GRENADE_SPLASH_KNOCKBACK 1.0f
#define ROCKET_KNOCKBACK	1.0f
#define ROCKET_SPLASH_KNOCKBACK 1.0f
#define ROCKET_KNOCKBACK_PRO	1.2f
#define ROCKET_SPLASH_KNOCKBACK_PRO 1.2f
#define LIGHTNING_KNOCKBACK	1.7f
#define LIGHTNING_KNOCKBACK_PRO	1.7f
#define RAILGUN_KNOCKBACK	1.0f
#define PLASMA_KNOCKBACK	1.0f
#define BFG_KNOCKBACK		1.0f


// weapon cripple factor (time the player friction is "debuff" after a hit)
#define GAUNTLET_CRIPPLE	1.0f
#define MACHINEGUN_CRIPPLE	0.4f
#define SHOTGUN_CRIPPLE		1.0f
#define GRENADE_CRIPPLE		1.0f
#define GRENADE_SPLASH_CRIPPLE	1.0f
#define ROCKET_CRIPPLE		1.0f
#define ROCKET_SPLASH_CRIPPLE	1.0f
#define LIGHTNING_CRIPPLE	0.5f
#define RAILGUN_CRIPPLE		1.0f
#define PLASMA_CRIPPLE		0.8f
#define BFG_CRIPPLE		1.0f

#define MACHINEGUN_CRIPPLE_PRO	0.5f
#define LIGHTNING_CRIPPLE_PRO	0.8f
#define RAILGUN_CRIPPLE_PRO	1.0f
#define PLASMA_CRIPPLE_PRO	0.8f


// Armor system
#define CPM_LAPROTECTION 0.5f
#define CPM_YAPROTECTION 0.60f
#define CPM_RAPROTECTION 0.75f
#define CPM_RABREAKPOINT 120.0f	// this is the point where a player with RA can pickup an YA
				// calculated through YAPROT / RAPROT = 0.60 / 0.75 = 0.8
				// then MAXYA * 0.8 = 150 * 0.8 = 120
#define CPM_YABREAKPOINT 80.0f	// point where a player with YA can pickup GA
#define CPM_RAMULTIPLIER 1.25f	// If a player has little RA and picks up an YA, multiply his
				// RA value with this. (0.75 / 0.60 = 1.25)
#define CPM_YAMULTIPLIER 1.2f	// same from YA to LA

#define CPM_RAPICK 150
#define CPM_YAPICK 100
#define CPM_LAPICK 50
#define CPM_TAPICK 5

#define CPM_RAMAXARMOR 200
#define CPM_YAMAXARMOR 150
#define CPM_LAMAXARMOR 100

#define CPM_LAARMORTYPE 0
#define CPM_YAARMORTYPE 1
#define CPM_RAARMORTYPE 2


// SHARED PART
// needed by both game and cgame or bg_*

#define LIGHTNING_RANGE		768

#define SHOTGUN_SPREAD		500 // 700
#define SHOTGUN_COUNT		18 // 11


// needed by bg_misc to make item tables

#define MACHINEGUN_PICKING_AMMO		50
#define SHOTGUN_PICKING_AMMO		5
#define GRENADE_LAUNCHER_PICKING_AMMO	5
#define ROCKET_LAUNCHER_PICKING_AMMO	10
#define LIGHTNING_PICKING_AMMO		200
#define RAILGUN_PICKING_AMMO		5
#define PLASMAGUN_PICKING_AMMO		75
#define BFG_PICKING_AMMO		20

#define MACHINEGUN_AMMO_BOX		50
#define SHOTGUN_AMMO_BOX		10
#define GRENADE_LAUNCHER_AMMO_BOX	5
#define ROCKET_LAUNCHER_AMMO_BOX	10
#define LIGHTNING_AMMO_BOX		150
#define RAILGUN_AMMO_BOX		5
#define PLASMAGUN_AMMO_BOX		75
#define BFG_AMMO_BOX			20

#define MACHINEGUN_MAX_AMMO		100
#define SHOTGUN_MAX_AMMO		15
#define GRENADE_LAUNCHER_MAX_AMMO	10
#define ROCKET_LAUNCHER_MAX_AMMO	20
#define LIGHTNING_MAX_AMMO		500
#define RAILGUN_MAX_AMMO		10
#define PLASMAGUN_MAX_AMMO		100
#define BFG_MAX_AMMO			40

#define ARMOR_HIGH		100
#define ARMOR_MEDIUM		50
#define ARMOR_LOW		25
#define ARMOR_SHARDS		5

// name convention (heavy, medium, light, tiny)
#define ITEM_HA_COLOR		'K'
#define ITEM_MA_COLOR		'z'
#define ITEM_LA_COLOR		'p'
#define ITEM_TA_COLOR		'c'

// name convention (mega, medium, light, tiny)
#define ITEM_MGH_COLOR		'K'
#define ITEM_MDH_COLOR		'z'
#define ITEM_LH_COLOR		'p'
#define ITEM_TH_COLOR		'c'

#define GAUNTLET_NAME		"Gauntlet"
#define GAUNTLET_SHORT		"PUM"
#define MACHINEGUN_NAME		"Machinegun"
#define MACHINEGUN_SHORT	"MG"
#define SHOTGUN_NAME		"Shotgun"
#define SHOTGUN_SHORT		"SSG"
#define GRENADE_NAME		"Grenade Launcher"
#define GRENADE_SHORT		"GL"
#define ROCKET_NAME		"Rocket Launcher"
#define ROCKET_SHORT		"RL"
#define LIGHTNING_NAME		"Lightning Gun"
#define LIGHTNING_SHORT		"LG"
#define RAILGUN_NAME		"Railgun"
#define RAILGUN_SHORT		"RG"
#define PLASMA_NAME		"Plasma Gun"
#define PLASMA_SHORT		"PG"
#define BFG_NAME		"BFG10K"
#define BFG_SHORT		"BFG"

#define GAUNTLET_COLOR		'5'
#define MACHINEGUN_COLOR	'3'
//#define SHOTGUN_COLOR		'g'
#define SHOTGUN_COLOR		'h'
#define GRENADE_COLOR		'v'
#define ROCKET_COLOR		'1'
#define LIGHTNING_COLOR		'V'
#define RAILGUN_COLOR		'2'
#define PLASMA_COLOR		'T'
#define BFG_COLOR		'O'

// string edition
#define S_GAUNTLET_COLOR	"^5"
#define S_MACHINEGUN_COLOR	"^3"
#define S_SHOTGUN_COLOR		"^h"
#define S_GRENADE_COLOR		"^v"
#define S_ROCKET_COLOR		"^1"
#define S_LIGHTNING_COLOR	"^V"
#define S_RAILGUN_COLOR		"^2"
#define S_PLASMA_COLOR		"^T"
#define S_BFG_COLOR		"^O"

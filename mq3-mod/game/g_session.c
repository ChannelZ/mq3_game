/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_local.h"

/*
=======================================================================

  SESSION DATA

Session data is the only data that stays persistant across level loads
and tournament restarts.
=======================================================================
*/

/*
================
G_WriteClientSessionData

Called on game shutdown
================
*/
void
G_WriteClientSessionData (gclient_t *client)
{
  const char * s;
  const char * var;

  s = va("%i %i %i %i %i %i %i",
	 client->sess.sessionTeam,
	 client->sess.spectatorTime,
	 client->sess.spectatorState,
	 client->sess.spectatorClient,
	 client->sess.wins,
	 client->sess.losses,
	 client->sess.teamLeader
	 );

  var = va("session%i", (int)(client - level.clients));

  gi->Cvar_Set (var, s);
}

/*
================
G_ReadSessionData

Called on a reconnect
================
*/
void
G_ReadSessionData (gclient_t *client)
{
  char s[MAX_STRING_CHARS];
  const char * var;
  int teamLeader;
  int spectatorState;
  int sessionTeam;

  var = va("session%i", (int)(client - level.clients));
  gi->Cvar_VariableStringBuffer (var, s, sizeof(s));

  sscanf (s, "%i %i %i %i %i %i %i",
	  &sessionTeam,
	  &client->sess.spectatorTime,
	  &spectatorState,
	  &client->sess.spectatorClient,
	  &client->sess.wins,
	  &client->sess.losses,
	  &teamLeader
	  );

  client->sess.spectatorState = (spectatorState_t)spectatorState;
  client->sess.teamLeader = (bool)teamLeader;

  // move to spectator
  client->sess.sessionTeam = (team_t)sessionTeam;
}

/*
================
G_InitSessionData

Called on a first-time connect
================
*/
void
G_InitSessionData (gclient_t * client, char * userinfo)
{
  clientSession_t *sess;
  const char *value;

  sess = &client->sess;

  // initial team determination
  if (BG_GametypeIsTeam (g_gametype->integer))
    {
      if (g_teamAutoJoin->integer)
	{
	  sess->sessionTeam = PickTeam (-1);
	  BroadcastTeamChange (client, -1);
	}
      else
	{
	  // always spawn as spectator in team games
	  sess->sessionTeam = TEAM_SPECTATOR;
	}
    }
  else
    {
      value = Info_ValueForKey (userinfo, "team");
      if (value[0] == 's')
	{
	  // a willing spectator, not a waiting-in-line
	  sess->sessionTeam = TEAM_SPECTATOR;
	}
      else
	{
	  switch (g_gametype->integer)
	    {
	    default:
	    case GT_FFA:
	      {
		// speconly don't join
		if (((g_maxGameClients->integer > 0)
		     && (level.numNonSpectatorClients >= g_maxGameClients->integer))
		    || (sess->spectatorTime == -1))
		  {
		    sess->sessionTeam = TEAM_SPECTATOR;
		  }
		else
		  {
		    sess->sessionTeam = TEAM_FREE;
		  }
		break;
	      }
	    case GT_TOURNAMENT:
	      {
		// if the game is full, go into a waiting mode
		// speconly don't join
		if (level.numNonSpectatorClients >= 2
		    || (sess->spectatorTime == -1))
		  {
		    sess->sessionTeam = TEAM_SPECTATOR;
		  }
		else
		  {
		    sess->sessionTeam = TEAM_FREE;
		  }
	      }
	      break;
	    }
	}
    }

  sess->spectatorState = SPECTATOR_FREE;
  if (sess->spectatorTime != -1)
    sess->spectatorTime = game.time;

  G_WriteClientSessionData (client);
}

/*
==================
G_InitWorldSession

==================
*/
void
G_InitWorldSession (void)
{
  char s[MAX_STRING_CHARS];
  int gt;

  gi->Cvar_VariableStringBuffer ("session", s, sizeof(s));
  gt = atoi (s);

  // if the gametype changed since the last session, don't use any
  // client sessions
  if (g_gametype->integer != gt)
    {
      level.newSession = true;
      G_Printf ("Gametype changed.\n");

      // also clear the configstrings
      gi->SetConfigstring (CS_TEAM1_NAME, "");
      gi->SetConfigstring (CS_TEAM2_NAME, "");
    }
}

/*
==================
G_WriteSessionData

==================
*/
void
G_WriteSessionData (void)
{
  int i;

  gi->Cvar_Set ("session", va("%i", g_gametype->integer));

  for (i = 0; i < level.maxclients; i++)
    {
      if (level.clients[i].pers.connected == CON_CONNECTED)
	{
	  G_WriteClientSessionData (&level.clients[i]);
	}
    }
}

/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "g_match.h"

/*
========================
MoveClientToIntermission

When the intermission starts, this will be called for all players.
If a new client connects, this will be called after the spawn function.
========================
*/
void
G_Intermission_MoveClient (lvl_t * level, gentity_t * ent)
//G_MoveClientToIntermission (lvl_t * level, gentity_t * ent)
{
  // take out of follow mode if needed
  if (ent->client->sess.spectatorState == SPECTATOR_FOLLOW)
    {
      StopFollowing (ent);
    }

  // move to the spot
  VectorCopy (level->intermission_origin, ent->sh.s.origin);
  VectorCopy (level->intermission_origin, ent->client->ps.origin);
  VectorCopy (level->intermission_angle, ent->client->ps.viewangles);
  ent->client->ps.pm_type = PM_INTERMISSION;

  // clean up powerup info
  Com_Memset (ent->client->ps.powerups, 0, sizeof (ent->client->ps.powerups));

  ent->client->ps.eFlags = 0;
  ent->sh.s.eFlags = 0;
  ent->sh.s.eType = ET_GENERAL;
  ent->sh.s.modelindex = 0;
  ent->sh.s.loopSound = 0;
  ent->sh.s.event = 0;
  ent->sh.r.contents = 0;
}

/*
==================
FindIntermissionPoint

This is also used for spectator spawns
==================
*/
gentity_t *
G_Intermission_FindPoint (vec3_t origin, vec3_t angle)
//G_FindIntermissionPoint (vec3_t origin, vec3_t angle)
{
  gentity_t *ent, *target;
  vec3_t dir;

  // find the intermission spot
  ent = G_Find (NULL, FOFS (classname), "info_player_intermission");
  if (!ent)
    {
      // the map creator forgot to put in an intermission point...
      ent = SelectSpawnPoint (vec3_origin, origin, angle, false);
    }
  else
    {
      VectorCopy (ent->sh.s.origin, origin);
      VectorCopy (ent->sh.s.angles, angle);
      // if it has a target, look towards it
      if (ent->target)
	{
	  target = G_PickTarget (ent->target);
	  if (target)
	    {
	      VectorSubtract (target->sh.s.origin, origin, dir);
	      vectoangles (dir, angle);
	    }
	}
    }
  return ent;
}

/*
=============
G_Intermission_ExitLevel

When the intermission has been exited, the server is either killed
or moved to a new level based on the "nextmap" cvar
=============
*/
// from g_match_tournament.c
void RemoveTournamentLoser (void);

void
G_Intermission_ExitLevel (lvl_t * level)
{
  int i;
  gclient_t* cl;
  char nextmap[MAX_STRING_CHARS];
  char d1[MAX_STRING_CHARS];

  // bot interbreeding
  if (gi->Cvar_VariableIntegerValue ("bot_enable"))
    {
      BotInterbreedEndMatch ();
    }

  // if we are running a tournement map, kick the loser to spectator status,
  // which will automatically grab the next spectator and restart
  if (g_gametype->integer == GT_TOURNAMENT)
    {
      if (!level->restarted)
	{
	  RemoveTournamentLoser ();
	  gi->SendConsoleCommand (EXEC_APPEND, "map_restart 0\n");
	  level->restarted = true;
	  level->changemap = NULL;
	  level->match.end_flag = END_NONE;
	  level->match.end_time = 0;
	}
      return;
    }

  gi->Cvar_VariableStringBuffer ("nextmap", nextmap, sizeof (nextmap));
  gi->Cvar_VariableStringBuffer ("d1", d1, sizeof (d1));

  if (!Q_stricmp (nextmap, "map_restart 0") && Q_stricmp (d1, ""))
    {
      gi->Cvar_Set ("nextmap", "vstr d2");
      gi->SendConsoleCommand (EXEC_APPEND, "vstr d1\n");
    }
  else
    {
      gi->SendConsoleCommand (EXEC_APPEND, "vstr nextmap\n");
    }

  level->changemap = NULL;
  level->match.end_flag = END_NONE;
  level->match.end_time = 0;

  // reset all the scores so we don't enter the intermission again
  level->teamScores[TEAM_RED] = 0;
  level->teamScores[TEAM_BLUE] = 0;
  for (i = 0; i < g_maxclients->integer; i++)
    {
      cl = level->clients + i;
      if (cl->pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      cl->ps.persistant[PERS_SCORE] = 0;
    }

  // we need to do this here before chaning to CON_CONNECTING
  G_WriteSessionData ();

  // change all client states to connecting, so the early players into the
  // next level will know the others aren't done reconnecting
  for (i = 0; i < g_maxclients->integer; i++)
    {
      if (level->clients[i].pers.connected == CON_CONNECTED)
	{
	  level->clients[i].pers.connected = CON_CONNECTING;
	}
    }
}

/*
=================
CheckIntermissionExit

The level will stay at the intermission for a minimum of 5 seconds
If all players wish to continue, the level will then exit.
If one or more players have not acknowledged the continue, the game will
wait 10 seconds before going on.
=================
*/
void
G_Intermission_CheckExit (game_locals_t * game, lvl_t * level)
{
  int ready, notReady, playerCount;
  int i;
  gclient_t *cl;
  int readyMask;

  // see which players are ready
  ready = 0;
  notReady = 0;
  readyMask = 0;
  playerCount = 0;
  for (i = 0; i < g_maxclients->integer; i++)
    {
      cl = level->clients + i;
      if (cl->pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      if (g_entities[cl->ps.clientNum].sh.r.svFlags & SVF_BOT)
	{
	  continue;
	}

      playerCount++;
      if (cl->misc.readyToExit)
	{
	  ready++;
	  if (i < 16)
	    {
	      readyMask |= 1 << i;
	    }
	}
      else
	{
	  notReady++;
	}
    }

  // copy the readyMask to each player's stats so
  // it can be displayed on the scoreboard
  for (i = 0; i < g_maxclients->integer; i++)
    {
      cl = level->clients + i;
      if (cl->pers.connected != CON_CONNECTED)
	{
	  continue;
	}
      cl->ps.stats[STAT_CLIENTS_READY] = readyMask;
    }

  // never exit in less than five seconds
  if (game->time < (level->match.end_time + 5000))
    {
      return;
    }

  // only test ready status when there are real players present
  if (playerCount > 0)
    {
      // if nobody wants to go, clear timer
      if (!ready)
	{
	  level->readyToExit = false;
	  return;
	}

      // if everyone wants to go, go now
      if (!notReady)
	{
	  G_Intermission_ExitLevel (level);
	  return;
	}
    }

  // the first person to ready starts the ten second timeout
  if (!level->readyToExit)
    {
      level->readyToExit = true;
      level->exitTime = game->time;
    }

  // if we have waited ten seconds since at least one player
  // wanted to exit, go ahead
  if (game->time < (level->exitTime + 10000))
    {
      return;
    }

  G_Intermission_ExitLevel (level);
}

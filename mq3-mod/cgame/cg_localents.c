/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
//

// cg_localents.c -- every frame, generate renderer commands for locally
// processed entities, like smoke puffs, gibs, shells, etc.

#include "cg_local.h"

#define MAX_LOCAL_ENTITIES 2048 // 512
localEntity_t cg_localEntities[MAX_LOCAL_ENTITIES];
localEntity_t cg_activeLocalEntities;	// double linked list
localEntity_t *cg_freeLocalEntities;	// single linked list

/*
===================
CG_InitLocalEntities

This is called at startup and for tournement restarts
===================
*/
void
CG_InitLocalEntities (void)
{
  int i;

  Com_Memset (cg_localEntities, 0, sizeof (cg_localEntities));
  cg_activeLocalEntities.next = &cg_activeLocalEntities;
  cg_activeLocalEntities.prev = &cg_activeLocalEntities;
  cg_freeLocalEntities = cg_localEntities;
  for (i = 0; i < MAX_LOCAL_ENTITIES - 1; i++)
    {
      cg_localEntities[i].next = &cg_localEntities[i + 1];
    }
}

/*
==================
CG_FreeLocalEntity
==================
*/
void
CG_FreeLocalEntity (localEntity_t * le)
{
  if (!le->prev)
    {
      CG_Error ("CG_FreeLocalEntity: not active");
    }

  // remove from the doubly linked active list
  le->prev->next = le->next;
  le->next->prev = le->prev;

  // the free list is only singly linked
  le->next = cg_freeLocalEntities;
  // le->prev = NULL;
  cg_freeLocalEntities = le;
}

/*
==================
CG_FreeAllLocalEntity

TODO : do it the direct way ?
==================
*/
void
CG_FreeAllLocalEntities (void)
{
  localEntity_t *le, *next;

  // walk the list backwards, so any new local entities generated
  // (trails, marks, etc) will be present this frame
  le = cg_activeLocalEntities.prev;
  for (; le != &cg_activeLocalEntities; le = next)
    {
      // grab next now, so if the local entity is freed we
      // still have it
      next = le->prev;

      CG_FreeLocalEntity (le);
    }
}

/*
===================
CG_AllocLocalEntity

Will allways succeed, even if it requires freeing an old active entity
===================
*/
localEntity_t *
CG_AllocLocalEntity (void)
{
  localEntity_t *le;

  if (!cg_freeLocalEntities)
    {
      // no free entities, so free the one at the end of the chain
      // remove the oldest active entity
      CG_FreeLocalEntity (cg_activeLocalEntities.prev);
    }

  le = cg_freeLocalEntities;
  cg_freeLocalEntities = cg_freeLocalEntities->next;

  Com_Memset (le, 0, sizeof (*le));

  // link into the active list
  le->next = cg_activeLocalEntities.next;
  le->prev = &cg_activeLocalEntities;
  cg_activeLocalEntities.next->prev = le;
  cg_activeLocalEntities.next = le;
  return le;
}

/*
====================================================================================

FRAGMENT PROCESSING

A fragment localentity interacts with the environment in some way (hitting walls),
or generates more localentities along a trail.

====================================================================================
*/

/*
================
CG_BloodTrail

Leave expanding blood puffs behind gibs
================
*/
#define MAX_PUFFS 10
void
CG_BloodTrail (localEntity_t * le)
{
  int t;
  int t2;
  int step;
  vec3_t newOrigin;
  localEntity_t *blood;

  step = 50;
  t = step * ((cg.levelTime - cg.levelFrameTime + step) / step);
  t2 = step * (cg.levelTime / step);

  for (; t <= t2; t += step)
    {
      BG_EvaluateTrajectory (&le->pos, t, newOrigin);

      blood = CG_SmokePuff (newOrigin, vec3_origin, 30,	// radius
			    1, 1, 1, 1,	// color
			    2000,	// trailTime
			    t,	// startTime
			    0,	// fadeInTime
			    0,	// flags
			    cgs.media.bloodTrailShader);
      // use the optimized version
      blood->leType = LE_FALL_SCALE_FADE;
      // drop a total of 40 units over its lifetime
      blood->pos.trDelta[2] = 40;
    }
}

/*
================
CG_FragmentBounceMark
================
*/
void
CG_FragmentBounceMark (localEntity_t * le, trace_t * trace)
{
  int radius;

  if (le->leMarkType == LEMT_BLOOD)
    {
      radius = 16 + (rand () & 31);
      CG_ImpactMark (cgs.media.bloodMarkShader, trace->endpos,
		     trace->plane.normal, random () * 360, 1, 1, 1, 1, true,
		     radius, false);
    }
  else if (le->leMarkType == LEMT_BURN)
    {
      radius = 8 + (rand () & 15);
      CG_ImpactMark (cgs.media.burnMarkShader, trace->endpos,
		     trace->plane.normal, random () * 360, 1, 1, 1, 1, true,
		     radius, false);
    }

  // don't allow a fragment to make multiple marks, or they
  // pile up while settling
  le->leMarkType = LEMT_NONE;
}

/*
================
CG_FragmentBounceSound
================
*/
void
CG_FragmentBounceSound (localEntity_t * le, trace_t * trace)
{
  if (le->leBounceSoundType == LEBS_BLOOD)
    {
      // half the gibs will make splat sounds
      if (rand () & 1)
	{
	  int r = rand () & 3;
	  sfxHandle_t s;

	  if (r == 0)
	    {
	      s = cgs.media.gibBounce1Sound;
	    }
	  else if (r == 1)
	    {
	      s = cgs.media.gibBounce2Sound;
	    }
	  else
	    {
	      s = cgs.media.gibBounce3Sound;
	    }
	  cgl->snd->StartSound (trace->endpos, ENTITYNUM_WORLD, CHAN_AUTO, s);
	}
    }

  // don't allow a fragment to make multiple bounce sounds,
  // or it gets too noisy as they settle
  le->leBounceSoundType = LEBS_NONE;
}

/*
================
CG_ReflectVelocity
================
*/
void
CG_ReflectVelocity (localEntity_t * le, trace_t * trace)
{
  vec3_t velocity;
  float dot;
  int hitTime;

  // reflect the velocity on the trace plane
  hitTime =
    cg.levelTime - cg.levelFrameTime + cg.levelFrameTime * trace->fraction;
  BG_EvaluateTrajectoryDelta (&le->pos, hitTime, velocity);
  dot = DotProduct (velocity, trace->plane.normal);
  VectorMA (velocity, -2 * dot, trace->plane.normal, le->pos.trDelta);

  VectorScale (le->pos.trDelta, le->bounceFactor, le->pos.trDelta);

  VectorCopy (trace->endpos, le->pos.trBase);
  le->pos.trTime = cg.levelTime;

  // check for stop, making sure that even on low FPS systems it doesn't bobble
  if (trace->allsolid
      || ((trace->plane.normal[2] > 0) && (le->pos.trDelta[2] < 40)))
    {
      le->pos.trType = TR_STATIONARY;
    }
}

/*
================
CG_AddFragment
================
*/
void
CG_AddFragment (localEntity_t * le)
{
  vec3_t newOrigin;
  trace_t trace;

  if (le->pos.trType == TR_STATIONARY)
    {
      // sink into the ground if near the removal time
      int t;
      float oldZ;

      t = le->endTime - cg.levelTime;
      if (t < SINK_TIME)
	{
	  // we must use an explicit lighting origin, otherwise the
	  // lighting would be lost as soon as the origin went
	  // into the ground
	  VectorCopy (le->refEntity.origin, le->refEntity.lightingOrigin);
	  le->refEntity.renderfx |= RF_LIGHTING_ORIGIN;
	  oldZ = le->refEntity.origin[2];
	  le->refEntity.origin[2] -= 16 * (1.0 - (float) t / SINK_TIME);
	  cgl->R_AddRefEntityToScene (&le->refEntity);
	  le->refEntity.origin[2] = oldZ;
	}
      else
	{
	  cgl->R_AddRefEntityToScene (&le->refEntity);
	}

      return;
    }

  // calculate new position
  BG_EvaluateTrajectory (&le->pos, cg.levelTime, newOrigin);

  // trace a line from previous position to new position
  CG_Trace (&trace, le->refEntity.origin, NULL, NULL, newOrigin, -1, CONTENTS_SOLID);
  if (trace.fraction == 1.0)
    {
      // still in free fall
      VectorCopy (newOrigin, le->refEntity.origin);

      if (le->leFlags & LEF_TUMBLE)
	{
	  vec3_t angles;

	  BG_EvaluateTrajectory (&le->angles, cg.levelTime, angles);
	  AnglesToAxis (angles, le->refEntity.axis);
	}

      cgl->R_AddRefEntityToScene (&le->refEntity);

      // add a blood trail
      if (le->leBounceSoundType == LEBS_BLOOD)
	{
	  CG_BloodTrail (le);
	}

      return;
    }

  // if it is in a nodrop zone, remove it
  // this keeps gibs from waiting at the bottom of pits of death
  // and floating levels
  if (cgl->CM_PointContents (trace.endpos, 0) & CONTENTS_NODROP)
    {
      CG_FreeLocalEntity (le);
      return;
    }

  // leave a mark
  CG_FragmentBounceMark (le, &trace);

  // do a bouncy sound
  CG_FragmentBounceSound (le, &trace);

  // reflect the velocity on the trace plane
  CG_ReflectVelocity (le, &trace);

  cgl->R_AddRefEntityToScene (&le->refEntity);
}

/*
================
CG_AddSurfacePoly
================
*/
static void
CG_AddSurfacePoly (localEntity_t * le)
{
  polyVert_t verts[4];
  float c;
  int col[3];
  float len = le->radius;

  c = (le->endTime - cg.levelTime) / (float)(le->endTime - le->startTime);
  if (c > 1)
    {
      c = 1.0f;		// can happen during connection problems
    }

  col[0] = lrintf (le->color[0] * 255.0f);
  col[1] = lrintf (le->color[1] * 255.0f);
  col[2] = lrintf (le->color[2] * 255.0f);
  col[3] = lrintf (c * 255.0f);

  VectorCopy (le->pos.trBase, verts[0].xyz);
  verts[0].st[0] = len;
  verts[0].st[1] = 0;
  verts[0].modulate[0] = col[0];
  verts[0].modulate[1] = col[1];
  verts[0].modulate[2] = col[2];
  verts[0].modulate[3] = col[3];

  VectorCopy (le->pos.trDelta, verts[1].xyz);
  verts[1].st[0] = len;
  verts[1].st[1] = 1;
  verts[1].modulate[0] = col[0];
  verts[1].modulate[1] = col[1];
  verts[1].modulate[2] = col[2];
  verts[1].modulate[3] = col[3];

  VectorCopy (le->angles.trDelta, verts[2].xyz);
  verts[2].st[0] = 1;
  verts[2].st[1] = 1;
  verts[2].modulate[0] = col[0];
  verts[2].modulate[1] = col[1];
  verts[2].modulate[2] = col[2];
  verts[2].modulate[3] = col[3];

  VectorCopy (le->angles.trBase, verts[3].xyz);
  verts[3].st[0] = 0;
  verts[3].st[1] = 0;
  verts[3].modulate[0] = col[0];
  verts[3].modulate[1] = col[1];
  verts[3].modulate[2] = col[2];
  verts[3].modulate[3] = col[3];

  cgl->R_AddPolysToScene (le->refEntity.customShader, 4, verts, 1);
}

/*
=====================================================================

TRIVIAL LOCAL ENTITIES

These only do simple scaling or modulation before passing to the renderer
=====================================================================
*/

/*
====================
CG_AddFadeRGB
====================
*/
void
CG_AddFadeRGB (localEntity_t * le)
{
  refEntity_t *re;
  float c;

  re = &le->refEntity;

  c = (le->endTime - cg.levelTime) * le->lifeRate;
  c *= 0xff;

  re->shaderRGBA[0] = le->color[0] * c;
  re->shaderRGBA[1] = le->color[1] * c;
  re->shaderRGBA[2] = le->color[2] * c;
  re->shaderRGBA[3] = le->color[3] * c;

  cgl->R_AddRefEntityToScene (re);
}

/*
==================
CG_AddMoveScaleFade
==================
*/
static void
CG_AddMoveScaleFade (localEntity_t * le)
{
  refEntity_t *re;
  float c;
  vec3_t delta;
  float len;

  re = &le->refEntity;

  if ((le->fadeInTime > le->startTime) && (cg.levelTime < le->fadeInTime))
    {
      // fade / grow time
      c = 1.0 - ((float)(le->fadeInTime - cg.levelTime)
		 / (float)(le->fadeInTime - le->startTime));
    }
  else
    {
      // fade / grow time
      c = (le->endTime - cg.levelTime) * le->lifeRate;
    }

  re->shaderRGBA[3] = 0xff * c * le->color[3];

  if (!(le->leFlags & LEF_PUFF_DONT_SCALE))
    {
      re->radius = le->radius * (1.0 - c) + 8;
    }

  BG_EvaluateTrajectory (&le->pos, cg.levelTime, re->origin);

  // if the view would be "inside" the sprite, kill the sprite
  // so it doesn't add too much overdraw
  VectorSubtract (re->origin, cg.refdef.vieworg, delta);
  len = VectorLength (delta);
  if (len < le->radius)
    {
      CG_FreeLocalEntity (le);
      return;
    }

  cgl->R_AddRefEntityToScene (re);
}

/*
===================
CG_AddScaleFade

For rocket smokes that hang in place, fade out, and are
removed if the view passes through them.
There are often many of these, so it needs to be simple.
===================
*/
static void
CG_AddScaleFade (localEntity_t * le)
{
  refEntity_t *re;
  float c;
  vec3_t delta;
  float len;

  re = &le->refEntity;

  // fade / grow time
  c = (le->endTime - cg.levelTime) * le->lifeRate;

  re->shaderRGBA[3] = 0xff * c * le->color[3];
  re->radius = le->radius * (1.0 - c) + 8;

  // if the view would be "inside" the sprite, kill the sprite
  // so it doesn't add too much overdraw
  VectorSubtract (re->origin, cg.refdef.vieworg, delta);
  len = VectorLength (delta);
  if (len < le->radius)
    {
      CG_FreeLocalEntity (le);
      return;
    }

  cgl->R_AddRefEntityToScene (re);
}

/*
=================
CG_AddFallScaleFade

This is just an optimized CG_AddMoveScaleFade
For blood mists that drift down, fade out, and are
removed if the view passes through them.
There are often 100+ of these, so it needs to be simple.
=================
*/
static void
CG_AddFallScaleFade (localEntity_t * le)
{
  refEntity_t *re;
  float c;
  vec3_t delta;
  float len;

  re = &le->refEntity;

  // fade time
  c = (le->endTime - cg.levelTime) * le->lifeRate;

  re->shaderRGBA[3] = 0xff * c * le->color[3];

  re->origin[2] = le->pos.trBase[2] - (1.0 - c) * le->pos.trDelta[2];

  re->radius = le->radius * (1.0 - c) + 16;

  // if the view would be "inside" the sprite, kill the sprite
  // so it doesn't add too much overdraw
  VectorSubtract (re->origin, cg.refdef.vieworg, delta);
  len = VectorLength (delta);
  if (len < le->radius)
    {
      CG_FreeLocalEntity (le);
      return;
    }

  cgl->R_AddRefEntityToScene (re);
}

/*
================
CG_AddExplosion
================
*/
static void
CG_AddExplosion (localEntity_t * ex)
{
  refEntity_t *ent;

  ent = &ex->refEntity;

  // add the entity
  cgl->R_AddRefEntityToScene (ent);

  // add the dlight
  if (ex->light)
    {
      float light;

      light = ((float)(cg.levelTime - ex->startTime)
	       / (float)(ex->endTime - ex->startTime));
      if (light < 0.5)
	{
	  light = 1.0;
	}
      else
	{
	  light = 1.0 - (light - 0.5) * 2;
	}
      light = ex->light * light;
      cgl->R_AddLightToScene (ent->origin, light, ex->lightColor[0],
			      ex->lightColor[1], ex->lightColor[2]);
    }
}

/*
================
CG_AddSpriteExplosion
================
*/
static void
CG_AddSpriteExplosion (localEntity_t * le)
{
  refEntity_t re;
  float c;

  re = le->refEntity;

  c = (le->endTime - cg.levelTime) / (float)(le->endTime - le->startTime);
  if (c > 1)
    {
      c = 1.0;		// can happen during connection problems
    }

  re.shaderRGBA[0] = 0xff;
  re.shaderRGBA[1] = 0xff;
  re.shaderRGBA[2] = 0xff;
  re.shaderRGBA[3] = 0xff * c * 0.33;

  re.reType = RT_SPRITE;
  re.radius = 42 * (1.0 - c) + 30;

  cgl->R_AddRefEntityToScene (&re);

  // add the dlight
  if (le->light)
    {
      float light;

      light = ((float)(cg.levelTime - le->startTime)
	       / (float)(le->endTime - le->startTime));
      if (light < 0.5)
	{
	  light = 1.0;
	}
      else
	{
	  light = 1.0 - (light - 0.5) * 2;
	}
      light = le->light * light;
      cgl->R_AddLightToScene (re.origin, light, le->lightColor[0],
			      le->lightColor[1], le->lightColor[2]);
    }
}

/*
====================
CG_RenderDigitNumber
====================
*/
void
CG_RenderDigitNumber (refEntity_t *re, vec3_t origin, vec3_t face,
		      int score, int size)
{
  int digits[10], numdigits, negative;
  int i;

  negative = false;
  if (score < 0)
    {
      negative = true;
      score = -score;
    }

  for (numdigits = 0; !(numdigits && !score); numdigits++)
    {
      digits[numdigits] = score % 10;
      score = score / 10;
    }

  if (negative)
    {
      digits[numdigits] = 10; // the minus character
      numdigits++;
    }

  for (i = 0; i < numdigits; i++)
    {

      VectorMA (origin, (float) (((float) numdigits / 2) - i) * size,
		face, re->origin);

      //VectorCopy (origin, re->origin);
      re->customShader = cgs.media.numberShaders[digits[numdigits - 1 - i]];
      cgl->R_AddRefEntityToScene (re);
    }
}


static void
CG_PlumColorFromValue (int score, byte *color, float fade)
{
  if (score < 0)
    {
      // negative score, drawn in red
      color[0] = 0xff;
      color[1] = 0x11;
      color[2] = 0x11;
    }
  else
    {
      // positive, white if it is "1"
      color[0] = 0xff;
      color[1] = 0xff;
      color[2] = 0xff;
      // violet
      if (score >= 50)
	{
	  color[1] = 0;
	}
      // blue
      else if (score >= 20)
	{
	  color[0] = color[1] = 0;
	}
      // yellow
      else if (score >= 10)
	{
	  color[2] = 0;
	}
      // green
      else if (score >= 2)
	{
	  color[0] = color[2] = 0;
	}
    }

  if (fade < 0.25)
    color[3] = 0xff * 4 * fade;
  else
    color[3] = 0xff;
}

/*
===================
CG_AddScorePlum
===================
*/
#define NUMBER_SIZE 8

void
CG_AddScorePlum (localEntity_t * le)
{
  refEntity_t *re;
  vec3_t origin, delta, dir, vec, up = {0, 0, 1};
  float c, len;
  int score;

  re = &le->refEntity;

  c = (le->endTime - cg.levelTime) * le->lifeRate;

  score = le->radius;

  re->radius = NUMBER_SIZE / 2;

  VectorCopy (le->pos.trBase, origin);
  origin[2] += 110 - c * 100;

  // build face vector
  VectorSubtract (cg.refdef.vieworg, origin, dir);
  CrossProduct (dir, up, vec);
  VectorNormalize (vec);

  VectorMA (origin, -10 + 20 * sin (c * 2 * M_PI), vec, origin);

  // if the view would be "inside" the sprite, kill the sprite
  // so it doesn't add too much overdraw
  VectorSubtract (origin, cg.refdef.vieworg, delta);
  len = VectorLength (delta);
  if (len < 20) // should be re->radius
    {
      CG_FreeLocalEntity (le);
      return;
    }

  CG_PlumColorFromValue (score, re->shaderRGBA, c);
  CG_RenderDigitNumber (re, origin, vec, score, NUMBER_SIZE);
}

//==============================================================================

/*
===================
CG_AddLocalEntities

===================
*/
void
CG_AddLocalEntities (void)
{
  localEntity_t *le, *next;

  // walk the list backwards, so any new local entities generated
  // (trails, marks, etc) will be present this frame
  le = cg_activeLocalEntities.prev;
  for (; le != &cg_activeLocalEntities; le = next)
    {
      // grab next now, so if the local entity is freed we
      // still have it
      next = le->prev;

      if (cg.levelTime >= le->endTime)
	{
	  CG_FreeLocalEntity (le);
	  continue;
	}

      switch (le->leType)
	{
	default:
	  CG_Error ("Bad leType: %i", le->leType);
	  break;

	case LE_MARK:
	  break;

	case LE_SPRITE_EXPLOSION:
	  CG_AddSpriteExplosion (le);
	  break;

	case LE_EXPLOSION:
	  CG_AddExplosion (le);
	  break;

	case LE_FRAGMENT:	// gibs and brass
	  CG_AddFragment (le);
	  break;

	case LE_SURFACE_POLY:
	  CG_AddSurfacePoly (le);
	  break;

	case LE_MOVE_SCALE_FADE:	// water bubbles
	  CG_AddMoveScaleFade (le);
	  break;

	case LE_FADE_RGB:	// teleporters, railtrails
	  CG_AddFadeRGB (le);
	  break;

	case LE_FALL_SCALE_FADE:	// gib blood trails
	  CG_AddFallScaleFade (le);
	  break;

	case LE_SCALE_FADE:	// rocket trails
	  CG_AddScaleFade (le);
	  break;

	case LE_SCOREPLUM:
	  CG_AddScorePlum (le);
	  break;
	}
    }
}

/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
//
// cg_consolecmds.c -- text commands typed in at the local console, or
// executed by a key binding

#include "cg_local.h"

/*
=================
CG_SizeUp_f

Keybinding command
=================
*/
static void
CG_SizeUp_f (void)
{
  cgl->Cvar_Set ("cg_viewsize", va ("%i", (int) (cg_viewsize->integer + 10)));
}

/*
=================
CG_SizeDown_f

Keybinding command
=================
*/
static void
CG_SizeDown_f (void)
{
  cgl->Cvar_Set ("cg_viewsize", va ("%i", (int) (cg_viewsize->integer - 10)));
}

/*
=============
CG_Viewpos_f

Debugging command to print the current position
=============
*/
static void
CG_Viewpos_f (void)
{
  CG_Printf ("(%i %i %i) : %i\n", (int) cg.refdef.vieworg[0],
	     (int) cg.refdef.vieworg[1], (int) cg.refdef.vieworg[2],
	     (int) cg.refdefViewAngles[YAW]);
}

/*
===============
CG_ScoresDown_f
===============
*/
static void
CG_ScoresDown_f (void)
{
  if (cg.scoresRequestTime + 2000 < cg.time)
    {
      // the scores are more than two seconds out of data,
      // so request new ones
      cg.scoresRequestTime = cg.time;
      cgl->SendClientCommand ("score");

      // leave the current scores up if they were already
      // displayed, but if this is the first hit, clear them out
      if (!cg.showScores)
	{
	  cg.showScores = true;
	  cg.numScores = 0;
	}
    }
  else
    {
      // show the cached contents even if they just pressed if it
      // is within two seconds
      cg.showScores = true;
    }
}

/*
===============
CG_ScoresUp_f
===============
*/
static void
CG_ScoresUp_f (void)
{
  if (cg.showScores)
    {
      cg.showScores = false;
      cg.scoreFadeTime = cg.time;
    }
}

/*
===============
CG_WStatsDown_f
===============
*/
static void
CG_WStatsDown_f (void)
{
  if ((cg.BoxRequestTime + 2000) < cg.time)
    {
      // the scores are more than two seconds out of data,
      // so request new ones
      cg.BoxRequestTime = cg.time;
      cgl->SendClientCommand (va ("boxstats %i", cg.snap->ps.clientNum));

      cg.BoxShow = true;
      // request a fresh set of stats
      cg.BoxPrintReady = false;
      // clear output datas
      cg.BoxPrintString[0] = '\0';
      cg.BoxPrintString2[0] = '\0';
      cg.BoxX = cg.BoxY = 0;
    }
  else
    {
      // show the cached contents even if they just pressed if it
      // is within two seconds
      cg.BoxShow = true;
    }
}

/*
===============
CG_WStatsUp_f
===============
*/
static void
CG_WStatsUp_f (void)
{
  if (cg.BoxShow)
    {
      cg.BoxShow = false;
      cg.BoxFadeTime = cg.time;
    }
}

typedef struct
{
  char *cmd;
  void (*function) (void);
} consoleCommand_t;

typedef struct
{
  char *cmd;
} completeConsoleCommand_t;

static consoleCommand_t commands[] = {
  {"testgun", CG_TestGun_f},
  {"testmodel", CG_TestModel_f},
  {"nextframe", CG_TestModelNextFrame_f},
  {"prevframe", CG_TestModelPrevFrame_f},
  {"nextskin", CG_TestModelNextSkin_f},
  {"prevskin", CG_TestModelPrevSkin_f},
  {"viewpos", CG_Viewpos_f},
  {"+scores", CG_ScoresDown_f},
  {"-scores", CG_ScoresUp_f},
  {"+wstats", CG_WStatsDown_f},
  {"-wstats", CG_WStatsUp_f},
  {"+zoom", CG_ZoomDown_f},
  {"-zoom", CG_ZoomUp_f},
  {"sizeup", CG_SizeUp_f},
  {"sizedown", CG_SizeDown_f},
  {"weapnext", CG_NextWeapon_f},
  {"weapprev", CG_PrevWeapon_f},
  {"weapon", CG_Weapon_f},
  {"weaponlist", CG_WeaponList_f},
  {"weaponcir", CG_WeaponCircular_f},
  /*
  {"tell_target", CG_TellTarget_f},
  {"tell_attacker", CG_TellAttacker_f},
  {"vtell_target", CG_VoiceTellTarget_f},
  {"vtell_attacker", CG_VoiceTellAttacker_f},
  {"tcmd", CG_TargetCommand_f},
  */
  {"loaddeferred", CG_LoadDeferredPlayers}
};

// keep this list in sync with the one in g_cmds.c
static completeConsoleCommand_t remotecommands[] = {
  {"kill"},
  {"say"},
  {"say_team"},
  {"tell"},
  {"give"},
  {"god"},
  {"notarget"},
  {"noclip"},
  {"team"},
  {"follow"},
  {"follownext"},
  {"followprev"},
  {"levelshot"},
  {"setviewpos"},
  {"callvote"},
  {"vote"},
  {"ready"},
  {"drop"},
  {"players"},
  {"list_maps"}
};

/*
=================
CG_ConsoleCommand

The string has been tokenized and can be retrieved with
Cmd_Argc() / Cmd_Argv()
=================
*/
bool
CG_ConsoleCommand (void)
{
  const char *cmd;
  unsigned int i;

  cmd = cgl->Argv (0);

  for (i = 0; i < ARRAY_LEN (commands); i++)
    {
      if (!Q_stricmp (cmd, commands[i].cmd))
	{
	  commands[i].function ();
	  return true;
	}
    }

  return false;
}

/*
=================
CG_InitConsoleCommands

Let the client system know about all of our commands
so it can perform tab completion
=================
*/
void
CG_InitConsoleCommands (void)
{
  unsigned int i;

  for (i = 0; i < ARRAY_LEN (commands); i++)
    {
      cgl->AddCommand (commands[i].cmd);
    }

  // the game server will interpret these commands, which will be automatically
  // forwarded to the server after they are not recognized locally
  for (i = 0; i < ARRAY_LEN (remotecommands); i++)
    {
      cgl->AddCommand (remotecommands[i].cmd);
    }
}

/*
=================
CG_RemoveConsoleCommands

remove added commands when closing the cgame module
=================
*/
void
CG_RemoveConsoleCommands (void)
{
  unsigned int i;

  for (i = 0; i < ARRAY_LEN (commands); i++)
    {
      cgl->RemoveCommand (commands[i].cmd);
    }

  for (i = 0; i < ARRAY_LEN (remotecommands); i++)
    {
      cgl->RemoveCommand (remotecommands[i].cmd);
    }
}

/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
//
// cg_drawtools.c -- helper functions called by cg_draw, cg_scoreboard, cg_info, etc
#include "cg_local.h"

/*
================
CG_AdjustFrom640

Adjusted for resolution and screen aspect ratio
================
*/
void
CG_AdjustFrom640 (float *x, float *y, float *w, float *h)
{
  // scale for screen sizes; taking a possible scale and offset
  *x *= cgs.screenXScale * cg.hudscale;
  *x += cg.hudoffsetx;
  *y *= cgs.screenYScale;
  *w *= cgs.screenXScale * cg.hudscale;
  *h *= cgs.screenYScale;
}

/*
================
CG_FillRect

Coordinates are 640*480 virtual values
=================
*/
void
CG_FillRect (float x, float y, float width, float height, const float *color)
{
  cgl->R_SetColor (color);

  CG_AdjustFrom640 (&x, &y, &width, &height);
  cgl->R_DrawStretchPic (x, y, width, height, 0, 0, 0, 0,
			 cgs.media.whiteShader);

  cgl->R_SetColor (NULL);
}

/*
================
CG_DrawSides

Coords are virtual 640x480
================
*/
void
CG_DrawSides (float x, float y, float w, float h, float size)
{
  CG_AdjustFrom640 (&x, &y, &w, &h);
  size *= cgs.screenXScale;
  cgl->R_DrawStretchPic (x, y, size, h, 0, 0, 0, 0, cgs.media.whiteShader);
  cgl->R_DrawStretchPic (x + w - size, y, size, h, 0, 0, 0, 0,
			 cgs.media.whiteShader);
}

void
CG_DrawTopBottom (float x, float y, float w, float h, float size)
{
  CG_AdjustFrom640 (&x, &y, &w, &h);
  size *= cgs.screenYScale;
  cgl->R_DrawStretchPic (x, y, w, size, 0, 0, 0, 0, cgs.media.whiteShader);
  cgl->R_DrawStretchPic (x, y + h - size, w, size, 0, 0, 0, 0,
			 cgs.media.whiteShader);
}

/*
================
CG_DrawRect

Coordinates are 640*480 virtual values
=================
*/
void
CG_DrawRect (float x, float y, float width, float height, float size,
	     const float *color)
{
  cgl->R_SetColor (color);

  CG_DrawTopBottom (x, y, width, height, size);
  CG_DrawSides (x, y, width, height, size);

  cgl->R_SetColor (NULL);
}

/*
================
CG_DrawPic

Coordinates are 640*480 virtual values
=================
*/
void
CG_DrawPic (float x, float y, float width, float height, qhandle_t hShader)
{
  CG_AdjustFrom640 (&x, &y, &width, &height);
  cgl->R_DrawStretchPic (x, y, width, height, 0, 0, 1, 1, hShader);
}

/*
===============
CG_DrawChar

Coordinates and size in 640*480 virtual screen size
===============
*/
void
CG_DrawChar (int x, int y, int width, int height, int ch)
{
  int row, col;
  float frow, fcol;
  float size;
  float ax, ay, aw, ah;

  ch &= 255;

  if (ch == ' ')
    {
      return;
    }

  ax = (float) x;
  ay = (float) y;
  aw = (float) width;
  ah = (float) height;
  CG_AdjustFrom640 (&ax, &ay, &aw, &ah);

  row = ch >> 4;
  col = ch & 15;

  frow = row * 0.0625;
  fcol = col * 0.0625;
  size = 0.0625;

  cgl->R_DrawStretchPic (ax, ay, aw, ah,
			 fcol, frow,
			 fcol + size, frow + size, cgs.media.charsetShader);
}

/*
==================
CG_DrawStringExt

Draws a multi-colored string with a drop shadow, optionally forcing
to a fixed color.

Coordinates are at 640 by 480 virtual resolution
==================
*/
void
CG_DrawStringExt (int x, int y, const char *string, const float *setColor,
		  bool forceColor, bool shadow,
		  int charWidth, int charHeight, int maxChars)
{
  vec4_t color;
  const char *s;
  int xx;
  int cnt;

  if (maxChars <= 0)
    maxChars = 32767;		// do them all!

  // draw the drop shadow
  if (shadow)
    {
      int shadowoffset;

      // reduce shadow drop for small fonts
      if ((charWidth <= 12) || (charHeight <= 12))
	{
	  shadowoffset = 1;
	}
      else
	{
	  shadowoffset = 2;
	}

      color[0] = color[1] = color[2] = 0;
      color[3] = setColor[3];
      cgl->R_SetColor (color);
      s = string;
      xx = x;
      cnt = 0;
      while (*s && (cnt < maxChars))
	{
	  if (Q_IsColorString (s))
	    {
	      s += 2;
	      continue;
	    }
	  CG_DrawChar (xx + shadowoffset, y + shadowoffset,
		       charWidth, charHeight, *s);
	  cnt++;
	  xx += charWidth;
	  s++;
	}
    }

  // draw the colored text
  s = string;
  xx = x;
  cnt = 0;
  cgl->R_SetColor (setColor);
  while (*s && (cnt < maxChars))
    {
      if (Q_IsColorString (s))
	{
	  if (!forceColor)
	    {
	      Com_Memcpy (color, g_color_table[ColorIndex2 (*(s + 1))],
			  sizeof (color));
	      color[3] = setColor[3];
	      cgl->R_SetColor (color);
	    }
	  s += 2;
	  continue;
	}
      CG_DrawChar (xx, y, charWidth, charHeight, *s);
      xx += charWidth;
      cnt++;
      s++;
    }
  cgl->R_SetColor (NULL);
}

void
CG_DrawBigString (int x, int y, const char *s, float alpha)
{
  float color[4];

  color[0] = color[1] = color[2] = 1.0f;
  color[3] = alpha;
  CG_DrawStringExt (x, y, s, color, false, true, BIGCHAR_WIDTH,
		    BIGCHAR_HEIGHT, 0);
}

void
CG_DrawBigStringColor (int x, int y, const char *s, vec4_t color)
{
  CG_DrawStringExt (x, y, s, color, true, true, BIGCHAR_WIDTH,
		    BIGCHAR_HEIGHT, 0);
}

void
CG_DrawString (int x, int y, int w, int h, const char *s, float alpha)
{
  float color[4];

  color[0] = color[1] = color[2] = 1.0;
  color[3] = alpha;

  CG_DrawStringExt (x, y, s, color, false, true, w, h, 0);
}

void
CG_DrawStringColor (int x, int y, int w, int h, const char *s, vec4_t color)
{
  CG_DrawStringExt (x, y, s, color, true, true, w, h, 0);
}

void
CG_DrawSmallString (int x, int y, const char *s, float alpha)
{
  float color[4];

  color[0] = color[1] = color[2] = 1.0f;
  color[3] = alpha;
  CG_DrawStringExt (x, y, s, color, false, false, SMALLCHAR_WIDTH,
		    SMALLCHAR_HEIGHT, 0);
}

void
CG_DrawSmallStringColor (int x, int y, const char *s, vec4_t color)
{
  CG_DrawStringExt (x, y, s, color, true, false, SMALLCHAR_WIDTH,
		    SMALLCHAR_HEIGHT, 0);
}

/*
=================
CG_DrawStrlen

Returns character count, skiping color escape codes
=================
*/
int
CG_DrawStrlen (const char *str)
{
  return Q_PrintStrlen (str);
}

/*
===============
CG_DrawChar2

Coordinates and size in the gameworld ref
===============
*/
void
CG_DrawChar2 (vec3_t origin, int charWidth, int charHeight, vec4_t color, int ch)
{
  int row, col;
  float frow, fcol;
  float size;
  polyVert_t verts[4];
  vec3_t corners[4];
  vec3_t stepdown, stepright;
  int i;

  ch &= 255;

  if (ch == ' ')
    {
      return;
    }

  // calculate the position of the char on the charset
  row = ch / 16;
  col = ch % 16;

  size = 0.0625; // 1/16
  frow = row * size;
  fcol = col * size;

  /*
  // set the polygon's texture coordinates
  // upper left
  verts[0].st[0] = fcol;
  verts[0].st[1] = frow;
  // lower left
  verts[1].st[0] = fcol;
  verts[1].st[1] = frow + size;
  // lower right
  verts[2].st[0] = fcol + size;
  verts[2].st[1] = frow + size;
  // upper right
  verts[3].st[0] = fcol + size;
  verts[3].st[1] = frow;
*/

  // NOTE : i just can't get it working the direct way...
  // using origin as the top left point and making the associated corners.

  // upper left
  verts[3].st[0] = fcol;
  verts[3].st[1] = frow;
  // lower left
  verts[2].st[0] = fcol;
  verts[2].st[1] = frow + size;
  // lower right
  verts[1].st[0] = fcol + size;
  verts[1].st[1] = frow + size;
  // upper right
  verts[0].st[0] = fcol + size;
  verts[0].st[1] = frow;

  // set the color
  for (i = 0; i < 4; i++)
    {
      verts[i].modulate[0] = (byte)(color[0] * 255);
      verts[i].modulate[1] = (byte)(color[1] * 255);
      verts[i].modulate[2] = (byte)(color[2] * 255);
      verts[i].modulate[3] = (byte)(color[3] * 255);
    }

  for (i = 0; i < 4; i++)
    {
      VectorCopy (origin, corners[i]);
    }

  VectorScale (cg.refdef.viewaxis[1], charWidth, stepright);
  VectorScale (cg.refdef.viewaxis[2], -charHeight, stepdown);

  // corners[0] is the reference at upper right

  // lower left
  VectorAdd (corners[1], stepdown, corners[1]);

  // lower right
  VectorAdd (corners[2], stepdown, corners[2]);
  VectorAdd (corners[2], stepright, corners[2]);

  // upper right
  VectorAdd (corners[3], stepright, corners[3]);

  // assignement to the corners
  VectorCopy (corners[0], verts[0].xyz);
  VectorCopy (corners[1], verts[1].xyz);
  VectorCopy (corners[2], verts[2].xyz);
  VectorCopy (corners[3], verts[3].xyz);

  cgl->R_AddPolysToScene (cgs.media.charsetShader, 4, verts, 1);
}

/*
==================
CG_DrawStringExt2 (variation)

Draws a multi-colored string with a drop shadow, optionally forcing
to a fixed color.

Coordinates are in the gameworld
==================
*/
void
CG_DrawStringExt2 (vec3_t origin, const char *string, const float *setColor,
		   bool forceColor, int charWidth, int charHeight, int maxChars)
{
  vec4_t color;
  const char *s;
  int cnt;
  int len;

  vec3_t neworg;

  if (maxChars <= 0)
    maxChars = 32767;		// do them all!

  // draw the colored text
  s = string;
  cnt = 0;

  len = CG_DrawStrlen (string);

  VectorMA (origin, (len/2) * charWidth, cg.refdef.viewaxis[1], neworg);

  Com_Memcpy (color, colorWhite, sizeof (color));
  color[3] = setColor[3];

  while (*s && (cnt < maxChars))
    {
      if (Q_IsColorString (s))
	{
	  if (!forceColor)
	    {
	      Com_Memcpy (color, g_color_table[ColorIndex2 (*(s + 1))],
			  sizeof (color));
	      color[3] = setColor[3];
	    }
	  s += 2;
	  continue;
	}

      CG_DrawChar2 (neworg, charWidth, charHeight, color, *s);

      VectorMA (neworg, -charWidth, cg.refdef.viewaxis[1], neworg);
      cnt++;
      s++;
    }
}

/*
=============
CG_TileClearBox

This repeats a 64*64 tile graphic to fill the screen around a sized down
refresh window.
=============
*/
static void
CG_TileClearBox (int x, int y, int w, int h, qhandle_t hShader)
{
  float s1, t1, s2, t2;

  s1 = x / 64.0;
  t1 = y / 64.0;
  s2 = (x + w) / 64.0;
  t2 = (y + h) / 64.0;
  cgl->R_DrawStretchPic (x, y, w, h, s1, t1, s2, t2, hShader);
}

/*
==============
CG_TileClear

Clear around a sized down screen
==============
*/
void
CG_TileClear (void)
{
  int top, bottom, left, right;
  int w, h;

  w = cgs.glconfig.vidWidth;
  h = cgs.glconfig.vidHeight;

  if ((cg.refdef.x == 0) && (cg.refdef.y == 0)
      && (cg.refdef.width == w) && (cg.refdef.height == h))
    {
      return;		// full screen rendering
    }

  // reset potential color
  cgl->R_SetColor (NULL);

  top = cg.refdef.y;
  bottom = top + cg.refdef.height - 1;
  left = cg.refdef.x;
  right = left + cg.refdef.width - 1;

  // clear above view screen
  CG_TileClearBox (0, 0, w, top, cgs.media.backTileShader);

  // clear below view screen
  CG_TileClearBox (0, bottom, w, h - bottom, cgs.media.backTileShader);

  // clear left of view screen
  CG_TileClearBox (0, top, left, bottom - top + 1, cgs.media.backTileShader);

  // clear right of view screen
  CG_TileClearBox (right, top, w - right, bottom - top + 1,
		   cgs.media.backTileShader);
}

/*
================
CG_FadeColor
================
*/
float *
CG_FadeColor (int startMsec, int totalMsec, timertype_t tp)
{
  static vec4_t color;
  int t;

  if (startMsec == 0)
    {
      return NULL;
    }

  if (tp == TIMER_REALTIME)
    {
      t = cg.time - startMsec;
    }
  else //if (tp == TIMER_SIMULATION)
    {
      t = cg.levelTime - startMsec;
    }

  if (t >= totalMsec)
    {
      return NULL;
    }

  // fade out
  if ((totalMsec - t) < FADE_TIME)
    {
      color[3] = (totalMsec - t) * 1.0 / FADE_TIME;
    }
  else
    {
      color[3] = 1.0;
    }
  color[0] = color[1] = color[2] = 1;

  return color;
}

float
CG_FadeColor2 (int startMsec, int totalMsec)
{
  float acolor;
  int t;

  if (startMsec == 0)
    {
      return 0.0;
    }

  t = cg.time - startMsec;

  if (t >= totalMsec)
    {
      return 0.0;
    }

  // fade out
  if ((totalMsec - t) < FADE_TIME)
    {
      acolor = (totalMsec - t) * 1.0 / FADE_TIME;
    }
  else
    {
      acolor = 1.0;
    }

  return acolor;
}

/*
================
CG_TeamColor
================
*/
float *
CG_TeamColor (team_t team)
{
  static vec4_t red = { 1, 0.2f, 0.2f, 1 };
  static vec4_t blue = { 0.2f, 0.2f, 1, 1 };
  static vec4_t other = { 1, 1, 1, 1 };
  static vec4_t spectator = { 0.7f, 0.7f, 0.7f, 1 };

  switch (team)
    {
    case TEAM_RED:
      return red;
    case TEAM_BLUE:
      return blue;
    case TEAM_SPECTATOR:
      return spectator;
    default:
      return other;
    }
}

/*
=================
CG_GetColorForHealth
=================
*/
void
CG_GetColorForHealth (int health, int armor, vec4_t hcolor)
{
  int count;
  int max;

  // calculate the total points of damage that can
  // be sustained at the current health / armor level
  if (health <= 0)
    {
      VectorClear (hcolor);	// black
      hcolor[3] = 1;
      return;
    }
  count = armor;
  max = health * ARMOR_PROTECTION / (1.0 - ARMOR_PROTECTION);
  if (max < count)
    {
      count = max;
    }
  health += count;

  // set the color based on health
  hcolor[0] = 1.0;
  hcolor[3] = 1.0;
  if (health >= 100)
    {
      hcolor[2] = 1.0;
    }
  else if (health < 66)
    {
      hcolor[2] = 0;
    }
  else
    {
      hcolor[2] = (health - 66) / 33.0;
    }

  if (health > 60)
    {
      hcolor[1] = 1.0;
    }
  else if (health < 30)
    {
      hcolor[1] = 0;
    }
  else
    {
      hcolor[1] = (health - 30) / 30.0;
    }
}

/*
=================
CG_ColorForHealth
=================
*/
void
CG_ColorForHealth (vec4_t hcolor)
{

  CG_GetColorForHealth (cg.snap->ps.stats[STAT_HEALTH],
			cg.snap->ps.stats[STAT_ARMOR], hcolor);
}

/*
=================
CG_ColorForPing
=================
*/
void
CG_ColorForPing (vec4_t hcolor, int ping)
{
  if (ping < 50)
    {
      // green
      Com_Memcpy (hcolor, colorGreen, sizeof (vec4_t));
      return;
    }
  else if (ping < 80)
    {
      // yellow
      Com_Memcpy (hcolor, colorYellow, sizeof (vec4_t));
      return;
    }
  else
    {
      // red
      Com_Memcpy (hcolor, colorRed, sizeof (vec4_t));
      return;
    }
}

/*
=====================
CG_ColorForPingString
=====================
*/
char *
CG_ColorForPingString (int ping)
{
  if (ping < 50)
    {
      // green
      return S_COLOR_GREEN;
    }
  else if (ping < 80)
    {
      // yellow
      return S_COLOR_YELLOW;
    }
  else
    {
      // red
      return S_COLOR_RED;
    }
}

/*
===================
dev_CH_ColorFromString
===================
*/
void
dev_CH_ColorFromStringHex (vec4_t hcolor, char *string)
{
  char *ptr;
  int i;
  int res = 0;
  char value = 0;

  ptr = string;

  hcolor[0] = hcolor[1] = hcolor[2] = hcolor[3] = 1;

  for (i = 0; i < 4; i++, ptr += 2)
    {
      if (!*ptr || !*(ptr + 1))
	{
	  break;
	}
      else
	{
	  res = c16to10[(int) *ptr];
	  if (res < 0)
	    break;
	  value <<= 4;
	  value += res;
	  res = c16to10[(int) *(ptr + 1)];
	  if (res < 0)
	    break;
	  value <<= 4;
	  value += res;

	  hcolor[i] = (float)(value) / (float)255;
	}
    }
}

void
dev_CG_ColorFromStringHex2Byte (byte hcolor[4], char *string)
{
  char *ptr;
  int i;
  int res = 0;

  hcolor[0] = hcolor[1] = hcolor[2] = hcolor[3] = 255;

  ptr = string;

  for (i = 0; i < 4; i++, ptr += 2)
    {
      if (!*ptr || !*(ptr + 1))
	{
	  break;
	}
      else
	{
	  res = c16to10[(int) *ptr];
	  if (res < 0)
	    break;
	  hcolor[i] <<= 4;
	  hcolor[i] += res;
	  res = c16to10[(int) *(ptr + 1)];
	  if (res < 0)
	    break;
	  hcolor[i] <<= 4;
	  hcolor[i] += res;
	  hcolor[i] >>= 8;
	}
    }
}

void
dev_CG_FillModColorFromString (const char *string, byte modelcolor[3][4])
{
  vec4_t fcol = { 1.0f, 1.0f, 1.0f, 1.0f };
  const char *val;
  int i, j;

  if (!string || !*string)
    {
      for (i = 0; i < 3; i++)
	{
	  for (j = 0; j < 4; j++)
	    {
	      modelcolor[i][j] = (byte) (fcol[j] * 255);
	    }
	}
    }
  else
    {
      val = string;

      Com_Memcpy (&fcol, g_color_table[(int) ColorIndex2 (*val)], sizeof (fcol));
      for (i = 0; i < 4; i++)
	{
	  modelcolor[0][i] = (byte) (fcol[i] * 255);
	}
      val++;
      if (*val == ':')
	val++;

      Com_Memcpy (&fcol, g_color_table[(int) ColorIndex2 (*val)], sizeof (fcol));
      for (i = 0; i < 4; i++)
	{
	  modelcolor[1][i] = (byte) (fcol[i] * 255);
	}
      val++;
      if (*val == ':')
	val++;

      Com_Memcpy (&fcol, g_color_table[(int) ColorIndex2 (*val)], sizeof (fcol));
      for (i = 0; i < 4; i++)
	{
	  modelcolor[2][i] = (byte) (fcol[i] * 255);
	}
    }
}

/*
====================
dev_CG_ParseXYcoordonnates
====================
*/
void
dev_CG_ParseXYcoordonnates (const char *s, int *x, int *y, int defx, int defy,
			    int minx, int miny, int maxx, int maxy)
{
  const char *p;
  int valx, valy;

  p = s;

  if (p && (*p >= '0') && (*p <= '9'))
    {
      valx = atoi (p);

      if ((valx <= minx) || (valx >= maxx))
	{
	  valx = defx;
	}

      while (*p && (*p != 'x'))
	{
	  p++;
	}

      p++;
      if ((*p >= '0') && (*p <= '9'))
	{
	  valy = atoi (p);
	  if ((valy <= miny) || (valy >= maxy))
	    {
	      valy = defy;
	    }
	}
      else
	{
	  valy = defy;
	}
    }
  else
    {
      // set up initial position
      valx = defx;
      valy = defy;
    }

  *x = valx;
  *y = valy;
}

/*
====================
CG_ColorFromString
====================
*/
/*
static void CG_ColorFromString( const char *v, vec3_t color ) {
	int val;

	VectorClear( color );

	val = atoi( v );

	if ( val < 1 || val > 7 ) {
		VectorSet( color, 1, 1, 1 );
		return;
	}

	if ( val & 1 ) {
		color[2] = 1.0f;
	}
	if ( val & 2 ) {
		color[1] = 1.0f;
	}
	if ( val & 4 ) {
		color[0] = 1.0f;
	}
}
*/

/*
=================
UI_DrawProportionalString2
=================
*/
static int propMap[128][3] = {
  {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1},
  {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1},
  {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1},
  {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1},
  {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1},
  {0, 0, -1}, {0, 0, -1},

  {0, 0, PROP_SPACE_WIDTH},	// SPACE
  {11, 122, 7},			// !
  {154, 181, 14},		// "
  {55, 122, 17},		// #
  {79, 122, 18},		// $
  {101, 122, 23},		// %
  {153, 122, 18},		// &
  {9, 93, 7},			// '
  {207, 122, 8},		// (
  {230, 122, 9},		// )
  {177, 122, 18},		// *
  {30, 152, 18},		// +
  {85, 181, 7},			// ,
  {34, 93, 11},			// -
  {110, 181, 6},		// .
  {130, 152, 14},		// /

  {22, 64, 17},			// 0
  {41, 64, 12},			// 1
  {58, 64, 17},			// 2
  {78, 64, 18},			// 3
  {98, 64, 19},			// 4
  {120, 64, 18},		// 5
  {141, 64, 18},		// 6
  {204, 64, 16},		// 7
  {162, 64, 17},		// 8
  {182, 64, 18},		// 9
  {59, 181, 7},			// :
  {35, 181, 7},			// ;
  {203, 152, 14},		// <
  {56, 93, 14},			// =
  {228, 152, 14},		// >
  {177, 181, 18},		// ?

  {28, 122, 22},		// @
  {5, 4, 18},			// A
  {27, 4, 18},			// B
  {48, 4, 18},			// C
  {69, 4, 17},			// D
  {90, 4, 13},			// E
  {106, 4, 13},			// F
  {121, 4, 18},			// G
  {143, 4, 17},			// H
  {164, 4, 8},			// I
  {175, 4, 16},			// J
  {195, 4, 18},			// K
  {216, 4, 12},			// L
  {230, 4, 23},			// M
  {6, 34, 18},			// N
  {27, 34, 18},			// O

  {48, 34, 18},			// P
  {68, 34, 18},			// Q
  {90, 34, 17},			// R
  {110, 34, 18},		// S
  {130, 34, 14},		// T
  {146, 34, 18},		// U
  {166, 34, 19},		// V
  {185, 34, 29},		// W
  {215, 34, 18},		// X
  {234, 34, 18},		// Y
  {5, 64, 14},			// Z
  {60, 152, 7},			// [
  {106, 151, 13},		// '\'
  {83, 152, 7},			// ]
  {128, 122, 17},		// ^
  {4, 152, 21},			// _

  {134, 181, 5},		// '
  {5, 4, 18},			// A
  {27, 4, 18},			// B
  {48, 4, 18},			// C
  {69, 4, 17},			// D
  {90, 4, 13},			// E
  {106, 4, 13},			// F
  {121, 4, 18},			// G
  {143, 4, 17},			// H
  {164, 4, 8},			// I
  {175, 4, 16},			// J
  {195, 4, 18},			// K
  {216, 4, 12},			// L
  {230, 4, 23},			// M
  {6, 34, 18},			// N
  {27, 34, 18},			// O

  {48, 34, 18},			// P
  {68, 34, 18},			// Q
  {90, 34, 17},			// R
  {110, 34, 18},		// S
  {130, 34, 14},		// T
  {146, 34, 18},		// U
  {166, 34, 19},		// V
  {185, 34, 29},		// W
  {215, 34, 18},		// X
  {234, 34, 18},		// Y
  {5, 64, 14},			// Z
  {153, 152, 13},		// {
  {11, 181, 5},			// |
  {180, 152, 13},		// }
  {79, 93, 17},			// ~
  {0, 0, -1}			// DEL
};

static int propMapB[26][3] = {
  {11, 12, 33},
  {49, 12, 31},
  {85, 12, 31},
  {120, 12, 30},
  {156, 12, 21},
  {183, 12, 21},
  {207, 12, 32},

  {13, 55, 30},
  {49, 55, 13},
  {66, 55, 29},
  {101, 55, 31},
  {135, 55, 21},
  {158, 55, 40},
  {204, 55, 32},

  {12, 97, 31},
  {48, 97, 31},
  {82, 97, 30},
  {118, 97, 30},
  {153, 97, 30},
  {185, 97, 25},
  {213, 97, 30},

  {11, 139, 32},
  {42, 139, 51},
  {93, 139, 32},
  {126, 139, 31},
  {158, 139, 25},
};

#define PROPB_GAP_WIDTH		4
#define PROPB_SPACE_WIDTH	12
#define PROPB_HEIGHT		36

/*
=================
UI_DrawBannerString
=================
*/
static void
UI_DrawBannerString2 (int x, int y, const char *str, vec4_t color)
{
  const char *s;
  unsigned char ch;
  float ax;
  float ay;
  float aw;
  float ah;
  float frow;
  float fcol;
  float fwidth;
  float fheight;

  // draw the colored text
  cgl->R_SetColor (color);

  ax = x * cgs.screenXScale;
  ay = y * cgs.screenYScale;

  s = str;
  while (*s)
    {
      ch = *s & 127;
      if (ch == ' ')
	{
	  ax += ((float) PROPB_SPACE_WIDTH
		 + (float) PROPB_GAP_WIDTH) * cgs.screenXScale;
	}
      else if (ch >= 'A' && ch <= 'Z')
	{
	  ch -= 'A';
	  fcol = (float) propMapB[ch][0] / 256.0f;
	  frow = (float) propMapB[ch][1] / 256.0f;
	  fwidth = (float) propMapB[ch][2] / 256.0f;
	  fheight = (float) PROPB_HEIGHT / 256.0f;
	  aw = (float) propMapB[ch][2] * cgs.screenXScale;
	  ah = (float) PROPB_HEIGHT * cgs.screenYScale;
	  cgl->R_DrawStretchPic (ax, ay, aw, ah, fcol, frow, fcol + fwidth,
				 frow + fheight, cgs.media.charsetPropB);
	  ax += (aw + (float) PROPB_GAP_WIDTH * cgs.screenXScale);
	}
      s++;
    }

  cgl->R_SetColor (NULL);
}

void
UI_DrawBannerString (int x, int y, const char *str, int style, vec4_t color)
{
  const char *s;
  int ch;
  int width;
  vec4_t drawcolor;

  // find the width of the drawn text
  s = str;
  width = 0;
  while (*s)
    {
      ch = *s;
      if (ch == ' ')
	{
	  width += PROPB_SPACE_WIDTH;
	}
      else if (ch >= 'A' && ch <= 'Z')
	{
	  width += propMapB[ch - 'A'][2] + PROPB_GAP_WIDTH;
	}
      s++;
    }
  width -= PROPB_GAP_WIDTH;

  switch (style & UI_FORMATMASK)
    {
    case UI_CENTER:
      x -= width / 2;
      break;

    case UI_RIGHT:
      x -= width;
      break;

    case UI_LEFT:
    default:
      break;
    }

  if (style & UI_DROPSHADOW)
    {
      drawcolor[0] = drawcolor[1] = drawcolor[2] = 0;
      drawcolor[3] = color[3];
      UI_DrawBannerString2 (x + 2, y + 2, str, drawcolor);
    }

  UI_DrawBannerString2 (x, y, str, color);
}


int
UI_ProportionalStringWidth (const char *str)
{
  const char *s;
  int ch;
  int charWidth;
  int width;

  s = str;
  width = 0;
  while (*s)
    {
      ch = *s & 127;
      charWidth = propMap[ch][2];
      if (charWidth != -1)
	{
	  width += charWidth;
	  width += PROP_GAP_WIDTH;
	}
      s++;
    }

  width -= PROP_GAP_WIDTH;
  return width;
}

static void
UI_DrawProportionalString2 (int x, int y, const char *str, vec4_t color,
			    float sizeScale, qhandle_t charset)
{
  const char *s;
  unsigned char ch;
  float ax;
  float ay;
  float aw;
  float ah;
  float frow;
  float fcol;
  float fwidth;
  float fheight;

  // draw the colored text
  cgl->R_SetColor (color);

  ax = x * cgs.screenXScale;
  ay = y * cgs.screenYScale;

  s = str;
  while (*s)
    {
      ch = *s & 127;
      if (ch == ' ')
	{
	  aw = (float) PROP_SPACE_WIDTH *cgs.screenXScale * sizeScale;
	}
      else if (propMap[ch][2] != -1)
	{
	  fcol = (float) propMap[ch][0] / 256.0f;
	  frow = (float) propMap[ch][1] / 256.0f;
	  fwidth = (float) propMap[ch][2] / 256.0f;
	  fheight = (float) PROP_HEIGHT / 256.0f;
	  aw = (float) propMap[ch][2] * cgs.screenXScale * sizeScale;
	  ah = (float) PROP_HEIGHT * cgs.screenYScale * sizeScale;
	  cgl->R_DrawStretchPic (ax, ay, aw, ah, fcol, frow, fcol + fwidth,
				 frow + fheight, charset);
	}
      else
	{
	  aw = 0;
	}

      ax += (aw + (float) PROP_GAP_WIDTH * cgs.screenXScale * sizeScale);
      s++;
    }

  cgl->R_SetColor (NULL);
}

/*
=================
UI_ProportionalSizeScale
=================
*/
float
UI_ProportionalSizeScale (int style)
{
  if (style & UI_SMALLFONT)
    {
      return 0.75;
    }

  return 1.00;
}

/*
=================
UI_DrawProportionalString
=================
*/
void
UI_DrawProportionalString (int x, int y, const char *str, int style,
			   vec4_t color)
{
  vec4_t drawcolor;
  int width;
  float sizeScale;

  sizeScale = UI_ProportionalSizeScale (style);

  switch (style & UI_FORMATMASK)
    {
    case UI_CENTER:
      width = UI_ProportionalStringWidth (str) * sizeScale;
      x -= width / 2;
      break;

    case UI_RIGHT:
      width = UI_ProportionalStringWidth (str) * sizeScale;
      x -= width;
      break;

    case UI_LEFT:
    default:
      break;
    }

  if (style & UI_DROPSHADOW)
    {
      drawcolor[0] = drawcolor[1] = drawcolor[2] = 0;
      drawcolor[3] = color[3];
      UI_DrawProportionalString2 (x + 2, y + 2, str, drawcolor, sizeScale,
				  cgs.media.charsetProp);
    }

  if (style & UI_INVERSE)
    {
      drawcolor[0] = color[0] * 0.8;
      drawcolor[1] = color[1] * 0.8;
      drawcolor[2] = color[2] * 0.8;
      drawcolor[3] = color[3];
      UI_DrawProportionalString2 (x, y, str, drawcolor, sizeScale,
				  cgs.media.charsetProp);
      return;
    }

  if (style & UI_PULSE)
    {
      drawcolor[0] = color[0] * 0.8;
      drawcolor[1] = color[1] * 0.8;
      drawcolor[2] = color[2] * 0.8;
      drawcolor[3] = color[3];
      UI_DrawProportionalString2 (x, y, str, color, sizeScale,
				  cgs.media.charsetProp);

      drawcolor[0] = color[0];
      drawcolor[1] = color[1];
      drawcolor[2] = color[2];
      drawcolor[3] = 0.5 + 0.5 * sin (cg.time / PULSE_DIVISOR);
      UI_DrawProportionalString2 (x, y, str, drawcolor, sizeScale,
				  cgs.media.charsetPropGlow);
      return;
    }

  UI_DrawProportionalString2 (x, y, str, color, sizeScale,
			      cgs.media.charsetProp);
}

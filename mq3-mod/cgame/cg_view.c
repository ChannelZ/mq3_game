/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
//
// cg_view.c -- setup all the parameters (position, angle, etc)
// for a 3D rendering
#include "cg_local.h"

/*
=============================================================================

  MODEL TESTING

The viewthing and gun positioning tools from Q2 have been integrated and
enhanced into a single model testing facility.

Model viewing can begin with either "testmodel <modelname>" or "testgun <modelname>".

The names must be the full pathname after the basedir, like
"models/weapons/v_launch/tris.md3" or "players/male/tris.md3"

Testmodel will create a fake entity 100 units in front of the current view
position, directly facing the viewer.  It will remain immobile, so you can
move around it to view it from different angles.

Testgun will cause the model to follow the player around and supress the real
view weapon model.  The default frame 0 of most guns is completely off screen,
so you will probably have to cycle a couple frames to see it.

"nextframe", "prevframe", "nextskin", and "prevskin" commands will change the
frame or skin of the testmodel.  These are bound to F5, F6, F7, and F8 in
q3default.cfg.

If a gun is being tested, the "gun_x", "gun_y", and "gun_z" variables will let
you adjust the positioning.

Note that none of the model testing features update while the game is paused, so
it may be convenient to test with deathmatch set to 1 so that bringing down the
console doesn't pause the game.

=============================================================================
*/

/*
=================
CG_TestModel_f

Creates an entity in front of the current position, which
can then be moved around
=================
*/
void
CG_TestModel_f (void)
{
  vec3_t angles;

  Com_Memset (&cg.testModelEntity, 0, sizeof (cg.testModelEntity));
  if (cgl->Argc () < 2)
    {
      return;
    }

  // cg.testModelName[MAX_QPATH] but to be sure use sizeof ?
  //Q_strncpyz (cg.testModelName, cgl->Argv( 1 ), MAX_QPATH );
  Q_strncpyz (cg.testModelName, cgl->Argv (1), sizeof (cg.testModelName));
  cg.testModelEntity.hModel = cgl->R_RegisterModel (cg.testModelName);

  if (!cg.testModelEntity.hModel)
    {
      CG_Printf ("Can't register model\n");
      return;
    }

  if (cgl->Argc () == 3)
    {
      cg.testModelEntity.backlerp = atof (cgl->Argv (2));
      cg.testModelEntity.frame = 1;
      cg.testModelEntity.oldframe = 0;
    }

  VectorMA (cg.refdef.vieworg, 100, cg.refdef.viewaxis[0],
	    cg.testModelEntity.origin);

  angles[PITCH] = 0;
  angles[YAW] = 180 + cg.refdefViewAngles[1];
  angles[ROLL] = 0;

  AnglesToAxis (angles, cg.testModelEntity.axis);
  cg.testGun = false;
}

/*
=================
CG_TestGun_f

Replaces the current view weapon with the given model
=================
*/
void
CG_TestGun_f (void)
{
  CG_TestModel_f ();
  cg.testGun = true;
  cg.testModelEntity.renderfx = RF_MINLIGHT | RF_DEPTHHACK | RF_FIRST_PERSON;
}

void
CG_TestModelNextFrame_f (void)
{
  cg.testModelEntity.frame++;
  CG_Printf ("frame %i\n", cg.testModelEntity.frame);
}

void
CG_TestModelPrevFrame_f (void)
{
  cg.testModelEntity.frame--;
  if (cg.testModelEntity.frame < 0)
    {
      cg.testModelEntity.frame = 0;
    }
  CG_Printf ("frame %i\n", cg.testModelEntity.frame);
}

void
CG_TestModelNextSkin_f (void)
{
  cg.testModelEntity.skinNum++;
  CG_Printf ("skin %i\n", cg.testModelEntity.skinNum);
}

void
CG_TestModelPrevSkin_f (void)
{
  cg.testModelEntity.skinNum--;
  if (cg.testModelEntity.skinNum < 0)
    {
      cg.testModelEntity.skinNum = 0;
    }
  CG_Printf ("skin %i\n", cg.testModelEntity.skinNum);
}

static void
CG_AddTestModel (void)
{
  int i;

  // re-register the model, because the level may have changed
  cg.testModelEntity.hModel = cgl->R_RegisterModel (cg.testModelName);
  if (!cg.testModelEntity.hModel)
    {
      CG_Printf ("Can't register model\n");
      return;
    }

  // if testing a gun, set the origin reletive to the view origin
  if (cg.testGun)
    {
      VectorCopy (cg.refdef.vieworg, cg.testModelEntity.origin);
      VectorCopy (cg.refdef.viewaxis[0], cg.testModelEntity.axis[0]);
      VectorCopy (cg.refdef.viewaxis[1], cg.testModelEntity.axis[1]);
      VectorCopy (cg.refdef.viewaxis[2], cg.testModelEntity.axis[2]);

      // allow the position to be adjusted
      for (i = 0; i < 3; i++)
	{
	  cg.testModelEntity.origin[i] +=
	    cg.refdef.viewaxis[0][i] * cg_gun_x->value;
	  cg.testModelEntity.origin[i] +=
	    cg.refdef.viewaxis[1][i] * cg_gun_y->value;
	  cg.testModelEntity.origin[i] +=
	    cg.refdef.viewaxis[2][i] * cg_gun_z->value;
	}
    }

  cgl->R_AddRefEntityToScene (&cg.testModelEntity);
}

//============================================================================

/*
=================
CG_CalcVrect

Sets the coordinates of the rendered window
=================
*/
static void
CG_CalcVrect (void)
{
  int size;

  // the intermission should allways be full screen
  if (cg.snap->ps.pm_type == PM_INTERMISSION)
    {
      size = 100;
    }
  else
    {
      // bound normal viewsize
      if (cg_viewsize->integer < 30)
	{
	  cgl->Cvar_SetNew (cg_viewsize, "30");
	  size = 30;
	}
      else if (cg_viewsize->integer > 100)
	{
	  cgl->Cvar_SetNew (cg_viewsize, "100");
	  size = 100;
	}
      else
	{
	  size = cg_viewsize->integer;
	}
    }
  cg.refdef.width = cgs.glconfig.vidWidth * size / 100;
  cg.refdef.width &= ~1;

  cg.refdef.height = cgs.glconfig.vidHeight * size / 100;
  cg.refdef.height &= ~1;

  cg.refdef.x = (cgs.glconfig.vidWidth - cg.refdef.width) / 2;
  cg.refdef.y = (cgs.glconfig.vidHeight - cg.refdef.height) / 2;
}

/*
=================
CG_CalcHudAspect

Compute ratio and offset to display the hud
=================
*/
static void
CG_CalcHudAspect (void)
{
  // hud scaling and offset
  float iratio, centersize;

  if (ch_centerunscaled->integer)
    {
      iratio = (float)cg.refdef.height / (float)cg.refdef.width;
      cg.hudscale = 1.333333f * iratio;
      // avoid dividing to compute the 4/3 box filled inside the current ratio
      centersize = iratio * (float)cg.refdef.width * 1.333333f;
      cg.hudoffsetx = (cg.refdef.width - centersize) / 2.0f;
    }
  else
    {
      cg.hudscale = 1.0f;
      cg.hudoffsetx = 0.0f;
    }
}

//==============================================================================

static void
CG_DamageFallOffset (void)
{
  int idelta;
  float delta;
  float f;

  idelta = cg.levelTime - cg.landTime;

  if (idelta < 0)
    {
      cg.landTime = cg.levelTime;
    }
  else if (idelta < LAND_DEFLECT_TIME)
    {
      delta = (float)idelta;

      f = delta / (float)LAND_DEFLECT_TIME;
      cg.refdef.vieworg[2] += cg.landChange * f;
    }
  else if (idelta < (LAND_DEFLECT_TIME + LAND_RETURN_TIME))
    {
      delta = (float)idelta;

      delta -= (float)LAND_DEFLECT_TIME;
      f = 1.0f - (delta / (float)LAND_RETURN_TIME);
      cg.refdef.vieworg[2] += cg.landChange * f;
    }
}

static void
CG_StepOffset (void)
{
  int timeDelta;

  // smooth out stair climbing
  timeDelta = cg.levelTime - cg.stepTime;
  if (timeDelta < 0)
    {
      cg.stepTime = cg.levelTime;
    }
  else if (timeDelta < STEP_TIME)
    {
      cg.refdef.vieworg[2] -=
	cg.stepChange * (STEP_TIME - timeDelta) / STEP_TIME;
    }
}

/*
===============
CG_OffsetThirdPersonView

===============
*/
#define FOCUS_DISTANCE 512
static void
CG_OffsetThirdPersonView (void)
{
  vec3_t forward, right, up;
  vec3_t view;
  vec3_t focusAngles;
  trace_t trace;
  static vec3_t mins = {-4, -4, -4};
  static vec3_t maxs = {4, 4, 4};
  vec3_t focusPoint;
  float focusDist;
  float forwardScale, sideScale;

  cg.refdef.vieworg[2] += cg.predictedPlayerState.viewheight;

  // add step offset
  if (cg_stepoffset->integer)
    {
      CG_StepOffset ();
    }

  VectorCopy (cg.refdefViewAngles, focusAngles);

  // if dead, look at killer
  if (cg.predictedPlayerState.stats[STAT_HEALTH] <= 0)
    {
      focusAngles[YAW] = cg.predictedPlayerState.stats[STAT_DEAD_YAW];
      cg.refdefViewAngles[YAW] = cg.predictedPlayerState.stats[STAT_DEAD_YAW];
    }

  if (focusAngles[PITCH] > 45)
    {
      focusAngles[PITCH] = 45;	// don't go too far overhead
    }
  AngleVectors (focusAngles, forward, NULL, NULL);

  VectorMA (cg.refdef.vieworg, FOCUS_DISTANCE, forward, focusPoint);

  VectorCopy (cg.refdef.vieworg, view);

  view[2] += 8;

  cg.refdefViewAngles[PITCH] *= 0.5;

  AngleVectors (cg.refdefViewAngles, forward, right, up);

  forwardScale = cos (cg_thirdPersonAngle->value / 180 * M_PI);
  sideScale = sin (cg_thirdPersonAngle->value / 180 * M_PI);
  VectorMA (view, -cg_thirdPersonRange->value * forwardScale, forward, view);
  VectorMA (view, -cg_thirdPersonRange->value * sideScale, right, view);

  // trace a ray from the origin to the viewpoint to make sure the view isn't
  // in a solid block.  Use an 8 by 8 block to prevent the view from near clipping anything

  CG_Trace (&trace, cg.refdef.vieworg, mins, maxs, view,
	    cg.predictedPlayerState.clientNum, MASK_SOLID);
  if (trace.fraction != 1.0)
    {
      VectorCopy (trace.endpos, view);
      view[2] += (1.0 - trace.fraction) * 32;
      // try another trace to this position, because a tunnel may have the ceiling
      // close enogh that this is poking out

      CG_Trace (&trace, cg.refdef.vieworg, mins, maxs, view,
		cg.predictedPlayerState.clientNum, MASK_SOLID);
      VectorCopy (trace.endpos, view);
    }

  VectorCopy (view, cg.refdef.vieworg);

  // select pitch to look at focus point from vieword
  VectorSubtract (focusPoint, cg.refdef.vieworg, focusPoint);
  focusDist = Q_sqrt (focusPoint[0] * focusPoint[0] + focusPoint[1] * focusPoint[1]);
  if (focusDist < 1)
    {
      focusDist = 1;		// should never happen
    }
  cg.refdefViewAngles[PITCH] = -180 / M_PI * atan2 (focusPoint[2], focusDist);
  cg.refdefViewAngles[YAW] -= cg_thirdPersonAngle->value;
}

/*
===============
CG_OffsetFirstPersonView

===============
*/
static void
CG_OffsetFirstPersonView (void)
{
  float *origin;
  float *angles;
  float bob;
  float ratio;
  float delta;
  float speed;
  int timeDelta;

  if (cg.snap->ps.pm_type == PM_INTERMISSION)
    {
      return;
    }

  origin = cg.refdef.vieworg;
  angles = cg.refdefViewAngles;

  // if dead, fix the angle and don't add any kick
  if (cg.snap->ps.stats[STAT_HEALTH] <= 0)
    {
      angles[ROLL] = 40;
      angles[PITCH] = -15;
      angles[YAW] = cg.snap->ps.stats[STAT_DEAD_YAW];
      origin[2] += cg.predictedPlayerState.viewheight;
      return;
    }

  // add angles based on damage kick
  if (cg.damageTime && cg_damagekick->integer)
    {
      ratio = cg.time - cg.damageTime;
      if (ratio < DAMAGE_DEFLECT_TIME)
	{
	  ratio /= DAMAGE_DEFLECT_TIME;
	  angles[PITCH] += ratio * cg.v_dmg_pitch;
	  angles[ROLL] += ratio * cg.v_dmg_roll;
	}
      else
	{
	  ratio = 1.0 - (ratio - DAMAGE_DEFLECT_TIME) / DAMAGE_RETURN_TIME;
	  if (ratio > 0)
	    {
	      angles[PITCH] += ratio * cg.v_dmg_pitch;
	      angles[ROLL] += ratio * cg.v_dmg_roll;
	    }
	}
    }

  // add pitch based on fall kick


  // add angles based on bob

  // make sure the bob is visible even at low speeds
  speed = cg.xyspeed > 200 ? cg.xyspeed : 200;

  delta = cg.bobfracsin * cg_bobpitch->value * speed;

  if (cg.predictedPlayerState.pm_flags & PMF_DUCKED)
    delta *= 3;		// crouching

  angles[PITCH] += delta;
  delta = cg.bobfracsin * cg_bobroll->value * speed;

  if (cg.predictedPlayerState.pm_flags & PMF_DUCKED)
    delta *= 3;		// crouching accentuates roll

  if (cg.bobcycle & 1)
    delta = -delta;

  angles[ROLL] += delta;

//===================================

  // add view height
  origin[2] += cg.predictedPlayerState.viewheight;

  // smooth out duck height changes
  timeDelta = cg.levelTime - cg.duckTime;
  if (timeDelta < 0)
    {
      cg.duckTime = cg.levelTime;
    }
  else if (timeDelta < DUCK_TIME)
    {
      cg.refdef.vieworg[2] -=
	cg.duckChange * (DUCK_TIME - timeDelta) / DUCK_TIME;
    }
  // add bob height
  bob = cg.bobfracsin * cg.xyspeed * cg_bobup->value;
  if (bob > 6)
    {
      bob = 6;
    }

  origin[2] += bob;

  // add fall height
  if (cg_damagefall->integer)
    {
      CG_DamageFallOffset ();
    }

  // add step offset
  if (cg_stepoffset->integer)
    {
      CG_StepOffset ();
    }

  // pivot the eye based on a neck length
}

//======================================================================

void
CG_ZoomDown_f (void)
{
  if (cg.zoomed)
    {
      return;
    }
  cg.zoomed = true;
  cg.zoomTime = cg.time;
}

void
CG_ZoomUp_f (void)
{
  if (!cg.zoomed)
    {
      return;
    }
  cg.zoomed = false;
  cg.zoomTime = cg.time;
}

/*
====================
CG_CalcFov

Fixed fov at intermissions, otherwise account for fov variable and zooms.
====================
*/
#define	WAVE_AMPLITUDE	1
#define	WAVE_FREQUENCY	0.4

static int
CG_CalcFov (void)
{
  float x;
  float phase;
  float v;
  int contents;
  float fov_x, fov_y;
  float zoomFov;
  float f;
  int inwater;

  if (cg.predictedPlayerState.pm_type == PM_INTERMISSION)
    {
      // if in intermission, use a fixed value
      fov_x = 90;
    }
  else
    {
      // user selectable
      if (cgs.dmflags & DF_FIXED_FOV)
	{
	  // dmflag to prevent wide fov for all clients
	  fov_x = 90;
	}
      else
	{
	  fov_x = cg_fov->value;
	  if (fov_x < 1)
	    {
	      fov_x = 1;
	    }
	  else if (fov_x > 160)
	    {
	      fov_x = 160;
	    }
	}

      // account for zooms
      zoomFov = cg_zoomFov->value;
      if (zoomFov < 1)
	{
	  zoomFov = 1;
	}
      else if (zoomFov > 160)
	{
	  zoomFov = 160;
	}

      if (cg.zoomed)
	{
	  if (cg_zoomtime->value)
	    {
	      f = (cg.time - cg.zoomTime) / cg_zoomtime->value;
	    }
	  else
	    {
	      f = 2;		// anything over 1.0
	    }
	  if (f > 1.0)
	    {
	      fov_x = zoomFov;
	    }
	  else
	    {
	      fov_x = fov_x + f * (zoomFov - fov_x);
	    }
	}
      else
	{
	  if (cg_zoomtime->value)
	    {
	      f = (cg.time - cg.zoomTime) / cg_zoomtime->value;
	    }
	  else
	    {
	      f = 2;
	    }
	  if (f > 1.0)
	    {
	      fov_x = fov_x;
	    }
	  else
	    {
	      fov_x = zoomFov + f * (fov_x - zoomFov);
	    }
	}
    }

  x = cg.refdef.width / tan (fov_x / 360 * M_PI);
  fov_y = atan2 (cg.refdef.height, x);
  fov_y = fov_y * 360 / M_PI;

  // warp if underwater
  contents = CG_PointContents (cg.refdef.vieworg, -1);
  if (contents & (CONTENTS_WATER | CONTENTS_SLIME | CONTENTS_LAVA))
    {
      phase = cg.time / 1000.0 * WAVE_FREQUENCY * M_PI * 2;
      v = WAVE_AMPLITUDE * sin (phase);
      fov_x += v;
      fov_y -= v;
      inwater = true;
    }
  else
    {
      inwater = false;
    }

  // set it
  cg.refdef.fov_x = fov_x;
  cg.refdef.fov_y = fov_y;

  if (!cg.zoomed)
    {
      cg.zoomSensitivity = 1;
    }
  else
    {
      //cg.zoomSensitivity = cg.refdef.fov_y / 75.0;
      // value already checked 0 divide avoided
      // note : negative values reverse axis
      if (cg_zoomsensitivityscale->value)
	{
	  cg.zoomSensitivity =
	    fov_x / cg_fov->value * cg_zoomsensitivityscale->value;
	}
      else
	{
	  cg.zoomSensitivity = 1;	// if you can't make scale value = cg_fov.val/fov_x
	}
    }

  return inwater;
}

/*
===============
CG_DamageBlendBlob

===============
*/
static void
CG_DamageBlendBlob (void)
{
  int t;
  int maxTime;
  refEntity_t ent;

  if (!cg.damageValue)
    {
      return;
    }

  maxTime = DAMAGE_TIME;
  t = cg.time - cg.damageTime;
  if (t <= 0 || t >= maxTime)
    {
      return;
    }

  Com_Memset (&ent, 0, sizeof (ent));
  ent.reType = RT_SPRITE;
  ent.renderfx = RF_FIRST_PERSON;

  VectorMA (cg.refdef.vieworg, 8, cg.refdef.viewaxis[0], ent.origin);
  VectorMA (ent.origin, cg.damageX * -8, cg.refdef.viewaxis[1], ent.origin);
  VectorMA (ent.origin, cg.damageY * 8, cg.refdef.viewaxis[2], ent.origin);

  ent.radius = cg.damageValue * 3;
  ent.customShader = cgs.media.viewBloodShader;
  ent.shaderRGBA[0] = 255;
  ent.shaderRGBA[1] = 255;
  ent.shaderRGBA[2] = 255;
  ent.shaderRGBA[3] = 200 * (1.0 - ((float) t / maxTime));
  cgl->R_AddRefEntityToScene (&ent);
}

/*
===============
CG_CalcViewValues

Sets cg.refdef view values
===============
*/
static int
CG_CalcViewValues (void)
{
  playerState_t *ps;

  Com_Memset (&cg.refdef, 0, sizeof (cg.refdef));

  // strings for in game rendering
  // Q_strncpyz( cg.refdef.text[0], "Park Ranger", sizeof(cg.refdef.text[0]) );
  // Q_strncpyz( cg.refdef.text[1], "19", sizeof(cg.refdef.text[1]) );

  // calculate size of 3D view
  CG_CalcVrect ();

  // compute hud scaling
  CG_CalcHudAspect ();

  ps = &cg.predictedPlayerState;

  // intermission view
  if (ps->pm_type == PM_INTERMISSION)
    {
      VectorCopy (ps->origin, cg.refdef.vieworg);
      VectorCopy (ps->viewangles, cg.refdefViewAngles);
      AnglesToAxis (cg.refdefViewAngles, cg.refdef.viewaxis);
      return CG_CalcFov ();
    }

  cg.bobcycle = (ps->bobCycle & 128) >> 7;
  cg.bobfracsin = fabs (sin ((ps->bobCycle & 127) / 127.0 * M_PI));
  cg.xyspeed = Q_sqrt (ps->velocity[0] * ps->velocity[0] +
		       ps->velocity[1] * ps->velocity[1]);

  VectorCopy (ps->origin, cg.refdef.vieworg);
  VectorCopy (ps->viewangles, cg.refdefViewAngles);

  // add error decay
  if (cg_errorDecay->value > 0)
    {
      int t;
      float f;

      t = cg.time - cg.predictedErrorTime;
      f = (cg_errorDecay->value - t) / cg_errorDecay->value;
      if ((f > 0) && (f < 1))
	{
	  VectorMA (cg.refdef.vieworg, f, cg.predictedError,
		    cg.refdef.vieworg);
	}
      else
	{
	  cg.predictedErrorTime = 0;
	}
    }

  if (cg.renderingThirdPerson)
    {
      // back away from character
      CG_OffsetThirdPersonView ();
    }
  else
    {
      // offset for local bobbing and kicks
      CG_OffsetFirstPersonView ();
    }

  // position eye reletive to origin
  AnglesToAxis (cg.refdefViewAngles, cg.refdef.viewaxis);

  if (cg.hyperspace)
    {
      cg.refdef.rdflags |= RDF_NOWORLDMODEL;
      cg.refdef.rdflags |= RDF_HYPERSPACE;
    }

  // field of view
  return CG_CalcFov ();
}

/*
=====================
CG_PowerupTimerSounds
=====================
*/
static void
CG_PowerupTimerSounds (void)
{
  int i;
  int t;

  // powerup timers going away
  for (i = 0; i < MAX_POWERUPS; i++)
    {
      t = cg.snap->ps.powerups[i];
      if (t <= cg.levelTime)
	{
	  continue;
	}
      if (t - cg.levelTime >= POWERUP_BLINKS * POWERUP_BLINK_TIME)
	{
	  continue;
	}

      if (((t - cg.levelTime) / POWERUP_BLINK_TIME)
	  != ((t - cg.oldLevelTime) / POWERUP_BLINK_TIME))
	{
	  cgl->snd->StartSound (NULL, cg.snap->ps.clientNum, CHAN_ITEM,
				cgs.media.wearOffSound);
	}
    }
}

/*
=====================
CG_AddBufferedSound
=====================
*/
void
CG_AddBufferedSound (sfxHandle_t sfx)
{
  if (!sfx)
    {
      return;
    }

  cg.soundBuffer[cg.soundBufferIn] = sfx;
  cg.soundBufferIn = (cg.soundBufferIn + 1) % MAX_SOUNDBUFFER;
  if (cg.soundBufferIn == cg.soundBufferOut)
    {
      cg.soundBufferOut++;
    }
}

/*
=====================
CG_PlayBufferedSounds
=====================
*/
static void
CG_PlayBufferedSounds (void)
{
  if (cg.soundTime < cg.time)
    {
      if ((cg.soundBufferOut != cg.soundBufferIn)
	  && (cg.soundBuffer[cg.soundBufferOut]))
	{
	  cgl->snd->StartLocalSound (cg.soundBuffer[cg.soundBufferOut],
				     CHAN_ANNOUNCER);
	  cg.soundBuffer[cg.soundBufferOut] = 0;
	  cg.soundBufferOut = (cg.soundBufferOut + 1) % MAX_SOUNDBUFFER;
	  cg.soundTime = cg.time + 750;
	}
    }
}

/*
===============
CG_GenerateDemoName
===============
*/
int
CG_GenerateDemoName (char *demoName, unsigned int size)
{
  qtime_t qtime;
  char *msg;
  char mapname[MAX_QPATH];
  char povname[36]; // MAX_NETNAME value
  char shortname[36]; // MAX_NETNAME value
  char shortname2[36];

  switch (cgs.gametype)
    {
      // gametype first
    case GT_FFA :
      Q_strncpyz (demoName, "FFA-", size);
      break;
    case GT_TOURNAMENT:
      Q_strncpyz (demoName, "1v1-", size);
      break;
    case GT_TEAM:
      Q_strncpyz (demoName, "TDM-", size);
      break;
    case GT_CTF:
      Q_strncpyz (demoName, "CTF-", size);
      break;
    case GT_ARENA:
      Q_strncpyz (demoName, "Arena-", size);
      break;
    default:
      break;
    }

  // player name(s)
  Q_strncpyz (povname, cgs.clientinfo[cg.predictedPlayerState.clientNum].name,
	      sizeof (povname));

  Q_CleanStr (povname);

  Q_strcat (demoName, size, va ("%s(POV)", povname));

  if ((cgs.gametype == GT_TOURNAMENT)
      || (BG_GametypeIsTeam (cgs.gametype)))
    {
      const char *msg;

      msg = CG_ConfigString (CS_TEAM1_NAME);
      if (*msg)
	{
	  Q_strncpyz (shortname, msg, sizeof (shortname));
	  Q_CleanStr (shortname);
	  if (Q_strcmp (shortname, povname))
	    {
	      Q_strcat (demoName, size, "-");
	      Q_strcat (demoName, size, shortname);
	    }
	}

      msg = CG_ConfigString (CS_TEAM2_NAME);
      if (*msg)
	{
	  Q_strncpyz (shortname2, msg, sizeof (shortname2));
	  Q_CleanStr (shortname2);
	  if (Q_strcmp (shortname2, povname))
	    {
	      Q_strcat (demoName, size, "-");
	      Q_strcat (demoName, size, shortname2);
	    }
	}
    }

  // could be nice to use 2 more configstring for team1/team2 names
  // useful for team games, add a /teamname command, and for duel

  Q_strcat (demoName, size, "-");

  // mapname
  // ptr = strchr (cgs.mapname, '/');
  // use the reverse version for precaution
  // just grab the map name, not the directory
  msg = strrchr (cgs.mapname, '/');
  Q_strncpyz (mapname, msg + 1, sizeof (mapname));
  // strip the .bsp extention
  msg = mapname;
  while (*msg)
    {
      if (*msg == '.')
	{
	  *msg = '\0';
	  break;
	}
      msg++;
    }
  Q_strcat (demoName, size, mapname);

  //Q_strcat (demoName, size, "-");

  // server name
  //info = CG_ConfigString (CS_SERVERINFO);
  //msg = Info_ValueForKey (info, CS_SERVERINFO_HOSTNAME);
  //Q_strncpyz (shortname, msg, sizeof (shortname));
  //Q_CleanStr (shortname);
  //Q_strcat (demoName, size, shortname);

  // date
  cgl->RealTime (&qtime);

  msg = va ("-%d_%d_%d-%d_%d_%d", qtime.tm_year + 1900, qtime.tm_mon + 1, qtime.tm_mday,
	    qtime.tm_hour, qtime.tm_min, qtime.tm_sec);
  Q_strcat (demoName, size, msg);

  return 1;
}

/*
=================
CG_ProcessAutoActions
=================
*/
static void
CG_ProcessAutoActions (void)
{
  int matchStatus;
  char demoName[MAX_QPATH];

  // don't record recursively when playing the demo
  if (cg.demoPlayback)
    return;

  matchStatus = cg.predictedPlayerState.stats[STAT_MATCHSTATUS];

  // we should start recording a demo
  if (!(cg.matchStatus & MS_COUNTDOWN) && (matchStatus & MS_COUNTDOWN))
    {
      if (cg_autoAction->integer & AUTOACTION_DEMO)
	{
	  if (CG_GenerateDemoName (demoName, sizeof (demoName)))
	    {
	      cgl->SendConsoleCommand (va ("record \"%s\";", demoName));
	    }
	  else
	    {
	      cgl->SendConsoleCommand ("record;");
	    }
	}
      cg.matchStatus |= MS_COUNTDOWN;
    }
  // stop recording the demo if countdown broke
  else if ((cg.matchStatus & MS_COUNTDOWN) && !(matchStatus & MS_COUNTDOWN)
	   && !(matchStatus & MS_LIVE))
    {
      if (cg_autoAction->integer & AUTOACTION_DEMO)
	{
	  cgl->SendConsoleCommand ("stoprecord;");
	}
      cg.matchStatus &= ~MS_COUNTDOWN;
    }
  // we are live, don't consider the countdown flag anymore
  else if ((cg.matchStatus & MS_COUNTDOWN) && !(matchStatus & MS_COUNTDOWN)
	   && (matchStatus & MS_LIVE))
    {
      cg.matchStatus &= ~MS_COUNTDOWN;
      cg.matchStatus |= MS_LIVE;
    }
  // match end
  if ((cg.matchStatus & MS_LIVE) && !(matchStatus & MS_LIVE))
    {
      //cg.matchStatus &= ~MS_LIVE;
      cg.matchStatus = 0;
    }

  if (cg.screenshotTime && (cg.time > cg.screenshotTime))
    {
      char fileName[MAX_QPATH];

      if (CG_GenerateDemoName (fileName, sizeof (fileName)))
	{
	  cgl->SendConsoleCommand (va ("screenshotJPEG \"%s\";", fileName));
	}
      else
	{
	  cgl->SendConsoleCommand ("screenshotJPEG;");
	}
      cg.screenshotTime = 0;
    }

  if (cg.demoStopTime && (cg.time > cg.demoStopTime))
    {
      cgl->SendConsoleCommand ("stoprecord;");
      cg.demoStopTime = 0;
    }
}

//=========================================================================

/*
=================
CG_DrawActiveFrame

Generates and draws a game scene and status information at the given time.
=================
*/
void
CG_DrawActiveFrame (int serverTime, int simulationTime)
{
  int inwater;

  // if we are only updating the screen as a loading
  // pacifier, don't even try to read snapshots
  if (cg.infoScreenText[0] != 0)
    {
      CG_DrawInformation ();
      return;
    }

  // if we haven't received any snapshots yet, all
  // we can draw is the information screen
  if (!cg.snap || (cg.snap->snapFlags & SNAPFLAG_NOT_ACTIVE))
    {
      CG_DrawInformation ();
      return;
    }

  // any looped sounds will be respecified as entities
  // are added to the render list
  cgl->snd->ClearLoopingSounds (false);

  // clear all the render lists
  cgl->R_ClearScene ();

  // decide on third person view
  cg.renderingThirdPerson = (cg_thirdPerson->integer
			     || (cg.snap->ps.stats[STAT_HEALTH] <= 0));

  // build cg.refdef
  inwater = CG_CalcViewValues ();

  // first person blend blobs, done after AnglesToAxis
  if (!cg.renderingThirdPerson && cg_damagesplash->integer)
    {
      CG_DamageBlendBlob ();
    }

  // build the render lists
  if (!cg.hyperspace)
    {
      CG_AddPacketEntities ();	// after calcViewValues, so predicted player state is correct
      CG_AddMarks ();
      CG_AddLocalEntities ();
    }
  CG_AddViewWeapon (&cg.predictedPlayerState);

  // add buffered sounds
  CG_PlayBufferedSounds ();

  // finish up the rest of the refdef
  if (cg.testModelEntity.hModel)
    {
      CG_AddTestModel ();
    }

  cg.refdef.time = cg.levelTime;
  Com_Memcpy (cg.refdef.areamask, cg.snap->areamask, sizeof (cg.refdef.areamask));

  // warning sounds when powerup is wearing off
  CG_PowerupTimerSounds ();

  // update audio positions
  cgl->snd->Respatialize (cg.snap->ps.clientNum, cg.refdef.vieworg,
			  cg.refdef.viewaxis, inwater);

  CG_AddLagometerFrameInfo ();

  if (cg_timescale->value != cg_timescaleFadeEnd->value)
    {
      if (cg_timescale->value < cg_timescaleFadeEnd->value)
	{
	  cg_timescale->value +=
	    cg_timescaleFadeSpeed->value * ((float) cg.frametime) / 1000;
	  if (cg_timescale->value > cg_timescaleFadeEnd->value)
	    cg_timescale->value = cg_timescaleFadeEnd->value;
	}
      else
	{
	  cg_timescale->value -=
	    cg_timescaleFadeSpeed->value * ((float) cg.frametime) / 1000;
	  if (cg_timescale->value < cg_timescaleFadeEnd->value)
	    cg_timescale->value = cg_timescaleFadeEnd->value;
	}
      if (cg_timescaleFadeSpeed->value)
	{
	  cgl->Cvar_Set ("timescale", va ("%f", cg_timescale->value));
	}
    }

  // this is a bit dirty, but avoids playing all looping sounds registered this frame
  if (cgs.svpaused)
    {
      cgl->snd->ClearLoopingSounds (false);
    }

  // actually issue the rendering calls
  CG_DrawActive ();

  if (cg_stats->integer)
    {
      CG_Printf ("cg.clientFrame:%i\n", cg.clientFrame);
    }
}

/*
=================
CG_ProcessActiveFrame

Process the snapshots and update the cgame knowledges of the game.
=================
*/
void
CG_ProcessActiveFrame (int serverTime, int simulationTime, bool demoPlayback)
{
  cg.demoPlayback = demoPlayback;

  cg.oldTime = cg.time;
  cg.time = serverTime;

  cg.frametime = cg.time - cg.oldTime;
  if (cg.frametime < 0)
    {
      cg.frametime = 0;
    }

  // if the game is paused take the snapshot simulationTime
  if (cgs.svpaused)
    {
      cg.oldLevelTime = cg.snap->simulationTime;
      cg.levelTime = cg.snap->simulationTime;
      cg.levelFrameTime = 0;
    }
  // if the simulationtime goes backward, reset all localentities
  // this can happend when a match starts or on a map_restart
  else if (simulationTime < cg.levelTime)
    {
      int i;
      centity_t *cent;

      CG_FreeAllLocalEntities ();

      cg.predictedPlayerEntity.pe.muzzleFlashTime = 0;
      cg.predictedPlayerEntity.pe.barrelTime = 0;
      cg.predictedPlayerEntity.pe.painTime = 0;
      cg.predictedPlayerEntity.miscTime = 0;
      cg.predictedPlayerEntity.trailTime = 0;

      for (i = 0; i < MAX_CLIENTS; i++)
	{
	  cent = &cg_entities[i];

	  cent->pe.muzzleFlashTime = 0;
	  cent->pe.barrelTime = 0;
	  cent->pe.painTime = 0;
	  cent->miscTime = 0;
	  cent->trailTime = 0;
	}

      cg.itemPickupTime = 0;
      cg.itemPickupBlendTime = 0;
      cg.rewardTime = 0;
      cg.powerupTime = 0;
      cg.stepTime = 0;
      cg.duckTime = 0;
      cg.landTime = 0;

      cg.oldLevelTime = simulationTime;
      cg.levelTime = simulationTime;
      cg.levelFrameTime = 0;
    }
  else
    {
      cg.oldLevelTime = cg.levelTime;
      cg.levelTime = simulationTime;
      cg.levelFrameTime = cg.levelTime - cg.oldLevelTime;
    }

  // update cvars
  CG_UpdateCvars ();

  // if we are only updating the screen as a loading
  // pacifier, don't even try to read snapshots
  if (cg.infoScreenText[0] != 0)
    {
      //CG_DrawInformation ();
      return;
    }

  // set up cg.snap and possibly cg.nextSnap
  CG_ProcessSnapshots ();

  // if we haven't received any snapshots yet, all
  // we can draw is the information screen
  if (!cg.snap || (cg.snap->snapFlags & SNAPFLAG_NOT_ACTIVE))
    {
      //CG_DrawInformation ();
      return;
    }

  // prepare the commands for autoactions
  CG_ProcessAutoActions ();

  // let the client system know what our weapon and zoom settings are
  cgl->SetUserCmdValue (cg.weaponSelect, cg.zoomSensitivity);

  // this counter will be bumped for every valid scene we generate
  cg.clientFrame++;

  // update cg.predictedPlayerState
  CG_PredictPlayerState ();
}

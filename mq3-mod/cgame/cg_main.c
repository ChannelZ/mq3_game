/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
//
// cg_main.c -- initialization and primary entry point for cgame
#include "cg_local.h"

struct cgamelib_import_s *cgl;
struct cgamelib_export_s cge;

char *
lib_info (void)
{
  return "CGame Library alpha 0.00001664";
}

cg_t cg;
cgs_t cgs;
centity_t cg_entities[MAX_GENTITIES];
//weaponInfo_t  cg_weapons[MAX_WEAPONS];
weaponInfo_t cg_weapons[WP_NUM_WEAPONS];
//itemInfo_t    cg_items[MAX_ITEMS];
itemInfo_t cg_items[BG_NUMITEMS];

modificationCounter_t modifCounter;

// avoid using them uninitialized
// team color model
byte tcolmodel[3][4] = {
  {255, 255, 255, 255},
  {255, 255, 255, 255},
  {255, 255, 255, 255}
};

// enemy color model
byte encolmodel[3][4] = {
  {255, 255, 255, 255},
  {255, 255, 255, 255},
  {255, 255, 255, 255}
};

// cvars
cvar_t * cg_centertime;

cvar_t * cg_drawGun;
cvar_t * cg_drawGunPosition;
cvar_t * cg_gun_x;
cvar_t * cg_gun_y;
cvar_t * cg_gun_z;
cvar_t * cg_runpitch;
cvar_t * cg_runroll;
cvar_t * cg_bobup;
cvar_t * cg_bobpitch;
cvar_t * cg_bobroll;
cvar_t * cg_swingSpeed;
cvar_t * cg_tracerChance;
cvar_t * cg_tracerWidth;
cvar_t * cg_tracerLength;

cvar_t * cg_crosshairSize;
cvar_t * cg_crosshairX;
cvar_t * cg_crosshairY;
cvar_t * cg_crosshairHealth;

cvar_t * cg_forceModel;
cvar_t * cg_forceRailColor;
cvar_t * cg_forceRailTeamColor;

cvar_t * cg_railTrailTime;
cvar_t * cg_noProjectileTrail;
cvar_t * cg_railTrailStyle;
cvar_t * cg_railTrailParam;
cvar_t * cg_oldRocket;
cvar_t * cg_oldPlasma;
cvar_t * cg_trueLightning;
cvar_t * cg_lightningStyle;

cvar_t * cg_shadows;
cvar_t * cg_blood;
cvar_t * cg_gibs;
cvar_t * cg_footsteps;
cvar_t * cg_addMarks;
cvar_t * cg_brassTime;
cvar_t * cg_lagometer;
cvar_t * cg_autoswitch;
cvar_t * cg_scorePlum;
cvar_t * cg_autoAction;

cvar_t * cg_drawTimer;
cvar_t * cg_drawTimerX;
cvar_t * cg_drawTimerY;
cvar_t * cg_drawTimerSize;
cvar_t * cg_drawFPS;
cvar_t * cg_drawSnapshot;
cvar_t * cg_draw2D;
cvar_t * cg_drawStatus;
cvar_t * cg_draw3dIcons;
cvar_t * cg_drawIcons;
cvar_t * cg_simpleItems;
cvar_t * cg_drawAmmoWarning;
cvar_t * cg_drawCrosshair;
cvar_t * cg_drawCrosshairNames;
cvar_t * cg_drawCrosshairCmds;
cvar_t * cg_drawRewards;
cvar_t * cg_drawSpectatorNames;
cvar_t * cg_drawAttacker;
cvar_t * cg_noTaunt;
cvar_t * cg_drawScoreboardOnDeath;

cvar_t * cg_fov;
cvar_t * cg_zoomFov;
cvar_t * cg_viewsize;
cvar_t * cg_thirdPerson;
cvar_t * cg_thirdPersonRange;
cvar_t * cg_thirdPersonAngle;

cvar_t * cg_teamChatTime;
cvar_t * cg_teamChatLines;
cvar_t * cg_teamChatTextSize;
cvar_t * cg_teamChatPosition;
cvar_t * cg_teamChatBackgroundColor;
cvar_t * cg_drawTeamOverlay;
cvar_t * cg_drawTeamOverlayX;
cvar_t * cg_drawTeamOverlayY;
cvar_t * cg_drawTeamOverlaySize;
cvar_t * cg_teamOverlayUserinfo;
cvar_t * cg_drawFriend;
cvar_t * cg_teamChatsOnly;
cvar_t * cg_noChatBeep;

cvar_t * cg_smoothClients;
cvar_t * cg_smoothSpectatorView;
cvar_t * cg_synchronousClients;
cvar_t * cg_predictItems;
cvar_t * cg_deferPlayers;

cvar_t * cg_timescaleFadeEnd;
cvar_t * cg_timescaleFadeSpeed;
cvar_t * cg_timescale;

cvar_t * cg_stats;
cvar_t * cg_buildScript;
cvar_t * cg_animSpeed;
cvar_t * cg_debugAnim;
cvar_t * cg_debugPosition;
cvar_t * cg_debugEvents;
cvar_t * cg_debugSnapshots;
cvar_t * cg_errorDecay;
cvar_t * cg_debugMove;
cvar_t * cg_nopredict;
cvar_t * cg_noPlayerAnims;
cvar_t * cg_showmiss;


cvar_t * cg_damagesplash;
cvar_t * cg_damagekick;
cvar_t * cg_damagefall;
cvar_t * cg_stepoffset;
cvar_t * cg_lightningimpact;
cvar_t * cg_muzzleflash;
cvar_t * cg_enemymodel;
cvar_t * cg_teammodel;
cvar_t * cg_enemycolors;
cvar_t * cg_teamcolors;
cvar_t * cg_forcecolor;
cvar_t * cg_deadbodydarken;
cvar_t * ch_crosshaircolor;
cvar_t * ch_crosshairpulseitems;
cvar_t * ch_weaponselectStyle;
cvar_t * ch_weaponselectFadeTime;
cvar_t * ch_weaponselectPosition;
cvar_t * ch_weaponselectDrawAll;
cvar_t * ch_statusbarStyle;
cvar_t * ch_statusbarBackgroundColor;
cvar_t * ch_centerunscaled;

cvar_t * cg_shotgunsmokepuff;
cvar_t * cg_nomip;
cvar_t * cg_nomip2;
cvar_t * cg_drawspeed;
cvar_t * cg_zoomtime;
cvar_t * cg_zoomsensitivityscale;
cvar_t * cg_autoswitchorder;
cvar_t * cg_noammoswitch;
cvar_t * cg_noweaponswitchonevent;
cvar_t * dev_cg_s_ambient;
cvar_t * cg_grenadecolor;
cvar_t * dev_cg_nohyperspace;
cvar_t * dev_cg_crosshairhideonfire;
cvar_t * dev_cg_drawBBox;
cvar_t * dev_cg_noteleporteffect;
cvar_t * dev_cg_cheat_timers;

cvar_t * ci_model;
cvar_t * ci_headmodel;
cvar_t * ci_team_model;
cvar_t * ci_team_headmodel;
cvar_t * ci_modelcolor;
cvar_t * ci_color1;
cvar_t * ci_color2;
cvar_t * ci_handicap;

/*
=================
CG_InitCvars
=================
*/
void
CG_InitCvars (void)
{
  cg_autoswitch = cgl->Cvar_Get ("cg_autoswitch", "1", CVAR_ARCHIVE);
  cg_drawGun = cgl->Cvar_Get ("cg_drawGun", "1", CVAR_ARCHIVE);
  cg_drawGunPosition = cgl->Cvar_Get ("cg_drawGunPosition", "1", CVAR_ARCHIVE);
  cg_zoomFov = cgl->Cvar_Get ("cg_zoomfov", "45", CVAR_ARCHIVE);
  cg_fov = cgl->Cvar_Get ("cg_fov", "90", CVAR_ARCHIVE);
  cg_viewsize = cgl->Cvar_Get ("cg_viewsize", "100", CVAR_ARCHIVE);
  cg_shadows = cgl->Cvar_Get ("cg_shadows", "1", CVAR_ARCHIVE);
  cg_gibs = cgl->Cvar_Get ("cg_gibs", "1", CVAR_ARCHIVE);
  cg_draw2D = cgl->Cvar_Get ("cg_draw2D", "1", CVAR_ARCHIVE);
  cg_drawStatus = cgl->Cvar_Get ("cg_drawStatus", "1", CVAR_ARCHIVE);
  cg_drawTimer = cgl->Cvar_Get ("cg_drawTimer", "1", CVAR_ARCHIVE);
  cg_drawTimerX = cgl->Cvar_Get ("cg_drawTimerX", "300", CVAR_ARCHIVE);
  cgl->Cvar_CheckRange (cg_drawTimerX, 1, 620, true);
  cg_drawTimerY = cgl->Cvar_Get ("cg_drawTimerY", "85", CVAR_ARCHIVE);
  cgl->Cvar_CheckRange (cg_drawTimerY, 1, 460, true);
  cg_drawTimerSize = cgl->Cvar_Get ("cg_drawTimerSize", "16", CVAR_ARCHIVE);
  cg_drawFPS = cgl->Cvar_Get ("cg_drawFPS", "1", CVAR_ARCHIVE);
  cg_drawSnapshot = cgl->Cvar_Get ("cg_drawSnapshot", "0", CVAR_ARCHIVE);
  cg_draw3dIcons = cgl->Cvar_Get ("cg_draw3dIcons", "0", CVAR_ARCHIVE);
  cg_drawIcons = cgl->Cvar_Get ("cg_drawIcons", "1", CVAR_ARCHIVE);
  cg_drawAmmoWarning = cgl->Cvar_Get ("cg_drawAmmoWarning", "1", CVAR_ARCHIVE);
  cg_drawAttacker = cgl->Cvar_Get ("cg_drawAttacker", "0", CVAR_ARCHIVE);
  cg_drawCrosshair = cgl->Cvar_Get ("cg_drawCrosshair", "4", CVAR_ARCHIVE);
  cg_drawCrosshairNames = cgl->Cvar_Get ("cg_drawCrosshairNames", "1", CVAR_ARCHIVE);
  cg_drawCrosshairCmds = cgl->Cvar_Get ("cg_drawCrosshairCmds", "0", CVAR_ARCHIVE);
  cg_drawRewards = cgl->Cvar_Get ("cg_drawRewards", "1", CVAR_ARCHIVE);
  cg_drawSpectatorNames = cgl->Cvar_Get ("cg_drawSpectatorNames", "1", CVAR_ARCHIVE);
  cg_drawScoreboardOnDeath = cgl->Cvar_Get ("cg_drawScoreboardOnDeath", "1", CVAR_ARCHIVE);
  cg_crosshairSize = cgl->Cvar_Get ("cg_crosshairSize", "24", CVAR_ARCHIVE);
  cg_crosshairHealth = cgl->Cvar_Get ("cg_crosshairHealth", "1", CVAR_ARCHIVE);
  cg_crosshairX = cgl->Cvar_Get ("cg_crosshairX", "0", CVAR_ARCHIVE);
  cg_crosshairY = cgl->Cvar_Get ("cg_crosshairY", "0", CVAR_ARCHIVE);
  cg_brassTime = cgl->Cvar_Get ("cg_brassTime", "2500", CVAR_ARCHIVE);
  cg_simpleItems = cgl->Cvar_Get ("cg_simpleItems", "1", CVAR_ARCHIVE);
  cg_addMarks = cgl->Cvar_Get ("cg_marks", "1", CVAR_ARCHIVE);
  cg_lagometer = cgl->Cvar_Get ("cg_lagometer", "1", CVAR_ARCHIVE);
  cg_railTrailTime = cgl->Cvar_Get ("cg_railTrailTime", "400", CVAR_ARCHIVE);
  cg_railTrailParam = cgl->Cvar_Get ("cg_railTrailParam", "10", CVAR_ARCHIVE);
  cg_gun_x = cgl->Cvar_Get ("cg_gunX", "0", CVAR_ARCHIVE);
  cg_gun_y = cgl->Cvar_Get ("cg_gunY", "0", CVAR_ARCHIVE);
  cg_gun_z = cgl->Cvar_Get ("cg_gunZ", "0", CVAR_ARCHIVE);
  cg_centertime = cgl->Cvar_Get ("cg_centertime", "3", CVAR_CHEAT);
  cg_bobup = cgl->Cvar_Get ("cg_bobup", "0.005", CVAR_ARCHIVE);
  cg_bobpitch = cgl->Cvar_Get ("cg_bobpitch", "0.002", CVAR_ARCHIVE);
  cg_bobroll = cgl->Cvar_Get ("cg_bobroll", "0.002", CVAR_ARCHIVE);
  cg_swingSpeed = cgl->Cvar_Get ("cg_swingSpeed", "0.3", CVAR_CHEAT);
  cg_animSpeed = cgl->Cvar_Get ("cg_animspeed", "1", CVAR_CHEAT);
  cg_debugAnim = cgl->Cvar_Get ("cg_debuganim", "0", CVAR_CHEAT);
  cg_debugPosition = cgl->Cvar_Get ("cg_debugposition", "0", CVAR_CHEAT);
  cg_debugEvents = cgl->Cvar_Get ("cg_debugevents", "0", CVAR_CHEAT);
  cg_debugSnapshots = cgl->Cvar_Get ("cg_debugsnapshots", "0", CVAR_CHEAT);
  cg_errorDecay = cgl->Cvar_Get ("cg_errordecay", "100", 0);
  cg_debugMove = cgl->Cvar_Get ("cg_debugMove", "0", 0);
  cg_nopredict = cgl->Cvar_Get ("cg_nopredict", "0", 0);
  cg_noPlayerAnims = cgl->Cvar_Get ("cg_noplayeranims", "0", CVAR_CHEAT);
  cg_showmiss = cgl->Cvar_Get ("cg_showmiss", "0", 0);
  cg_footsteps = cgl->Cvar_Get ("cg_footsteps", "1", CVAR_ARCHIVE);
  cg_tracerChance = cgl->Cvar_Get ("cg_tracerchance", "0.4", CVAR_CHEAT);
  cg_tracerWidth = cgl->Cvar_Get ("cg_tracerwidth", "1", CVAR_CHEAT);
  cg_tracerLength = cgl->Cvar_Get ("cg_tracerlength", "100", CVAR_CHEAT);
  cg_thirdPersonRange = cgl->Cvar_Get ("cg_thirdPersonRange", "40", CVAR_CHEAT);
  cg_thirdPersonAngle = cgl->Cvar_Get ("cg_thirdPersonAngle", "0", CVAR_CHEAT);
  cg_thirdPerson = cgl->Cvar_Get ("cg_thirdPerson", "0", 0);
  cg_teamChatTime = cgl->Cvar_Get ("cg_teamChatTime", "3000", CVAR_ARCHIVE);
  cg_teamChatLines = cgl->Cvar_Get ("cg_teamChatLines", "0", CVAR_ARCHIVE);
  cg_teamChatTextSize = cgl->Cvar_Get ("cg_teamChatTextSize", "0", CVAR_ARCHIVE);
  cg_teamChatPosition = cgl->Cvar_Get ("cg_teamChatPosition", "0", CVAR_ARCHIVE);
  cg_teamChatBackgroundColor = cgl->Cvar_Get ("cg_teamChatBackgroundColor", "", CVAR_ARCHIVE);
  cg_forceModel = cgl->Cvar_Get ("cg_forceModel", "1", CVAR_ARCHIVE);
  cg_forceRailColor = cgl->Cvar_Get ("cg_forceRailColor", "0", CVAR_ARCHIVE);
  cg_forceRailTeamColor = cgl->Cvar_Get ("cg_forceRailTeamColor", "0", CVAR_ARCHIVE);
  cg_predictItems = cgl->Cvar_Get ("cg_predictItems", "0", CVAR_ARCHIVE); // unused
  cg_deferPlayers = cgl->Cvar_Get ("cg_deferPlayers", "1", CVAR_ARCHIVE);
  cg_drawTeamOverlay = cgl->Cvar_Get ("cg_drawTeamOverlay", "0", CVAR_ARCHIVE);
  cg_drawTeamOverlayX = cgl->Cvar_Get ("cg_drawTeamOverlayX", "", CVAR_ARCHIVE);
  cg_drawTeamOverlayY = cgl->Cvar_Get ("cg_drawTeamOverlayY", "", CVAR_ARCHIVE);
  cg_drawTeamOverlaySize = cgl->Cvar_Get ("cg_drawTeamOverlaySize", "", CVAR_ARCHIVE);
  cg_teamOverlayUserinfo = cgl->Cvar_Get ("teamoverlay", "0", CVAR_ROM | CVAR_USERINFO);
  cg_stats = cgl->Cvar_Get ("cg_stats", "0", 0);
  cg_drawFriend = cgl->Cvar_Get ("cg_drawFriend", "1", CVAR_ARCHIVE);
  cg_teamChatsOnly = cgl->Cvar_Get ("cg_teamChatsOnly", "0", CVAR_ARCHIVE);
  cg_noChatBeep = cgl->Cvar_Get ("cg_noChatBeep", "0", CVAR_ARCHIVE);
  // the following variables are created in other parts of the system,
  // but we also reference them here

  // force loading of all possible data amd error on failures
  //cg_buildScript = cgl->Cvar_Get ("com_buildScript", "0", 0);
  // bit of stupid hack to load all the resources on cgame load
  // this should be temporary the time to improve the loading process
  // to allow dynamical new resource loading if not already loaded
  cg_buildScript = cgl->Cvar_Get ("cg_buildScript", "1", 0);
  cg_blood = cgl->Cvar_Get ("cg_blood", "1", CVAR_ARCHIVE);
  cg_synchronousClients = cgl->Cvar_Get ("g_synchronousClients", "0", CVAR_SYSTEMINFO); // communicated by systeminfo
  cg_timescaleFadeEnd = cgl->Cvar_Get ("cg_timescaleFadeEnd", "1", 0);
  cg_timescaleFadeSpeed = cgl->Cvar_Get ("cg_timescaleFadeSpeed", "0", 0);
  cg_timescale = cgl->Cvar_Get ("timescale", "1", 0); // grab the cvar created by the client
  cg_scorePlum = cgl->Cvar_Get ("cg_scorePlums", "1", CVAR_ARCHIVE);
  cg_autoAction = cgl->Cvar_Get ("cg_autoAction", "0", CVAR_ARCHIVE);
  cg_smoothClients = cgl->Cvar_Get ("cg_smoothClients", "0", /*CVAR_USERINFO | */CVAR_ARCHIVE);
  cg_smoothSpectatorView = cgl->Cvar_Get ("cg_smoothSpectatorView", "1", CVAR_ARCHIVE);

  cg_noTaunt = cgl->Cvar_Get ("cg_noTaunt", "0", CVAR_ARCHIVE);
  cg_noProjectileTrail = cgl->Cvar_Get ("cg_noProjectileTrail", "0", CVAR_ARCHIVE);
  cg_railTrailStyle = cgl->Cvar_Get ("cg_railTrailStyle", "1", CVAR_ARCHIVE);
  cg_oldRocket = cgl->Cvar_Get ("cg_oldRocket", "1", CVAR_ARCHIVE);
  cg_oldPlasma = cgl->Cvar_Get ("cg_oldPlasma", "1", CVAR_ARCHIVE);
  cg_trueLightning = cgl->Cvar_Get ("cg_trueLightning", "0.4", CVAR_ARCHIVE);
  cg_lightningStyle = cgl->Cvar_Get ("cg_lightningStyle", "1", CVAR_ARCHIVE);
  cg_damagesplash = cgl->Cvar_Get ("cg_damagesplash", "1", CVAR_ARCHIVE);
  cg_damagekick = cgl->Cvar_Get ("cg_damagekick", "1", CVAR_ARCHIVE);
  cg_damagefall = cgl->Cvar_Get ("cg_damagefall", "1", CVAR_ARCHIVE);
  cg_stepoffset = cgl->Cvar_Get ("cg_stepoffset", "1", CVAR_ARCHIVE);
  cg_muzzleflash = cgl->Cvar_Get ("cg_muzzleflash", "1", CVAR_ARCHIVE);
  cg_lightningimpact = cgl->Cvar_Get ("cg_lightningimpact", "1", CVAR_ARCHIVE);
  cg_enemymodel = cgl->Cvar_Get ("cg_enemymodel", "keel", CVAR_ARCHIVE);
  cg_teammodel = cgl->Cvar_Get ("cg_teammodel", "sarge", CVAR_ARCHIVE);
  cg_enemycolors = cgl->Cvar_Get ("cg_enemycolors", "1:2:3", CVAR_ARCHIVE);
  cg_teamcolors = cgl->Cvar_Get ("cg_teamcolors", "4:5:6", CVAR_ARCHIVE);
  cg_forcecolor = cgl->Cvar_Get ("cg_forcecolor", "0", CVAR_ARCHIVE);
  cg_deadbodydarken = cgl->Cvar_Get ("cg_deadbodydarken", "1", CVAR_ARCHIVE);
  ch_crosshaircolor = cgl->Cvar_Get ("ch_crosshaircolor", "", CVAR_ARCHIVE);
  ch_crosshairpulseitems = cgl->Cvar_Get ("ch_crosshairpulseitems", "1", CVAR_ARCHIVE);
  ch_weaponselectStyle = cgl->Cvar_Get ("ch_weaponselectStyle", "1", CVAR_ARCHIVE);
  ch_weaponselectFadeTime = cgl->Cvar_Get ("ch_weaponselectFadeTime", "1400", CVAR_ARCHIVE);
  ch_weaponselectPosition = cgl->Cvar_Get ("ch_weaponselectPosition", "", CVAR_ARCHIVE);
  ch_weaponselectDrawAll = cgl->Cvar_Get ("ch_weaponselectDrawAll", "0", CVAR_ARCHIVE);
  ch_statusbarStyle = cgl->Cvar_Get ("ch_statusbarStyle", "1", CVAR_ARCHIVE);
  ch_statusbarBackgroundColor = cgl->Cvar_Get ("ch_statusbarBackgroundColor", "-1", CVAR_ARCHIVE);
  ch_centerunscaled = cgl->Cvar_Get ("ch_centerunscaled", "0", CVAR_ARCHIVE);

  cg_shotgunsmokepuff = cgl->Cvar_Get ("cg_shotgunsmokepuff", "1", CVAR_ARCHIVE);
  cg_nomip = cgl->Cvar_Get ("cg_nomip", "0", CVAR_ARCHIVE | CVAR_LATCH);
  cg_nomip2 = cgl->Cvar_Get ("cg_nomip2", "0", CVAR_ARCHIVE | CVAR_LATCH);
  cg_drawspeed = cgl->Cvar_Get ("cg_drawspeed", "0", CVAR_ARCHIVE);
  cg_zoomtime = cgl->Cvar_Get ("cg_zoomtime", "150", CVAR_ARCHIVE);
  cg_zoomsensitivityscale = cgl->Cvar_Get ("cg_zoomsensitivityscale", "1", CVAR_ARCHIVE);
  cg_autoswitchorder = cgl->Cvar_Get ("cg_autoswitchorder", "8 7 6 5 4 3 2 1", CVAR_ARCHIVE);
  cg_noammoswitch = cgl->Cvar_Get ("cg_noammoswitch", "1", CVAR_ARCHIVE);
  cg_noweaponswitchonevent = cgl->Cvar_Get ("cg_noweaponswitchonevent", "0", CVAR_ARCHIVE);

  dev_cg_s_ambient = cgl->Cvar_Get ("dev_cg_s_ambient", "1", CVAR_ARCHIVE);
  cg_grenadecolor = cgl->Cvar_Get ("cg_grenadecolor", "a", CVAR_ARCHIVE);
  dev_cg_nohyperspace = cgl->Cvar_Get ("dev_cg_nohyperspace", "0", CVAR_ARCHIVE);
  // the flag should just be 0 ?
  dev_cg_crosshairhideonfire = cgl->Cvar_Get ("dev_cg_crosshairhideonfire", "0", CVAR_ARCHIVE);
  dev_cg_drawBBox = cgl->Cvar_Get ("dev_cg_drawBBox", "0", CVAR_TEMP);
  dev_cg_noteleporteffect = cgl->Cvar_Get ("dev_cg_noteleporteffect", "1", CVAR_ARCHIVE);
  dev_cg_cheat_timers = cgl->Cvar_Get ("dev_cg_cheat_timers", "0", CVAR_TEMP);

  ci_model = cgl->Cvar_Get ("ci_model", "ranger", CVAR_USERINFO | CVAR_ARCHIVE);
  ci_headmodel = cgl->Cvar_Get ("ci_headmodel", "ranger", CVAR_USERINFO | CVAR_ARCHIVE);
  ci_team_model = cgl->Cvar_Get ("ci_team_model", "visor", CVAR_USERINFO | CVAR_ARCHIVE);
  ci_team_headmodel = cgl->Cvar_Get ("ci_team_headmodel", "visor", CVAR_USERINFO | CVAR_ARCHIVE);
  ci_modelcolor = cgl->Cvar_Get ("ci_modelcolor", "777", CVAR_USERINFO | CVAR_ARCHIVE);
  ci_color1 = cgl->Cvar_Get ("ci_color1", "4", CVAR_USERINFO | CVAR_ARCHIVE);
  ci_color2 = cgl->Cvar_Get ("ci_color2", "5", CVAR_USERINFO | CVAR_ARCHIVE);
  ci_handicap = cgl->Cvar_Get ("ci_handicap", "100", CVAR_USERINFO | CVAR_ARCHIVE);

  modifCounter.forceModel = cg_forceModel->modificationCount;
  modifCounter.enemyModelName = cg_enemymodel->modificationCount;
  modifCounter.teamModelName = cg_teammodel->modificationCount;
  modifCounter.enemyColor = cg_enemycolors->modificationCount;
  modifCounter.teamColor = cg_teamcolors->modificationCount;
  modifCounter.teamChange = cgs.clientinfo[cg.clientNum].team;

  modifCounter.drawTeamOverlay = cg_drawTeamOverlay->modificationCount;

  // let the server know we want teamoverlay infos, via USERINFO string
  if (cg_drawTeamOverlay->integer > 0)
    {
      cgl->Cvar_Set ("teamoverlay", "1");
    }
  else
    {
      cgl->Cvar_Set ("teamoverlay", "0");
    }
}

/*
===================
CG_ForceModelChange
===================
*/
static void
CG_ForceModelChange (bool deferre)
{
  int i;

  for (i = 0; i < cgs.maxclients; i++)
    {
      const char *clientInfo;

      clientInfo = CG_ConfigString (CS_PLAYERS + i);
      if (!clientInfo[0])
	{
	  continue;
	}
      CG_NewClientInfo2 (i, deferre);
    }
}

/*
========================
dev_CG_UpdateModelColors (void)
=========================
*/
void
dev_CG_UpdateModelColors (void)
{
  dev_CG_FillModColorFromString (cg_enemycolors->string, encolmodel);
  dev_CG_FillModColorFromString (cg_teamcolors->string, tcolmodel);
}

/*
=================
CG_UpdateCvars
=================
*/

void
CG_UpdateCvars (void)
{
  // check for modications here

  // If team overlay is on, ask for updates from the server. If it's off,
  // let the server know so we don't receive it
  if (modifCounter.drawTeamOverlay != cg_drawTeamOverlay->modificationCount)
    {
      modifCounter.drawTeamOverlay = cg_drawTeamOverlay->modificationCount;

      if (cg_drawTeamOverlay->integer > 0)
	{
	  cgl->Cvar_Set ("teamoverlay", "1");
	}
      else
	{
	  cgl->Cvar_Set ("teamoverlay", "0");
	}
    }

  // if force model changed
  if (modifCounter.forceModel != cg_forceModel->modificationCount)
    {
      modifCounter.forceModel = cg_forceModel->modificationCount;
      CG_ForceModelChange (true);
    }
  if (modifCounter.enemyModelName != cg_enemymodel->modificationCount)
    {
      modifCounter.enemyModelName = cg_enemymodel->modificationCount;
      CG_ForceModelChange (true);
    }
  if (modifCounter.teamModelName != cg_teammodel->modificationCount)
    {
      modifCounter.teamModelName = cg_teammodel->modificationCount;
      CG_ForceModelChange (true);
    }
  if (modifCounter.teamChange != cgs.clientinfo[cg.clientNum].team)
    {
      modifCounter.teamChange = cgs.clientinfo[cg.clientNum].team;
      CG_ForceModelChange (false);
    }

  if (modifCounter.enemyColor != cg_enemycolors->modificationCount)
    {
      modifCounter.enemyColor = cg_enemycolors->modificationCount;
      dev_CG_UpdateModelColors ();	// put a flag for whether it's enemy or team
    }
  if (modifCounter.teamColor != cg_teamcolors->modificationCount)
    {
      modifCounter.teamColor = cg_teamcolors->modificationCount;
      dev_CG_UpdateModelColors ();
    }
}

int
CG_CrosshairPlayer (void)
{
  if (cg.time > (cg.crosshairClientTime + 1000))
    {
      return -1;
    }
  return cg.crosshairClientNum;
}

int
CG_LastAttacker (void)
{
  if (!cg.attackerTime)
    {
      return -1;
    }
  return cg.snap->ps.persistant[PERS_ATTACKER];
}

//========================================================================

/*
=================
CG_RegisterItemSounds

The server says this item is used on this level
=================
*/
static void
CG_RegisterItemSounds (int itemNum)
{
  const gitem_t *item;
  char data[MAX_QPATH];
  char *s, *start;
  int len;

  item = &bg_itemlist[itemNum];

  if (item->pickup_sound)
    {
      cgl->snd->RegisterSound (item->pickup_sound);
    }

  // parse the space seperated precache string for other media
  s = item->sounds;
  if (!s || !s[0])
    return;

  while (*s)
    {
      start = s;
      while (*s && (*s != ' '))
	{
	  s++;
	}

      len = s - start;
      if ((len >= MAX_QPATH) || (len < 5))
	{
	  CG_Error ("PrecacheItem: %s has bad precache string",
		    item->classname);
	  return;
	}
      memcpy (data, start, len);
      data[len] = '\0';
      // this test is to continue registering other sounds
      // item->sounds : "sound1.wav sound2.wav ..."
      if (*s)
	{
	  s++;
	}

      if (!strcmp (data + len - 3, "wav"))
	{
	  cgl->snd->RegisterSound (data);
	}
    }
}

/*
=================
CG_RegisterSounds

called during a precache command
=================
*/
static void
CG_RegisterSounds (void)
{
  int i;
  char items[MAX_ITEMS + 1];
  char name[MAX_QPATH];
  const char *soundName;

  // voice commands

  cgs.media.oneMinuteSound =
    cgl->snd->RegisterSound ("sound/feedback/1_minute.wav");
  cgs.media.fiveMinuteSound =
    cgl->snd->RegisterSound ("sound/feedback/5_minute.wav");
  cgs.media.suddenDeathSound =
    cgl->snd->RegisterSound ("sound/feedback/sudden_death.wav");
  cgs.media.oneFragSound =
    cgl->snd->RegisterSound ("sound/feedback/1_frag.wav");
  cgs.media.twoFragSound =
    cgl->snd->RegisterSound ("sound/feedback/2_frags.wav");
  cgs.media.threeFragSound =
    cgl->snd->RegisterSound ("sound/feedback/3_frags.wav");
  cgs.media.count3Sound =
    cgl->snd->RegisterSound ("sound/feedback/three.wav");
  cgs.media.count2Sound =
    cgl->snd->RegisterSound ("sound/feedback/two.wav");
  cgs.media.count1Sound =
    cgl->snd->RegisterSound ("sound/feedback/one.wav");
  cgs.media.countFightSound =
    cgl->snd->RegisterSound ("sound/feedback/fight.wav");
  cgs.media.countPrepareSound =
    cgl->snd->RegisterSound ("sound/feedback/prepare.wav");

  if (BG_GametypeIsTeam (cgs.gametype) || cg_buildScript->integer)
    {
      cgs.media.redLeadsSound =
	cgl->snd->RegisterSound ("sound/feedback/redleads.wav");
      cgs.media.blueLeadsSound =
	cgl->snd->RegisterSound ("sound/feedback/blueleads.wav");
      cgs.media.teamsTiedSound =
	cgl->snd->RegisterSound ("sound/feedback/teamstied.wav");
      cgs.media.hitTeamSound =
	cgl->snd->RegisterSound ("sound/feedback/hit_teammate.wav");

      if (cgs.gametype == GT_CTF || cg_buildScript->integer)
	{
	  cgs.media.redScoredSound =
	    cgl->snd->RegisterSound ("sound/teamplay/voc_red_scores.wav");
	  cgs.media.blueScoredSound =
	    cgl->snd->RegisterSound ("sound/teamplay/voc_blue_scores.wav");

	  cgs.media.captureAwardSound =
	    cgl->snd->RegisterSound ("sound/teamplay/flagcapture_yourteam.wav");
	  cgs.media.captureYourTeamSound =
	    cgl->snd->RegisterSound ("sound/teamplay/flagcapture_yourteam.wav");
	  cgs.media.captureOpponentSound =
	    cgl->snd->RegisterSound ("sound/teamplay/flagcapture_opponent.wav");

	  cgs.media.returnYourTeamSound =
	    cgl->snd->RegisterSound ("sound/teamplay/flagreturn_yourteam.wav");
	  cgs.media.returnOpponentSound =
	    cgl->snd->RegisterSound ("sound/teamplay/flagreturn_opponent.wav");

	  cgs.media.takenYourTeamSound =
	    cgl->snd->RegisterSound ("sound/teamplay/flagtaken_yourteam.wav");
	  cgs.media.takenOpponentSound =
	    cgl->snd->RegisterSound ("sound/teamplay/flagtaken_opponent.wav");

	  cgs.media.redFlagReturnedSound =
	    cgl->snd->RegisterSound ("sound/teamplay/voc_red_returned.wav");
	  cgs.media.blueFlagReturnedSound =
	    cgl->snd->RegisterSound ("sound/teamplay/voc_blue_returned.wav");
	  cgs.media.enemyTookYourFlagSound =
	    cgl->snd->RegisterSound ("sound/teamplay/voc_enemy_flag.wav");
	  cgs.media.yourTeamTookEnemyFlagSound =
	    cgl->snd->RegisterSound ("sound/teamplay/voc_team_flag.wav");

	  cgs.media.youHaveFlagSound =
	    cgl->snd->RegisterSound ("sound/teamplay/voc_you_flag.wav");
	  cgs.media.holyShitSound =
	    cgl->snd->RegisterSound ("sound/feedback/voc_holyshit.wav");

	  cgs.media.yourTeamTookTheFlagSound =
	    cgl->snd->RegisterSound ("sound/teamplay/voc_team_1flag.wav");
	  cgs.media.enemyTookTheFlagSound =
	    cgl->snd->RegisterSound ("sound/teamplay/voc_enemy_1flag.wav");
	}
    }

  cgs.media.tracerSound =
    cgl->snd->RegisterSound ("sound/weapons/machinegun/buletby1.wav");
  cgs.media.selectSound =
    cgl->snd->RegisterSound ("sound/weapons/change.wav");
  cgs.media.wearOffSound =
    cgl->snd->RegisterSound ("sound/items/wearoff.wav");
  cgs.media.useNothingSound =
    cgl->snd->RegisterSound ("sound/items/use_nothing.wav");
  cgs.media.gibSound =
    cgl->snd->RegisterSound ("sound/player/gibsplt1.wav");
  cgs.media.gibBounce1Sound =
    cgl->snd->RegisterSound ("sound/player/gibimp1.wav");
  cgs.media.gibBounce2Sound =
    cgl->snd->RegisterSound ("sound/player/gibimp2.wav");
  cgs.media.gibBounce3Sound =
    cgl->snd->RegisterSound ("sound/player/gibimp3.wav");

  cgs.media.teleInSound =
    cgl->snd->RegisterSound ("sound/world/telein.wav");
  cgs.media.teleOutSound =
    cgl->snd->RegisterSound ("sound/world/teleout.wav");
  cgs.media.respawnSound =
    cgl->snd->RegisterSound ("sound/items/respawn1.wav");

  cgs.media.noAmmoSound =
    cgl->snd->RegisterSound ("sound/weapons/noammo.wav");

  cgs.media.talkSound =
    cgl->snd->RegisterSound ("sound/player/talk.wav");
  cgs.media.landSound =
    cgl->snd->RegisterSound ("sound/player/land1.wav");
  cgs.media.collideSound =
    cgl->snd->RegisterSound ("sound/player/land1.wav");

  cgs.media.hitSound =
    cgl->snd->RegisterSound ("sound/feedback/hit.wav");

  cgs.media.impressiveSound =
    cgl->snd->RegisterSound ("sound/feedback/impressive.wav");
  cgs.media.excellentSound =
    cgl->snd->RegisterSound ("sound/feedback/excellent.wav");
  cgs.media.deniedSound =
    cgl->snd->RegisterSound ("sound/feedback/denied.wav");
  cgs.media.humiliationSound =
    cgl->snd->RegisterSound ("sound/feedback/humiliation.wav");
  cgs.media.assistSound =
    cgl->snd->RegisterSound ("sound/feedback/assist.wav");
  cgs.media.defendSound =
    cgl->snd->RegisterSound ("sound/feedback/defense.wav");

  cgs.media.takenLeadSound =
    cgl->snd->RegisterSound ("sound/feedback/takenlead.wav");
  cgs.media.tiedLeadSound =
    cgl->snd->RegisterSound ("sound/feedback/tiedlead.wav");
  cgs.media.lostLeadSound =
    cgl->snd->RegisterSound ("sound/feedback/lostlead.wav");
  cgs.media.watrInSound =
    cgl->snd->RegisterSound ("sound/player/watr_in.wav");
  cgs.media.watrOutSound =
    cgl->snd->RegisterSound ("sound/player/watr_out.wav");
  cgs.media.watrUnSound =
    cgl->snd->RegisterSound ("sound/player/watr_un.wav");

  cgs.media.jumpPadSound =
    cgl->snd->RegisterSound ("sound/world/jumppad.wav");

  for (i = 0; i < 4; i++)
    {
      Com_sprintf (name, sizeof (name), "sound/player/footsteps/step%i.wav", i + 1);
      cgs.media.footsteps[FOOTSTEP_NORMAL][i] = cgl->snd->RegisterSound (name);

      Com_sprintf (name, sizeof (name), "sound/player/footsteps/boot%i.wav", i + 1);
      cgs.media.footsteps[FOOTSTEP_BOOT][i] = cgl->snd->RegisterSound (name);

      Com_sprintf (name, sizeof (name), "sound/player/footsteps/flesh%i.wav", i + 1);
      cgs.media.footsteps[FOOTSTEP_FLESH][i] = cgl->snd->RegisterSound (name);

      Com_sprintf (name, sizeof (name), "sound/player/footsteps/mech%i.wav", i + 1);
      cgs.media.footsteps[FOOTSTEP_MECH][i] = cgl->snd->RegisterSound (name);

      Com_sprintf (name, sizeof (name), "sound/player/footsteps/energy%i.wav", i + 1);
      cgs.media.footsteps[FOOTSTEP_ENERGY][i] = cgl->snd->RegisterSound (name);

      Com_sprintf (name, sizeof (name), "sound/player/footsteps/splash%i.wav", i + 1);
      cgs.media.footsteps[FOOTSTEP_SPLASH][i] = cgl->snd->RegisterSound (name);

      Com_sprintf (name, sizeof (name), "sound/player/footsteps/clank%i.wav", i + 1);
      cgs.media.footsteps[FOOTSTEP_METAL][i] = cgl->snd->RegisterSound (name);
    }

  // only register the items that the server says we need
  Q_strncpyz (items, CG_ConfigString (CS_ITEMS), sizeof(items));

  for (i = 1; i < bg_numItems; i++)
    {
      if ((items[i ] == '1') || cg_buildScript->integer)
	{
	  CG_RegisterItemSounds (i);
	}
    }

  // what about the i = 0 case ?
  for (i = 1; i < MAX_SOUNDS; i++)
    {
      soundName = CG_ConfigString (CS_SOUNDS + i);
      if (!soundName[0])
	{
	  break;
	}
      if (soundName[0] == '*')
	{
	  continue;		// custom sound
	}
      cgs.gameSounds[i] = cgl->snd->RegisterSound (soundName);
    }

  // FIXME: only register needed items
  // issue : manage developper/cheat on mode where you can spawn these items
  // on maps where there shouldn't be some items
  cgs.media.flightSound =
    cgl->snd->RegisterSound ("sound/items/flight.wav");
  cgs.media.medkitSound =
    cgl->snd->RegisterSound ("sound/items/use_medkit.wav");
  cgs.media.quadSound =
    cgl->snd->RegisterSound ("sound/items/damage3.wav");
  // TODO : move this to cg_registerweapon
  cgs.media.sfx_ric1 =
    cgl->snd->RegisterSound ("sound/weapons/machinegun/ric1.wav");
  cgs.media.sfx_ric2 =
    cgl->snd->RegisterSound ("sound/weapons/machinegun/ric2.wav");
  cgs.media.sfx_ric3 =
    cgl->snd->RegisterSound ("sound/weapons/machinegun/ric3.wav");
  cgs.media.sfx_railg =
    cgl->snd->RegisterSound ("sound/weapons/railgun/railgf1a.wav");
  cgs.media.sfx_rockexp =
    cgl->snd->RegisterSound ("sound/weapons/rocket/rocklx1a.wav");
  cgs.media.sfx_plasmaexp =
    cgl->snd->RegisterSound ("sound/weapons/plasma/plasmx1a.wav");

  cgs.media.regenSound =
    cgl->snd->RegisterSound ("sound/items/regen.wav");
  cgs.media.protectSound =
    cgl->snd->RegisterSound ("sound/items/protect3.wav");
  cgs.media.n_healthSound =
    cgl->snd->RegisterSound ("sound/items/n_health.wav");
  cgs.media.hgrenb1aSound =
    cgl->snd->RegisterSound ("sound/weapons/grenade/hgrenb1a.wav");
  cgs.media.hgrenb2aSound =
    cgl->snd->RegisterSound ("sound/weapons/grenade/hgrenb2a.wav");
}

//===================================================================================

/*
=================
CG_RegisterGraphics

This function may execute for a couple of minutes with a slow disk.
=================
*/
static void
CG_RegisterGraphics (void)
{
  int i;
  char items[MAX_ITEMS + 1];
  static const char *sb_nums[11] = {
    "gfx/2d/numbers/zero_32b",
    "gfx/2d/numbers/one_32b",
    "gfx/2d/numbers/two_32b",
    "gfx/2d/numbers/three_32b",
    "gfx/2d/numbers/four_32b",
    "gfx/2d/numbers/five_32b",
    "gfx/2d/numbers/six_32b",
    "gfx/2d/numbers/seven_32b",
    "gfx/2d/numbers/eight_32b",
    "gfx/2d/numbers/nine_32b",
    "gfx/2d/numbers/minus_32b",
  };

  // clear any references to old media
  Com_Memset (&cg.refdef, 0, sizeof (cg.refdef));
  cgl->R_ClearScene ();

  CG_LoadingString (cgs.mapname);

  cgl->R_LoadWorldMap (cgs.mapname);

  // precache status bar pics
  CG_LoadingString ("game media");

  for (i = 0; i < 11; i++)
    {
      cgs.media.numberShaders[i] = cgl->R_RegisterShader (sb_nums[i]);
    }

  // no bots atm
  //cgs.media.botSkillShaders[0] = cgl->R_RegisterShaderNoMip ("menu/art/skill1.tga");
  //cgs.media.botSkillShaders[1] = cgl->R_RegisterShaderNoMip ("menu/art/skill2.tga");
  //cgs.media.botSkillShaders[2] = cgl->R_RegisterShaderNoMip ("menu/art/skill3.tga");
  //cgs.media.botSkillShaders[3] = cgl->R_RegisterShaderNoMip ("menu/art/skill4.tga");
  //cgs.media.botSkillShaders[4] = cgl->R_RegisterShaderNoMip ("menu/art/skill5.tga");

  // generic shaders, those that are always needed
  cgs.media.viewBloodShader = cgl->R_RegisterShader ("viewBloodBlend");

  cgs.media.deferShader = CG_R_RegisterShaderNoMip ("gfx/2d/defer.tga");

  if (cg_nomip->integer & NM_SMOKE)
    {
      cgs.media.smokePuffShader = CG_R_RegisterShaderNoMip ("smokePuff");
    }
  else
    {
      cgs.media.smokePuffShader = cgl->R_RegisterShader ("smokePuff");
    }

  if (cg_nomip->integer & NM_SHOTGUN_SMOKE)
    {
      cgs.media.shotgunSmokePuffShader = CG_R_RegisterShaderNoMip ("shotgunSmokePuff");
    }
  else
    {
      cgs.media.shotgunSmokePuffShader = cgl->R_RegisterShader ("shotgunSmokePuff");
    }

  cgs.media.bloodTrailShader = cgl->R_RegisterShader ("bloodTrail");
  cgs.media.lagometerShader = cgl->R_RegisterShader ("lagometer");
  cgs.media.connectionShader = cgl->R_RegisterShader ("disconnected");

  if (cg_nomip->integer & NM_SMOKE)
    cgs.media.waterBubbleShader = CG_R_RegisterShaderNoMip ("waterBubble");
  else
    cgs.media.waterBubbleShader = cgl->R_RegisterShader ("waterBubble");

  cgs.media.tracerShader = cgl->R_RegisterShader ("gfx/misc/tracer");
  cgs.media.selectShader = cgl->R_RegisterShader ("gfx/2d/select");
  cgs.media.yellowboxShader = cgl->R_RegisterShader ("gfx/2d/boxyellowborders");

  for (i = 0; i < NUM_CROSSHAIRS; i++)
    {
      cgs.media.crosshairShader[i] =
	cgl->R_RegisterShader (va ("gfx/2d/crosshair%c", 'a' + i));
    }

  cgs.media.backTileShader = cgl->R_RegisterShader ("gfx/2d/backtile");
  cgs.media.noammoShader = CG_R_RegisterShaderNoMip ("icons/noammo");

  // powerup shaders
  cgs.media.quadShader = cgl->R_RegisterShader ("powerups/quad");
  cgs.media.quadWeaponShader = cgl->R_RegisterShader ("powerups/quadWeapon");
  cgs.media.battleSuitShader = cgl->R_RegisterShader ("powerups/battleSuit");
  cgs.media.battleWeaponShader = cgl->R_RegisterShader ("powerups/battleWeapon");
  cgs.media.invisShader = cgl->R_RegisterShader ("powerups/invisibility");
  cgs.media.regenShader = cgl->R_RegisterShader ("powerups/regen");
  if (cg_nomip->integer & NM_SMOKE)
    {
      cgs.media.hastePuffShader = CG_R_RegisterShaderNoMip ("hasteSmokePuff");
    }
  else
    {
      cgs.media.hastePuffShader = cgl->R_RegisterShader ("hasteSmokePuff");
    }

  // hud, no anymore team specific, it can be colorized
  cgs.media.teamStatusBar = cgl->R_RegisterShader ("gfx/2d/colorbar.tga");

  // game specific
  if (BG_GametypeIsTeam (cgs.gametype) || cg_buildScript->integer)
    {
      cgs.media.friendShader = cgl->R_RegisterShader ("sprites/foe");
      cgs.media.redQuadShader = cgl->R_RegisterShader ("powerups/blueflag");	// obvious !

      if (cgs.gametype == GT_CTF || cg_buildScript->integer)
	{
	  cgs.media.redFlagModel = cgl->R_RegisterModel ("models/flags/r_flag.md3");
	  cgs.media.blueFlagModel = cgl->R_RegisterModel ("models/flags/b_flag.md3");
	  cgs.media.redFlagShader[0] = CG_R_RegisterShaderNoMip ("icons/iconf_red1");
	  cgs.media.redFlagShader[1] = CG_R_RegisterShaderNoMip ("icons/iconf_red2");
	  cgs.media.redFlagShader[2] = CG_R_RegisterShaderNoMip ("icons/iconf_red3");
	  cgs.media.blueFlagShader[0] = CG_R_RegisterShaderNoMip ("icons/iconf_blu1");
	  cgs.media.blueFlagShader[1] = CG_R_RegisterShaderNoMip ("icons/iconf_blu2");
	  cgs.media.blueFlagShader[2] = CG_R_RegisterShaderNoMip ("icons/iconf_blu3");
	}
    }

  // NOTE : this is requested for hud display, nothing related with items
  cgs.media.armorIconLA = CG_R_RegisterShaderNoMip ("icons/iconr_green");
  cgs.media.armorIconYA = CG_R_RegisterShaderNoMip ("icons/iconr_yellow");
  cgs.media.armorIconRA = CG_R_RegisterShaderNoMip ("icons/iconr_red");

  cgs.media.healthIcon = CG_R_RegisterShaderNoMip ("icons/iconh_yellow");

  cgs.media.machinegunBrassModel =
    cgl->R_RegisterModel ("models/weapons2/shells/m_shell.md3");
  cgs.media.shotgunBrassModel =
    cgl->R_RegisterModel ("models/weapons2/shells/s_shell.md3");

  cgs.media.gibAbdomen = cgl->R_RegisterModel ("models/gibs/abdomen.md3");
  cgs.media.gibArm = cgl->R_RegisterModel ("models/gibs/arm.md3");
  cgs.media.gibChest = cgl->R_RegisterModel ("models/gibs/chest.md3");
  cgs.media.gibFist = cgl->R_RegisterModel ("models/gibs/fist.md3");
  cgs.media.gibFoot = cgl->R_RegisterModel ("models/gibs/foot.md3");
  cgs.media.gibForearm = cgl->R_RegisterModel ("models/gibs/forearm.md3");
  cgs.media.gibIntestine = cgl->R_RegisterModel ("models/gibs/intestine.md3");
  cgs.media.gibLeg = cgl->R_RegisterModel ("models/gibs/leg.md3");
  cgs.media.gibSkull = cgl->R_RegisterModel ("models/gibs/skull.md3");
  cgs.media.gibBrain = cgl->R_RegisterModel ("models/gibs/brain.md3");

  // try to load an alternate balloon shader if it exists,
  // otherwise keep the regular one, shaders can't be overwritten atm
  cgs.media.balloonShader = cgl->R_RegisterShader ("sprites/balloon2");
  if (cgs.media.balloonShader == 0)
    {
      cgs.media.balloonShader = cgl->R_RegisterShader ("sprites/balloon");
    }

  if (cg_nomip->integer & NM_BLOOD_IMPACT)
    cgs.media.bloodExplosionShader = CG_R_RegisterShaderNoMip ("bloodExplosion");
  else
    cgs.media.bloodExplosionShader = cgl->R_RegisterShader ("bloodExplosion");

  if (cg_nomip->integer & NM_BULLET_IMPACT)
    cgs.media.bulletFlashModel = cgl->R_RegisterModelExt ("models/weaphits/bullet.md3",
							  SHAD_NOPICMIP);
  else
    cgs.media.bulletFlashModel = cgl->R_RegisterModel ("models/weaphits/bullet.md3");

  cgs.media.ringFlashModel = cgl->R_RegisterModel ("models/weaphits/ring02.md3");
  cgs.media.dishFlashModel = cgl->R_RegisterModel ("models/weaphits/boom01.md3");

  if (cg_nomip2->integer & NM2_TELEPORT_EFFECT)
    {
      // could use usual register function, it doesn't seem the model as attached shaders
      cgs.media.teleportEffectModel = cgl->R_RegisterModelExt ("models/misc/telep.md3",
							     SHAD_NOPICMIP);
      cgs.media.teleportEffectShader = CG_R_RegisterShaderNoMip ("teleportEffect");
    }
  else
    {
      cgs.media.teleportEffectShader = cgl->R_RegisterShader ("teleportEffect");
      cgs.media.teleportEffectModel = cgl->R_RegisterModel ("models/misc/telep.md3");
    }

  cgs.media.medalImpressive = CG_R_RegisterShaderNoMip ("medal_impressive");
  cgs.media.medalExcellent = CG_R_RegisterShaderNoMip ("medal_excellent");
  cgs.media.medalGauntlet = CG_R_RegisterShaderNoMip ("medal_gauntlet");
  cgs.media.medalDefend = CG_R_RegisterShaderNoMip ("medal_defend");
  cgs.media.medalAssist = CG_R_RegisterShaderNoMip ("medal_assist");
  cgs.media.medalCapture = CG_R_RegisterShaderNoMip ("medal_capture");

  Com_Memset (cg_items, 0, sizeof (cg_items));
  Com_Memset (cg_weapons, 0, sizeof (cg_weapons));

  // only register the items that the server says we need
  Q_strncpyz (items, CG_ConfigString (CS_ITEMS), sizeof (items));

  for (i = 1; i < bg_numItems; i++)
    {
      if (items[i] == '1' || cg_buildScript->integer)
	{
	  CG_LoadingItem (i);
	  CG_RegisterItemVisuals (i);
	}
    }

  // wall marks
  if (cg_nomip->integer & NM_MARKS)
    {
      cgs.media.burnMarkShader = CG_R_RegisterShaderNoMip ("gfx/damage/burn_med_mrk");
      cgs.media.holeMarkShader = CG_R_RegisterShaderNoMip ("gfx/damage/hole_lg_mrk");
      cgs.media.shadowMarkShader = CG_R_RegisterShaderNoMip ("markShadow");
      cgs.media.wakeMarkShader = CG_R_RegisterShaderNoMip ("wake");
      cgs.media.bloodMarkShader = CG_R_RegisterShaderNoMip ("bloodMark");
    }
  else
    {
      cgs.media.burnMarkShader = cgl->R_RegisterShader ("gfx/damage/burn_med_mrk");
      cgs.media.holeMarkShader = cgl->R_RegisterShader ("gfx/damage/hole_lg_mrk");
      cgs.media.shadowMarkShader = cgl->R_RegisterShader ("markShadow");
      cgs.media.wakeMarkShader = cgl->R_RegisterShader ("wake");
      cgs.media.bloodMarkShader = cgl->R_RegisterShader ("bloodMark");
    }

  if (cg_nomip->integer & NM_BULLET_IMPACT)
    {
      cgs.media.bulletMarkShader = CG_R_RegisterShaderNoMip ("gfx/damage/bullet_mrk");
    }
  else
    {
      cgs.media.bulletMarkShader = cgl->R_RegisterShader ("gfx/damage/bullet_mrk");
    }

  // register this one after items, so we won't affect nomip status...
  cgs.media.armorModel = cgl->R_RegisterModel ("models/powerups/armor/armor_yel.md3");

  // register the inline models
  cgs.numInlineModels = cgl->CM_NumInlineModels ();

  for (i = 1; i < cgs.numInlineModels; i++)
    {
      char name[10];
      vec3_t mins, maxs;
      int j;

      Com_sprintf (name, sizeof (name), "*%i", i);

      cgs.inlineDrawModel[i] = cgl->R_RegisterModel (name);

      cgl->R_ModelBounds (cgs.inlineDrawModel[i], mins, maxs);
      for (j = 0; j < 3; j++)
	{
	  cgs.inlineModelMidpoints[i][j] = mins[j] + 0.5 * (maxs[j] - mins[j]);
	}
    }

  // register all the server specified models
  for (i = 1; i < MAX_MODELS; i++)
    {
      const char *modelName;

      modelName = CG_ConfigString (CS_MODELS + i);
      if (!modelName[0])
	{
	  break;
	}
      cgs.gameModels[i] = cgl->R_RegisterModel (modelName);
    }
}

/*
=======================
CG_BuildSpectatorString

=======================
*/
void
CG_BuildSpectatorString (void)
{
  int i;

  cg.spectatorList[0] = 0;
  // look in configstring for sv_maxclients
  for (i = 0; i < cgs.maxclients; i++)
    {
      if (cgs.clientinfo[i].infoValid
	  && (cgs.clientinfo[i].team == TEAM_SPECTATOR))
	{
	  Q_strcat (cg.spectatorList, sizeof (cg.spectatorList),
		    va ("%s     ", cgs.clientinfo[i].name));
	}
    }

  i = strlen (cg.spectatorList);
  if (i != cg.spectatorLen)
    {
      cg.spectatorLen = i;
      cg.spectatorWidth = -1;
    }
}

/*
===================
CG_RegisterClients
===================
*/
static void
CG_RegisterClients (void)
{
  int i;

  CG_LoadingClient (cg.clientNum);
  CG_NewClientInfo2 (cg.clientNum, false);

  for (i = 0; i < cgs.maxclients; i++)
    {
      const char *clientInfo;

      if (cg.clientNum == i)
	{
	  continue;
	}

      clientInfo = CG_ConfigString (CS_PLAYERS + i);
      if (!clientInfo[0])
	{
	  continue;
	}

      CG_LoadingClient (i);
      CG_NewClientInfo2 (i, false);
    }

  CG_BuildSpectatorString ();
}

//===========================================================================

/*
=================
CG_ConfigString
=================
*/
const char *
CG_ConfigString (int index)
{
  if (index < 0 || index >= MAX_CONFIGSTRINGS)
    {
      CG_Error ("CG_ConfigString: bad index: %i", index);
    }
  return cgs.gameState.stringData + cgs.gameState.stringOffsets[index];
}

//==================================================================

/*
======================
CG_StartMusic

======================
*/
void
CG_StartMusic (void)
{
  char *s;
  char parm1[MAX_QPATH], parm2[MAX_QPATH];

  // start the background music
  s = (char *) CG_ConfigString (CS_MUSIC);
  Q_strncpyz (parm1, COM_Parse (&s), sizeof (parm1));
  Q_strncpyz (parm2, COM_Parse (&s), sizeof (parm2));

  cgl->snd->StartBackgroundTrack (parm1, parm2);
}

/*
=================
CG_Init

Called after every level change or subsystem restart
Will perform callbacks to make the loading info screen update.
=================
*/
char *
CG_Init (int serverMessageNum, int serverCommandSequence, int clientNum)
{
  const char *s;

  CG_Printf ("------- CGame Initialization -------\n");

  // clear everything
  Com_Memset (&cgs, 0, sizeof (cgs));
  Com_Memset (&cg, 0, sizeof (cg));
  Com_Memset (cg_entities, 0, sizeof (cg_entities));
  Com_Memset (cg_weapons, 0, sizeof (cg_weapons));
  Com_Memset (cg_items, 0, sizeof (cg_items));
  Com_Memset (&modifCounter, 0, sizeof (modifCounter));

  cg.clientNum = clientNum;

  cgs.processedSnapshotNum = serverMessageNum;
  cgs.serverCommandSequence = serverCommandSequence;

  // load a few needed things before we do any screen updates
  cgs.media.charsetShader = cgl->R_RegisterShader ("gfx/2d/bigchars");
  cgs.media.whiteShader = cgl->R_RegisterShader ("white");
  cgs.media.charsetProp = CG_R_RegisterShaderNoMip ("menu/art/font1_prop.tga");
  cgs.media.charsetPropGlow = CG_R_RegisterShaderNoMip ("menu/art/font1_prop_glo.tga");
  cgs.media.charsetPropB = CG_R_RegisterShaderNoMip ("menu/art/font2_prop.tga");

  CG_InitCvars ();

  CG_InitConsoleCommands ();

  // cg_spawn should hangle this
  //cg.weaponSelect = WP_MACHINEGUN;

  // get the rendering configuration from the client system
  cgl->GetGlconfig (&cgs.glconfig);
  cgs.screenXScale = cgs.glconfig.vidWidth / 640.0;
  cgs.screenYScale = cgs.glconfig.vidHeight / 480.0;
  // so that we see the loading screen as the ratio is not yet computed
  cg.hudscale = 1.0f;

  // get the gamestate from the client system (configstrings)
  cgl->GetGameState (&cgs.gameState);

  // check version
  s = CG_ConfigString (CS_GAME_VERSION);
  if (strcmp (s, GAME_VERSION))
    {
      CG_Error ("Client/Server game mismatch: %s/%s", GAME_VERSION, s);
    }

  CG_ParseServerinfo ();

  // load the new map
  CG_LoadingString ("collision map");

  cgl->CM_LoadMap (cgs.mapname);


  cg.loading = true;		// force players to load instead of defer

  CG_LoadingString ("sounds");

  CG_RegisterSounds ();

  CG_LoadingString ("graphics");

  CG_RegisterGraphics ();

  CG_LoadingString ("clients");

  CG_RegisterClients ();	// if low on memory, some clients will be deferred


  cg.loading = false;		// future players will be deferred

  CG_InitLocalEntities ();

  CG_InitMarkPolys ();

  // remove the last loading update
  cg.infoScreenText[0] = '\0';

  // Make sure we have update values (scores)
  CG_SetConfigValues ();

  CG_StartMusic ();

  CG_LoadingString ("");


  cgl->snd->ClearLoopingSounds (true);

  return NULL;
}

/*
=================
CG_Shutdown

Called before every level change or subsystem restart
=================
*/
void
CG_Shutdown (void)
{
  // some mods may need to do cleanup work here,
  // like closing files or archiving session data
  CG_RemoveConsoleCommands ();
}

/*
==================
CG_EventHandling
==================
 type 0 - no event handling
      1 - team menu
      2 - hud editor
*/
void
CG_EventHandling (int type)
{
}

void
CG_KeyEvent (int key, bool down)
{
}

void
CG_MouseEvent (int x, int y)
{
}

/*
=================
GetCGameAPI

resolving functions for cgame import and export
return a valid cgamelib_export_t point
=================
*/
cgamelib_export_t *
GetCGameAPI (int version, cgamelib_import_t * cgimp)
{
  // no way to communicate with the client, stop here
  if (cgimp == NULL)
    {
      return NULL;
    }

  cgl = cgimp;

  // we can start talking from now
  CG_Printf ("GetGameAPI () : Loaded Server API in Game\n");

  // setup the print/error functions for qshared
  q_shared_import_t q_shared_imp;
  q_shared_imp.print = cgl->Printf;
  q_shared_imp.error = cgl->Error;

  Q_Set_Interface (&q_shared_imp);

  // export some functions the client will need to run the cgame
  Com_Memset (&cge, 0, sizeof (cge));

  cge.init = CG_Init;
  cge.shutdown = CG_Shutdown;
  cge.console_command = CG_ConsoleCommand;
  cge.draw_active_frame = CG_DrawActiveFrame;
  cge.process_active_frame = CG_ProcessActiveFrame;
  cge.crosshair_player = CG_CrosshairPlayer;
  cge.last_attacker = CG_LastAttacker;
  cge.keyevent = CG_KeyEvent;
  cge.mouseevent = CG_MouseEvent;
  cge.event_handling = CG_EventHandling;

  return &cge;
}

/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
//
// cg_weapons.c -- events and effects dealing with weapons
#include "cg_local.h"

// temp ? unify player related defines in a header ?
// the game one is defined in bg_pmove.c
#define LOCALRAILGUN_RELOADTIME 1500

/*
==========================
CG_MachineGunEjectBrass
==========================
*/
static void
CG_MachineGunEjectBrass (centity_t * cent)
{
  localEntity_t *le;
  refEntity_t *re;
  vec3_t velocity, xvelocity;
  vec3_t offset, xoffset;
  float waterScale = 1.0f;
  vec3_t v[3];

  if (cg_brassTime->integer <= 0)
    {
      return;
    }

  le = CG_AllocLocalEntity ();
  re = &le->refEntity;

  velocity[0] = 0;
  velocity[1] = -50 + 40 * crandom ();	// -> this direction
  velocity[2] = 100 + 50 * crandom ();

  le->leType = LE_FRAGMENT;
  le->startTime = cg.levelTime;
  le->endTime = (le->startTime + cg_brassTime->integer
		 + (cg_brassTime->integer / 4) * random ());

  le->pos.trType = TR_GRAVITY;
  le->pos.trTime = cg.levelTime - (rand () & 15);

  AnglesToAxis (cent->lerpAngles, v);

  offset[0] = 8;
  offset[1] = -4;
  offset[2] = 24;

  xoffset[0] = offset[0] * v[0][0] + offset[1] * v[1][0] + offset[2] * v[2][0];
  xoffset[1] = offset[0] * v[0][1] + offset[1] * v[1][1] + offset[2] * v[2][1];
  xoffset[2] = offset[0] * v[0][2] + offset[1] * v[1][2] + offset[2] * v[2][2];
  VectorAdd (cent->lerpOrigin, xoffset, re->origin);

  VectorCopy (re->origin, le->pos.trBase);

  if (CG_PointContents (re->origin, -1) & CONTENTS_WATER)
    {
      waterScale = 0.10f;
    }

  xvelocity[0] = velocity[0] * v[0][0] + velocity[1] * v[1][0] + velocity[2] * v[2][0];
  xvelocity[1] = velocity[0] * v[0][1] + velocity[1] * v[1][1] + velocity[2] * v[2][1];
  xvelocity[2] = velocity[0] * v[0][2] + velocity[1] * v[1][2] + velocity[2] * v[2][2];
  VectorScale (xvelocity, waterScale, le->pos.trDelta);

  AxisCopy (axisDefault, re->axis);
  re->hModel = cgs.media.machinegunBrassModel;

  le->bounceFactor = 0.4 * waterScale;

  le->angles.trType = TR_LINEAR;
  le->angles.trTime = cg.levelTime;
  le->angles.trBase[0] = rand () & 31;
  le->angles.trBase[1] = rand () & 31;
  le->angles.trBase[2] = rand () & 31;
  le->angles.trDelta[0] = 2;
  le->angles.trDelta[1] = 1;
  le->angles.trDelta[2] = 0;

  le->leFlags = LEF_TUMBLE;
  le->leBounceSoundType = LEBS_BRASS;
  le->leMarkType = LEMT_NONE;
}

/*
==========================
CG_ShotgunEjectBrass
==========================
*/
static void
CG_ShotgunEjectBrass (centity_t * cent)
{
  localEntity_t *le;
  refEntity_t *re;
  vec3_t velocity, xvelocity;
  vec3_t offset, xoffset;
  vec3_t v[3];
  float waterScale = 1.0f;
  int i;

  if (cg_brassTime->integer <= 0)
    {
      return;
    }

  for (i = 0; i < 2; i++)
    {
      le = CG_AllocLocalEntity ();
      re = &le->refEntity;

      velocity[0] = 60 + 60 * crandom ();
      if (i == 0)
	{
	  velocity[1] = 40 + 10 * crandom ();
	}
      else
	{
	  velocity[1] = -40 + 10 * crandom ();
	}
      velocity[2] = 100 + 50 * crandom ();

      le->leType = LE_FRAGMENT;
      le->startTime = cg.levelTime;
      le->endTime = (le->startTime + cg_brassTime->integer
		     + (cg_brassTime->integer / 4) * random ());

      le->pos.trType = TR_GRAVITY;
      le->pos.trTime = cg.levelTime;

      AnglesToAxis (cent->lerpAngles, v);

      offset[0] = 8;
      offset[1] = 0;
      offset[2] = 24;

      xoffset[0] = offset[0] * v[0][0] + offset[1] * v[1][0] + offset[2] * v[2][0];
      xoffset[1] = offset[0] * v[0][1] + offset[1] * v[1][1] + offset[2] * v[2][1];
      xoffset[2] = offset[0] * v[0][2] + offset[1] * v[1][2] + offset[2] * v[2][2];
      VectorAdd (cent->lerpOrigin, xoffset, re->origin);
      VectorCopy (re->origin, le->pos.trBase);

      if (CG_PointContents (re->origin, -1) & CONTENTS_WATER)
	{
	  waterScale = 0.10f;
	}

      xvelocity[0] = velocity[0] * v[0][0] + velocity[1] * v[1][0] + velocity[2] * v[2][0];
      xvelocity[1] = velocity[0] * v[0][1] + velocity[1] * v[1][1] + velocity[2] * v[2][1];
      xvelocity[2] = velocity[0] * v[0][2] + velocity[1] * v[1][2] + velocity[2] * v[2][2];
      VectorScale (xvelocity, waterScale, le->pos.trDelta);

      AxisCopy (axisDefault, re->axis);
      re->hModel = cgs.media.shotgunBrassModel;
      le->bounceFactor = 0.3f;

      le->angles.trType = TR_LINEAR;
      le->angles.trTime = cg.levelTime;
      le->angles.trBase[0] = rand () & 31;
      le->angles.trBase[1] = rand () & 31;
      le->angles.trBase[2] = rand () & 31;
      le->angles.trDelta[0] = 1;
      le->angles.trDelta[1] = 0.5;
      le->angles.trDelta[2] = 0;

      le->leFlags = LEF_TUMBLE;
      le->leBounceSoundType = LEBS_BRASS;
      le->leMarkType = LEMT_NONE;
    }
}

// ------------------------------------------------------------

/*
================
CG_ColorizeRail
================
*/
static void
CG_ColorizeRail (int clientNum, vec4_t c1, vec4_t c2)
{
  bool coloring_done = false;
  clientInfo_t *ci;

  ci = &cgs.clientinfo[clientNum];

  if (clientNum != cg.snap->ps.clientNum)
    {
      // same team
      if ((cgs.gametype >= GT_TEAM)
	  && (ci->team == cgs.clientinfo[cg.snap->ps.clientNum].team))
	{
	  if (cg_forceRailTeamColor->string[0] != '0')
	    {
	      char val = cg_forceRailTeamColor->string[0];
	      Com_Memcpy (c1, g_color_table[ColorIndex2 (val)], sizeof (vec4_t));
	      c2[0] = c1[0];
	      c2[1] = c1[1];
	      c2[2] = c1[2];
	      c2[3] = c1[3];

	      coloring_done = true;
	    }
	}
      else if (cg_forceRailColor->string[0] != '0')
	{
	  char val = cg_forceRailColor->string[0];
	  Com_Memcpy (c1, g_color_table[ColorIndex2 (val)], sizeof (vec4_t));
	  c2[0] = c1[0];
	  c2[1] = c1[1];
	  c2[2] = c1[2];
	  c2[3] = c1[3];

	  coloring_done = true;
	}
    }

  if (!coloring_done)
    {
      c1[0] = ci->color1[0];
      c1[1] = ci->color1[1];
      c1[2] = ci->color1[2];
      c1[3] = ci->color1[3];

      c2[0] = ci->color2[0];
      c2[1] = ci->color2[1];
      c2[2] = ci->color2[2];
      c2[3] = ci->color2[3];
    }
}

#define RAILTRAIL_RADIUS   4
#define RAILTRAIL_ROTATION 1
#define RAILTRAIL_SPACING_DEFAULT  5

/*
===================
CG_RailTrailStyle1
===================
*/
static void
CG_RailTrailStyle1 (vec3_t start, vec3_t end, vec4_t c1, vec4_t c2)
{
  localEntity_t *le;
  refEntity_t *re;
  vec3_t move, vec;
  float len;
  int i;
  int railtrail_spacing;

  if (cg_railTrailParam->integer > 0)
    {
      railtrail_spacing = cg_railTrailParam->integer;
    }
  else
    {
      CG_Printf ("cg_railTrailParam must be a strictly positive integer\n");
      railtrail_spacing = RAILTRAIL_SPACING_DEFAULT;
    }

  VectorCopy (start, move);
  VectorSubtract (end, start, vec);
  len = VectorNormalize (vec);

  VectorMA (move, 20, vec, move);
  VectorScale (vec, (float)railtrail_spacing, vec);

  // treatment
  for (i = 0; i < (len/railtrail_spacing); i++)
    {
      le = CG_AllocLocalEntity ();
      re = &le->refEntity;
      le->leFlags = LEF_PUFF_DONT_SCALE;
      le->leType = LE_MOVE_SCALE_FADE;
      le->startTime = cg.levelTime;
      le->endTime = le->startTime + (i >> 1) + cg_railTrailTime->value;
      le->lifeRate = 1.0 / (le->endTime - le->startTime);

      re->shaderTime = cg.levelTime / 1000.0f;
      re->reType = RT_SPRITE;
      re->radius = 2.0f;
      re->customShader = cgs.media.railRingsShader;

      re->shaderRGBA[0] = c1[0] * 255;
      re->shaderRGBA[1] = c1[1] * 255;
      re->shaderRGBA[2] = c1[2] * 255;
      re->shaderRGBA[3] = 255;

      le->color[0] = c1[0] * 0.75;
      le->color[1] = c1[1] * 0.75;
      le->color[2] = c1[2] * 0.75;
      le->color[3] = 1.0f;

      le->pos.trType = TR_LINEAR;
      le->pos.trTime = cg.levelTime;

      VectorCopy (move, le->pos.trBase);

      VectorAdd (move, vec, move);
    }
}

/*
===================
CG_RailTrailStyle2
===================
*/
static void
CG_RailTrailStyle2 (vec3_t start, vec3_t end, vec4_t c1, vec4_t c2)
{
  localEntity_t *le;
  refEntity_t *re;
  vec3_t axis[36];
  vec3_t move, move2, vec, temp;
  float len;
  int i, j, skip;
  int railtrail_spacing;

  if (cg_railTrailParam->integer > 0)
    {
      railtrail_spacing = cg_railTrailParam->integer;
    }
  else
    {
      CG_Printf ("cg_railTrailParam must be a strictly positive integer\n");
      railtrail_spacing = RAILTRAIL_SPACING_DEFAULT;
    }

  VectorCopy (start, move);
  VectorSubtract (end, start, vec);
  len = VectorNormalize (vec);

  PerpendicularVector (temp, vec);

  for (i = 0; i < 36; i++)
    {
      RotatePointAroundVector (axis[i], vec, temp, i * 10);	//banshee 2.4 was 10
    }

  VectorMA (move, 20, vec, move);
  VectorScale (vec, (float)railtrail_spacing, vec);

  skip = -1;
  j = 18;

  // treatment
  for (i = 0; i < len; i += railtrail_spacing)
    {
      if (i != skip)
	{
	  skip = i + railtrail_spacing;
	  le = CG_AllocLocalEntity ();
	  re = &le->refEntity;
	  le->leFlags = LEF_PUFF_DONT_SCALE;
	  le->leType = LE_MOVE_SCALE_FADE;
	  le->startTime = cg.levelTime;
	  le->endTime = le->startTime + (i >> 1) + cg_railTrailTime->value;
	  le->lifeRate = 1.0 / (le->endTime - le->startTime);

	  re->shaderTime = cg.levelTime / 1000.0f;
	  re->reType = RT_SPRITE;
	  re->radius = 1.5f;
	  re->customShader = cgs.media.railRingsShader;

	  re->shaderRGBA[0] = c2[0] * 255;
	  re->shaderRGBA[1] = c2[1] * 255;
	  re->shaderRGBA[2] = c2[2] * 255;
	  re->shaderRGBA[3] = 255;

	  le->color[0] = c2[0] * 0.75;
	  le->color[1] = c2[1] * 0.75;
	  le->color[2] = c2[2] * 0.75;
	  le->color[3] = 1.0f;

	  le->pos.trType = TR_LINEAR;
	  le->pos.trTime = cg.levelTime;

	  VectorCopy (move, move2);
	  VectorMA (move2, RAILTRAIL_RADIUS, axis[j], move2);
	  VectorCopy (move2, le->pos.trBase);

	  le->pos.trDelta[0] = axis[j][0] * 6;
	  le->pos.trDelta[1] = axis[j][1] * 6;
	  le->pos.trDelta[2] = axis[j][2] * 6;
	}

      VectorAdd (move, vec, move);

      j = (j + RAILTRAIL_ROTATION) % 36;
    }
}

/*
===================
CG_RailTrailStyle3
===================
*/
static void
CG_RailTrailStyle3 (vec3_t start, vec3_t end, vec4_t c1, vec4_t c2)
{
  localEntity_t *le;
  refEntity_t *re;
  vec3_t move, vec;
  vec3_t pdir, dir;
  float len;
  int i;
  float railtrail_width;

  if (cg_railTrailParam->integer > 0)
    {
      railtrail_width = cg_railTrailParam->value;
    }
  else
    {
      CG_Printf ("cg_railTrailParam must be a strictly positive integer\n");
      railtrail_width = (float)RAILTRAIL_RADIUS;
    }

  VectorSubtract (end, start, vec);
  len = VectorNormalize (vec);


  for (i = 0; i < 2; i++)
    {
      // derive the perpendicular vector we need to crossproduct
      if (i == 0)
	{
	  // the plane perpendicular to the one
	  // we want to draw things in
	  // vertical
	  pdir[0] = 0.0f;
	  pdir[1] = 0.0f;
	  pdir[2] = 1.0f;

	  // compute horizontal plane
	  CrossProduct (vec, pdir, dir);
	}
      else if (i == 1)
	{
	  // horizontal
	  pdir[0] = 0.0f;
	  pdir[1] = 1.0f;
	  pdir[2] = 0.0f;

	  // compute vertical plane
	  CrossProduct (vec, pdir, dir);
	}

      // treatment
      le = CG_AllocLocalEntity ();
      re = &le->refEntity;
      le->leFlags = LEF_PUFF_DONT_SCALE;
      le->leType = LE_SURFACE_POLY;
      le->startTime = cg.levelTime;
      le->endTime = le->startTime + cg_railTrailTime->value;
      le->lifeRate = 1.0 / (le->endTime - le->startTime);

      re->shaderTime = cg.levelTime / 1000.0f;
      re->reType = RT_POLY;
      re->radius = len;
      re->customShader = cgs.media.railRingsShader;

      re->shaderRGBA[0] = c1[0] * 255;
      re->shaderRGBA[1] = c1[1] * 255;
      re->shaderRGBA[2] = c1[2] * 255;
      re->shaderRGBA[3] = 255;

      le->color[0] = c1[0] * 0.75;
      le->color[1] = c1[1] * 0.75;
      le->color[2] = c1[2] * 0.75;
      le->color[3] = 1.0f;

      le->pos.trType = TR_LINEAR;
      le->pos.trTime = cg.levelTime;

      VectorCopy (end, move);

      VectorMA (move, railtrail_width, dir, move);
      VectorCopy (move, le->pos.trBase);

      VectorMA (move, -2.0f * railtrail_width, dir, move);
      VectorCopy (move, le->pos.trDelta);

      VectorCopy (start, move);

      VectorMA (move, -railtrail_width, dir, move);
      VectorCopy (move, le->angles.trDelta);

      VectorMA (move, 2.0f * railtrail_width, dir, move);
      VectorCopy (move, le->angles.trBase);
    }
}

/*
==========================
CG_RailTrail
==========================
*/
void
CG_RailTrail (int clientNum, vec3_t start, vec3_t end)
{
  vec4_t c1, c2;
  int style = 1;

  start[2] -= 4;

  CG_ColorizeRail (clientNum, c1, c2);

  switch (cg_railTrailStyle->integer)
    {
    default:
    case 1:
      {
	CG_RailTrailStyle1 (start, end, c1, c2);
	break;
      }
    case 2:
      {
	CG_RailTrailStyle2 (start, end, c1, c2);
	break;
      }
    case 3:
      {
	CG_RailTrailStyle3 (start, end, c1, c2);
	break;
      }
    }
}

/*
==========================
CG_RocketTrail
==========================
*/
static void
CG_RocketTrail (centity_t * ent, const weaponInfo_t * wi)
{
  int step;
  vec3_t origin, lastPos;
  int t;
  int startTime, contents;
  int lastContents;
  entityState_t *es;
  vec3_t up;
  localEntity_t *smoke;

  if (cg_noProjectileTrail->integer)
    {
      return;
    }

  up[0] = 0;
  up[1] = 0;
  up[2] = 0;

  step = 50;

  es = &ent->currentState;
  startTime = ent->trailTime;
  // nice trick : xx[0-49] -> xx50
  // allow to regulate update frequency (every step ms)
  t = step * ((startTime + step) / step);

  BG_EvaluateTrajectory (&es->pos, cg.levelTime, origin);
  contents = CG_PointContents (origin, -1);

  /*
     // if object (e.g. grenade) is stationary, don't toss up smoke
     if ( es->pos.trType == TR_STATIONARY ) {
     ent->trailTime = cg.time;
     return;
     }
   */

  BG_EvaluateTrajectory (&es->pos, ent->trailTime, lastPos);
  lastContents = CG_PointContents (lastPos, -1);

  ent->trailTime = cg.levelTime;

  if (contents & (CONTENTS_WATER | CONTENTS_SLIME | CONTENTS_LAVA))
    {
      if (contents & lastContents & CONTENTS_WATER)
	{
	  CG_BubbleTrail (lastPos, origin, 8);
	}
      return;
    }

  for (; t <= ent->trailTime; t += step)
    {
      BG_EvaluateTrajectory (&es->pos, t, lastPos);

      smoke = CG_SmokePuff (lastPos, up,
			    wi->trailRadius,
			    1, 1, 1, 0.33f,
			    wi->wiTrailTime,
			    t, 0, 0, cgs.media.smokePuffShader);
      // use the optimized local entity add
      smoke->leType = LE_SCALE_FADE;
    }
}

/*
==========================
CG_PlasmaTrail
==========================
*/
static void
CG_PlasmaTrail (centity_t * cent, const weaponInfo_t * wi)
{
  localEntity_t *le;
  refEntity_t *re;
  entityState_t *es;
  vec3_t velocity, xvelocity, origin;
  vec3_t offset, xoffset;
  vec3_t v[3];

  float waterScale = 1.0f;

  if (cg_noProjectileTrail->integer || cg_oldPlasma->integer)
    {
      return;
    }

  es = &cent->currentState;

  BG_EvaluateTrajectory (&es->pos, cg.levelTime, origin);

  le = CG_AllocLocalEntity ();
  re = &le->refEntity;

  velocity[0] = 60 - 120 * crandom ();
  velocity[1] = 40 - 80 * crandom ();
  velocity[2] = 100 - 200 * crandom ();

  le->leType = LE_MOVE_SCALE_FADE;
  le->leFlags = LEF_TUMBLE;
  le->leBounceSoundType = LEBS_NONE;
  le->leMarkType = LEMT_NONE;

  le->startTime = cg.levelTime;
  le->endTime = le->startTime + 600;

  le->pos.trType = TR_GRAVITY;
  le->pos.trTime = cg.levelTime;

  AnglesToAxis (cent->lerpAngles, v);

  offset[0] = 2;
  offset[1] = 2;
  offset[2] = 2;

  xoffset[0] = offset[0] * v[0][0] + offset[1] * v[1][0] + offset[2] * v[2][0];
  xoffset[1] = offset[0] * v[0][1] + offset[1] * v[1][1] + offset[2] * v[2][1];
  xoffset[2] = offset[0] * v[0][2] + offset[1] * v[1][2] + offset[2] * v[2][2];

  VectorAdd (origin, xoffset, re->origin);
  VectorCopy (re->origin, le->pos.trBase);

  if (CG_PointContents (re->origin, -1) & CONTENTS_WATER)
    {
      waterScale = 0.10f;
    }

  xvelocity[0] = velocity[0] * v[0][0] + velocity[1] * v[1][0] + velocity[2] * v[2][0];
  xvelocity[1] = velocity[0] * v[0][1] + velocity[1] * v[1][1] + velocity[2] * v[2][1];
  xvelocity[2] = velocity[0] * v[0][2] + velocity[1] * v[1][2] + velocity[2] * v[2][2];
  VectorScale (xvelocity, waterScale, le->pos.trDelta);

  AxisCopy (axisDefault, re->axis);
  re->shaderTime = cg.levelTime / 1000.0f;
  re->reType = RT_SPRITE;
  re->radius = 0.25f;
  re->customShader = cgs.media.plasmaTrailShader;
  le->bounceFactor = 0.3f;

  re->shaderRGBA[0] = wi->flashDlightColor[0] * 63;
  re->shaderRGBA[1] = wi->flashDlightColor[1] * 63;
  re->shaderRGBA[2] = wi->flashDlightColor[2] * 63;
  re->shaderRGBA[3] = 63;

  le->color[0] = wi->flashDlightColor[0] * 0.2;
  le->color[1] = wi->flashDlightColor[1] * 0.2;
  le->color[2] = wi->flashDlightColor[2] * 0.2;
  le->color[3] = 0.25f;

  le->angles.trType = TR_LINEAR;
  le->angles.trTime = cg.levelTime;
  le->angles.trBase[0] = rand () & 31;
  le->angles.trBase[1] = rand () & 31;
  le->angles.trBase[2] = rand () & 31;
  le->angles.trDelta[0] = 1;
  le->angles.trDelta[1] = 0.5;
  le->angles.trDelta[2] = 0;
}

/*
==========================
CG_GrenadeTrail
==========================
*/
static void
CG_GrenadeTrail (centity_t * ent, const weaponInfo_t * wi)
{
  CG_RocketTrail (ent, wi);
}

/*
=================
CG_RegisterWeapon

The server says this item is used on this level
weapon_t is an enum of available weapons
=================
*/
void
CG_RegisterWeapon (weapon_t weaponNum)
{
  weaponInfo_t *weaponInfo;
  const gitem_t *item;
  const gitem_t *ammo;
  char path[MAX_QPATH];
  vec3_t mins, maxs;
  int i;

  // usual check and we want to exclude weapon 0
  if ((weaponNum <= 0) || (weaponNum >= WP_NUM_WEAPONS))
    {
      return;
    }

  weaponInfo = &cg_weapons[weaponNum];

  if (weaponInfo->registered)
    {
      return;
    }

  Com_Memset (weaponInfo, 0, sizeof (*weaponInfo));
  weaponInfo->registered = true;

  // bg_itemlist[0] is NULL so start to bg_itemlist[1]
  for (item = bg_itemlist + 1; item->classname; item++)
    {
      if ((item->giType == IT_WEAPON) && (item->giTag == (unsigned int) weaponNum))
	{
	  weaponInfo->item = item;
	  break;
	}
    }

  if (!item->classname)
    {
      CG_Error ("Couldn't find weapon %i", weaponNum);
    }

  CG_RegisterItemVisuals (item - bg_itemlist);

  // load cmodel before model so filecache works
  if (cg_nomip2->integer & NM2_WEAPONS)
    weaponInfo->weaponModel = cgl->R_RegisterModelExt (item->world_model[0],
						       SHAD_NOPICMIP);
  else
    weaponInfo->weaponModel = cgl->R_RegisterModel (item->world_model[0]);

  // calc midpoint for rotation
  cgl->R_ModelBounds (weaponInfo->weaponModel, mins, maxs);
  for (i = 0; i < 3; i++)
    {
      weaponInfo->weaponMidpoint[i] = mins[i] + 0.5 * (maxs[i] - mins[i]);
    }

  weaponInfo->weaponIcon = cgl->R_RegisterShader (item->icon);
  weaponInfo->ammoIcon = cgl->R_RegisterShader (item->icon);

  // register associated ammos too
  for (ammo = bg_itemlist + 1; ammo->classname; ammo++)
    {
      if ((ammo->giType == IT_AMMO) && (ammo->giTag == (unsigned int) weaponNum))
	{
	  break;
	}
    }

  if (ammo->classname && ammo->world_model[0])
    {
      if (cg_nomip2->integer & NM2_ITEMS)
	weaponInfo->ammoModel = cgl->R_RegisterModelExt (ammo->world_model[0],
							 SHAD_NOPICMIP);
      else
	weaponInfo->ammoModel = cgl->R_RegisterModel (ammo->world_model[0]);
    }

  strcpy (path, item->world_model[0]);
  COM_StripExtension (path, path, sizeof (path));
  strcat (path, "_flash.md3");

  if (cg_nomip2->integer & NM2_WEAPONS)
    weaponInfo->flashModel = cgl->R_RegisterModelExt (path, SHAD_NOPICMIP);
  else
    weaponInfo->flashModel = cgl->R_RegisterModel (path);

  strcpy (path, item->world_model[0]);
  COM_StripExtension (path, path, sizeof (path));
  strcat (path, "_barrel.md3");

  if (cg_nomip2->integer & NM2_WEAPONS)
    weaponInfo->barrelModel = cgl->R_RegisterModelExt (path, SHAD_NOPICMIP);
  else
    weaponInfo->barrelModel = cgl->R_RegisterModel (path);

  strcpy (path, item->world_model[0]);
  COM_StripExtension (path, path, sizeof (path));
  strcat (path, "_hand.md3");

  if (cg_nomip2->integer & NM2_WEAPONS)
    weaponInfo->handsModel = cgl->R_RegisterModelExt (path, SHAD_NOPICMIP);
  else
    weaponInfo->handsModel = cgl->R_RegisterModel (path);

  if (!weaponInfo->handsModel)
    {
      weaponInfo->handsModel =
	cgl->R_RegisterModel ("models/weapons2/shotgun/shotgun_hand.md3");
    }

  weaponInfo->loopFireSound = false;

  // weapon specific work
  switch (weaponNum)
    {
    case WP_GAUNTLET:
      MAKERGB (weaponInfo->flashDlightColor, 0.6f, 0.6f, 1.0f);
      weaponInfo->firingSound =
	cgl->snd->RegisterSound ("sound/weapons/melee/fstrun.wav");
      weaponInfo->flashSound[0] =
	cgl->snd->RegisterSound ("sound/weapons/melee/fstatck.wav");

      break;

    case WP_LIGHTNING:
      {
	MAKERGB (weaponInfo->flashDlightColor, 0.6f, 0.6f, 1.0f);
	weaponInfo->readySound =
	  cgl->snd->RegisterSound ("sound/weapons/melee/fsthum.wav");
	weaponInfo->firingSound =
	  cgl->snd->RegisterSound ("sound/weapons/lightning/lg_hum.wav");

	weaponInfo->flashSound[0] =
	  cgl->snd->RegisterSound ("sound/weapons/lightning/lg_fire.wav");

	for (i = 0; i < MAX_LIGHTNING_SHADERS; i++)
	  {
	    if (cg_nomip->integer & NM_SHAFT_LIGHTNING)
	      {
		if (cg_nomip->integer & NM_SHAFT_MIPMAP)
		  {
		    cgs.media.lightningShader[i]
		      = cgl->R_RegisterShaderExt (va ("lightningBolt%d", i),
						  SHAD_NOPICMIP | SHAD_NOMIPMAPS);
		  }
		else
		  {
		    cgs.media.lightningShader[i]
		      = cgl->R_RegisterShaderExt (va ("lightningBolt%d", i),
						  SHAD_NOPICMIP);
		  }
	      }
	    else
	      {
		cgs.media.lightningShader[i]
		  = cgl->R_RegisterShader (va ("lightningBolt%d", i));
	      }
	  }

	if (cg_nomip->integer & NM_SHAFT_IMPACT)
	  cgs.media.lightningExplosionModel =
	    cgl->R_RegisterModelExt ("models/weaphits/crackle.md3", SHAD_NOPICMIP);
	else
	  cgs.media.lightningExplosionModel =
	    cgl->R_RegisterModel ("models/weaphits/crackle.md3");

	cgs.media.sfx_lghit1 =
	  cgl->snd->RegisterSound ("sound/weapons/lightning/lg_hit.wav");
	cgs.media.sfx_lghit2 =
	  cgl->snd->RegisterSound ("sound/weapons/lightning/lg_hit2.wav");
	cgs.media.sfx_lghit3 =
	  cgl->snd->RegisterSound ("sound/weapons/lightning/lg_hit3.wav");

	break;
      }

    case WP_MACHINEGUN:
      MAKERGB (weaponInfo->flashDlightColor, 1, 1, 0);
      weaponInfo->flashSound[0] =
	cgl->snd->RegisterSound ("sound/weapons/machinegun/machgf1b.wav");
      weaponInfo->flashSound[1] =
	cgl->snd->RegisterSound ("sound/weapons/machinegun/machgf2b.wav");
      weaponInfo->flashSound[2] =
	cgl->snd->RegisterSound ("sound/weapons/machinegun/machgf3b.wav");
      weaponInfo->flashSound[3] =
	cgl->snd->RegisterSound ("sound/weapons/machinegun/machgf4b.wav");
      weaponInfo->ejectBrassFunc = CG_MachineGunEjectBrass;

      if (cg_nomip->integer & NM_BULLET_IMPACT)
	cgs.media.bulletExplosionShader = CG_R_RegisterShaderNoMip ("bulletExplosion");
      else
	cgs.media.bulletExplosionShader = cgl->R_RegisterShader ("bulletExplosion");

      break;

    case WP_SHOTGUN:
      MAKERGB (weaponInfo->flashDlightColor, 1, 1, 0);
      weaponInfo->flashSound[0] =
	cgl->snd->RegisterSound ("sound/weapons/shotgun/sshotf1b.wav");
      weaponInfo->ejectBrassFunc = CG_ShotgunEjectBrass;

      break;

    case WP_ROCKET_LAUNCHER:
      if (cg_nomip2->integer & NM2_ROCKET_PROJ)
	weaponInfo->missileModel = cgl->R_RegisterModelExt ("models/ammo/rocket/rocket.md3",
							    SHAD_NOPICMIP);
      else
	weaponInfo->missileModel =
	  cgl->R_RegisterModel ("models/ammo/rocket/rocket.md3");

      weaponInfo->missileSound =
	cgl->snd->RegisterSound ("sound/weapons/rocket/rockfly.wav");
      weaponInfo->missileTrailFunc = CG_RocketTrail;
      weaponInfo->missileDlight = 200;
      weaponInfo->wiTrailTime = 2000;
      weaponInfo->trailRadius = 64;

      MAKERGB (weaponInfo->missileDlightColor, 1, 0.75f, 0);
      MAKERGB (weaponInfo->flashDlightColor, 1, 0.75f, 0);

      weaponInfo->flashSound[0] =
	cgl->snd->RegisterSound ("sound/weapons/rocket/rocklf1a.wav");
      if (cg_nomip->integer & NM_ROCKET_EXPLO)
	cgs.media.rocketExplosionShader = CG_R_RegisterShaderNoMip ("rocketExplosion");
      else
	cgs.media.rocketExplosionShader = cgl->R_RegisterShader ("rocketExplosion");

      break;

    case WP_GRENADE_LAUNCHER:
      {
	if (cg_nomip2->integer & NM2_GRENADE_PROJ)
	  weaponInfo->missileModel =
	    cgl->R_RegisterModelExt ("models/ammo/grenade1.md3",
				     SHAD_FORCE_LIGHTMAP | SHAD_NOPICMIP);
	else
	  weaponInfo->missileModel =
	    cgl->R_RegisterModelExt ("models/ammo/grenade1.md3", SHAD_FORCE_LIGHTMAP);

	weaponInfo->missileTrailFunc = CG_GrenadeTrail;
	weaponInfo->wiTrailTime = 700;
	weaponInfo->trailRadius = 32;
	MAKERGB (weaponInfo->flashDlightColor, 1, 0.70f, 0);
	weaponInfo->flashSound[0] =
	  cgl->snd->RegisterSound ("sound/weapons/grenade/grenlf1a.wav");

	if (cg_nomip->integer & NM_GRENADE_EXPLO)
	  cgs.media.grenadeExplosionShader = CG_R_RegisterShaderNoMip ("grenadeExplosion");
	else
	  cgs.media.grenadeExplosionShader = cgl->R_RegisterShader ("grenadeExplosion");

	break;
      }

    case WP_PLASMAGUN:
//              weaponInfo->missileModel = cgs.media.invulnerabilityPowerupModel;
      weaponInfo->missileTrailFunc = CG_PlasmaTrail;
      weaponInfo->missileSound =
	cgl->snd->RegisterSound ("sound/weapons/plasma/lasfly.wav");
      MAKERGB (weaponInfo->flashDlightColor, 0.6f, 0.6f, 1.0f);
      weaponInfo->flashSound[0] =
	cgl->snd->RegisterSound ("sound/weapons/plasma/hyprbf1a.wav");

      if (cg_nomip->integer & NM_PLASMA)
	cgs.media.plasmaBallShader = CG_R_RegisterShaderNoMip ("sprites/plasma1");
      else
	cgs.media.plasmaBallShader = cgl->R_RegisterShader ("sprites/plasma1");

      if (cg_nomip->integer & NM_PLASMA)
	cgs.media.energyMarkShader = CG_R_RegisterShaderNoMip ("gfx/damage/plasma_mrk");
      else
	cgs.media.energyMarkShader = cgl->R_RegisterShader ("gfx/damage/plasma_mrk");

      if (cg_nomip->integer & NM_PLASMA)
	cgs.media.plasmaExplosionShader = CG_R_RegisterShaderNoMip ("plasmaExplosion");
      else
	cgs.media.plasmaExplosionShader = cgl->R_RegisterShader ("plasmaExplosion");

      if (cg_nomip->integer & NM_PLASMA)
	cgs.media.plasmaTrailShader = CG_R_RegisterShaderNoMip ("railDisc");
      else
	cgs.media.plasmaTrailShader = cgl->R_RegisterShader ("railDisc");

      break;

    case WP_RAILGUN:
      weaponInfo->readySound =
	cgl->snd->RegisterSound ("sound/weapons/railgun/rg_hum.wav");
      MAKERGB (weaponInfo->flashDlightColor, 1, 0.5f, 0);
      weaponInfo->flashSound[0] =
	cgl->snd->RegisterSound ("sound/weapons/railgun/railgf1a.wav");
      // fixing some "NULL AddPolyToScene" spam because of an uninitialized shader...
      if (cg_nomip->integer & NM_RAIL)
	cgs.media.energyMarkShader = CG_R_RegisterShaderNoMip ("gfx/damage/plasma_mrk");
      else
	cgs.media.energyMarkShader = cgl->R_RegisterShader ("gfx/damage/plasma_mrk");

      if (cg_nomip->integer & NM_RAIL)
	{
	  cgs.media.railExplosionShader = CG_R_RegisterShaderNoMip ("railExplosion");
	  cgs.media.railRingsShader = CG_R_RegisterShaderNoMip ("railDisc");
	  cgs.media.railCoreShader = CG_R_RegisterShaderNoMip ("railCore");
	}
      else
	{
	  cgs.media.railExplosionShader = cgl->R_RegisterShader ("railExplosion");
	  cgs.media.railRingsShader = cgl->R_RegisterShader ("railDisc");
	  cgs.media.railCoreShader = cgl->R_RegisterShader ("railCore");
	}

      break;

    case WP_BFG:
      weaponInfo->readySound =
	cgl->snd->RegisterSound ("sound/weapons/bfg/bfg_hum.wav");
      MAKERGB (weaponInfo->flashDlightColor, 1, 0.7f, 1);
      weaponInfo->flashSound[0] =
	cgl->snd->RegisterSound ("sound/weapons/bfg/bfg_fire.wav");
      if (cg_nomip->integer & NM_BFG_EXPLO)
	cgs.media.bfgExplosionShader = CG_R_RegisterShaderNoMip ("bfgExplosion");
      else
	cgs.media.bfgExplosionShader = cgl->R_RegisterShader ("bfgExplosion");

      if (cg_nomip2->integer & NM2_BFG_PROJ)
	weaponInfo->missileModel =
	  cgl->R_RegisterModelExt ("models/weaphits/bfg.md3", SHAD_NOPICMIP);
      else
	weaponInfo->missileModel =
	  cgl->R_RegisterModel ("models/weaphits/bfg.md3");

      weaponInfo->missileSound =
	cgl->snd->RegisterSound ("sound/weapons/rocket/rockfly.wav");

      break;

    default:
      MAKERGB (weaponInfo->flashDlightColor, 1, 1, 1);
      weaponInfo->flashSound[0] =
	cgl->snd->RegisterSound ("sound/weapons/rocket/rocklf1a.wav");
      break;
    }
}

/*
=================
CG_RegisterItemVisuals

The server says this item is used on this level
=================
*/
void
CG_RegisterItemVisuals (int itemNum)
{
  itemInfo_t *itemInfo;
  const gitem_t *item;

  // may someone want to register item 0 : the NULL item ?
  if ((itemNum < 0) || (itemNum >= bg_numItems))
    {
      CG_Error ("CG_RegisterItemVisuals: itemNum %d out of range [0-%d]",
		itemNum, bg_numItems - 1);
    }

  itemInfo = &cg_items[itemNum];
  if (itemInfo->registered)
    {
      return;
    }

  // Should add a channel to com_printf and then chan_mapinfo
  //Com_Printf ("register item nb : %i\n", itemNum);
  item = &bg_itemlist[itemNum];

  Com_Memset (itemInfo, 0, sizeof (&itemInfo));
  itemInfo->registered = true;

  if (item->giType == IT_WEAPON)
    {
      CG_RegisterWeapon ((weapon_t) item->giTag);
    }

  if (cg_nomip2->integer & NM2_ITEMS)
    {
      itemInfo->models[0] =
	cgl->R_RegisterModelExt (item->world_model[0], SHAD_NOPICMIP);
      itemInfo->icon = CG_R_RegisterShaderNoMip (item->icon);
    }
  else
    {
      itemInfo->models[0] = cgl->R_RegisterModel (item->world_model[0]);
      itemInfo->icon = cgl->R_RegisterShader (item->icon);
    }

  //
  // powerups have an accompanying ring or sphere
  //
  if ((item->giType == IT_POWERUP) || (item->giType == IT_HEALTH)
      || (item->giType == IT_ARMOR) || (item->giType == IT_HOLDABLE))
    {
      if (item->world_model[1])
	{
	  if (cg_nomip2->integer & NM2_ITEMS)
	    itemInfo->models[1] =
	      cgl->R_RegisterModelExt (item->world_model[1], SHAD_NOPICMIP);
	  else
	    itemInfo->models[1] = cgl->R_RegisterModel (item->world_model[1]);
	}
    }
}

/*
========================================================================================

VIEW WEAPON

========================================================================================
*/

/*
=================
CG_MapTorsoToWeaponFrame

=================
*/
static int
CG_MapTorsoToWeaponFrame (clientInfo_t * ci, int frame)
{
  // change weapon
  if ((frame >= ci->animations[TORSO_DROP].firstFrame)
      && (frame < (ci->animations[TORSO_DROP].firstFrame + 9)))
    {
      return (frame - ci->animations[TORSO_DROP].firstFrame + 6);
    }

  // stand attack
  if ((frame >= ci->animations[TORSO_ATTACK].firstFrame)
      && (frame < (ci->animations[TORSO_ATTACK].firstFrame + 6)))
    {
      return (1 + frame - ci->animations[TORSO_ATTACK].firstFrame);
    }

  // stand attack 2
  if ((frame >= ci->animations[TORSO_ATTACK2].firstFrame)
      && (frame < (ci->animations[TORSO_ATTACK2].firstFrame + 6)))
    {
      return (1 + frame - ci->animations[TORSO_ATTACK2].firstFrame);
    }

  return 0;
}

/*
==============
CG_CalculateWeaponPosition
==============
*/
static void
CG_CalculateWeaponPosition (vec3_t origin, vec3_t angles)
{
  float scale;
  int delta;
  float fracsin;

  VectorCopy (cg.refdef.vieworg, origin);
  VectorCopy (cg.refdefViewAngles, angles);

  // on odd legs, invert some angles
  if (cg.bobcycle & 1)
    {
      scale = -cg.xyspeed;
    }
  else
    {
      scale = cg.xyspeed;
    }

  // gun angles from bobbing
  angles[ROLL] += scale * cg.bobfracsin * 0.005;
  angles[YAW] += scale * cg.bobfracsin * 0.01;
  angles[PITCH] += cg.xyspeed * cg.bobfracsin * 0.005;

  // drop the weapon when landing
  delta = cg.levelTime - cg.landTime;
  if (delta < LAND_DEFLECT_TIME)
    {
      origin[2] += cg.landChange * 0.25 * delta / LAND_DEFLECT_TIME;
    }
  else if (delta < (LAND_DEFLECT_TIME + LAND_RETURN_TIME))
    {
      origin[2] += cg.landChange * 0.25 *
	(LAND_DEFLECT_TIME + LAND_RETURN_TIME - delta) / LAND_RETURN_TIME;
    }

  // idle drift
  scale = cg.xyspeed + 40;
  fracsin = sin (cg.levelTime * 0.001);
  angles[ROLL] += scale * fracsin * 0.01;
  angles[YAW] += scale * fracsin * 0.01;
  angles[PITCH] += scale * fracsin * 0.01;
}

/*
===============
CG_LightningBolt

Origin will be the exact tag point, which is slightly
different than the muzzle point used for determining hits.
The cent should be the non-predicted cent if it is from the player,
so the endpoint will reflect the simulated strike (lagging the predicted
angle)
===============
*/
static void
CG_LightningBolt (centity_t *cent, vec3_t origin)
{
  int i, index;
  trace_t trace;
  float len, len2;
  vec3_t forward, right;
  vec3_t muzzlePoint, endPoint, diff;
  int anim;
  polyVert_t verts[4];
  const float LIGHTNING_WIDTH = 8.0f;

  index = cg_lightningStyle->integer;
  // some checks
  if ((index < 1) && (index > MAX_LIGHTNING_SHADERS))
    {
      CG_Printf ("cg_lightningStyle is out of range [1-3]\n");
      index = 1;
    }

  // CPMA  "true" lightning
  // rendering the client
  if ((cent->currentState.number == cg.predictedPlayerState.clientNum)
      && (cg_trueLightning->value != 0.0f))
    {
      // the client doesn't care to use the skin as the beam origin
      vec3_t angle;
      int i;

      for (i = 0; i < 3; i++)
	{
	  float a = cent->lerpAngles[i] - cg.refdefViewAngles[i];
	  if (a > 180)
	    {
	      a -= 360;
	    }
	  if (a < -180)
	    {
	      a += 360;
	    }

	  angle[i] = cg.refdefViewAngles[i] + a * (1.0 - cg_trueLightning->value);
	  if (angle[i] < 0)
	    {
	      angle[i] += 360;
	    }
	  if (angle[i] > 360)
	    {
	      angle[i] -= 360;
	    }
	}

      AngleVectors (angle, forward, right, NULL);
      // if we want full "trueLightning" use predicted beam origin too
      if (cg_trueLightning->integer == 1)
	{
	  VectorCopy (cg.predictedPlayerState.origin, muzzlePoint);
	}
      else
	{
	  VectorCopy (cent->lerpOrigin, muzzlePoint);
	}
    }
  else
    {
      // !CPMA
      AngleVectors (cent->lerpAngles, forward, right, NULL);
      //AngleVectors (cent->currentState.apos.trBase, forward, right, NULL);
      VectorCopy (cent->lerpOrigin, muzzlePoint);
    }

  // has nothing to do with crouch or jump
  // only to make the beam fit the crosshair
  anim = cent->currentState.legsAnim & ~ANIM_TOGGLEBIT;
  if ((anim == LEGS_WALKCR) || (anim == LEGS_IDLECR))
    {
      muzzlePoint[2] += CROUCH_VIEWHEIGHT;
    }
  else
    {
      muzzlePoint[2] += DEFAULT_VIEWHEIGHT;
    }

  // project forward by the lightning range
  VectorMA (muzzlePoint, LIGHTNING_RANGE, forward, endPoint);

  // see if it hit a wall
  CG_Trace (&trace, muzzlePoint, vec3_origin, vec3_origin, endPoint,
	    cent->currentState.number, MASK_SHOT);

  // this is the endpoint
  VectorCopy (trace.endpos, endPoint);

  // use the provided origin, even though it may be slightly
  // different than the muzzle origin
  VectorSubtract (endPoint, origin, diff);
  len = VectorNormalize (diff);
  len2 = len / 256.0f;

  // rotate the lightning beam
  RotatePointAroundVector (muzzlePoint, diff, right, -20);
  VectorCopy (muzzlePoint, right);

  for (i = 0; i < 2; i++)
    {
      // here is the work
      VectorMA (endPoint, LIGHTNING_WIDTH, right, verts[0].xyz);
      verts[0].st[0] = len2;
      verts[0].st[1] = 0;
      verts[0].modulate[0] = 255;
      verts[0].modulate[1] = 255;
      verts[0].modulate[2] = 255;
      verts[0].modulate[3] = 255;

      VectorMA (endPoint, -LIGHTNING_WIDTH, right, verts[1].xyz);
      verts[1].st[0] = len2;
      verts[1].st[1] = 1;
      verts[1].modulate[0] = 255;
      verts[1].modulate[1] = 255;
      verts[1].modulate[2] = 255;
      verts[1].modulate[3] = 255;

      VectorMA (origin, -LIGHTNING_WIDTH, right, verts[2].xyz);
      verts[2].st[0] = 1;
      verts[2].st[1] = 1;
      verts[2].modulate[0] = 255;
      verts[2].modulate[1] = 255;
      verts[2].modulate[2] = 255;
      verts[2].modulate[3] = 255;

      VectorMA (origin, LIGHTNING_WIDTH, right, verts[3].xyz);
      verts[3].st[0] = 0;
      verts[3].st[1] = 0;
      verts[3].modulate[0] = 255;
      verts[3].modulate[1] = 255;
      verts[3].modulate[2] = 255;
      verts[3].modulate[3] = 255;

      // rendering
      cgl->R_AddPolysToScene (cgs.media.lightningShader[index], 4, verts, 1);

      // multiple passes to get a nice effect (muzzlePoint is no more needed)
      RotatePointAroundVector (muzzlePoint, diff, right, 180);
      VectorCopy (muzzlePoint, right);
    }

  // rotate the lightning beam
  RotatePointAroundVector (muzzlePoint, diff, right, 40);
  VectorCopy (muzzlePoint, right);

  for (i = 0; i < 2; i++)
    {
      // here is the work
      VectorMA (endPoint, LIGHTNING_WIDTH, right, verts[0].xyz);
      verts[0].st[0] = len2;
      verts[0].st[1] = 0;
      verts[0].modulate[0] = 255;
      verts[0].modulate[1] = 255;
      verts[0].modulate[2] = 255;
      verts[0].modulate[3] = 255;

      VectorMA (endPoint, -LIGHTNING_WIDTH, right, verts[1].xyz);
      verts[1].st[0] = len2;
      verts[1].st[1] = 1;
      verts[1].modulate[0] = 255;
      verts[1].modulate[1] = 255;
      verts[1].modulate[2] = 255;
      verts[1].modulate[3] = 255;

      VectorMA (origin, -LIGHTNING_WIDTH, right, verts[2].xyz);
      verts[2].st[0] = 1;
      verts[2].st[1] = 1;
      verts[2].modulate[0] = 255;
      verts[2].modulate[1] = 255;
      verts[2].modulate[2] = 255;
      verts[2].modulate[3] = 255;

      VectorMA (origin, LIGHTNING_WIDTH, right, verts[3].xyz);
      verts[3].st[0] = 0;
      verts[3].st[1] = 0;
      verts[3].modulate[0] = 255;
      verts[3].modulate[1] = 255;
      verts[3].modulate[2] = 255;
      verts[3].modulate[3] = 255;

      // rendering
      cgl->R_AddPolysToScene (cgs.media.lightningShader[index], 4, verts, 1);

      // multiple passes to get a nice effect (muzzlePoint is no more needed)
      RotatePointAroundVector (muzzlePoint, diff, right, 180);
      VectorCopy (muzzlePoint, right);
    }

  // add the impact flare if it hit something, 32 is the minimal dist for display
  if ((trace.fraction < 1.0) && cg_lightningimpact->integer && (len > 32))
    {
      refEntity_t beam;
      vec3_t angles;
      vec3_t dir;

      Com_Memset (&beam, 0, sizeof (beam));
      beam.hModel = cgs.media.lightningExplosionModel;

      VectorMA (trace.endpos, -16, diff, beam.origin);

      // make a random orientation
      angles[0] = rand () % 360;
      angles[1] = rand () % 360;
      angles[2] = rand () % 360;
      AnglesToAxis (angles, beam.axis);
      cgl->R_AddRefEntityToScene (&beam);
    }
}

/*
======================
CG_MachinegunSpinAngle
======================
*/
#define SPIN_SPEED 0.9
#define COAST_TIME 1000

static float
CG_MachinegunSpinAngle (centity_t * cent)
{
  int delta;
  float angle;
  float speed;

  delta = cg.levelTime - cent->pe.barrelTime;
  if (cent->pe.barrelSpinning)
    {
      angle = cent->pe.barrelAngle + delta * SPIN_SPEED;
    }
  else
    {
      if (delta > COAST_TIME)
	{
	  delta = COAST_TIME;
	}

      speed = 0.5 * (SPIN_SPEED + (float) (COAST_TIME - delta) / COAST_TIME);
      angle = cent->pe.barrelAngle + delta * speed;
    }

  if (cent->pe.barrelSpinning == !(cent->currentState.eFlags & EF_FIRING))
    {
      cent->pe.barrelTime = cg.levelTime;
      cent->pe.barrelAngle = AngleMod (angle);
      // cast to _Bool
      cent->pe.barrelSpinning = ! !(cent->currentState.eFlags & EF_FIRING);
    }

  return angle;
}

/*
========================
CG_AddWeaponWithPowerups
========================
*/
static void
CG_AddWeaponWithPowerups (refEntity_t * gun, int powerups)
{
  // add powerup effects
  if (powerups & (1 << PW_INVIS))
    {
      gun->customShader = cgs.media.invisShader;
      cgl->R_AddRefEntityToScene (gun);
    }
  else
    {
      cgl->R_AddRefEntityToScene (gun);

      if (powerups & (1 << PW_BATTLESUIT))
	{
	  gun->customShader = cgs.media.battleWeaponShader;
	  cgl->R_AddRefEntityToScene (gun);
	}

      if (powerups & (1 << PW_QUAD))
	{
	  gun->customShader = cgs.media.quadWeaponShader;
	  cgl->R_AddRefEntityToScene (gun);
	}
    }
}

/*
=============
CG_AddPlayerWeapon

Used for both the view weapon (ps is valid) and the world modelother character models (ps is NULL)
The main player will have this called for BOTH cases, so effects like light and
sound should only be done on the world model case.
=============
*/
void
CG_AddPlayerWeapon (refEntity_t *parent, playerState_t *ps,
		    centity_t *cent, team_t team)
{
  refEntity_t gun;
  refEntity_t barrel;
  refEntity_t flash;
  vec3_t angles;
  weapon_t weaponNum;
  weaponInfo_t *weapon;
  centity_t *nonPredictedCent;

  weaponNum = cent->currentState.weapon;

  CG_RegisterWeapon (weaponNum);
  weapon = &cg_weapons[weaponNum];

  // add the weapon
  Com_Memset (&gun, 0, sizeof (gun));
  VectorCopy (parent->lightingOrigin, gun.lightingOrigin);
  gun.shadowPlane = parent->shadowPlane;
  gun.renderfx = parent->renderfx;

  // set custom shading for railgun refire rate
  if (ps)
    {
      // use the predicted player state or the current one ?
      if ((cg.predictedPlayerState.weapon == WP_RAILGUN)
	  && (cg.predictedPlayerState.weaponstate == WEAPON_FIRING))
	{
	  float f;
	  clientInfo_t *ci;
	  ci = &cgs.clientinfo[cent->currentState.clientNum];

	  f = ((float) cg.predictedPlayerState.weaponTime
	       / (float) LOCALRAILGUN_RELOADTIME);
	  // removed tests, should not happend if weaponTime isn't changed
	  //if (f > 1.0f) f=1.0f;
	  //if (f < 0.0f) f=0.0f;

	  // time going from 1 to 0
	  // we want def to be null at start while color is full and decreases
	  // def - def * time + color * time
	  // def : white here is 1.0f + factor gives the result
	  gun.shaderRGBA[1] = 255 * (1.0f - f * (1.0f - ci->color1[1]));
	  gun.shaderRGBA[0] = 255 * (1.0f - f * (1.0f - ci->color1[0]));
	  gun.shaderRGBA[2] = 255 * (1.0f - f * (1.0f - ci->color1[2]));
	  // previous formula is "safe"
	  //if (gun.shaderRGBA[0] > 255) gun.shaderRGBA[0] = 255;
	  //if (gun.shaderRGBA[1] > 255) gun.shaderRGBA[1] = 255;
	  //if (gun.shaderRGBA[2] > 255) gun.shaderRGBA[2] = 255;
	  //gun.shaderRGBA[3] = 255;
	}
      else
	{
	  gun.shaderRGBA[0] = 255;
	  gun.shaderRGBA[1] = 255;
	  gun.shaderRGBA[2] = 255;
	  gun.shaderRGBA[3] = 255;
	}
    }

  // when dead, nothing to draw
  gun.hModel = weapon->weaponModel;
  if (!gun.hModel)
    {
      return;
    }

  // 3rd person view
  if (!ps)
    {
      // add weapon ready sound
      cent->pe.lightningFiring = false;
      if ((cent->currentState.eFlags & EF_FIRING) && weapon->firingSound)
	{
	  // lightning gun and guantlet make a different sound when fire is held down
	  cgl->snd->AddLoopingSound (cent->currentState.number,
				     cent->lerpOrigin, vec3_origin,
				     weapon->firingSound);
	  cent->pe.lightningFiring = true;
	}
      else if (weapon->readySound)
	{
	  cgl->snd->AddLoopingSound (cent->currentState.number,
				     cent->lerpOrigin, vec3_origin,
				     weapon->readySound);
	}
    }

  // use this hacked one to position the weapon onscreen
  if (ps && !cg.renderingThirdPerson)
    CG_PositionEntityOnTag (&gun, parent, parent->hModel, "tag_weapon", true);
  else
    CG_PositionEntityOnTag (&gun, parent, parent->hModel, "tag_weapon", false);

  CG_AddWeaponWithPowerups (&gun, cent->currentState.powerups);

  // add the spinning barrel
  if (weapon->barrelModel)
    {
      Com_Memset (&barrel, 0, sizeof (barrel));
      VectorCopy (parent->lightingOrigin, barrel.lightingOrigin);
      barrel.shadowPlane = parent->shadowPlane;
      barrel.renderfx = parent->renderfx;

      barrel.hModel = weapon->barrelModel;
      angles[YAW] = 0;
      angles[PITCH] = 0;
      angles[ROLL] = CG_MachinegunSpinAngle (cent);
      AnglesToAxis (angles, barrel.axis);

      CG_PositionRotatedEntityOnTag (&barrel, &gun, weapon->weaponModel,
				     "tag_barrel");

      CG_AddWeaponWithPowerups (&barrel, cent->currentState.powerups);
    }

  // make sure we aren't looking at cg.predictedPlayerEntity for LG
  nonPredictedCent = &cg_entities[cent->currentState.clientNum];

  // add the flash
  if (!cg_muzzleflash->integer
      && (cent->currentState.clientNum == cg.snap->ps.clientNum))
    {
      // could add weaponNum == WP_GAUNTLET || weaponNum == WP_GRAPPLING_HOOK
      if ((weaponNum == WP_LIGHTNING)
	  && (nonPredictedCent->currentState.eFlags & EF_FIRING))
	{
	  // continuous flash
	}
      else
	{
	  return;
	}
    }
  else
    {
      if (((weaponNum == WP_LIGHTNING) || (weaponNum == WP_GAUNTLET))
	  && (nonPredictedCent->currentState.eFlags & EF_FIRING))
	{
	  // continuous flash
	}
      else
	{
	  // impulse flash
	  if (((cg.levelTime - cent->pe.muzzleFlashTime) > MUZZLE_FLASH_TIME))
	    {
	      return;
	    }
	}
    }

  Com_Memset (&flash, 0, sizeof (flash));
  VectorCopy (parent->lightingOrigin, flash.lightingOrigin);
  flash.shadowPlane = parent->shadowPlane;
  flash.renderfx = parent->renderfx;

  flash.hModel = weapon->flashModel;
  if (!flash.hModel)
    {
      return;
    }
  angles[YAW] = 0;
  angles[PITCH] = 0;
  angles[ROLL] = crandom () * 10;
  AnglesToAxis (angles, flash.axis);

  // colorize the railgun blast
  if (weaponNum == WP_RAILGUN)
    {
      vec4_t c1;
      vec4_t c2;

      CG_ColorizeRail (cent->currentState.clientNum, c1, c2);

      flash.shaderRGBA[0] = 255 * c1[0];
      flash.shaderRGBA[1] = 255 * c1[1];
      flash.shaderRGBA[2] = 255 * c1[2];
      flash.shaderRGBA[3] = 255 * c1[3];
    }

  CG_PositionRotatedEntityOnTag (&flash, &gun, weapon->weaponModel,
				 "tag_flash");
  cgl->R_AddRefEntityToScene (&flash);

  if ((ps && ((ps->weapon == WP_LIGHTNING)
	      && ((ps->weaponstate == WEAPON_READY)
		  || (ps->weaponstate == WEAPON_FIRING))))
      || (cg.renderingThirdPerson)
      || (cent->currentState.number != cg.predictedPlayerState.clientNum))
    {
      if (cent->currentState.weapon == WP_LIGHTNING)
	{
	  // add lightning bolt
	  CG_LightningBolt (nonPredictedCent, flash.origin);

	  if (weapon->flashDlightColor[0] || weapon->flashDlightColor[1]
	      || weapon->flashDlightColor[2])
	    {
	      cgl->R_AddLightToScene (flash.origin, 300 + (rand () & 31),
				      weapon->flashDlightColor[0],
				      weapon->flashDlightColor[1],
				      weapon->flashDlightColor[2]);
	    }
	}
    }
}

/*
==============
CG_AddViewWeapon

Add the weapon, and flash for the player's view
==============
*/
void
CG_AddViewWeapon (playerState_t *ps)
{
  refEntity_t hand;
  centity_t *cent;
  clientInfo_t *ci;
  float fovOffset;
  vec3_t angles;
  weaponInfo_t *weapon;

  if (ps->persistant[PERS_TEAM] == TEAM_SPECTATOR)
    {
      return;
    }

  if (ps->pm_type == PM_INTERMISSION)
    {
      return;
    }

  // no gun if in third person view
  if (cg.renderingThirdPerson)
    {
      return;
    }

  // allow the gun to be completely removed
  if (!cg_drawGun->integer)
    {
      vec3_t origin;

      // if trueLightning is 1 allow firing effect unsynchronised
      // with the server
      if ((cg.predictedPlayerState.weapon == WP_LIGHTNING)
	  && (cg.predictedPlayerState.weaponstate == WEAPON_FIRING)
	  && (((cg.predictedPlayerState.eFlags & EF_FIRING)
	       && (cg_trueLightning->integer))
	      || (cg.snap->ps.eFlags & EF_FIRING)))
	{
	  // special hack for lightning gun...
	  VectorCopy (cg.refdef.vieworg, origin);
	  VectorMA (origin, -8, cg.refdef.viewaxis[2], origin);
	  CG_LightningBolt (&cg_entities[ps->clientNum], origin);
	}
      return;
    }

  // don't draw if testing a gun model
  if (cg.testGun)
    {
      return;
    }

  // drop gun lower at higher fov
  if (cg_fov->integer > 90)
    {
      fovOffset = -0.2 * (cg_fov->integer - 90);
    }
  else
    {
      fovOffset = 0;
    }

  cent = &cg.predictedPlayerEntity;	// &cg_entities[cg.snap->ps.clientNum];
  // shouldn't be needed as we have registered item on map launch
  // but to be sure to avoid any segfault
  CG_RegisterWeapon (ps->weapon);
  weapon = &cg_weapons[ps->weapon];

  Com_Memset (&hand, 0, sizeof (hand));

  // set up gun position
  if (cg_drawGun->integer != 2)
    {
      CG_CalculateWeaponPosition (hand.origin, angles);
    }
  else
    {		// drawGun 2, because 0 was already checked
      VectorCopy (cg.refdef.vieworg, hand.origin);
      VectorCopy (cg.refdefViewAngles, angles);
    }

  VectorMA (hand.origin, cg_gun_x->value, cg.refdef.viewaxis[0], hand.origin);
  VectorMA (hand.origin, cg_gun_y->value, cg.refdef.viewaxis[1], hand.origin);
  VectorMA (hand.origin, (cg_gun_z->value + fovOffset), cg.refdef.viewaxis[2],
	    hand.origin);

  AnglesToAxis (angles, hand.axis);

  // map torso animations to weapon animations
  // get clientinfo for animation map
  ci = &cgs.clientinfo[cent->currentState.clientNum];
  hand.frame = CG_MapTorsoToWeaponFrame (ci, cent->pe.torso.frame);
  hand.oldframe = CG_MapTorsoToWeaponFrame (ci, cent->pe.torso.oldFrame);
  hand.backlerp = cent->pe.torso.backlerp;

  hand.hModel = weapon->handsModel;
  hand.renderfx = RF_DEPTHHACK | RF_FIRST_PERSON | RF_MINLIGHT;

  // add everything onto the hand
  CG_AddPlayerWeapon (&hand, ps, &cg.predictedPlayerEntity,
		      ps->persistant[PERS_TEAM]);
}

/*
==============================================================================

WEAPON SELECTION

==============================================================================
*/

/*
===================
CG_DrawWeaponSelect
===================
*/
extern const int ammowarning_table[WP_NUM_WEAPONS];
void
CG_DrawWeaponSelect (void)
{
  int i;
  float color[4];
  float fadecolor;

  // don't display if dead
  if (cg.predictedPlayerState.stats[STAT_HEALTH] <= 0)
    {
      return;
    }

  // fade
  if (ch_weaponselectFadeTime->integer > 0)
    {
      fadecolor = CG_FadeColor2 (cg.weaponSelectTime, ch_weaponselectFadeTime->integer);
      if (!fadecolor)
	{
	  // nothing to draw
	  return;
	}
    }

  color[0] = color[1] = color[2] = color[3] = 1.0;
  cgl->R_SetColor (color);

  // showing weapon select clears pickup item display, but not the blend blob
  cg.itemPickupTime = 0;

  // little hack : when spectating or playing demos
  // don't use clientside cg.weaponSelect value but the one from playerstate
  if (cg.demoPlayback || (cg.clientNum != cg.snap->ps.clientNum))
    {
      cg.weaponSelect = cg.snap->ps.weapon;
    }

  if (ch_weaponselectStyle->integer == 1)
    {
      const char *p;
      int bits;
      int count;
      int x, y, w;
      char buff[4];
      int val;

      // count the number of weapons or weapon ammos owned
      bits = cg.snap->ps.stats[STAT_WEAPONS];
      count = 0;
      for (i = 1; i < WP_NUM_WEAPONS; i++)
	{
	  if (bits & (1 << i) || cg.snap->ps.ammo[i])
	    {
	      count++;
	    }
	}

      // allowing custom position format : "XxY"; X, Y coordinates
      p = ch_weaponselectPosition->string;
      if (p && (*p >= '0') && (*p <= '9'))
	{
	  x = atoi (p);
	  if ((x <= 0) || (x >= SCREEN_HEIGHT))
	    {
	      x = 320 - count * 40/2;
	    }

	  while (*p && (*p != 'x'))
	    {
	      p++;
	    }

	  p++;
	  if ((*p >= '0') && (*p <= '9'))
	    {
	      y = atoi (p);
	      if ((y <= 0) || (y >= SCREEN_WIDTH))
		{
		  y = 380;
		}
	    }
	  else
	    {
	      y = 380;
	    }
	}
      else
	{
	  // set up initial position
	  x = 320 - count * 20;
	  y = 380;
	}

      // draw each weapon
      for (i = 1; i < WP_NUM_WEAPONS; i++)
	{
	  // skip if we have neither weapon or ammos
	  if (!(bits & (1 << i)) && !cg.snap->ps.ammo[i])
	    {
	      continue;
	    }

	  // we might have a "give all" adding unregistered items
	  CG_RegisterWeapon ((weapon_t) i);

	  // draw weapon icon
	  CG_DrawPic (x, y, 32, 32, cg_weapons[i].weaponIcon);

	  // draw selection marker
	  if (i == cg.weaponSelect)
	    {
	      CG_DrawPic (x - 4, y - 4, 40, 40, cgs.media.selectShader);
	    }

	  // no ammo cross on top
	  if (!cg.snap->ps.ammo[i] || (cg.snap->ps.ammo[i] && !(bits & (1 << i))))
	    {
	      CG_DrawPic (x, y, 32, 32, cgs.media.noammoShader);
	    }

	  // draw the number of ammo for each weapon
	  if (i == WP_GAUNTLET)
	    {
	      x += 40;
	      continue;
	    }

	  val = cg.snap->ps.ammo[i];

	  // we have ammos but not the weapon, make it light blue
	  if (!(bits & (1 << i)))
	    {
	      color[0] = 0.5;
	      color[1] = 0.8;
	      color[2] = 1.0;
	      color[3] = 0.7;
	      cgl->R_SetColor (color);
	    }
	  // we have the weapon but no more ammos, draw it in red
	  else if (val == 0)
	    {
	      color[0] = 1;
	      color[1] = 0.1;
	      color[2] = 0.1;
	      color[3] = 1.0;
	      cgl->R_SetColor (color);
	    }
	  else if (val <= ammowarning_table[i])
	    {
	      // orange
	      color[0] = 1;
	      color[1] = 0.9;
	      color[2] = 0.0;
	      color[3] = 1.0;
	      cgl->R_SetColor (color);
	    }
	  // otherwise keep the white coloring

	  if (val >= 100)
	    val = 99;

	  Com_sprintf (buff, sizeof (buff), "%i", val);

	  // check for buff values !!!!
	  if (!Q_isanumber ((const char *) &buff[0]))
	    {
	      buff[0] = '.';
	    }
	  if (!Q_isanumber ((const char *) &buff[1]))
	    {
	      buff[1] = '.';
	    }

	  CG_DrawChar (x, y + 16, 16, 16, buff[0]);
	  CG_DrawChar (x + 16, y + 16, 16, 16, buff[1]);

	  color[0] = color[1] = color[2] = color[3] = 1.0;
	  cgl->R_SetColor (color);

	  x += 40;
	}

      // draw the selected name
      if (0)
	{
	  char *name;
	  if (cg_weapons[cg.weaponSelect].item)
	    {
	      name = cg_weapons[cg.weaponSelect].item->pickup_name;
	      if (name)
		{
		  w = CG_DrawStrlen (name) * BIGCHAR_WIDTH;
		  x = (SCREEN_WIDTH - w) / 2;
		  CG_DrawBigStringColor (x, y - 22, name, color);
		}
	    }
	}
    }
  // we will have a bit of duplicated code but it really doesn't matter
  // this one draws an horizontal bar a bit like the first one but it's wider
  else if (ch_weaponselectStyle->integer == 2)
    {
      const char *p;
      int bits;
      int count;
      int x, y;
      char buff[4];
      int val; // this one is used multiple times
      int size_x, size_y;
      int icon_x, icon_y;
      int interline;

      // count the number of weapons owned (we don't care about ammos)
      bits = cg.snap->ps.stats[STAT_WEAPONS];
      count = 0;
      val = 0;

      if (ch_weaponselectDrawAll->integer)
	{
	  weaponInfo_t *weaponInfo;

	  for (i = 1; i < WP_NUM_WEAPONS; i++)
	    {
	      weaponInfo = &cg_weapons[i];
	      if (weaponInfo->registered)
		{
		  count++;

		  if (cg.snap->ps.ammo[i] >= 100)
		    val += 3;
		  else
		    val += 2;
		}
	    }
	}
      else
	{
	  for (i = 1; i < WP_NUM_WEAPONS; i++)
	    {
	      if (bits & (1 << i))
		{
		  count++;

		  if (cg.snap->ps.ammo[i] >= 100)
		    val += 3;
		  else
		    val += 2;
		}
	    }
	}

      // adjust a bit because gauntlet isn't taking a lot of space
      // and we didn't estimate the selected item size (which is bigger)
      val += 2;
      count += 2;

      // allowing custom position format : "XxY"; X, Y coordinates
      p = ch_weaponselectPosition->string;
      if (p && (*p >= '0') && (*p <= '9'))
	{
	  x = atoi (p);
	  if ((x <= 0) || (x >= SCREEN_HEIGHT))
	    {
	      //x = 320 - count * 64/2;
	      x = 320 - (val / 2) * 12 - (count / 2) * 16;
	    }
	  else
	    {
	      //x -= count * 64/2;
	      x -= (val / 2) * 12 - (count / 2) * 16;
	    }

	  while (*p && (*p != 'x'))
	    {
	      p++;
	    }

	  p++;
	  if ((*p >= '0') && (*p <= '9'))
	    {
	      y = atoi (p);
	      if ((y <= 0) || (y >= SCREEN_WIDTH))
		{
		  y = 380;
		}
	    }
	  else
	    {
	      y = 380;
	    }
	}
      else
	{
	  // set up initial position
	  //x = 320 - count * 64/2;
	  x = 320 - (val / 2) * 12 - (count / 2) * 16;
	  y = 380;
	}

      // distance between the previous weapon's ammo numbers and the next icon
      interline = 8;

      // draw each weapon
      for (i = 1; i < WP_NUM_WEAPONS; i++)
	{
	  if (ch_weaponselectDrawAll->integer)
	    {
	      weaponInfo_t *weaponInfo;

	      weaponInfo = &cg_weapons[i];
	      if (!weaponInfo->registered)
		continue;
	    }
	  else
	    {
	      if (!(bits & (1 << i)) && !cg.snap->ps.ammo[i])
		{
		  continue;
		}
	    }

	  // we might have a "give all" adding unregistered items
	  CG_RegisterWeapon ((weapon_t) i);

	  color[0] = color[1] = color[2] = 1.0;

	  // draw weapon icon
	  if (i == cg.weaponSelect)
	    {
	      // current selected is a bigger
	      size_x = size_y = 20;
	      icon_x = icon_y = 32;
	      CG_DrawPic (x, y - 4, icon_x, icon_y, cg_weapons[i].weaponIcon);
	      // draw text clearly
	      color[3] = 1.0;
	    }
	  else
	    {
	      // draw the picture a bit bigger than the text
	      size_x = size_y = 12;
	      icon_x = icon_y = 16;
	      CG_DrawPic (x, y, icon_x, icon_y, cg_weapons[i].weaponIcon);
	      // a bit of transparency for the text
	      color[3] = 0.5;
	    }

	  /*
	  // draw selection marker
	  if (i == cg.weaponSelect)
	    {
	      CG_DrawPic (x - 4, y - 4, 64, 16, cgs.media.yellowboxShader);
	    }
	  */

	  // no ammo cross on top
	  // also draw it if we have ammos but not the weapon
	  val = cg.snap->ps.ammo[i];
	  if (!val || (val && !(bits & (1 << i))))
	    {
	      if (i == cg.weaponSelect)
		CG_DrawPic (x, y - 4, icon_x, icon_y, cgs.media.noammoShader);
	      else
		CG_DrawPic (x, y, icon_x, icon_y, cgs.media.noammoShader);
	    }

	  // draw the number of ammo for each weapon
	  if (i == WP_GAUNTLET)
	    {
	      x += icon_x + 2 * interline;
	      continue;
	    }

	  // we have ammos but not the weapon, make it orange/yellow
	  /*
	  if (!(bits & (1 << i)))
	    {
	      color[0] = 1.0;
	      color[1] = 0.9;
	      color[2] = 0.0;
	      color[3] = 0.5;
	      cgl->R_SetColor (color);
	    }
	  */

	  // we have the weapon but no more ammos, draw it in red
	  if (val == 0)
	    {
	      color[0] = 1;
	      color[1] = 0.1;
	      color[2] = 0.1;
	    }
	  // low on ammos for this one
	  else if (val <= ammowarning_table[i])
	    {
	      // orange
	      color[0] = 1;
	      color[1] = 0.9;
	      color[2] = 0.0;
	    }
	  // otherwise keep the white coloring

	  if (val >= 1000)
	    val = 999;

	  Com_sprintf (buff, sizeof (buff), "%d", val);

	  // adjust a bit to keep it centered
	  if (i == cg.weaponSelect)
	    {
	      CG_DrawStringExt (x + icon_x, y + 2, buff, color, false, true,
				size_x, size_y, 0);
	    }
	  else
	    {
	      CG_DrawStringExt (x + icon_x + 2, y, buff, color, false, true,
				size_x, size_y, 0);
	    }

	  // reuse this variable
	  if (val < 100)
	    val = 2;
	  else
	    val = 3;

	  if (i == cg.weaponSelect)
	    {
	      x += icon_x + (val * size_x) + interline;
	    }
	  else
	    {
	      x += icon_x + (val * size_x) + interline;
	    }
	}
    }
  // this one is drawn vertically
  else if (ch_weaponselectStyle->integer == 3)
    {
      const char *p;
      int bits;
      int count;
      int x, y;
      char buff[4];
      int val;
      int size_x, size_y;
      int icon_x, icon_y;
      int interline;

      // count the number of weapons owned (we don't care about ammos)
      bits = cg.snap->ps.stats[STAT_WEAPONS];
      count = 0;

      if (ch_weaponselectDrawAll->integer)
	{
	  weaponInfo_t *weaponInfo;

	  for (i = 1; i < WP_NUM_WEAPONS; i++)
	    {
	      weaponInfo = &cg_weapons[i];
	      if (weaponInfo->registered)
		{
		  count++;
		}
	    }
	}
      else
	{
	  for (i = 1; i < WP_NUM_WEAPONS; i++)
	    {
	      if (bits & (1 << i) || cg.snap->ps.ammo[i])
		{
		  count++;
		}
	    }
	}

      // allowing custom position format : "XxY"; X, Y coordinates
      p = ch_weaponselectPosition->string;
      if (p && (*p >= '0') && (*p <= '9'))
	{
	  x = atoi (p);
	  if ((x <= 0) || (x >= SCREEN_WIDTH))
	    {
	      x = 10;
	    }

	  while (*p && (*p != 'x'))
	    {
	      p++;
	    }

	  p++;
	  if ((*p >= '0') && (*p <= '9'))
	    {
	      y = atoi (p);
	      if ((y <= 0) || (y >= SCREEN_HEIGHT))
		{
		  y = 240 - count * 32/2;
		}
	      else
		{
		  y -= count * 32/2;
		}
	    }
	  else
	    {
	      y = 240 - count * 32/2;
	    }
	}
      else
	{
	  // set up initial position
	  x = 10;
	  y = 240 - count * 32/2;
	}

      // initial values
      interline = 22;

      CG_DrawTopBottom (x, y - 8, 64, count * interline + 8, 1);

      // draw each weapon
      for (i = 1; i < WP_NUM_WEAPONS; i++)
	{
	  if (ch_weaponselectDrawAll->integer)
	    {
	      weaponInfo_t *weaponInfo;

	      weaponInfo = &cg_weapons[i];
	      if (!weaponInfo->registered)
		continue;
	    }
	  else
	    {
	      if (!(bits & (1 << i)) && !cg.snap->ps.ammo[i])
		{
		  continue;
		}
	    }

	  // we have unregistered items to draw
	  CG_RegisterWeapon ((weapon_t) i);

	  color[0] = color[1] = color[2] = 1.0;

	  // draw weapon icon
	  if (i == cg.weaponSelect)
	    {
	      size_x = size_y = 18;
	      icon_x = icon_y = 22;
	      CG_DrawPic (x, y - 4, icon_x, icon_y, cg_weapons[i].weaponIcon);
	      // draw text clearly
	      color[3] = 1.0;
	    }
	  else
	    {
	      // draw the picture a bit bigger
	      size_x = size_y = 12;
	      icon_x = icon_y = 16;
	      CG_DrawPic (x, y, icon_x, icon_y, cg_weapons[i].weaponIcon);
	      // a bit of transparency for the text
	      color[3] = 0.5;
	    }

	  /*
	  // draw selection marker
	  if (i == cg.weaponSelect)
	    {
	      CG_DrawPic (x - 4, y - 4, 64, 16, cgs.media.yellowboxShader);
	    }
	  */
	  // no ammo cross on top
	  val = cg.snap->ps.ammo[i];
	  if (!val || (val && !(bits & (1 << i))))
	    {
	      if (i == cg.weaponSelect)
		CG_DrawPic (x, y - 4, icon_x, icon_y, cgs.media.noammoShader);
	      else
		CG_DrawPic (x, y, icon_x, icon_y, cgs.media.noammoShader);
	    }

	  // draw the number of ammo for each weapon
	  if (i == WP_GAUNTLET)
	    {
	      y += interline;
	      continue;
	    }

	  // we have ammos but not the weapon, make it orange/yellow
	  /*
	  if (!(bits & (1 << i)))
	    {
	      color[0] = 1.0;
	      color[1] = 0.9;
	      color[2] = 0.0;
	      color[3] = 0.5;
	      cgl->R_SetColor (color);
	    }
	  */

	  // we have the weapon but no more ammos, draw it in red
	  if (val == 0)
	    {
	      color[0] = 1;
	      color[1] = 0.1;
	      color[2] = 0.1;
	    }
	  else if (val <= ammowarning_table[i])
	    {
	      // orange
	      color[0] = 1;
	      color[1] = 0.9;
	      color[2] = 0.0;
	    }
	  // otherwise keep the white coloring

	  if (val >= 1000)
	    val = 999;

	  Com_sprintf (buff, sizeof (buff), "%d", val);

	  // adjust a bit to keep it centered
	  if (i == cg.weaponSelect)
	    {
	      CG_DrawStringExt (x + icon_x + 6, y - 4, buff, color, false, true,
				size_x, size_y, 0);
	    }
	  else
	    {
	      CG_DrawStringExt (x + icon_x + 6, y, buff, color, false, true,
				size_x, size_y, 0);
	    }

	  y += interline;
	}
    }

  cgl->R_SetColor (NULL);
}

/*
===============
CG_WeaponSelectable
===============
*/
static bool
CG_WeaponSelectable (int i)
{
  if (!cg.snap->ps.ammo[i])
    {
      return false;
    }
  if (!(cg.snap->ps.stats[STAT_WEAPONS] & (1 << i)))
    {
      return false;
    }

  return true;
}

/*
===============
CG_NextWeapon_f
===============
*/
void
CG_NextWeapon_f (void)
{
  int i;
  int original;

  if (!cg.snap)
    {
      return;
    }
  if (cg.snap->ps.pm_flags & PMF_FOLLOW)
    {
      return;
    }

  cg.weaponSelectTime = cg.time;
  original = cg.weaponSelect;

  for (i = 0; i < WP_NUM_WEAPONS; i++)
    {
      cg.weaponSelect++;
      if (cg.weaponSelect == WP_NUM_WEAPONS)
	{
	  cg.weaponSelect = 0;
	}

      if (cg.weaponSelect == WP_GAUNTLET)
	{
	  continue;		// never cycle to gauntlet
	}

      if (CG_WeaponSelectable (cg.weaponSelect))
	{
	  break;
	}
    }

  if (i == WP_NUM_WEAPONS)
    {
      cg.weaponSelect = original;
    }
}

/*
===============
CG_PrevWeapon_f
===============
*/
void
CG_PrevWeapon_f (void)
{
  int i;
  int original;

  if (!cg.snap)
    {
      return;
    }
  if (cg.snap->ps.pm_flags & PMF_FOLLOW)
    {
      return;
    }

  cg.weaponSelectTime = cg.time;
  original = cg.weaponSelect;

  for (i = 0; i < WP_NUM_WEAPONS; i++)
    {
      cg.weaponSelect--;
      if (cg.weaponSelect == -1)
	{
	  cg.weaponSelect = WP_NUM_WEAPONS - 1;
	}

      if (cg.weaponSelect == WP_GAUNTLET)
	{
	  continue;		// never cycle to gauntlet
	}

      if (CG_WeaponSelectable (cg.weaponSelect))
	{
	  break;
	}
    }

  if (i == WP_NUM_WEAPONS)
    {
      cg.weaponSelect = original;
    }
}

/*
===============
CG_Weapon_f

Take the first argument and try to change to the given weapon
===============
*/
void
CG_Weapon_f (void)
{
  int num;

  if (!cg.snap)
    {
      return;
    }
  if (cg.snap->ps.pm_flags & PMF_FOLLOW)
    {
      return;
    }

  num = atoi (cgl->Argv (1));

  if ((num < 1) || (num > MAX_WEAPONS - 1))
    {
      return;
    }

  cg.weaponSelectTime = cg.time;

  // check if the player has the the weapon
  if (!(cg.snap->ps.stats[STAT_WEAPONS] & (1 << num)))
    {
      return;
    }

  // check if we have ammos or want to switch even without ammo
  if (cg_noammoswitch->integer || (cg.snap->ps.ammo[num] > 0)
      || (num == WP_GAUNTLET))
    {
      cg.weaponSelect = num;
    }
  /*
     else
     return;
   */
}

/*
===============
CG_WeaponList_f

change to the first valid listed weapon (ex : weapon 5 3 2)
noammoswitch allows to select the weapon even without ammo
if carrying a weapon without ammo and trying to switch to the same weapon
the second weapon (if there is) will be tried for switch
===============
*/
void
CG_WeaponList_f (void)
{
  int num;
  int nargs;
  int i;

  if (!cg.snap)
    {
      return;
    }
  if (cg.snap->ps.pm_flags & PMF_FOLLOW)
    {
      return;
    }

  // nargs is a least 1 (the command name)
  nargs = cgl->Argc ();
  // no arguments to the function, no work to do
  if (nargs < 2)
    {
      return;
    }

  for (i = 1; i < nargs; i++)
    {
      num = atoi (cgl->Argv (i));

      // if this argument is non valid, check the next one
      if ((num < 1) || (num > WP_NUM_WEAPONS - 1))
	{
	  continue;
	}

      cg.weaponSelectTime = cg.time;

      // check whether the player has the wanted weapon, else check the next one
      if (!(cg.snap->ps.stats[STAT_WEAPONS] & (1 << num)))
	{
	  continue;
	}
      // change to weapon number $num
      else
	{
	  // cycle if the player carries the first listed weapon and has no ammo
	  if ((cg.snap->ps.weapon == num) && (cg.snap->ps.ammo[num] <= 0)
	      && (nargs > 2))
	    {
	      continue;
	    }

	  // check if we have ammos or want to switch even without ammo
	  if (cg_noammoswitch->integer || (cg.snap->ps.ammo[num] > 0)
	      || (num == WP_GAUNTLET))
	    {
	      cg.weaponSelect = num;
	      return;
	    }
	  else
	    {
	      continue;
	    }
	}
    }
}

/*
===============
CG_WeaponCircular_f

change to the first valid weapon next with a circular permutation to the right
if current weapon is listed, start switching from this argument
===============
*/
void
CG_WeaponCircular_f (void)
{
  int num;
  int nargs;
  int i;
  int index;

  if (!cg.snap)
    {
      return;
    }
  if (cg.snap->ps.pm_flags & PMF_FOLLOW)
    {
      return;
    }

  nargs = cgl->Argc ();
  // no arguments to the function, no work to do
  if (nargs < 2)
    {
      return;
    }

  // check if the current weapon is listed
  index = 0;
  for (i = 1; i < nargs; i++)
    {
      if (atoi (cgl->Argv (i)) == cg.snap->ps.weapon)
	{
	  index = i;
	  break;
	}
    }

  cg.weaponSelectTime = cg.time;

  // i is only used as a counter
  for (i = 1; i < nargs; i++)
    {
      // go to the next argument, initial value is 0
      // or the argument number the weapon we own, everything is ok
      index++;
      // if we are at the end of the list, cycle to the 1st element
      if (index == nargs)
	index = 1;

      num = atoi (cgl->Argv (index));
      // check whether num is valid, otherwise cycle to the next argument
      if ((num < 1) || (num > WP_NUM_WEAPONS - 1))
	{
	  continue;
	}

      /*
         // the player already has this weapon active, cycle to the next weapon
         // note : this should not happen
         if (cg.snap->ps.weapon == num)
         {
         continue;
         }
       */

      // check for if the player has the weapon
      if (!(cg.snap->ps.stats[STAT_WEAPONS] & (1 << num)))
	{
	  continue;
	}
      else
	{
	  // check if we have ammos or want to switch even without ammo
	  if (cg_noammoswitch->integer || (cg.snap->ps.ammo[num] > 0)
	      || (num == WP_GAUNTLET))
	    {
	      cg.weaponSelect = num;
	      return;
	    }
	  else
	    {
	      continue;
	    }
	}
    }
}

/*
===================
CG_OutOfAmmoChange

The current weapon has just run out of ammo
===================
*/
void
CG_OutOfAmmoChange (void)
{
  int i;
  int nargs;
  int num;
  char str[MAX_CVAR_VALUE_STRING];
  char *ptr;

  // we don't want to switch
  if (cg_noweaponswitchonevent->integer)
    {
      if (cg_noweaponswitchonevent->integer == 2)
	cgl->snd->StartLocalSound (cgs.media.noAmmoSound, CHAN_LOCAL_SOUND);

      return;
    }

  cg.weaponSelectTime = cg.time;

  Q_strncpyz (str, cg_autoswitchorder->string, sizeof (str));

  // parsing the dev_cg_autoswitchorder
  for (ptr = str; *ptr; ptr++)
    {
      // check for non valid characters
      if (*ptr == ' ')
	continue;

      // atoi () returns 0 if non valid
      num = atoi (ptr);

      // check whether num is valid, otherwise cycle to the next argument
      if ((num < 1) || (num > WP_NUM_WEAPONS - 1))
	{
	  continue;
	}

      // check for if the player has the weapon
      if (!(cg.snap->ps.stats[STAT_WEAPONS] & (1 << num)))
	{
	  continue;
	}

      // check for ammos
      if ((cg.snap->ps.ammo[num] > 0) || (num == WP_GAUNTLET))
	{
	  cg.weaponSelect = num;
	  return;
	}
      else
	{
	  continue;
	}
    }

  // if we didn't get it from the string
  // use the legacy autoswitch
  for (i = WP_NUM_WEAPONS - 1; i > 0; i--)
    {
      if (CG_WeaponSelectable (i))
	{
	  cg.weaponSelect = i;
	  break;
	}
    }
}

/*
===================================================================================================

WEAPON EVENTS

===================================================================================================
*/

/*
================
CG_FireWeapon

Caused by an EV_FIRE_WEAPON event
================
*/
void
CG_FireWeapon (centity_t *cent)
{
  entityState_t *ent;
  int c;
  weaponInfo_t *weap;

  ent = &cent->currentState;
  if (ent->weapon == WP_NONE)
    {
      return;
    }
  if (ent->weapon >= WP_NUM_WEAPONS)
    {
      CG_Error ("CG_FireWeapon: ent->weapon >= WP_NUM_WEAPONS");
      return;
    }

  weap = &cg_weapons[ent->weapon];

  // mark the entity as muzzle flashing, so when it is added it will
  // append the flash to the weapon model
  // TODO : understand why this isn't correctly updated
  // for 3rd person players when getting hit
  cent->pe.muzzleFlashTime = cg.levelTime;
  cg_entities[ent->number].pe.muzzleFlashTime = cg.levelTime;

  // lightning gun only does this this on initial press
  if (ent->weapon == WP_LIGHTNING)
    {
      if (cent->pe.lightningFiring)
	{
	  return;
	}
    }

  // play quad sound if needed
  if (cent->currentState.powerups & (1 << PW_QUAD))
    {
      cgl->snd->StartSound (NULL, cent->currentState.number, CHAN_ITEM,
			    cgs.media.quadSound);
    }

  // play a sound
  for (c = 0; c < 4; c++)
    {
      if (!weap->flashSound[c])
	{
	  break;
	}
    }

  if (c > 0)
    {
      c = rand () % c;
      if (weap->flashSound[c])
	{
	  cgl->snd->StartSound (NULL, ent->number, CHAN_WEAPON,
				weap->flashSound[c]);
	}
    }

  // do brass ejection
  if (weap->ejectBrassFunc && cg_brassTime->integer > 0)
    {
      weap->ejectBrassFunc (cent);
    }
}

/*
=================
CG_MissileHitWall

Caused by an EV_MISSILE_MISS event, or directly by local bullet tracing
=================
*/
void
CG_MissileHitWall (weapon_t weapon, int clientNum, vec3_t origin, vec3_t dir,
		   impactSound_t soundType)
{
  qhandle_t mod;
  qhandle_t mark;
  qhandle_t shader;
  sfxHandle_t sfx;
  float radius;
  float light;
  vec3_t lightColor;
  localEntity_t *le;
  int r;
  bool alphaFade;
  bool isSprite;
  int duration;
  vec3_t sprOrg;
  vec3_t sprVel;

  mark = 0;
  radius = 32;
  sfx = 0;
  mod = 0;
  shader = 0;
  light = 0;
  lightColor[0] = 1;
  lightColor[1] = 1;
  lightColor[2] = 0;

  // set defaults
  isSprite = false;
  duration = 600;

  switch (weapon)
    {
    default:
    case WP_LIGHTNING:
      // no explosion at LG impact, it is added with the beam
      r = rand () & 3;
      if (r < 2)
	{
	  sfx = cgs.media.sfx_lghit2;
	}
      else if (r == 2)
	{
	  sfx = cgs.media.sfx_lghit1;
	}
      else
	{
	  sfx = cgs.media.sfx_lghit3;
	}
      mark = cgs.media.holeMarkShader;
      radius = 12;
      break;

    case WP_GRENADE_LAUNCHER:
      mod = cgs.media.dishFlashModel;
      shader = cgs.media.grenadeExplosionShader;
      sfx = cgs.media.sfx_rockexp;
      mark = cgs.media.burnMarkShader;
      radius = 64;
      light = 300;
      isSprite = true;
      break;

    case WP_ROCKET_LAUNCHER:
      mod = cgs.media.dishFlashModel;
      shader = cgs.media.rocketExplosionShader;
      sfx = cgs.media.sfx_rockexp;
      mark = cgs.media.burnMarkShader;
      radius = 64;
      light = 300;
      isSprite = true;
      duration = 1000;
      lightColor[0] = 1;
      lightColor[1] = 0.75;
      lightColor[2] = 0.0;
      /*
         if (cg_oldRocket->integer == 0) {
         // explosion sprite animation
         VectorMA( origin, 24, dir, sprOrg );
         VectorScale( dir, 64, sprVel );

         CG_ParticleExplosion( "explode1", sprOrg, sprVel, 1400, 20, 30 );
         }
       */
      break;

    case WP_RAILGUN:
      mod = cgs.media.ringFlashModel;
      shader = cgs.media.railExplosionShader;
      sfx = cgs.media.sfx_plasmaexp;
      mark = cgs.media.energyMarkShader;
      radius = 24;
      break;

    case WP_PLASMAGUN:
      mod = cgs.media.ringFlashModel;
      shader = cgs.media.plasmaExplosionShader;
      sfx = cgs.media.sfx_plasmaexp;
      mark = cgs.media.energyMarkShader;
      radius = 16;
      break;

    case WP_BFG:
      mod = cgs.media.dishFlashModel;
      shader = cgs.media.bfgExplosionShader;
      sfx = cgs.media.sfx_rockexp;
      mark = cgs.media.burnMarkShader;
      radius = 32;
      isSprite = true;
      break;

    case WP_SHOTGUN:
      mod = cgs.media.bulletFlashModel;
      shader = cgs.media.bulletExplosionShader;
      mark = cgs.media.bulletMarkShader;
      sfx = 0;
      radius = 4;
      break;

    case WP_MACHINEGUN:
      mod = cgs.media.bulletFlashModel;
      shader = cgs.media.bulletExplosionShader;
      mark = cgs.media.bulletMarkShader;

      r = rand () & 3;
      if (r == 0)
	{
	  sfx = cgs.media.sfx_ric1;
	}
      else if (r == 1)
	{
	  sfx = cgs.media.sfx_ric2;
	}
      else
	{
	  sfx = cgs.media.sfx_ric3;
	}
      radius = 8;
      break;
    }

  if (sfx)
    {
      cgl->snd->StartSound (origin, ENTITYNUM_WORLD, CHAN_AUTO, sfx);
    }

  //
  // create the explosion
  //
  if (mod)
    {
      le = CG_MakeExplosion (origin, dir, mod, shader, duration, isSprite);
      le->light = light;
      VectorCopy (lightColor, le->lightColor);
      if (weapon == WP_RAILGUN)
	{
	  // colorize with client color
	  VectorCopy (cgs.clientinfo[clientNum].color1, le->color);
	}
    }

  //
  // impact mark
  //
  alphaFade = (mark == cgs.media.energyMarkShader);	// plasma fades alpha, all others fade color
  if (weapon == WP_RAILGUN)
    {
      float *color;
      vec4_t c1;
      vec4_t c2;

      CG_ColorizeRail (clientNum, c1, c2);

      color = c2;
      CG_ImpactMark (mark, origin, dir, random () * 360, color[0], color[1],
		     color[2], 1.0f, alphaFade, radius, false);
    }
  else
    {
      CG_ImpactMark (mark, origin, dir, random () * 360, 1, 1, 1, 1,
		     alphaFade, radius, false);
    }
}

/*
=================
CG_MissileHitPlayer
=================
*/
void
CG_MissileHitPlayer (weapon_t weapon, vec3_t origin, vec3_t dir,
		     int entityNum)
{
  CG_Bleed (origin, entityNum);

  // some weapons will make an explosion with the blood, while
  // others will just make the blood
  switch (weapon)
    {
    case WP_GRENADE_LAUNCHER:
    case WP_ROCKET_LAUNCHER:
      CG_MissileHitWall (weapon, 0, origin, dir, IMPACTSOUND_FLESH);
      break;
    default:
      break;
    }
}

/*
============================================================================

SHOTGUN TRACING

============================================================================
*/

/*
================
CG_ShotgunPellet
================
*/
static void
CG_ShotgunPellet (vec3_t start, vec3_t end, int skipNum)
{
  trace_t tr;
  trace_t trace;
  int sourceContentType, destContentType;

  CG_Trace (&tr, start, NULL, NULL, end, skipNum, MASK_SHOT);

  sourceContentType = cgl->CM_PointContents (start, 0);
  destContentType = cgl->CM_PointContents (tr.endpos, 0);

  // do a complete bubble trail
  if (sourceContentType == destContentType)
    {
      if (sourceContentType & CONTENTS_WATER)
	{
	  CG_BubbleTrail (start, tr.endpos, 32);
	}
    }
  // from water to air
  else if (sourceContentType & CONTENTS_WATER)
    {
      cgl->CM_BoxTrace (&trace, end, start, NULL, NULL, 0, CONTENTS_WATER);
      CG_BubbleTrail (start, trace.endpos, 32);
    }
  // from air to water
  else if (destContentType & CONTENTS_WATER)
    {
      cgl->CM_BoxTrace (&trace, start, end, NULL, NULL, 0, CONTENTS_WATER);
      CG_BubbleTrail (trace.endpos, tr.endpos, 32);
    }

  if (tr.surfaceFlags & SURF_NOIMPACT)
    {
      return;
    }

  if (cg_entities[tr.entityNum].currentState.eType == ET_PLAYER)
    {
      CG_MissileHitPlayer (WP_SHOTGUN, tr.endpos, tr.plane.normal, tr.entityNum);
    }
  else
    {
      /*
         // already checked
         if ( tr.surfaceFlags & SURF_NOIMPACT ) {
         // SURF_NOIMPACT will not make a flame puff or a mark
         return;
         }
       */
      if (tr.surfaceFlags & SURF_METALSTEPS)
	{
	  CG_MissileHitWall (WP_SHOTGUN, 0, tr.endpos, tr.plane.normal,
			     IMPACTSOUND_METAL);
	}
      else
	{
	  CG_MissileHitWall (WP_SHOTGUN, 0, tr.endpos, tr.plane.normal,
			     IMPACTSOUND_DEFAULT);
	}
    }
}

/*
================
CG_ShotgunPattern

Perform the same traces the server did to locate the
hit splashes
================
*/
static void
CG_ShotgunPattern (vec3_t origin, vec3_t origin2, int seed, int otherEntNum)
{
  int i;
  float r, u;
  vec3_t end;
  vec3_t forward, right, up;

  // derive the right and up vectors from the forward vector, because
  // the client won't have any other information
  VectorNormalize2 (origin2, forward);
  PerpendicularVector (right, forward);
  CrossProduct (forward, right, up);

  // generate the "random" spread pattern
  for (i = 0; i < SHOTGUN_COUNT; i++)
    {
      r = Q_crandom (&seed) * SHOTGUN_SPREAD * 16;
      u = Q_crandom (&seed) * SHOTGUN_SPREAD * 16;
      VectorMA (origin, 8192 * 16, forward, end);
      VectorMA (end, r, right, end);
      VectorMA (end, u, up, end);

      CG_ShotgunPellet (origin, end, otherEntNum);
    }
}

/*
==============
CG_ShotgunFire
==============
*/
void
CG_ShotgunFire (entityState_t * es)
{
  vec3_t v;
  vec3_t up;
  int contents;

  VectorSubtract (es->origin2, es->pos.trBase, v);
  VectorNormalize (v);
  VectorScale (v, 32, v);
  VectorAdd (es->pos.trBase, v, v);

  if (cg_shotgunsmokepuff->integer)
    {
      contents = cgl->CM_PointContents (es->pos.trBase, 0);
      if (!(contents & CONTENTS_WATER))
	{
	  VectorSet (up, 0, 0, 8);
	  CG_SmokePuff (v, up, 32, 1, 1, 1, 0.33f, 900, cg.levelTime, 0,
			LEF_PUFF_DONT_SCALE,
			cgs.media.shotgunSmokePuffShader);
	}
    }
  //CG_ShotgunPattern (es->pos.trBase, es->origin2, es->eventParm,
  //		     es->otherEntityNum);
}

/*
============================================================================

BULLETS

============================================================================
*/

/*
===============
CG_Tracer
===============
*/
void
CG_Tracer (vec3_t source, vec3_t dest)
{
  vec3_t forward, right;
  polyVert_t verts[4];
  vec3_t line;
  float len, begin, end;
  vec3_t start, finish;
  vec3_t midpoint;

  // tracer
  VectorSubtract (dest, source, forward);
  len = VectorNormalize (forward);

  // start at least a little ways from the muzzle
  if (len < 100)
    {
      return;
    }
  begin = 50 + random () * (len - 60);
  end = begin + cg_tracerLength->value;
  if (end > len)
    {
      end = len;
    }
  VectorMA (source, begin, forward, start);
  VectorMA (source, end, forward, finish);

  line[0] = DotProduct (forward, cg.refdef.viewaxis[1]);
  line[1] = DotProduct (forward, cg.refdef.viewaxis[2]);

  VectorScale (cg.refdef.viewaxis[1], line[1], right);
  VectorMA (right, -line[0], cg.refdef.viewaxis[2], right);
  VectorNormalize (right);

  VectorMA (finish, cg_tracerWidth->value, right, verts[0].xyz);
  verts[0].st[0] = 0;
  verts[0].st[1] = 1;
  verts[0].modulate[0] = 255;
  verts[0].modulate[1] = 255;
  verts[0].modulate[2] = 255;
  verts[0].modulate[3] = 255;

  VectorMA (finish, -cg_tracerWidth->value, right, verts[1].xyz);
  verts[1].st[0] = 1;
  verts[1].st[1] = 0;
  verts[1].modulate[0] = 255;
  verts[1].modulate[1] = 255;
  verts[1].modulate[2] = 255;
  verts[1].modulate[3] = 255;

  VectorMA (start, -cg_tracerWidth->value, right, verts[2].xyz);
  verts[2].st[0] = 1;
  verts[2].st[1] = 1;
  verts[2].modulate[0] = 255;
  verts[2].modulate[1] = 255;
  verts[2].modulate[2] = 255;
  verts[2].modulate[3] = 255;

  VectorMA (start, cg_tracerWidth->value, right, verts[3].xyz);
  verts[3].st[0] = 0;
  verts[3].st[1] = 0;
  verts[3].modulate[0] = 255;
  verts[3].modulate[1] = 255;
  verts[3].modulate[2] = 255;
  verts[3].modulate[3] = 255;

  cgl->R_AddPolysToScene (cgs.media.tracerShader, 4, verts, 1);

  midpoint[0] = (start[0] + finish[0]) * 0.5;
  midpoint[1] = (start[1] + finish[1]) * 0.5;
  midpoint[2] = (start[2] + finish[2]) * 0.5;

  // add the tracer sound
  cgl->snd->StartSound (midpoint, ENTITYNUM_WORLD, CHAN_AUTO,
			cgs.media.tracerSound);
}

/*
======================
CG_CalcMuzzlePoint
======================
*/
static bool
CG_CalcMuzzlePoint (int entityNum, vec3_t muzzle)
{
  vec3_t forward;
  centity_t *cent;
  int anim;

  if (entityNum == cg.snap->ps.clientNum)
    {
      VectorCopy (cg.snap->ps.origin, muzzle);
      muzzle[2] += cg.snap->ps.viewheight;
      AngleVectors (cg.snap->ps.viewangles, forward, NULL, NULL);
      VectorMA (muzzle, 14, forward, muzzle);
      return true;
    }

  cent = &cg_entities[entityNum];
  if (!cent->currentValid)
    {
      return false;
    }

  VectorCopy (cent->currentState.pos.trBase, muzzle);

  AngleVectors (cent->currentState.apos.trBase, forward, NULL, NULL);
  anim = cent->currentState.legsAnim & ~ANIM_TOGGLEBIT;
  if ((anim == LEGS_WALKCR) || (anim == LEGS_IDLECR))
    {
      muzzle[2] += CROUCH_VIEWHEIGHT;
    }
  else
    {
      muzzle[2] += DEFAULT_VIEWHEIGHT;
    }

  VectorMA (muzzle, 14, forward, muzzle);

  return true;
}

/*
======================
CG_Bullet

Renders bullet effects.
======================
*/
void
CG_Bullet (vec3_t end, int sourceEntityNum, vec3_t normal,
	   weapon_t weap, bool flesh, int fleshEntityNum)
{
  trace_t trace;
  int sourceContentType, destContentType;
  vec3_t start;

  // if the shooter is currently valid, calc a source point and possibly
  // do trail effects
  if ((sourceEntityNum >= 0) && (cg_tracerChance->value > 0))
    {
      if (CG_CalcMuzzlePoint (sourceEntityNum, start))
	{
	  sourceContentType = cgl->CM_PointContents (start, 0);
	  destContentType = cgl->CM_PointContents (end, 0);

	  // do a complete bubble trail if necessary
	  if ((sourceContentType == destContentType)
	      && (sourceContentType & CONTENTS_WATER))
	    {
	      CG_BubbleTrail (start, end, 32);
	    }
	  // bubble trail from water into air
	  else if ((sourceContentType & CONTENTS_WATER))
	    {
	      cgl->CM_BoxTrace (&trace, end, start, NULL, NULL, 0,
				CONTENTS_WATER);
	      CG_BubbleTrail (start, trace.endpos, 32);
	    }
	  // bubble trail from air into water
	  else if ((destContentType & CONTENTS_WATER))
	    {
	      cgl->CM_BoxTrace (&trace, start, end, NULL, NULL, 0,
				CONTENTS_WATER);
	      CG_BubbleTrail (trace.endpos, end, 32);
	    }

	  // draw a tracer
	  if (random () < cg_tracerChance->value)
	    {
	      CG_Tracer (start, end);
	    }
	}
    }

  // impact splash and mark
  if (flesh)
    {
      CG_Bleed (end, fleshEntityNum);
    }
  else
    {
      assert ((weap == WP_MACHINEGUN) || (weap == WP_SHOTGUN));
      CG_MissileHitWall (weap, 0, end, normal, IMPACTSOUND_DEFAULT);
    }
}

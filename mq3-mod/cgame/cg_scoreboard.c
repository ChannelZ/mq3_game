/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
//
// cg_scoreboard -- draw the scoreboard on top of the game screen
#include "cg_local.h"


#define SB_TOP		140
#define SB_BOTTOM	410
#define SB_LINE_HEIGHT	16
#define SB_MAXCLIENTS	((SB_BOTTOM - SB_TOP) / SB_LINE_HEIGHT)

// ffa specific
#define SB_FFA_X 140
#define SB_FFA_HEADER 24

// tourney specific
#define SB_TOURNEY_X 90
#define SB_TOURNEY_HEADER 32

// tdm/ctf specific
#define SB_TEAM_X 24
#define SB_TEAM_HEADER 64

// spectator specific
#define SB_SPECTATOR_X 160

/*
========================
CG_DisplayScoreboardFace
========================
*/
static void
CG_DisplayScoreboardFace (int x, int y, int w, int h, int clientNum)
{
  vec3_t headAngles;

  VectorClear (headAngles);
  headAngles[YAW] = 180;
  CG_DrawHead (x, y, w, h, clientNum, headAngles);
}

/*
==================
CG_DisplayHandicap
==================
*/
static void
CG_DisplayHandicap (int x, int y, clientInfo_t *ci, vec4_t color)
{
  char string[3];

  // draw the handicap or bot skill marker
  if ((ci->botSkill > 0) && (ci->botSkill <= 5))
    {
      Com_sprintf (string, sizeof (string), "%2i", ci->botSkill);
      CG_DrawSmallStringColor (x, y, string, color);
    }
  else if (ci->handicap < 100)
    {
      Com_sprintf (string, sizeof (string), "%2i", ci->handicap);
      CG_DrawSmallStringColor (x, y, string, color);
    }
}

/*
===================
CG_DisplayReadyIcon
===================
*/
static void
CG_DisplayReadyIcon (int x, int y, score_t *score, float fade)
{
  const char *s;
  int mask;

  // add the "ready" marker for intermission exiting or ready during warmup
  s = CG_ConfigString (CS_PLAYER_READY_MASK);
  mask = atoi (s);

  if (mask & (1 << score->client))
    {
      vec4_t color2;

      color2[0] = colorYellow[0];
      color2[1] = colorYellow[1];
      color2[2] = colorYellow[2];
      color2[3] = fade;
      CG_DrawBigStringColor (x, y, "R", color2);
    }
  else if (cg.snap->ps.stats[STAT_CLIENTS_READY] & (1 << score->client))
    {
      vec4_t color2;

      color2[0] = colorWhite[0];
      color2[1] = colorWhite[1];
      color2[2] = colorWhite[2];
      color2[3] = fade;
      CG_DrawBigStringColor (x, y, "R", color2);
    }
}

/*
===================
CG_DrawFFAScoreLine
===================
*/
static void
CG_DrawFFAScoreLine (int x, int y, score_t *score, float *color)
{
  char string[MAX_STRING_CHARS];
  clientInfo_t *ci;
  float fade = color[3];
  char name[MAX_NAME_LENGTH];

  ci = &cgs.clientinfo[score->client];

  // draw the handicap or bot skill marker
  CG_DisplayHandicap (x, y, ci, color);

  x += 16;

  // draw the face
  CG_DisplayScoreboardFace (x, y, 16, 16, score->client);

  // draw the ready icon
  CG_DisplayReadyIcon (x, y, score, fade);

  x += 16;

  // construct the name string
  Q_PrintFixedColoredString (ci->name, name, sizeof (name), 13);

  // draw the score line
  Com_sprintf (string, sizeof (string),
	       "%s%5i %s%3i  %s%3i %s%4i %s%4i %s%s",
	       S_COLOR_WHITE, score->score,
	       CG_ColorForPingString (score->ping), score->ping,
	       S_COLOR_CYAN, score->time,
	       S_COLOR_GREEN, score->done / 100,
	       S_COLOR_RED, score->taken / 100,
	       S_COLOR_WHITE, name);

  // highlight your position
  if (score->client == cg.snap->ps.clientNum)
    {
      float hcolor[4];
      int rank;

      rank = cg.snap->ps.persistant[PERS_RANK] & ~RANK_TIED_FLAG;

      if (rank == 0)
	{
	  hcolor[0] = 0;
	  hcolor[1] = 0;
	  hcolor[2] = 0.7f;
	}
      else if (rank == 1)
	{
	  hcolor[0] = 0.7f;
	  hcolor[1] = 0.0f;
	  hcolor[2] = 0.0f;
	}
      else if (rank == 2)
	{
	  hcolor[0] = 0.7f;
	  hcolor[1] = 0.7f;
	  hcolor[2] = 0.0f;
	}
      else
	{
	  hcolor[0] = 0.7f;
	  hcolor[1] = 0.7f;
	  hcolor[2] = 0.7f;
	}

      hcolor[3] = fade * 0.7f;
      CG_FillRect (x, y, 640 - x - 12, 14, hcolor);
    }

  // display the text
  CG_DrawString (x, y, 12, 12, string, fade);
}

/*
====================
CG_DrawDuelScoreLine
====================
*/
static void
CG_DrawDuelScoreLine (int x, int y, score_t *score, float *color)
{
  char string[MAX_STRING_CHARS];
  clientInfo_t *ci;
  float fade = color[3];
  char name[MAX_NAME_LENGTH];

  ci = &cgs.clientinfo[score->client];

  // draw the wins / losses
  Com_sprintf (string, sizeof (string), "%2i/%2i", ci->wins, ci->losses);
  CG_DrawSmallStringColor (x, y, string, color);

  x += 32;

  // draw the face
  CG_DisplayScoreboardFace (x, y, 16, 16, score->client);

  // draw the handicap or bot skill marker
  CG_DisplayHandicap (x, y, ci, color);

  // draw the ready icon
  CG_DisplayReadyIcon (x, y, score, fade);

  x += 22;

  // construct the name string
  Q_PrintFixedColoredString (ci->name, name, sizeof (name), 14);

  // draw score line
  Com_sprintf (string, sizeof (string),
	       "  %s%3i  %s%3i  %s%3i %s%s",
	       S_COLOR_WHITE, score->score,
	       CG_ColorForPingString (score->ping), score->ping,
	       S_COLOR_CYAN, score->time,
	       S_COLOR_WHITE, name);

  // highlight your position
  if (score->client == cg.snap->ps.clientNum)
    {
      float hcolor[4];
      int rank;

      rank = cg.snap->ps.persistant[PERS_RANK] & ~RANK_TIED_FLAG;

      if (rank == 0)
	{
	  hcolor[0] = 0;
	  hcolor[1] = 0;
	  hcolor[2] = 0.7f;
	}
      else
	{
	  hcolor[0] = 0.7f;
	  hcolor[1] = 0;
	  hcolor[2] = 0;
	}

      hcolor[3] = fade * 0.7;
      CG_FillRect (x, y, 640 - x - 16, BIGCHAR_HEIGHT + 1, hcolor);
    }

  // display the text
  CG_DrawBigString (x, y, string, fade);
}

/*
=================
CG_DrawTDMScoreLine
=================
*/
static void
CG_DrawTDMScoreLine (int x, int y, int lineLength, score_t *score, float *color)
{
  char string[MAX_STRING_CHARS];
  clientInfo_t *ci;
  float fade = color[3];
  char name[MAX_NAME_LENGTH];

  ci = &cgs.clientinfo[score->client];

  // draw the face
  CG_DisplayScoreboardFace (x, y, 16, 16, score->client);

  // draw the handicap or bot skill marker
  CG_DisplayHandicap (x, y, ci, color);

  // draw the ready icon
  CG_DisplayReadyIcon (x, y, score, fade);

  x += 28;

  // construct the name string
  Q_PrintFixedColoredString (ci->name, name, sizeof (name), 10);

  // draw the score line
  Com_sprintf (string, sizeof (string),
	       "%s%3i %s%3i %s%3i %s%3i %s%s",
	       S_COLOR_WHITE, score->score,
	       S_COLOR_CYAN, score->net,
	       CG_ColorForPingString (score->ping), score->ping,
	       S_COLOR_CYAN, score->time,
	       S_COLOR_WHITE, name);

  // highlight your position in grey
  if (score->client == cg.snap->ps.clientNum)
    {
      float hcolor[4];

      hcolor[0] = 0.7f;
      hcolor[1] = 0.7f;
      hcolor[2] = 0.7f;

      hcolor[3] = fade * 0.7;
      CG_FillRect (x, y - 1, lineLength - 36, 14, hcolor);
    }

  // display the text
  CG_DrawString (x, y, 10, 10, string, fade);
}

/*
=================
CG_DrawCTFScoreLine
=================
*/
static void
CG_DrawCTFScoreLine (int x, int y, int lineLength, score_t *score, float *color)
{
  char string[MAX_STRING_CHARS];
  clientInfo_t *ci;
  float fade = color[3];
  char name[MAX_NAME_LENGTH];

  ci = &cgs.clientinfo[score->client];

  // draw the handicap or bot skill marker (unless player has flag)
  if (ci->powerups & (1 << PW_REDFLAG))
    {
      CG_DrawFlagModel (x, y, 16, 16, TEAM_RED, false);
    }
  else if (ci->powerups & (1 << PW_BLUEFLAG))
    {
      CG_DrawFlagModel (x, y, 16, 16, TEAM_BLUE, false);
    }
  else
    {
      // draw the face
      CG_DisplayScoreboardFace (x, y, 16, 16, score->client);

      // draw the handicap or bot skill marker
      CG_DisplayHandicap (x, y, ci, color);
    }

  // draw the ready icon
  CG_DisplayReadyIcon (x, y, score, fade);

  x += 28;

  // construct the name string
  Q_PrintFixedColoredString (ci->name, name, sizeof (name), 10);

  // draw the score line
  Com_sprintf (string, sizeof (string),
	       "%s%3i %s%3i %s%3i     %s%s",
	       S_COLOR_WHITE, score->score,
	       CG_ColorForPingString (score->ping), score->ping,
	       S_COLOR_CYAN, score->time,
	       S_COLOR_WHITE, name);

  // highlight your position in grey
  if (score->client == cg.snap->ps.clientNum)
    {
      float hcolor[4];

      hcolor[0] = 0.7f;
      hcolor[1] = 0.7f;
      hcolor[2] = 0.7f;

      hcolor[3] = fade * 0.7;
      CG_FillRect (x, y - 1, lineLength - 36, 14, hcolor);
    }

  // display the text
  CG_DrawString (x, y, 10, 10, string, fade);

  // draw the reward score
  Com_sprintf (string, sizeof (string),
	       "%s%3i",
	       S_COLOR_ORANGE2, score->defendCount);

  CG_DrawString (x + 122, y - 1, 8, 6, string, fade);

  // draw the reward score
  Com_sprintf (string, sizeof (string),
	       "%s%3i",
	       S_COLOR_MAGENTA, score->assistCount);

  CG_DrawString (x + 122, y + 6, 8, 6, string, fade);
}

/*
======================
CG_SpectatorScoreboard
======================
*/
static int
CG_SpectatorScoreboard (int y, float fade, int maxClients, int lineHeight)
{
  int i;
  float color[4];
  int count;
  score_t *score;
  clientInfo_t *ci;
  char *msg;
  char string[MAX_STRING_CHARS];
  char name[MAX_NAME_LENGTH];

  color[0] = color[1] = color[2] = 1.0;
  color[3] = fade;

  // counting the number of spectators
  count = 0;
  for (i = 0; (i < cg.numScores); i++)
    {
      score = &cg.scores[i];

      if ((score->client < 0) || (score->client >= cgs.maxclients))
	{
	  CG_Printf ("Bad score->client: %i [%i]\n", score->client, i);
	  continue;
	}
      ci = &cgs.clientinfo[score->client];

      if ((ci->team != TEAM_SPECTATOR) && (score->ping != -1))
	{
	  continue;
	}

      count++;
    }

  // if there are not spectators just return
  if (count == 0)
    return 0;

  // title
  msg = va ("%sSpectators %s(%s%i%s)", S_COLOR_YELLOW, S_COLOR_WHITE,
	    S_COLOR_CYAN, count, S_COLOR_WHITE);
  CG_DrawString (SB_SPECTATOR_X, y, 12, 12, msg, fade);

  // jump a bit of space before displaying name
  y += 24;

  // displaying spectators
  count = 0;
  for (i = 0; (i < cg.numScores) && (count < maxClients); i++)
    {
      score = &cg.scores[i];

      if ((score->client < 0) || (score->client >= cgs.maxclients))
	{
	  CG_Printf ("Bad score->client: %i [%i]\n", score->client, i);
	  continue;
	}
      ci = &cgs.clientinfo[score->client];

      if ((ci->team != TEAM_SPECTATOR) && (score->ping != -1))
	{
	  continue;
	}

      // generating name string
      Q_PrintFixedColoredString (ci->name, name, sizeof (name), 10);

      Com_sprintf (string, sizeof (string),
		   "%s%3i %s%s",
		   S_COLOR_GREEN, score->ping,
		   S_COLOR_WHITE, name);

      // displaying
      // 16 * 12 (name) + 3 * 12 (ping) + 10
      CG_DrawString (92 + (count % 2) * (192 + 46),
		     y + lineHeight * (count / 2),
		     12, 12, string, fade);

      count++;
    }

  return count;
}

/*
=================
CG_FFAScoreboard
=================
*/
static int
CG_FFAScoreboard (int y, float fade, int maxClients, int lineHeight)
{
  int i;
  float color[4];
  int count;
  score_t *score;
  clientInfo_t *ci;

  color[0] = color[1] = 1.0f;
  color[2] = 0.0f;
  color[3] = fade;

  // positions are a bit messy atm...
  CG_DrawStringColor (SB_FFA_X + 32, y - 16, 12, 12,
		      "Score Lag Time Done Take Name", color);

  color[0] = color[1] = color[2] = 1.0;
  color[3] = fade;

  count = 0;
  for (i = 0; (i < cg.numScores) && (count < maxClients); i++)
    {
      score = &cg.scores[i];

      if ((score->client < 0) || (score->client >= cgs.maxclients))
	{
	  CG_Printf ("Bad score->client: %i [%i]\n", score->client, i);
	  continue;
	}
      ci = &cgs.clientinfo[score->client];

      // not playing
      if (ci->team != TEAM_FREE)
	{
	  continue;
	}

      // connecting
      if (score->ping == -1)
	{
	  continue;
	}

      CG_DrawFFAScoreLine (SB_FFA_X, y + lineHeight * count, score, color);

      count++;
    }

  return count;
}

/*
=================
CG_DuelScoreboard
=================
*/
static int
CG_DuelScoreboard (int y, float fade, int maxClients, int lineHeight)
{
  int i;
  float color[4];
  int count;
  score_t *score;
  clientInfo_t *ci;

  color[0] = color[1] = 1.0f;
  color[2] = 0.0f;
  color[3] = fade;

  // positions are a bit messy atm...
  CG_DrawStringColor (SB_TOURNEY_X + 54, y, 16, 16,
		      "Score Ping Time Name", color);
  y += 24;

  color[0] = color[1] = color[2] = 1.0f;
  color[3] = fade;

  count = 0;
  for (i = 0; (i < cg.numScores) && (count < maxClients); i++)
    {
      score = &cg.scores[i];

      if ((score->client < 0) || (score->client >= cgs.maxclients))
	{
	  CG_Printf ("Bad score->client: %i [%i]\n", score->client, i);
	  continue;
	}
      ci = &cgs.clientinfo[score->client];

      // not playing
      if (ci->team != TEAM_FREE)
	{
	  continue;
	}

      // connecting
      if (score->ping == -1)
	{
	  continue;
	}

      CG_DrawDuelScoreLine (SB_TOURNEY_X, y + lineHeight * count, score, color);

      count++;
    }

  // keep space for header
  return count;
}

/*
=================
CG_TDMScoreboard
=================
*/
static int
CG_TDMScoreboard (int baseheight, float fade, int maxClients, int lineHeight)
{
  int i, j;
  team_t team;
  score_t *score;
  clientInfo_t *ci;
  float color[4];
  int count[2];
  int clcount;
  int maxcount;
  int ping[2];
  char *msg;
  int x;
  int y;

  color[0] = color[1] = color[2] = 1.0f;
  color[3] = fade;
  maxcount = 0;

  // 1st pass, average ping, number of players per team
  for (j = 0, team = TEAM_RED; team < TEAM_SPECTATOR; team++, j++)
    {
      count[j] = 0;
      ping[j] = 0;
      for (i = 0; (i < cg.numScores); i++)
	{
	  score = &cg.scores[i];

	  if ((score->client < 0) || (score->client >= cgs.maxclients))
	    {
	      CG_Printf ("Bad score->client: %i [%i]\n", score->client, i);
	      continue;
	    }
	  ci = &cgs.clientinfo[score->client];

	  // not playing or not in the good team
	  if (ci->team != team)
	    {
	      continue;
	    }

	  // connecting
	  if (score->ping == -1)
	    {
	      continue;
	    }

	  ping[j] += score->ping;
	  count[j]++;
	}

      maxcount = (count[j] > maxcount) ? count[j] : maxcount;
    }

  // drawing the colored background
  for (j = 0, team = TEAM_RED, x = SB_TEAM_X;
       team < TEAM_SPECTATOR;
       team++, j++, x = (SCREEN_WIDTH / 2) + 1)
    {
      y = baseheight;

      if (team == TEAM_RED)
	{
	  // red
	  color[0] = 0.7;
	  color[1] = 0;
	  color[2] = 0.1;
	  color[3] = 0.5 * fade;

	  CG_FillRect (x, y - 2,
		       (SCREEN_WIDTH / 2) - SB_TEAM_X,
		       (64 + (maxcount * 16)), color);
	}
      else
	{
	  // blue
	  color[0] = 0.1;
	  color[1] = 0;
	  color[2] = 0.7;
	  color[3] = 0.5 * fade;

	  CG_FillRect (x, y - 2,
		       (SCREEN_WIDTH / 2) - SB_TEAM_X,
		       (64 + (maxcount * 16)), color);
	}
    }

  // 2nd pass displaying players/team infos
  for (j = 0, team = TEAM_RED, x = SB_TEAM_X;
       team < TEAM_SPECTATOR;
       team++, j++, x = (SCREEN_WIDTH / 2) + 1)
    {
      y = baseheight;

      if (count[j] == 0)
	{
	  ping[j] = 0;
	}
      else
	{
	  ping[j] = ping[j] / count[j];
	}

      color[0] = color[1] = color[2] = 1.0f;
      color[3] = fade;

      msg = va ("%3i", cg.teamScores[j]);
      CG_DrawStringColor (x, y, 28, 28, msg, color);

      msg = va ("Players");
      CG_DrawStringColor (x + 96, y, 10, 10, msg, color);

      msg = va ("%3i", count[j]);
      CG_DrawStringColor (x + 96, y + 12, 16, 16, msg, color);

      color[0] = color[1] = color[2] = 1.0f;
      color[3] = fade;

      msg = va ("AvgPing");
      CG_DrawStringColor (x + 176, y, 10, 10, msg, color);

      // colorize ping
      CG_ColorForPing (color, ping[j]);
      color[3] = fade;

      msg = va ("%3i", ping[j]);
      CG_DrawStringColor (x + 176, y + 12, 14, 14, msg, color);

      y += 32;

      color[2] = 0.0f;
      color[0] = color[1] = 1.0;
      color[3] = fade;

      // positions are a bit messy atm...
      CG_DrawStringColor (x + 10, y, 10, 10,
			  "Score Net Lag Min Name", color);

      y += 20;

      clcount = 0;
      for (i = 0; (i < cg.numScores) && (clcount < maxClients); i++)
	{
	  score = &cg.scores[i];

	  if ((score->client < 0) || (score->client >= cgs.maxclients))
	    {
	      CG_Printf ("Bad score->client: %i [%i]\n", score->client, i);
	      continue;
	    }
	  ci = &cgs.clientinfo[score->client];

	  // not playing or not in the good team
	  if (ci->team != team)
	    {
	      continue;
	    }

	  // connecting
	  if (score->ping == -1)
	    {
	      continue;
	    }

	  color[0] = color[1] = color[2] = 1.0f;
	  color[3] = fade;

	  // drawing this player line
	  CG_DrawTDMScoreLine (x + 2, y + lineHeight * clcount,
			       (SCREEN_WIDTH / 2) - SB_TEAM_X,
			       score, color);
	  clcount++;
	}
    }

  return maxcount;
}

/*
=================
CG_CTFScoreboard
=================
*/
static int
CG_CTFScoreboard (int baseheight, float fade, int maxClients, int lineHeight)
{
  int i, j;
  team_t team;
  score_t *score;
  clientInfo_t *ci;
  float color[4];
  int count[2];
  int clcount;
  int maxcount;
  int ping[2];
  int points[2];
  char *msg;
  int x;
  int y;

  color[0] = color[1] = color[2] = 1.0f;
  color[3] = fade;
  maxcount = 0;

  // 1st pass, average ping, number of players per team
  for (j = 0, team = TEAM_RED; team < TEAM_SPECTATOR; team++, j++)
    {
      count[j] = 0;
      ping[j] = 0;
      points[j] = 0;
      for (i = 0; (i < cg.numScores); i++)
	{
	  score = &cg.scores[i];

	  if ((score->client < 0) || (score->client >= cgs.maxclients))
	    {
	      CG_Printf ("Bad score->client: %i [%i]\n", score->client, i);
	      continue;
	    }
	  ci = &cgs.clientinfo[score->client];

	  // not playing or not in the good team
	  if (ci->team != team)
	    {
	      continue;
	    }

	  // connecting
	  if (score->ping == -1)
	    {
	      continue;
	    }

	  ping[j] += score->ping;
	  points[j] += score->score;
	  count[j]++;
	}

      maxcount = (count[j] > maxcount) ? count[j] : maxcount;
    }

  // drawing the colored background
  for (j = 0, team = TEAM_RED, x = SB_TEAM_X;
       team < TEAM_SPECTATOR;
       team++, j++, x = (SCREEN_WIDTH / 2) + 1)
    {
      y = baseheight;

      if (team == TEAM_RED)
	{
	  // red
	  color[0] = 0.7;
	  color[1] = 0;
	  color[2] = 0.1;
	  color[3] = 0.5 * fade;

	  CG_FillRect (x, y - 2,
		       (SCREEN_WIDTH / 2) - SB_TEAM_X,
		       (64 + (maxcount * 16)), color);
	}
      else
	{
	  // blue
	  color[0] = 0.1;
	  color[1] = 0;
	  color[2] = 0.7;
	  color[3] = 0.5 * fade;

	  CG_FillRect (x, y - 2,
		       (SCREEN_WIDTH / 2) - SB_TEAM_X,
		       (64 + (maxcount * 16)), color);
	}
    }

  // 2nd pass displaying players/team infos
  for (j = 0, team = TEAM_RED, x = SB_TEAM_X;
       team < TEAM_SPECTATOR;
       team++, j++, x = (SCREEN_WIDTH / 2) + 1)
    {
      y = baseheight;

      if (count[j] == 0)
	{
	  ping[j] = 0;
	}
      else
	{
	  ping[j] = ping[j] / count[j];
	}

      // white
      color[0] = color[1] = color[2] = 1.0f;
      color[3] = fade;

      msg = va ("%-2i", cg.teamScores[j]);
      CG_DrawStringColor (x, y, 28, 28, msg, color);

      msg = va ("Points");
      CG_DrawStringColor (x + 66, y, 10, 10, msg, color);

      // yellow
      color[0] = color[1] = 1.0f;
      color[2] = 0.0f;
      color[3] = fade;

      msg = va ("%3i", points[j]);
      CG_DrawStringColor (x + 66, y + 12, 16, 16, msg, color);

      // white
      color[0] = color[1] = color[2] = 1.0f;
      color[3] = fade;

      msg = va ("Players");
      CG_DrawStringColor (x + 152, y, 10, 10, msg, color);

      msg = va ("%3i", count[j]);
      CG_DrawStringColor (x + 152, y + 12, 16, 16, msg, color);

      color[0] = color[1] = color[2] = 1.0f;
      color[3] = fade;

      msg = va ("Ping");
      CG_DrawStringColor (x + 234, y, 10, 10, msg, color);

      // colorize ping
      CG_ColorForPing (color, ping[j]);
      color[3] = fade;

      msg = va ("%3i", ping[j]);
      CG_DrawStringColor (x + 224, y + 12, 14, 14, msg, color);

      y += 32;

      color[2] = 0.0f;
      color[0] = color[1] = 1.0;
      color[3] = fade;

      // positions are a bit messy atm...
      CG_DrawStringColor (x + 10, y, 10, 10,
			  "Score Lag Min Awd Name", color);

      y += 20;

      clcount = 0;
      for (i = 0; (i < cg.numScores) && (clcount < maxClients); i++)
	{
	  score = &cg.scores[i];

	  if ((score->client < 0) || (score->client >= cgs.maxclients))
	    {
	      CG_Printf ("Bad score->client: %i [%i]\n", score->client, i);
	      continue;
	    }
	  ci = &cgs.clientinfo[score->client];

	  // not playing or not in the good team
	  if (ci->team != team)
	    {
	      continue;
	    }

	  // connecting
	  if (score->ping == -1)
	    {
	      continue;
	    }

	  color[0] = color[1] = color[2] = 1.0f;
	  color[3] = fade;

	  // drawing this player line
	  CG_DrawCTFScoreLine (x + 2, y + lineHeight * clcount,
			       (SCREEN_WIDTH / 2) - SB_TEAM_X,
			       score, color);
	  clcount++;
	}
    }

  return maxcount;
}

/*
=================
CG_DrawScoreboard

Draw the normal in-game scoreboard
=================
*/
bool
CG_DrawOldScoreboard (void)
{
  int x, y, w, n1, n2;
  float fade;
  float *fadeColor;
  char *s;
  int maxClients;
  int lineHeight;

  // don't draw scoreboard during death while warmup up
  if (cg.warmup && !cg.showScores && !cg.intermissionStarted)
    {
      return false;
    }

  if (cg.showScores || (cg.predictedPlayerState.pm_type == PM_INTERMISSION)
      || ((cg.predictedPlayerState.pm_type == PM_DEAD) && cg_drawScoreboardOnDeath->integer))
    {
      fade = 1.0;
    }
  else
    {
      fadeColor = CG_FadeColor (cg.scoreFadeTime, FADE_TIME, TIMER_REALTIME);

      if (!fadeColor)
	{
	  // next time scoreboard comes up, don't print killer
	  cg.killerName[0] = 0;
	  return false;
	}
      fade = *fadeColor;
    }

  // fragged by ... line
  if (cg.killerName[0])
    {
      s = va ("Fragged by %s", cg.killerName);
      w = CG_DrawStrlen (s) * BIGCHAR_WIDTH;
      x = (SCREEN_WIDTH - w) / 2;
      y = 40;
      CG_DrawBigString (x, y, s, fade);
    }

  // current rank
  if (!BG_GametypeIsTeam (cgs.gametype))
    {
      if (cg.snap->ps.persistant[PERS_TEAM] != TEAM_SPECTATOR)
	{
	  s = va ("%s place with %i",
		  CG_PlaceString (cg.snap->ps.persistant[PERS_RANK] + 1),
		  cg.snap->ps.persistant[PERS_SCORE]);
	  w = CG_DrawStrlen (s) * BIGCHAR_WIDTH;
	  x = (SCREEN_WIDTH - w) / 2;
	  y = 60;
	  CG_DrawBigString (x, y, s, fade);
	}
    }
  else
    {
      if (cg.teamScores[0] == cg.teamScores[1])
	{
	  s = va ("Teams are tied at %i", cg.teamScores[0]);
	}
      else if (cg.teamScores[0] >= cg.teamScores[1])
	{
	  s = va ("Red leads %i to %i", cg.teamScores[0], cg.teamScores[1]);
	}
      else
	{
	  s = va ("Blue leads %i to %i", cg.teamScores[1], cg.teamScores[0]);
	}

      w = CG_DrawStrlen (s) * BIGCHAR_WIDTH;
      x = (SCREEN_WIDTH - w) / 2;
      y = 60;
      CG_DrawBigString (x, y, s, fade);
    }

  // scoreboard
  y = SB_TOP;

  maxClients = SB_MAXCLIENTS;
  lineHeight = SB_LINE_HEIGHT;

  if (cgs.gametype == GT_FFA)
    {
      // free for all scoreboard

      n1 = CG_FFAScoreboard (y, fade, maxClients, lineHeight);
      y += (n1 * lineHeight) + SB_FFA_HEADER;

      n2 = CG_SpectatorScoreboard (y, fade, maxClients - n1, lineHeight);
      y += (n2 * lineHeight) + BIGCHAR_HEIGHT;
    }
  else if (cgs.gametype == GT_TOURNAMENT)
    {
      // duel scoreboard
      n1 = CG_DuelScoreboard (y, fade, maxClients, lineHeight);
      y += (n1 * lineHeight) + SB_TOURNEY_HEADER;

      n2 = CG_SpectatorScoreboard (y, fade, maxClients - n1, lineHeight);
      y += (n2 * lineHeight) + BIGCHAR_HEIGHT;
    }
  else if (cgs.gametype == GT_TEAM)
    {
      // tdm scoreboard

      n1 = CG_TDMScoreboard (y, fade, maxClients, lineHeight);
      y += (n1 * lineHeight) + SB_TEAM_HEADER + BIGCHAR_HEIGHT;

      n2 = CG_SpectatorScoreboard (y, fade, maxClients - n1, lineHeight);
      y += (n2 * lineHeight) + BIGCHAR_HEIGHT;
    }
  else if (cgs.gametype == GT_CTF)
    {
      // ctf scoreboard

      n1 = CG_CTFScoreboard (y, fade, maxClients, lineHeight);
      y += (n1 * lineHeight) + SB_TEAM_HEADER + BIGCHAR_HEIGHT;

      n2 = CG_SpectatorScoreboard (y, fade, maxClients - n1, lineHeight);
      y += (n2 * lineHeight) + BIGCHAR_HEIGHT;
    }
  else if (cgs.gametype == GT_ARENA)
    {
      // TODO: using tdmscoreboard temporary
      // arena scoreboard

      n1 = CG_TDMScoreboard (y, fade, maxClients, lineHeight);
      y += (n1 * lineHeight) + SB_TEAM_HEADER + BIGCHAR_HEIGHT;

      n2 = CG_SpectatorScoreboard (y, fade, maxClients - n1, lineHeight);
      y += (n2 * lineHeight) + BIGCHAR_HEIGHT;
    }

  // load any models that have been deferred
  if (cg.deferredPlayerLoading)
    {
      CG_LoadDeferredPlayers ();
    }

  return true;
}

//================================================================================

/*
================
CG_CenterGiantLine
================
*/
static void
CG_CenterGiantLine (float y, const char *string)
{
  float x;
  vec4_t color;

  color[0] = 1;
  color[1] = 1;
  color[2] = 1;
  color[3] = 1;

  x = 0.5 * (640 - GIANT_WIDTH * CG_DrawStrlen (string));

  CG_DrawStringExt (x, y, string, color, true, true, GIANT_WIDTH,
		    GIANT_HEIGHT, 0);
}

/*
=================
CG_DrawOldTourneyScoreboard

Draw the oversize scoreboard for tournements
=================
*/
void
CG_DrawOldTourneyScoreboard (void)
{
  const char *s;
  vec4_t color;
  int min, tens, ones;
  clientInfo_t *ci;
  int y;
  int i;

  // request more scores regularly
  if (cg.scoresRequestTime + 2000 < cg.time)
    {
      cg.scoresRequestTime = cg.time;
      cgl->SendClientCommand ("score");
    }

  // draw the dialog background
  color[0] = color[1] = color[2] = 0;
  color[3] = 1;
  CG_FillRect (0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, color);

  color[0] = 1;
  color[1] = 1;
  color[2] = 1;
  color[3] = 1;

  // print the mesage of the day
  s = CG_ConfigString (CS_MOTD);
  if (!s[0])
    {
      s = "Scoreboard";
    }

  // print optional title
  CG_CenterGiantLine (8, s);

  // print server time
  ones = cg.levelTime / 1000;
  min = ones / 60;
  ones %= 60;
  tens = ones / 10;
  ones %= 10;
  s = va ("%i:%i%i", min, tens, ones);

  CG_CenterGiantLine (64, s);

  // print the two scores

  y = 160;
  if (BG_GametypeIsTeam (cgs.gametype))
    {
      //
      // teamplay scoreboard
      //
      CG_DrawStringExt (8, y, "Red Team", color, true, true, GIANT_WIDTH,
			GIANT_HEIGHT, 0);
      s = va ("%i", cg.teamScores[0]);
      CG_DrawStringExt (632 - GIANT_WIDTH * strlen (s), y, s, color, true,
			true, GIANT_WIDTH, GIANT_HEIGHT, 0);

      y += 64;

      CG_DrawStringExt (8, y, "Blue Team", color, true, true, GIANT_WIDTH,
			GIANT_HEIGHT, 0);
      s = va ("%i", cg.teamScores[1]);
      CG_DrawStringExt (632 - GIANT_WIDTH * strlen (s), y, s, color, true,
			true, GIANT_WIDTH, GIANT_HEIGHT, 0);
    }
  else
    {
      //
      // free for all scoreboard
      //
      for (i = 0; i < MAX_CLIENTS; i++)
	{
	  ci = &cgs.clientinfo[i];
	  if (!ci->infoValid)
	    {
	      continue;
	    }
	  if (ci->team != TEAM_FREE)
	    {
	      continue;
	    }

	  CG_DrawStringExt (8, y, ci->name, color, true, true, GIANT_WIDTH,
			    GIANT_HEIGHT, 0);
	  s = va ("%i", ci->score);
	  CG_DrawStringExt (632 - GIANT_WIDTH * strlen (s), y, s, color,
			    true, true, GIANT_WIDTH, GIANT_HEIGHT, 0);
	  y += 64;
	}
    }
}

//================================================================================

/*
===============
CG_DrawBoxPrint

Draw a box on the screen
filled with the text sent by the server
===============
*/
#define BOXFADETIME_VALUE 500 // in msec
#define BOXPRINT_HEIGHT 10
#define BOXPRINT_WIDTH 8
#define BOXPRINT_INTERLINE 12
#define BOXPRINT_BOTTOM_BORDER 370
#define BOXPRINT_LEFT_OFFSET 10
#define BOXPRINT_UPPER_BORDER 180

void
CG_DrawBoxPrint (void)
{
  vec4_t color;
  char printString[128];
  char *start;
  char *endline;
  int lines;
  int initialheight;
  float factor = 1.0f;

  if (!cg.BoxPrintReady)
    return;

  // nothing to show here
  if (!cg.BoxShow && ((cg.BoxFadeTime + BOXFADETIME_VALUE) <= cg.time))
    return;

  if (!cg.BoxShow && ((cg.BoxFadeTime + BOXFADETIME_VALUE) > cg.time))
    {
      factor = (float)(cg.time - cg.BoxFadeTime) / (float)BOXFADETIME_VALUE;
      if (factor > 1)
	factor = 1;
      if (factor <= 0)
	factor = 1;

      factor = 1 - factor;
      color[3] = factor;
    }
  else
    {
      color[3] = 1;
    }

  // autorefresh ?
  /*
  if ((cg.BoxRequestTime + 2000) < cg.time)
    {
      cg.BoxRequestTime = cg.time;
      cgl->SendClientCommand (va ("boxstats %i", cg.snap->ps.clientNum));
    }
  */

  lines = 0;

  // always position the box over this line
  initialheight = BOXPRINT_BOTTOM_BORDER - (cg.BoxY * BOXPRINT_INTERLINE);

  // upper border has the priority over the bottom one (this can recover the hud)
  if (initialheight <= BOXPRINT_UPPER_BORDER)
    {
      initialheight = BOXPRINT_UPPER_BORDER;
    }

  // draw a blue box behind the text with grey borders

  // grey
  color[0] = 0.5;
  color[1] = 0.5;
  color[2] = 0.5;
  color[3] = 0.5 * factor;

  CG_DrawRect (BOXPRINT_LEFT_OFFSET - 4, initialheight - 4,
	       cg.BoxX * BOXPRINT_WIDTH + 8, cg.BoxY * BOXPRINT_INTERLINE + 8,
	       2, color);

  // blue
  color[0] = 0.1;
  color[1] = 0;
  color[2] = 0.7;
  color[3] = 0.5 * factor;

  CG_FillRect (BOXPRINT_LEFT_OFFSET - 2, initialheight - 2,
	       cg.BoxX * BOXPRINT_WIDTH + 4, cg.BoxY * BOXPRINT_INTERLINE + 4,
	       color);

  // white (text color)
  color[0] = 1;
  color[1] = 1;
  color[2] = 1;
  color[3] = factor;

  // display the text line by line
  start = cg.BoxPrintString;
  // doing the first string
  while ((endline = strchr (start, '\n')))
    {
      // pointer distance; 0 means we have nothing other than a \n to "display"
      int dist = endline - start;
      if ((dist < 0) || (dist >= 128))
	{
	  CG_Error ("CG_DrawBoxString () : wrong line length");
	}

      // we want to jump line if dist = 0
      if (dist != 0)
	{
	  // all the caracters between end and start => distance = dist + 1
	  Q_strncpyz (printString, start, dist + 1);

	  CG_DrawStringExt (BOXPRINT_LEFT_OFFSET,
			    initialheight + lines * BOXPRINT_INTERLINE,
			    printString, color, false, false,
			    BOXPRINT_WIDTH, BOXPRINT_HEIGHT, 0);
	}

      lines++;
      //
      endline++;
      start = endline;
    }

  // display the text line by line
  start = cg.BoxPrintString2;
  // and the second one
  while ((endline = strchr (start, '\n')))
    {
      // pointer distance; 0 means we have nothing other than a \n to "display"
      int dist = endline - start;
      if ((dist < 0) || (dist >= 128))
	{
	  CG_Error ("CG_DrawBoxString () : wrong line length");
	}

      // we want to jump line if dist = 0
      if (dist != 0)
	{
	  // all the caracters between end and start => distance = dist + 1
	  Q_strncpyz (printString, start, dist + 1);

	  CG_DrawStringExt (BOXPRINT_LEFT_OFFSET,
			    initialheight + lines * BOXPRINT_INTERLINE,
			    printString, color, false, false,
			    BOXPRINT_WIDTH, BOXPRINT_HEIGHT, 0);
	}

      lines++;
      //
      endline++;
      start = endline;
    }
}

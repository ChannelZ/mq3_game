===========================
mQ3 Quick Guide
===========================

Presentation
============

mQ3 is a Quake 3 (by ID Software) fork with no means of retro compatibility.
It's aimed to provide a stable game for players and admins to play competitive.
For developers the goal is to provide a solid and well documented code base.
Everything is expected to be free (libre) the source code, documentation as well
as game resources.

Don't expect things to go VERY fast, things will advance maybe slowly but they will advance.
Of course you can help to walk the path and help the game to grow faster.

Irc : **#mq3** @ irc.quakenet.org

.. contents::

Defend your freedom !

Virtual doesn't mean imaginary.
People usually think nothing really matters when it isn't right before their nose.
Virtual is as true as what is said to be Real.
Virtual doesn't need as much material support to be expressed, unlike
what you think to be Real and that's the only difference.
The man before his computer is the same human, has the same body, has the same mind,
might he play football or video games.

For virtual said experiences you need both your body (to interact with the user interface machine)
and your mind to think and feel the picture, to experience what you are facing.
(might it be real or virtual, your brain receive an information to handle)
Your mind is as much sollicited (maybe even more) as your regular daily "Real" experience,
you should "protect" it and have the same expectations (or maybe even more)
as in your social and political life.
Request, cherish and protect your freedom. Only then you'll understand what freedom is.


Installation
============

You should just have to uncompress the archive containing everything
need to play the game in a fresh new directory.

Starting the game
-----------------

To launch the game just start the mq3.i386 if you run a GNU/Linux OS
or start mq3.x86.exe for windows users.

You should end up in the quake console.
There currently is no advanced in game menu.

Basic commands
~~~~~~~~~~~~~~

Surviving with the console !

You first must be able to open/close the console default keys are ` and ~.
You must take care of you keyboard layout.
Shift + ESC is a hardcoded combinaison to toggle the console.
You can set now key with the cl_consoleKeys ? cvar

A short list of the commands needed to survive :

- connect ip:port (eg. connect 88.191.76.125:27980)
- password changeme
- reconnect
- exec configfile
- list_* (Press the TAB key to autocomplete the available commands, eg list_cmd, list_cvars, list_colors)
- bind key "action"
- unbind key
- set variable "sometext"
 to execute the content of a variable (if it is valid)
- vstr variable


Licences
--------

The current game archive contains free (GPL licenced) ressources
but also ressources from ID software's Quake 3 game.
(these files are listed in the filelist.txt zipped in z-min0-nonfree-\*.pk3)

That's why you must have a valid Quake 3 licence to play the game.

Configuration
=============

Because there is no ingame menu and no UI to make you game configuration,
you will have to spend a bit of time making you config file !

But don't worry we provided all the informations you need for this process.

Example config
--------------

You can find an example and commentated configuration with the game.
This configuration is the default one the game will use.
The config can be found under the base/ directory.

 *base/cfgs/example.cfg*

If you want to change you graphical configuration, two example configs are provide.

 *base/cfgs/graphmip.cfg*

 *base/cfgs/graphwoau.cfg*

Want some config for spectating matches or demos ?

 *base/cfgs/configvideo.cfg*

More doc !
----------------

You may want to have a full list of the available cvars
or more informations about some of them.

For this you can take a look at the **doc/** directory.

All cgame/game cvars are describbed here.
Some client cvars and cmds are still missing.

Video Settings
--------------

You may want to change your screen settings,
you can use the **list_modes** command to view buildin video modes
and change them by settings the r_mode cvar to the wanted value.

 *r_mode 6; vid_restart*

If you have a widescreen (and don't want the picture to be cropped)
You can take a look at the following cvars

- r_availableModes	(contains a list of valid resolutions)

- r_mode -1		(we want to use our hand set resolution)
- r_customWidth
- r_customHeight

Input Settings
--------------

The engine uses the SDL backend for default input.
The SDL lib relays on the OS interfaces for inputs.
You can want to have a more straitforward input method
by using the *in_mouse 2; in_restart* settings.

On windows, the game will use the "raw input" interfaces.

Linux users should set the usb device the mouse is plugged in :

- in_mousedevice "/dev/input/event3"

You can get a list of available interfaces with the *in_listevdev* command.
Take a look at the doc for more infos, and how to configure udev to get the mouse ready.

As the GNU/Linux "raw" mouse input runs in a dedicated thread, you can increase the mouse polling rate
and feel the difference.
http://wiki.quakeworld.nu/Howto_customise_mouse_polling_rate

Contributing
============

You want to contribute, you're welcome, all you need is time.
You don't need to be a pro at coding to be of any help.

Here is a short list of topics that can need attention (or you can find others):

- documentation and translation (a good documentation for users and also developers is essential.

- as a player you're welcome to report bugs or ask for features on the project's `redmine`_

- might you not like some too minimalist textures or find some ugly,
  take your Gimp and make better ones, this will be really appreciated.

- quality models (players/weapons/items) and also maps.
  If you feel like creating new (interesting) items you can contact us, we welcome new features too.

- write some short articles about the game, this can be guides about configs/scripts
  but also howto tweak/toy with the engine, change resources etc...

- make quality movies (machinima (this might request to develop appropriated tools), fragmovie etc...)

- for people that want to develop, see next the part, even if you don't know how to code at the moment.

- you already did some work in the past for a mod, you made models, textures or a map, you can
  contribute, we welcome any new resources (especially maps (with the .map) and models (also with sources if possible))

For any submission to the game, the best is to make a post on the redmine, send me a mail or to join on irc.

.. _redmine: http://redmine.quakelive.fr/

Developing
===========

Setting up a developement environnement
---------------------------------------

Help needed
-----------

- UI developer to make a GTK or QT UI to easily generate a basic config file

- Ressources : Help us replace all the copyrighted files in the filelist.txt to get a really free game.

Hacking with the code
-------------------

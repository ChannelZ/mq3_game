/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "client.h"

// globals
Dlib_t sndlib;
sndexport_t *snde;
sndimport_t sndi;

// the client has it's own sndexport_t structure with the wrapper functions
sndexport_t clsnde;

/*
=====================
CL_FillSoundImportLib
=====================
*/
static void
CL_FillSoundImportLib (sndimport_t * si)
{
  si->Printf = CL_Printf;
  si->Error = CL_Error;
  si->DPrintf = CL_DPrintf;
  si->Milliseconds = Sys_Milliseconds;
  si->Cmd_Argc = qce->cmd.Argc;
  si->Cmd_Argv = qce->cmd.Argv;
  si->Cmd_AddCommand = qce->cmd.AddCommand;
  si->Cmd_RemoveCommand = qce->cmd.RemoveCommand;
  si->Cvar_Get = qce->cvar.Get;
  si->Cvar_Set = qce->cvar.Set;
  si->Cvar_SetNew = qce->cvar.SetNew;
  si->Hunk_Alloc = qce->mem.Hunk_Alloc;
  si->Hunk_AllocateTempMemory = qce->mem.Hunk_AllocateTempMemory;
  si->Hunk_FreeTempMemory = qce->mem.Hunk_FreeTempMemory;
  si->FS_FOpenFileByMode = qce->fs.FOpenFileByMode;
  si->FS_Read = qce->fs.Read2;
  si->FS_ReadFile = qce->fs.ReadFile;
  si->FS_Write = qce->fs.Write;
  si->FS_FCloseFile = qce->fs.FCloseFile;
  si->FS_Seek = qce->fs.Seek;
  si->FS_FTell = qce->fs.FTell;
  si->FS_FileExists = qce->fs.FileExists;
  si->Z_Malloc = qce->mem.Z_Malloc;
  si->Z_Free = qce->mem.Z_Free;
}

/*
=====================
CL_FillSoundExportLib
=====================
*/
static void
CL_FillSoundExportLib (sndexport_t * snde)
{
  snde->Shutdown = CL_Snd_Shutdown;
  snde->StartSound = CL_Snd_StartSound;
  snde->StartLocalSound = CL_Snd_StartLocalSound;
  snde->StartBackgroundTrack = CL_Snd_StartBackgroundTrack;
  snde->StopBackgroundTrack = CL_Snd_StopBackgroundTrack;
  snde->RawSamples = CL_Snd_RawSamples;
  snde->StopAllSounds = CL_Snd_StopAllSounds;
  snde->ClearLoopingSounds = CL_Snd_ClearLoopingSounds;
  snde->AddLoopingSound = CL_Snd_AddLoopingSound;
  snde->AddRealLoopingSound = CL_Snd_AddRealLoopingSound;
  snde->StopLoopingSound = CL_Snd_StopLoopingSound;
  snde->Respatialize = CL_Snd_Respatialize;
  snde->UpdateEntityPosition = CL_Snd_UpdateEntityPosition;
  snde->Update = CL_Snd_Update;
  snde->DisableSounds = CL_Snd_DisableSounds;
  snde->BeginRegistration = CL_Snd_BeginRegistration;
  snde->RegisterSound = CL_Snd_RegisterSound;
  snde->ClearSoundBuffer = CL_Snd_ClearSoundBuffer;
  snde->SoundInfo = CL_Snd_SoundInfo;
  snde->SoundList = CL_Snd_SoundList;
}

/*
===============
CL_InitSoundLib
===============
*/
bool
CL_InitSoundLib (void)
{
  void *Hdl;
  sndexport_t *(*SoundAPI) (sndimport_t *);

  // fill the client copy of the sndexport structure even if the loading failed
  // the wrapped functions will handle theses cases
  CL_FillSoundExportLib (&clsnde);

  // free the Dlib struct
  Com_Memset (&sndlib, 0, sizeof(sndlib));

  // load the dynamic library
  sndlib.dynHandle = Sys_DLLoadType ("libq3snd", sndlib.fqpath, CLIENT);

  if (sndlib.dynHandle == NULL)
    {
      cls.soundlibLoaded = false;

      CL_Printf ("Can't load sound plugin\n");

      return false;
      //CL_Error (ERR_FATAL, "Can't load sound plugin\n");
    }

  // the library is loaded, resolve
  Hdl = sndlib.dynHandle;

  SoundAPI = (sndexport_t *(*)(sndimport_t *)) Sys_DLLoadFunction (Hdl, "GetSoundLibAPI", true);

  // we have a valid sound lib
  cls.soundlibLoaded = true;

  // soundAPI is valid here
  CL_FillSoundImportLib (&sndi);

  snde = SoundAPI (&sndi);

  if (snde == NULL)
    {
      cls.soundlibLoaded = false;
      CL_Printf ("Can't initialize sound.\n");

      return false;
    }

  return true;
}

/*
=================
CL_UnloadSoundLib
=================
*/
void
CL_UnloadSoundLib (void)
{
  if (!cls.soundlibLoaded)
    return;

  if (sndlib.dynHandle == NULL)
    return;

  /*
  if ((snde == NULL) || (snde->Shutdown == NULL))
    return;
  */

  CL_Printf ("Unloading Sound Lib\n");
  //snde->Shutdown ();
  Sys_DLUnload (sndlib.dynHandle);
  // by security but shouldn't be used
  snde = NULL;
  Com_Memset (&sndi, 0, sizeof (sndi));

  // only place to set this false
  cls.soundlibLoaded = false;
}

/*
================
CL_ShutdownSound
================
*/
void
CL_ShutdownSound (void)
{
  if (!cls.soundStarted)
    return;

  snde->Shutdown ();

  cls.soundRegistered = false;
  cls.soundStarted = false;
}

// list of macros and functions the client is safe to use for sound

  //void (*Init) (void);
void
CL_Snd_Shutdown (void)
{
  if (cls.soundStarted)
    snde->Shutdown ();
}

void
CL_Snd_StartSound (vec3_t origin, int entnum, int entchannel,
		   sfxHandle_t sfx)
{
  if (cls.soundStarted)
    snde->StartSound (origin, entnum, entchannel, sfx);
}

void
CL_Snd_StartLocalSound (sfxHandle_t sfx, int channelNum)
{
  if (cls.soundStarted)
    snde->StartLocalSound (sfx, channelNum);
}

void
CL_Snd_StartBackgroundTrack (const char *intro, const char *loop)
{
  if (cls.soundStarted)
    snde->StartBackgroundTrack (intro, loop);
}
void
CL_Snd_StopBackgroundTrack (void)
{
  if (cls.soundStarted)
    snde->StopBackgroundTrack ();
}

void
CL_Snd_RawSamples (int stream, int samples, int rate, int width,
		   int channels, const byte * data, float volume)
{
  if (cls.soundStarted)
    snde->RawSamples (stream, samples, rate, width, channels, data, volume);
}

void
CL_Snd_StopAllSounds (void)
{
  if (cls.soundStarted)
    snde->StopAllSounds ();
}

void
CL_Snd_ClearLoopingSounds (bool killall)
{
  if (cls.soundStarted)
    snde->ClearLoopingSounds (killall);
}

void
CL_Snd_AddLoopingSound (int entityNum, const vec3_t origin,
			const vec3_t velocity, sfxHandle_t sfx)
{
  if (cls.soundStarted)
    snde->AddLoopingSound (entityNum, origin, velocity, sfx);
}

void
CL_Snd_AddRealLoopingSound (int entityNum, const vec3_t origin,
			    const vec3_t velocity, sfxHandle_t sfx)
{
  if (cls.soundStarted)
    snde->AddLoopingSound (entityNum, origin, velocity, sfx);
}

void
CL_Snd_StopLoopingSound (int entityNum)
{
  if (cls.soundStarted)
    snde->StopLoopingSound (entityNum);
}

void
CL_Snd_Respatialize (int entityNum, const vec3_t origin, vec3_t axis[3],
		     int inwater)
{
  if (cls.soundStarted)
    snde->Respatialize (entityNum, origin, axis, inwater);
}

void
CL_Snd_UpdateEntityPosition (int entityNum, const vec3_t origin)
{
  if (cls.soundStarted)
    snde->UpdateEntityPosition (entityNum, origin);
}

void
CL_Snd_Update (void)
{
  if (cls.soundStarted)
    snde->Update ();
}

void
CL_Snd_DisableSounds (void)
{
  if (cls.soundStarted)
    snde->DisableSounds ();
}

void
CL_Snd_BeginRegistration (void)
{
  if (cls.soundStarted)
    snde->BeginRegistration ();
}

sfxHandle_t
CL_Snd_RegisterSound (const char *sample)
{
  if (cls.soundStarted)
    return snde->RegisterSound (sample);

  return 0;
}

void
CL_Snd_ClearSoundBuffer (void)
{
  if (cls.soundStarted)
    snde->ClearSoundBuffer ();
}

void
CL_Snd_SoundInfo (void)
{
  if (cls.soundStarted)
    snde->SoundInfo ();
}

void
CL_Snd_SoundList (void)
{
  if (cls.soundStarted)
    snde->SoundList ();
}

/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "client.h"

/*
=======================================================================

CLIENT SIDE DEMO RECORDING

=======================================================================
*/

// this shouldn't be here ?
// maintain a list of compatible protocols for demo playing
int demo_protocols[] = {66, 67, PROTOCOL_VERSION, 0};

/*
====================
CL_WriteDemoMessage

Dumps the current net message, prefixed by the length
====================
*/

void
CL_WriteDemoMessage (msg_t * msg, int headerBytes)
{
  int len;
  long swlen;

  // write the packet sequence
  len = clc.serverMessageSequence;
  swlen = LittleLong (len);
  qce->fs.Write (&swlen, 4, clc.demofile);

  // skip the packet sequencing information
  len = msg->cursize - headerBytes;
  swlen = LittleLong (len);
  qce->fs.Write (&swlen, 4, clc.demofile);
  qce->fs.Write (msg->data + headerBytes, len, clc.demofile);
}

/*
====================
CL_StopRecording_f

stop recording a demo
====================
*/
void
CL_StopRecord_f (void)
{
  int len;

  if (!clc.demorecording)
    {
      CL_Printf ("Not recording a demo.\n");
      return;
    }

  // finish up
  // no need to LittleLong here,
  // -1 is represented the same way regardless of endianness
  len = -1;
  qce->fs.Write (&len, 4, clc.demofile);
  qce->fs.Write (&len, 4, clc.demofile);
  qce->fs.FCloseFile (clc.demofile);
  clc.demofile = 0;
  clc.demorecording = false;
  CL_Printf ("Stopped demo.\n");
}

/*
==================
CL_DemoFilename
==================
*/
void
CL_DemoFilename (int number, char *fileName, size_t size)
{
  int a, b, c, d;

  if (number < 0 || number > 9999)
    number = 9999;

  a = number / 1000;
  number -= a * 1000;
  b = number / 100;
  number -= b * 100;
  c = number / 10;
  number -= c * 10;
  d = number;

  Com_sprintf (fileName, size, "demo%i%i%i%i", a, b, c, d);
}

/*
====================
CL_Record_f

record <demoname>

Begins recording a demo from the current position
====================
*/
void
CL_Record_f (void)
{
  char name[MAX_OSPATH];
  byte bufData[MAX_MSGLEN];
  char demoName[MAX_QPATH];
  msg_t buf;
  int i;
  int len;
  entityState_t *ent;
  entityState_t nullstate;
  char *s;

  if (qce->cmd.Argc () > 2)
    {
      CL_Printf ("record <demoname>\n");
      return;
    }

  if (clc.demorecording)
    {
      CL_Printf ("Already recording.\n");
      return;
    }

  if (cls.state != CA_ACTIVE)
    {
      CL_Printf ("You must be in a level to record.\n");
      return;
    }

  // sync 0 doesn't prevent recording, so not forcing it off ..
  // everyone does g_sync 1 ; record ; g_sync 0 ..
  if (qce->net.IsLocalAddress (clc.serverAddress)
      && !qce->cvar.VariableValue ("g_synchronousClients"))
    {
      CL_Printf (S_COLOR_YELLOW "WARNING: You should set "
		 "'g_synchronousClients 1' for smoother demo recording\n");
    }

  if (qce->cmd.Argc () == 2)
    {
      s = qce->cmd.Argv (1);
      Q_strncpyz (demoName, s, sizeof (demoName));
      Com_sprintf (name, sizeof (name), "demos/%s.%s%d",
		   demoName, DEMOEXT, PROTOCOL_VERSION);
    }
  else
    {
      int number;

      // scan for a free demo name
      for (number = 0; number <= 9999; number++)
	{
	  CL_DemoFilename (number, demoName, sizeof(demoName));
	  Com_sprintf (name, sizeof (name), "demos/%s.%s%d",
		       demoName, DEMOEXT, PROTOCOL_VERSION);

	  if (!qce->fs.FileExists (name))
	    break;		// file doesn't exist
	}
    }

  // open the demo file

  CL_Printf ("recording to %s.\n", name);
  clc.demofile = qce->fs.FOpenFileWrite (name);
  if (!clc.demofile)
    {
      CL_Printf ("ERROR: couldn't open.\n");
      return;
    }
  clc.demorecording = true;

  Q_strncpyz (clc.demoName, demoName, sizeof (clc.demoName));

  // don't start saving messages until a non-delta compressed message is received
  clc.demowaiting = true;

  // write out the gamestate message
  qce->msg.Init (&buf, bufData, sizeof (bufData));
  qce->msg.Bitstream (&buf);

  // NOTE, MRE: all server->client messages now acknowledge
  qce->msg.WriteLong (&buf, clc.reliableSequence);

  qce->msg.WriteByte (&buf, svc_gamestate);
  qce->msg.WriteLong (&buf, clc.serverCommandSequence);

  // configstrings
  for (i = 0; i < MAX_CONFIGSTRINGS; i++)
    {
      if (!cl.gameState.stringOffsets[i])
	{
	  continue;
	}
      s = cl.gameState.stringData + cl.gameState.stringOffsets[i];
      qce->msg.WriteByte (&buf, svc_configstring);
      qce->msg.WriteShort (&buf, i);
      qce->msg.WriteBigString (&buf, s);
    }

  // baselines
  Com_Memset (&nullstate, 0, sizeof (nullstate));
  for (i = 0; i < MAX_GENTITIES; i++)
    {
      ent = &cl.entityBaselines[i];
      if (!ent->number)
	{
	  continue;
	}
      qce->msg.WriteByte (&buf, svc_baseline);
      qce->msg.WriteDeltaEntity (&buf, &nullstate, ent, true);
    }

  qce->msg.WriteByte (&buf, svc_EOF);

  // finished writing the gamestate stuff

  // write the client num
  qce->msg.WriteLong (&buf, clc.clientNum);
  // write the checksum feed
  qce->msg.WriteLong (&buf, clc.checksumFeed);

  // finished writing the client packet
  qce->msg.WriteByte (&buf, svc_EOF);

  // write it to the demo file
  len = LittleLong (clc.serverMessageSequence - 1);
  qce->fs.Write (&len, 4, clc.demofile);

  len = LittleLong (buf.cursize);
  qce->fs.Write (&len, 4, clc.demofile);
  qce->fs.Write (buf.data, buf.cursize, clc.demofile);

  // the rest of the demo file will be copied from net messages
}

/*
=======================================================================

CLIENT SIDE DEMO PLAYBACK

=======================================================================
*/

/*
=================
CL_DemoFrameDurationSDev
=================
*/
static float
CL_DemoFrameDurationSDev (void)
{
  int i;
  int numFrames;
  float mean = 0.0f;
  float variance = 0.0f;

  if ((clc.timeDemoFrames - 1) > MAX_TIMEDEMO_DURATIONS)
    numFrames = MAX_TIMEDEMO_DURATIONS;
  else
    numFrames = clc.timeDemoFrames - 1;

  for (i = 0; i < numFrames; i++)
    mean += clc.timeDemoDurations[i];
  mean /= numFrames;

  for (i = 0; i < numFrames; i++)
    {
      float x = clc.timeDemoDurations[i];

      variance += ((x - mean) * (x - mean));
    }
  variance /= numFrames;

  return Q_sqrt (variance);
}

/*
=================
CL_DemoCompleted
=================
*/
void
CL_DemoCompleted (void)
{
  char buffer[MAX_STRING_CHARS];

  if (cl_timedemo && cl_timedemo->integer)
    {
      int time;

      time = Sys_Milliseconds () - clc.timeDemoStart;
      if (time > 0)
	{
	  // Millisecond times are frame durations:
	  // minimum/average/maximum/std deviation
	  Com_sprintf (buffer, sizeof (buffer),
		       "%i frames %3.1f seconds %3.1f fps %d.0/%.1f/%d.0/%.1f ms\n",
		       clc.timeDemoFrames, (time / 1000.0),
		       clc.timeDemoFrames * 1000.0 / time,
		       clc.timeDemoMinDuration,
		       time / (float) clc.timeDemoFrames,
		       clc.timeDemoMaxDuration, CL_DemoFrameDurationSDev ());
	  CL_Printf ("%s", buffer);

	  // Write a log of all the frame durations
	  if (cl_timedemoLog && strlen (cl_timedemoLog->string) > 0)
	    {
	      int i;
	      int numFrames;
	      fileHandle_t f;

	      if ((clc.timeDemoFrames - 1) > MAX_TIMEDEMO_DURATIONS)
		numFrames = MAX_TIMEDEMO_DURATIONS;
	      else
		numFrames = clc.timeDemoFrames - 1;

	      f = qce->fs.FOpenFileWrite (cl_timedemoLog->string);
	      if (f)
		{
		  qce->fs.Printf (f, "# %s", buffer);

		  for (i = 0; i < numFrames; i++)
		    qce->fs.Printf (f, "%d\n", clc.timeDemoDurations[i]);

		  qce->fs.FCloseFile (f);
		  CL_Printf ("%s written\n", cl_timedemoLog->string);
		}
	      else
		{
		  CL_Printf ("Couldn't open %s for writing\n",
			     cl_timedemoLog->string);
		}
	    }
	}
    }
  // reset the pause time for an other demo
  clc.demopausetime = 0;

  CL_Disconnect (true);
  //CL_NextDemo (); // add a proper playlist manager
}

/*
=================
CL_ReadDemoMessage
=================
*/
void
CL_ReadDemoMessage (void)
{
  int r;
  msg_t buf;
  byte bufData[MAX_MSGLEN];
  int s;

  if (!clc.demofile)
    {
      CL_DemoCompleted ();
      return;
    }

  // get the sequence number
  r = qce->fs.Read (&s, 4, clc.demofile);
  if (r != 4)
    {
      CL_DemoCompleted ();
      return;
    }
  clc.serverMessageSequence = LittleLong (s);

  // init the message
  qce->msg.Init (&buf, bufData, sizeof (bufData));

  // get the length
  r = qce->fs.Read (&buf.cursize, 4, clc.demofile);
  if (r != 4)
    {
      CL_DemoCompleted ();
      return;
    }
  buf.cursize = LittleLong (buf.cursize);
  if (buf.cursize == -1)
    {
      CL_DemoCompleted ();
      return;
    }
  if (buf.cursize > buf.maxsize)
    {
      CL_Error (ERR_DROP, "CL_ReadDemoMessage: demoMsglen > MAX_MSGLEN");
    }
  r = qce->fs.Read (buf.data, buf.cursize, clc.demofile);
  if (r != buf.cursize)
    {
      CL_Printf ("Demo file was truncated.\n");
      CL_DemoCompleted ();
      return;
    }

  clc.lastPacketTime = cls.realtime;
  buf.readcount = 0;
  CL_ParseServerMessage (&buf, cls.realtime);
}

/*
====================
CL_WalkDemoExt
====================
*/
static void
CL_WalkDemoExt (char *arg, char *name, int *demofile)
{
  int i = 0;
  *demofile = 0;

  // testing a compatibility list
  while (demo_protocols[i])
    {
      Com_sprintf (name, MAX_OSPATH, "demos/%s.%s%d",
		   arg, DEMOEXT, demo_protocols[i]);

      qce->fs.FOpenFileRead (name, demofile, true);
      if (*demofile)
	{
	  CL_Printf ("Demo file: %s\n", name);
	  break;
	}
      else
	{
	  CL_Printf ("Not found: %s\n", name);
	}
      i++;
    }
}

/*
====================
CL_CompleteDemoName
====================
*/
void
CL_CompleteDemoName (char *args, int argNum)
{
  if (argNum == 2)
    {
      char demoExt[16];

      Com_sprintf (demoExt, sizeof (demoExt), ".dm_%d", PROTOCOL_VERSION);
      qce->fs.Field_CompleteFilename ("demos", demoExt, true);
    }
}

/*
====================
CL_PlayDemo_f

demo <demoname>

====================
*/
void
CL_PlayDemo_f (void)
{
  char name[MAX_OSPATH];
  char *arg, *ext_test;
  int protocol, i;
  char retry[MAX_OSPATH];

  if (qce->cmd.Argc () != 2)
    {
      CL_Printf ("demo <demoname>\n");
      return;
    }

  // make sure a local server is killed
  // 2 means don't force disconnect of local client
  //Cvar_Set ("sv_killserver", "2");

  CL_Disconnect (true);

  // open the demo file
  arg = qce->cmd.Argv (1);

  // check for an extension .DEMOEXT_?? (?? is protocol)
  ext_test = Q_strrchr (arg, '.');

  if (ext_test && !Q_stricmpn (ext_test + 1, DEMOEXT, ARRAY_LEN (DEMOEXT) - 1))
    {
      protocol = atoi (ext_test + ARRAY_LEN (DEMOEXT));

      for(i = 0; demo_protocols[i]; i++)
	{
	  if (demo_protocols[i] == protocol)
	    break;
	}

      if (demo_protocols[i])
	{
	  Com_sprintf (name, sizeof (name), "demos/%s", arg);
	  qce->fs.FOpenFileRead (name, &clc.demofile, true);
	}
      else
	{
	  int len;

	  CL_Printf ("Protocol %d not supported for demos\n", protocol);
	  len = ext_test - arg;

	  if (len >= ARRAY_LEN (retry))
	    len = ARRAY_LEN (retry) - 1;

	  Q_strncpyz (retry, arg, len + 1);
	  retry[len] = '\0';
	  CL_WalkDemoExt (retry, name, &clc.demofile);
	}
    }
  else
    {
      CL_WalkDemoExt (arg, name, &clc.demofile);
    }

  if (!clc.demofile)
    {
      CL_Error (ERR_DISCONNECT, "couldn't open %s", name);
      return;
    }
  Q_strncpyz (clc.demoName, qce->cmd.Argv (1), sizeof (clc.demoName));

  Con_Close ();

  cls.state = CA_CONNECTED;
  clc.demoplaying = true;
  Q_strncpyz (cls.servername, qce->cmd.Argv (1), sizeof (cls.servername));

  // executing demo config if one is specified
  if (*cl_demoConfigFile->string)
    {
      qce->cmd.Cbuf_AddText (va ("exec %s", cl_demoConfigFile->string));
    }

  // read demo messages until connected
  while (cls.state >= CA_CONNECTED && cls.state < CA_PRIMED)
    {
      CL_ReadDemoMessage ();
    }
  // don't get the first snapshot this frame, to prevent the long
  // time from the gamestate load from messing causing a time skip
  clc.firstDemoFrameSkipped = false;
}

/*
====================
CL_DemoJump_f

demojump <demosequence>
====================
*/
void
CL_DemoJump_f (void)
{
  char *arg;
  int sequence;
  int savetime;

  // we must have loaded the demo first
  if (!clc.demoplaying)
    {
      CL_Printf ("A demo must be running first\n");
      return;
    }

  if (qce->cmd.Argc () != 2)
    {
      CL_Printf ("demojump <demosequence>\n"
		 "demojump +<demoseqdelta>\n");
      return;
    }

  arg = qce->cmd.Argv (1);
  if (arg[0] == '+')
    {
      int delta = atoi (arg + 1);

      sequence = clc.serverMessageSequence + delta;
    }
  else
    {
      sequence = atoi (arg);
    }

  if (sequence <= clc.serverMessageSequence)
    {
      CL_Printf ("demoSeq < currentSeq : Can't jump backward\n");
    }

  // be sure we don't infinite loop
  // if (cls.state != CA_ACTIVE) could also be used
  savetime = cl.snap.serverTime;
  while ((clc.serverMessageSequence < sequence) && clc.demoplaying)
    {
      CL_ReadDemoMessage ();

      // update cgame simulation without drawing
      // TODO : if the jump is too big and we should end the demo
      // we have an ugly error atm when processing snapshots
      // "CG_ProcessSnapshots: n < cg.latestSnapshotNum"
      CL_CGameProcessing ();
    }
  clc.demopausetime -= (cl.snap.serverTime - savetime);
}

/*
====================
CL_DemoPause_f

command : demopause
====================
*/
void
CL_DemoPause_f (void)
{
  static int tempo;

  if (!clc.demoplaying)
    return;

  if (clc.demopaused)
    {
      // we don't need to pass through the event backend
      int diff = cls.realtime;

      clc.demopaused = 0;
      clc.demopausetime += diff - tempo;
      clc.demopauselasttime = cls.realtime;
      //tempo = 0; // not needed
    }
  else
    {
      clc.demopaused = 1;
      tempo = cls.realtime;

      clc.demopauselasttime = cls.realtime;

      // if the client wants some info on the demo position print them
      if (cl_demoShowSequence->integer)
	CL_Printf ("Seq : %i\n", clc.serverMessageSequence);
    }
}

/*
====================
CL_PauseDemoIcons

displaying a play/pause icon when changing playback
====================
*/
#define DEMO_ICON_FADE_TIME 1000
void
CL_PauseDemoIcons (void)
{
  qhandle_t demoshaderpause, demoshaderplay;
  vec4_t color;

  demoshaderpause = re.RegisterShader ("gfx/misc/iconpause");
  demoshaderplay = re.RegisterShader ("gfx/misc/iconplay");

  int diff = cls.realtime - clc.demopauselasttime;
  if (diff < DEMO_ICON_FADE_TIME)
    {
      color[0] = color[1] = color[2] = 1.0f;
      color[3] = 1 - ((float)diff / (float)DEMO_ICON_FADE_TIME);
      re.SetColor (color);
      if (clc.demopaused)
	{
	  SCR_DrawPic (15, 50, 48, 48, demoshaderpause);
	}
      else
	{
	  SCR_DrawPic (15, 50, 48, 48, demoshaderplay);
	}
      re.SetColor (NULL);
    }
}

/*
====================
CL_StartDemoLoop

Closing the main menu will restart the demo loop
====================
*/
void
CL_StartDemoLoop (void)
{
  // start the demo loop again
  qce->cmd.Cbuf_AddText ("d1\n");
  Key_SetCatcher (0);
}

/*
==================
CL_NextDemo

Called when a demo or cinematic finishes
If the "nextdemo" cvar is set, that command will be issued
==================
*/
/*
void
CL_NextDemo (void)
{
  char v[MAX_STRING_CHARS];

  Q_strncpyz (v, qce->cvar.VariableString ("nextdemo"), sizeof (v));
  v[MAX_STRING_CHARS - 1] = 0;
  CL_DPrintf ("CL_NextDemo: %s\n", v);
  if (!v[0])
    {
      return;
    }

  qce->cvar.Set ("nextdemo", "");
  qce->cmd.Cbuf_AddText (v);
  qce->cmd.Cbuf_AddText ("\n");
  qce->cmd.Cbuf_Execute ();
}
*/

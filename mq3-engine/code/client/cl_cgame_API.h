/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "snd/snd_public.h"

#define CMD_BACKUP	64
#define CMD_MASK	(CMD_BACKUP - 1)
// allow a lot of command backups for very fast systems
// multiple commands may be combined into a single packet, so this
// needs to be larger than PACKET_BACKUP


#define MAX_ENTITIES_IN_SNAPSHOT 256

// snapshots are a view of the server at a given time

// Snapshots are generated at regular time intervals by the server,
// but they may not be sent if a client's rate level is exceeded, or
// they may be dropped by the network.
struct snapshot_s
{
  int snapFlags;		// SNAPFLAG_RATE_DELAYED, etc
  int ping;
  int serverTime;		// server time the message is valid for (in msec)
  int simulationTime;		// simulation time the game simulation was when snapshoting

  byte areamask[MAX_MAP_AREA_BYTES];	// portalarea visibility bits

  playerState_t ps;		// complete information about the current player at this time

  int numEntities;		// all of the entities that need to be presented
  entityState_t entities[MAX_ENTITIES_IN_SNAPSHOT];	// at the time of this snapshot

  int numServerCommands;	// text based server commands to execute when this
  int serverCommandSequence;	// snapshot becomes current
};
typedef struct snapshot_s snapshot_t;

/*
==================================================================

functions imported from the main executable

==================================================================
*/

#define	CGAME_IMPORT_API_VERSION	4

/*
========================================

APIs

========================================
*/

struct cgamelib_export_s
{
  char *(*init) (int serverMessageNum, int serverCommandSequence,
		 int clientNum);
  void (*shutdown) (void);
  bool (*console_command) (void);
  // serverTime is the clientside estimation of the server time
  // (this should be above snap->serverTime)
  // simulationTime is the clientside estimation of the game simulation time
  void (*draw_active_frame) (int serverTime, int simulationTime);
  void (*process_active_frame) (int serverTime, int simulationTime, bool demoPlayback);
  int (*crosshair_player) (void);
  int (*last_attacker) (void);
  void (*keyevent) (int key, bool down);
  void (*mouseevent) (int x, int y);
  void (*event_handling) (int type);
};
typedef struct cgamelib_export_s cgamelib_export_t;


struct cgamelib_import_s
{
  struct sndexport_s *snd;

  void (*Printf) (const char *fmt, ...);
  void (*Error) (int code, const char *fmt, ...);
  unsigned int (*Milliseconds) (void);
  int (*Argc) (void);
  const char *(*Argv) (int n);
  void (*ArgvBuffer) (int n, char *buffer, int bufferLength);
  void (*Args) (char *buffer, int bufferLength);
  int (*FS_FOpenFile) (const char *qpath, fileHandle_t * f, fsMode_t mode);
  int (*FS_Read) (const void *buffer, int len, fileHandle_t f);
  int (*FS_Write) (const void *buffer, int len, fileHandle_t f);
  void (*FS_FCloseFile) (fileHandle_t f);
  //int (*FS_GetFileList) (const char *path, const char *extension, char *listbuf, int bufsize);
  int (*FS_Seek) (fileHandle_t f, long offset, int origin);	// fsOrigin_t
  void (*SendConsoleCommand) (const char *text);
  void (*SendClientCommand) (const char *cmd);
  cvar_t *(*Cvar_Get) (const char * var_name, const char * value, int flags);
  void (*Cvar_Set) (const char *var_name, const char *value);
  int (*Cvar_SetNew) (cvar_t * var, const char * value);
  void (*Cvar_CheckRange) (cvar_t * var, float min, float max, bool integral);
  //int (*Cvar_VariableIntegerValue) (const char *var_name);
  //float (*Cvar_VariableValue) (const char *var_name);
  void (*Cvar_VariableStringBuffer) (const char *var_name, char *buffer,
				     int bufsize);
  void (*AddCommand) (const char *cmdName);
  void (*RemoveCommand) (const char *cmdName);

  void (*UpdateScreen) (void);

  void (*CM_LoadMap) (const char *mapname);
  int (*CM_NumInlineModels) (void);
  clipHandle_t (*CM_InlineModel) (int index);
  clipHandle_t (*CM_TempBoxModel) (const vec3_t mins, const vec3_t maxs);
  int (*CM_PointContents) (const vec3_t p, clipHandle_t model);
  int (*CM_TransformedPointContents) (const vec3_t p, clipHandle_t model,
				      const vec3_t origin,
				      const vec3_t angles);
  void (*CM_BoxTrace) (trace_t * results, const vec3_t start,
		       const vec3_t end, const vec3_t mins, const vec3_t maxs,
		       clipHandle_t model, int brushmask);
  void (*CM_TransformedBoxTrace) (trace_t * results, const vec3_t start,
				  const vec3_t end, const vec3_t mins,
				  const vec3_t maxs, clipHandle_t model,
				  const vec3_t origin,
				  int brushmask /*, const vec3_t angles */ );
  int (*CM_MarkFragments) (int numPoints, const vec3_t * points,
			   const vec3_t projection, int maxPoints,
			   vec3_t pointBuffer, int maxFragments,
			   markFragment_t * fragmentBuffer);

  void (*R_LoadWorldMap) (const char *mapname);
  qhandle_t (*R_RegisterModel) (const char *name);
  qhandle_t (*R_RegisterModelExt) (const char *name, int flags);
  qhandle_t (*R_RegisterSkin) (const char *name);
  qhandle_t (*R_RegisterSkinExt) (const char *name, int flags);
  qhandle_t (*R_RegisterShader) (const char *name);
  qhandle_t (*R_RegisterShaderExt) (const char *name, int flags);
  void (*R_LoadFace) (const char * fileName, int pointSize,
		      const char * name, fontFace_t * face);
  void (*R_FreeFace) (fontFace_t * face);

  void (*R_ClearScene) (void);
  void (*R_AddRefEntityToScene) (const refEntity_t * re);
  //void (*R_AddPolyToScene) (qhandle_t hShader , int numVerts,
  //                           const polyVert_t *verts);
  void (*R_AddPolysToScene) (qhandle_t hShader, int numVerts,
			     const polyVert_t * verts, int num);
  int (*R_LightForPoint) (vec3_t point, vec3_t ambientLight,
			  vec3_t directedLight, vec3_t lightDir);
  void (*R_AddLightToScene) (const vec3_t org, float intensity,
			     float r, float g, float b);
  void (*R_AddAdditiveLightToScene) (const vec3_t org, float intensity,
				     float r, float g, float b);
  void (*R_RenderScene) (const refdef_t * fd);

  void (*R_SetColor) (const float *rgba);
  void (*R_DrawStretchPic) (float x, float y, float w, float h,
			    float s1, float t1, float s2, float t2,
			    qhandle_t hShader);
  void (*R_ModelBounds) (clipHandle_t model, vec3_t mins, vec3_t maxs);
  int (*R_LerpTag) (orientation_t * tag, clipHandle_t mod, int startFrame,
		    int endFrame, float frac, const char *tagName);

  bool (*R_inPVS) (const vec3_t p1, const vec3_t p2);
  void (*GetGlconfig) (glconfig_t * glconfig);
  void (*GetGameState) (gameState_t * gamestate);
  void (*GetCurrentSnapshotNumber) (int *snapshotNumber, int *serverTime);
  bool (*GetSnapshot) (int snapshotNumber, snapshot_t * snapshot);
  bool (*GetServerCommand) (int serverCommandNumber);
  int (*GetCurrentCmdNumber) (void);
  bool (*GetUserCmd) (int cmdNumber, usercmd_t * ucmd);
  void (*SetUserCmdValue) (int stateValue, float sensitivityScale);

  //bool (*Key_IsDown) (int keynum);
  int (*Key_GetCatcher) (void);
  void (*Key_SetCatcher) (int catcher);
  //int (*Key_GetKey) (const char *binding);

  int (*RealTime) (qtime_t * qtime);
  void (*SnapVector) (float *v);

  bool (*GetEntityToken) (char *buffer, int bufferSize);

  int (*MemoryRemaining) (void);
};
typedef struct cgamelib_import_s cgamelib_import_t;

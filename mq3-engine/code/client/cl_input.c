/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// cl.input.c  -- builds an intended movement command to send to the server

#include "client.h"

// global to this file
unsigned frame_msec;
int old_cl_frameTime;

/*
============================================================
Cvars
============================================================
*/


// let theses cvars remain local to this file
// move their assignement from CL_Init to some CL_Input_Init function
cvar_t *cl_yawspeed;
cvar_t *cl_pitchspeed;

cvar_t *cl_run;

cvar_t *cl_anglespeedkey;

/*
===============================================================================

KEY BUTTONS

Continuous button event tracking is complicated by the fact that two different
input sources (say, mouse button 1 and the control key) can both press the
same button, but the button should only be released when both of the
pressing key have been released.

When a key event issues a button command (+forward, +attack, etc), it appends
its key number as argv(1) so it can be matched up with the release.

argv(2) will be set to the time the event happened, which allows exact
control even at low framerates when the down and up events may both get qued
at the same time.

===============================================================================
*/

kbutton_t in_left, in_right, in_forward, in_back;
kbutton_t in_lookup, in_lookdown, in_moveleft, in_moveright;
kbutton_t in_strafe, in_speed;
kbutton_t in_up, in_down;

#define IN_MAX_BUTTONS 8
kbutton_t in_buttons[IN_MAX_BUTTONS];

bool in_mlooking;

/*
=============
IN_CenterView
=============
*/
static void
IN_CenterView (void)
{
  cl.viewangles[PITCH] = -SHORT2ANGLE (cl.snap.ps.delta_angles[PITCH]);
}

/*
============
IN_MLookDown
============
*/
static void
IN_MLookDown (void)
{
  in_mlooking = true;
}

/*
==========
IN_MLookUp
==========
*/
static void
IN_MLookUp (void)
{
  in_mlooking = false;
  if (!cl_freelook->integer)
    {
      IN_CenterView ();
    }
}

/*
==========
IN_KeyDown
==========
*/
static void
IN_KeyDown (kbutton_t * b)
{
  int k;
  char *c;

  c = qce->cmd.Argv (1);
  if (c[0])
    {
      k = atoi (c);
    }
  else
    {
      k = -1;		// typed manually at the console for continuous down
    }

  if (k == b->down[0] || k == b->down[1])
    {
      return;		// repeating key
    }

  if (!b->down[0])
    {
      b->down[0] = k;
    }
  else if (!b->down[1])
    {
      b->down[1] = k;
    }
  else
    {
      CL_Printf ("Three keys down for a button!\n");
      return;
    }

  if (b->active)
    {
      return;		// still down
    }

  // save timestamp for partial frame summing
  c = qce->cmd.Argv (2);
  b->downtime = atoi (c);

  b->active = true;
  b->wasPressed = true;
}

/*
========
IN_KeyUp
========
*/
static void
IN_KeyUp (kbutton_t * b)
{
  int k;
  char *c;
  unsigned uptime;

  c = qce->cmd.Argv (1);
  if (c[0])
    {
      k = atoi (c);
    }
  else
    {
      // typed manually at the console, assume for unsticking, so clear all
      b->down[0] = b->down[1] = 0;
      b->active = false;
      return;
    }

  if (b->down[0] == k)
    {
      b->down[0] = 0;
    }
  else if (b->down[1] == k)
    {
      b->down[1] = 0;
    }
  else
    {
      return;		// key up without coresponding down (menu pass through)
    }
  if (b->down[0] || b->down[1])
    {
      return;		// some other key is still holding it down
    }

  b->active = false;

  // save timestamp for partial frame summing
  c = qce->cmd.Argv (2);
  uptime = atoi (c);
  if (uptime)
    {
      b->msec += uptime - b->downtime;
    }
  else
    {
      b->msec += frame_msec / 2;
    }

  b->active = false;
}

/*
===============
CL_KeyState

Returns the fraction of the frame that the key was down
===============
*/
float
CL_KeyState (kbutton_t * key)
{
  float val;
  int msec;

  msec = key->msec;
  key->msec = 0;

  if (key->active)
    {
      // still down
      if (!key->downtime)
	{
	  msec = cl_frameTime;
	}
      else
	{
	  msec += cl_frameTime - key->downtime;
	}
      key->downtime = cl_frameTime;
    }

#if 0
  if (msec)
    {
      CL_Printf ("%i ", msec);
    }
#endif

  val = (float)msec / (float)frame_msec;
  if (val < 0)
    {
      val = 0;
    }
  if (val > 1)
    {
      val = 1;
    }

  return val;
}

/*
================
Linking with kbutton structures
================
*/
static void
IN_UpDown (void)
{
  IN_KeyDown (&in_up);
}

static void
IN_UpUp (void)
{
  IN_KeyUp (&in_up);
}

static void
IN_DownDown (void)
{
  IN_KeyDown (&in_down);
}

static void
IN_DownUp (void)
{
  IN_KeyUp (&in_down);
}

static void
IN_LeftDown (void)
{
  IN_KeyDown (&in_left);
}

static void
IN_LeftUp (void)
{
  IN_KeyUp (&in_left);
}

static void
IN_RightDown (void)
{
  IN_KeyDown (&in_right);
}

static void
IN_RightUp (void)
{
  IN_KeyUp (&in_right);
}

static void
IN_ForwardDown (void)
{
  IN_KeyDown (&in_forward);
}

static void
IN_ForwardUp (void)
{
  IN_KeyUp (&in_forward);
}

static void
IN_BackDown (void)
{
  IN_KeyDown (&in_back);
}

static void
IN_BackUp (void)
{
  IN_KeyUp (&in_back);
}

static void
IN_LookupDown (void)
{
  IN_KeyDown (&in_lookup);
}

static void
IN_LookupUp (void)
{
  IN_KeyUp (&in_lookup);
}

static void
IN_LookdownDown (void)
{
  IN_KeyDown (&in_lookdown);
}

static void
IN_LookdownUp (void)
{
  IN_KeyUp (&in_lookdown);
}

static void
IN_MoveleftDown (void)
{
  IN_KeyDown (&in_moveleft);
}

static void
IN_MoveleftUp (void)
{
  IN_KeyUp (&in_moveleft);
}

static void
IN_MoverightDown (void)
{
  IN_KeyDown (&in_moveright);
}

static void
IN_MoverightUp (void)
{
  IN_KeyUp (&in_moveright);
}

static void
IN_SpeedDown (void)
{
  IN_KeyDown (&in_speed);
}

static void
IN_SpeedUp (void)
{
  IN_KeyUp (&in_speed);
}

static void
IN_StrafeDown (void)
{
  IN_KeyDown (&in_strafe);
}

static void
IN_StrafeUp (void)
{
  IN_KeyUp (&in_strafe);
}

static void
IN_Button0Down (void)
{
  IN_KeyDown (&in_buttons[0]);
}

static void
IN_Button0Up (void)
{
  IN_KeyUp (&in_buttons[0]);
}

static void
IN_Button1Down (void)
{
  IN_KeyDown (&in_buttons[1]);
}

static void
IN_Button1Up (void)
{
  IN_KeyUp (&in_buttons[1]);
}

static void
IN_Button2Down (void)
{
  IN_KeyDown (&in_buttons[2]);
}

static void
IN_Button2Up (void)
{
  IN_KeyUp (&in_buttons[2]);
}

static void
IN_Button3Down (void)
{
  IN_KeyDown (&in_buttons[3]);
}

static void
IN_Button3Up (void)
{
  IN_KeyUp (&in_buttons[3]);
}

static void
IN_Button4Down (void)
{
  IN_KeyDown (&in_buttons[4]);
}

static void
IN_Button4Up (void)
{
  IN_KeyUp (&in_buttons[4]);
}

static void
IN_Button5Down (void)
{
  IN_KeyDown (&in_buttons[5]);
}

static void
IN_Button5Up (void)
{
  IN_KeyUp (&in_buttons[5]);
}

static void
IN_Button6Down (void)
{
  IN_KeyDown (&in_buttons[6]);
}

static void
IN_Button6Up (void)
{
  IN_KeyUp (&in_buttons[6]);
}

static void
IN_Button7Down (void)
{
  IN_KeyDown (&in_buttons[7]);
}

static void
IN_Button7Up (void)
{
  IN_KeyUp (&in_buttons[7]);
}

//==========================================================================

/*
================
CL_AdjustAngles

Moves the local angle positions
================
*/
static void
CL_AdjustAngles (void)
{
  float speed;

  if (in_speed.active)
    {
      speed = 0.001 * cls.frametime * cl_anglespeedkey->value;
    }
  else
    {
      speed = 0.001 * cls.frametime;
    }

  if (!in_strafe.active)
    {
      cl.viewangles[YAW] -=
	speed * cl_yawspeed->value * CL_KeyState (&in_right);
      cl.viewangles[YAW] +=
	speed * cl_yawspeed->value * CL_KeyState (&in_left);
    }

  cl.viewangles[PITCH] -=
    speed * cl_pitchspeed->value * CL_KeyState (&in_lookup);
  cl.viewangles[PITCH] +=
    speed * cl_pitchspeed->value * CL_KeyState (&in_lookdown);
}

/*
================
CL_KeyMove

Sets the usercmd_t based on key states
================
*/
static void
CL_KeyMove (usercmd_t * cmd)
{
  int movespeed;
  int forward, side, up;

  //
  // adjust for speed key / running
  // the walking flag is to keep animations consistant
  // even during acceleration and deceleration
  //
  if (in_speed.active ^ cl_run->integer)
    {
      movespeed = 127;
      cmd->buttons &= ~BUTTON_WALKING;
    }
  else
    {
      cmd->buttons |= BUTTON_WALKING;
      movespeed = 64;
    }

  forward = 0;
  side = 0;
  up = 0;
  if (in_strafe.active)
    {
      side += movespeed * CL_KeyState (&in_right);
      side -= movespeed * CL_KeyState (&in_left);
    }

  side += movespeed * CL_KeyState (&in_moveright);
  side -= movespeed * CL_KeyState (&in_moveleft);

  up += movespeed * CL_KeyState (&in_up);
  up -= movespeed * CL_KeyState (&in_down);

  forward += movespeed * CL_KeyState (&in_forward);
  forward -= movespeed * CL_KeyState (&in_back);

  cmd->forwardmove = ClampChar (forward);
  cmd->rightmove = ClampChar (side);
  cmd->upmove = ClampChar (up);
}

/*
=================
CL_MouseEvent
=================
*/
void
CL_MouseEvent (int dx, int dy, int time)
{
  cl.mouseDx[cl.mouseIndex] += dx;
  cl.mouseDy[cl.mouseIndex] += dy;
}

/*
=================
CL_MouseMove
=================
*/
static void
CL_MouseMove (usercmd_t *cmd)
{
  float mx, my;
  float accelSensitivityX, accelSensitivityY;
  static int counter = 0;

  // allow mouse smoothing
  if (m_filter->integer)
    {
      mx = (cl.mouseDx[0] + cl.mouseDx[1]) * 0.5;
      my = (cl.mouseDy[0] + cl.mouseDy[1]) * 0.5;
    }
  else
    {
      mx = cl.mouseDx[cl.mouseIndex];
      my = cl.mouseDy[cl.mouseIndex];
    }

  // flip index
  cl.mouseIndex ^= 1;
  cl.mouseDx[cl.mouseIndex] = 0;
  cl.mouseDy[cl.mouseIndex] = 0;

  // nothing to do
  if ((mx == 0.0f) && (my == 0.0f))
    {
      return;
    }

  // accelstyle 1; using offsets before applying the accel factor
  if (cl_mouseAccelStyle->integer == 1)
    {
      float rateX, rateY;

      rateX = fabsf (mx) / (float) frame_msec;
      rateY = fabsf (my) / (float) frame_msec;

      // X axis
      if (rateX < cl_mouseAccelOffsetX->value)
	{
	  // not enough rate for accel
	  accelSensitivityX = cl_sensitivity->value;
	}
      else
	{
	  // here we got accel with an offset
	  // also split sensitivity and mouseaccel ?
	  accelSensitivityX = cl_sensitivity->value
	    + (rateX - cl_mouseAccelOffsetX->value) * cl_mouseAccel->value;
	}
      // Y axis
      if (rateY < cl_mouseAccelOffsetY->value)
	{
	  accelSensitivityY = cl_sensitivity->value;
	}
      else
	{
	  accelSensitivityY = cl_sensitivity->value
	    + (rateY - cl_mouseAccelOffsetY->value) * cl_mouseAccel->value;
	}

      // not a very "clean" output
      // useful for tweaking accel style 1
      if (cl_showMouseRate->integer && !(counter % 50))
	{
	  CL_Printf ("x : %4f - y : %4f : accel : x : %4f - y : %4f\n",
		     (rateX - cl_mouseAccelOffsetX->value),
		     (rateY - cl_mouseAccelOffsetY->value),
		     accelSensitivityX, accelSensitivityY);
	}
      counter++;
    }
  // default accel from q3, acceleration factor is coupled with X and Y axis
  else if (cl_mouseAccelStyle->integer == 2)
    {
      float rate;

      rate = sqrtf (mx * mx + my * my) / (float) frame_msec;

      accelSensitivityX = cl_sensitivity->value + rate * cl_mouseAccel->value;
      accelSensitivityY = cl_sensitivity->value + rate * cl_mouseAccel->value;

      if (cl_showMouseRate->integer && !(counter % 50))
	{
	  CL_Printf ("rate: %4f, accelSensitivity: %4f\n", rate, accelSensitivityX);
	}
      counter++;
    }
  // default accel "style", but with axis decoupled
  else
    {
      float rateX, rateY;

      rateX = fabsf (mx) / (float) frame_msec;
      rateY = fabsf (my) / (float) frame_msec;

      accelSensitivityX = cl_sensitivity->value + rateX * cl_mouseAccel->value;
      accelSensitivityY = cl_sensitivity->value + rateY * cl_mouseAccel->value;

      if (cl_showMouseRate->integer && !(counter % 50))
	{
	  CL_Printf ("rateX: %4f, rateY: %4f, "
		     "accelSensitivityX: %4f, accelSensitivityY: %4f\n",
		     rateX, rateY, accelSensitivityX, accelSensitivityY);
	}
      counter++;
    }

  // scale by FOV
  accelSensitivityX *= cl.cgameSensitivity;
  accelSensitivityY *= cl.cgameSensitivity;

  mx *= accelSensitivityX;
  my *= accelSensitivityY;

  // add mouse X/Y movement to cmd
  if (in_strafe.active)
    {
      cmd->rightmove = ClampChar (cmd->rightmove + m_side->value * mx);
    }
  else
    {
      cl.viewangles[YAW] -= m_yaw->value * mx;
    }

  if ((in_mlooking || cl_freelook->integer) && !in_strafe.active)
    {
      cl.viewangles[PITCH] += m_pitch->value * my;
    }
  else
    {
      cmd->forwardmove = ClampChar (cmd->forwardmove - m_forward->value * my);
    }
}

/*
==============
CL_CmdButtons
==============
*/
static void
CL_CmdButtons (usercmd_t * cmd)
{
  int i;

  //
  // figure button bits
  // send a button bit even if the key was pressed and released in
  // less than a frame
  //      
  for (i = 0; i < IN_MAX_BUTTONS; i++)
    {
      if (in_buttons[i].active || in_buttons[i].wasPressed)
	{
	  cmd->buttons |= 1 << i;
	}
      in_buttons[i].wasPressed = false;
    }

  // the game knows when a player is in console or chatting
  if (Key_GetCatcher ())
    {
      cmd->buttons |= BUTTON_TALK;
    }

  // allow the game to know if any key at all is
  // currently pressed, even if it isn't bound to anything
  if (anykeydown && Key_GetCatcher () == 0)
    {
      cmd->buttons |= BUTTON_ANY;
    }
}

/*
==============
CL_FinishMove
==============
*/
static void
CL_FinishMove (usercmd_t * cmd)
{
  int i;

  // copy the state that the cgame is currently sending
  cmd->weapon = cl.cgameUserCmdWeapon;

  // send the current server time so the amount of movement
  // can be determined without allowing cheating
  cmd->serverTime = cl.serverTime;

  for (i = 0; i < 3; i++)
    {
      cmd->angles[i] = ANGLE2SHORT (cl.viewangles[i]);
    }
}

/*
=================
CL_CreateCmd
=================
*/
bool
CL_CreateCmd (usercmd_t * cmd)
{
  vec3_t oldAngles;

  VectorCopy (cl.viewangles, oldAngles);

  // keyboard angle adjustment
  CL_AdjustAngles ();

  Com_Memset (cmd, 0, sizeof (*cmd));

  CL_CmdButtons (cmd);

  // get basic movement from keyboard
  CL_KeyMove (cmd);

  // get basic movement from mouse
  CL_MouseMove (cmd);

  // check to make sure the angles haven't wrapped
  if (cl.viewangles[PITCH] - oldAngles[PITCH] > 90)
    {
      cl.viewangles[PITCH] = oldAngles[PITCH] + 90;
    }
  else if (oldAngles[PITCH] - cl.viewangles[PITCH] > 90)
    {
      cl.viewangles[PITCH] = oldAngles[PITCH] - 90;
    }

  // store out the final values
  CL_FinishMove (cmd);

  return true;
}

/*
=================
CL_CreateNewCommands

Create a new usercmd_t structure for this frame
=================
*/
void
CL_CreateNewCommands (void)
{
  int cmdNum;

  // no need to create usercmds until we have a gamestate
  if (cls.state < CA_PRIMED)
    {
      return;
    }

  frame_msec = cl_frameTime - old_cl_frameTime;

  // if running less than 5fps, truncate the extra time to prevent
  // unexpected moves after a hitch
  if (frame_msec > 200)
    {
      frame_msec = 200;
    }
  old_cl_frameTime = cl_frameTime;


  // generate a command for this frame
  cl.cmdNumber++;
  cmdNum = cl.cmdNumber & CMD_MASK;
  CL_CreateCmd (&cl.cmds[cmdNum]);
}

/*
============
CL_InitInput
============
*/
void
CL_InitInput (void)
{
  qce->cmd.AddCommand ("centerview", IN_CenterView);

  qce->cmd.AddCommand ("+moveup", IN_UpDown);
  qce->cmd.AddCommand ("-moveup", IN_UpUp);
  qce->cmd.AddCommand ("+movedown", IN_DownDown);
  qce->cmd.AddCommand ("-movedown", IN_DownUp);
  qce->cmd.AddCommand ("+forward", IN_ForwardDown);
  qce->cmd.AddCommand ("-forward", IN_ForwardUp);
  qce->cmd.AddCommand ("+back", IN_BackDown);
  qce->cmd.AddCommand ("-back", IN_BackUp);
  qce->cmd.AddCommand ("+moveleft", IN_MoveleftDown);
  qce->cmd.AddCommand ("-moveleft", IN_MoveleftUp);
  qce->cmd.AddCommand ("+moveright", IN_MoverightDown);
  qce->cmd.AddCommand ("-moveright", IN_MoverightUp);
  qce->cmd.AddCommand ("+left", IN_LeftDown);
  qce->cmd.AddCommand ("-left", IN_LeftUp);
  qce->cmd.AddCommand ("+right", IN_RightDown);
  qce->cmd.AddCommand ("-right", IN_RightUp);
  qce->cmd.AddCommand ("+lookup", IN_LookupDown);
  qce->cmd.AddCommand ("-lookup", IN_LookupUp);
  qce->cmd.AddCommand ("+lookdown", IN_LookdownDown);
  qce->cmd.AddCommand ("-lookdown", IN_LookdownUp);
  qce->cmd.AddCommand ("+strafe", IN_StrafeDown);
  qce->cmd.AddCommand ("-strafe", IN_StrafeUp);
  qce->cmd.AddCommand ("+speed", IN_SpeedDown);
  qce->cmd.AddCommand ("-speed", IN_SpeedUp);
  qce->cmd.AddCommand ("+attack", IN_Button0Down);
  qce->cmd.AddCommand ("-attack", IN_Button0Up);
  qce->cmd.AddCommand ("+button0", IN_Button0Down);
  qce->cmd.AddCommand ("-button0", IN_Button0Up);
  qce->cmd.AddCommand ("+button1", IN_Button1Down);
  qce->cmd.AddCommand ("-button1", IN_Button1Up);
  qce->cmd.AddCommand ("+button2", IN_Button2Down);
  qce->cmd.AddCommand ("-button2", IN_Button2Up);
  qce->cmd.AddCommand ("+button3", IN_Button3Down);
  qce->cmd.AddCommand ("-button3", IN_Button3Up);
  qce->cmd.AddCommand ("+button4", IN_Button4Down);
  qce->cmd.AddCommand ("-button4", IN_Button4Up);
  qce->cmd.AddCommand ("+button5", IN_Button5Down);
  qce->cmd.AddCommand ("-button5", IN_Button5Up);
  qce->cmd.AddCommand ("+button6", IN_Button6Down);
  qce->cmd.AddCommand ("-button6", IN_Button6Up);
  qce->cmd.AddCommand ("+button7", IN_Button7Down);
  qce->cmd.AddCommand ("-button7", IN_Button7Up);
  qce->cmd.AddCommand ("+mlook", IN_MLookDown);
  qce->cmd.AddCommand ("-mlook", IN_MLookUp);

  cl_nodelta = qce->cvar.Get ("cl_nodelta", "0", 0);
}

/*
=============
CL_CloseInput
=============
*/
void
CL_CloseInput (void)
{
  qce->cmd.RemoveCommand ("centerview");

  qce->cmd.RemoveCommand ("+moveup");
  qce->cmd.RemoveCommand ("-moveup");
  qce->cmd.RemoveCommand ("+movedown");
  qce->cmd.RemoveCommand ("-movedown");
  qce->cmd.RemoveCommand ("+forward");
  qce->cmd.RemoveCommand ("-forward");
  qce->cmd.RemoveCommand ("+back");
  qce->cmd.RemoveCommand ("-back");
  qce->cmd.RemoveCommand ("+moveleft");
  qce->cmd.RemoveCommand ("-moveleft");
  qce->cmd.RemoveCommand ("+moveright");
  qce->cmd.RemoveCommand ("-moveright");
  qce->cmd.RemoveCommand ("+left");
  qce->cmd.RemoveCommand ("-left");
  qce->cmd.RemoveCommand ("+right");
  qce->cmd.RemoveCommand ("-right");
  qce->cmd.RemoveCommand ("+lookup");
  qce->cmd.RemoveCommand ("-lookup");
  qce->cmd.RemoveCommand ("+lookdown");
  qce->cmd.RemoveCommand ("-lookdown");
  qce->cmd.RemoveCommand ("+strafe");
  qce->cmd.RemoveCommand ("-strafe");
  qce->cmd.RemoveCommand ("+speed");
  qce->cmd.RemoveCommand ("-speed");
  qce->cmd.RemoveCommand ("+attack");
  qce->cmd.RemoveCommand ("-attack");
  qce->cmd.RemoveCommand ("+button0");
  qce->cmd.RemoveCommand ("-button0");
  qce->cmd.RemoveCommand ("+button1");
  qce->cmd.RemoveCommand ("-button1");
  qce->cmd.RemoveCommand ("+button2");
  qce->cmd.RemoveCommand ("-button2");
  qce->cmd.RemoveCommand ("+button3");
  qce->cmd.RemoveCommand ("-button3");
  qce->cmd.RemoveCommand ("+button4");
  qce->cmd.RemoveCommand ("-button4");
  qce->cmd.RemoveCommand ("+button5");
  qce->cmd.RemoveCommand ("-button5");
  qce->cmd.RemoveCommand ("+button6");
  qce->cmd.RemoveCommand ("-button6");
  qce->cmd.RemoveCommand ("+button7");
  qce->cmd.RemoveCommand ("-button7");
  qce->cmd.RemoveCommand ("+mlook");
  qce->cmd.RemoveCommand ("-mlook");
}

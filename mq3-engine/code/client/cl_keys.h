/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// Remove this dependancy: should be handled by input module
#include "../input/in_keycodes.h"

struct qkey_s
{
  bool down;
  int repeats;		// if > 1, it is autorepeating
  char *binding;
};
typedef struct qkey_s qkey_t;

extern qkey_t keys[MAX_KEYS];

// NOTE : the declaration of field_t and Field_Clear is in qcommon
void Field_KeyDownEvent (field_t * edit, int key);
void Field_CharEvent (field_t * edit, const char *s);
void Field_Draw (field_t * edit, int x, int y, int width, bool showCursor,
		 bool noColorEscape);
void Field_BigDraw (field_t * edit, int x, int y, int width,
		    bool showCursor, bool noColorEscape);

#define	COMMAND_HISTORY	32
extern field_t historyEditLines[COMMAND_HISTORY];

extern field_t g_consoleField;
extern field_t chatField;
extern int anykeydown;
extern bool chat_team;
extern int chat_playerNum;

void Key_WriteBindings (fileHandle_t f);
void Key_SetBinding (int keynum, const char *binding);
char *Key_GetBinding (int keynum);
bool Key_IsDown (int keynum);
bool Key_GetOverstrikeMode (void);
void Key_SetOverstrikeMode (bool state);
void Key_ClearStates (void);
int Key_GetKey (const char *binding);

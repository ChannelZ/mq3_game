/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// console.c

#include "client.h"

#define DEFAULT_CONSOLE_WIDTH 78

int g_console_field_width = DEFAULT_CONSOLE_WIDTH;

//#define       NUM_CON_TIMES 4

// this will need qcommon to be loaded to register cvars
bool con_backendinitialized = false;

console_t con;

cvar_t *con_conspeed;
cvar_t *con_notifytime;
cvar_t *con_fraction;
cvar_t *con_notifylines;
cvar_t *con_timestamps;
cvar_t *con_backgroundcolor;
cvar_t *con_bordercolor;

/*
====================
Console cursor
shifting functions
====================
*/
void
Con_PageUpC (console_t * con)
{
  con->display -= 2;
  if (con->current - con->display >= con->totallines)
    {
      con->display = con->current - con->totallines + 1;
    }
}

void
Con_PageDownC (console_t * con)
{
  con->display += 2;
  if (con->display > con->current)
    {
      con->display = con->current;
    }
}

void
Con_TopC (console_t * con)
{
  con->display = con->totallines;
  if (con->current - con->display >= con->totallines)
    {
      con->display = con->current - con->totallines + 1;
    }
}

void
Con_BottomC (console_t * con)
{
  con->display = con->current;
}

// functions for main console
void
Con_PageUp (void)
{
  Con_PageUpC (&con);
}

void
Con_PageDown (void)
{
  Con_PageDownC (&con);
}

void
Con_Bottom (void)
{
  Con_BottomC (&con);
}

void
Con_Top (void)
{
  Con_TopC (&con);
}

/*
================
Con_ToggleConsole_f
================
*/
void
Con_ToggleConsole_f (void)
{
  // Can't toggle the console when it's the only thing available
  if ((cls.state == CA_DISCONNECTED) && (Key_GetCatcher () == KEYCATCH_CONSOLE))
    {
      return;
    }

  qce->fs.Field_Clear (&g_consoleField);
  g_consoleField.widthInChars = g_console_field_width;

  Con_ClearNotify ();
  Key_SetCatcher (Key_GetCatcher () ^ KEYCATCH_CONSOLE);
}

/*
================
Con_MessageMode_f

Talk to everyone
================
*/
void
Con_MessageMode_f (void)
{
  chat_playerNum = -1;
  chat_team = false;
  qce->fs.Field_Clear (&chatField);
  chatField.widthInChars = 30;

  Key_SetCatcher (Key_GetCatcher () ^ KEYCATCH_MESSAGE);
}

/*
================
Con_MessageMode2_f

Talk to the team
================
*/
void
Con_MessageMode2_f (void)
{
  chat_playerNum = -1;
  chat_team = true;
  qce->fs.Field_Clear (&chatField);
  chatField.widthInChars = 25;
  Key_SetCatcher (Key_GetCatcher () ^ KEYCATCH_MESSAGE);
}

/*
================
Con_Clear_f
================
*/
void
Con_Clear (console_t * con)
{
  int i;

  for (i = 0; i < CON_TEXTSIZE; i++)
    {
      // reset color to white and add spaces as text
      con->text[i] = ' ';
      con->tcolor[i] = ColorIndex (COLOR_WHITE);
    }

  Con_BottomC (con);	// go to end
}

void
Con_Clear_f (void)
{
  Con_Clear (&con);
}

/*
================
Con_Dump_f

Save the console contents out to a file
================
*/
void
Con_Dump_f (void)
{
  int l, x;
  char *line;
  fileHandle_t f;
  char buffer[1024];
  char out[MAX_QPATH];

  if (qce->cmd.Argc () != 2)
    {
      CL_Printf ("usage: condump <filename>\n");
      return;
    }

  COM_StripExtension (qce->cmd.Argv (1), out, sizeof (out));
  Q_strcat (out, sizeof (out), ".dump");

  CL_Printf ("Dumped console text to %s.\n", out);

  f = qce->fs.FOpenFileWrite (out);
  if (!f)
    {
      CL_Printf ("ERROR: couldn't open %s.\n", out);
      return;
    }

  // skip empty lines
  for (l = (con.current - con.totallines + 1); l <= con.current; l++)
    {
      line = con.text + (l % con.totallines) * con.linewidth;
      for (x = 0; x < con.linewidth; x++)
	{
	  if (line[x] != ' ')
	    break;
	}
      if (x != con.linewidth)
	break;
    }

  // write the remaining lines
  buffer[con.linewidth] = 0;

  for (/* current l position */; l <= con.current; l++)
    {
      line = con.text + (l % con.totallines) * con.linewidth;
      // copy the characters
      assert (con.linewidth < sizeof (buffer));

      Com_Memcpy (buffer, line, con.linewidth);

      for (x = con.linewidth - 1; x >= 0 ; x--)
	{
	  if (buffer[x] == ' ')
	    buffer[x] = 0;
	  else
	    break;
	}

      buffer[x] = '\n';
      qce->fs.Write (buffer, strlen (buffer), f);
    }

  qce->fs.FCloseFile (f);
}

/*
================
Con_ClearNotify
================
*/
void
Con_ClearNotifyC (console_t * con)
{
  int i;

  for (i = 0; i < MAX_NOTIFY_LIGNS; i++)
    {
      con->times[i] = 0;
    }
}

void
Con_ClearNotify (void)
{
  Con_ClearNotifyC (&con);
}

/*
================
Con_CheckResize

If the line width has changed, reformat the buffer.
================
*/
void
Con_CheckResizeC (console_t * con)
{
  int width;

  width = (SCREEN_WIDTH / SMALLCHAR_WIDTH) - 2; // (640 / 8) - 2 = 78

  if (width == con->linewidth)
    return;

  if (con->linewidth < 1)	// video hasn't been initialized yet
    {
      width = DEFAULT_CONSOLE_WIDTH;
      con->linewidth = width;
      con->totallines = CON_TEXTSIZE / con->linewidth;
      Con_Clear (con);
    }
  // atm the console has a lot of hardcoded size parameters
  // extend this using console size variable instead of SCREEN_WIDTH
  // but it'll need rework for all printing functions

  con->current = con->totallines - 1;
  con->display = con->current;
}

void
Con_CheckResize (void)
{
  Con_CheckResizeC (&con);
}

/*
==================
Cmd_CompleteDumpName
==================
*/
void
Cmd_CompleteDumpName (char *args, int argNum)
{
  if (argNum == 2)
    {
      qce->fs.Field_CompleteFilename ("", "dump", false);
    }
}

/*
================
Con_Init
================
*/
void
Con_Init (void)
{
  int i;

  con_notifytime = qce->cvar.Get ("con_notifytime", "3", CVAR_ARCHIVE);
  con_conspeed = qce->cvar.Get ("con_conspeed", "3", CVAR_ARCHIVE);
  con_backgroundcolor = qce->cvar.Get ("con_backgroundcolor",
				       "ff0000ff", CVAR_ARCHIVE);
  con_bordercolor = qce->cvar.Get ("con_bordercolor", "ffff00ff", CVAR_ARCHIVE);
  con_fraction = qce->cvar.Get ("con_fraction", "0.5", CVAR_ARCHIVE);
  con_notifylines = qce->cvar.Get ("con_notifylines", "6", CVAR_ARCHIVE);
  qce->cvar.CheckRange (con_notifylines, 0, MAX_NOTIFY_LIGNS, true);
  con_timestamps = qce->cvar.Get ("con_timestamps", "1", CVAR_ARCHIVE);

  // console field init
  qce->fs.Field_Clear (&g_consoleField);
  g_consoleField.widthInChars = g_console_field_width;
  // history field init
  for (i = 0; i < COMMAND_HISTORY; i++)
    {
      qce->fs.Field_Clear (&historyEditLines[i]);
      historyEditLines[i].widthInChars = g_console_field_width;
    }
  CL_LoadConsoleHistory ();

  qce->cmd.AddCommand ("toggleconsole", Con_ToggleConsole_f);
  qce->cmd.AddCommand ("messagemode", Con_MessageMode_f);
  qce->cmd.AddCommand ("messagemode2", Con_MessageMode2_f);
  qce->cmd.AddCommand ("clear", Con_Clear_f);
  qce->cmd.AddCommand ("condump", Con_Dump_f);
  qce->cmd.SetCommandCompletionFunc ("condump", Cmd_CompleteDumpName);

  con_backendinitialized = true;
}

/*
===============
Con_Linefeed

A new line has been validate (\n or too long text)
===============
*/
void
Con_LinefeedC (console_t * con, bool skipnotify)
{
  int i, index;

  // mark time for transparent overlay
  if (con->current >= 0)
    {
      if (skipnotify)
	con->times[con->current % MAX_NOTIFY_LIGNS] = 0;
      else
	con->times[con->current % MAX_NOTIFY_LIGNS] = cls.realtime;
    }

  con->x = 0;
  // autoscroll when on bottom of the console
  if (con->display == con->current)
    {
      con->display++;
    }
  con->current++;

  for (i = 0; i < con->linewidth; i++)
    {
      // initizalize/reset the next line
      index = (con->current % con->totallines) * con->linewidth + i;
      con->text[index] = ' ';
      con->tcolor[index] = ColorIndex (COLOR_WHITE);
    }
}

void
Con_Linefeed (bool skipnotify)
{
  Con_LinefeedC (&con, skipnotify);
}

/*
==================
CL_ConsoleAddCharC

Characters should only be added to console
through this helper function
==================
*/
static void
CL_ConsoleAddCharC (console_t *con, char carac, char color)
{
  int y;

  y = con->current % con->totallines;
  con->text[y * con->linewidth + con->x] = carac;
  con->tcolor[y * con->linewidth + con->x] = color;
  con->x++;
}

/*
================
CL_ConsolePrint

Handles cursor positioning, line wrapping, etc
All console printing must go through this in order to be logged to disk
If no console is visible, the text will appear at the top of the game window
================
*/
#define SKIPNOTIFY_STRING "[skipnotify]"
void
CL_ConsolePrintC (console_t * con, char *txt)
{
  int y;
  int c;
  int color;
  bool skipnotify = false;	// NERVE - SMF

  // TTimo - prefix for text that shows up in console but not in notify
  // backported from RTCW
  unsigned int skipsize = strlen (SKIPNOTIFY_STRING);

  if (!Q_strncmp (txt, SKIPNOTIFY_STRING, skipsize))
    {
      skipnotify = true;
      txt += skipsize;
    }

  // for some demos we don't want to ever show anything on the console
  if (cl_noprint && cl_noprint->integer)
    {
      return;
    }

  if (!con->initialized)
    {
      con->color[0] = con->color[1] = con->color[2] = con->color[3] = 1.0f;
      con->linewidth = -1;
      Con_CheckResizeC (con);
      con->initialized = true;
    }

  color = ColorIndex (COLOR_WHITE);

  while ((c = *txt) != 0)
    {
      if (Q_IsColorString (txt))
	{
	  color = ColorIndex2 (*(txt + 1));
	  txt += 2;
	  continue;
	}

      txt++;

      switch (c)
	{
	case '\n':
	  Con_LinefeedC (con, skipnotify);
	  break;
	case '\r':
	  con->x = 0;
	  break;
	default:	// display character and advance
	  {
	    // begin the new line, add timestamps
	    if ((con->x == 0)
		&& (con_backendinitialized)
		&& (con_timestamps->integer))
	      {
		qtime_t now;
		int diz, unit;

		if (cls.qcomLoaded)
		  qce->Com_RealTime (&now);
		else
		  Com_Memset (&now, 0, sizeof (now));

		CL_ConsoleAddCharC (con, '[', ColorIndex2 ('o'));

		diz = now.tm_hour / 10;
		unit = now.tm_hour - (diz * 10);
		CL_ConsoleAddCharC (con, Com_IntNumberToChar (diz),
				    ColorIndex (COLOR_YELLOW));
		CL_ConsoleAddCharC (con, Com_IntNumberToChar (unit),
				    ColorIndex (COLOR_YELLOW));

		CL_ConsoleAddCharC (con, ':', ColorIndex (COLOR_WHITE));

		diz = now.tm_min / 10;
		unit = now.tm_min - (diz * 10);
		CL_ConsoleAddCharC (con, Com_IntNumberToChar (diz),
				    ColorIndex2 (COLOR_YELLOW));
		CL_ConsoleAddCharC (con, Com_IntNumberToChar (unit),
				    ColorIndex2 (COLOR_YELLOW));

		CL_ConsoleAddCharC (con, ']', ColorIndex2 ('o'));
	      }

	    CL_ConsoleAddCharC (con, c, color);

	    // looking one char ahead to add some "line cut" symbol at end of line
	    if ((con->x + 1) >= con->linewidth)
	      {
		CL_ConsoleAddCharC (con, '*', ColorIndex (COLOR_GREEN));
		Con_LinefeedC (con, skipnotify);
	      }
	  }
	  break;
	}
    }
}

void
CL_ConsolePrint (char *txt)
{
  CL_ConsolePrintC (&con, txt);
}

/*
==============================================================================

DRAWING

==============================================================================
*/

/*
================
Con_DrawInput

Draw the editline after a ] prompt
================
*/
#define INTERLINE_SPACE 0
void
Con_DrawInputFC (field_t * field, console_t * console)
{
  int y;

  if (cls.state != CA_DISCONNECTED && !(Key_GetCatcher () & KEYCATCH_CONSOLE))
    {
      return;
    }

  if (cls.useLegacyConsoleFont)
    y = console->vislines - (SCR_ConsoleFontCharHeight () * 2.0f) + INTERLINE_SPACE;
  else
    y = console->vislines - (SCR_ConsoleFontCharHeight ()) + INTERLINE_SPACE;

  SCR_GlyphColor (console->color);

  // setup some small offset, a whitespace long
  SCR_DrawConsoleFontChar (console->xadjust + SCR_ConsoleFontCharWidth (" "),
			   y, "]");

  Field_Draw (field, console->xadjust + 2 * SCR_ConsoleFontCharWidth (" "), y,
	      SCREEN_WIDTH - 3 * SCR_ConsoleFontCharWidth (" "), true, false);
}

void
Con_DrawInput (void)
{
  Con_DrawInputFC (&g_consoleField, &con);
}

/*
================
Con_DrawNotify

Draws the last few lines of output transparently over the game top
================
*/
void
Con_DrawNotifyFC (field_t * field, console_t * con)
{
  int x;
  float v;
  char *text;
  char *tcolor;
  int i;
  int time;
  int skip;
  int currentColor;
  float offset;
  int len;

  v = SCR_ConsoleFontCharHeight ();

  currentColor = -1;
  for (i = con->current - con_notifylines->integer; i <= con->current; i++)
    {
      if (con_notifytime->integer <= 0)
	{
	  break;
	}
      if (i < 0)
	continue;

      time = con->times[i % MAX_NOTIFY_LIGNS];
      // nothing to notify here
      if (time == 0)
	continue;

      time = cls.realtime - time;
      // is line too old ?
      if (time > (con_notifytime->value * 1000))
	continue;

      text = con->text + (i % con->totallines) * con->linewidth;
      tcolor = con->tcolor + (i % con->totallines) * con->linewidth;

      x = 0;
      offset = (cl_conXOffset->integer + con->xadjust);

      while (x < con->linewidth)
	{
	  if (text[x] == ' ')
	    {
	      offset += SCR_ConsoleFontCharWidth (&text[x]);
	      x++;
	      continue;
	    }

	  if (tcolor[x] != currentColor)
	    {
	      currentColor = tcolor[x];
	      SCR_GlyphColor (g_color_table[currentColor]);
	    }
	  /*
	  SCR_DrawSmallChar (cl_conXOffset->integer + con->xadjust +
			     (x + 1) * SMALLCHAR_WIDTH, (int)v, &text[x]);
	  */

	  SCR_DrawConsoleFontChar (offset, v, &text[x]);

	  len = Q_UTF8Width (&text[x]);
	  if (len < 1)
	    {
	      CL_Error (ERR_FATAL, "LEN < 1 - this shouldn't happend\n");
	    }

	  offset += SCR_ConsoleFontCharWidth (&text[x]);
	  x += len;
	}

      v += SCR_ConsoleFontCharHeight ();
    }

  // some more offset
  v += SCR_ConsoleFontCharHeight () * 0.5f;

  // draw the chat line
  if (Key_GetCatcher () & KEYCATCH_MESSAGE)
    {
      SCR_GlyphColor (g_color_table[ColorIndex (COLOR_WHITE)]);

      offset = (cl_conXOffset->integer + con->xadjust);

      if (chat_team)
	{
	  SCR_DrawSmallStringExt (8, v, "say_team:", NULL,
				  false, false);
	  skip = 10;
	}
      else
	{
	  SCR_DrawSmallStringExt (8, v, "say:", NULL,
				  false, false);
	  skip = 5;
	}

      Field_BigDraw (field, skip * SCR_ConsoleFontCharWidth (" ") + offset, v,
		     SCREEN_WIDTH - (skip + 2) * SCR_ConsoleFontCharWidth (" "),
		     true, true);

      v += SCR_ConsoleFontCharHeight ();
    }
}

void
Con_DrawNotify (void)
{
  Con_DrawNotifyFC (&chatField, &con);
}

/*
================
Con_DrawSolidConsole

Draws the console with the solid background
================
*/
void
Con_DrawSolidConsoleC (console_t * con, float frac)
{
  int i, x;
  int rows;
  char *text;
  char *tcolor;
  int row;
  int lines;
  int currentColor;
  vec4_t color;
  qtime_t now;
  char *nowString;
  float totalWidth, currentWidthLocation;
  int index;
  float y;
  float offset;

  if (cls.useLegacyConsoleFont)
    lines = SCREEN_HEIGHT * frac;
  else
    lines = cls.glconfig.vidHeight * frac;

  if (lines <= 0)
    return;

  // shouldn't happend, except with buggy developers
  if (cls.useLegacyConsoleFont)
    {
      if (lines > SCREEN_HEIGHT)
	lines = SCREEN_HEIGHT;
    }
  else
    {
      if (lines > cls.glconfig.vidHeight)
	lines = cls.glconfig.vidHeight;
    }

  // on wide screens, we will center the text ?
  con->xadjust = 0;

  // draw the background
  y = frac * (float)SCREEN_HEIGHT;

  if (y < 1.0f)
    {
      y = 0.0f;
    }
  else
    {
      // convert an hex color string to a vec4
      dev_Tools_ColorFromStringHex (color, con_backgroundcolor->string);
      re.SetColor (color);

      SCR_DrawPic (0, 0, SCREEN_WIDTH, y, cls.consoleShader);
    }

  // draw the border
  dev_Tools_ColorFromStringHex (color, con_bordercolor->string);
  re.SetColor (color);

  SCR_FillRect (0, y, SCREEN_WIDTH, 2, color);

  // draw the version number
  SCR_GlyphColor (g_color_table[ColorIndex2 ('e')]);

  i = strlen (Q3_VERSION);
  totalWidth = SCR_ConsoleFontStringWidth (Q3_VERSION, i);
  if (cls.useLegacyConsoleFont)
    offset = SCREEN_WIDTH;
  else
    offset = cls.glconfig.vidWidth;

  currentWidthLocation = 0;
  for (x = 0; x < i; x++)
    {
      index = (totalWidth - currentWidthLocation + 1);
      SCR_DrawConsoleFontChar (offset - index,
			       (lines - SCR_ConsoleFontCharHeight ()),
			       &Q3_VERSION[x]);
      currentWidthLocation += SCR_ConsoleFontCharWidth (&Q3_VERSION[x]);
    }

  // draw the client local time
  qce->Com_RealTime (&now);

  nowString = va ("%02d:%02d:%02d", now.tm_hour, now.tm_min, now.tm_sec);
  SCR_GlyphColor (g_color_table[ColorIndex2 ('s')]);

  i = strlen (nowString);
  totalWidth = SCR_ConsoleFontStringWidth (nowString, i);
  if (cls.useLegacyConsoleFont)
    offset = SCREEN_WIDTH;
  else
    offset = cls.glconfig.vidWidth;

  currentWidthLocation = 0;
  for (x = 0; x < i; x++)
    {
      index = (totalWidth - currentWidthLocation + 1);
      SCR_DrawConsoleFontChar (offset - index,
			       (lines - (SCR_ConsoleFontCharHeight () * 2.0f)),
			       &nowString[x]);
      currentWidthLocation += SCR_ConsoleFontCharWidth (&nowString[x]);
    }

  // draw the text
  con->vislines = lines;

  // rows of text to draw
  // adding a line offset so that we don't do offscreen printing
  // the rows calculating process should be improved

  // offset before the logged text is displayed
  if (cls.useLegacyConsoleFont)
    y = lines - (SCR_ConsoleFontCharHeight () * 3.5f);
  else
    y = lines - (SCR_ConsoleFontCharHeight () * 2.5f);

  // adding 4 pixels upper border
  rows = (int)((y - 4.0f) / (SCR_ConsoleFontCharHeight ()));

  // draw from the bottom up, out of any regular line calculation
  if (con->display != con->current)
    {
      // draw arrows to show the buffer is backscrolled
      SCR_GlyphColor (g_color_table[ColorIndex (COLOR_RED)]);

      for (x = 0; x < con->linewidth; x += 4)
	{
	  if (cls.useLegacyConsoleFont)
	    SCR_DrawConsoleFontChar (con->xadjust + (x + 1) * SMALLCHAR_WIDTH,
				     lines - SCR_ConsoleFontCharHeight (), "^");
	  else
	    SCR_DrawConsoleFontChar (con->xadjust + (x + 1) * SMALLCHAR_WIDTH,
				     lines, "^");
	}
    }

  row = con->display;

  // empty line
  if (con->x == 0)
    {
      row--;
    }

  currentColor = 7;
  SCR_GlyphColor (g_color_table[currentColor]);

  // drawing all lines
  for (i = 0; i < rows; i++, y -= SCR_ConsoleFontCharHeight (), row--)
    {
      // small offset
      currentWidthLocation = SCR_ConsoleFontCharWidth (" ");

      if (row < 0)
	{
	  break;
	}
      if ((con->current - row) >= con->totallines)
	{
	  // past scrollback wrap point
	  continue;
	}

      // drawing a line, drawing all the characters
      // NOTE : using GlyphLine one could avoid a lot of ogl calls
      // but this needs some work to keep a proper colorizing
      // I would suggest making ogl calllists to colorize
      // so that all the line + colors can be issued in a single
      // callLists

      text = con->text + (row % con->totallines) * con->linewidth;
      tcolor = con->tcolor + (row % con->totallines) * con->linewidth;

      x = 0;
      while (x < con->linewidth)
	{
	  // we could tcolor[x] & MAX_DEFINED_COLORS
	  if (tcolor[x] != currentColor)
	    {
	      currentColor = tcolor[x];
	      SCR_GlyphColor (g_color_table[currentColor % (MAX_DEFINED_COLORS)]);
	    }

	  currentWidthLocation += SCR_DrawConsoleFontChar (con->xadjust + currentWidthLocation,
							   y, &text[x]);
	  x += Q_UTF8Width (&text[x]);
	}

      /*
      // drawing a line, drawing all the characters
      for (x = 0; x < con->linewidth; x++)
	{
	  if (tcolor[x] != currentColor)
	    {
	      currentColor = tcolor[x];
	      re.SetColor (g_color_table[currentColor % (MAX_DEFINED_COLORS)]);
	    }

	  SCR_DrawConsoleFontChar (con->xadjust + currentWidthLocation,
				   y, &text[x]);
	  currentWidthLocation += SCR_ConsoleFontCharWidth (&text[x]);
	}
      */
    }

  // draw the input prompt, user text, and cursor if desired
  Con_DrawInput ();

  SCR_GlyphColor (NULL);
}

void
Con_DrawSolidConsole (float frac)
{
  Con_DrawSolidConsoleC (&con, frac);
}

/*
==================
Con_DrawConsole
==================
*/
void
Con_DrawConsoleC (console_t * con)
{
  // check for console width changes from a vid mode change
  Con_CheckResizeC (con);

  // if disconnected, render console full screen
  if ((cls.state == CA_DISCONNECTED) || (cls.state == CA_UNINITIALIZED)
      || (cls.state == CA_CHALLENGING) || (cls.state == CA_CONNECTING)
      || (cls.state == CA_CONNECTED))
    {
      // next line : allows key repeat & mouse when loading the game without args
      Key_SetCatcher (Key_GetCatcher () | KEYCATCH_CONSOLE);
      Con_DrawSolidConsoleC (con, 1.0f);
      return;
    }

  if (con->displayFrac)
    {
      Con_DrawSolidConsoleC (con, con->displayFrac);
    }
  else
    {
      // draw notify lines if game is active
      if (cls.state == CA_ACTIVE)
	{
	  Con_DrawNotifyFC (&chatField, con); // change this
	}
    }
}

void
Con_DrawConsole (void)
{
  Con_DrawConsoleC (&con);
}

//================================================================

/*
==================
Con_RunConsole

Scroll it up or down
==================
*/
void
Con_RunConsoleC (console_t * con)
{
  // decide on the destination height of the console
  if (Key_GetCatcher () & KEYCATCH_CONSOLE)
    {
      // check cvar con_fraction boundaries
      // if not valid, set it to half size,
      // so a wrong setup wont hide the console
      if (con_fraction->value > 1.0f)
	{
	  qce->cvar.SetNew (con_fraction, "0.5");
	}
      // be sure to always have a bit of the console available
      else if (con_fraction->value < 0.1f)
	{
	  qce->cvar.SetNew (con_fraction, "0.1");
	}

      con->finalFrac = con_fraction->value;
    }
  else
    con->finalFrac = 0;		// none visible

  // shortcut
  if (con_conspeed->integer == -1)
    {
      con->displayFrac = con->finalFrac;
      return;
    }

  // scroll towards the destination height
  if (con->finalFrac < con->displayFrac)
    {
      con->displayFrac -= con_conspeed->value * cls.realFrametime * 0.001;
      if (con->finalFrac > con->displayFrac)
	con->displayFrac = con->finalFrac;
    }
  else if (con->finalFrac > con->displayFrac)
    {
      con->displayFrac += con_conspeed->value * cls.realFrametime * 0.001;
      if (con->finalFrac < con->displayFrac)
	con->displayFrac = con->finalFrac;
    }
}

void
Con_RunConsole (void)
{
  Con_RunConsoleC (&con);
}

/*
==================
Con_Close
==================
*/
void
Con_CloseC (console_t * con)
{
  if (!cl_running->integer)
    {
      return;
    }
  qce->fs.Field_Clear (&g_consoleField);
  Con_ClearNotifyC (con);
  Key_SetCatcher (Key_GetCatcher () & ~KEYCATCH_CONSOLE);
  con->finalFrac = 0;		// none visible
  con->displayFrac = 0;
}

void
Con_Close (void)
{
  Con_CloseC (&con);
}

/*
==============
CG_DrawColorList_f
==============
*/
void
CL_DrawColorList_f (void)
{
  char string[MAX_STRING_CHARS];
  int i;
  char c;
  memset (string, 0, sizeof (string));

  for (i = 0; i < MAX_DEFINED_COLORS; i++)
    {
      if (i < 10)
	c = (char)'0' + i;
      else if (i < 36 && i >= 10)
	c = (char)'a' + (i - 10);
      else
	c = (char)'A' + (i - 36);
      Q_strcat (string, sizeof (string), va("^%c%c ", c, c));
    }

  Q_strcat (string, sizeof (string), "\n"); // Q_strcat adds the ending \0
  CL_Printf ("%s", string);
}

/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// cl_scrn.c -- master for refresh, status bar, console, chat, notify, etc

#include "client.h"

/*
================
SCR_AdjustFrom640

Adjusted for resolution and screen aspect ratio
================
*/
void
SCR_AdjustFrom640 (float *x, float *y, float *w, float *h)
{
  float xscale;
  float yscale;

  // scale for screen sizes
  xscale = cls.glconfig.vidWidth / 640.0;
  yscale = cls.glconfig.vidHeight / 480.0;
  if (x != NULL)
    {
      *x *= xscale;
    }
  if (y != NULL)
    {
      *y *= yscale;
    }
  if (w != NULL)
    {
      *w *= xscale;
    }
  if (h != NULL)
    {
      *h *= yscale;
    }
}

/*
================
SCR_FillRect

Coordinates are 640*480 virtual values
=================
*/
void
SCR_FillRect (float x, float y, float width, float height, const float *color)
{
  re.SetColor (color);

  SCR_AdjustFrom640 (&x, &y, &width, &height);
  re.DrawStretchPic (x, y, width, height, 0, 0, 0, 0, cls.whiteShader);

  re.SetColor (NULL);
}

/*
================
SCR_DrawPic

Coordinates are 640*480 virtual values
=================
*/
void
SCR_DrawPic (float x, float y, float width, float height, qhandle_t hShader)
{
  SCR_AdjustFrom640 (&x, &y, &width, &height);
  re.DrawStretchPic (x, y, width, height, 0, 0, 1, 1, hShader);
}

/*
===============
SCR_GlyphColor

Use this one to colorize fonts
it will choose the appropriate function
===============
*/
void
SCR_GlyphColor (const float *rgba)
{
  if (cls.useLegacyConsoleFont)
    re.SetColor (rgba);
  else
    re.SetFontColor (rgba);
}

/*
==============
SCR_DrawGlyph
==============
*/
static int
SCR_DrawGlyph (fontFace_t *face, wchar_t val, int baseline, int y)
{
  return re.DrawGlyph (face, val, baseline, y);
}

/*
=================
SCR_DrawChar

chars are drawn at 640*480 virtual screen size
=================
*/
void
SCR_DrawChar (int x, int y, float sizex, float sizey, int ch)
{
  int row, col;
  float frow, fcol;
  float ax, ay, aw, ah;
  float size;

  ch &= 255;

  if (ch == ' ')
    {
      return;
    }

  // if the char will not appear on screen
  if (y < -sizey)
    {
      return;
    }

  ax = x;
  ay = y;
  aw = sizex;
  ah = sizey;
  SCR_AdjustFrom640 (&ax, &ay, &aw, &ah);

  row = ch >> 4;
  col = ch & 15;

  frow = row * 0.0625; // 0.0625 = 8/128
  fcol = col * 0.0625;
  size = 0.0625;

  re.DrawStretchPic (ax, ay, aw, ah, fcol, frow,
		     fcol + size, frow + size, cls.charSetShader);
}

/*
=======================
SCR_DrawConsoleFontChar
=======================
*/
int
SCR_DrawConsoleFontChar (float x, float y, const char *s)
{
  int advance;

  if (cls.useLegacyConsoleFont)
    {
      SCR_DrawChar ((int)x, (int)y, SMALLCHAR_WIDTH, SMALLCHAR_HEIGHT, Q_UTF8CodePoint (s));
      advance = SMALLCHAR_WIDTH;
    }
  else
    {
      advance = SCR_DrawGlyph (&cls.consoleFace, Q_UTF8CodePoint (s), (int)x, (int)y);
    }

  return advance;
}

/*
========================
SCR_ConsoleFontCharWidth

REMOVE ME
========================
*/
float
SCR_ConsoleFontCharWidth (const char *s)
{
  float width;

  if (cls.useLegacyConsoleFont)
    {
      width = SMALLCHAR_WIDTH;
    }
  else
    {
      width = cls.consoleFace.bbox_width * (1 + cl_consoleFontKerning->value);
    }

  return width;
}

/*
=========================
SCR_ConsoleFontCharHeight

REMOVE ME
=========================
*/
float
SCR_ConsoleFontCharHeight ()
{
  float height;

  if (cls.useLegacyConsoleFont)
    {
      height = SMALLCHAR_HEIGHT;
    }
  else
    {
      height = cls.consoleFace.bbox_height * 2.0f;
    }

  return height;
}

/*
==========================
SCR_ConsoleFontStringWidth

REMOVE ME
==========================
*/
float
SCR_ConsoleFontStringWidth (const char *s, int len)
{
  float width;
  int offset;

  if (cls.useLegacyConsoleFont)
    {
      width = len * SMALLCHAR_WIDTH;
    }
  else
    {
      width = 0;

      while (*s && (len > 0))
	{
	  width += SCR_ConsoleFontCharWidth (s);

	  offset = Q_UTF8Width (s);
	  s += offset;
	  len -= offset;
	}
    }

  return width;
}

/*
==================
SCR_DrawBigString[Color]

Draws a multi-colored string with a drop shadow, optionally forcing
to a fixed color.

Coordinates are at 640 by 480 virtual resolution
==================
*/
void
SCR_DrawStringExt (int x, int y, float sizex, float sizey,
		   const char *string, float *setColor,
		   bool forceColor, bool allowColor)
{
  vec4_t color;
  const char *s;
  int xx;

  // draw the drop shadow
  color[0] = color[1] = color[2] = 0;
  color[3] = setColor[3];
  re.SetColor (color);

  s = string;
  xx = x;
  while (*s)
    {
      if (allowColor && Q_IsColorString (s))
	{
	  s += 2;
	  continue;
	}

      SCR_DrawChar (xx + 2, y + 2, sizex, sizey, Q_UTF8CodePoint (s));
      xx += sizex;
      s += Q_UTF8Width (s);
    }

  // draw the colored text
  re.SetColor (setColor);

  s = string;
  xx = x;
  while (*s)
    {
      if (allowColor && Q_IsColorString (s))
	{
	  if (!forceColor)
	    {
	      Com_Memcpy (color, g_color_table[ColorIndex2 (*(s + 1))],
			  sizeof (color));
	      color[3] = setColor[3];
	      re.SetColor (color);
	    }

	  s += 2;
	  continue;
	}

      SCR_DrawChar (xx, y, sizex, sizey, Q_UTF8CodePoint (s));
      xx += sizex;
      s += Q_UTF8Width (s);
    }

  re.SetColor (NULL);
}

/*
==================
SCR_DrawBigString
==================
*/
void
SCR_DrawBigString (int x, int y, const char *s, float alpha,
		   bool allowColor)
{
  float color[4];

  color[0] = color[1] = color[2] = 1.0;
  color[3] = alpha;
  SCR_DrawStringExt (x, y, 8, 16, s, color, false, allowColor);
}

/*
=======================
SCR_DrawBigStringColor
=======================
*/
void
SCR_DrawBigStringColor (int x, int y, const char *s, vec4_t color,
			bool allowColor)
{
  SCR_DrawStringExt (x, y, BIGCHAR_WIDTH, BIGCHAR_HEIGHT, s, color, true, allowColor);
}

/*
==================
SCR_DrawSmallString[Color]

Draws a multi-colored string with a drop shadow, optionally forcing
to a fixed color.
==================
*/
void
SCR_DrawSmallStringExt (int x, int y, const char *string, float *setColor,
			bool forceColor, bool allowColor)
{
  vec4_t color;
  const char *s;
  int xx;
  float xxf;

  // draw the drop shadow
  /*
  color[0] = color[1] = color[2] = 0;
  color[3] = setColor[3];
  re.SetColor (color);
  s = string;
  xx = x;
  xxf = (float)xx;

  while (*s)
    {
      if (allowColor && Q_IsColorString (s))
	{
	  s += 2;
	  continue;
	}
      SCR_DrawConsoleFontChar (xxf + 2, y + 2, s);
      xxf += SCR_ConsoleFontCharWidth (s);
      //s++;
      s += Q_UTF8Width (s);
    }
  */

  // draw the colored text
  s = string;
  xx = x;
  xxf = (float)xx;

  SCR_GlyphColor (setColor);
  while (*s)
    {
      if (Q_IsColorString (s))
	{
	  if (!forceColor)
	    {
	      Com_Memcpy (color, g_color_table[ColorIndex2 (*(s + 1))],
			  sizeof (color));
	      color[3] = setColor[3];
	      SCR_GlyphColor (color);
	    }
	  if (allowColor)
	    {
	      s += 2;
	      continue;
	    }
	}

      xxf += SCR_DrawConsoleFontChar (xxf, y, s);
      s += Q_UTF8Width (s);
    }
  SCR_GlyphColor (NULL);
}

/*
=================
SCR_Strlen

skips color escape codes
=================
*/
static int
SCR_Strlen (const char *str)
{
  const char *s = str;
  int count = 0;

  while (*s)
    {
      if (Q_IsColorString (s))
	{
	  s += 2;
	}
      else
	{
	  count++;
	  s++;
	}
    }

  return count;
}

/*
=================
SCR_GetBigStringWidth
=================
*/
int
SCR_GetBigStringWidth (const char *str)
{
  return SCR_Strlen (str) * 16;
}

//===============================================================================

/*
=================
SCR_DrawDemoRecording
=================
*/
void
SCR_DrawDemoRecording (void)
{
  char string[1024];
  int pos;

  if (!clc.demorecording)
    {
      return;
    }

  pos = qce->fs.FTell (clc.demofile);
  sprintf (string, "RECORDING %s: %ik", clc.demoName, pos / 1024);

  SCR_DrawStringExt (320 - strlen (string) * 4, 20, 8, 8, string,
		     g_color_table[7], true, false);
}

//=============================================================================

/*
==================
SCR_Init
==================
*/
void
SCR_Init (void)
{
  // nothing special to do
}

//=======================================================

/*
==================
SCR_DrawScreenField
==================
*/
void
SCR_DrawScreenField (void)
{
  static int lastdraw = 0;
  static connstate_t lasttype = 0;

  re.BeginFrame ();

  // clear the screen unless game renderings is active
  // avoids crappy effects in the text console when cgame isn't loaded
  if (cls.state != CA_ACTIVE)
    {
      re.SetColor (g_color_table[0]);
      re.DrawStretchPic (0, 0, cls.glconfig.vidWidth,
			 cls.glconfig.vidHeight, 0, 0, 0, 0,
			 cls.whiteShader);
      re.SetColor (NULL);
    }

  switch (cls.state)
    {
    default:
      CL_Error (ERR_FATAL, "SCR_DrawScreenField: bad cls.state");
      break;
      /*
    case CA_CINEMATIC:
      //SCR_DrawCinematic();
      break;
      */
    case CA_DISCONNECTED:
      // force menu up
      if (cls.soundStarted)
	snde->StopAllSounds ();
      //VM_Call( uivm, UI_SET_ACTIVE_MENU, UIMENU_MAIN );
      break;
    case CA_CONNECTING:
      if ((cls.realtime - lastdraw) > 1000
	  || lasttype != CA_CONNECTING)
	{
	  CL_Printf ("Connecting\n");
	  lastdraw = cls.realtime;
	  lasttype = CA_CONNECTING;
	}
      break;
    case CA_CHALLENGING:
      if ((cls.realtime - lastdraw) > 1000
	  || lasttype != CA_CHALLENGING)
	{
	  CL_Printf ("Awaiting Challenge\n");
	  lastdraw = cls.realtime;
	  lasttype = CA_CHALLENGING;
	}
      break;
    case CA_CONNECTED:
      if ((cls.realtime - lastdraw) > 1000
	  || lasttype != CA_CONNECTED)
	{
	  CL_Printf ("Connected\n");
	  lastdraw = cls.realtime;
	  lasttype = CA_CONNECTED;
	}
      break;
      // connecting clients will only show the connection dialog
      // refresh to update the time
    case CA_LOADING:
    case CA_PRIMED:
      // draw the game information screen and loading progress
      CL_CGameProcessing ();
      CL_CGameRendering ();

      // also draw the connection information, so it doesn't
      // flash away too briefly on local or lan games
      break;
    case CA_ACTIVE:
      {
	CL_CGameProcessing ();
	CL_CGameRendering ();

	if (cl_drawDemoRecording->integer)
	  SCR_DrawDemoRecording (); // not the best place to put this...

	CL_PauseDemoIcons ();
      }
      break;
    }

  // console draws next
  Con_DrawConsole ();
}

/*
==================
SCR_UpdateScreen

This is called every frame, and can also be called explicitly to flush
text to the screen.
==================
*/
int scr_recursive;

void
SCR_UpdateScreen (void)
{
  //static int recursive = 0;

  if (!cls.rendererStarted)
    {
      return;		// not initialized yet
    }

  scr_recursive++;

  if (scr_recursive >= 2)
    {
      CL_Error (ERR_FATAL, "SCR_UpdateScreen: recursively called");
    }

  // If there is no VM, there are also no rendering commands issued. Stop the renderer in
  // that case.
  SCR_DrawScreenField ();

  if (cl_speeds->integer)
    {
      re.EndFrame (&time_frontend, &time_backend);
    }
  else
    {
      re.EndFrame (NULL, NULL);
    }
  scr_recursive--;
}

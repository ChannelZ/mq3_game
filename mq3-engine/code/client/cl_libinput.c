/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "client.h"

// globals
inimport_t inpi;
inexport_t *inpe;
Dlib_t inlib;

static void
CL_FillInputImportLib (inimport_t * ii)
{
  ii->Printf = CL_Printf;
  ii->Error = CL_Error;
  ii->DPrintf = CL_Printf;
  ii->Milliseconds = Sys_Milliseconds;
  ii->Cmd_Argc = qce->cmd.Argc;
  ii->Cmd_Argv = qce->cmd.Argv;
  ii->Cmd_AddCommand = qce->cmd.AddCommand;
  ii->Cmd_RemoveCommand = qce->cmd.RemoveCommand;
  ii->Cvar_Get = qce->cvar.Get;
  ii->Cvar_Set = qce->cvar.Set;
  ii->Hunk_AllocateTempMemory = qce->mem.Hunk_AllocateTempMemory;
  ii->Hunk_FreeTempMemory = qce->mem.Hunk_FreeTempMemory;
  ii->FS_FOpenFileByMode = qce->fs.FOpenFileByMode;
  ii->FS_Read = qce->fs.Read2;
  ii->FS_ReadFile = qce->fs.ReadFile;
  ii->FS_Write = qce->fs.Write;
  ii->FS_FCloseFile = qce->fs.FCloseFile;
  ii->FS_Seek = qce->fs.Seek;
  ii->FS_FTell = qce->fs.FTell;
  ii->Z_Malloc = qce->mem.Z_Malloc;
  ii->Z_Free = qce->mem.Z_Free;

  ii->Key_KeynumToString = Key_KeynumToString;
  ii->Key_StringToKeynum = Key_StringToKeynum;
  ii->QueueEvent = qce->event.QueueEvent;
  ii->Key_GetCatcher = Key_GetCatcher;
  ii->COM_Parse = COM_Parse;
  ii->Com_HexStrToInt = Com_HexStrToInt;
}

void
CL_InitInputLib (void)
{
  inexport_t *(*InputAPI) (inimport_t *);
  void *Hdl;

  // free the Dlib struct
  Com_Memset (&inlib, 0, sizeof (inlib));

  // load the dynamic library
  inlib.dynHandle = Sys_DLLoadType ("libq3input", inlib.fqpath, CLIENT);

  if (inlib.dynHandle == NULL)
    {
      CL_Error (ERR_FATAL, "Can't load input plugin\n");
    }

  // the library is loaded, resolve
  Hdl = inlib.dynHandle;

  InputAPI = (inexport_t *(*)(inimport_t *)) Sys_DLLoadFunction (Hdl, "GetInputLibAPI", true);

  // the input lib has properly been loaded and we found the function to fill the api
  cls.inputlibLoaded = true;

  // inputAPI is valid here
  CL_FillInputImportLib (&inpi);

  inpe = InputAPI (&inpi);
}

void
CL_UnloadInputLib (void)
{
  if (!cls.inputlibLoaded)
    return;

  if (inlib.dynHandle == NULL)
    return;
  /*
  if ((inpe == NULL) || (inpe->Shutdown == NULL))
    return;
  */
  CL_Printf ("Closing Input Lib\n");
  //inpe->Shutdown ();
  Sys_DLUnload (inlib.dynHandle);
  // by security but shouldn't be used
  inpe = NULL;
  Com_Memset (&inpi, 0, sizeof (inpi));

  // only set to false here
  cls.inputlibLoaded = false;
}


void
CL_ShutdownInput (void)
{
  if (!cls.inputStarted)
    return;

  inpe->Shutdown ();
  cls.inputStarted = false;
}

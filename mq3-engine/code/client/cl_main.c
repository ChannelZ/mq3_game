/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// cl_main.c  -- client main loop

#include "client.h"
#include <limits.h>

cvar_t *cl_nodelta;

cvar_t *cl_noprint;

cvar_t *rcon_client_password;
cvar_t *rconAddress;

cvar_t *cl_timeout;
cvar_t *cl_maxpackets;
cvar_t *cl_packetdup;
cvar_t *cl_timeNudge;
cvar_t *cl_showTimeDelta;
cvar_t *cl_showSimTimeDelta;
cvar_t *cl_simTimeSmooth;

cvar_t *cl_shownet;
cvar_t *cl_showSend;
cvar_t *cl_timedemo;
cvar_t *cl_timedemoLog;
cvar_t *cl_autoRecordDemo;
cvar_t *cl_drawDemoRecording;

cvar_t *cl_avidemo;
cvar_t *cl_aviFrameRate;
cvar_t *cl_forceavidemo;
cvar_t *cl_demoShowSequence;
cvar_t *cl_demoConfigFile;

cvar_t *cl_freelook;
cvar_t *cl_sensitivity;

cvar_t *cl_mouseAccel;
cvar_t *cl_showMouseRate;
cvar_t *cl_mouseAccelOffsetX;
cvar_t *cl_mouseAccelOffsetY;
cvar_t *cl_mouseAccelStyle;

cvar_t *m_pitch;
cvar_t *m_yaw;
cvar_t *m_forward;
cvar_t *m_side;
cvar_t *m_filter;

cvar_t *cl_activeAction;

cvar_t *cl_conXOffset;
cvar_t *cl_consoleFont;
cvar_t *cl_consoleFontSize;
cvar_t *cl_consoleFontKerning;

cvar_t *cl_serverStatusResendTime;

cvar_t *cl_maxPing;

cvar_t *cl_lanForcePackets;

//cvar_t *cl_guidServerUniq;

cvar_t *cl_buildscript;
cvar_t *cl_timescale;

cvar_t *cl_running;
cvar_t *cl_developer;

cvar_t *cl_speeds;
cvar_t *cl_dropsim;
cvar_t *cl_fixedtime;
cvar_t *cl_maxfps;

cvar_t *cl_waste_cputime;

// tunning internet connection
cvar_t *dev_nc_wrapnextpackettime;
cvar_t *dev_nc_wraptimedelta;
cvar_t *dev_nc_addtimedelta;
cvar_t *dev_nc_window_max;
cvar_t *dev_nc_window_min;

clientActive_t cl;
clientConnection_t clc;
clientStatic_t cls;

// RENDERER
// dynamic librarie
Dlib_t rdlib;
// Structure containing functions exported from refresh DLL
refexport_t re;

// QCOMMON
Dlib_t qclib;
qcom_export_t * qce;
qcom_import_t qimp;

bool cl_errorEntered;	// an error happend, don't recurse
char cl_errorMessage[MAXPRINTMSG];

// if there's an error during a frame, jump the rest of it
jmp_buf abortframe;

int cl_frameTime;

// add me in CL_Frame
/*static */int time_frontend;		// renderer frontend time
/*static */int time_backend;		// renderer backend time


/*
=======================================================================

CLIENT RELIABLE COMMAND COMMUNICATION

=======================================================================
*/

/*
======================
CL_AddReliableCommand

The given command will be transmitted to the server, and is gauranteed to
not have future usercmd_t executed before it is executed
======================
*/
void
CL_AddReliableCommand (const char *cmd)
{
  int index;
  int unacknowledged = clc.reliableSequence - clc.reliableAcknowledge;

  // if we would be losing an old command that hasn't been acknowledged,
  // we must drop the connection
  // also leave one slot open for the disconnect command in this case.
  if (((unacknowledged >= (MAX_RELIABLE_COMMANDS - 1)) && Q_stricmp (cmd, "disconnect"))
      || ((unacknowledged > (MAX_RELIABLE_COMMANDS - 1)) && !Q_stricmp (cmd, "disconnect")))
    {
      if (cl_errorEntered)
	return;
      else
	CL_Error (ERR_DROP, "Client command overflow");
    }
  clc.reliableSequence++;
  index = clc.reliableSequence & (MAX_RELIABLE_COMMANDS - 1);
  Q_strncpyz (clc.reliableCommands[index], cmd,
	      sizeof (clc.reliableCommands[index]));
}

//======================================================================

/*
====================
CL_PrintWelcomeMessage

Be a little verbose for the player,
useful if the game was launched without arguments
====================
*/
void
CL_PrintWelcomeMessage (void)
{
  CL_Printf ("^7Welcome, have a nice game\n");
}


/*
=====================
CL_ShutdownAll
=====================
*/
void
CL_ShutdownAll (void)
{
  // clear sounds
  if (cls.soundRegistered)
    {
      snde->DisableSounds ();
      cls.soundRegistered = false;
    }

  // shutdown CGame
  if (cls.cgameStarted)
    {
      CL_ShutdownCGame ();
      cls.cgameStarted = false;
    }

  // shutdown the renderer
  CL_ShutdownRef (false);
}

/*
=================
CL_FlushMemory

Called by CL_Connect_f, CL_PlayDemo_f, and CL_ParseGamestate the only
ways a client gets into a game
Also called by CL_Error
=================
*/
void
CL_FlushMemory (void)
{
  // shutdown all the client stuff (free sounds, unload renderer and cgame so)
  CL_ShutdownAll ();

  // clear the whole hunk
  qce->mem.Hunk_Clear ();
  // clear collision map data
  qce->cm.ClearMap ();

  CL_StartHunkUsers (false);
}

/*
=====================
CL_ClearState

Called before parsing a gamestate or on disconnect (maybe useless ?)
Resets the clientActive (cl) structure
=====================
*/
void
CL_ClearState (void)
{
  Com_Memset (&cl, 0, sizeof (cl));
}

/*
=====================
CL_Disconnect

Called when a connection, demo, or cinematic is being terminated.
Goes from a connected state to either a menu state or a console state
Sends a disconnect message to the server
This is also called on CL_Error and Com_Quit, so it shouldn't cause any errors
=====================
*/
void
CL_Disconnect (bool showMainMenu)
{
  if (!cl_running || !cl_running->integer)
    {
      return;
    }

  if (clc.demorecording)
    {
      CL_StopRecord_f ();
    }

  if (clc.demofile)
    {
      qce->fs.FCloseFile (clc.demofile);
      clc.demofile = 0;
    }

  // stop sounds too
  CL_Snd_ClearSoundBuffer ();

  // send a disconnect message to the server
  // send it a few times in case one is dropped
  if (cls.state >= CA_CONNECTED)
    {
      CL_AddReliableCommand ("disconnect");
      CL_WritePacket ();
      CL_WritePacket ();
      //CL_WritePacket ();
    }

  // wipe the clientActive structure
  CL_ClearState ();

  // wipe the client connection
  Com_Memset (&clc, 0, sizeof (clc));

  cls.state = CA_DISCONNECTED;
}

/*
===================
CL_ForwardCommandToServer

adds the current command line as a clientCommand
things like godmode, noclip, etc, are commands directed to the server,
so when they are typed in at the console, they will need to be forwarded.
===================
*/
void
CL_ForwardCommandToServer (const char *string)
{
  char *cmd;

  // string is tokenized before calling this function
  cmd = qce->cmd.Argv (0);

  // ignore key up commands
  if (cmd[0] == '-')
    {
      return;
    }

  if (clc.demoplaying || (cls.state < CA_CONNECTED) || (cmd[0] == '+'))
    {
      CL_Printf ("Unknown command (can't forward to sv) \"%s" S_COLOR_WHITE "\"\n", cmd);
      return;
    }

  // forward the command string to the server
  CL_AddReliableCommand (string);
}

/*
======================================================================

CONSOLE COMMANDS

======================================================================
*/

/*
==================
CL_ForwardToServer_f
==================
*/
void
CL_ForwardToServer_f (void)
{
  if ((cls.state != CA_ACTIVE) || clc.demoplaying)
    {
      CL_Printf ("Not connected to a server.\n");
      return;
    }

  // don't forward the first argument as it is "cmd"
  if (qce->cmd.Argc () > 1)
    {
      CL_AddReliableCommand (qce->cmd.Args ()); // from 1st to last arg
    }
}

/*
==================
CL_Disconnect_f
==================
*/
void
CL_Disconnect_f (void)
{
  if (cls.state != CA_DISCONNECTED)
    {
      CL_Error (ERR_DISCONNECT, "Disconnected from server");
    }
}

/*
================
CL_Reconnect_f

================
*/
void
CL_Reconnect_f (void)
{
  if (!strlen (cls.servername))
    {
      CL_Printf ("No server address registered.\n");
      return;
    }

  qce->cmd.Cbuf_AddText (va ("connect %s\n", cls.servername));
}

/*
================
CL_Connect_f

================
*/
void
CL_Connect_f (void)
{
  char *server;
  const char *serverString;
  int argc = qce->cmd.Argc ();
  netadrtype_t family = NA_UNSPEC;

  if (argc != 2 && argc != 3)
    {
      CL_Printf ("usage: connect [-4|-6] server\n");
      return;
    }

  if (argc == 2)
    {
      server = qce->cmd.Argv (1);
    }
  else
    {
      if (!strcmp (qce->cmd.Argv (1), "-4"))
	{
	  family = NA_IP;
	}
      else if (!strcmp (qce->cmd.Argv (1), "-6"))
	{
	  family = NA_IP6;
	}
      else
	CL_Printf ("warning: only -4 or -6 as address type understood.\n");

      server = qce->cmd.Argv (2);
    }

  // clear any previous "server full" type messages
  clc.serverMessage[0] = 0;

  // drop any previous connection
  CL_Disconnect (true);
  Con_Close ();

  Q_strncpyz (cls.servername, server, sizeof (cls.servername));

  if (!qce->net.StringToAdr (cls.servername, &clc.serverAddress, family))
    {
      CL_Printf ("Bad server address\n");
      cls.state = CA_DISCONNECTED;
      return;
    }
  if (clc.serverAddress.port == 0)
    {
      clc.serverAddress.port = BigShort (PORT_SERVER);
    }

  serverString = qce->net.AdrToStringwPort (clc.serverAddress);

  CL_Printf ("%s resolved to %s\n", cls.servername, serverString);

  // we need to authenticate with a challenge
  cls.state = CA_CONNECTING;

  // Set a client challenge number that ideally is mirrored back by the server.
  clc.challenge = ((rand () << 16) ^ rand ()) ^ qce->event.Milliseconds ();

  Key_SetCatcher (0);
  clc.connectTime = -99999;	// CL_CheckForResend() will fire immediately
  clc.connectPacketCount = 0;

  // server connection string, do this properly
  // create a ROM cvar to get infos about the current connected server
  qce->cvar.Set ("cl_currentServerAddress", server);
}

#define MAX_RCON_MESSAGE 1024

/*
==================
CL_CompleteRcon
==================
*/
static void
CL_CompleteRcon (char *args, int argNum)
{
  if (argNum == 2)
    {
      // Skip "rcon "
      char *p = Com_SkipTokens (args, 1, " ");

      if (p > args)
	qce->fs.Field_CompleteCommand (p, true, true);
    }
}

/*
=====================
CL_Rcon_f

  Send the rest of the command line over as
  an unconnected command.
=====================
*/
void
CL_Rcon_f (void)
{
  char message[MAX_RCON_MESSAGE];
  netadr_t to;

  if (!rcon_client_password->string)
    {
      CL_Printf ("You must set 'rconpassword' before\n"
		 "issuing an rcon command.\n");
      return;
    }

  // skip the "rcon " string (1st arg)
  Com_sprintf (message, sizeof (message), "rcon %s %s",
	       rcon_client_password->string, qce->cmd.ArgsFrom (1));

  // send to the connected server
  if (cls.state >= CA_CONNECTED)
    {
      to = clc.netchan.remoteAddress;
    }
  // otherwise to rconAddress
  else
    {
      if (!strlen (rconAddress->string))
	{
	  CL_Printf ("You must either be connected,\n"
		     "or set the 'rconAddress' cvar\n"
		     "to issue rcon commands\n");

	  return;
	}
      qce->net.StringToAdr (rconAddress->string, &to, NA_UNSPEC);
      if (to.port == 0)
	{
	  to.port = BigShort (PORT_SERVER);
	}
    }

  qce->net.OutOfBandPrint (NS_CLIENT, to, "%s", message);
}

/*
=================
CL_Vid_Restart_f

Restart the video subsystem

we also have to reload the UI and CGame because the renderer
doesn't know what graphics to reload
=================
*/
void
CL_Vid_Restart_f (void)
{
  // Settings may have changed so stop video recording now
  if (cl_avidemo->integer)
    qce->cvar.SetNew (cl_avidemo, "0");

  // stop demo recording as we will reload cgame module
  if (clc.demorecording)
    CL_StopRecord_f ();

  // don't let them loop during the restart
  if (cls.soundStarted)
    {
      snde->StopAllSounds ();
      cls.soundRegistered = false;
    }

  // shutdown the CGame
  if (cls.cgameStarted)
    {
      CL_ShutdownCGame ();
      cls.cgameStarted = false;
    }

  // also restarted the inputs
  //CL_ShutdownInput ();

  // clear faces

  // shutdown the renderer and clear the renderer interface
  CL_ShutdownRef (true);

  // clear pak references
  qce->fs.ClearPakReferences (/*FS_UI_REF | */FS_CGAME_REF);

  // reinitialize the filesystem if the game directory or checksum has changed
  qce->fs.ConditionalRestart (clc.checksumFeed);

  // clear the whole hunk
  qce->mem.Hunk_Clear ();

  // initialize the renderer interface
  CL_InitRefLib ();

  if (!cls.soundlibLoaded)
    {
      CL_Printf ("InitSoundlib\n");
      cls.soundStarted = CL_InitSoundLib ();
    }

  // startup all the client stuff
  CL_StartHunkUsers (false);

  // start the cgame if connected
  if (cls.state > CA_CONNECTED)
    {
      CL_InitCGame ();
    }
}

/*
=================
CL_Snd_Restart_f

Restart the sound subsystem
The cgame and game must also be forced to restart because
handles will be invalid
=================
*/
void
CL_Snd_Restart_f (void)
{
  //CL_ShutdownSound ();

  //CL_UnloadSoundLib ();

  // because we need to reload the sound files
  // TODO : split renderer/sound code
  CL_Vid_Restart_f ();
}

/*
==================
CL_OpenedPK3List_f
==================
*/
void
CL_OpenedPK3List_f (void)
{
  CL_Printf ("Opened PK3 Names: %s\n", qce->fs.LoadedPakNames ());
}

/*
==================
CL_ReferencedPK3List_f
==================
*/
void
CL_ReferencedPK3List_f (void)
{
  CL_Printf ("Referenced PK3 Names: %s\n", qce->fs.ReferencedPakNames ());
}

/*
==================
CL_Configstrings_f
==================
*/
void
CL_Configstrings_f (void)
{
  int i;
  int ofs;

  if (cls.state != CA_ACTIVE)
    {
      CL_Printf ("Not connected to a server.\n");
      return;
    }

  for (i = 0; i < MAX_CONFIGSTRINGS; i++)
    {
      ofs = cl.gameState.stringOffsets[i];
      if (!ofs)
	{
	  continue;
	}
      CL_Printf ("%4i: %s\n", i, cl.gameState.stringData + ofs);
    }
}

/*
==============
CL_Clientinfo_f
==============
*/
void
CL_Clientinfo_f (void)
{
  CL_Printf ("--------- Client Information ---------\n");
  CL_Printf ("state: %i\n", cls.state);
  CL_Printf ("Server: %s\n", cls.servername);
  CL_Printf ("User info settings:\n");
  qce->Info_Print (qce->cvar.InfoString (CVAR_USERINFO));
  CL_Printf ("--------------------------------------\n");
}

/*
==================
CL_ShowIP_f
==================
*/
void
CL_ShowIP_f (void)
{
  qce->net.ShowIP ();
}

//====================================================================

/*
=================
CL_CheckFilesAvailable

True if the client has all referencedpaks from server
=================
*/
bool
CL_CheckFilesAvailable (void)
{
  char missingfiles[1024];
  // it's possible that some referenced files on the server are missing
  if (qce->fs.ComparePaks (missingfiles, sizeof (missingfiles), false))
    {
      // let the client know what he's missing
      CL_Printf
	("\nWARNING: You are missing some files referenced by the server:\n%s"
	 "You might not be able to join the game\n", missingfiles);
      return false;
    }
  return true;
}

/*
=================
CL_LaunchCGame

Loading and running CGame
=================
*/
void
CL_LaunchCGame (void)
{
  // let the client game init and load data
  cls.state = CA_LOADING;

  // Pump the loop, this may change gamestate!
  CL_EventLoop ();

  // if the gamestate was changed by calling Com_EventLoop
  // then we loaded everything already and we don't want to do it again.
  if (cls.state != CA_LOADING)
    {
      return;
    }

  // flush client memory and start loading stuff
  // this will also (re)load the UI
  // if this is a local client then only the client part of the hunk
  // will be cleared, note that this is done after the hunk mark has been set
  CL_FlushMemory ();

  // initialize the CGame
  CL_InitCGame ();

  // real need to send it 3 times ?
  CL_WritePacket ();
  CL_WritePacket ();
  //CL_WritePacket ();
}

/*
=================
CL_CheckForResend

Resend a connect message if the last one has timed out
=================
*/
void
CL_CheckForResend (void)
{
  int port;
  char info[MAX_INFO_STRING];
  char data[MAX_INFO_STRING];

  // don't send anything if playing back a demo
  if (clc.demoplaying)
    {
      return;
    }

  // resend if we haven't gotten a reply yet
  if ((cls.state != CA_CONNECTING) && (cls.state != CA_CHALLENGING))
    {
      return;
    }

  if ((cls.realtime - clc.connectTime) < RETRANSMIT_TIMEOUT)
    {
      return;
    }

  clc.connectTime = cls.realtime;	// for retransmit requests
  clc.connectPacketCount++;

  switch (cls.state)
    {
    case CA_CONNECTING:
      // The challenge request shall be followed by a client challenge
      // so no malicious server can hijack this connection.
      Com_sprintf (data, sizeof (data), "getchallenge %d", clc.challenge);

      qce->net.OutOfBandPrint (NS_CLIENT, clc.serverAddress, "%s", data);
      break;

    case CA_CHALLENGING:
      // sending back the challenge
      port = qce->cvar.VariableValue ("net_qport");

      Q_strncpyz (info, qce->cvar.InfoString (CVAR_USERINFO), sizeof (info));
      Info_SetValueForKey (info, "protocol", va ("%i", PROTOCOL_VERSION));
      Info_SetValueForKey (info, "qport", va ("%i", port));
      Info_SetValueForKey (info, "challenge", va ("%i", clc.challenge));

      // TTimo adding " " around the userinfo string to avoid truncated userinfo on the server
      //   (Com_TokenizeString tokenizes around spaces)
      Com_sprintf (data, sizeof (data), "connect \"%s\"", info);

      // including the ending \0 in the length ?
      qce->net.OutOfBandData (NS_CLIENT, clc.serverAddress, (byte *) & data[0],
			      strlen (data) + 1);

      // the most current userinfo has been sent, so watch for any
      // newer changes to userinfo variables
      qce->cvar.Set_ModifiedFlags (qce->cvar.Get_ModifiedFlags () & ~CVAR_USERINFO);
      break;

    default:
      CL_Error (ERR_FATAL, "CL_CheckForResend: bad cls.state");
    }
}

/*
===================
CL_DisconnectPacket

Sometimes the server can drop the client and the netchan based
disconnect can be lost.  If the client continues to send packets
to the server, the server will send out of band disconnect packets
to the client so it doesn't have to wait for the full timeout period.
===================
*/
void
CL_DisconnectPacket (netadr_t from)
{
  if (cls.state < CA_AUTHORIZING)
    {
      return;
    }

  // if not from our server, ignore it
  if (!qce->net.CompareAdr (from, clc.netchan.remoteAddress))
    {
      return;
    }

  // if we have received packets within three seconds, ignore it
  // (it might be a malicious spoof)
  if ((cls.realtime - clc.lastPacketTime) < 3000)
    {
      return;
    }

  // drop the connection
  CL_Printf ("Server disconnected for unknown reason\n");
  qce->cvar.Set ("com_errorMessage", "Server disconnected for unknown reason\n");

  CL_Disconnect (true);
}

/*
=================
CL_ConnectionlessPacket

Responses to broadcasts, etc
=================
*/
void
CL_ConnectionlessPacket (netadr_t from, msg_t * msg)
{
  char *s;
  char *c;

  qce->msg.BeginReadingOOB (msg);
  qce->msg.ReadLong (msg);		// skip the -1

  s = qce->msg.ReadStringLine (msg);

  qce->cmd.TokenizeString (s);

  c = qce->cmd.Argv (0);

  CL_DPrintf ("CL packet %s: %s\n", qce->net.AdrToStringwPort (from), c);

  // challenge from the server we are connecting to
  if (!Q_stricmp (c, "challengeResponse"))
    {
      if (cls.state != CA_CONNECTING)
	{
	  CL_DPrintf ("Unwanted challenge response received.  Ignored.\n");
	  return;
	}

      if (!qce->net.CompareAdr (from, clc.serverAddress))
	{
	  // This challenge response is not coming from the expected address.
	  // Check whether we have a matching client challenge to prevent
	  // connection hi-jacking.

	  c = qce->cmd.Argv (2);

	  if (!*c || (atoi (c) != clc.challenge))
	    {
	      CL_DPrintf
		("Challenge response received from unexpected source. Ignored.\n");
	      return;
	    }
	}

      // start sending challenge response instead of challenge request packets
      clc.challenge = atoi (qce->cmd.Argv (1));
      cls.state = CA_CHALLENGING;
      clc.connectPacketCount = 0;
      clc.connectTime = -99999;

      // take this address as the new server address.  This allows
      // a server proxy to hand off connections to multiple servers
      clc.serverAddress = from;
      CL_DPrintf ("challengeResponse: %d\n", clc.challenge);
      return;
    }

  // server connection
  if (!Q_stricmp (c, "connectResponse"))
    {
      if (cls.state >= CA_CONNECTED)
	{
	  CL_Printf ("Dup connect received.  Ignored.\n");
	  return;
	}
      if (cls.state != CA_CHALLENGING)
	{
	  CL_Printf
	    ("connectResponse packet while not connecting. Ignored.\n");
	  return;
	}
      if (!qce->net.CompareAdr (from, clc.serverAddress))
	{
	  CL_Printf ("connectResponse from wrong address. Ignored.\n");
	  return;
	}
      qce->net.Netchan_Setup (NS_CLIENT, &clc.netchan, from,
			      qce->cvar.VariableValue ("net_qport"));
      cls.state = CA_CONNECTED;
      clc.lastPacketSentTime = -9999;	// send first packet immediately
      return;
    }

  // a disconnect message from the server, which will happen if the server
  // dropped the connection but it is still getting packets from us
  if (!Q_stricmp (c, "disconnect"))
    {
      CL_DisconnectPacket (from);
      return;
    }

  // echo request from server
  if (!Q_stricmp (c, "echo"))
    {
      qce->net.OutOfBandPrint (NS_CLIENT, from, "%s", qce->cmd.Argv (1));
      return;
    }

  // echo request from server
  if (!Q_stricmp (c, "print"))
    {
      s = qce->msg.ReadString (msg);
      Q_strncpyz (clc.serverMessage, s, sizeof (clc.serverMessage));
      CL_Printf ("%s", s);
      return;
    }

  CL_DPrintf ("Unknown connectionless packet command.\n");
}

/*
=================
CL_PacketEvent

A packet has arrived from the main event loop
=================
*/
void
CL_PacketEvent (netadr_t from, msg_t * msg, int time)
{
  int headerBytes;

  clc.lastPacketTime = cls.realtime;

  if ((msg->cursize >= 4) && (*(int *) msg->data == -1))
    {
      CL_ConnectionlessPacket (from, msg);
      return;
    }

  if (cls.state < CA_CONNECTED)
    {
      return;			// can't be a valid sequenced packet
    }

  if (msg->cursize < 4)
    {
      CL_Printf ("%s: Runt packet\n", qce->net.AdrToStringwPort (from));
      return;
    }

  //
  // packet from server
  //
  if (!qce->net.CompareAdr (from, clc.netchan.remoteAddress))
    {
      CL_DPrintf ("%s:sequenced packet without connection\n",
		  qce->net.AdrToStringwPort (from));
      // FIXME: send a client disconnect?
      return;
    }

  if (!CL_Netchan_Process (&clc.netchan, msg))
    {
      return;		// out of order, duplicated, etc
    }

  // the header has different lengths for reliable and unreliable messages
  headerBytes = msg->readcount;

  // track the last message received so it can be returned in 
  // client messages, allowing the server to detect a dropped
  // gamestate
  clc.serverMessageSequence = LittleLong (*(int *) msg->data);

  CL_ParseServerMessage (msg, time);

  //
  // we don't know if it is ok to save a demo message until
  // after we have parsed the frame
  //
  if (clc.demorecording && !clc.demowaiting)
    {
      CL_WriteDemoMessage (msg, headerBytes);
    }
}

/*
==================
CL_CheckTimeout

==================
*/
void
CL_CheckTimeout (void)
{
  //
  // check timeout
  //
  int timediff;
  timediff = cls.realtime - clc.lastPacketTime;

  if ((cls.state >= CA_CONNECTED) && (timediff > (cl_timeout->value * 1000)))
    {
      if (++cl.timeoutcount > 5)
	{			// timeoutcount saves debugger
	  CL_Printf ("\nServer connection timed out.\n");
	  CL_Disconnect (true);
	  return;
	}
    }
  else
    {
      cl.timeoutcount = 0;
    }
}

//============================================================================

/*
=============
CL_AutoRecordAction

=============
*/
static void
CL_AutoRecordAction (void)
{
  if (cl_autoRecordDemo->integer)
    {
      if (cls.state == CA_ACTIVE && !clc.demorecording && !clc.demoplaying)
	{
	  // If not recording a demo, and we should be, start one
	  qtime_t now;
	  char *nowString;
	  char *p;
	  char mapName[MAX_QPATH];
	  char serverName[MAX_OSPATH];

	  qce->Com_RealTime (&now);
	  nowString = va ("%04d%02d%02d%02d%02d%02d",
			  1900 + now.tm_year,
			  1 + now.tm_mon,
			  now.tm_mday, now.tm_hour, now.tm_min, now.tm_sec);

	  Q_strncpyz (serverName, cls.servername, MAX_OSPATH);
	  // Replace the ":" in the address as it is not a valid
	  // file name character
	  p = strstr (serverName, ":");
	  if (p)
	    {
	      *p = '.';
	    }

	  Q_strncpyz (mapName, COM_SkipPath (cl.mapname),
		      sizeof (cl.mapname));
	  COM_StripExtension (mapName, mapName, sizeof (mapName));

	  qce->cmd.Cbuf_ExecuteText (EXEC_NOW, va ("record %s-%s-%s", nowString, serverName,
						   mapName));
	}
      else if (cls.state != CA_ACTIVE && clc.demorecording)
	{
	  // Recording, but not CA_ACTIVE, so stop recording
	  CL_StopRecord_f ();
	}
    }
}

/*
==================
CL_CheckUserinfo

==================
*/
void
CL_CheckUserinfo (void)
{
  int flags;
  // don't add reliable commands when not yet connected
  if (cls.state < CA_CHALLENGING)
    return;

  // send a reliable userinfo update if needed
  flags = qce->cvar.Get_ModifiedFlags ();
  if (flags & CVAR_USERINFO)
    {
      flags &= ~CVAR_USERINFO;
      qce->cvar.Set_ModifiedFlags (flags);
      CL_AddReliableCommand (va ("userinfo \"%s\"",
				 qce->cvar.InfoString (CVAR_USERINFO)));
    }
}

/*
================
CL_ModifyMsec
================
*/
int
CL_ModifyMsec (int msec)
{
  int clampTime;

  //
  // modify time for debugging values
  //
  if (cl_fixedtime->integer)
    {
      msec = cl_fixedtime->integer;
    }
  else if (cl_timescale->value)
    {
      msec *= cl_timescale->value;
    }

  // don't let it scale below 1 msec
  if ((msec < 1) && cl_timescale->value)
    {
      msec = 1;
    }

  clampTime = 5000;

  if (msec > clampTime)
    {
      msec = clampTime;
    }

  return msec;
}

/*
=================
CL_TimeVal

System time remaining before rendering the next frame
=================
*/
int
CL_TimeVal (int minMsec, int lastFrameTime)
{
  int timeVal;

  timeVal = Sys_Milliseconds () - lastFrameTime;

  if (timeVal >= minMsec)
    timeVal = 0;
  else
    timeVal = minMsec - timeVal;

  return timeVal;
}

/*
=================
CL_MinMsec

Determining the time between two successive frames
For msec negative, client should run a frame *now*
=================
*/

// NOTE : we could add modifier depending on some conditions
// minimize/maximized etc... as ioq3 does.
int
CL_MinMsec (int frameTime, int lastTime)
{
  int msec;

  // this would cause the client to run "faster" than max_fps
  // after a slowdown just to have a proper mean value of max_fps
  // violating the condition that max_fps is the maximal framerate...
  const int use_timing_bias = 0;

  if (cl_timedemo->integer)
    {
      msec = 1;
    }
  else
    {
      msec = (int) ceilf (1000.0 / cl_maxfps->value);

      if (use_timing_bias)
	{
	  static int bias = 0;
	  int timeVal;
	  timeVal = frameTime - lastTime;

	  bias += timeVal - msec;

	  if (bias > msec)
	    bias = msec;

	  // Adjust minMsec if previous frame took too long to render so
	  // that framerate is stable at the requested value.
	  msec -= bias;
	}
    }

  if (msec < 0)
    {
      CL_Printf ("CL_MinMsec(): Negative msec [%d] ! Fix maxfps !\n", msec);
      msec = 0;
    }

  return msec;
}

/*
==================
CL_Frame

structure :
1: Tag (put timestamps and enqueue) new network messages

2: Sleep and Loop conditions
   if a new packet arrives return to 1:

3: Process infos and run the simulation
4: Store/Send results
5: Display to the screen
==================
*/
extern int scr_recursive;

void
CL_Frame (/*int msec*/void)
{
  int frameMsec;
  int minMsec;

  // com_frame imported code
  int msec;
  static int lastTime = 0;
  int timeVal;

  // imported from com_frame
  if (setjmp (abortframe))
    {
      return;		// an ERR_DROP was thrown
    }

  // should give 1000/fps as we don't handle bias
  minMsec = CL_MinMsec (cl_frameTime, lastTime);

  // timestamps received net packets
  qce->event.TagEvents ();

  // sleep a bit and loop for the last msec
  if (cl_waste_cputime->integer == 2)
    {
      timeVal = CL_TimeVal (minMsec, cl_frameTime);

      if (timeVal >= 1)
	{
	  int ret;

	  ret = qce->net.Sleep (timeVal - 1);
	  /* Sys_Sleep2 (timeVal - 1); */

	  // some network packet arrived before newframe
	  if (ret > 0)
	    {
	      return;
	    }

	  // nearly time to render; loop for 1 ms
	  do
	    {
	      ;
	    }
	  while (CL_TimeVal (minMsec, cl_frameTime));
	}
    }
  // don't sleep and loop
  // NOTE : performances can be WORSE on some machine
  //        when the OS needs ressources for concurrent processes
  //	    like the inputs
  else if (cl_waste_cputime->integer)
    {
      do
	{
	  ;
	}
      while (CL_TimeVal (minMsec, cl_frameTime));
    }
  // otherwise sleep the calculated time and do the work on wakeup
  else
    {
      timeVal = CL_TimeVal (minMsec, cl_frameTime);

      if (timeVal > 0)
	{
	  int ret;

	  ret = qce->net.Sleep (timeVal);
	  /* Sys_Sleep2 (timeVal); */

	  // new packet arrived, tag it
	  // otherwise do the work
	  if (ret > 0)
	    {
	      return;
	    }
	}
    }

  lastTime = cl_frameTime;
  cl_frameTime = CL_EventLoop ();
  msec = cl_frameTime - lastTime;

  qce->cmd.Cbuf_Execute ();

  // cl_frame ()
  if (!cl_running->integer)
    {
      CL_Error (ERR_FATAL, "cl_running = 0, this shouldn't happend\n");
    }

  // if it isn't time for the next frame, do nothing
  if (cl_maxfps->integer < 1)
    {
      // this shouldn't happend
      qce->cvar.SetNew (cl_maxfps, "10");
    }

  // intentional framerate; this has no impact on gameplay
  frameMsec =  (int) ceilf (1000.0 / cl_maxfps->value * cl_timescale->value);
  // don't let it scale below 1ms
  if (frameMsec < 1)
    {
      float time;

      time = floorf (cl_maxfps->value / 1000.0f);
      qce->cvar.SetNew (cl_timescale, va ("%f", time));
    }

  // mess with msec if needed
  msec = CL_ModifyMsec (msec);

  // NOTE bubu: these calls seem uneffective as we just called them
  // and if cbuf_execute generated some event, wait for the next frame...
  //CL_EventLoop ();
  //qce->cmd.Cbuf_Execute ();

  // if dumping tgas, lock to a fixed fps
  if (cl_avidemo->integer && cl_aviFrameRate->integer && msec)
    {
      // save the current screen
      if (cls.state == CA_ACTIVE || cl_forceavidemo->integer)
	{
	  qce->cmd.Cbuf_ExecuteText (EXEC_NOW, "screenshotJPEG silent\n");
	}

      // fixed time for next frame'
      msec = (int) ceilf (1000.0f / cl_aviFrameRate->value * cl_timescale->value);
      if (msec <= 0)
	{
	  msec = 1;
	}
    }

  // we might want to autorecord the client's game
  CL_AutoRecordAction ();

  // save the msec before checking pause
  cls.realFrametime = msec;

  // decide the simulation time
  cls.frametime = msec;

  cls.realtime += cls.frametime;

  // see if we need to update any userinfo
  CL_CheckUserinfo ();

  // if we haven't gotten a packet in a long time,
  // drop the connection
  CL_CheckTimeout ();

  // send intentions now
  CL_SendCmd ();

  // update inputs
  inpe->Frame ((cls.state != CA_DISCONNECTED) && (cls.state != CA_ACTIVE));

  // resend a connection request if necessary
  CL_CheckForResend ();

  // decide on the serverTime to render
  CL_SetCGameTime ();

  // update the screen
  SCR_UpdateScreen ();

  // update audio
  CL_Snd_Update ();

  // advance local effects for next frame
  //SCR_RunCinematic ();

  Con_RunConsole ();

  cls.framecount++;
}

//============================================================================

/*
================
CL_RefPrintf

DLL glue
================
*/
void
CL_RefPrintf (int print_level, const char *fmt, ...)
{
  va_list argptr;
  char msg[MAXPRINTMSG];

  va_start (argptr, fmt);
  Q_vsnprintf (msg, sizeof (msg), fmt, argptr);
  va_end (argptr);

  if (print_level == PRINT_ALL)
    {
      CL_Printf ("%s", msg);
    }
  else if (print_level == PRINT_WARNING)
    {
      CL_Printf (S_COLOR_YELLOW "%s", msg);	// yellow
    }
  else if (print_level == PRINT_DEVELOPER)
    {
      CL_DPrintf (S_COLOR_RED "%s", msg);	// red
    }
}

/*
============
CL_ShutdownRef
============
*/
void
CL_ShutdownRef (bool destroyWindow)
{
  if (!cls.rendererStarted)
    return;

  /*
  if (!re.Shutdown)
    {
      return;
    }
  */
  re.FreeFace (&cls.consoleFace);

  re.Shutdown (destroyWindow);
  cls.rendererStarted = false;
}

void
CL_UnloadRefLib (void)
{
  if (!cls.rendererlibLoaded)
    return;

  Sys_DLUnload (rdlib.dynHandle);
  Com_Memset (&re, 0, sizeof (re));
  cls.rendererlibLoaded = false;
}

/*
============
CL_InitRenderer
============
*/
void
CL_InitRenderer (void)
{
  // this sets up the renderer and calls R_Init
  re.BeginRegistration (&cls.glconfig);

  // load character sets
  cls.charSetShader = re.RegisterShader ("gfx/2d/bigchars");
  cls.whiteShader = re.RegisterShader ("white");
  cls.consoleShader = re.RegisterShader ("console");
  g_console_field_width = cls.glconfig.vidWidth / SMALLCHAR_WIDTH - 2;
  g_consoleField.widthInChars = g_console_field_width; // 78 in 640; 126 in 1024

  cls.useLegacyConsoleFont = true;

  // register console font specified by cl_console, if any
  // filehandle is unuseed but forces FS_FOpenFileRead() to heed purecheck
  // because it does not when filehandle is NULL
  if (*cl_consoleFont->string)
    {
      fileHandle_t f;

      if (qce->fs.FOpenFileRead (cl_consoleFont->string, &f, FS_READ) >= 0)
	{
	  re.LoadFace (cl_consoleFont->string, cl_consoleFontSize->integer,
		       cl_consoleFont->string, &cls.consoleFace);
	  cls.useLegacyConsoleFont = false;
	}
      qce->fs.FCloseFile (f);
    }
}

/*
============================
CL_StartHunkUsers

After the server has cleared the hunk, these will need to be restarted
This is the only place that any of these functions are called from
============================
*/
void
CL_StartHunkUsers (bool rendererOnly)
{
  /*
  if (!cl_running)
    {
      return;
    }

  if (!cl_running->integer)
    {
      return;
    }
  */

  CL_DPrintf ("CL_StartHunkUsers ()\n");

  if (!cls.rendererStarted)
    {
      if (!cls.rendererlibLoaded)
	{
	  CL_Error (ERR_FATAL, "StartHunkUsers () : Renderer Lib not loaded\n");
	}

      CL_InitRenderer ();
      cls.rendererStarted = true;
      inpe->Restart ();
      cls.inputStarted = true;
    }

  if (rendererOnly)
    {
      return;
    }

  if (cls.soundStarted && !cls.soundRegistered)
    {
      if (!cls.soundlibLoaded)
	{
	  CL_Error (ERR_FATAL, "StartHunkUsers () : Sound Lib not loaded\n");
	}

      snde->BeginRegistration ();
      cls.soundRegistered = true;
    }
}

/*
============
CL_RefMalloc
============
*/
void *
CL_RefMalloc (int size)
{
  return qce->mem.Z_TagMalloc (size, TAG_RENDERER);
}

int
CL_ScaledMilliseconds (void)
{
  return Sys_Milliseconds () * cl_timescale->value;
}

/*
============
CL_InitRef
============
*/
void
CL_InitRefLib (void)
{
  refimport_t ri;
  refexport_t *ret;
  void *Hdl;
  refexport_t *(*RefAPI) (int version, refimport_t * riptr);

  Com_Memset (&rdlib, 0, sizeof (rdlib));

  CL_Printf ("----- Initializing Renderer (Loading renderer lib) ----\n");
  // load the dynamic library
  rdlib.dynHandle = Sys_DLLoadType ("libq3renderer", rdlib.fqpath, CLIENT);

  Hdl = rdlib.dynHandle;
  RefAPI = (refexport_t * (*)(int, refimport_t *))
    Sys_DLLoadFunction (Hdl, "GetRefAPI", true);

  cls.rendererlibLoaded = true;

  ri.Cmd_AddCommand = qce->cmd.AddCommand;
  ri.Cmd_RemoveCommand = qce->cmd.RemoveCommand;
  ri.Cmd_Argc = qce->cmd.Argc;
  ri.Cmd_Argv = qce->cmd.Argv;
  ri.Cmd_ExecuteText = qce->cmd.Cbuf_ExecuteText;
  ri.Printf = CL_RefPrintf;
  ri.Error = CL_Error;
  // better add a GetTimeScale function, because you can get 
  // CL_ScaledMS < (previous MS with timescale higher),
  // ScaledMS should only be used to diff in the same function to avoid errors
  ri.Milliseconds = CL_ScaledMilliseconds;
  ri.Malloc = CL_RefMalloc;
  ri.Free = qce->mem.Z_Free;
#ifdef HUNK_DEBUG
  ri.Hunk_AllocDebug = qce->mem.Hunk_AllocDebug;
#else
  ri.Hunk_Alloc = qce->mem.Hunk_Alloc;
#endif
  ri.Hunk_AllocateTempMemory = qce->mem.Hunk_AllocateTempMemory;
  ri.Hunk_FreeTempMemory = qce->mem.Hunk_FreeTempMemory;
  ri.CM_DrawDebugSurface = qce->cm.DrawDebugSurface;
  ri.CM_ClusterPVS = qce->cm.ClusterPVS;
  ri.FS_ReadFile = qce->fs.ReadFile;
  ri.FS_FOpenFileRead = qce->fs.FOpenFileRead;
  ri.FS_FCloseFile = qce->fs.FCloseFile;
  ri.FS_Seek = qce->fs.Seek;
  ri.FS_Read = qce->fs.Read;
  ri.FS_FTell = qce->fs.FTell;
  ri.FS_FreeFile = qce->fs.FreeFile;
  ri.FS_WriteFile = qce->fs.WriteFile;
  ri.FS_FreeFileList = qce->fs.FreeFileList;
  ri.FS_ListFiles = qce->fs.ListFiles;
  ri.FS_FileIsInPAK = qce->fs.FileIsInPAK;
  ri.FS_FileExists = qce->fs.FileExists;
  ri.Cvar_Get = qce->cvar.Get;
  ri.Cvar_Set = qce->cvar.Set;
  ri.Cvar_CheckRange = qce->cvar.CheckRange;
  ri.Cvar_VariableIntegerValue = qce->cvar.VariableIntegerValue;

  ret = RefAPI (REF_API_VERSION, &ri);

  CL_Printf ("-------------------------------\n");

  if (!ret)
    {
      CL_Error (ERR_FATAL, "Couldn't initialize refresh");
    }

  re = *ret;
}

//===========================================================================================

void
CL_SetModel_f (void)
{
  char *arg;
  char name[256];

  arg = qce->cmd.Argv (1);
  if (arg[0])
    {
      qce->cvar.Set ("model", arg);
      qce->cvar.Set ("headmodel", arg);
    }
  else
    {
      qce->cvar.VariableStringBuffer ("model", name, sizeof (name));
      CL_Printf ("model is set to %s\n", name);
    }
}

//===========================================================================================

/*
_-_-_-_-_-_-_-_-_-# ! (O_O) ! Gabi special hack de la mort ! __(-_(-_(-_(-_(-_(-_-)
use rather popen than system
*/
void
dev_HACK_system_f (void)
{
  char arg[MAX_STRING_CHARS];
  int i, argc;

  // at least 2 args : <cmd> <arg1>
  argc = qce->cmd.Argc ();
  if (argc < 2)
    {
      CL_Printf ("usage : system \"command\"\n");
      return;
    }

  // initialize before loop (if there's a loop ^^, more than 2 args)
  Com_Memset (arg, 0, sizeof(arg));

  for (i = 1; i < argc; i++)
    {
      Q_strcat (arg, sizeof (arg), qce->cmd.Argv (i));
      Q_strcat (arg, sizeof (arg), " ");
    }
  //CL_Printf ("system (%s)\n", arg);

  // useless test ?
  if (arg[0])
    {
      system (arg);
    }
}

/*
===============
CL_180_hax

make a quick 180°
===============
*/
void
CL_180Hax_f (void)
{
  cl.viewangles[YAW] += 180.0f;
}

/*
===============
Cmd_PVstr_f

Inserts the current value of a variable as command text
===============
*/
void
Cmd_PVstr_f (void)
{
  char *v = NULL;
  static unsigned int press = 0;
  static unsigned int rel = 0;

  // 5 because of bind <key> +vstr <variablename1> <variablename2>
  // but i don't see where is the cmd_argv[] index stripped
  if (qce->cmd.Argc () != 5)
    {
      CL_Printf
	("bind <key> +vstr <variablename1> <variablename2>:"
	 "execute a variable command on key press and release\n");
      return;
    }

  switch (qce->cmd.Argv (0)[0])
    {
    case '+':
      v = qce->cvar.VariableString (qce->cmd.Argv (1));
      press++;
      break;
    case '-':
      // we check this because otherwise key release would fire even in the console...
      if (rel < press)
	{
	  v = qce->cvar.VariableString (qce->cmd.Argv (2));
	  rel++;
	}
      break;
    default:
      CL_Printf ("Cmd_PVstr_f: unexpected leading character '%c'\n",
		 qce->cmd.Argv (0)[0]);
    }
  if (v)
    {
      //Cbuf_AddText( va("%s\n", v ) );
      qce->cmd.Cbuf_ExecuteText (EXEC_NOW, va ("%s\n", v));
    }
}

/*
=============
CL_Quit_f

Both client and server can use this, and it will
do the apropriate things.
=============
*/
void
CL_Quit_f (void)
{
  // don't try to shutdown if we are in a recursive error
  char *p = qce->cmd.Args ();
  if (!cl_errorEntered)
    {
      CL_Shutdown (p[0] ? p : "Server quit", true);
      // can we call these with an invalid qce pointer ?
      qce->Com_Shutdown ();
      qce->fs.Shutdown (true);
      //CL_UnloadCommonLib ();
    }
  Sys_Quit ();
}

/*
====================
CL_Init
====================
*/
void
CL_Init (void)
{
  CL_Printf ("----- Client Initialization -----\n");

  Con_Init ();

  CL_ClearState ();

  cls.state = CA_DISCONNECTED;	// no longer CA_UNINITIALIZED

  cls.realtime = 0;

  CL_InitInput ();

  //
  // register our variables
  //
  cl_noprint = qce->cvar.Get ("cl_noprint", "0", 0);

  cl_timeout = qce->cvar.Get ("cl_timeout", "200", 0);

  rcon_client_password = qce->cvar.Get ("rconPassword", "", CVAR_TEMP);
  rconAddress = qce->cvar.Get ("rconAddress", "", 0);

  cl_timeNudge = qce->cvar.Get ("cl_timeNudge", "0", CVAR_ARCHIVE);
  cl_shownet = qce->cvar.Get ("cl_shownet", "0", CVAR_TEMP);
  cl_showSend = qce->cvar.Get ("cl_showSend", "0", CVAR_TEMP);
  cl_showTimeDelta = qce->cvar.Get ("cl_showTimeDelta", "0", CVAR_TEMP);
  cl_showSimTimeDelta = qce->cvar.Get ("cl_showSimTimeDelta", "0", CVAR_TEMP);
  cl_activeAction = qce->cvar.Get ("cl_activeAction", "", CVAR_TEMP);
  cl_simTimeSmooth = qce->cvar.Get ("cl_simTimeSmooth", "1", CVAR_ARCHIVE);

  cl_timedemo = qce->cvar.Get ("timedemo", "0", 0);
  cl_timedemoLog = qce->cvar.Get ("cl_timedemoLog", "", CVAR_ARCHIVE);
  cl_autoRecordDemo = qce->cvar.Get ("cl_autoRecordDemo", "0", CVAR_ARCHIVE);
  cl_drawDemoRecording = qce->cvar.Get ("cl_drawDemoRecording", "1", CVAR_ARCHIVE);

  cl_avidemo = qce->cvar.Get ("cl_avidemo", "0", 0);
  cl_aviFrameRate = qce->cvar.Get ("cl_aviFrameRate", "50", CVAR_ARCHIVE);
  cl_forceavidemo = qce->cvar.Get ("cl_forceavidemo", "0", 0);
  cl_demoShowSequence = qce->cvar.Get ("cl_demoShowSequence", "0", CVAR_TEMP);
  cl_demoConfigFile = qce->cvar.Get ("cl_demoConfigFile", "", CVAR_ARCHIVE);

  cl_yawspeed = qce->cvar.Get ("cl_yawspeed", "140", CVAR_ARCHIVE);
  cl_pitchspeed = qce->cvar.Get ("cl_pitchspeed", "140", CVAR_ARCHIVE);
  cl_anglespeedkey = qce->cvar.Get ("cl_anglespeedkey", "1.5", 0);

  cl_maxpackets = qce->cvar.Get ("cl_maxpackets", "125", CVAR_ARCHIVE);
  qce->cvar.CheckRange (cl_maxpackets, 15, 125, true);
  cl_packetdup = qce->cvar.Get ("cl_packetdup", "1", CVAR_ARCHIVE);
  qce->cvar.CheckRange (cl_packetdup, 0, 5, true);

  cl_run = qce->cvar.Get ("cl_run", "1", CVAR_ARCHIVE);

  cl_sensitivity = qce->cvar.Get ("sensitivity", "5", CVAR_ARCHIVE);
  cl_mouseAccel = qce->cvar.Get ("cl_mouseAccel", "0", CVAR_ARCHIVE);
  cl_freelook = qce->cvar.Get ("cl_freelook", "1", CVAR_ARCHIVE);
  cl_showMouseRate = qce->cvar.Get ("cl_showmouserate", "0", 0);
  // 0: legacy mouse acceleration
  // 1: new implementation
  cl_mouseAccelStyle = qce->cvar.Get ("cl_mouseAccelStyle", "0", CVAR_ARCHIVE);
  // offset for style 1, ignored otherwise
  // this should be set to the max rate value wanted before accel starts
  cl_mouseAccelOffsetX = qce->cvar.Get ("cl_mouseAccelOffsetX", "5", CVAR_ARCHIVE);
  cl_mouseAccelOffsetY = qce->cvar.Get ("cl_mouseAccelOffsetY", "5", CVAR_ARCHIVE);
  qce->cvar.CheckRange (cl_mouseAccelOffsetX, 0.001f, 50000.0f, false);
  qce->cvar.CheckRange (cl_mouseAccelOffsetY, 0.001f, 50000.0f, false);

  m_pitch = qce->cvar.Get ("m_pitch", "0.022", CVAR_ARCHIVE);
  m_yaw = qce->cvar.Get ("m_yaw", "0.022", CVAR_ARCHIVE);
  m_forward = qce->cvar.Get ("m_forward", "0.25", CVAR_ARCHIVE);
  m_side = qce->cvar.Get ("m_side", "0.25", CVAR_ARCHIVE);
  m_filter = qce->cvar.Get ("m_filter", "0", CVAR_ARCHIVE);

  cl_conXOffset = qce->cvar.Get ("cl_conXOffset", "0", CVAR_ARCHIVE);

  cl_consoleFont = qce->cvar.Get ("cl_consoleFont", "", CVAR_ARCHIVE | CVAR_LATCH);
  cl_consoleFontSize = qce->cvar.Get ("cl_consoleFontSize", "16", CVAR_ARCHIVE | CVAR_LATCH);
  cl_consoleFontKerning = qce->cvar.Get ("cl_consoleFontKerning", "0", CVAR_ARCHIVE);

  cl_serverStatusResendTime = qce->cvar.Get ("cl_serverStatusResendTime", "750", 0);

  cl_maxPing = qce->cvar.Get ("cl_maxPing", "800", CVAR_ARCHIVE);

  cl_lanForcePackets = qce->cvar.Get ("cl_lanForcePackets", "1", CVAR_ARCHIVE);

  cl_buildscript = qce->cvar.Get ("cl_buildscript", "0", 0);
  cl_timescale = qce->cvar.Get ("timescale", "1", CVAR_SYSTEMINFO | CVAR_ROM); // communcated by sysinfo

  cl_running = qce->cvar.Get ("cl_running", "0", CVAR_ROM);
  cl_developer = qce->cvar.Get ("developer", "0", CVAR_TEMP);

  cl_speeds = qce->cvar.Get ("cl_speeds", "0", 0);
  cl_dropsim = qce->cvar.Get ("cl_dropsim", "0", CVAR_CHEAT);
  cl_fixedtime = qce->cvar.Get ("cl_fixedtime", "0", CVAR_CHEAT);
  cl_maxfps = qce->cvar.Get ("cl_maxfps", "125", CVAR_ARCHIVE);
  qce->cvar.CheckRange (cl_maxfps, 10, 1000, true);

  cl_waste_cputime = qce->cvar.Get ("cl_waste_cputime", "0", CVAR_ARCHIVE);

  // "netcode" tunning
  dev_nc_wrapnextpackettime = qce->cvar.Get ("dev_nc_wrapnextpackettime", "0", CVAR_ARCHIVE);
  dev_nc_wraptimedelta  = qce->cvar.Get ("dev_nc_wraptimedelta", "0", CVAR_ARCHIVE);
  dev_nc_addtimedelta  = qce->cvar.Get ("dev_nc_addtimedelta", "1", CVAR_ARCHIVE);
  dev_nc_window_max = qce->cvar.Get ("dev_nc_window_max", "6", CVAR_ARCHIVE);
  dev_nc_window_min = qce->cvar.Get ("dev_nc_window_min", "3", CVAR_ARCHIVE);

  // userinfo
  qce->cvar.Get ("rate", "25000", CVAR_USERINFO | CVAR_ARCHIVE);
  qce->cvar.Get ("snaps", "50", CVAR_USERINFO | CVAR_ARCHIVE);
  qce->cvar.Get ("password", "", CVAR_USERINFO);
  // move this to cgame ?
  qce->cvar.Get ("name", "UnnamedPlayer", CVAR_USERINFO | CVAR_ARCHIVE);

  //
  // register our commands
  //
  qce->cmd.AddCommand ("cmd", CL_ForwardToServer_f);
  qce->cmd.AddCommand ("configstrings", CL_Configstrings_f);
  qce->cmd.AddCommand ("clientinfo", CL_Clientinfo_f);
  qce->cmd.AddCommand ("snd_restart", CL_Snd_Restart_f);
  qce->cmd.AddCommand ("vid_restart", CL_Vid_Restart_f);
  qce->cmd.AddCommand ("disconnect", CL_Disconnect_f);
  qce->cmd.AddCommand ("record", CL_Record_f);
  qce->cmd.AddCommand ("stoprecord", CL_StopRecord_f);
  qce->cmd.AddCommand ("demo", CL_PlayDemo_f);
  qce->cmd.SetCommandCompletionFunc ("demo", CL_CompleteDemoName);
  qce->cmd.AddCommand ("demopause", CL_DemoPause_f);
  qce->cmd.AddCommand ("demojump", CL_DemoJump_f);
  //qce->cmd.AddCommand ("cinematic", CL_PlayCinematic_f);
  qce->cmd.AddCommand ("connect", CL_Connect_f);
  qce->cmd.AddCommand ("reconnect", CL_Reconnect_f);
  qce->cmd.AddCommand ("rcon", CL_Rcon_f);
  qce->cmd.SetCommandCompletionFunc ("rcon", CL_CompleteRcon);
  qce->cmd.AddCommand ("showip", CL_ShowIP_f);
  qce->cmd.AddCommand ("fs_openedList", CL_OpenedPK3List_f);
  qce->cmd.AddCommand ("fs_referencedList", CL_ReferencedPK3List_f);
  // move this to cgame
  //qce->cmd.AddCommand ("model", CL_SetModel_f);
  qce->cmd.AddCommand ("dev_hk_system", dev_HACK_system_f);
  qce->cmd.AddCommand ("dev_180hax", CL_180Hax_f);
  /* bu add */
  qce->cmd.AddCommand ("+vstr", Cmd_PVstr_f);
  qce->cmd.AddCommand ("-vstr", Cmd_PVstr_f);
  /* bu add end */
  qce->cmd.AddCommand ("list_colors", CL_DrawColorList_f);
  qce->cmd.AddCommand ("list_playermodels", CL_ListPlayerModels_f);

  qce->cmd.AddCommand ("quit", CL_Quit_f);

  //Cbuf_Execute ();

  qce->cvar.SetNew (cl_running, "1");

  CL_Printf ("----- Client Initialization Complete -----\n");
}

/*
===============
CL_Shutdown

use soft mode when the client in intended to restart some work the same session
===============
*/
void
CL_Shutdown (const char * finalmsg, bool closesession)
{
  static bool recursive = false;

  // check whether the client is running at all.
  if (!(cl_running && cl_running->integer))
    return;

  CL_Printf ("----- CL_Shutdown (%s) -----\n", finalmsg);

  if (recursive)
    {
      CL_Printf ("WARNING: Recursive shutdown\n");
      return;
    }
  recursive = true;

  CL_Disconnect (true);

  // shutdown the CGame
  if (cls.cgameStarted)
    {
      CL_ShutdownCGame ();
      cls.cgameStarted = false;
    }

  // sound
  if (closesession)
    {
      CL_ShutdownSound ();
      CL_UnloadSoundLib ();
    }
  else
    {
      CL_Snd_DisableSounds ();
    }

  // renderer
  if (closesession)
    CL_ShutdownRef (true);
  else
    CL_ShutdownRef (false);

  // inputs
  if (closesession)
    {
      CL_CloseInput ();
      CL_ShutdownInput ();
      CL_UnloadInputLib ();
      CL_UnloadRefLib ();
    }

  // we plan calling starthunkusers again
  if (closesession)
    {
      qce->cmd.RemoveCommand ("cmd");
      qce->cmd.RemoveCommand ("configstrings");
      qce->cmd.RemoveCommand ("userinfo");
      qce->cmd.RemoveCommand ("snd_restart");
      qce->cmd.RemoveCommand ("vid_restart");
      qce->cmd.RemoveCommand ("disconnect");
      qce->cmd.RemoveCommand ("record");
      qce->cmd.RemoveCommand ("stoprecord");
      qce->cmd.RemoveCommand ("demo");
      qce->cmd.RemoveCommand ("connect");
      qce->cmd.RemoveCommand ("rcon");
      qce->cmd.RemoveCommand ("showip");
      qce->cmd.RemoveCommand ("model");
    }

  if (closesession)
    qce->cvar.SetNew (cl_running, "0");

  if (closesession)
    {
      Com_Memset (&cls, 0, sizeof (cls));
      Key_SetCatcher (0);
    }
  else
    {
      Key_SetCatcher (KEYCATCH_CONSOLE);
    }

  CL_Printf ("-----------------------\n");
  recursive = false;
}

//============================================================================

jmp_buf *
CL_GetJmpFrame (void)
{
  return &abortframe;
}

pg_status
CL_ProgramStatus (void)
{
  if (cl_running && cl_running->integer)
    return running;
  else
    return sleeping;
}

void
CL_Shutdown_Export (const char * msg)
{
  CL_Shutdown (msg, false);
}

bool
CL_QcomStatus (void)
{
  return cls.qcomLoaded;
}

void
CL_FillQcomLibAPI (qcom_import_t * qcimp)
{
  qcimp->shutdown = CL_Shutdown_Export;
  qcimp->gamecommand = CL_GameCommand; // used by Cmd_ExecuteString ();
  qcimp->get_jmpframe = CL_GetJmpFrame;
  qcimp->get_program_status = CL_ProgramStatus;
  qcimp->CL_InitKeyCommands = CL_InitKeyCommands;
  qcimp->CL_ForwardCommandToServer = CL_ForwardCommandToServer;
  qcimp->Key_KeynameCompletion = Key_KeynameCompletion;
  qcimp->Key_WriteBindings = Key_WriteBindings;

  qcimp->Printf = CL_Printf;
  qcimp->DPrintf = CL_DPrintf;
  qcimp->Error = CL_Fatal_Error;

  qcimp->Sys_DefaultHomePath = Sys_DefaultHomePath;
  qcimp->Sys_DefaultInstallPath = Sys_DefaultInstallPath;
  qcimp->Sys_DefaultAppPath = Sys_DefaultAppPath;

  qcimp->Sys_Milliseconds = Sys_Milliseconds;
  qcimp->Sys_RandomBytes = Sys_RandomBytes;

  qcimp->Sys_Mkdir = Sys_Mkdir;
  qcimp->Sys_Mkfifo = Sys_Mkfifo;

  qcimp->Sys_ConsoleInput = Sys_ConsoleInput;

  qcimp->Sys_ListFiles = Sys_ListFiles;
  qcimp->Sys_FreeFileList = Sys_FreeFileList;

  qcimp->Sys_SetEnv = Sys_SetEnv;
}


void
CL_LoadCommonLib (void)
{
  void *Hdl;
  qcom_export_t *(*QcomAPI) (qcom_import_t * qimp, int isdedicated);

  // free the structure
  Com_Memset (&qclib, 0, sizeof (qclib));

  // load the dynamic library
  qclib.dynHandle = Sys_DLLoadType ("libq3common", qclib.fqpath, CLIENT);

  // resolve export/import structures
  Hdl = qclib.dynHandle;

  QcomAPI = Sys_DLLoadFunction (Hdl, "CommonAPI", true);

  CL_FillQcomLibAPI (&qimp);

  qce = QcomAPI (&qimp, QCFLAG_CLIENT);

  // stop running the programm and send a proper error message
  if (qce == NULL)
    {
      CL_Error (ERR_FATAL, "SV_LoadCommonLib (): failed to get a valid qce pointer\n");
    }
}

void
CL_UnloadCommonLib (void)
{
  if (qclib.dynHandle == NULL)
    return;

  CL_Printf ("Unloading Common Lib\n");
  Sys_DLUnload (qclib.dynHandle);

  qce = NULL;
  Com_Memset (&qimp, 0, sizeof (qimp));
}

/*
==========================
MAIN LOOP FUNCTIONS

EventLoop
CL_Frame
main
==========================
*/
/*
=================
CL_EventLoop

Returns last event time
=================
*/
int
CL_EventLoop (void)
{
  sysEvent_t ev;
  netadr_t evFrom;
  byte bufData[MAX_MSGLEN];
  msg_t buf;

  qce->msg.Init (&buf, bufData, sizeof (bufData));

  while (1)
    {
      qce->net.FlushPacketQueue ();
      ev = qce->event.GetEvent ();
      /*
      // if no more events are available
      if (ev.evType == SE_NONE)
	{
	  // manually get packet events for the loopback channel
	  // (from this program to himself)
	  while (qce->net.GetLoopPacket (NS_CLIENT, &evFrom, &buf))
	    {
	      CL_PacketEvent (evFrom, &buf);
	    }
	  return ev.evTime;
	}
      */
      switch (ev.evType)
	{
	default:
	  CL_Error (ERR_FATAL, "CL_EventLoop: bad event type %i", ev.evType);
	  break;
	case SE_NONE:
	  return ev.evTime;
	  break;
	case SE_KEY:
	  CL_KeyEvent (ev.evValue, ev.evValue2, ev.evTime);
	  break;
	case SE_CHAR:
	  CL_CharEvent ((const char *)ev.evPtr);
	  break;
	case SE_MOUSE:
	  CL_MouseEvent (ev.evValue, ev.evValue2, ev.evTime);
	  break;
	case SE_CONSOLE:
	  qce->cmd.Cbuf_AddText ((char *)ev.evPtr);
	  qce->cmd.Cbuf_AddText ("\n");
	  break;
	case SE_PACKET:
	  // this cvar allows simulation of connections that
	  // drop a lot of packets.  Note that loopback connections
	  // don't go through here at all.
	  if (cl_dropsim->value > 0)
	    {
	      static int seed;

	      if (Q_random (&seed) < cl_dropsim->value)
		{
		  break;	// drop this packet
		}
	    }

	  evFrom = *(netadr_t *)ev.evPtr;
	  buf.cursize = ev.evPtrLength - sizeof (evFrom);

	  // we must copy the contents of the message out, because
	  // the event buffers are only large enough to hold the
	  // exact payload, but channel messages need to be large
	  // enough to hold fragment reassembly
	  if ((unsigned)buf.cursize > buf.maxsize)
	    {
	      CL_Printf ("CL_EventLoop: oversize packet\n");
	      continue;
	    }
	  Com_Memcpy (buf.data, (byte *)((netadr_t *)ev.evPtr + 1), buf.cursize);

	  // keep track of ev.evTime for network calculations?
	  CL_PacketEvent (evFrom, &buf, ev.evTime);
	  break;
	}

      // free any block data
      qce->event.FreeEvent (&ev);
    }

  // never reached
  CL_Error (ERR_FATAL, "SV_EventLoop ()\n");
  return 0;
}

/*
=================
main
=================
*/
int
main (int argc, char **argv)
{
  int i;
  char commandLine[MAX_STRING_CHARS] = { 0 };

  sys_import_t sysimp;

  sysimp.print = CL_Printf;
  sysimp.dprint = CL_DPrintf;
  sysimp.error = CL_Error;
  sysimp.shutdown = CL_Shutdown_Export;
  sysimp.isQcomLoaded = CL_QcomStatus;

  Sys_Set_Interface (&sysimp, true);

  // setup the print/error functions for qshared
  q_shared_import_t q_shared_imp;
  q_shared_imp.print = CL_Printf;
  q_shared_imp.error = CL_Fatal_Error;

  Q_Set_Interface (&q_shared_imp);

  Sys_PlatformInit ();

  // Set the initial time base
  Sys_Milliseconds ();

  // check for some arguments and set binary path
  Sys_ArgsInit (argc, argv);

  // Concatenate the command line for passing to Com_Init
  for (i = 1; i < argc; i++)
    {
      const bool containsSpaces = strchr (argv[i], ' ') != NULL;
      if (containsSpaces)
	Q_strcat (commandLine, sizeof (commandLine), "\"");

      Q_strcat (commandLine, sizeof(commandLine), argv[i]);

      if (containsSpaces)
	Q_strcat (commandLine, sizeof (commandLine), "\"");

      Q_strcat (commandLine, sizeof(commandLine), " ");
    }

  // load qcommon module
  CL_LoadCommonLib ();

  qce->Com_Init (commandLine);

  // using qce->... functions is valid from now
  cls.qcomLoaded = true;

  // not sure whether i can put this one late
  // set com_frameTime so that if a map is started on the
  // command line it will still be able to count on com_frameTime
  // being random enough for a serverid
  cl_frameTime = qce->event.Milliseconds ();

  // client related things to init; cvars, cmds etc...
  CL_Init ();

  // non critical cvar about system (better here than in qcommon)
  Sys_Init (); // informative cvars

  if (Sys_WritePIDFile ())
    {
      const char *message = "The last time " CLIENT_WINDOW_TITLE " ran, "
	"it didn't exit properly. This may be due to inappropriate video "
	"settings.";

      Sys_Error ("%s", message);
    }

  qce->net.Init (); // start networking
  //CON_Init (); // client console subsystem (not needed here)

  // loading renderer lib
  CL_InitRefLib ();

  // loading inputs lib
  CL_InitInputLib ();

  CL_InitRenderer ();
  cls.rendererStarted = true;

  inpe->Init ();
  cls.inputStarted = true;

  // loading sound lib
  // prepare this for the future to be able to run the game without sound
  cls.soundStarted = CL_InitSoundLib ();

  CL_StartHunkUsers (false);

  // print a welcome/informative message to the user
  CL_PrintWelcomeMessage ();

  while (1)
    {
      CL_Frame ();
    }
  return 0;
}

/*
=============
CL_Printf

Both client and server can use this, and it will output
to the apropriate place.

A raw string should NEVER be passed as fmt, because of "%f" type crashers.
=============
*/
void
CL_Printf (const char *fmt, ...)
{
  va_list argptr;
  char msg[MAXPRINTMSG];

  va_start (argptr,fmt);
  Q_vsnprintf (msg, sizeof(msg), fmt, argptr);
  va_end (argptr);

  // echo to dedicated console and terminal
  Sys_Print (msg);

  // echo in the ingame console
  CL_ConsolePrint (msg);

  // archive in a log file if lib is loaded (no early print)
  if (cls.qcomLoaded)
    qce->Com_PrintLog (msg);
}

/*
================
CL_DPrintf

A CL_Printf that only shows up if the "developer" cvar is set
================
*/
void
CL_DPrintf (const char *fmt, ...)
{
  va_list argptr;
  char msg[MAXPRINTMSG];

  if (!cl_developer || !cl_developer->integer)
    {
      return;		// don't confuse non-developers with techie stuff...
    }

  va_start (argptr,fmt);
  Q_vsnprintf (msg, sizeof(msg), fmt, argptr);
  va_end (argptr);

  CL_Printf ("%s", msg);
}

/*
=============
CL_Error

Both client and server can use this, and it will
do the apropriate things.
=============
*/
void
CL_Error (int code, const char *fmt, ...)
{
  va_list argptr;
  static int lastErrorTime;
  static int errorCount;
  int currentTime;

  // when we are running automated scripts, make sure we
  // know if anything failed
  if (cl_buildscript && cl_buildscript->integer)
    {
      code = ERR_FATAL;
    }

  // if we are getting a solid stream of ERR_DROP, do an ERR_FATAL
  currentTime = Sys_Milliseconds ();
  if (currentTime - lastErrorTime < 100)
    {
      if (++errorCount > 3)
	{
	  code = ERR_FATAL;
	}
    }
  else
    {
      errorCount = 0;
    }
  lastErrorTime = currentTime;

  if (cl_errorEntered)
    {
      Sys_Error ("recursive error after: %s", cl_errorMessage);
    }
  cl_errorEntered = true;

  va_start (argptr,fmt);
  Q_vsnprintf (cl_errorMessage, sizeof(cl_errorMessage), fmt, argptr);
  va_end (argptr);

  if (code == ERR_DISCONNECT || code == ERR_SERVERDISCONNECT)
    {
      CL_Shutdown ("Server disconnected", false);
      CL_FlushMemory ();

      // make sure we can get at our local stuff
      // go back to cl_frame ()
      cl_errorEntered = false;
      // should handle this case properly in another block because it can happend
      // during a frame drawing
      if (code == ERR_SERVERDISCONNECT)
	{
	  scr_recursive = 0;
	}

      longjmp (abortframe, -1);
    }
  else if (code == ERR_DROP)
    {
      CL_Printf ("********************\nERROR: %s\n********************\n", cl_errorMessage);
      CL_Shutdown (va("Client crashed: %s",  cl_errorMessage), false);
      CL_FlushMemory ();

      // go back to sv_frame ()
      cl_errorEntered = false;
      longjmp (abortframe, -1);
    }
  else // err_fatal, end the executable
    {
      CL_Shutdown (va("Client fatal crashed: %s", cl_errorMessage), true);
      if (cls.qcomLoaded)
	{
	  qce->Com_Shutdown ();
	  qce->fs.Shutdown (true);
	  //CL_UnloadCommonLib ();
	}
      Sys_Quit ();
    }

  Sys_Error ("%s", cl_errorMessage);
}

// Theses Two are here for compatibility with q_shared.c
void CL_Fatal_Error (int level, const char *error, ...)
{
  va_list argptr;
  char text[1024];

  va_start (argptr, error);
  Q_vsnprintf (text, sizeof(text), error, argptr);
  va_end (argptr);

  CL_Error (ERR_FATAL, "QShared - lvl %i |%s\n", level, text);
}

/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "client.h"

/*
===================
dev_Tools_ColorFromStringHex

hexadecimal 8 characters string to 4 * char stored in vec4_t
===================
*/
void
dev_Tools_ColorFromStringHex (vec4_t hcolor, char *string)
{
  char *ptr;
  int i;
  int res = 0;
  char value = 0;

  ptr = string;

  hcolor[0] = hcolor[1] = hcolor[2] = hcolor[3] = 1;

  for (i = 0; i < 4; i++, ptr += 2)
    {
      if (!*ptr || !*(ptr + 1))
	{
	  break;
	}
      else
	{
	  res = c16to10[(int) (*ptr)];
	  if (res < 0)
	    break;
	  value <<= 4;
	  value += res;
	  res = c16to10[(int) (*(ptr + 1))];
	  if (res < 0)
	    break;
	  value <<= 4;
	  value += res;

	  hcolor[i] = (double) (value) / (double) 255;
	}
    }
}

/*
===================
CL_ListPlayerModels_f

List all available player models that can be loaded
===================
*/
#define NUMBER_DIRLIST 2048
void
CL_ListPlayerModels_f (void)
{
  int numdirs;
  int numfiles;
  char dirlist[NUMBER_DIRLIST];
  char filelist[NUMBER_DIRLIST];
  char skinname[MAX_QPATH];
  char *dirptr;
  char *fileptr;
  int i, j;
  int dirlen, filelen;
  bool firstprint;

  CL_Printf (S_COLOR_GREEN "Available player models (and associated skins)"
	     S_COLOR_WHITE ":\n");

  // grab the directories (thus models) we have in the repertory
  numdirs = qce->fs.GetFileList ("models/players", "/", dirlist, sizeof (dirlist));

  dirptr = dirlist;
  for (i = 0; i < numdirs; i++, dirptr += dirlen + 1)
    {
      dirlen = strlen (dirptr);

      if (dirlen && dirptr[dirlen - 1] == '/')
	dirptr[dirlen - 1] = '\0';

      // we want to skip the current directory and go through the subdirectories
      if (!strcmp (dirptr, ".") || !strcmp (dirptr, "..") || !strcmp (dirptr, ""))
	continue;

      // print the model (directory) name
      CL_Printf ("%s", dirptr);

      // grab a list of skin files for this model
      numfiles = qce->fs.GetFileList (va("models/players/%s", dirptr), "skin",
				      filelist, sizeof (filelist));

      // print a list of skins for this model
      CL_Printf (" (");
      firstprint = true;

      fileptr = filelist;
      for (j = 0; j < numfiles; j++, fileptr += filelen + 1)
	{
	  filelen = strlen (fileptr);

	  // assume that if we have upper_ we already have head_ and lower_
	  if (!strncmp (fileptr, "upper_", 6))
	    {
	      char *skip = strchr (fileptr, '_');
	      skip++;

	      COM_StripExtension (skip, skinname, sizeof (skinname));

	      if (firstprint)
		{
		  CL_Printf ("%s", skinname);
		  firstprint = false;
		}
	      else
		{
		  CL_Printf (" %s", skinname);
		}
	    }
	}

      CL_Printf (")\n");
    }
}

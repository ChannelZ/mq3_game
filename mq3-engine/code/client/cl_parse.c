/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// cl_parse.c  -- parse a message received from the server

#include "client.h"

int cl_connectedToCheatServer;

char * svc_strings[] =
{
  "svc_bad",

  "svc_nop",
  "svc_gamestate",
  "svc_configstring",
  "svc_baseline",
  "svc_serverCommand",
  "svc_snapshot",
  "svc_EOF",
};

void
SHOWNET (msg_t * msg, char *s)
{
  if (cl_shownet->integer >= 2)
    {
      CL_Printf ("%3i:%s\n", msg->readcount - 1, s);
    }
}

/*
=========================================================================

MESSAGE PARSING

=========================================================================
*/

/*
==================
CL_DeltaEntity

Parses deltas from the given base and adds the resulting entity
to the current frame
==================
*/
void
CL_DeltaEntity (msg_t * msg, clSnapshot_t * frame, int newnum,
		entityState_t * old, bool unchanged)
{
  entityState_t *state;

  // save the parsed entity state into the big circular buffer so
  // it can be used as the source for a later delta
  state = &cl.parseEntities[cl.parseEntitiesNum & (MAX_PARSE_ENTITIES - 1)];

  if (unchanged)
    {
      *state = *old;
    }
  else
    {
      qce->msg.ReadDeltaEntity (msg, old, state, newnum);
    }

  if (state->number == (MAX_GENTITIES - 1))
    {
      return;		// entity was delta removed
    }
  cl.parseEntitiesNum++;
  frame->numEntities++;
}

/*
==================
CL_ParsePacketEntities

==================
*/
void
CL_ParsePacketEntities (msg_t * msg, clSnapshot_t * oldframe,
			clSnapshot_t * newframe)
{
  int newnum;
  entityState_t *oldstate;
  int oldindex, oldnum;

  newframe->parseEntitiesNum = cl.parseEntitiesNum;
  newframe->numEntities = 0;

  // delta from the entities present in oldframe
  oldindex = 0;
  oldstate = NULL;
  if (!oldframe)
    {
      oldnum = 99999;
    }
  else
    {
      if (oldframe->numEntities <= 0)
	{
	  oldnum = 99999;
	}
      else
	{
	  oldstate = &cl.parseEntities[(oldframe->parseEntitiesNum)
				       & (MAX_PARSE_ENTITIES - 1)];
	  oldnum = oldstate->number;
	}
    }

  while (1)
    {
      // read the entity index number
      newnum = qce->msg.ReadBits (msg, GENTITYNUM_BITS);

      if (newnum == (MAX_GENTITIES - 1))
	{
	  break;
	}

      if (msg->readcount > msg->cursize)
	{
	  CL_Error (ERR_DROP, "CL_ParsePacketEntities: end of message");
	}

      while (oldnum < newnum)
	{
	  // one or more entities from the old packet are unchanged
	  if (cl_shownet->integer == 3)
	    {
	      CL_Printf ("%3i:  unchanged: %i\n", msg->readcount, oldnum);
	    }
	  CL_DeltaEntity (msg, newframe, oldnum, oldstate, true);

	  oldindex++;

	  if (oldindex >= oldframe->numEntities)
	    {
	      oldnum = 99999;
	    }
	  else
	    {
	      oldstate = &cl.parseEntities[(oldframe->parseEntitiesNum + oldindex)
					   & (MAX_PARSE_ENTITIES - 1)];
	      oldnum = oldstate->number;
	    }
	}

      if (oldnum == newnum)
	{
	  // delta from previous state
	  if (cl_shownet->integer == 3)
	    {
	      CL_Printf ("%3i:  delta: %i\n", msg->readcount, newnum);
	    }
	  CL_DeltaEntity (msg, newframe, newnum, oldstate, false);

	  oldindex++;

	  if (oldindex >= oldframe->numEntities)
	    {
	      oldnum = 99999;
	    }
	  else
	    {
	      oldstate = &cl.parseEntities[(oldframe->parseEntitiesNum + oldindex)
					   & (MAX_PARSE_ENTITIES - 1)];
	      oldnum = oldstate->number;
	    }
	  continue;
	}

      if (oldnum > newnum)
	{
	  // delta from baseline
	  if (cl_shownet->integer == 3)
	    {
	      CL_Printf ("%3i:  baseline: %i\n", msg->readcount, newnum);
	    }
	  CL_DeltaEntity (msg, newframe, newnum, &cl.entityBaselines[newnum], false);
	  continue;
	}
    }

  // any remaining entities in the old frame are copied over
  while (oldnum != 99999)
    {
      // one or more entities from the old packet are unchanged
      if (cl_shownet->integer == 3)
	{
	  CL_Printf ("%3i:  unchanged: %i\n", msg->readcount, oldnum);
	}
      CL_DeltaEntity (msg, newframe, oldnum, oldstate, true);

      oldindex++;

      if (oldindex >= oldframe->numEntities)
	{
	  oldnum = 99999;
	}
      else
	{
	  oldstate = &cl.parseEntities[(oldframe->parseEntitiesNum + oldindex)
				       & (MAX_PARSE_ENTITIES - 1)];
	  oldnum = oldstate->number;
	}
    }
}

/*
================
CL_ParseSnapshot

If the snapshot is parsed properly, it will be copied to
cl.snap and saved in cl.snapshots[].  If the snapshot is invalid
for any reason, no changes to the state will be made at all.
================
*/
void
CL_ParseSnapshot (msg_t * msg, int time)
{
  int len;
  clSnapshot_t *old;
  clSnapshot_t newSnap;
  int deltaNum;
  int oldMessageNum;
  int i, packetNum;

  // read in the new snapshot to a temporary buffer
  // we will only copy to cl.snap if it is valid
  Com_Memset (&newSnap, 0, sizeof (newSnap));

  // we will have read any new server commands in this
  // message before we got to svc_snapshot
  newSnap.serverCommandNum = clc.serverCommandSequence;

  newSnap.serverTime = qce->msg.ReadLong (msg);

  newSnap.simulationTime = qce->msg.ReadLong (msg);

  newSnap.messageNum = clc.serverMessageSequence;

  deltaNum = qce->msg.ReadByte (msg);
  if (!deltaNum)
    {
      newSnap.deltaNum = -1;
    }
  else
    {
      newSnap.deltaNum = newSnap.messageNum - deltaNum;
    }
  newSnap.snapFlags = qce->msg.ReadByte (msg);

  // If the frame is delta compressed from data that we
  // no longer have available, we must suck up the rest of
  // the frame, but not use it, then ask for a non-compressed
  // message 
  if (newSnap.deltaNum <= 0)
    {
      newSnap.valid = true;	// uncompressed frame
      old = NULL;
      clc.demowaiting = false;	// we can start recording now
    }
  else
    {
      old = &cl.snapshots[newSnap.deltaNum & PACKET_MASK];
      if (!old->valid)
	{
	  // should never happen
	  CL_Printf
	    ("Delta from invalid frame (not supposed to happen!).\n");
	}
      else if (old->messageNum != newSnap.deltaNum)
	{
	  // The frame that the server did the delta from
	  // is too old, so we can't reconstruct it properly.
	  CL_Printf ("Delta frame too old.\n");
	}
      else if ((cl.parseEntitiesNum - old->parseEntitiesNum) >
	       (MAX_PARSE_ENTITIES - 128))
	{
	  CL_Printf ("Delta parseEntitiesNum too old.\n");
	}
      else
	{
	  newSnap.valid = true;	// valid delta parse
	}
    }

  // read areamask
  len = qce->msg.ReadByte (msg);

  if (len > sizeof (newSnap.areamask))
    {
      CL_Error (ERR_DROP, "CL_ParseSnapshot: Invalid size %d for areamask.", len);
      return;
    }

  qce->msg.ReadData (msg, &newSnap.areamask, len);

  // read playerinfo
  SHOWNET (msg, "playerstate");
  if (old)
    {
      qce->msg.ReadDeltaPlayerstate (msg, &old->ps, &newSnap.ps);
    }
  else
    {
      qce->msg.ReadDeltaPlayerstate (msg, NULL, &newSnap.ps);
    }

  // read packet entities
  SHOWNET (msg, "packet entities");
  CL_ParsePacketEntities (msg, old, &newSnap);

  // if not valid, dump the entire thing now that it has
  // been properly read
  if (!newSnap.valid)
    {
      return;
    }

  // clear the valid flags of any snapshots between the last
  // received and this one, so if there was a dropped packet
  // it won't look like something valid to delta from next
  // time we wrap around in the buffer
  oldMessageNum = cl.snap.messageNum + 1;

  if (newSnap.messageNum - oldMessageNum >= PACKET_BACKUP)
    {
      oldMessageNum = newSnap.messageNum - (PACKET_BACKUP - 1);
    }
  for (; oldMessageNum < newSnap.messageNum; oldMessageNum++)
    {
      cl.snapshots[oldMessageNum & PACKET_MASK].valid = false;
    }

  // copy to the current good spot
  cl.snap = newSnap;
  cl.snap.ping = 999;
  cl.snap.receivedTime = time;
  // calculate ping time
  for (i = 0; i < PACKET_BACKUP; i++)
    {
      packetNum = (clc.netchan.outgoingSequence - 1 - i) & PACKET_MASK;
      if (cl.snap.ps.commandTime >= cl.outPackets[packetNum].p_serverTime)
	{
	  cl.snap.ping = cls.realtime - cl.outPackets[packetNum].p_realtime;
	  break;
	}
    }
  // save the frame off in the backup array for later delta comparisons
  cl.snapshots[cl.snap.messageNum & PACKET_MASK] = cl.snap;

  if (cl_shownet->integer == 3)
    {
      CL_Printf ("   snapshot:%i  delta:%i  ping:%i  time:%i\n", cl.snap.messageNum,
		 cl.snap.deltaNum, cl.snap.ping, cl.snap.receivedTime);
    }

  cl.newSnapshots = true;
}

//=====================================================================

/*
==================
CL_SystemInfoChanged

The systeminfo configstring has been changed, so parse
new information out of it.  This will happen at every
gamestate, and possibly during gameplay.
==================
*/
void
CL_SystemInfoChanged (void)
{
  char *systemInfo;
  const char *s;
  char key[BIG_INFO_KEY];
  char value[BIG_INFO_VALUE];
  bool gameSet;

  systemInfo = cl.gameState.stringData + cl.gameState.stringOffsets[CS_SYSTEMINFO];
  // NOTE TTimo:
  // when the serverId changes, any further messages we send to the server
  // will use this new serverId
  // in some cases, outdated cp commands might get sent with this news serverId
  cl.serverId = atoi (Info_ValueForKey (systemInfo, "sv_serverid"));

  // don't set any vars when playing a demo
  if (clc.demoplaying)
    {
      return;
    }

  s = Info_ValueForKey (systemInfo, "sv_cheats");
  cl_connectedToCheatServer = atoi (s);
  if (!cl_connectedToCheatServer)
    {
      qce->cvar.SetCheatState ();
    }

  /*
  // check pure server string
  s = Info_ValueForKey (systemInfo, "sv_paks");
  t = Info_ValueForKey (systemInfo, "sv_pakNames");

  s = Info_ValueForKey (systemInfo, "sv_referencedPaks");
  t = Info_ValueForKey (systemInfo, "sv_referencedPakNames");
  */

  gameSet = false;
  // scan through all the variables in the systeminfo and locally set cvars to match
  s = systemInfo;
  while (s)
    {
      int cvar_flags;

      Info_NextPair (&s, key, value);
      if (!key[0])
	{
	  break;
	}

      // ehw!
      if (!Q_stricmp (key, "fs_game"))
	{
	  if (qce->fs.CheckDirTraversal (value))
	    {
	      CL_Printf (S_COLOR_YELLOW
			 "WARNING: Server sent invalid fs_game value %s\n",value);
	      continue;
	    }

	  gameSet = true;
	}

      cvar_flags = qce->cvar.Flags (key);
      if (cvar_flags == CVAR_NONEXISTENT)
	{
	  qce->cvar.Get (key, value, CVAR_SERVER_CREATED | CVAR_ROM);
	}
      else
	{
	  // If this cvar may not be modified by a server discard the value.
	  if (!(cvar_flags & (CVAR_SYSTEMINFO | CVAR_SERVER_CREATED)))
	    {
	      CL_Printf (S_COLOR_YELLOW
			  "WARNING: server is not allowed to set %s=%s\n",
			  key, value);
	      continue;
	    }

	  qce->cvar.Set (key, value);
	}
    }
  // if game folder should not be set and it is set at the client side
  if (!gameSet && *qce->cvar.VariableString ("fs_game"))
    {
      qce->cvar.Set ("fs_game", "");
    }
}

/*
==================
CL_ParseServerInfo
==================
*/
static void
CL_ParseServerInfo (void)
{
  const char *serverInfo;

  serverInfo = cl.gameState.stringData + cl.gameState.stringOffsets[CS_SERVERINFO];

  // nothing to do here
}

/*
==================
CL_ParseGamestate
==================
*/
void
CL_ParseGamestate (msg_t * msg)
{
  int i;
  entityState_t *es;
  int newnum;
  entityState_t nullstate;
  int cmd;
  char *s;

  // HACK
  // reconnect to the serv on map change
  // this avoid receiving outdated or too early packets the
  // client might not treat the correct way, putting itself
  // in an inconsistent state
  // TODO : upgrade the process, make a way for the server
  // to let the clients know it will(has) change(d) map
  if (cls.state == CA_ACTIVE)
    {
      qce->cmd.Cbuf_AddText ("reconnect");
      return;
    }

  Con_Close ();

  clc.connectPacketCount = 0;

  // wipe local client state
  CL_ClearState ();

  // a gamestate always marks a server command sequence
  clc.serverCommandSequence = qce->msg.ReadLong (msg);

  // parse all the configstrings and baselines
  cl.gameState.dataCount = 1;	// leave a 0 at the beginning for uninitialized configstrings
  while (1)
    {
      cmd = qce->msg.ReadByte (msg);

      if (cmd == svc_EOF)
	{
	  break;
	}

      if (cmd == svc_configstring)
	{
	  int len;

	  i = qce->msg.ReadShort (msg);
	  if (i < 0 || i >= MAX_CONFIGSTRINGS)
	    {
	      CL_Error (ERR_DROP, "configstring > MAX_CONFIGSTRINGS");
	    }
	  s = qce->msg.ReadBigString (msg);
	  len = strlen (s);

	  if (len + 1 + cl.gameState.dataCount > MAX_GAMESTATE_CHARS)
	    {
	      CL_Error (ERR_DROP, "MAX_GAMESTATE_CHARS exceeded");
	    }

	  // append it to the gameState string buffer
	  cl.gameState.stringOffsets[i] = cl.gameState.dataCount;
	  Com_Memcpy (cl.gameState.stringData + cl.gameState.dataCount, s, len + 1);
	  cl.gameState.dataCount += len + 1;
	}
      else if (cmd == svc_baseline)
	{
	  newnum = qce->msg.ReadBits (msg, GENTITYNUM_BITS);
	  if (newnum < 0 || newnum >= MAX_GENTITIES)
	    {
	      CL_Error (ERR_DROP, "Baseline number out of range: %i", newnum);
	    }
	  Com_Memset (&nullstate, 0, sizeof (nullstate));
	  es = &cl.entityBaselines[newnum];
	  qce->msg.ReadDeltaEntity (msg, &nullstate, es, newnum);
	}
      else
	{
	  CL_Error (ERR_DROP, "CL_ParseGamestate: bad command byte");
	}
    }

  clc.clientNum = qce->msg.ReadLong (msg);
  // read the checksum feed
  clc.checksumFeed = qce->msg.ReadLong (msg);

  // parse useful values out of CS_SERVERINFO
  CL_ParseServerInfo ();

  // parse serverId and other cvars
  CL_SystemInfoChanged ();

  // stop recording now so the demo won't have an unnecessary level load at the end.
  if (cl_autoRecordDemo->integer && clc.demorecording)
    CL_StopRecord_f ();

  // reinitialize the filesystem if the game directory has changed
  qce->fs.ConditionalRestart (clc.checksumFeed);

  // are we ready to load cgame ?
  CL_CheckFilesAvailable ();

  // lets load cgame
  CL_LaunchCGame ();
}

//=====================================================================

/*
=====================
CL_ParseCommandString

Command strings are just saved off until cgame asks for them
when it transitions a snapshot
=====================
*/
void
CL_ParseCommandString (msg_t * msg)
{
  char *s;
  int seq;
  int index;

  seq = qce->msg.ReadLong (msg);
  s = qce->msg.ReadString (msg);

  // see if we have already executed stored it off
  if (clc.serverCommandSequence >= seq)
    {
      return;
    }
  clc.serverCommandSequence = seq;

  index = seq & (MAX_RELIABLE_COMMANDS - 1);
  Q_strncpyz (clc.serverCommands[index], s, sizeof (clc.serverCommands[index]));
}

/*
=====================
CL_ParseServerMessage
=====================
*/
void
CL_ParseServerMessage (msg_t * msg, int time)
{
  int cmd;

  if (cl_shownet->integer == 1)
    {
      CL_Printf ("%i ", msg->cursize);
    }
  else if (cl_shownet->integer >= 2)
    {
      CL_Printf ("------------------\n");
    }

  qce->msg.Bitstream (msg);

  // get the reliable sequence acknowledge number
  clc.reliableAcknowledge = qce->msg.ReadLong (msg);
  // 
  if (clc.reliableAcknowledge < clc.reliableSequence - MAX_RELIABLE_COMMANDS)
    {
      clc.reliableAcknowledge = clc.reliableSequence;
    }

  //
  // parse the message
  //
  while (1)
    {
      if (msg->readcount > msg->cursize)
	{
	  CL_Error (ERR_DROP,
		    "CL_ParseServerMessage: read past end of server message");
	  break;
	}

      cmd = qce->msg.ReadByte (msg);

      if (cmd == svc_EOF)
	{
	  SHOWNET (msg, "END OF MESSAGE");
	  break;
	}

      if (cl_shownet->integer >= 2)
	{
	  if ((cmd < 0) || (!svc_strings[cmd]))
	    {
	      CL_Printf ("%3i:BAD CMD %i\n", msg->readcount - 1, cmd);
	    }
	  else
	    {
	      SHOWNET (msg, svc_strings[cmd]);
	    }
	}

      // other commands
      switch (cmd)
	{
	default:
	  CL_Error (ERR_DROP,
		    "CL_ParseServerMessage: Illegible server message\n");
	  break;
	case svc_nop:
	  break;
	case svc_serverCommand:
	  CL_ParseCommandString (msg);
	  break;
	case svc_gamestate:
	  CL_ParseGamestate (msg);
	  break;
	case svc_snapshot:
	  CL_ParseSnapshot (msg, time);
	  break;
	}
    }
}

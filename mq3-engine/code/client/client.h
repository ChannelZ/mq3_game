/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// client.h -- primary header for client

#include "qcommon/q_shared.h"

#include "qcommon/qcom_extern.h"
#include "qcommon/qcom_public.h"

#include "sys/sys_public_dlib.h"
#include "sys/sys_public_interface.h"

#include "cl_keys.h"
#include "renderer/tr_public.h"
//#include "snd/snd_public.h" // included in cl_cgame_API
#include "input/in_public.h"
#include "cl_cgame_API.h"

#include "qcommon/dynlib_local.h"

#include "qcommon/q_graphics.h"


// global client varialbe
extern int cl_frameTime;	// defined in cl_main

extern int time_frontend;	// renderer frontend time
extern int time_backend;	// renderer backend time

extern int g_console_field_width; // cl_console

#define	RETRANSMIT_TIMEOUT	3000	// time between connection packet retransmits

// snapshots are a view of the server at a given time
struct clSnapshot_s
{
  bool valid;	// cleared if delta parsing was invalid
  int snapFlags;	// rate delayed and dropped commands

  int serverTime;	// server time the message is valid for (in msec)
  int simulationTime;	// game simulation time the message is valid for (in msec)

  int messageNum;	// copied from netchan->incoming_sequence
  int deltaNum;		// messageNum the delta is from
  int ping;		// time from when cmdNum-1 was sent to time packet was reeceived
  byte areamask[MAX_MAP_AREA_BYTES];	// portalarea visibility bits

  int cmdNum;		// the next cmdNum the server is expecting
  playerState_t ps;	// complete information about the current player at this time

  int numEntities;	// all of the entities that need to be presented
  int parseEntitiesNum;	// at the time of this snapshot

  int serverCommandNum;	// execute all commands up to this before
			// making the snapshot current

  // local to the client and not extracted from the server snapshot

  // currently unused !
  int receivedTime;	// keep track of the precise time the snapshot
			// was received by the client
};
typedef struct clSnapshot_s clSnapshot_t;

/*
=============================================================================

the clientActive_t structure is wiped completely at every
new gamestate_t, potentially several times during an established connection

=============================================================================
*/

typedef struct
{
  int p_cmdNumber;	// cl.cmdNumber when packet was sent
  int p_serverTime;	// usercmd->serverTime when packet was sent
  int p_realtime;	// cls.realtime when packet was sent
} outPacket_t;

// the parseEntities array must be large enough to hold PACKET_BACKUP frames of
// entities, so that when a delta compressed message arrives from the server
// it can be un-deltad from the original 
#define	MAX_PARSE_ENTITIES	2048


struct clientActive_s
{
  int timeoutcount;		// it requires several frames in a timeout condition
				// to disconnect, preventing debugging breaks from
				// causing immediate disconnects on continue
  clSnapshot_t snap;		// latest received from server

  int serverTime;		// may be paused during play
  int oldServerTime;		// to prevent time from flowing bakcwards
  int oldFrameServerTime;	// to check tournament restarts
  int serverTimeDelta;		// cl.serverTime = cls.realtime + cl.serverTimeDelta
				// this value changes as net lag varies
  int simulationTime;		// client side view of the game simulation time
  int oldSimulationTime;	// to check tournament restarts and prevent going backward
  int simulationTimeDelta;	// cl.simulationTime = cls.readtime + cl.simulationTimeDelta
				// the value changes with lags and simulationtimenudge

  bool extrapolatedSnapshot;// set if any cgame frame has been forced to extrapolate
				// cleared when CL_AdjustTimeDelta looks at it
  bool newSnapshots;	// set on parse of any valid packet

  gameState_t gameState;	// configstrings
  char mapname[MAX_QPATH];	// extracted from CS_SERVERINFO

  int parseEntitiesNum;		// index (not anded off) into cl_parse_entities[]

  int mouseDx[2], mouseDy[2];	// added to by mouse events
  int mouseIndex;

  // cgame communicates a few values to the client system
  int cgameUserCmdWeapon;	// current weapon to add to usercmd_t
  float cgameSensitivity;

  // cmds[cmdNumber] is the predicted command, [cmdNumber-1] is the last
  // properly generated command
  usercmd_t cmds[CMD_BACKUP];	// each mesage will send several old cmds
  int cmdNumber;		// incremented each frame, because multiple
				// frames may need to be packed into a single packet

  outPacket_t outPackets[PACKET_BACKUP]; // information about each packet we have sent out

  // the client maintains its own idea of view angles, which are
  // sent to the server each frame.  It is cleared to 0 upon entering each level.
  // the server sends a delta each frame which is added to the locally
  // tracked view angles to account for standing on rotating objects,
  // and teleport direction changes
  vec3_t viewangles;

  int serverId;		// included in each client message so the server
			// can tell if it is for a prior map_restart

  // big stuff at end of structure so most offsets are 15 bits or less
  clSnapshot_t snapshots[PACKET_BACKUP];

  // for delta compression when not in previous frame
  entityState_t entityBaselines[MAX_GENTITIES];

  entityState_t parseEntities[MAX_PARSE_ENTITIES];
};
typedef struct clientActive_s clientActive_t;

/*
=============================================================================

the clientConnection_t structure is wiped when disconnecting from a server,
either to go to a full screen console, play a demo, or connect to a different server

A connection can be to either a server through the network layer or a
demo through a file.

=============================================================================
*/
#define MAX_TIMEDEMO_DURATIONS	4096

struct clientConnection_s
{
  int clientNum;
  int lastPacketSentTime;		// for retransmits during connection
  int lastPacketTime;			// for timeouts

  netadr_t serverAddress;
  int connectTime;			// for connection retransmits
  int connectPacketCount;		// for display on connection dialog
  char serverMessage[MAX_STRING_TOKENS];// for display on connection dialog

  int challenge;			// from the server to use for connecting
  int checksumFeed;			// from the server for checksum calculations

  // these are our reliable messages that go to the server
  int reliableSequence;
  int reliableAcknowledge;		// the last one the server has executed
  char reliableCommands[MAX_RELIABLE_COMMANDS][MAX_STRING_CHARS];

  // server message (unreliable) and command (reliable) sequence
  // numbers are NOT cleared at level changes, but continue to
  // increase as long as the connection is valid

  // message sequence is used by both the network layer and the
  // delta compression layer
  int serverMessageSequence;

  // reliable messages received from server
  int serverCommandSequence;
  // last server command grabbed or executed with CL_GetServerCommand
  int lastExecutedServerCommand;
  char serverCommands[MAX_RELIABLE_COMMANDS][MAX_STRING_CHARS];

  // demo information
  char demoName[MAX_QPATH];
  bool demorecording;
  bool demoplaying;
  bool demowaiting;	// don't record until a non-delta message is received
  bool demopaused;
  int demopausetime;
  int demopauselasttime;
  bool firstDemoFrameSkipped;
  fileHandle_t demofile;

  int timeDemoFrames;		// counter of rendered frames
  int timeDemoStart;		// cls.realtime before first frame
  int timeDemoBaseTime;		// each frame will be at this time + frameNum * 50
  int timeDemoLastFrame;	// time the last frame was rendered
  int timeDemoMinDuration;	// minimum frame duration
  int timeDemoMaxDuration;	// maximum frame duration
  unsigned char	timeDemoDurations[MAX_TIMEDEMO_DURATIONS];// log of frame durations

  // big stuff at end of structure so most offsets are 15 bits or less
  netchan_t netchan;
};
typedef struct clientConnection_s clientConnection_t;

/*
==================================================================

the clientStatic_t structure is never wiped, and is used even when
no client connection is active at all

==================================================================
*/

struct clientStatic_s
{
  connstate_t state;		// connection status

  char servername[MAX_OSPATH];	// name of server from original connect (used by reconnect)

  bool qcomLoaded;
  // when the server clears the hunk, all of these must be restarted
  bool rendererlibLoaded;
  bool rendererStarted;
  bool soundlibLoaded;
  bool soundRegistered;
  bool soundStarted;
  bool inputlibLoaded;
  bool inputStarted;
  bool cgamelibLoaded;	// no real need here as we load the lib as we start cgame
  bool cgameStarted;	// and shut it down when cgame is stopped/restarted

  int framecount;
  int frametime;		// msec since last frame

  int realtime;			// ignores pause
  int realFrametime;		// ignoring pause, so console always works

  // rendering info
  glconfig_t glconfig;
  qhandle_t charSetShader;
  qhandle_t whiteShader;
  qhandle_t consoleShader;

  bool useLegacyConsoleFont;
  fontFace_t consoleFace;
};
typedef struct clientStatic_s clientStatic_t;

/*
==================================================================

the console_s structure stores all the informations to print
characters on screen

==================================================================
*/
#define MAX_NOTIFY_LIGNS 10
#define CON_TEXTSIZE 32768

struct console_s
{
  bool initialized;

  /*
  short text[CON_TEXTSIZE];	// first 8 bits are for the text char,
				// the 8 others are for the color char
  */
  char text[CON_TEXTSIZE];
  char tcolor[CON_TEXTSIZE];
  int current;			// line where next message will be printed
  int x;			// offset in current line for next print
  int display;			// bottom of console displays this line

  int linewidth;		// characters across screen
  int totallines;		// total lines in console scrollback

  float xadjust;		// for wide aspect screens

  float displayFrac;		// aproaches finalFrac at scr_conspeed
  float finalFrac;		// 0.0 to 1.0 lines of console to display

  int vislines;			// in scanlines

  int times[MAX_NOTIFY_LIGNS];	// cls.realtime time the line was generated
  vec4_t color;			// for transparent notify lines
};
typedef struct console_s console_t;

//=============================================================================

struct kbutton_s
{
  int down[2];		// key nums holding it down
  unsigned downtime;	// msec timestamp
  unsigned msec;	// msec down this frame if both a down and up happened
  bool active;		// current state
  bool wasPressed;	// set when down, not cleared when up
};
typedef struct kbutton_s kbutton_t;

//=============================================================================

extern refexport_t re;	// interface to refresh .dll

// dynamic lib
//extern Dlib_t qclib;
extern qcom_export_t * qce;

//extern Dlib_t clcglib;
extern cgamelib_export_t *clcge;

//extern Dlib_t sndlib;
extern sndexport_t *snde;
// client wrapped structure to pass to cgame
sndexport_t clsnde;

// input
//extern Dlib_t inlib;
extern inexport_t *inpe;

// global client structures
extern clientActive_t cl;	// cl_main
extern clientConnection_t clc;	// cl_main
extern clientStatic_t cls;	// cl_main

//
// cvars
//
extern	cvar_t	*cl_nodelta;
extern	cvar_t	*cl_noprint;
extern	cvar_t	*cl_maxpackets;
extern	cvar_t	*cl_packetdup;
extern	cvar_t	*cl_shownet;
extern	cvar_t	*cl_showSend;
extern	cvar_t	*cl_timeNudge;
extern	cvar_t	*cl_showTimeDelta;
extern	cvar_t	*cl_showSimTimeDelta;
extern	cvar_t	*cl_simTimeSmooth;

extern	cvar_t	*cl_yawspeed;
extern	cvar_t	*cl_pitchspeed;
extern	cvar_t	*cl_run;
extern	cvar_t	*cl_anglespeedkey;

extern	cvar_t	*cl_sensitivity;
extern	cvar_t	*cl_freelook;

extern	cvar_t	*cl_mouseAccel;
extern	cvar_t	*cl_showMouseRate;

extern	cvar_t	*cl_mouseAccelOffsetX;
extern	cvar_t	*cl_mouseAccelOffsetY;
extern	cvar_t	*cl_mouseAccelStyle;

extern	cvar_t	*m_pitch;
extern	cvar_t	*m_yaw;
extern	cvar_t	*m_forward;
extern	cvar_t	*m_side;
extern	cvar_t	*m_filter;

extern	cvar_t	*cl_timedemo;
extern	cvar_t	*cl_timedemoLog;
extern	cvar_t	*cl_autoRecordDemo;
extern	cvar_t	*cl_drawDemoRecording;
extern	cvar_t	*cl_demoShowSequence;
extern	cvar_t	*cl_demoConfigFile;
extern	cvar_t	*cl_avidemo;
extern	cvar_t	*cl_aviFrameRate;
extern	cvar_t	*cl_forceavidemo;

extern	cvar_t	*cl_activeAction;

extern	cvar_t	*cl_conXOffset;
extern	cvar_t	*cl_consoleFont;
extern	cvar_t	*cl_consoleFontSize;
extern	cvar_t	*cl_consoleFontKerning;

extern	cvar_t	*cl_maxPing;

extern	cvar_t	*cl_lanForcePackets;
extern	cvar_t	*cl_autoRecordDemo;

extern	cvar_t	*cl_buildscript;
extern	cvar_t	*cl_timescale;

extern	cvar_t	*cl_running;
extern	cvar_t	*cl_developer;

extern	cvar_t	*cl_speeds;
extern	cvar_t	*cl_dropsim;
extern	cvar_t	*cl_fixedtime;
extern	cvar_t	*cl_maxfps;

extern	cvar_t	*cl_waste_cputime;

extern	cvar_t	*dev_nc_wrapnextpackettime;
extern	cvar_t	*dev_nc_wraptimedelta;
extern	cvar_t	*dev_nc_addtimedelta;
extern	cvar_t	*dev_nc_window_max;
extern	cvar_t	*dev_nc_window_min;

extern int cl_connectedToCheatServer;

//=================================================

//
// cl_main
//
void CL_Init (void);
void CL_Shutdown (const char * finalmsg, bool closesession);

int CL_EventLoop (void);

void CL_FlushMemory (void);
void CL_ShutdownAll (void);
void CL_AddReliableCommand (const char *cmd);

void CL_StartHunkUsers (bool rendererOnly);

void CL_ClearState (void);
void CL_Disconnect_f (void);
void CL_GetChallengePacket (void);
void CL_Vid_Restart_f (void);
void CL_Snd_Restart_f (void);

void CL_ShutdownRef (bool destroyWindow);
void CL_UnloadRefLib (void);
void CL_InitRefLib (void);

void CL_Disconnect (bool showMainMenu);

bool CL_CheckFilesAvailable (void);
void CL_LaunchCGame (void);

void CL_Printf (const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
void CL_DPrintf (const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
void CL_Error (int code, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));
void CL_Fatal_Error (int code, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));

//
// cl_input
//
void CL_InitInput (void);
void CL_CloseInput (void);
void CL_CreateNewCommands (void);
void CL_MouseEvent (int dx, int dy, int time);

//
// cl_parse.c
//
void CL_SystemInfoChanged( void );
void CL_ParseServerMessage (msg_t * msg, int time);

//====================================================================

//
// console
//
void Con_DrawCharacter (int cx, int line, int num);

void Con_CheckResize (void);
void Con_Init (void);
void Con_Clear_f (void);
void Con_ToggleConsole_f (void);
void Con_DrawNotify (void);
void Con_ClearNotify (void);
void Con_RunConsole (void);
void Con_DrawConsole (void);
void Con_PageUp (void);
void Con_PageDown (void);
void Con_Top (void);
void Con_Bottom (void);
void Con_Close (void);

void CL_ConsolePrint (char * msg);

void CL_DrawColorList_f (void);

//
// cl_scrn.c
//
void SCR_Init (void);
void SCR_UpdateScreen (void);

// returns in virtual 640x480 coordinates
int SCR_GetBigStringWidth (const char *str);

void SCR_AdjustFrom640 (float *x, float *y, float *w, float *h);
void SCR_FillRect (float x, float y, float width, float height,
		   const float *color );
void SCR_DrawPic (float x, float y, float width, float height, qhandle_t hShader);
void SCR_DrawNamedPic (float x, float y, float width, float height, const char *picname);

// draws a string with embedded color control characters with fade
void SCR_DrawBigString (int x, int y, const char *s, float alpha, bool allowColor);
// ignores embedded color control characters
void SCR_DrawBigStringColor (int x, int y, const char *s, vec4_t color, bool allowColor);
void SCR_DrawSmallStringExt (int x, int y, const char *string, float *setColor,
			     bool forceColor, bool allowColor);
void SCR_DrawChar (int x, int y, float sizex, float sizey, int ch);
int SCR_DrawConsoleFontChar (float x, float y, const char *s);
float SCR_ConsoleFontCharWidth (const char *s);
float SCR_ConsoleFontCharHeight (void);
float SCR_ConsoleFontStringWidth (const char *s, int len);
void SCR_GlyphColor (const float *rgba);

//
// cl_cgame.c
//
void CL_InitCGame (void);
void CL_ShutdownCGame (void);
bool CL_GameCommand (void);
void CL_CGameRendering (void);
void CL_CGameProcessing (void);
void CL_SetCGameTime (void);
void CL_FirstSnapshot (void);

//
// cl_net_chan.c
//
void CL_Netchan_Transmit (netchan_t *chan, msg_t* msg);
void CL_Netchan_TransmitNextFragment (netchan_t *chan);
bool CL_Netchan_Process (netchan_t *chan, msg_t *msg);

void CL_SendCmd (void);
void CL_WritePacket (void);

//
// cl_keys.c
//
void CL_KeyEvent (int key, bool down, unsigned int time);
void CL_CharEvent (const char *s);
void CL_InitKeyCommands (void);
void Key_KeynameCompletion (void (*callback) (const char *s));
void Key_SetCatcher (int catcher);
int Key_GetCatcher (void);

int Key_StringToKeynum (char *str);
char * Key_KeynumToString (int keynum);

void CL_LoadConsoleHistory (void);
void CL_SaveConsoleHistory (void);

//
// cl_demo.c
//

void CL_StartDemoLoop (void);
void CL_ReadDemoMessage (void);
void CL_WriteDemoMessage (msg_t *msg, int headerBytes);

void CL_StopRecord_f (void);
void CL_Record_f (void);
void CL_PlayDemo_f (void);
void CL_DemoPause_f (void);
void CL_DemoJump_f (void);

void CL_CompleteDemoName (char *args, int argNum);
void CL_PauseDemoIcons (void);

//
// cl_tools.c
//
void dev_Tools_ColorFromStringHex( vec4_t hcolor, char * string );
void CL_ListPlayerModels_f (void);

//
// cl_libsnd.c
//
bool CL_InitSoundLib (void);
void CL_UnloadSoundLib (void);
void CL_ShutdownSound (void);
// sound api
void CL_Snd_Shutdown (void);
void CL_Snd_StartSound (vec3_t origin, int entnum, int entchannel,
			sfxHandle_t sfx);
void CL_Snd_StartLocalSound (sfxHandle_t sfx, int channelNum);
void CL_Snd_StartBackgroundTrack (const char *intro, const char *loop);
void CL_Snd_StopBackgroundTrack (void);
void CL_Snd_RawSamples (int stream, int samples, int rate, int width,
			int channels, const byte * data, float volume);
void CL_Snd_StopAllSounds (void);
void CL_Snd_ClearLoopingSounds (bool killall);
void CL_Snd_AddLoopingSound (int entityNum, const vec3_t origin,
			     const vec3_t velocity, sfxHandle_t sfx);
void CL_Snd_AddRealLoopingSound (int entityNum, const vec3_t origin,
				 const vec3_t velocity, sfxHandle_t sfx);
void CL_Snd_StopLoopingSound (int entityNum);
void CL_Snd_Respatialize (int entityNum, const vec3_t origin, vec3_t axis[3],
			  int inwater);
void CL_Snd_UpdateEntityPosition (int entityNum, const vec3_t origin);
void CL_Snd_Update (void);
void CL_Snd_DisableSounds (void);
void CL_Snd_BeginRegistration (void);
sfxHandle_t CL_Snd_RegisterSound (const char *sample);
void CL_Snd_ClearSoundBuffer (void);
void CL_Snd_SoundInfo (void);
void CL_Snd_SoundList (void);

//
// cl_libinput.c
//
void CL_InitInputLib (void);
void CL_UnloadInputLib (void);
void CL_ShutdownInput (void);

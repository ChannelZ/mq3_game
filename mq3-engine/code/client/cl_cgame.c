/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// cl_cgame.c  -- client system interaction with client game

#include "client.h"

// CGAME
// dynamic librarie
Dlib_t clcglib;
// resolved functions of the dyn lib for cgame
cgamelib_export_t *clcge;
cgamelib_import_t clcgi;

/*
====================
CL_GetGameState
====================
*/
void
CL_GetGameState (gameState_t * gs)
{
  *gs = cl.gameState;
}

/*
====================
CL_GetGlconfig
====================
*/
void
CL_GetGlconfig (glconfig_t * glconfig)
{
  *glconfig = cls.glconfig;
}

/*
====================
CL_GetUserCmd
====================
*/
bool
CL_GetUserCmd (int cmdNumber, usercmd_t * ucmd)
{
  // cmds[cmdNumber] is the last properly generated command

  // can't return anything that we haven't created yet
  if (cmdNumber > cl.cmdNumber)
    {
      CL_Error (ERR_DROP, "CL_GetUserCmd: %i >= %i", cmdNumber, cl.cmdNumber);
    }

  // the usercmd has been overwritten in the wrapping
  // buffer because it is too far out of date
  if (cmdNumber <= (cl.cmdNumber - CMD_BACKUP))
    {
      return false;
    }

  *ucmd = cl.cmds[cmdNumber & CMD_MASK];

  return true;
}

int
CL_GetCurrentCmdNumber (void)
{
  return cl.cmdNumber;
}

/*
====================
CL_GetParseEntityState
====================
*/
bool
CL_GetParseEntityState (int parseEntityNumber, entityState_t * state)
{
  // can't return anything that hasn't been parsed yet
  if (parseEntityNumber >= cl.parseEntitiesNum)
    {
      CL_Error (ERR_DROP, "CL_GetParseEntityState: %i >= %i",
		parseEntityNumber, cl.parseEntitiesNum);
    }

  // can't return anything that has been overwritten in the circular buffer
  if (parseEntityNumber <= (cl.parseEntitiesNum - MAX_PARSE_ENTITIES))
    {
      return false;
    }

  *state = cl.parseEntities[parseEntityNumber & (MAX_PARSE_ENTITIES - 1)];
  return true;
}

/*
====================
CL_GetCurrentSnapshotNumber
====================
*/
void
CL_GetCurrentSnapshotNumber (int *snapshotNumber, int *serverTime)
{
  *snapshotNumber = cl.snap.messageNum;
  *serverTime = cl.snap.serverTime;
}

/*
====================
CL_GetSnapshot
====================
*/
bool
CL_GetSnapshot (int snapshotNumber, snapshot_t * snapshot)
{
  clSnapshot_t *clSnap;
  int i, count, index;

  if (snapshotNumber > cl.snap.messageNum)
    {
      CL_Error (ERR_DROP,
		"CL_GetSnapshot: snapshotNumber > cl.snapshot.messageNum");
    }

  // if the frame has fallen out of the circular buffer, we can't return it
  if (snapshotNumber <= (cl.snap.messageNum - PACKET_BACKUP))
    {
      return false;
    }

  // if the frame is not valid, we can't return it
  clSnap = &cl.snapshots[snapshotNumber & PACKET_MASK];
  if (!clSnap->valid)
    {
      return false;
    }

  // if the entities in the frame have fallen out of their
  // circular buffer, we can't return it
  if (clSnap->parseEntitiesNum <= (cl.parseEntitiesNum - MAX_PARSE_ENTITIES))
    {
      return false;
    }

  // write the snapshot
  snapshot->snapFlags = clSnap->snapFlags;
  snapshot->serverCommandSequence = clSnap->serverCommandNum;
  snapshot->ping = clSnap->ping;
  snapshot->serverTime = clSnap->serverTime;
  snapshot->simulationTime = clSnap->simulationTime;
  Com_Memcpy (snapshot->areamask, clSnap->areamask,
	      sizeof (snapshot->areamask));
  snapshot->ps = clSnap->ps;
  count = clSnap->numEntities;
  if (count > MAX_ENTITIES_IN_SNAPSHOT)
    {
      CL_DPrintf ("CL_GetSnapshot: truncated %i entities to %i\n",
		  count, MAX_ENTITIES_IN_SNAPSHOT);
      count = MAX_ENTITIES_IN_SNAPSHOT;
    }
  snapshot->numEntities = count;
  for (i = 0; i < count; i++)
    {
      index = (clSnap->parseEntitiesNum + i) & (MAX_PARSE_ENTITIES - 1);
      snapshot->entities[i] = cl.parseEntities[index];
    }

  // FIXME: configstring changes and server commands!!!

  return true;
}

/*
=====================
CL_SetUserCmdValue
=====================
*/
void
CL_SetUserCmdValue (int userCmdValue, float sensitivityScale)
{
  cl.cgameUserCmdWeapon = userCmdValue;
  cl.cgameSensitivity = sensitivityScale;
}

/*
=====================
CL_AddCgameCommand
=====================
*/
void
CL_AddCgameCommand (const char *cmdName)
{
  qce->cmd.AddCommand (cmdName, NULL);
}

/*
=====================
CL_CgameError
=====================
*/
void
CL_CgameError (const char *string)
{
  CL_Error (ERR_DROP, "%s", string);
}

/*
=====================
CL_ConfigstringModified
=====================
*/
void
CL_ConfigstringModified (void)
{
  char *old, *s;
  int i, index;
  char *dup;
  gameState_t oldGs;
  int len;

  index = atoi (qce->cmd.Argv (1));
  if (index < 0 || index >= MAX_CONFIGSTRINGS)
    {
      CL_Error (ERR_DROP, "configstring > MAX_CONFIGSTRINGS");
    }
  // get everything after "cs <num>"
  s = qce->cmd.ArgsFrom (2);

  old = cl.gameState.stringData + cl.gameState.stringOffsets[index];
  if (!strcmp (old, s))
    {
      return;		// unchanged
    }

  // build the new gameState_t
  oldGs = cl.gameState;

  Com_Memset (&cl.gameState, 0, sizeof (cl.gameState));

  // leave the first 0 for uninitialized strings
  cl.gameState.dataCount = 1;

  for (i = 0; i < MAX_CONFIGSTRINGS; i++)
    {
      if (i == index)
	{
	  dup = s;
	}
      else
	{
	  dup = oldGs.stringData + oldGs.stringOffsets[i];
	}
      if (!dup[0])
	{
	  continue;	// leave with the default empty string
	}

      len = strlen (dup);

      if ((len + 1 + cl.gameState.dataCount) > MAX_GAMESTATE_CHARS)
	{
	  CL_Error (ERR_DROP, "MAX_GAMESTATE_CHARS exceeded");
	}

      // append it to the gameState string buffer
      cl.gameState.stringOffsets[i] = cl.gameState.dataCount;
      Com_Memcpy (cl.gameState.stringData + cl.gameState.dataCount, dup, len + 1);
      cl.gameState.dataCount += len + 1;
    }

  if (index == CS_SYSTEMINFO)
    {
      // parse serverId and other cvars
      CL_SystemInfoChanged ();
    }
}

/*
===================
CL_GetServerCommand

Set up argc/argv for the given command
===================
*/
bool
CL_GetServerCommand (int serverCommandNumber)
{
  char *s;
  char *cmd;
  static char bigConfigString[BIG_INFO_STRING];
  int argc;

  // if we have irretrievably lost a reliable command, drop the connection
  if (serverCommandNumber <= (clc.serverCommandSequence - MAX_RELIABLE_COMMANDS))
    {
      // when a demo record was started after the client got a whole bunch of
      // reliable commands then the client never got those first reliable commands
      if (clc.demoplaying)
	return false;

	CL_Error (ERR_DROP,
		  "CL_GetServerCommand: a reliable command was cycled out");
      return false;
    }

  if (serverCommandNumber > clc.serverCommandSequence)
    {
      CL_Error (ERR_DROP,
		"CL_GetServerCommand: requested a command not received");
      return false;
    }

  s = clc.serverCommands[serverCommandNumber & (MAX_RELIABLE_COMMANDS - 1)];
  clc.lastExecutedServerCommand = serverCommandNumber;

  CL_DPrintf ("serverCommand: %i : %s\n", serverCommandNumber, s);

rescan:
  qce->cmd.TokenizeString (s);
  cmd = qce->cmd.Argv (0);
  argc = qce->cmd.Argc ();

  if (!strcmp (cmd, "disconnect"))
    {
      // allow server to indicate why they were disconnected
      if (argc >= 2)
	{
	  CL_Error (ERR_SERVERDISCONNECT, "Server disconnected - %s",
		    qce->cmd.Argv (1));
	}
      else
	{
	  CL_Error (ERR_SERVERDISCONNECT, "Server disconnected\n");
	}
    }

  if (!strcmp (cmd, "bcs0"))
    {
      Com_sprintf (bigConfigString, BIG_INFO_STRING, "cs %s \"%s",
		   qce->cmd.Argv (1), qce->cmd.Argv (2));
      return false;
    }

  if (!strcmp (cmd, "bcs1"))
    {
      s = qce->cmd.Argv (2);
      if (strlen (bigConfigString) + strlen (s) >= BIG_INFO_STRING)
	{
	  CL_Error (ERR_DROP, "bcs1 exceeded BIG_INFO_STRING");
	}
      strcat (bigConfigString, s);
      return false;
    }

  if (!strcmp (cmd, "bcs2"))
    {
      s = qce->cmd.Argv (2);
      if (strlen (bigConfigString) + strlen (s) + 1 >= BIG_INFO_STRING)
	{
	  CL_Error (ERR_DROP, "bcs2 exceeded BIG_INFO_STRING");
	}
      strcat (bigConfigString, s);
      strcat (bigConfigString, "\"");
      s = bigConfigString;
      goto rescan;
    }

  if (!strcmp (cmd, "cs"))
    {
      CL_ConfigstringModified (); // look not to call Cmd_TokenizeString ()
      return true;
    }

  if (!strcmp (cmd, "map_restart"))
    {
      // clear notify lines and outgoing commands before passing
      // the restart to the cgame
      Con_ClearNotify (); // look not to call Cmd_TokenizeString ()
      Com_Memset (cl.cmds, 0, sizeof (cl.cmds));
      return true;
    }

  if (!strcmp (cmd, "clientLevelShot"))
    {
      if (cl_developer->integer)
	{
	  // close the console
	  Con_Close ();
	  // take a special screenshot next frame
	  qce->cmd.Cbuf_AddText ("wait;wait;wait;wait;screenshot levelshot\n");
	  return true;
	}
      return false;
    }
  // we may want to put a "connect to other server" command here

  // cgame can now act on the command
  return true;
}

/*
====================
CL_CM_LoadMap

Just adds default parameters that cgame doesn't need to know about
====================
*/
void
CL_CM_LoadMap (const char *mapname)
{
  int checksum;

  qce->cm.LoadMap (mapname, true, &checksum);
}

/*
====================
CL_ShutdonwCGame

====================
*/
void
CL_ShutdownCGame (void)
{
  if (!cls.cgamelibLoaded)
    {
      return;
    }

  // these shouldn't be needed
  if (clcglib.dynHandle == NULL || clcge == NULL || clcge->shutdown == NULL)
    {
      return;
    }

  // valid pointers
  CL_Printf ("CL_ShutdownCGame ()\n");

  clcge->shutdown ();

  clcge = NULL;

  Sys_DLUnload (clcglib.dynHandle);

  // this is the only function to turn this false
  cls.cgamelibLoaded = false;
}

/*
====================
CL_Key_SetCatcher
====================
*/
void
CL_Key_SetCatcher (int catcher)
{
  Key_SetCatcher (catcher | (Key_GetCatcher () & KEYCATCH_CONSOLE));
}

/*
====================
GetClientLibAPI
====================
*/
bool
GetClientLibAPI (cgamelib_import_t * cglib)
{
  // initialize and fill the structure
  Com_Memset (cglib, 0, sizeof (*cglib));

  cglib->Printf = CL_Printf;
  cglib->Error = CL_Error;
  cglib->Milliseconds = Sys_Milliseconds;
  cglib->Argc = qce->cmd.Argc;
  cglib->Argv = qce->cmd.Argv;
  cglib->ArgvBuffer = qce->cmd.ArgvBuffer;
  cglib->Args = qce->cmd.ArgsBuffer;
  cglib->FS_FOpenFile = qce->fs.FOpenFileByMode;
  cglib->FS_Read = qce->fs.Read2;
  cglib->FS_Write = qce->fs.Write;
  cglib->FS_FCloseFile = qce->fs.FCloseFile;
  cglib->FS_Seek = qce->fs.Seek;
  cglib->SendConsoleCommand = qce->cmd.Cbuf_AddText;
  cglib->SendClientCommand = CL_AddReliableCommand;
  cglib->Cvar_Get = qce->cvar.Get;
  cglib->Cvar_Set = qce->cvar.Set;
  cglib->Cvar_SetNew = qce->cvar.SetNew;
  cglib->Cvar_CheckRange = qce->cvar.CheckRange;
  cglib->Cvar_VariableStringBuffer = qce->cvar.VariableStringBuffer;
  cglib->AddCommand = CL_AddCgameCommand;
  cglib->RemoveCommand = qce->cmd.RemoveCommand;
  cglib->UpdateScreen = SCR_UpdateScreen;
  cglib->CM_LoadMap = CL_CM_LoadMap;
  cglib->CM_NumInlineModels = qce->cm.NumInlineModels;
  cglib->CM_InlineModel = qce->cm.InlineModel;
  cglib->CM_TempBoxModel = qce->cm.TempBoxModel;
  cglib->CM_PointContents = qce->cm.PointContents;
  cglib->CM_TransformedPointContents = qce->cm.TransformedPointContents;
  cglib->CM_BoxTrace = qce->cm.BoxTrace;
  cglib->CM_TransformedBoxTrace = qce->cm.TransformedBoxTrace;
  cglib->CM_MarkFragments = re.MarkFragments;

  cglib->R_LoadWorldMap = re.LoadWorld;
  cglib->R_RegisterModel = re.RegisterModel;
  cglib->R_RegisterModelExt = re.RegisterModelExt;
  cglib->R_RegisterSkin = re.RegisterSkin;
  cglib->R_RegisterSkinExt = re.RegisterSkinExt;
  cglib->R_RegisterShader = re.RegisterShader;
  cglib->R_RegisterShaderExt = re.RegisterShaderExt;

  cglib->R_LoadFace = re.LoadFace;
  cglib->R_FreeFace = re.FreeFace;

  cglib->R_ClearScene = re.ClearScene;
  cglib->R_AddRefEntityToScene = re.AddRefEntityToScene;
  cglib->R_AddPolysToScene = re.AddPolyToScene;
  cglib->R_LightForPoint = re.LightForPoint;
  cglib->R_AddLightToScene = re.AddLightToScene;
  cglib->R_AddAdditiveLightToScene = re.AddAdditiveLightToScene;
  cglib->R_RenderScene = re.RenderScene;
  cglib->R_SetColor = re.SetColor;
  cglib->R_DrawStretchPic = re.DrawStretchPic;
  cglib->R_ModelBounds = re.ModelBounds;
  cglib->R_LerpTag = re.LerpTag;
  cglib->R_inPVS = re.inPVS;
  cglib->GetGlconfig = CL_GetGlconfig;
  cglib->GetGameState = CL_GetGameState;
  cglib->GetCurrentSnapshotNumber = CL_GetCurrentSnapshotNumber;
  cglib->GetSnapshot = CL_GetSnapshot;
  cglib->GetServerCommand = CL_GetServerCommand;
  cglib->GetCurrentCmdNumber = CL_GetCurrentCmdNumber;
  cglib->GetUserCmd = CL_GetUserCmd;
  cglib->SetUserCmdValue = CL_SetUserCmdValue;
  //cglib->Key_IsDown = Key_IsDown;
  cglib->Key_GetCatcher = Key_GetCatcher;
  cglib->Key_SetCatcher = CL_Key_SetCatcher;
  //cglib->Key_GetKey = Key_GetKey;

  cglib->RealTime = qce->Com_RealTime;
  cglib->SnapVector = SnapVector;
  cglib->GetEntityToken = re.GetEntityToken;

  cglib->MemoryRemaining = qce->mem.Hunk_MemoryRemaining;

  cglib->snd = &clsnde;

  return true;
}

/*
====================
CL_InitCGame

Should only be called by CL_StartHunkUsers
====================
*/
void
CL_InitCGame (void)
{
  const char *info;
  const char *mapname;
  int t1, t2;
  void *Hdl;
  char *error;
  cgamelib_export_t *(*CGameAPI) (int, cgamelib_import_t *);

  t1 = Sys_Milliseconds ();

  // put away the console
  Con_Close ();

  // find the current mapname
  info = cl.gameState.stringData + cl.gameState.stringOffsets[CS_SERVERINFO];
  mapname = Info_ValueForKey (info, "mapname");
  Com_sprintf (cl.mapname, sizeof (cl.mapname), "maps/%s.bsp", mapname);

  // free the structure
  Com_Memset (&clcglib, 0, sizeof (clcglib));

  // load the dynamic library
  clcglib.dynHandle = Sys_DLLoad ("cgame", clcglib.fqpath);

  // the library is loaded, resolve cgame functions
  Hdl = clcglib.dynHandle;

  // search for GetCGameAPI in cgame.so
  CGameAPI = Sys_DLLoadFunction (Hdl, "GetCGameAPI", true);

  CL_Printf ("CL_InitCGame () : Found CGameAPI ().\n");

  // only set here, we have properly loaded the lib
  cls.cgamelibLoaded = true;

  //cls.state = CA_LOADING;

  // building cgame import structure
  GetClientLibAPI (&clcgi);

  // send this structure to cgame.so and retreiving a valid cgame export struct
  clcge = CGameAPI (1, &clcgi);

  if (clcge == NULL)
    {
      CL_Error (ERR_DROP, "CL_InitCGame () : NULL cgameexport structure.\n");
    }
  CL_Printf ("CL_InitCGame () : Valid cgame functions structures\n");

  // init for this gamestate
  // use the lastExecutedServerCommand instead of the serverCommandSequence
  // otherwise server commands sent just before a gamestate are dropped
  error = clcge->init (clc.serverMessageSequence,
		       clc.lastExecutedServerCommand, clc.clientNum);
  // returns NULL if valid; otherwise an error message
  if (error != NULL)
    {
      CL_Error (ERR_DROP,
		"CL_InitCGame () : error calling CG_Init () : %s\n", error);
    }

  // reset any CVAR_CHEAT cvars registered by cgame
  if (!clc.demoplaying && !cl_connectedToCheatServer)
    qce->cvar.SetCheatState ();

  // we will send a usercmd this frame, which
  // will cause the server to send us the first snapshot
  cls.state = CA_PRIMED;

  // have the renderer touch all its images, so they are present
  // on the card even if the driver does deferred loading
  re.EndRegistration ();

  // make sure everything is paged in; still should avoid some glitches early in the game
  if (!Sys_LowPhysicalMemory ())
    {
      qce->mem.TouchMemory ();
    }

  t2 = Sys_Milliseconds ();

  CL_Printf ("CL_InitCGame: %5.2f seconds\n", (float)(t2 - t1) / 1000.0);

  // clear anything that got printed
  Con_ClearNotify ();

  cls.cgameStarted = true;
}

/*
====================
CL_GameCommand

See if the current console command is claimed by the cgame
====================
*/
bool
CL_GameCommand (void)
{
  if (!cls.cgameStarted)
    {
      return false;
    }

  return clcge->console_command ();
}

/*
=====================
CL_CGameRendering
=====================
*/
void
CL_CGameRendering ()
{
  clcge->draw_active_frame (cl.serverTime, cl.simulationTime);
}

/*
=====================
CL_CGameProcessing
=====================
*/
void
CL_CGameProcessing ()
{
  clcge->process_active_frame (cl.serverTime, cl.simulationTime, clc.demoplaying);
}

/*
=================
CL_AdjustTimeDelta

Adjust the clients view of server time.

We attempt to have cl.serverTime exactly equal the server's view
of time plus the timeNudge, but with variable latencies over
the internet it will often need to drift a bit to match conditions.

Our ideal time would be to have the adjusted time approach, but not pass,
the very latest snapshot.

Adjustments are only made when a new snapshot arrives with a rational
latency, which keeps the adjustment process framerate independent and
prevents massive overadjustment during times of significant packet loss
or bursted delayed packets.
=================
*/

#define RESET_TIME 500
#define RESET_SIM_TIME 100

void
CL_AdjustTimeDelta (void)
{
  int resetTime;
  int newDelta;
  int deltaDelta;
  int waitingTime = cl_frameTime - cl.snap.receivedTime;
  int delta_window_max = dev_nc_window_max->integer;
  int delta_window_min = dev_nc_window_min->integer;

  cl.newSnapshots = false;

  // the delta never drifts when replaying a demo
  if (clc.demoplaying)
    {
      return;
    }

  // if the current time is WAY off, just correct to the current value
  resetTime = RESET_TIME;

  newDelta = cl.snap.serverTime - (cls.realtime - waitingTime);
  deltaDelta = abs (newDelta - cl.serverTimeDelta);

  if (deltaDelta > resetTime)
    {
      cl.serverTimeDelta = newDelta;
      cl.oldServerTime = cl.snap.serverTime;	// FIXME: is this a problem for cgame?
      cl.serverTime = cl.snap.serverTime;
      if (cl_showTimeDelta->integer)
	{
	  CL_Printf ("<RESET> ");
	}
    }
  else if (deltaDelta > 100)
    {
      // fast adjust, cut the difference in half
      if (cl_showTimeDelta->integer)
	{
	  CL_Printf ("<FAST> ");
	}
      cl.serverTimeDelta = (cl.serverTimeDelta + newDelta) >> 1;
    }
  else
    {
      // slow drift adjust, only move 1 or 2 msec

      // if any of the frames between this and the previous snapshot
      // had to be extrapolated, nudge our sense of time back a little
      // the granularity of +1 / -2 is too high for timescale modified frametimes
      if (cl_timescale->value == 0 || cl_timescale->value == 1)
	{
	  if (cl.extrapolatedSnapshot)
	    {
	      cl.extrapolatedSnapshot = false;
	      cl.serverTimeDelta -= dev_nc_wraptimedelta->integer;
	      if (cl_showTimeDelta->integer)
		{
		  CL_Printf ("<WRAP> ");
		}
	    }
	  else
	    {
	      // check whether we already are near of the server time
	      if (deltaDelta > delta_window_max)
		{
		  // move our sense of time forward to minimize total latency
		  cl.serverTimeDelta += dev_nc_addtimedelta->integer;
		}
	      else if (deltaDelta < delta_window_min)
		{
		  // move our sense of time backward not to go before servertime
		  cl.serverTimeDelta -= dev_nc_addtimedelta->integer;
		}
	      else
		{
		  // nothing to do here !
		}
	    }
	}
    }

  if (cl_showTimeDelta->integer)
    {
      CL_Printf ("%i ", (cl.snap.serverTime - cl.serverTimeDelta
			 - (cls.realtime - waitingTime)));
    }
}

/*
=================
CL_AdjustSimTimeDelta

do the work even if the game is paused !
=================
*/
void
CL_AdjustSimTimeDelta (void)
{
  int resetTime;
  int newDelta;
  int deltaDelta;
  int waitingTime = cl_frameTime - cl.snap.receivedTime;

  // the delta never drifts when replaying a demo
  if (clc.demoplaying)
    {
      return;
    }

  // if the current time is WAY off, just correct to the current value
  resetTime = RESET_SIM_TIME;

  newDelta = cl.snap.simulationTime - (cls.realtime - waitingTime);
  deltaDelta = abs (newDelta - cl.simulationTimeDelta);

  if (deltaDelta > resetTime)
    {
      cl.simulationTimeDelta = newDelta;
      cl.oldSimulationTime = cl.snap.simulationTime;
      cl.simulationTime = cl.snap.simulationTime;

      if (cl_showSimTimeDelta->integer)
	{
	  CL_Printf ("<RESET SIM> ");
	}
    }
  else
    {
      // never going too far from cg.snap.simulationTime
      // we are maximum RESET_SIM_TIME ahead
      if (cl.simulationTime > cl.snap.simulationTime)
	{
	  cl.simulationTimeDelta -= 1;

	  if (cl_showSimTimeDelta->integer >= 2)
	    {
	      CL_Printf ("<SIM DOWN> ");
	    }
	}
      else if (cl.simulationTime < (cl.snap.simulationTime - 3))
	{
	  cl.simulationTimeDelta += 1;

	  if (cl_showSimTimeDelta->integer >= 2)
	    {
	      CL_Printf ("<SIM UP> ");
	    }
	}
      else // we are exaclty synced
	{
	  if (cl_showSimTimeDelta->integer >= 2)
	    {
	      CL_Printf ("<SIM SYNC> ");
	    }
	}
    }
}

/*
==================
CL_FirstSnapshot
==================
*/
void
CL_FirstSnapshot (void)
{
  // ignore snapshots that don't have entities
  if (cl.snap.snapFlags & SNAPFLAG_NOT_ACTIVE)
    {
      return;
    }
  cls.state = CA_ACTIVE;

  // set the timedelta so we are exactly on this first frame
  cl.serverTimeDelta = cl.snap.serverTime - cls.realtime;
  cl.oldServerTime = cl.snap.serverTime;

  cl.simulationTimeDelta = cl.snap.simulationTime - cls.realtime;
  cl.oldSimulationTime = cl.snap.simulationTime;

  clc.timeDemoBaseTime = cl.snap.serverTime;

  // if this is the first frame of active play,
  // execute the contents of activeAction now
  // this is to allow scripting a timedemo to start right
  // after loading
  if (cl_activeAction->string[0])
    {
      qce->cmd.Cbuf_AddText (cl_activeAction->string);
      qce->cvar.SetNew (cl_activeAction, "");
    }
}

/*
==================
CL_SetCGameTime
==================
*/
#define RAW_NETCODE 0 // for tests - bubu
void
CL_SetCGameTime (void)
{
  static int lastprintTime = -1;

  // getting a valid frame message ends the connection process
  if (cls.state != CA_ACTIVE)
    {
      if (cls.state != CA_PRIMED)
	{
	  return;
	}
      if (clc.demoplaying)
	{
	  // we shouldn't get the first snapshot on the same frame
	  // as the gamestate, because it causes a bad time skip
	  if (!clc.firstDemoFrameSkipped)
	    {
	      clc.firstDemoFrameSkipped = true;
	      return;
	    }
	  CL_ReadDemoMessage ();
	}
      if (cl.newSnapshots)
	{
	  cl.newSnapshots = false;
	  CL_FirstSnapshot ();
	}
      // activeAction may change the state
      if (cls.state != CA_ACTIVE)
	{
	  return;
	}
    }

  // if we have gotten to this point, cl.snap is guaranteed to be valid
  if (!cl.snap.valid)
    {
      CL_Error (ERR_DROP, "CL_SetCGameTime: !cl.snap.valid");
    }

  if (cl.snap.serverTime < cl.oldFrameServerTime)
    {
      // issue a reconnect as this situation
      // seems to mainly occur on a map change
      qce->cmd.Cbuf_AddText ("reconnect");

      CL_Error (ERR_DROP, "cl.snap.serverTime < cl.oldFrameServerTime");
    }
  cl.oldFrameServerTime = cl.snap.serverTime;

  // get our current view of time

  if (clc.demoplaying && clc.demopaused)
    {
      return;
    }
  else
    {
      // this is ping !
      //int propagationTime = cl.snap.receivedTime - cl.snap.serverTime;
      // time during arrival and tagging (which may differ if packet
      // arrived during framerendering) with time to start treatment
      // and rendering.
      int waitingTime = cl_frameTime - cl.snap.receivedTime;

      // CL_Printf ("diff: %d - off: %d\n", propagationTime, waitingTime);

      // cl_timeNudge is a user adjustable cvar that allows more
      // or less latency to be added in the interest of better 
      // smoothness or better responsiveness.
      int tn;

      tn = cl_timeNudge->integer;
      if (tn < -30)
	{
	  tn = -30;
	}
      else if (tn > 30)
	{
	  tn = 30;
	}

      // bu : added a term to adjust demo pausing, seems it's only needed here.
      cl.serverTime = cls.realtime + cl.serverTimeDelta - tn - clc.demopausetime;

      // be sure we don't play a demo to do client realtime interpolation
      if (cl_simTimeSmooth->integer && !clc.demoplaying)
	{
	  cl.simulationTime = cls.realtime + cl.simulationTimeDelta;
	}
      else
	{
	  cl.simulationTime = cl.snap.simulationTime;
	}

#if RAW_NETCODE
      // with raw_netcode,
      // cgame is synchronised with game and the game is not "smoothed"
      // what you see is what the server has calculated
      // if the client is running faster than the server (cl_maxfps > sv_fps)
      // some sounds might be dropped (pickup sounds), because they are based
      // on server time (fix this ?) and some frames are "redraw" identically
      // should be nice to test that on lan with sv_fps 125
      cl.serverTime = cl.snap.serverTime;
      cl.oldServerTime = cl.serverTime;

      cl.simulationTime = cl.snap.simulationTime;
      cl.oldSimulationTime = cl.simulationTime;

      // NOTE : a good online model would be the client @ a chosen framerate (ex : 125)
      // with extrapolation (prediction) for the player view (movements, jumps, angles...)
      // [otherwise you have inputs (crosshair, movements...) from the server, but lagged]
      // and all other entities directly forwarded from the server
      // if sv_fps = 20 the player should see other players a bit 'jerky' (no interpolation)
      // and item sounds delayed; this should be nice with sv_fps 60
#else
      // guarantee that time will never flow backwards, even if
      // serverTimeDelta made an adjustment or cl_timeNudge was changed
      if (cl.serverTime < cl.oldServerTime)
	{
	  cl.serverTime = cl.oldServerTime;
	}
      cl.oldServerTime = cl.serverTime;

      // don't flow backwards on regular simulation
      // this might happend on a map_restart or matchStart
      // will be handled by the adjusttimedelta function
      if (cl.simulationTime < cl.oldSimulationTime)
	{
	  // reset cgame entities ?
	}
      cl.oldSimulationTime = cl.simulationTime;
#endif

      // timescale tunner, to know when cl.serverTime goes negative
      if (0)
	{
	  if (lastprintTime < cl.serverTime)
	    {
	      CL_Printf ("%d\n", cl.snap.serverTime - cl.serverTime);
	      lastprintTime = cl.serverTime + 1000;
	    }
	}

      // note if we are almost past the latest frame (without timeNudge),
      // so we will try and adjust back a bit when the next snapshot arrives
      int maxservertime = cl.snap.serverTime - dev_nc_wrapnextpackettime->integer;

      if (((cls.realtime - waitingTime) + cl.serverTimeDelta) >= maxservertime)
	{
	  cl.extrapolatedSnapshot = true;
	}
    }

  // if we have gotten new snapshots, drift serverTimeDelta
  // don't do this every frame, or a period of packet loss would
  // make a huge adjustment
  if (cl.newSnapshots)
    {
      CL_AdjustTimeDelta ();
      CL_AdjustSimTimeDelta ();
    }

  if (!clc.demoplaying)
    {
      return;	// return here if not playing a demo
    }

  // if we are playing a demo back, we can just keep reading
  // messages from the demo file until the cgame definately
  // has valid snapshots to interpolate between

  // a timedemo will always use a deterministic set of time samples
  // no matter what speed machine it is run on,
  // while a normal demo may have different time samples
  // each time it is played back
  if (cl_timedemo->integer)
    {
      int now = Sys_Milliseconds ();
      int frameDuration;
      int index;

      if (!clc.timeDemoStart)
	{
	  clc.timeDemoStart = clc.timeDemoLastFrame = now;
	  clc.timeDemoMinDuration = INT_MAX;
	  clc.timeDemoMaxDuration = 0;
	}

      frameDuration = now - clc.timeDemoLastFrame;
      clc.timeDemoLastFrame = now;

      // Ignore the first measurement as it'll always be 0
      if (clc.timeDemoFrames > 0)
	{
	  if (frameDuration > clc.timeDemoMaxDuration)
	    clc.timeDemoMaxDuration = frameDuration;

	  if (frameDuration < clc.timeDemoMinDuration)
	    clc.timeDemoMinDuration = frameDuration;

	  // 255 ms = about 4fps
	  if (frameDuration > UCHAR_MAX)
	    frameDuration = UCHAR_MAX;

	  index = (clc.timeDemoFrames - 1) % MAX_TIMEDEMO_DURATIONS;
	  clc.timeDemoDurations[index] = frameDuration;
	}

      clc.timeDemoFrames++;
      cl.serverTime = clc.timeDemoBaseTime + clc.timeDemoFrames * 50;
    }

  while (cl.serverTime >= cl.snap.serverTime)
    {
      // feed another messag, which should change
      // the contents of cl.snap
      CL_ReadDemoMessage ();
      if (cls.state != CA_ACTIVE)
	{
	  return;	// end of demo
	}
    }
}

/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "qcommon/q_shared.h"

#include "client.h"

/*
==============
CL_Netchan_Encode

	// first 12 bytes of the data are always:
	long serverId;
	long messageAcknowledge;
	long reliableAcknowledge;

==============
*/
static void
CL_Netchan_Encode (msg_t * msg)
{
  long serverId, messageAcknowledge, reliableAcknowledge;
  int i, index, srdc, sbit, soob;
  byte key, *string;

  if (msg->cursize <= CL_ENCODE_START)
    {
      return;
    }

  srdc = msg->readcount;
  sbit = msg->bit;
  soob = msg->oob;

  msg->bit = 0;
  msg->readcount = 0;
  msg->oob = 0;

  serverId = qce->msg.ReadLong (msg);
  messageAcknowledge = qce->msg.ReadLong (msg);
  reliableAcknowledge = qce->msg.ReadLong (msg);

  msg->oob = soob;
  msg->bit = sbit;
  msg->readcount = srdc;

  string = (byte *)clc.serverCommands[reliableAcknowledge & (MAX_RELIABLE_COMMANDS - 1)];
  index = 0;
  //
  key = clc.challenge ^ serverId ^ messageAcknowledge;
  for (i = CL_ENCODE_START; i < msg->cursize; i++)
    {
      // modify the key with the last received now acknowledged server command
      if (!string[index])
	index = 0;
      if (string[index] > 127 || string[index] == '%')
	{
	  key ^= '.' << (i & 1);
	}
      else
	{
	  key ^= string[index] << (i & 1);
	}
      index++;
      // encode the data with this key
      *(msg->data + i) = (*(msg->data + i)) ^ key;
    }
}

/*
==============
CL_Netchan_Decode

	// first four bytes of the data are always:
	long reliableAcknowledge;

==============
*/
static void
CL_Netchan_Decode (msg_t * msg)
{
  long reliableAcknowledge, i, index;
  byte key, *string;
  int srdc, sbit, soob;

  srdc = msg->readcount;
  sbit = msg->bit;
  soob = msg->oob;

  msg->oob = 0;

  reliableAcknowledge = qce->msg.ReadLong (msg);

  msg->oob = soob;
  msg->bit = sbit;
  msg->readcount = srdc;

  string = (byte *)clc.reliableCommands[reliableAcknowledge & (MAX_RELIABLE_COMMANDS - 1)];
  index = 0;
  // xor the client challenge with the netchan sequence number
  // (need something that changes every message)
  key = clc.challenge ^ LittleLong (*(unsigned *)msg->data);
  for (i = msg->readcount + CL_DECODE_START; i < msg->cursize; i++)
    {
      // modify the key with the last sent and with this message acknowledged client command
      if (!string[index])
	index = 0;
      if (string[index] > 127 || string[index] == '%')
	{
	  key ^= '.' << (i & 1);
	}
      else
	{
	  key ^= string[index] << (i & 1);
	}
      index++;
      // decode the data with this key
      *(msg->data + i) = *(msg->data + i) ^ key;
    }
}

/*
=================
CL_Netchan_TransmitNextFragment
=================
*/
void
CL_Netchan_TransmitNextFragment (netchan_t * chan)
{
  qce->net.Netchan_TransmitNextFragment (chan);
}

/*
===============
CL_Netchan_Transmit
================
*/
void
CL_Netchan_Transmit (netchan_t * chan, msg_t * msg)
{
  qce->msg.WriteByte (msg, clc_EOF);

  //CL_Netchan_Encode (msg);
  qce->net.Netchan_Transmit (chan, msg->cursize, msg->data);
}

/*
=================
CL_Netchan_Process
=================
*/
bool
CL_Netchan_Process (netchan_t * chan, msg_t * msg)
{
  int ret;
  static int newsize = 0;

  ret = qce->net.Netchan_Process (chan, msg);
  if (!ret)
    return false;
  //CL_Netchan_Decode (msg);
  newsize += msg->cursize;
  return true;
}

/*
=================
CL_ReadyToSendPacket

Returns false if we are over the maxpackets limit
and should choke back the bandwidth a bit by not sending
a packet this frame.  All the commands will still get
delivered in the next packet, but saving a header and
getting more delta compression will reduce total bandwidth.
=================
*/
bool
CL_ReadyToSendPacket (void)
{
  int oldPacketNum;
  int delta;

  // don't send anything if playing back a demo
  if (clc.demoplaying)
    {
      return false;
    }

  // if we don't have a valid gamestate yet, only send
  // one packet a second
  if ((cls.state != CA_ACTIVE)
      && (cls.state != CA_PRIMED)
      && (cls.realtime - clc.lastPacketSentTime < 1000))
    {
      return false;
    }

  // send every frame for loopbacks
  if (clc.netchan.remoteAddress.type == NA_LOOPBACK)
    {
      return true;
    }

  // send every frame for LAN
  if (cl_lanForcePackets->integer
      && qce->net.IsLANAddress (clc.netchan.remoteAddress))
    {
      return true;
    }

  // cl_maxpackets->integer is a valid integer between 15 and 125

  oldPacketNum = (clc.netchan.outgoingSequence - 1) & PACKET_MASK;
  delta = cls.realtime - cl.outPackets[oldPacketNum].p_realtime;
  if (delta < (1000 / cl_maxpackets->integer))
    {
      // the accumulated commands will go out in the next packet
      return false;
    }

  return true;
}

// ============================================
/*
int
Com_HashKey(char *string, int maxlen)
{
  register int hash, i;

  hash = 0;
  for (i = 0; i < maxlen && string[i] != '\0'; i++)
    {
      hash += string[i] * (119 + i);
    }
  hash = (hash ^ (hash >> 10) ^ (hash >> 20));
  return hash;
}
*/
/*
===================
CL_WritePacket

Create and send the command packet to the server
Including both the reliable commands and the usercmds

During normal gameplay, a client packet will contain something like:

4	sequence number
2	qport
4	serverid
4	acknowledged sequence number
4	clc.serverCommandSequence
<optional reliable commands>
1	clc_move or clc_moveNoDelta
1	command count
<count * usercmds>

===================
*/
void
CL_WritePacket (void)
{
  msg_t buf;
  byte data[MAX_MSGLEN];
  int i, j, index;
  usercmd_t *cmd, *oldcmd;
  usercmd_t nullcmd;
  int packetNum;
  int oldPacketNum;
  int count;

  // don't send anything if playing back a demo
  if (clc.demoplaying)
    {
      return;
    }

  Com_Memset (&nullcmd, 0, sizeof (nullcmd));
  oldcmd = &nullcmd;

  qce->msg.Init (&buf, data, sizeof (data));

  qce->msg.Bitstream (&buf);
  // write the current serverId so the server
  // can tell if this is from the current gameState
  qce->msg.WriteLong (&buf, cl.serverId);

  // write the last message we received, which can
  // be used for delta compression, and is also used
  // to tell if we dropped a gamestate
  qce->msg.WriteLong (&buf, clc.serverMessageSequence);

  // write the last reliable message we received
  qce->msg.WriteLong (&buf, clc.serverCommandSequence);

  // write any unacknowledged clientCommands
  for (i = clc.reliableAcknowledge + 1; i <= clc.reliableSequence; i++)
    {
      qce->msg.WriteByte (&buf, clc_clientCommand);
      qce->msg.WriteLong (&buf, i);
      index = i & (MAX_RELIABLE_COMMANDS - 1);
      qce->msg.WriteString (&buf, clc.reliableCommands[index]);
    }

  // we want to send all the usercmds that were generated in the last
  // few packet, so even if a couple packets are dropped in a row,
  // all the cmds will make it to the server
  oldPacketNum = (clc.netchan.outgoingSequence - 1 - cl_packetdup->integer);
  oldPacketNum &= PACKET_MASK;

  count = cl.cmdNumber - cl.outPackets[oldPacketNum].p_cmdNumber;
  if (count > MAX_PACKET_USERCMDS)
    {
      count = MAX_PACKET_USERCMDS;
      CL_Printf ("MAX_PACKET_USERCMDS\n");
    }

  if (count >= 1)
    {
      if (cl_showSend->integer)
	{
	  CL_Printf ("(%i)", count);
	}

      // begin a client move command
      if (cl_nodelta->integer || !cl.snap.valid || clc.demowaiting
	  || (clc.serverMessageSequence != cl.snap.messageNum))
	{
	  qce->msg.WriteByte (&buf, clc_moveNoDelta);
	}
      else
	{
	  qce->msg.WriteByte (&buf, clc_move);
	}

      // write the command count
      qce->msg.WriteByte (&buf, count);

      /*
      // use the checksum feed in the key
      key = clc.checksumFeed;
      // also use the message acknowledge
      key ^= clc.serverMessageSequence;
      // also use the last acknowledged server command in the key
      index = clc.serverCommandSequence & (MAX_RELIABLE_COMMANDS - 1);
      key ^= Com_HashKey (clc.serverCommands[index], 32);
      */
      // write all the commands, including the predicted command
      for (i = 0; i < count; i++)
	{
	  j = (cl.cmdNumber - count + i + 1) & CMD_MASK;
	  cmd = &cl.cmds[j];
	  qce->msg.WriteDeltaUsercmd (&buf, oldcmd, cmd);
	  oldcmd = cmd;
	}
    }

  //
  // deliver the message
  //
  packetNum = clc.netchan.outgoingSequence & PACKET_MASK;
  cl.outPackets[packetNum].p_realtime = cls.realtime;
  cl.outPackets[packetNum].p_serverTime = oldcmd->serverTime;
  cl.outPackets[packetNum].p_cmdNumber = cl.cmdNumber;
  clc.lastPacketSentTime = cls.realtime;

  if (cl_showSend->integer)
    {
      CL_Printf ("%i ", buf.cursize);
    }

  CL_Netchan_Transmit (&clc.netchan, &buf);

  // clients never really should have messages large enough
  // to fragment, but in case they do, fire them all off at once
  // TTimo: this causes a packet burst, which is bad karma for winsock
  // added a WARNING message, we'll see if there are legit situations where this happens
  while (clc.netchan.unsentFragments)
    {
      CL_DPrintf
	("WARNING: #462 unsent fragments (not supposed to happen!)\n");
      CL_Netchan_TransmitNextFragment (&clc.netchan);
    }
}

/*
=================
CL_SendCmd

Called every frame to builds and sends a command packet to the server.
=================
*/
void
CL_SendCmd (void)
{
  // don't send any message if not connected
  if (cls.state < CA_CONNECTED)
    {
      return;
    }

  // we create commands even if a demo is playing,
  CL_CreateNewCommands ();

  // don't send a packet if the last packet was sent too recently
  if (!CL_ReadyToSendPacket ())
    {
      if (cl_showSend->integer)
	{
	  CL_Printf (". ");
	}
      return;
    }

  CL_WritePacket ();
}

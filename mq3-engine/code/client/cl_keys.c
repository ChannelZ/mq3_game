/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "client.h"

/*

key up events are sent even if in console mode

*/

field_t historyEditLines[COMMAND_HISTORY];

int nextHistoryLine;		// the last line in the history buffer, not masked
int historyLine;		// the line being displayed from history buffer
				// will be <= nextHistoryLine

field_t g_consoleField;
field_t chatField;
bool chat_team;
int chat_playerNum;

bool key_overstrikeMode;

int anykeydown;
qkey_t keys[MAX_KEYS];

static int keyCatchers = 0;

typedef struct
{
  char *name;
  int keynum;
} keyname_t;

// names not in this list can either be lowercase ascii, or '0xnn' hex sequences
keyname_t keynames[] = {
  {"TAB", K_TAB},
  {"ENTER", K_ENTER},
  {"ESCAPE", K_ESCAPE},
  {"SPACE", K_SPACE},
  {"BACKSPACE", K_BACKSPACE},
  {"UPARROW", K_UPARROW},
  {"DOWNARROW", K_DOWNARROW},
  {"LEFTARROW", K_LEFTARROW},
  {"RIGHTARROW", K_RIGHTARROW},

  {"ALT", K_ALT},
  {"CTRL", K_CTRL},
  {"SHIFT", K_SHIFT},

  {"COMMAND", K_COMMAND},

  {"CAPSLOCK", K_CAPSLOCK},

  {"F1", K_F1},
  {"F2", K_F2},
  {"F3", K_F3},
  {"F4", K_F4},
  {"F5", K_F5},
  {"F6", K_F6},
  {"F7", K_F7},
  {"F8", K_F8},
  {"F9", K_F9},
  {"F10", K_F10},
  {"F11", K_F11},
  {"F12", K_F12},
  {"F13", K_F13},
  {"F14", K_F14},
  {"F15", K_F15},

  {"INS", K_INS},
  {"DEL", K_DEL},
  {"PGDN", K_PGDN},
  {"PGUP", K_PGUP},
  {"HOME", K_HOME},
  {"END", K_END},

  {"MOUSE1", K_MOUSE1},
  {"MOUSE2", K_MOUSE2},
  {"MOUSE3", K_MOUSE3},
  {"MOUSE4", K_MOUSE4},
  {"MOUSE5", K_MOUSE5},

  {"MWHEELUP", K_MWHEELUP},
  {"MWHEELDOWN", K_MWHEELDOWN},

  {"AUX1", K_AUX1},
  {"AUX2", K_AUX2},
  {"AUX3", K_AUX3},
  {"AUX4", K_AUX4},
  {"AUX5", K_AUX5},
  {"AUX6", K_AUX6},
  {"AUX7", K_AUX7},
  {"AUX8", K_AUX8},
  {"AUX9", K_AUX9},
  {"AUX10", K_AUX10},
  {"AUX11", K_AUX11},
  {"AUX12", K_AUX12},
  {"AUX13", K_AUX13},
  {"AUX14", K_AUX14},
  {"AUX15", K_AUX15},
  {"AUX16", K_AUX16},

  {"KP_HOME", K_KP_HOME},
  {"KP_UPARROW", K_KP_UPARROW},
  {"KP_PGUP", K_KP_PGUP},
  {"KP_LEFTARROW", K_KP_LEFTARROW},
  {"KP_5", K_KP_5},
  {"KP_RIGHTARROW", K_KP_RIGHTARROW},
  {"KP_END", K_KP_END},
  {"KP_DOWNARROW", K_KP_DOWNARROW},
  {"KP_PGDN", K_KP_PGDN},
  {"KP_ENTER", K_KP_ENTER},
  {"KP_INS", K_KP_INS},
  {"KP_DEL", K_KP_DEL},
  {"KP_SLASH", K_KP_SLASH},
  {"KP_MINUS", K_KP_MINUS},
  {"KP_PLUS", K_KP_PLUS},
  {"KP_NUMLOCK", K_KP_NUMLOCK},
  {"KP_STAR", K_KP_STAR},
  {"KP_EQUALS", K_KP_EQUALS},

  {"PAUSE", K_PAUSE},

  {"SEMICOLON", ';'},		// because a raw semicolon seperates commands

  {"WORLD_0", K_WORLD_0},
  {"WORLD_1", K_WORLD_1},
  {"WORLD_2", K_WORLD_2},
  {"WORLD_3", K_WORLD_3},
  {"WORLD_4", K_WORLD_4},
  {"WORLD_5", K_WORLD_5},
  {"WORLD_6", K_WORLD_6},
  {"WORLD_7", K_WORLD_7},
  {"WORLD_8", K_WORLD_8},
  {"WORLD_9", K_WORLD_9},
  {"WORLD_10", K_WORLD_10},
  {"WORLD_11", K_WORLD_11},
  {"WORLD_12", K_WORLD_12},
  {"WORLD_13", K_WORLD_13},
  {"WORLD_14", K_WORLD_14},
  {"WORLD_15", K_WORLD_15},
  {"WORLD_16", K_WORLD_16},
  {"WORLD_17", K_WORLD_17},
  {"WORLD_18", K_WORLD_18},
  {"WORLD_19", K_WORLD_19},
  {"WORLD_20", K_WORLD_20},
  {"WORLD_21", K_WORLD_21},
  {"WORLD_22", K_WORLD_22},
  {"WORLD_23", K_WORLD_23},
  {"WORLD_24", K_WORLD_24},
  {"WORLD_25", K_WORLD_25},
  {"WORLD_26", K_WORLD_26},
  {"WORLD_27", K_WORLD_27},
  {"WORLD_28", K_WORLD_28},
  {"WORLD_29", K_WORLD_29},
  {"WORLD_30", K_WORLD_30},
  {"WORLD_31", K_WORLD_31},
  {"WORLD_32", K_WORLD_32},
  {"WORLD_33", K_WORLD_33},
  {"WORLD_34", K_WORLD_34},
  {"WORLD_35", K_WORLD_35},
  {"WORLD_36", K_WORLD_36},
  {"WORLD_37", K_WORLD_37},
  {"WORLD_38", K_WORLD_38},
  {"WORLD_39", K_WORLD_39},
  {"WORLD_40", K_WORLD_40},
  {"WORLD_41", K_WORLD_41},
  {"WORLD_42", K_WORLD_42},
  {"WORLD_43", K_WORLD_43},
  {"WORLD_44", K_WORLD_44},
  {"WORLD_45", K_WORLD_45},
  {"WORLD_46", K_WORLD_46},
  {"WORLD_47", K_WORLD_47},
  {"WORLD_48", K_WORLD_48},
  {"WORLD_49", K_WORLD_49},
  {"WORLD_50", K_WORLD_50},
  {"WORLD_51", K_WORLD_51},
  {"WORLD_52", K_WORLD_52},
  {"WORLD_53", K_WORLD_53},
  {"WORLD_54", K_WORLD_54},
  {"WORLD_55", K_WORLD_55},
  {"WORLD_56", K_WORLD_56},
  {"WORLD_57", K_WORLD_57},
  {"WORLD_58", K_WORLD_58},
  {"WORLD_59", K_WORLD_59},
  {"WORLD_60", K_WORLD_60},
  {"WORLD_61", K_WORLD_61},
  {"WORLD_62", K_WORLD_62},
  {"WORLD_63", K_WORLD_63},
  {"WORLD_64", K_WORLD_64},
  {"WORLD_65", K_WORLD_65},
  {"WORLD_66", K_WORLD_66},
  {"WORLD_67", K_WORLD_67},
  {"WORLD_68", K_WORLD_68},
  {"WORLD_69", K_WORLD_69},
  {"WORLD_70", K_WORLD_70},
  {"WORLD_71", K_WORLD_71},
  {"WORLD_72", K_WORLD_72},
  {"WORLD_73", K_WORLD_73},
  {"WORLD_74", K_WORLD_74},
  {"WORLD_75", K_WORLD_75},
  {"WORLD_76", K_WORLD_76},
  {"WORLD_77", K_WORLD_77},
  {"WORLD_78", K_WORLD_78},
  {"WORLD_79", K_WORLD_79},
  {"WORLD_80", K_WORLD_80},
  {"WORLD_81", K_WORLD_81},
  {"WORLD_82", K_WORLD_82},
  {"WORLD_83", K_WORLD_83},
  {"WORLD_84", K_WORLD_84},
  {"WORLD_85", K_WORLD_85},
  {"WORLD_86", K_WORLD_86},
  {"WORLD_87", K_WORLD_87},
  {"WORLD_88", K_WORLD_88},
  {"WORLD_89", K_WORLD_89},
  {"WORLD_90", K_WORLD_90},
  {"WORLD_91", K_WORLD_91},
  {"WORLD_92", K_WORLD_92},
  {"WORLD_93", K_WORLD_93},
  {"WORLD_94", K_WORLD_94},
  {"WORLD_95", K_WORLD_95},

  {"WINDOWS", K_SUPER},
  {"COMPOSE", K_COMPOSE},
  {"MODE", K_MODE},
  {"HELP", K_HELP},
  {"PRINT", K_PRINT},
  {"SYSREQ", K_SYSREQ},
  {"SCROLLOCK", K_SCROLLOCK},
  {"BREAK", K_BREAK},
  {"MENU", K_MENU},
  {"POWER", K_POWER},
  {"EURO", K_EURO},
  {"UNDO", K_UNDO},

  {NULL, 0}
};

/*
=============================================================================

EDIT FIELDS

=============================================================================
*/

/*
===================
Field_Draw

Handles horizontal scrolling and cursor blinking
x, y, and width are in pixels
===================
*/
void
Field_VariableSizeDraw (field_t * edit, int x, int y, int width, int size,
			bool showCursor, bool noColorEscape)
{
  int len;
  int drawLen;
  int prestep;
  int cursorChar;
  char str[MAX_STRING_CHARS];
  int i;
  int nwidth;
  vec4_t color;

  // - 1 so there is always a space for the cursor
  drawLen = edit->widthInChars - 1;

  // calculating a new field width, if we have space to enter more text
  nwidth = (int)((float)width / SCR_ConsoleFontCharWidth (" "));
  if (nwidth > edit->widthInChars)
    {
      if ((x + nwidth * SCR_ConsoleFontCharWidth (" ")) < SCREEN_WIDTH)
	{
	  drawLen = nwidth - 1;
	}
    }

  len = strlen (edit->buffer);

  // guarantee that cursor will be visible
  if (len <= drawLen)
    {
      prestep = 0;
    }
  else
    {
      if (edit->scroll + drawLen > len)
	{
	  edit->scroll = len - drawLen;
	  if (edit->scroll < 0)
	    {
	      edit->scroll = 0;
	    }

	  while (Q_UTF8ContByte (edit->buffer[edit->scroll])
		 && (edit->scroll > 0))
	    {
	      edit->scroll--;
	    }
	}
      prestep = edit->scroll;
    }

  if (prestep + drawLen > len)
    {
      drawLen = len - prestep;
    }

  while (Q_UTF8ContByte (edit->buffer[prestep + drawLen])
	 && (prestep + drawLen < len))
    drawLen++;

  // extract <drawLen> characters from the field at <prestep>
  if (drawLen >= MAX_STRING_CHARS)
    {
      CL_Error (ERR_DROP, "drawLen >= MAX_STRING_CHARS");
    }

  Com_Memcpy (str, edit->buffer + prestep, drawLen);
  str[drawLen] = 0;

  color[0] = color[1] = color[2] = color[3] = 1.0;

  // draw the cursor
  // NOTE : cursor position should be processed before the string
  // so that the character under the cursor can be color reversed
  // off blink, toggle every 500ms
  // TODO : find a way to change the highlighted character color
  if (showCursor && ((int) (cls.realtime >> 9) & 1))
    {
      vec4_t cursorcolor;

      if (key_overstrikeMode)
	{
	  cursorChar = 11;
	  cursorcolor[0] = cursorcolor[1] = 1.0f;
	  cursorcolor[2] = 0.2f;
	  cursorcolor[3] = 0.7f;
	}
      else
	{
	  cursorChar = 10;
	  cursorcolor[0] = cursorcolor[1] = cursorcolor[2] = 1.0f;
	  cursorcolor[3] = 0.7f;
	  // override char color
	  color[0] = color[1] = color[2] = 1.0f;
	}

      //i = drawLen - Q_PrintStrlen (str);
      i = drawLen - Q_UTF8Strlen (str);

      if (size == SMALLCHAR_WIDTH)
	{
	  float xlocation;
	  float ylocation;
	  float width, height;

	  if (cls.useLegacyConsoleFont)
	    {
	      SCR_DrawChar (x + (edit->cursor - prestep - i) * size,
			    y, SMALLCHAR_WIDTH, SMALLCHAR_HEIGHT, cursorChar);
	    }
	  else
	    {
	      /*
	      static char c;
	      float xlocation = x + SCR_ConsoleFontStringWidth (str, edit->cursor);
	      c = (char) cursorChar & 0x7F;
	      // we expect a string, and this one is not '\0' terminated
	      SCR_DrawConsoleFontChar (xlocation , y, &c);
	      */
	      xlocation = (float)x + SCR_ConsoleFontStringWidth (str, edit->cursor);
	      ylocation = (float)y - SCR_ConsoleFontCharHeight () * 0.8f;
	      width = SCR_ConsoleFontCharWidth (&str[edit->cursor - prestep - i]);
	      height = SCR_ConsoleFontCharHeight () * 1.1f;

	      // keep the cursor visible
	      if (width < ((SCR_ConsoleFontCharWidth (" ") * 0.5f)))
		{
		  width = (SCR_ConsoleFontCharWidth (" ") * 0.5f);
		}

	      // draw the cursor as a small rect, DejaVu font hasn't the cursor char
	      re.SetColor (cursorcolor);
	      re.DrawStretchPic (xlocation, ylocation, width, height,
				 0, 0, 0, 0, cls.whiteShader);
	      re.SetColor (NULL);

	      // this one might look better and is easier to implement
	      //SCR_DrawConsoleFontChar (xlocation, y, '_');
	    }
	}
      else
	{
	  str[0] = (char)cursorChar;
	  str[1] = 0;
	  SCR_DrawBigString (x + (edit->cursor - prestep - i) * size, y, str,
			     1.0, false);

	}
    }


  Com_Memcpy (str, edit->buffer + prestep, drawLen);
  str[drawLen] = 0;

  // draw it
  if (size == SMALLCHAR_WIDTH)
    {
      SCR_DrawSmallStringExt (x, y, str, color, false, noColorEscape);
    }
  else
    {
      // draw big string with drop shadow
      SCR_DrawBigString (x, y, str, 1.0, noColorEscape);
    }
}

void
Field_Draw (field_t * edit, int x, int y, int width, bool showCursor,
	    bool noColorEscape)
{
  Field_VariableSizeDraw (edit, x, y, width, SMALLCHAR_WIDTH, showCursor,
			  noColorEscape);
}

void
Field_BigDraw (field_t * edit, int x, int y, int width, bool showCursor,
	       bool noColorEscape)
{
  Field_VariableSizeDraw (edit, x, y, width, SMALLCHAR_WIDTH, showCursor,
			  noColorEscape);
}

/*
================
Field_Paste
================
*/
void
Field_Paste (field_t * edit)
{
  char* cbd;
  const char* s;
  int pasteLen, width;

  cbd = (char*) Sys_GetClipboardData ();

  if (cbd == NULL)
    {
      return;
    }

  // send as if typed, so insert / overstrike works properly
  pasteLen = strlen (cbd);

  s = cbd;
  while (pasteLen > 0)
    {
      Field_CharEvent (edit, s);

      width = ((Q_UTF8Width (s) > 0) ? Q_UTF8Width (s) : 1);

      s += width;
      pasteLen -= width;
    }

  qce->mem.Z_Free (cbd);
}

/*
=================
Field_KeyDownEvent

Performs the basic line editing functions for the console,
in-game talk, and menu fields

Key events are used for non-printable characters, others are gotten from char events.
=================
*/
void
Field_KeyDownEvent (field_t * edit, int key)
{
  int len, width;
  char *s;

  // shift-insert is paste
  if (((key == K_INS) || (key == K_KP_INS)) && keys[K_SHIFT].down)
    {
      Field_Paste (edit);
      return;
    }

  key = tolower (key);
  len = strlen (edit->buffer);
  s = &edit->buffer[edit->cursor];
  width = Q_UTF8Width (edit->buffer + edit->cursor);

  switch (key)
    {
    case K_DEL:
      if ((edit->cursor + width) <= len)
	{
	  memmove (edit->buffer + edit->cursor,
		   edit->buffer + edit->cursor + width, len - edit->cursor);
	}
      break;
    case K_BACKSPACE:
      // where is this handle ?
      break;

    case K_RIGHTARROW:
      if ((edit->cursor + width) <= len)
	{
	  edit->cursor += width;
	}
      break;

    case K_LEFTARROW:
      while (edit->cursor > 0)
	{
	  edit->cursor--;
	  if (!Q_UTF8ContByte (edit->buffer[edit->cursor]))
	    break;
	}
      break;

    case K_HOME:
      edit->cursor = 0;
      break;

    case K_END:
      edit->cursor = len;
      while ((s > edit->buffer) && (edit->cursor > 0) && (Q_UTF8ContByte (*s)))
	{
	  edit->cursor--;
	  s--;
	}
      break;

    case K_INS:
      key_overstrikeMode ^= 1;
      break;

    default:
      break;
    }


  // NOTE bu : the next lines are kind of obscure to me

  width = Q_UTF8Width (edit->buffer + edit->scroll);

  // Change scroll if cursor is no longer visible
  if (edit->cursor < edit->scroll)
    {
      edit->scroll = edit->cursor;
    }
  else if ((edit->cursor > (edit->scroll + edit->widthInChars + width))
	   && (edit->cursor <= len))
    {
      edit->scroll = edit->cursor - edit->widthInChars + 1;
      while (Q_UTF8ContByte (edit->buffer[edit->scroll] && (edit->scroll > 0)))
	edit->scroll--;
    }
}

/*
==================
Field_CharEvent
==================
*/
void
Field_CharEvent (field_t * edit, const char *s)
{
  int len, width;

  if (*s == ('v' - 'a' + 1))
    {				// ctrl-v is paste
      Field_Paste (edit);
      return;
    }

  if (*s == ('c' - 'a' + 1))
    {				// ctrl-c clears the field
      qce->fs.Field_Clear (edit);
      return;
    }

  len = strlen (edit->buffer);

  // this also is KP_BACKSPACE, prepared by sdl_input
  if (*s == ('h' - 'a' + 1))
    {				// ctrl-h is backspace
      while (edit->cursor > 0)
	{
	  bool isContinue = Q_UTF8ContByte (edit->buffer[edit->cursor - 1]);

	  memmove (edit->buffer + edit->cursor - 1,
		   edit->buffer + edit->cursor, len + 1 - edit->cursor);
	  edit->cursor--;
	  if (edit->cursor < edit->scroll)
	    {
	      edit->scroll--;
	    }
	  if (!isContinue)
	    {
	      break;
	    }
	}
      return;
    }

  if (*s == ('a' - 'a' + 1))
    {				// ctrl-a is home
      edit->cursor = 0;
      edit->scroll = 0;
      return;
    }

  if (*s == ('e' - 'a' + 1))
    {				// ctrl-e is end
      edit->cursor = len;
      edit->scroll = edit->cursor - edit->widthInChars;
      return;
    }

  //
  // ignore any other non printable chars
  //
  if (*(unsigned char *)s < 32)
    {
      return;
    }

  if (key_overstrikeMode)
    {
      int diff;
      int owidth;

      // leave rom for the leading slash and trailing \0
      if (edit->cursor >= (MAX_EDIT_LINE - 2))
	return;
      // NOTE : bu, this won't work...
      // we will translate the end of the string minus
      // the difference between the new character and the old one
      width = Q_UTF8Width (s);
      owidth = Q_UTF8Width (&edit->buffer[edit->cursor]);
      diff = width - owidth;

      // use edit->cursor or len ?
      if ((edit->cursor + diff) >= (MAX_EDIT_LINE - 2))
	return;

      memmove (edit->buffer + edit->cursor + width,
	       edit->buffer + edit->cursor + owidth,
	       len + 1 - owidth - edit->cursor);

      Com_Memcpy (edit->buffer + edit->cursor, s, width);
      edit->cursor += width;
    }
  else
    {				// insert mode
      width = Q_UTF8Width (s);

      // NOTE bu : i'm almost sure this can be overflowed...
      // slash and \0
      if (len >= (MAX_EDIT_LINE - 2))
	return; // all full

      if ((edit->cursor + width) >= (MAX_EDIT_LINE - 2))
	return;

      memmove (edit->buffer + edit->cursor + width,
	       edit->buffer + edit->cursor, len + 1 - edit->cursor);

      Com_Memcpy (edit->buffer + edit->cursor, s, width);
      edit->cursor += width;
    }

  if (edit->cursor >= edit->widthInChars)
    {
      do
	{
	  edit->scroll++;
	}
      while (Q_UTF8ContByte (edit->buffer[edit->scroll]) && (edit->scroll < edit->cursor));
    }

  if (edit->cursor == len + 1)
    {
      edit->buffer[edit->cursor] = 0;
    }
}

/*
=============================================================================

CONSOLE LINE EDITING

==============================================================================
*/

/*
====================
Console_Key

Handles history and console scrollback
====================
*/
void
Console_Key (int key)
{
  // ctrl-L clears screen
  if (key == 'l' && keys[K_CTRL].down)
    {
      qce->cmd.Cbuf_AddText ("clear\n");
      return;
    }

  // enter finishes the line
  if (key == K_ENTER || key == K_KP_ENTER)
    {
      // if not in the game explicitly prepend a slash if needed
      if ((cls.state != CA_ACTIVE)
	  && (g_consoleField.buffer[0])
	  && (g_consoleField.buffer[0] != '\\')
	  && (g_consoleField.buffer[0] != '/'))
	{
	  memmove (g_consoleField.buffer + 1, g_consoleField.buffer, MAX_EDIT_LINE - 1);
	  g_consoleField.buffer[0] = '\\';
	  g_consoleField.cursor++;
	}

      CL_Printf ("]%s\n", g_consoleField.buffer);

      // leading slash is an explicit command
      if (g_consoleField.buffer[0] == '\\' || g_consoleField.buffer[0] == '/')
	{
	  qce->cmd.Cbuf_AddText (g_consoleField.buffer + 1);	// valid command
	  qce->cmd.Cbuf_AddText ("\n");
	}
      else
	{
	  // other text will be chat messages
	  if (!g_consoleField.buffer[0])
	    {
	      return;		// empty lines just scroll the console without adding to history
	    }
	  else
	    {
	      qce->cmd.Cbuf_AddText ("cmd say ");
	      qce->cmd.Cbuf_AddText (g_consoleField.buffer);
	      qce->cmd.Cbuf_AddText ("\n");
	    }
	}

      // copy line to history buffer
      historyEditLines[nextHistoryLine % COMMAND_HISTORY] = g_consoleField;
      nextHistoryLine++;
      historyLine = nextHistoryLine;

      qce->fs.Field_Clear (&g_consoleField);

      g_consoleField.widthInChars = g_console_field_width;

      CL_SaveConsoleHistory ();
      return;
    }

  // command completion
  if (key == K_TAB)
    {
      qce->fs.Field_AutoComplete (&g_consoleField);
      return;
    }

  // command history (ctrl-p ctrl-n for unix style)
  if (((key == K_MWHEELUP) && (keys[K_SHIFT].down))
      || (key == K_UPARROW) || (key == K_KP_UPARROW)
      || ((tolower (key) == 'p') && keys[K_CTRL].down))
    {
      if ((nextHistoryLine - historyLine < COMMAND_HISTORY) && (historyLine > 0))
	{
	  historyLine--;
	}
      g_consoleField = historyEditLines[historyLine % COMMAND_HISTORY];
      return;
    }

  if (((key == K_MWHEELDOWN) && (keys[K_SHIFT].down))
      || (key == K_DOWNARROW) || (key == K_KP_DOWNARROW)
      || ((tolower (key) == 'n') && keys[K_CTRL].down))
    {
      historyLine++;
      if (historyLine >= nextHistoryLine)
	{
	  historyLine = nextHistoryLine;
	  qce->fs.Field_Clear (&g_consoleField);
	  g_consoleField.widthInChars = g_console_field_width;
	  return;
	}
      g_consoleField = historyEditLines[historyLine % COMMAND_HISTORY];
      return;
    }

  // console scrolling
  if (key == K_PGUP)
    {
      Con_PageUp ();
      return;
    }

  if (key == K_PGDN)
    {
      Con_PageDown ();
      return;
    }

  if (key == K_MWHEELUP)
    {				//----(SA) added some mousewheel functionality to the console
      Con_PageUp ();
      if (keys[K_CTRL].down)
	{			// hold <ctrl> to accelerate scrolling
	  Con_PageUp ();
	  Con_PageUp ();
	}
      return;
    }

  if (key == K_MWHEELDOWN)
    {				//----(SA) added some mousewheel functionality to the console
      Con_PageDown ();
      if (keys[K_CTRL].down)
	{			// hold <ctrl> to accelerate scrolling
	  Con_PageDown ();
	  Con_PageDown ();
	}
      return;
    }

  // ctrl-home = top of console
  if ((key == K_HOME) && (keys[K_CTRL].down))
    {
      Con_Top ();
      return;
    }

  // ctrl-end = bottom of console
  if ((key == K_END) && (keys[K_CTRL].down))
    {
      Con_Bottom ();
      return;
    }

  // pass to the normal editline routine
  Field_KeyDownEvent (&g_consoleField, key);
}

//============================================================================


/*
================
Message_Key

In game talk message
================
*/
void
Message_Key (int key)
{
  char buffer[MAX_STRING_CHARS];

  if (key == K_ESCAPE)
    {
      Key_SetCatcher (Key_GetCatcher () & ~KEYCATCH_MESSAGE);
      qce->fs.Field_Clear (&chatField);
      return;
    }

  if ((key == K_ENTER) || (key == K_KP_ENTER))
    {
      if (chatField.buffer[0] && (cls.state == CA_ACTIVE))
	{
	  if (chat_playerNum != -1)
	    {
	      Com_sprintf (buffer, sizeof (buffer), "tell %i \"%s\"\n",
			   chat_playerNum, chatField.buffer);
	    }
	  else if (chat_team)
	    {
	      Com_sprintf (buffer, sizeof (buffer), "say_team \"%s\"\n",
			   chatField.buffer);
	    }
	  else
	    {
	      Com_sprintf (buffer, sizeof (buffer), "say \"%s\"\n",
			   chatField.buffer);
	    }

	  CL_AddReliableCommand (buffer);
	}
      Key_SetCatcher (Key_GetCatcher () & ~KEYCATCH_MESSAGE);
      qce->fs.Field_Clear (&chatField);
      return;
    }

  Field_KeyDownEvent (&chatField, key);
}

//============================================================================

/*
===================
Key_IsDown
===================
*/
bool
Key_IsDown (int keynum)
{
  if (keynum < 0 || keynum >= MAX_KEYS)
    {
      return false;
    }

  return keys[keynum].down;
}

/*
===================
Key_StringToKeynum

Returns a key number to be used to index keys[] by looking at
the given string.  Single ascii characters return themselves, while
the K_* names are matched up.

0x11 will be interpreted as raw hex, which will allow new controlers
to be configured even if they don't have defined names.
===================
*/
int
Key_StringToKeynum (char *str)
{
  keyname_t *kn;

  if (!str || !str[0])
    {
      return -1;
    }
  if (!str[1])	// single character
    {
      return str[0];
    }

  // check for hex code
  if (strlen (str) == 4)
    {
      int n = Com_HexStrToInt (str);

      if (n >= 0)
	{
	  return n;
	}
    }

  // scan for a text match
  for (kn = keynames; kn->name; kn++)
    {
      if (!Q_stricmp (str, kn->name))
	return kn->keynum;
    }

  return -1;
}

/*
===================
Key_KeynumToString

Returns a string (either a single ascii char, a K_* name, or a 0x11 hex string) for the
given keynum.
===================
*/
char *
Key_KeynumToString (int keynum)
{
  keyname_t *kn;
  static char tinystr[5];
  int i, j;

  if (keynum == -1)
    {
      return "<KEY NOT FOUND>";
    }

  if (keynum < 0 || keynum >= MAX_KEYS)
    {
      return "<OUT OF RANGE>";
    }

  // check for printable ascii (don't use quote)
  if ((keynum > 32) && (keynum < 127) && (keynum != '"') && (keynum != ';'))
    {
      tinystr[0] = keynum;
      tinystr[1] = 0;
      return tinystr;
    }

  // check for a key string
  for (kn = keynames; kn->name; kn++)
    {
      if (keynum == kn->keynum)
	{
	  return kn->name;
	}
    }

  // make a hex string
  i = keynum >> 4;
  j = keynum & 15;

  tinystr[0] = '0';
  tinystr[1] = 'x';
  tinystr[2] = (i > 9) ? (i - 10 + 'a') : (i + '0');
  tinystr[3] = (j > 9) ? (j - 10 + 'a') : (j + '0');
  tinystr[4] = 0;

  return tinystr;
}

/*
===================
Key_SetBinding
===================
*/
void
Key_SetBinding (int keynum, const char *binding)
{
  if (keynum < 0 || keynum >= MAX_KEYS)
    {
      return;
    }

  // free old bindings
  if (keys[keynum].binding)
    {
      qce->mem.Z_Free (keys[keynum].binding);
    }

  // allocate memory for new binding
  keys[keynum].binding = qce->mem.CopyString (binding);

  // consider this like modifying an archived cvar, so the
  // file write will be triggered at the next oportunity
  qce->cvar.Set_ModifiedFlags (qce->cvar.Get_ModifiedFlags () | CVAR_ARCHIVE);
}

/*
===================
Key_GetBinding
===================
*/
char *
Key_GetBinding (int keynum)
{
  if (keynum < 0 || keynum >= MAX_KEYS)
    {
      return "";
    }

  return keys[keynum].binding;
}

/* 
===================
Key_GetKey
===================
*/
int
Key_GetKey (const char *binding)
{
  int i;

  if (binding)
    {
      for (i = 0; i < MAX_KEYS; i++)
	{
	  if (keys[i].binding && !Q_stricmp (binding, keys[i].binding))
	    {
	      return i;
	    }
	}
    }
  return -1;
}

/*
===================
Key_Unbind_f
===================
*/
void
Key_Unbind_f (void)
{
  int b;

  if (qce->cmd.Argc () != 2)
    {
      CL_Printf ("unbind <key> : remove commands from a key\n");
      return;
    }

  b = Key_StringToKeynum (qce->cmd.Argv (1));
  if (b == -1)
    {
      CL_Printf ("\"%s\" isn't a valid key\n", qce->cmd.Argv (1));
      return;
    }

  Key_SetBinding (b, "");
}

/*
===================
Key_Unbindall_f
===================
*/
void
Key_Unbindall_f (void)
{
  int i;

  for (i = 0; i < MAX_KEYS; i++)
    {
      if (keys[i].binding)
	Key_SetBinding (i, "");
    }
}

/*
===================
Key_Bind_f
===================
*/
void
Key_Bind_f (void)
{
  int c, b;

  c = qce->cmd.Argc ();

  if (c < 2)
    {
      CL_Printf ("bind <key> [command] : attach a command to a key\n");
      return;
    }

  b = Key_StringToKeynum (qce->cmd.Argv (1));
  if (b == -1)
    {
      CL_Printf ("\"%s\" isn't a valid key\n", qce->cmd.Argv (1));
      return;
    }

  // display current binding
  if (c == 2)
    {
      if (keys[b].binding)
	CL_Printf ("\"%s\" = \"%s\"\n", qce->cmd.Argv (1), keys[b].binding);
      else
	CL_Printf ("\"%s\" is not bound\n", qce->cmd.Argv (1));
      return;
    }

  // grab the rest of the command line to set the binding
  Key_SetBinding (b, qce->cmd.ArgsFrom (2));
}

/*
============
Key_WriteBindings

Writes lines containing "bind key value"
============
*/
void
Key_WriteBindings (fileHandle_t f)
{
  int i;

  qce->fs.Printf (f, "unbindall\n");

  for (i = 0; i < MAX_KEYS; i++)
    {
      if (keys[i].binding && keys[i].binding[0])
	{
	  qce->fs.Printf (f, "bind %s \"%s\"\n", Key_KeynumToString (i),
			  keys[i].binding);

	}
    }
}

/*
============
Key_Bindlist_f

============
*/
void
Key_Bindlist_f (void)
{
  int i;

  for (i = 0; i < MAX_KEYS; i++)
    {
      if (keys[i].binding && keys[i].binding[0])
	{
	  CL_Printf ("%s \"%s\"\n", Key_KeynumToString (i), keys[i].binding);
	}
    }
}

/*
============
Key_KeynameCompletion
============
*/
void
Key_KeynameCompletion (void (*callback) (const char *s))
{
  int i;

  for (i = 0; keynames[i].name != NULL; i++)
    callback (keynames[i].name);
}

/*
====================
Key_CompleteUnbind
====================
*/
static void
Key_CompleteUnbind (char *args, int argNum)
{
  if (argNum == 2)
    {
      // Skip "unbind "
      char *p = Com_SkipTokens (args, 1, " ");

      if (p > args)
	qce->fs.Field_CompleteKeyname ();
    }
}

/*
====================
Key_CompleteBind
====================
*/
static void
Key_CompleteBind (char *args, int argNum)
{
  char *p;

  if (argNum == 2)
    {
      // Skip "bind "
      p = Com_SkipTokens (args, 1, " ");

      if (p > args)
	qce->fs.Field_CompleteKeyname ();
    }
  else if (argNum >= 3)
    {
      // Skip "bind <key> "
      p = Com_SkipTokens (args, 2, " ");

      if (p > args)
	qce->fs.Field_CompleteCommand (p, true, true);
    }
}

/*
===================
CL_InitKeyCommands
===================
*/
void
CL_InitKeyCommands (void)
{
  // register our functions
  qce->cmd.AddCommand ("bind", Key_Bind_f);
  qce->cmd.SetCommandCompletionFunc ("bind", Key_CompleteBind);
  qce->cmd.AddCommand ("unbind", Key_Unbind_f);
  qce->cmd.SetCommandCompletionFunc ("unbind", Key_CompleteUnbind);
  qce->cmd.AddCommand ("unbindall", Key_Unbindall_f);
  qce->cmd.AddCommand ("bindlist", Key_Bindlist_f);
}

/*
===================
CL_ParseBinding

Execute the commands in the bind string
===================
*/
void
CL_ParseBinding (int key, bool down, unsigned time)
{
  char buf[MAX_STRING_CHARS];
  char *p = buf;
  char *end;

  if (!keys[key].binding || !keys[key].binding[0])
    return;

  Q_strncpyz (buf, keys[key].binding, sizeof (buf));

  while (1)
    {
      while (isspace (*p))
	p++;

      // multiple commands
      end = strchr (p, ';');
      if (end)
	*end = '\0';

      if (*p == '+')
	{
	  // button commands add keynum and time as parameters
	  // so that multiple sources can be discriminated and
	  // subframe corrected
	  char *cmd;
	  cmd = va ("%c%s %d %d\n", ((down) ? '+' : '-'), p + 1, key, time);
	  qce->cmd.Cbuf_AddText (cmd);
	}
      else if (down)
	{
	  // normal commands only execute on key press
	  qce->cmd.Cbuf_AddText (p);
	  qce->cmd.Cbuf_AddText ("\n");
	}

      if (!end)
	break;
      p = end + 1;
    }
}

/*
===================
CL_KeyDownEvent

Called by CL_KeyEvent to handle a keypress
===================
*/
void
CL_KeyDownEvent (int key, unsigned time)
{
  keys[key].down = true;
  keys[key].repeats++;
  if (keys[key].repeats == 1)
    anykeydown++;

  if (keys[K_ALT].down && (key == K_ENTER))
    {
      int val = qce->cvar.VariableIntegerValue ("r_fullscreen");
      qce->cvar.Set ("r_fullscreen", va ("%i", !val));
      return;
    }

  // console key is hardcoded, so the user can never unbind it
  if (key == K_CONSOLE || (keys[K_SHIFT].down && (key == K_ESCAPE)))
    {
      Con_ToggleConsole_f ();
      Key_ClearStates ();
      return;
    }

  // escape is always handled special
  if (key == K_ESCAPE)
    {
      if (Key_GetCatcher () & KEYCATCH_MESSAGE)
	{
	  // clear message mode
	  Message_Key (key);
	  return;
	}
    }

  // distribute the key down event to the apropriate handler
  // if we aren't connected, show up the console
  if (Key_GetCatcher () & KEYCATCH_CONSOLE)
    {
      Console_Key (key);
    }
  else if (Key_GetCatcher( ) & KEYCATCH_MESSAGE)
    {
      Message_Key (key);
    }
  else
    {
      // send the bound action
      CL_ParseBinding (key, true, time);
    }
  return;
}

/*
===================
CL_KeyUpEvent

Called by CL_KeyEvent to handle a keyrelease
===================
*/
void CL_KeyUpEvent( int key, unsigned time )
{
  keys[key].repeats = 0;
  keys[key].down = false;
  anykeydown--;
  if (anykeydown < 0)
    {
      anykeydown = 0;
    }

  // don't process key-up events for the console key
  if (key == K_CONSOLE || (key == K_ESCAPE && keys[K_SHIFT].down))
    return;

  //
  // key up events only perform actions if the game key binding is
  // a button command (leading + sign).  These will be processed even in
  // console mode and menu mode, to keep the character from continuing
  // an action started before a mode switch.
  //
  if (cls.state != CA_DISCONNECTED)
    CL_ParseBinding (key, false, time);
}

/*
===================
CL_KeyEvent

Called by the system for both key up and key down events
===================
*/
void
CL_KeyEvent (int key, bool down, unsigned time)
{
  if (down)
    CL_KeyDownEvent (key, time);
  else
    CL_KeyUpEvent (key, time);
}

/*
===================
CL_CharEvent

Normal keyboard characters, already shifted / capslocked / etc
===================
*/
void
CL_CharEvent (const char *key)
{
  // delete is not a printable character and is
  // otherwise handled by Field_KeyDownEvent
  if (*key == K_BACKSPACE)
    {
      return;
    }

  // distribute the key down event to the apropriate handler
  if (Key_GetCatcher () & KEYCATCH_CONSOLE)
    {
      Field_CharEvent (&g_consoleField, key);
    }
  else if (Key_GetCatcher () & KEYCATCH_MESSAGE)
    {
      Field_CharEvent (&chatField, key);
    }
}

/*
===================
Key_ClearStates
===================
*/
void
Key_ClearStates (void)
{
  int i;

  anykeydown = 0;

  for (i = 0; i < MAX_KEYS; i++)
    {
      if (keys[i].down)
	{
	  CL_KeyEvent (i, false, 0);
	}
      keys[i].down = 0;
      keys[i].repeats = 0;
    }
}

/*
====================
Key_GetCatcher
====================
*/
int
Key_GetCatcher (void)
{
  return keyCatchers;
}

/*
====================
Key_SetCatcher
====================
*/
void
Key_SetCatcher (int catcher)
{
  // If the catcher state is changing, clear all key states
  if (catcher != keyCatchers)
    Key_ClearStates ();

  keyCatchers = catcher;
}

// ======================================================
// Console History

// This must not exceed MAX_CMD_LINE
#define MAX_CONSOLE_SAVE_BUFFER 1024
#define CONSOLE_HISTORY_FILE "q3history"
static char consoleSaveBuffer[MAX_CONSOLE_SAVE_BUFFER];
static int consoleSaveBufferSize = 0;

/*
================
CL_LoadConsoleHistory

Load the console history from cl_consoleHistory
================
*/
void
CL_LoadConsoleHistory (void)
{
  char *token, *text_p;
  int i, numLines = 0;
  uint numChars;
  fileHandle_t f;

  consoleSaveBufferSize = qce->fs.FOpenFileRead (CONSOLE_HISTORY_FILE, &f, false);
  if (!f) // same as testing (consoleSaveBufferSize == -1)
    {
      CL_Printf ("Couldn't open/read %s.\n", CONSOLE_HISTORY_FILE);
      return;
    }

  if (consoleSaveBufferSize <= MAX_CONSOLE_SAVE_BUFFER)
    {
      int size;

      size = qce->fs.Read (consoleSaveBuffer, consoleSaveBufferSize, f);
      if (size != consoleSaveBufferSize)
	{
	  CL_Printf ("Couldn't read the whole file %s.\n", CONSOLE_HISTORY_FILE);
	  qce->fs.FCloseFile (f);
	  return;
	}

      text_p = consoleSaveBuffer;

      for (i = COMMAND_HISTORY - 1; i >= 0; i--)
	{
	  token = COM_Parse (&text_p);
	  if (*token == 0)
	    break;

	  historyEditLines[i].cursor = atoi (token);

	  token = COM_Parse (&text_p);
	  if (*token == 0)
	    break;

	  historyEditLines[i].scroll = atoi (token);

	  token = COM_Parse (&text_p);
	  if (*token == 0)
	    break;

	  numChars = atoi (token);
	  text_p++;
	  if (numChars > (strlen (consoleSaveBuffer) - (text_p - consoleSaveBuffer)))
	    {
	      CL_DPrintf (S_COLOR_YELLOW "WARNING: probable corrupt history\n");
	      break;
	    }
	  strncpy (historyEditLines[i].buffer, text_p, numChars);
	  historyEditLines[i].buffer[numChars] = '\0';
	  text_p += numChars;

	  numLines++;
	}

      memmove (&historyEditLines[0], &historyEditLines[i + 1],
	       numLines * sizeof (field_t));

      for (i = numLines; i < COMMAND_HISTORY; i++)
	qce->fs.Field_Clear (&historyEditLines[i]);

      historyLine = nextHistoryLine = numLines;
    }

  qce->fs.FCloseFile (f);
}

/*
================
CL_SaveConsoleHistory

Save the console history into the cvar cl_consoleHistory
so that it persists across invocations of q3
================
*/
void
CL_SaveConsoleHistory (void)
{
  int i;
  int lineLength, saveBufferLength, additionalLength;
  fileHandle_t f;
  char *msg;

  consoleSaveBuffer[0] = '\0';

  i = (nextHistoryLine - 1) % COMMAND_HISTORY;
  do
    {
      if (historyEditLines[i].buffer[0])
	{
	  lineLength = strlen (historyEditLines[i].buffer);
	  saveBufferLength = strlen (consoleSaveBuffer);

	  msg = va ("%d %d %d %s ", historyEditLines[i].cursor,
		    historyEditLines[i].scroll, lineLength,
		    historyEditLines[i].buffer);

	  additionalLength = strlen (msg);

	  if ((saveBufferLength + additionalLength) < MAX_CONSOLE_SAVE_BUFFER)
	    {
	      Q_strcat (consoleSaveBuffer, MAX_CONSOLE_SAVE_BUFFER, msg);
	    }
	  else
	    break;
	}
      i = (i - 1 + COMMAND_HISTORY) % COMMAND_HISTORY;
    }
  while (i != (nextHistoryLine - 1) % COMMAND_HISTORY);

  consoleSaveBufferSize = strlen (consoleSaveBuffer);

  f = qce->fs.FOpenFileWrite (CONSOLE_HISTORY_FILE);
  if (!f)
    {
      CL_Printf ("Couldn't write %s.\n", CONSOLE_HISTORY_FILE);
      return;
    }

  lineLength = qce->fs.Write (consoleSaveBuffer, consoleSaveBufferSize, f);
  if (lineLength < consoleSaveBufferSize)
    CL_Printf ("Couldn't write all the file %s.\n", CONSOLE_HISTORY_FILE);

  qce->fs.FCloseFile (f);
}

/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/

#include "tr_local.h"

/*
========================================================================

TGA files are used for 24/32 bit images

========================================================================
*/

// Targa is stored in little-endian order

#define TGA_COLOR_MAP_ABSENT    0
#define TGA_COLOR_MAP_PRESENT   1

#define TGA_IMAGE_TYPE_NONE          0	// no image data
#define TGA_IMAGE_TYPE_COLORMAP      1	// uncompressed, color-mapped
#define TGA_IMAGE_TYPE_BGR           2	// uncompressed, true-color
#define TGA_IMAGE_TYPE_MONO          3	// uncompressed, black and white
#define TGA_IMAGE_TYPE_COLORMAP_RLE  9	// run-length, color-mapped
#define TGA_IMAGE_TYPE_BGR_RLE      10	// run-length, true-color
#define TGA_IMAGE_TYPE_MONO_RLE     11	// run-length, black and white


typedef struct _TargaHeader
{
  unsigned char id_length;
  /* color map = palette */
  unsigned char colormap_type;
  unsigned char image_type;

  /* color map specification */
  unsigned short colormap_index;
  unsigned short colormap_length;
  unsigned char colormap_size;

  /* image specification */
  unsigned short x_origin;
  unsigned short y_origin;
  unsigned short width;
  unsigned short height;
  unsigned char pixel_size;

  /* bits 0,1,2,3 - attribute bits per pixel
   * bit  4       - set if image is stored right-to-left
   * bit  5       - set if image is stored top-to-bottom
   * bits 6,7     - unused (must be set to zero)
   */
  unsigned char attributes;
} TargaHeader;

void
R_LoadTGA (const char *name, byte ** pic, int *width, int *height)
{
  unsigned columns, rows, numPixels;
  byte *pixbuf;
  int row, column;
  byte *buf_p;
  byte *end;
  TargaHeader targa_header;
  byte *targa_rgba = NULL;
  int length;
  union
  {
    byte *b;
    void *v;
  } buffer;

  *pic = NULL;

  /*
  if ((width == NULL) || (height == NULL))
    return;

  if (width)
    *width = 0;
  if (height)
    *height = 0;
  */

  assert ((width != NULL));
  assert ((height != NULL));

  //
  // load the file
  //
  length = ri.FS_ReadFile ((char *) name, &buffer.v);
  if (!buffer.b || length < 0)
    {
      return;
    }

  if (length < 18)
    {
      ri.Error (ERR_DROP, "LoadTGA: header too short (%s)\n", name);
    }

  buf_p = buffer.b;
  end = buffer.b + length;

  targa_header.id_length = buf_p[0];
  targa_header.colormap_type = buf_p[1];
  if (targa_header.colormap_type != TGA_COLOR_MAP_ABSENT)
    {
      ri.Error (ERR_DROP,
		"LoadTGA : header : bad cmap_type : colormap not supported (%s)\n",
		name);
    }

  targa_header.image_type = buf_p[2];
  if (targa_header.image_type == TGA_IMAGE_TYPE_NONE)
    {
      ri.Error (ERR_DROP, "LoadTGA : header : no imagge (%s)\n", name);
    }

  memcpy (&targa_header.colormap_index, &buf_p[3], 2);
  memcpy (&targa_header.colormap_length, &buf_p[5], 2);
  memcpy (&targa_header.x_origin, &buf_p[8], 2);
  memcpy (&targa_header.y_origin, &buf_p[10], 2);
  memcpy (&targa_header.width, &buf_p[12], 2);
  memcpy (&targa_header.height, &buf_p[14], 2);

  /*
     // why can't we make a direct assignement on x86 ?
     targa_header.colormap_index = (unsigned short)buf_p[3];
     targa_header.colormap_length = (unsigned short)buf_p[5];
     targa_header.x_origin = (unsigned short)buf_p[8];
     targa_header.y_origin = (unsigned short)buf_p[10];
     targa_header.width = (unsigned short)buf_p[12];
     targa_header.height = (unsigned short)buf_p[14];
   */
  targa_header.colormap_size = buf_p[7];
  targa_header.pixel_size = buf_p[16];
  targa_header.attributes = buf_p[17];

  targa_header.colormap_index = LittleShort (targa_header.colormap_index);
  targa_header.colormap_length = LittleShort (targa_header.colormap_length);
  targa_header.x_origin = LittleShort (targa_header.x_origin);
  targa_header.y_origin = LittleShort (targa_header.y_origin);
  targa_header.width = LittleShort (targa_header.width);
  targa_header.height = LittleShort (targa_header.height);

  buf_p += 18;

  if (targa_header.image_type != TGA_IMAGE_TYPE_BGR
      && targa_header.image_type != TGA_IMAGE_TYPE_BGR_RLE
      && targa_header.image_type != TGA_IMAGE_TYPE_MONO
      && targa_header.image_type != TGA_IMAGE_TYPE_MONO_RLE)
    {
      ri.Error (ERR_DROP,
		"LoadTGA: Only type 2 (RGB), 3 (gray), and 10 (RGB) TGA images supported\n");
    }

  if ((targa_header.pixel_size != 32 && targa_header.pixel_size != 24)
      && targa_header.image_type != TGA_IMAGE_TYPE_MONO
      && targa_header.image_type != TGA_IMAGE_TYPE_MONO_RLE)
    {
      ri.Error (ERR_DROP,
		"LoadTGA: Only 32 or 24 bit images supported (no colormaps) or Mono 8bits images\n");
    }

  columns = targa_header.width;
  rows = targa_header.height;
  numPixels = columns * rows * 4;

  if (!columns || !rows || numPixels > 0x7FFFFFFF
      || numPixels / columns / 4 != rows)
    {
      ri.Error (ERR_DROP, "LoadTGA: %s has an invalid image size\n", name);
    }


  targa_rgba = ri.Malloc (numPixels);
  if (targa_rgba == NULL)
    {
      ri.Error (ERR_DROP, "LoadTGA : unnable to allocate memory for %s\n",
		name);
    }

  if (targa_header.id_length != 0)
    {
      if (buf_p + targa_header.id_length > end)
	ri.Error (ERR_DROP,
		  "LoadTGA: header too short (%s) to skip comments\n", name);

      buf_p += targa_header.id_length;	// skip TARGA image comment
    }

  // note : conversion errors from uint -> int can't happend
  // because values come from a short
  // uncompressed TGA (RGB or Gray scale)
  if (targa_header.image_type == TGA_IMAGE_TYPE_BGR
      || targa_header.image_type == TGA_IMAGE_TYPE_MONO)
    {
      if (buf_p + columns * rows * targa_header.pixel_size / 8 > end)
	{
	  ri.Error (ERR_DROP, "LoadTGA: file truncated (%s)\n", name);
	}

      // Uncompressed RGB or gray scale image
      // we start the work here
      for (row = (int) (rows - 1); row >= 0; row--)
	{
	  pixbuf = targa_rgba + row * columns * 4;
	  for (column = 0; column < (int) columns; column++)
	    {
	      unsigned char red, green, blue, alphabyte;
	      switch (targa_header.pixel_size)
		{
		case 8:
		  blue = *buf_p++;
		  green = blue;
		  red = blue;
		  *pixbuf++ = red;
		  *pixbuf++ = green;
		  *pixbuf++ = blue;
		  *pixbuf++ = 255;
		  break;
		case 16:
		  blue = *buf_p++;
		  green = blue;
		  red = blue;
		  alphabyte = *buf_p++;
		  *pixbuf++ = red;
		  *pixbuf++ = green;
		  *pixbuf++ = blue;
		  *pixbuf++ = alphabyte;
		  break;
		case 24:
		  blue = *buf_p++;
		  green = *buf_p++;
		  red = *buf_p++;
		  *pixbuf++ = red;
		  *pixbuf++ = green;
		  *pixbuf++ = blue;
		  *pixbuf++ = 255;
		  break;
		case 32:
		  blue = *buf_p++;
		  green = *buf_p++;
		  red = *buf_p++;
		  alphabyte = *buf_p++;
		  *pixbuf++ = red;
		  *pixbuf++ = green;
		  *pixbuf++ = blue;
		  *pixbuf++ = alphabyte;
		  break;
		default:
		  ri.Error (ERR_DROP,
			    "LoadTGA: illegal pixel_size '%d' in file '%s'\n",
			    targa_header.pixel_size, name);
		  break;
		}
	    }
	}
    }
  else if (targa_header.image_type == TGA_IMAGE_TYPE_BGR_RLE
	   || targa_header.image_type == TGA_IMAGE_TYPE_MONO_RLE)
    {				// Runlength encoded RGB images
      unsigned char red, green, blue, alphabyte, packetHeader, packetSize, j;

      red = 0;
      green = 0;
      blue = 0;
      alphabyte = 0xff;

      for (row = (int) (rows - 1); row >= 0; row--)
	{
	  pixbuf = targa_rgba + row * columns * 4;
	  for (column = 0; column < (int) columns;)
	    {
	      if (buf_p + 1 > end)
		ri.Error (ERR_DROP, "LoadTGA: file truncated (%s)\n", name);
	      packetHeader = *buf_p++;
	      packetSize = 1 + (packetHeader & 0x7f);
	      if (packetHeader & 0x80)
		{		// run-length packet
		  if (buf_p + targa_header.pixel_size / 8 > end)
		    ri.Error (ERR_DROP, "LoadTGA: file truncated (%s)\n",
			      name);
		  switch (targa_header.pixel_size)
		    {
		    case 8:
		      blue = *buf_p++;
		      green = blue;
		      red = blue;
		      alphabyte = 255;
		      break;
		    case 16:
		      blue = *buf_p++;
		      green = blue;
		      red = blue;
		      alphabyte = *buf_p++;
		      break;
		    case 24:
		      blue = *buf_p++;
		      green = *buf_p++;
		      red = *buf_p++;
		      alphabyte = 255;
		      break;
		    case 32:
		      blue = *buf_p++;
		      green = *buf_p++;
		      red = *buf_p++;
		      alphabyte = *buf_p++;
		      break;
		    default:
		      ri.Error (ERR_DROP,
				"LoadTGA RLE: illegal pixel_size '%d' in file '%s'\n",
				targa_header.pixel_size, name);
		      break;
		    }

		  for (j = 0; j < packetSize; j++)
		    {
		      *pixbuf++ = red;
		      *pixbuf++ = green;
		      *pixbuf++ = blue;
		      *pixbuf++ = alphabyte;
		      column++;
		      if (column == (int) columns)
			{	// run spans across rows
			  column = 0;
			  if (row > 0)
			    row--;
			  else
			    goto breakOut;
			  pixbuf = targa_rgba + row * columns * 4;
			}
		    }
		}
	      else
		{		// non run-length packet
		  if (buf_p + targa_header.pixel_size / 8 * packetSize > end)
		    ri.Error (ERR_DROP, "LoadTGA: file truncated (%s)\n",
			      name);
		  for (j = 0; j < packetSize; j++)
		    {
		      switch (targa_header.pixel_size)
			{
			case 8:
			  blue = *buf_p++;
			  green = blue;
			  red = blue;
			  *pixbuf++ = red;
			  *pixbuf++ = green;
			  *pixbuf++ = blue;
			  *pixbuf++ = 255;
			  break;
			case 16:
			  blue = *buf_p++;
			  green = blue;
			  red = blue;
			  alphabyte = *buf_p++;
			  *pixbuf++ = red;
			  *pixbuf++ = green;
			  *pixbuf++ = blue;
			  *pixbuf++ = alphabyte;
			  break;
			case 24:
			  blue = *buf_p++;
			  green = *buf_p++;
			  red = *buf_p++;
			  *pixbuf++ = red;
			  *pixbuf++ = green;
			  *pixbuf++ = blue;
			  *pixbuf++ = 255;
			  break;
			case 32:
			  blue = *buf_p++;
			  green = *buf_p++;
			  red = *buf_p++;
			  alphabyte = *buf_p++;
			  *pixbuf++ = red;
			  *pixbuf++ = green;
			  *pixbuf++ = blue;
			  *pixbuf++ = alphabyte;
			  break;
			default:
			  ri.Error (ERR_DROP,
				    "LoadTGA RLE: illegal pixel_size '%d' in file '%s'\n",
				    targa_header.pixel_size, name);
			  break;
			}
		      column++;
		      if (column == (int) columns)
			{	// pixel packet run spans across rows
			  column = 0;
			  if (row > 0)
			    row--;
			  else
			    goto breakOut;
			  pixbuf = targa_rgba + row * columns * 4;
			}
		    }
		}
	    }
	breakOut:;
	}
    }

#if 0
  // TTimo: this is the chunk of code to ensure a behavior that meets TGA specs 
  // bit 5 set => top-down
  if (targa_header.attributes & 0x20)
    {
      unsigned char *flip = (unsigned char *) malloc (columns * 4);
      unsigned char *src, *dst;

      for (row = 0; row < rows / 2; row++)
	{
	  src = targa_rgba + row * 4 * columns;
	  dst = targa_rgba + (rows - row - 1) * 4 * columns;

	  memcpy (flip, src, columns * 4);
	  memcpy (src, dst, columns * 4);
	  memcpy (dst, flip, columns * 4);
	}
      free (flip);
    }
#endif
  // instead we just print a warning
  if (targa_header.attributes & 0x20)
    {
      ri.Printf (PRINT_WARNING,
		 "WARNING: '%s' TGA file header declares top-down image, ignoring\n",
		 name);
    }

  // checks made at function entrance
  *width = columns;
  *height = rows;

  *pic = targa_rgba;

  ri.FS_FreeFile (buffer.v);
}

/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
// tr_font.c
//

#include "tr_local.h"
//#include "qcommon/qcommon.h"

// ttf for tr_font
#ifdef USE_LOCAL_HEADERS
	#include "ft2build.h"
#else
	#include <ft2build.h>
#endif
#include FT_FREETYPE_H
//#include FT_OUTLINE_H
#include FT_GLYPH_H

FT_Library ftLibrary = NULL;

// get some space between glyphs because floatting p. texturing isn't precise
#define MEGABUF_X_INTERLINE 4
#define MEGABUF_Y_INTERLINE 8

/*
=======================
RE_CalculateMegaBufPos
=======================
*/
void
RE_CalculateMegaBufPos (fontFace_t *face, wchar_t val, int * resx, int * resy)
{
  int index;
  int x;
  int y;

  assert ((GLYPH_CHARSTART <= val) && (val < GLYPH_CHAREND));

  index = val - GLYPH_CHARSTART;

  x = (index % 12) * (face->bbox_xmax + MEGABUF_X_INTERLINE);
  y = (index / 12) * (face->bbox_ymax + MEGABUF_Y_INTERLINE);

  *resx = x;
  *resy = y;
}

/*
==================
RE_ConstructGlyph
==================
*/
static unsigned char *
RE_ConstructGlyph (FT_Face face, faceGlyph_t *glyph, FT_ULong c)
{
  int i, j;
  FT_UInt ci;
  FT_Glyph ftGlyph;
  int w, h;
  FT_BitmapGlyph bitmap_glyph;
  FT_Glyph_Metrics *met;
  unsigned char *buffer;

  // make sure everything is here
  assert (face != NULL);
  assert (glyph != NULL);
  assert (glyph->oglregistered == false);

  // get the char index in the charmap
  ci = FT_Get_Char_Index (face, c);
  if (!ci)
    {
      ri.Printf (PRINT_ALL, "Unrecognized character : %u\n", c);
      // dont return, use the nullchar
      // return;
    }

  // load the glyph image from the given index
  FT_Load_Glyph (face, ci, FT_LOAD_DEFAULT);

  // get a copy of the glyph (ftGlyph)
  if (FT_Get_Glyph (face->glyph, &ftGlyph))
    {
      // error
      ri.Printf (PRINT_ALL, "FT_Get_Glyph failed - %lc - %d\n", (wint_t)c, ci);
      return NULL;
    }

  // we have a regular ascii glyph, already non initialized
  // generate a bitmap from the glyph and free old glyph's memory
  FT_Glyph_To_Bitmap (&ftGlyph, FT_RENDER_MODE_NORMAL, 0, 1);

  // cast to the bitmapGlyph structure
  bitmap_glyph = (FT_BitmapGlyph)ftGlyph;

  // shortcut to access the glyph metric
  met = &face->glyph->metrics;

  // save metrics
  glyph->width = met->width / 64;
  glyph->height = met->height / 64;

  glyph->bearingX = met->horiBearingX / 64;
  glyph->bearingY = met->horiBearingY / 64;
  glyph->advance = met->horiAdvance / 64;

  assert (glyph->width == bitmap_glyph->bitmap.width);
  assert (glyph->height == bitmap_glyph->bitmap.rows);

  glyph->left = face->glyph->bitmap_left;
  glyph->top = face->glyph->bitmap_top;

  w = glyph->width;
  h = glyph->height;

  unsigned char *buf = (unsigned char *)bitmap_glyph->bitmap.buffer;

  // allocate memory to store the glyph
  buffer = ri.Hunk_AllocateTempMemory (sizeof (unsigned char) * h * w * 2);

  if (buffer)
    {
      // fill the image buffer
      for (i = 0; i < h; i++)
	{
	  for (j = 0; j < w; j++)
	    {
	      buffer [(2 * (j + i * w))] = (unsigned char)255;

	      // we need to flip the up/down orientation for OpenGL
	      //buffer [(2 * (j + i * w)) + 1] = buf[j + w * (h - i - 1)];
	      buffer [(2 * (j + i * w)) + 1] = buf[j +  i * w];
	    }
	}
    }

  // free the memory
  FT_Done_Glyph (ftGlyph);

  return buffer;
}

/*
===================
RE_RegisterMegaBuf
===================
*/
static void
RE_RegisterMegaBuf (unsigned char * buffer, unsigned int width,
		    unsigned int height, unsigned int texture)
{
  // storing the texture in the opengl buffer
  qglBindTexture (GL_TEXTURE_2D, texture);
  qglTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  qglTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  qglTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, width, height,
		 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, buffer);

  qglBindTexture (GL_TEXTURE_2D, 0);
}

/*
=================
RE_RegisterGlyph

the idea behind the translation in the list is to be able
to make grouped listcalls for a whole line
=================
*/
static void
RE_RegisterGlyph (faceGlyph_t * glyph, unsigned char * buffer, unsigned int index, unsigned int texture)
{
  // storing the texture in the opengl buffer
  qglBindTexture (GL_TEXTURE_2D, texture);
  qglTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  qglTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  qglTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, glyph->width, glyph->height,
		 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, buffer);

  qglNewList (index, GL_COMPILE);

  qglBindTexture (GL_TEXTURE_2D, texture);

  // align against a line
  qglTranslated (glyph->bearingX, -glyph->bearingY, 0);

  qglBegin (GL_QUADS);
  qglTexCoord2f (0.0, 0.0); qglVertex2d (0.0, 0.0);
  qglTexCoord2f (0, 1.0); qglVertex2d (0.0, glyph->height);
  qglTexCoord2f (1.0, 1.0);
  qglVertex2d (glyph->width, glyph->height);
  qglTexCoord2f (1.0, 0.0);
  qglVertex2d (glyph->width, 0.0);
  qglEnd ();

  // advance and restore previous pos
  qglTranslated (glyph->advance - glyph->bearingX, glyph->bearingY, 0);

  qglEndList ();

  glyph->oglregistered = true;
}

/*
=============
RE_LoadGlyph
=============
*/
static void
RE_LoadGlyph (fontFace_t *fontface, faceGlyph_t * glyph,
	      wchar_t val, bool cached, void *megabuf)
{
  FT_Face face;
  void *buffer;

  if (!fontface)
    return;

  // already registered
  if (glyph->oglregistered)
    {
      ri.Printf (PRINT_ALL, "Already registered : %lc\n", val);
      return;
    }

  // grab the Face index
  face = (FT_Face) fontface->ftFace;

  buffer = RE_ConstructGlyph (face, glyph, val);

  if (buffer)
    {
      // storing the texture in the opengl buffer
      // REMOVE ME : glyph->index shouldn't be used anymore
      if (cached)
	{
	  RE_RegisterGlyph (glyph, buffer,
			    fontface->cacheListOffset + glyph->index,
			    fontface->cacheTextures[glyph->index]);
	}
      else
	{
	  int index;
	  int index2;
	  int i, j;
	  int x, y;
	  float textotx, textoty;

	  // copying on the megatexture
	  RE_CalculateMegaBufPos (fontface, val, &x, &y);

	  textotx = (float) fontface->megabuf_width;
	  textoty = (float) fontface->megabuf_height;

	  glyph->texoffsetx = x;
	  glyph->texoffsety = y;

	  glyph->upx = x / textotx;
	  glyph->upy = y / textoty;

	  glyph->dox = glyph->upx + (float)glyph->width / textotx;
	  glyph->doy = glyph->upy + (float)glyph->height / textoty;

	  unsigned char *pic = (unsigned char *) megabuf;
	  unsigned char *buf = (unsigned char *) buffer;

	  for (i = 0; i < glyph->height; i++)
	    {
	      for (j = 0; j < glyph->width; j++)
		{
		  index = (2 * (x + (y + i) * fontface->megabuf_width + j));
		  index2 = (2 * (j + i * glyph->width));
		  pic [index] = buf [index2];
		  pic [index + 1] = buf [index2 + 1];
		}
	    }
	}

      ri.Hunk_FreeTempMemory (buffer);
    }
}

/*
==================
RE_GetCachedGlyph
==================
*/
faceGlyph_t *
RE_GetCachedGlyph (fontFace_t *face, wchar_t val)
{
  faceGlyph_t *glyph;
  unsigned int i;

  glyph = NULL;
  // look in the cache for the glyph associated
  for (i = 0; i < GLYPHS_CACHE; i++)
    {
      if (face->cachePair[i] == val)
	{
	  glyph = &face->glyphs[GLYPHS_PER_FONT + i];
	  break;
	}
    }

  return glyph;
}

/*
=======================
RE_RegisterCachedGlyph
=======================
*/
faceGlyph_t *
RE_RegisterCachedGlyph (fontFace_t *face, wchar_t val)
{
  faceGlyph_t *glyph;
  int index;

  // grab a new cachable index
  index = face->cacheIndex % GLYPHS_CACHE;
  face->cacheIndex++;

  glyph = &face->glyphs[GLYPHS_PER_FONT + index];

  // the glyph was already registered to another char, freeing it
  if (glyph->oglregistered)
    {
      ri.Printf (PRINT_ALL, "Free Old Cache\n");
      glyph->oglregistered = false;
    }

  // update the cache pair table
  face->cachePair[index] = val;

  glyph->index = index;

  // generate the glyph
  RE_LoadGlyph (face, glyph, val, true, NULL);

  return glyph;
}

/*
====================
RE_GetGlyph

look for registered ascii glyph
then for existing cached glyph
otherwise create the glyph
====================
*/
faceGlyph_t *
RE_GetGlyph (fontFace_t *face, wchar_t val)
{
  faceGlyph_t *glyph;

  // todo ? filter val < GLYPH_CHARSTART to null char ?
  if ((GLYPH_CHARSTART <= val) && (val < GLYPH_CHAREND))
    {
      glyph = &face->glyphs[val - GLYPH_CHARSTART];
    }
  else
    {
      glyph = RE_GetCachedGlyph (face, val);

      if (glyph == NULL)
	{
	  glyph = RE_RegisterCachedGlyph (face, val);
	}
    }

  assert (glyph != NULL);

  return glyph;
}

// =====================================================

static unsigned long
stream_read (FT_Stream stream, unsigned long offset,
	     unsigned char *buffer, unsigned long count)
{
  if (count == 0)
    {
      int r;

      r = ri.FS_Seek ((fileHandle_t) stream->descriptor.value, (long) offset, FS_SEEK_SET);

      if (r == offset)
	return 0;
      else
	return r;
    }

  if (!buffer)
    {
      ri.Printf (PRINT_ALL, "stream_read: buffer is NULL\n");
      return 0;
    }

  if (!stream->descriptor.value)
    {
      ri.Printf (PRINT_ALL, "stream_read: stream->descriptor.value is zero\n");
      return 0;
    }

  if (ri.FS_FTell ((fileHandle_t) stream->descriptor.value ) != (long)offset)
    {
      ri.FS_Seek ((fileHandle_t) stream->descriptor.value, (long) offset, FS_SEEK_SET);
    }

  return ri.FS_Read ((char *) buffer, (long) count, (fileHandle_t) stream->descriptor.value);
}

static void
stream_close (FT_Stream stream)
{
  ri.FS_FCloseFile ((fileHandle_t) stream->descriptor.value);
}

// this should use the engine memory handler
static void *
memory_alloc (FT_Memory memory, long size)
{
  return malloc (size);
}

static void
memory_free (FT_Memory memory, void *block)
{
  free (block);
}

static void *
memory_realloc (FT_Memory memory, long cur_size, long new_size, void *block)
{
  return realloc (block, new_size);
}

/*
============
RE_LoadFace
============
*/
void
RE_LoadFace (const char *fileName, int pointSize, const char *name, fontFace_t *face)
{
  fileHandle_t h;
  static struct FT_MemoryRec_ memory =
    {
      NULL,
      memory_alloc,
      memory_free,
      memory_realloc,
    };
  FT_Stream stream;
  FT_Open_Args oa =
    {
      FT_OPEN_STREAM,
      NULL,
      0L,
      NULL,
      NULL, // stream
      NULL, // driver
      0,
      NULL,
    };
  FT_Face ftFace;

  float dpi = 72.0f;
  // change the scale to be relative to 1 based on 72 dpi
  // (so dpi of 144 means a scale of .5)
  //float glyphScale =  72.0f / dpi;
  int ec;
  int i;
  faceGlyph_t *glyph;
  wchar_t c;
  void *megabuf;

  if (!face || !fileName || !name)
    return;

  if (ftLibrary == NULL)
    {
      ri.Printf (PRINT_ALL, "RE_LoadFace: FreeType not initialized.\n");
      return;
    }

  ec = ri.FS_ReadFile (fileName, NULL);
  if (ec <= 0)
    {
      ri.Printf (PRINT_ALL, "RE_LoadFace: Unable to read font file %s\n", fileName);
      return;
    }

  oa.stream = malloc (sizeof(*stream));

  stream = oa.stream;
  face->mem = oa.stream;

  ri.FS_FOpenFileRead (fileName, &h, true);

  stream->base = NULL;
  stream->size = ri.FS_ReadFile (fileName, NULL);
  stream->pos = 0;
  stream->descriptor.value = h;
  stream->pathname.pointer = (void *) fileName;
  stream->read = (FT_Stream_IoFunc) stream_read;
  stream->close = (FT_Stream_CloseFunc) stream_close;
  stream->memory = &memory;
  stream->cursor = NULL;
  stream->limit = NULL;

  //ec = FT_Open_Face (ftLibrary, &oa, 0, (FT_Face *) &(face->opaque));
  ec = FT_Open_Face (ftLibrary, &oa, 0, &ftFace);
  if (ec != 0 )
    {
      ri.Printf (PRINT_ALL, "RE_LoadFace: FreeType2, Unable to open face;"
		 " error code: %d\n", ec);
      return;
    }

  //ftFace = (FT_Face) face->opaque;
  face->ftFace = ftFace;

  if (!ftFace)
    {
      ri.Printf (PRINT_ALL, "RE_LoadFace: face handle is NULL");
      return;
    }

  if (pointSize <= 0)
    {
      pointSize = 12;
    }

  if ((ec = FT_Set_Char_Size (ftFace, pointSize << 6, pointSize << 6, dpi, dpi)) != 0)
    {
      ri.Printf (PRINT_ALL, "RE_LoadFace: FreeType2, Unable to set face"
		 " char size; error code: %d\n", ec);
      return;
    }

  // keep track of the font name
  strncpy (face->name, name, MAX_QPATH);

  // how it will be send to the graphic card
  qglPixelStorei (GL_UNPACK_ALIGNMENT, 2);

  // generating the ascii chars list/textures for ogl
  face->cacheListOffset = qglGenLists (GLYPHS_CACHE);
  qglGenTextures (GLYPHS_CACHE, face->cacheTextures);

  // building a big map with all ascii glyphs
  face->bbox_xmax = (ftFace->bbox.xMax - ftFace->bbox.xMin) / 64;
  face->bbox_ymax = (ftFace->bbox.yMax - ftFace->bbox.yMin) / 64;

  // keep some pixels for interline
  int buf_width = (face->bbox_xmax + MEGABUF_X_INTERLINE) * 12;
  int buf_height = (face->bbox_ymax + MEGABUF_Y_INTERLINE) * 8;

  // big memory for all fonts and 2 color channels (in char)
  size_t mem = (buf_width * buf_height * 2 * sizeof (unsigned char));

  // making a 12 * 14 font map
  megabuf = ri.Hunk_AllocateTempMemory (mem);

  // set the buffer to 0
  Com_Memset (megabuf, '\0', mem);

  face->megabuf_width = buf_width;
  face->megabuf_height = buf_height;

  ri.Printf (PRINT_ALL, "RE_LoadFace (%s) - allocated : %d Bytes\n",
	     name, mem);

  for (c = GLYPH_CHARSTART, i = 0; c < GLYPH_CHAREND; c++, i++)
    {
      glyph = &face->glyphs[i];

      RE_LoadGlyph (face, glyph, c, false, megabuf);

      // use this character as reference char for spacing
      if (c == L'I')
	{
	  face->bbox_width = glyph->advance;
	  face->bbox_height = glyph->height;
	}
    }

  // register the megabuf to openGL
  // generating the ascii chars list/textures for ogl
  qglGenTextures (1, &face->megabuf_index);

  RE_RegisterMegaBuf (megabuf, buf_width, buf_height, face->megabuf_index);
  face->megabuf_registered = true;

  ri.Hunk_FreeTempMemory (megabuf);
}

/*
============
RE_FreeFace
============
*/
void
RE_FreeFace (fontFace_t *face)
{
  FT_Face ftFace;
  faceGlyph_t *glyph;
  int i;

  if (!face)
    return;

  ftFace = face->ftFace;

  if (!ftFace)
    {
      if (face->mem)
	{
	  free (face->mem);
	  face->mem = NULL;
	}

      return;
    }

  // removing textures from opengl
  qglDeleteTextures (1, &face->megabuf_index);

  if (face->cacheTextures[0])
    {
      qglDeleteTextures (GLYPHS_CACHE, face->cacheTextures);
      Com_Memset (face->cacheTextures, 0, sizeof (GLYPHS_CACHE));
    }

  for (i = 0; i < GLYPHS_CACHE; i++)
    face->cachePair[i] = 0;

  // let the backend know we don't have the textures in memory anymore
  for (i = 0; i < GLYPHS_PER_FONT; i++)
    {
      face->glyphs[i].oglregistered = false;
    }

  face->cacheIndex = 0;

  if (ftLibrary)
    {
      FT_Done_Face (ftFace);
      face->ftFace = NULL;
    }

  if (face->mem)
    {
      free (face->mem);
      face->mem = NULL;
    }
}

/*
==============
R_InitFreeType
==============
*/
void
R_InitFreeType (void)
{
  if (FT_Init_FreeType (&ftLibrary))
    {
      ri.Printf (PRINT_ALL,
		 "R_InitFreeType: Unable to initialize FreeType.\n");
    }
  ri.Printf (PRINT_ALL, "R_InitFreeType: Initialized.\n");
}

/*
==============
R_DoneFreeType
==============
*/
void
R_DoneFreeType (void)
{
  if (ftLibrary)
    {
      FT_Done_FreeType (ftLibrary);
      ftLibrary = NULL;
    }
}

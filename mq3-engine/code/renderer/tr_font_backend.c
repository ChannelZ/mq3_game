/*
  GPLv3

  File from mq3; a Quake III Arena fork

  Copyright 2011 Gabriel Schnoering
*/

#include "tr_local.h"


fontCommandList_t ftcmdList;

vec4_t fontColor = {1.0, 1.0, 1.0, 1.0};

unsigned int lasttexture;

/*
=============
RB_SetFontColor
=============
*/
static const void *
RB_SetFontColor (const void *data)
{
  const setFontColorCommand_t *cmd;

  cmd = (const setFontColorCommand_t *) data;

  fontColor[0] = cmd->color[0];
  fontColor[1] = cmd->color[1];
  fontColor[2] = cmd->color[2];
  fontColor[3] = cmd->color[3];

  return (const void *) (cmd + 1);
}

/*
=============
RB_DrawGlyph
=============
*/
static const void *
RB_DrawGlyph (const void *data)
{
  const drawGlyphCommand_t *cmd;
  //int start, end;
  int index, indey;

  cmd = (const drawGlyphCommand_t *) data;

  //start = ri.Milliseconds ();
  qglColor4f (fontColor[0] * 0.5, fontColor[1] * 0.5,
	      fontColor[2] * 0.5, fontColor[3] * 0.9);

  indey = cmd->baseline;
  index = cmd->x;

  qglTranslated (index + cmd->bearingX, indey - cmd->bearingY, 0);

  if (lasttexture != cmd->textureId)
    {
      qglBindTexture (GL_TEXTURE_2D, cmd->textureId);
      lasttexture = cmd->textureId;
    }

  qglBegin (GL_QUADS);

  qglTexCoord2f (cmd->upx, cmd->upy);
  qglVertex2i (0, 0);

  qglTexCoord2f (cmd->upx, cmd->doy);
  qglVertex2i (0, cmd->height);

  qglTexCoord2f (cmd->dox, cmd->doy);
  qglVertex2i (cmd->width, cmd->height);

  qglTexCoord2f (cmd->dox, cmd->upy);
  qglVertex2i (cmd->width, 0);

  qglEnd ();

  // restore previous pos
  qglTranslated (-cmd->bearingX - index, cmd->bearingY - indey, 0);

  //end = ri.Milliseconds ();
  //ri.Printf (PRINT_ALL, "%i msec to draw all fonts\n", end - start);

  return (const void *) (cmd + 1);
}

/*
=============
RB_DrawGlyph
=============
*/
static const void *
RB_DrawGlyphCached (const void *data)
{
  const drawGlyphCachedCommand_t *cmd;

  cmd = (const drawGlyphCachedCommand_t *) data;

  // this a stupid hack;
  // if we used a cached font, assume we'll have to reload the ascii map
  lasttexture = -1;

  qglPushMatrix ();

  qglTranslatef (cmd->x, cmd->baseline, 0);

  qglCallList (cmd->index);

  qglPopMatrix ();

  return (const void *) (cmd + 1);
}
/*
============
R_GetFontCommandBuffer

make sure there is enough command space, waiting on the
render thread if needed.
============
*/
void *
R_GetFontCommandBuffer (int bytes)
{
  fontCommandList_t *cmdList;

  cmdList = &ftcmdList;

  // always leave room for the end of list command
  if (cmdList->used + bytes + 4 > MAX_FONT_COMMANDS)
    {
      if (bytes > MAX_FONT_COMMANDS - 4)
	{
	  ri.Error (ERR_FATAL, "R_GetCommandBuffer: bad size %i", bytes);
	}
      // if we run out of room, just start dropping commands
      return NULL;
    }

  cmdList->used += bytes;

  return cmdList->cmds + cmdList->used - bytes;
}

/*
====================
RB_ExecuteFontCommands

This function will be called synchronously
after all the shader drawing
====================
*/
static void
RB_ExecuteFontCommands (const void *data)
{
  //int t1, t2;

  //t1 = ri.Milliseconds ();

  lasttexture = -1;

  while (1)
    {
      switch (*(const int *) data)
	{
	case RC_SET_COLOR:
	  data = RB_SetFontColor (data);
	  break;
	case RC_DRAW_GLYPH:
	  data = RB_DrawGlyph (data);
	  break;
	case RC_DRAW_GLYPH_CACHE:
	  data = RB_DrawGlyphCached (data);
	  break;
	case RC_END_OF_LIST:
	default:
	  /*
	  t2 = ri.Milliseconds ();
	  */
	  return;
	}
    }
}

/*
==================
R_HasFontToRender
==================
*/
bool
R_HasFontToRender (void)
{
  fontCommandList_t *cmdList;
  cmdList = &ftcmdList;

  return !!cmdList->used;
}

/*
====================
R_IssueFontCommands
====================
*/
void
R_IssueFontCommands (void)
{
  fontCommandList_t *cmdList;

  cmdList = &ftcmdList;
  assert (cmdList);

  *(int *) (cmdList->cmds + cmdList->used) = RC_END_OF_LIST;

  cmdList->used = 0;

  RB_ExecuteFontCommands ((void *)cmdList->cmds);
}

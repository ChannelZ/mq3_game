/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#ifndef IN_PUBLIC_H
#define IN_PUBLIC_H

struct inexport_s
{
  void (*Init) (void);
  void (*Frame) (int loading);
  void (*Shutdown) (void);
  void (*Restart) (void);

};
typedef struct inexport_s inexport_t;

struct inimport_s
{
  void (*Printf) (const char * msg, ...);
  void (*DPrintf) (const char * msg, ...);
  void (*Error) (int lvl, const char * msg, ...);
  int (*Milliseconds) (void);
  int  (*Cmd_Argc) (void);
  char *(*Cmd_Argv) (int i);
  void (*Cmd_AddCommand) (const char *name, void(*cmd)(void));
  void (*Cmd_RemoveCommand) (const char * name);
  cvar_t *(*Cvar_Get) (const char *name, const char *value, int flags);
  void (*Cvar_Set) (const char *name, const char *value);
  void *(*Hunk_AllocateTempMemory) (int size);
  void (*Hunk_FreeTempMemory) (void *block);

  int (*FS_FOpenFileByMode) (const char *qpath, fileHandle_t *f, fsMode_t mode);
  int (*FS_Read) (const void *buffer, int len, fileHandle_t f);
  int (*FS_ReadFile) (const char * qpath, void **buffer);
  int (*FS_Write) (const void *buffer, int len, fileHandle_t f);
  void (*FS_FCloseFile) (fileHandle_t f);
  int (*FS_Seek) (fileHandle_t f, long offset, int origin);
  int (*FS_FTell) (fileHandle_t f);

  void *(*Z_Malloc) (int bytes);
  void (*Z_Free) (void *buf);

  // specific things
  char *(*Key_KeynumToString) (int keynum);
  int (*Key_StringToKeynum) (char * token);
  void (*QueueEvent) (int time, sysEventType_t type, int value, int value2, int ptrLength, void *ptr);
  int (*Key_GetCatcher) (void);
  char *(*COM_Parse) (char **data_p);
  int (*Com_HexStrToInt) (const char * str);

  //void (*GetRePubInfos) (re_PubInfos_t *reinfos);
};
typedef struct inimport_s inimport_t;

#endif /* IN_PUBLIC_H */

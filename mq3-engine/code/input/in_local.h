/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "qcommon/q_shared.h"
#include "qcommon/qcom_extern.h"

#include "in_public.h"
#include "in_keycodes.h"

extern inimport_t * ini;

void IN_Frame (int loading);
void IN_Init (void);
void IN_Shutdown (void);
void IN_Restart (void);

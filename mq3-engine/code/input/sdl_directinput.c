/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#ifdef USE_LOCAL_HEADERS
#	include "SDL.h"
#else
#	include <SDL.h>
#endif

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "in_local.h"

#ifdef EVDEV_MOUSE
#include <unistd.h>

#include <linux/input.h>
#include <pthread.h>

// needed for open ()
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif

static cvar_t *in_keyboardDebug = NULL;

static bool mouseAvailable = false;
static bool mouseActive = false;
static bool keyRepeatEnabled = false;

static cvar_t *in_mouse;
static cvar_t *in_nograb;

#ifdef EVDEV_MOUSE
static cvar_t *in_mousedevice;
static int evdev_fd = -1;
static pthread_t evdev_thread;
#endif

#ifdef WINRAW_MOUSE
#include <windows.h>
static int winrawmouse = false;
#endif

cvar_t *in_consoleKeys;

#define CTRLCOMB(a) ((a)-'a'+1)

/*
===============
IN_PrintKey
===============
*/
static void
IN_PrintKey (const SDL_keysym *keysym, keyNum_t key, bool down)
{
  if (down)
    ini->Printf ("+ ");
  else
    ini->Printf ("  ");

  ini->Printf ("0x%02x \"%s\"", keysym->scancode,
	       SDL_GetKeyName (keysym->sym));

  if (keysym->mod & KMOD_LSHIFT)   ini->Printf (" KMOD_LSHIFT");
  if (keysym->mod & KMOD_RSHIFT)   ini->Printf (" KMOD_RSHIFT");
  if (keysym->mod & KMOD_LCTRL)    ini->Printf (" KMOD_LCTRL");
  if (keysym->mod & KMOD_RCTRL)    ini->Printf (" KMOD_RCTRL");
  if (keysym->mod & KMOD_LALT)     ini->Printf (" KMOD_LALT");
  if (keysym->mod & KMOD_RALT)     ini->Printf (" KMOD_RALT");
  if (keysym->mod & KMOD_LMETA)    ini->Printf (" KMOD_LMETA");
  if (keysym->mod & KMOD_RMETA)    ini->Printf (" KMOD_RMETA");
  if (keysym->mod & KMOD_NUM)      ini->Printf (" KMOD_NUM");
  if (keysym->mod & KMOD_CAPS)     ini->Printf (" KMOD_CAPS");
  if (keysym->mod & KMOD_MODE)     ini->Printf (" KMOD_MODE");
  if (keysym->mod & KMOD_RESERVED) ini->Printf (" KMOD_RESERVED");

  ini->Printf (" Q:0x%02x(%s)", key, ini->Key_KeynumToString (key));

  if (keysym->unicode)
    {
      ini->Printf (" U:0x%02x", keysym->unicode);

      if (keysym->unicode > ' ' && keysym->unicode < '~')
	ini->Printf ("(%c)", (char)keysym->unicode);
    }

  ini->Printf ("\n");
}

#define MAX_CONSOLE_KEYS 16

/*
===============
IN_IsConsoleKey
===============
*/
static bool
IN_IsConsoleKey (keyNum_t key, const unsigned char character)
{
  typedef struct consoleKey_s
  {
    enum
      {
	KEY,
	CHARACTER
      } type;

    union
    {
      keyNum_t key;
      unsigned char character;
    } u;
  } consoleKey_t;

  static consoleKey_t consoleKeys[MAX_CONSOLE_KEYS];
  static int numConsoleKeys = 0;
  int i;

  // Only parse the variable when it changes
  if (in_consoleKeys->modified)
    {
      char *text_p, *token;

      in_consoleKeys->modified = false;
      text_p = in_consoleKeys->string;
      numConsoleKeys = 0;

      while (numConsoleKeys < MAX_CONSOLE_KEYS)
	{
	  consoleKey_t *c = &consoleKeys[numConsoleKeys];
	  int charCode = 0;

	  token = ini->COM_Parse (&text_p);
	  if (!token[0])
	    break;

	  if (strlen(token) == 4)
	    charCode = ini->Com_HexStrToInt (token);

	  if (charCode > 0)
	    {
	      c->type = CHARACTER;
	      c->u.character = (unsigned char)charCode;
	    }
	  else
	    {
	      c->type = KEY;
	      c->u.key = ini->Key_StringToKeynum (token);

	      // 0 isn't a key
	      if (c->u.key <= 0)
		continue;
	    }

	  numConsoleKeys++;
	}
    }

  // If the character is the same as the key, prefer the character
  if (key == character)
    key = 0;

  for (i = 0; i < numConsoleKeys; i++)
    {
      consoleKey_t *c = &consoleKeys[i];

      switch (c->type)
	{
	case KEY:
	  if (key && c->u.key == key)
	    return true;
	  break;

	case CHARACTER:
	  if (c->u.character == character)
	    return true;
	  break;
	}
    }

  return false;
}


// bu : bit stupid, but don't want to bother with proper interfaces now
char *
Q_UTF8Encode (unsigned long codepoint)
{
  static char sbuf[2][5];
  static int index = 0;
  char *buf = sbuf[index++ & 1];

  wctomb (buf, (wchar_t)codepoint);

  return buf;
}

/*
===============
IN_TranslateSDLToQ3Key
===============
*/
static const char *
IN_TranslateSDLToQ3Key (SDL_keysym *keysym,
			keyNum_t *key, bool down)
{
  static unsigned char buf[5] = {0};

  *buf = '\0';
  *key = 0;

  if ((keysym->sym >= SDLK_SPACE) && (keysym->sym < SDLK_DELETE))
    {
      // These happen to match the ASCII chars
      *key = (int)keysym->sym;
    }
  else
    {
      switch (keysym->sym)
	{
	case SDLK_PAGEUP:       *key = K_PGUP;          break;
	case SDLK_KP9:          *key = K_KP_PGUP;       break;
	case SDLK_PAGEDOWN:     *key = K_PGDN;          break;
	case SDLK_KP3:          *key = K_KP_PGDN;       break;
	case SDLK_KP7:          *key = K_KP_HOME;       break;
	case SDLK_HOME:         *key = K_HOME;          break;
	case SDLK_KP1:          *key = K_KP_END;        break;
	case SDLK_END:          *key = K_END;           break;
	case SDLK_KP4:          *key = K_KP_LEFTARROW;  break;
	case SDLK_LEFT:         *key = K_LEFTARROW;     break;
	case SDLK_KP6:          *key = K_KP_RIGHTARROW; break;
	case SDLK_RIGHT:        *key = K_RIGHTARROW;    break;
	case SDLK_KP2:          *key = K_KP_DOWNARROW;  break;
	case SDLK_DOWN:         *key = K_DOWNARROW;     break;
	case SDLK_KP8:          *key = K_KP_UPARROW;    break;
	case SDLK_UP:           *key = K_UPARROW;       break;
	case SDLK_ESCAPE:       *key = K_ESCAPE;        break;
	case SDLK_KP_ENTER:     *key = K_KP_ENTER;      break;
	case SDLK_RETURN:       *key = K_ENTER;         break;
	case SDLK_TAB:          *key = K_TAB;           break;
	case SDLK_F1:           *key = K_F1;            break;
	case SDLK_F2:           *key = K_F2;            break;
	case SDLK_F3:           *key = K_F3;            break;
	case SDLK_F4:           *key = K_F4;            break;
	case SDLK_F5:           *key = K_F5;            break;
	case SDLK_F6:           *key = K_F6;            break;
	case SDLK_F7:           *key = K_F7;            break;
	case SDLK_F8:           *key = K_F8;            break;
	case SDLK_F9:           *key = K_F9;            break;
	case SDLK_F10:          *key = K_F10;           break;
	case SDLK_F11:          *key = K_F11;           break;
	case SDLK_F12:          *key = K_F12;           break;
	case SDLK_F13:          *key = K_F13;           break;
	case SDLK_F14:          *key = K_F14;           break;
	case SDLK_F15:          *key = K_F15;           break;

	case SDLK_BACKSPACE:    *key = K_BACKSPACE;     break;
	case SDLK_KP_PERIOD:    *key = K_KP_DEL;        break;
	case SDLK_DELETE:       *key = K_DEL;           break;
	case SDLK_PAUSE:        *key = K_PAUSE;         break;

	case SDLK_LSHIFT:
	case SDLK_RSHIFT:       *key = K_SHIFT;         break;

	case SDLK_LCTRL:
	case SDLK_RCTRL:        *key = K_CTRL;          break;

	case SDLK_RMETA:
	case SDLK_LMETA:        *key = K_COMMAND;       break;

	case SDLK_RALT:
	case SDLK_LALT:         *key = K_ALT;           break;

	case SDLK_LSUPER:
	case SDLK_RSUPER:       *key = K_SUPER;         break;

	case SDLK_KP5:          *key = K_KP_5;          break;
	case SDLK_INSERT:       *key = K_INS;           break;
	case SDLK_KP0:          *key = K_KP_INS;        break;
	case SDLK_KP_MULTIPLY:  *key = K_KP_STAR;       break;
	case SDLK_KP_PLUS:      *key = K_KP_PLUS;       break;
	case SDLK_KP_MINUS:     *key = K_KP_MINUS;      break;
	case SDLK_KP_DIVIDE:    *key = K_KP_SLASH;      break;

	case SDLK_MODE:         *key = K_MODE;          break;
	case SDLK_COMPOSE:      *key = K_COMPOSE;       break;
	case SDLK_HELP:         *key = K_HELP;          break;
	case SDLK_PRINT:        *key = K_PRINT;         break;
	case SDLK_SYSREQ:       *key = K_SYSREQ;        break;
	case SDLK_BREAK:        *key = K_BREAK;         break;
	case SDLK_MENU:         *key = K_MENU;          break;
	case SDLK_POWER:        *key = K_POWER;         break;
	case SDLK_EURO:         *key = K_EURO;          break;
	case SDLK_UNDO:         *key = K_UNDO;          break;
	case SDLK_SCROLLOCK:    *key = K_SCROLLOCK;     break;
	case SDLK_NUMLOCK:      *key = K_KP_NUMLOCK;    break;
	case SDLK_CAPSLOCK:     *key = K_CAPSLOCK;      break;

	default:
	  if (keysym->sym >= SDLK_WORLD_0 && keysym->sym <= SDLK_WORLD_95)
	    *key = (keysym->sym - SDLK_WORLD_0) + K_WORLD_0;
	  break;
	}
    }

  if (down && keysym->unicode && !(keysym->unicode & 0xFF00))
    {
      unsigned char ch = (unsigned char)keysym->unicode & 0xFF;

      switch (ch)
	{
	case 127: // ASCII delete
	  if (*key != K_DEL)
	    {
	      // ctrl-h
	      *buf = CTRLCOMB('h');
	      buf[1] = '\0';
	      break;
	    }
	  // fallthrough

	default:
	  *buf = ch;
	  buf[1] = '\0';
	  break;
	}
    }

  if (in_keyboardDebug->integer)
    IN_PrintKey (keysym, *key, down);

  if (IN_IsConsoleKey (*key, *buf))
    {
      // Console keys can't be bound or generate characters
      *key = K_CONSOLE;
      *buf = '\0';
    }

  // Keys that have ASCII names but produce no character are probably
  // dead keys -- ignore them
  if (down && strlen (ini->Key_KeynumToString (*key)) == 1
      && keysym->unicode == 0)
    {
      if (in_keyboardDebug->integer)
	ini->Printf ("  Ignored dead key '%c'\n", *key);

      *key = 0;
    }

#ifdef _WIN32
  // restrict to ascii chars, because setlocale doesn't support utf8
  Com_Memcpy (buf, Q_UTF8Encode (keysym->unicode & 127), sizeof (buf));
#else
  Com_Memcpy (buf, Q_UTF8Encode (keysym->unicode), sizeof (buf));
#endif

  // NOTE bu : just to be sure
  buf[4] = '\0';

  return (char *)buf;
}

/*
===============
IN_GobbleMotionEvents
===============
*/
static void
IN_GobbleMotionEvents (void)
{
  SDL_Event dummy[1];

  // Gobble any mouse motion events
  SDL_PumpEvents ();
  while (SDL_PeepEvents (dummy, 1, SDL_GETEVENT,
			 SDL_EVENTMASK (SDL_MOUSEMOTION)))
    { }
}

/*
===============
IN_ActivateMouse
===============
*/
static void
IN_ActivateMouse (void)
{
  //re_PubInfos_t reinfos;
  //ini->GetRePubInfos (&reinfos);

  if (!mouseAvailable || !SDL_WasInit(SDL_INIT_VIDEO))
    return;

  if (!mouseActive)
    {
      SDL_ShowCursor (0);
      SDL_WM_GrabInput (SDL_GRAB_ON);

      IN_GobbleMotionEvents ();
    }

  // in_nograb makes no sense in fullscreen mode
  if (/*!reinfos.isfullscreen*/1)
    {
      if (in_nograb->modified || !mouseActive)
	{
	  if (in_nograb->integer)
	    SDL_WM_GrabInput (SDL_GRAB_OFF);
	  else
	    SDL_WM_GrabInput (SDL_GRAB_ON);

	  in_nograb->modified = false;
	}
    }

  mouseActive = true;
}

/*
===============
IN_DeactivateMouse
===============
*/
static void
IN_DeactivateMouse (void)
{
  if (!SDL_WasInit (SDL_INIT_VIDEO))
    return;

  // Always show the cursor when the mouse is disabled,
  // but not when fullscreen
  //re_PubInfos_t reinfos;
  //re.GetPubInfos (&reinfos);

  if (/*!reinfos.isfullscreen*/0)
    SDL_ShowCursor (1);

  if (!mouseAvailable)
    return;

  if (mouseActive)
    {
      //re_PubInfos_t reinfos;
      //re.GetPubInfos (&reinfos);
      IN_GobbleMotionEvents ();

      SDL_WM_GrabInput (SDL_GRAB_OFF);

      // Don't warp the mouse unless the cursor is within the window
      if (SDL_GetAppState () & SDL_APPMOUSEFOCUS)
	//SDL_WarpMouse( reinfos.glvidWidth / 2, reinfos.glvidHeight / 2 );

	mouseActive = false;
    }
}

/*
===============
IN_ProcessEvents
===============
*/
static void
IN_ProcessEvents (void)
{
  SDL_Event e;
  const char *character = NULL;
  keyNum_t key = 0;

  if (!SDL_WasInit (SDL_INIT_VIDEO))
    return;

  if (ini->Key_GetCatcher( ) == 0 && keyRepeatEnabled)
    {
      SDL_EnableKeyRepeat (0, 0);
      keyRepeatEnabled = false;
    }
  else if (!keyRepeatEnabled)
    {
      SDL_EnableKeyRepeat (SDL_DEFAULT_REPEAT_DELAY,
			   SDL_DEFAULT_REPEAT_INTERVAL);
      keyRepeatEnabled = true;
    }

  while (SDL_PollEvent (&e))
    {
      switch (e.type)
	{
	case SDL_KEYDOWN:
	  character = IN_TranslateSDLToQ3Key (&e.key.keysym, &key, true);

	  if (character && *character)
	    {
	      void *buf = ini->Z_Malloc (sizeof (buf));
	      Com_Memcpy (buf, character, sizeof (buf));
	      ini->QueueEvent (0, SE_CHAR, sizeof (buf), 0, 0, buf);
	    }

	  if (key)
	    ini->QueueEvent (0, SE_KEY, key, true, 0, NULL);
	  break;

	case SDL_KEYUP:
	  character = IN_TranslateSDLToQ3Key (&e.key.keysym, &key, false);
	  /*
	  if (character && *character)
	    {
	      void *buf = ini->Z_Malloc (sizeof (buf));
	      Com_Memcpy (buf, character, sizeof (buf));
	      ini->QueueEvent (0, SE_CHAR, sizeof (buf), 0, 0, buf);
	    }
	  */
	  if (key)
	    ini->QueueEvent (0, SE_KEY, key, false, 0, NULL);
	  break;
	case SDL_MOUSEMOTION:
#ifdef EVDEV_MOUSE
	  // evdev input not active when fd = -1
	  if (mouseActive && (evdev_fd == -1))
#else
#ifdef WINRAWMOUSE
	    if (mouseActive && !winrawmouse)
#else
	  if (mouseActive)
#endif
#endif
	    ini->QueueEvent( 0, SE_MOUSE, e.motion.xrel, e.motion.yrel, 0, NULL );
	  break;

	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
	  {
#ifdef EVDEV_MOUSE
	    // evdev input is managing clics too
	    if (evdev_fd != -1)
	      break;
#endif

#ifdef WINRAWMOUSE
	    if (winrawmouse)
	      break;
#endif

	    unsigned char b;
	    switch (e.button.button)
	      {
	      case 1:   b = K_MOUSE1;     break;
	      case 2:   b = K_MOUSE3;     break;
	      case 3:   b = K_MOUSE2;     break;
	      case 4:   b = K_MWHEELUP;   break;
	      case 5:   b = K_MWHEELDOWN; break;
	      case 6:   b = K_MOUSE4;     break;
	      case 7:   b = K_MOUSE5;     break;
	      default:  b = K_AUX1 + (e.button.button - 8) % 16; break;
	      }
	    ini->QueueEvent (0, SE_KEY, b,
			     (e.type == SDL_MOUSEBUTTONDOWN ? true : false), 0, NULL);
	  }
	  break;
	case SDL_QUIT:
	  //Sys_Quit( );
	  break;

	default:
	  break;
	}
    }
}

#ifdef EVDEV_MOUSE
void
IN_ListEvdev_f (void)
{
  int fd, i;
  char device[64], name[128];

  ini->Printf( "------- Evdev Input Listing -------\n" );

  for (i = 0; i < 16; i++)
    {
      snprintf (device, sizeof(device), "/dev/input/event%i", i);
      fd = open (device, O_RDONLY | O_NOFOLLOW);

      // error = we can't use it so don't bother know the kind of error it is
      // the most common error will be that the device doesn't exist
      if (fd == -1)
	continue;

      name[0] = 0;
      ioctl (fd, EVIOCGNAME (sizeof(name)), name);
      close (fd);

      ini->Printf ("event%i : %s\n", i, name);
    }
  ini->Printf( "------- Listing Ended -------\n" );
}

// avoid crash when the api is broken (e.g on client exit)
// TODO ? : stop inputs before breaking common api ?
inline void
EvDev_QueueEvent (int time, sysEventType_t type, int value,
		  int value2, int ptrLength, void *ptr)
{
  if (!ini->QueueEvent)
    return;

  ini->QueueEvent (time, type, value, value2, ptrLength, ptr);
}

/*
==================
EvDev_UpdateMouse

Let us be very defensive as the thread can be cancel at anytime
and the API be made invalid
==================
*/
void
EvDev_UpdateMouse (void * args)
{
  struct input_event event;
  int ret;

  // we want to be stoped immediatly when asked
  ret = pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
  if (ret)
    {
      ini->Printf ("EvDev_UpdateMouse () : pthread_setcanceltype failed with error : %i\n", ret);
    }

  ini->Printf ("Starting mouse thread\n");

  // lets do the work and infinite loop
  while (1)
    {
      ret = read (evdev_fd, &event, sizeof (struct input_event));

      if (ret == -1)
	{
	  if (!ini->Printf)
	    return;

	  ini->Printf ("EvDev_UpdateMouse () - read error : %m\n");
	  return; // ending the thread in case we have errors
	}

      if ((unsigned int)ret < sizeof (struct input_event))
	{
	  if (!ini->Printf)
	    return;

	  ini->Printf ("EvDev_UpdateMouse () - read couldn't grab all the infos : %m\n");
	  return; // ending the thread in case we have errors
	}

      // mouse mouvement
      if (event.type == EV_REL)
	{
	  switch (event.code)
	    {
	    case REL_X:
	      EvDev_QueueEvent (0, SE_MOUSE, event.value, 0, 0, NULL);
	      break;

	    case REL_Y:
	      EvDev_QueueEvent (0, SE_MOUSE, 0, event.value, 0, NULL);
	      break;

	      // no key release for mouse wheel ? (can't bind +action)
	    case REL_WHEEL:
	      switch (event.value)
		{
		case 1:
		  EvDev_QueueEvent (0, SE_KEY, K_MWHEELUP, 1, 0, NULL);
		  break;
		case -1:
		  EvDev_QueueEvent (0, SE_KEY, K_MWHEELDOWN, 1, 0, NULL);
		  break;
		}
	      break;
	    }
	}
      // other clics, value represents pressed/released
      else if (event.type == EV_KEY)
	{
	  switch (event.code)
	    {
	    case BTN_LEFT:
	      EvDev_QueueEvent (0, SE_KEY, K_MOUSE1, event.value, 0, NULL);
	      break;
	    case BTN_RIGHT:
	      EvDev_QueueEvent (0, SE_KEY, K_MOUSE2, event.value, 0, NULL);
	      break;
	    case BTN_MIDDLE:
	      EvDev_QueueEvent (0, SE_KEY, K_MOUSE3, event.value, 0, NULL);
	      break;
	    case BTN_SIDE:
	      EvDev_QueueEvent (0, SE_KEY, K_MOUSE4, event.value, 0, NULL);
	      break;
	    case BTN_EXTRA:
	      EvDev_QueueEvent (0, SE_KEY, K_MOUSE5, event.value, 0, NULL);
	      break;
	    case BTN_FORWARD:
	      EvDev_QueueEvent (0, SE_KEY, K_AUX1, event.value, 0, NULL);
	      break;
	    case BTN_BACK:
	      EvDev_QueueEvent (0, SE_KEY, K_AUX2, event.value, 0, NULL);
	      break;
	    case BTN_TASK:
	      EvDev_QueueEvent (0, SE_KEY, K_AUX3, event.value, 0, NULL);
	      break;
	    }
	}
    }
}

int
IN_InitializeEvdevMouse (void)
{
  int everror;

  ini->Printf ("Initializing evdev input : %s\n", in_mousedevice->string);

  evdev_fd = open (in_mousedevice->string, O_RDONLY);

  if (evdev_fd == -1)
    {
      ini->Printf ("IN_InitializeEvdevMouse () : error opening input device :\n"
		   "%s - error : %m\n", in_mousedevice->string);
      return 0;
    }

  everror = pthread_create (&evdev_thread, NULL, (void *)EvDev_UpdateMouse, NULL);

  if (everror)
    {
      ini->Printf ("IN_InitializeEvdevMouse () - thread creation failed with error : %i\n", everror);
      return 0;
    }

  //mouseActive = true;

  return 1;
}

void
IN_EvdevMouseShutdown (void)
{
  if (evdev_fd != -1)
    {
      int everror;

      ini->Printf ("Stopping Evdev mouse thread\n");
      everror = pthread_cancel (evdev_thread);
      if (everror)
	{
	  ini->Printf ("IN_EvdevMouseShutdown () - thread_cancel failed with error : %i\n", everror);
	}

      evdev_fd = -1;
      mouseActive = false;
      mouseAvailable = false; // this is also set by in_shutdown
    }
}
#endif

#ifdef WINRAW_MOUSE

#define RAW_NAME_SIZE 512

bool
IN_InitRawMouse (void)
{
  PRAWINPUTDEVICELIST devices;
  UINT num;
  UINT size = RAW_NAME_SIZE;
  char name[RAW_NAME_SIZE];
  short int i, mice;

  //detect the existence of raw devices
  GetRawInputDeviceList (NULL, &num, sizeof (RAWINPUTDEVICELIST));
  if(!(devices = (PRAWINPUTDEVICELIST) malloc (sizeof (RAWINPUTDEVICELIST) * num)))
    {
      //this won't spam the console because it sets rawmouse to 0 and hence stops the attempts via that:
      ini->Printf ("..Raw Mouse malloc fail. Reverting to non-raw input.\n");
      //Cvar_SetNew (in_rawmouse, "0"); //fixme? *maybe* unsafer than cvar_set but we don't want to alter the config.
      return false;
  }
  GetRawInputDeviceList (devices, &num, sizeof (RAWINPUTDEVICELIST));

  for (i = mice = 0; i < num; i++)
    {
    if (devices[i].dwType == RIM_TYPEMOUSE)
      {
	GetRawInputDeviceInfo (devices[i].hDevice, RIDI_DEVICENAME, name, (PUINT)&size);
	if (!strncmp(name,"\\\\?\\Root#RDP_MOU#", 17)) continue; /*	ignore remote desktop mouse; -3 escapes
									fixme? there may be more similarly non-applicable devices */
	mice++;
      }
    }
  free (devices);

  if (!mice)
    {
      ini->Printf ("..No Raw Mouse Devices are detected. Reverting to non-raw mouse input\n");
      return false;
    }

  // http://www.usb.org/developers/devclass_docs/Hut1_12.pdf
  RAWINPUTDEVICE dev =
  {
    1,	// usUsagePage - generic desktop controls
    2,	// usUsage - mouse
    0,	// dwFlags
    0	// hwndTarget
  };

  if (!RegisterRawInputDevices (&dev, 1, sizeof(dev)))
    {
      ini->Printf("Raw input registration failed. (0x%x)\n"
		  "even if %u devices had been found. Reverting to non-raw mouse input\n", GetLastError(), mice);
      return false;
    }

  ini->Printf ("Registered for raw input.\n");
  winrawmouse = true;
  return true;
}

void
IN_ShutdownRawMouse (void)
{
  RAWINPUTDEVICE dev =
    {
      1,	// usUsagePage - generic desktop controls
      2,	// usUsage - mouse
      RIDEV_REMOVE,	// dwFlags
      0	// hwndTarget
    };
  winrawmouse = false;
  if (!RegisterRawInputDevices (&dev, 1, sizeof(dev)))
    {
      ini->Printf("Mouse release failed. (0x%x)\n", GetLastError());
    }
  mouseActive = false;
}

bool
IN_RawMouseActive (void)
{
  return mouseActive && winrawmouse;
}

void
IN_WinRaw_UpdateMouse (HRAWINPUT lParam)
{
  if (!IN_RawMouseActive())
    {
      return;
    }

  RAWINPUT ri;
  unsigned int i = sizeof (ri);

  // while should be better here, if only...
  if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, &ri, &i, sizeof (RAWINPUTHEADER)) != -1)
    {
      for (i = 0; i < 5; i++)
	{
	  if (ri.data.mouse.ulButtons & (1<<(i*2)))
	    {
	      ini->QueueEvent (0, SE_KEY, K_MOUSE1+i, true, 0, NULL);
	    }
	  else if (ri.data.mouse.ulButtons & (1 << (i*2+1)))
	    {
	      ini->QueueEvent (0, SE_KEY, K_MOUSE1+i, false, 0, NULL);
	    }
	}
      if (ri.data.mouse.lLastX || ri.data.mouse.lLastY)
	{
	  ini->QueueEvent(0, SE_MOUSE, ri.data.mouse.lLastX, ri.data.mouse.lLastY, 0, NULL);
	}
    }
}

// NOTE : bubu, this is just UGLY ! can't understand how people can love this kind of "APIs"

/*	Collect WindowProc msgs here (instead of requiring SDL); initially based on http://forums.indiegamer.com/showthread.php?t=2138
	This is still not restricted by FPS.
	The important benefit is that this way stock SDL is useable for raw input, let alone it makes code management easier. */

WNDPROC lpPrevWndFunc;
HWND hWnd;

LONG WINAPI
WindowProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  if (uMsg == WM_INPUT)
    IN_WinRaw_UpdateMouse ((HRAWINPUT)lParam);

  /*	call explicitely the previous WindowProc address;
	return value has no material effect here */
  return CallWindowProc (lpPrevWndFunc, hWnd, uMsg, wParam, lParam);
}


#include "SDL_syswm.h"

//"[..] call [it] after SDL_Init() but before SDL_SetVideoMode()."
// called right before SDL_SetVideoMode();
void init_WndProc (void)
{
  SDL_SysWMinfo wminfo;

  SDL_GetWMInfo (&wminfo);

  hWnd = wminfo.window; //we got the SDL window handle

  /*  Set a new address for WindowProc()/window procedure (with GWLP_WNDPROC) on SDL window's hWnd handle.
      The previous value for GWLP_WNDPROC is returned, i.e. the previous address to WindowProc.
      Usable on both 64 and 32bit, unlike SetWindowLong() */
  lpPrevWndFunc = (WNDPROC) SetWindowLong (hWnd, GWL_WNDPROC, (LONG) WindowProc);
}

#endif

/*
===============
IN_Frame
===============
*/
void
IN_Frame (int loading)
{
  //bool loading;
  //re_PubInfos_t reinfos;
  //re.GetPubInfos (&reinfos);

  IN_ProcessEvents ();

  // If not DISCONNECTED (main menu) or ACTIVE (in game), we're loading
  //loading = !!( cls.state != CA_DISCONNECTED && cls.state != CA_ACTIVE );

  if (/*!reinfos.isfullscreen*/1 && (ini->Key_GetCatcher () & KEYCATCH_CONSOLE))
    {
      // Console is down in windowed mode
      IN_DeactivateMouse ();
    }
  else if (/*!reinfos.isfullscreen*/0 && loading)
    {
      // Loading in windowed mode
      IN_DeactivateMouse ();
    }
  else if (!(SDL_GetAppState() & SDL_APPINPUTFOCUS))
    {
      // Window not got focus
      IN_DeactivateMouse ();
    }
  else
    IN_ActivateMouse ();
}

/*
===============
IN_Init
===============
*/
void
IN_Init (void)
{
  int ret;
  int style;
#ifdef WINRAW_MOUSE
  static bool wndproc_init = false;
#endif

  if (!SDL_WasInit (SDL_INIT_VIDEO))
    {
      ini->Error (ERR_FATAL, "IN_Init called before SDL_Init( SDL_INIT_VIDEO )\n");
      return;
    }

  ini->DPrintf ("\n------- Input Initialization -------\n");

  // adding commands
  ini->Cmd_AddCommand ("in_restart", IN_Restart);
#ifdef EVDEV_MOUSE
  ini->Cmd_AddCommand ("in_listevdev", IN_ListEvdev_f);
  in_mousedevice = ini->Cvar_Get ("in_mousedevice", "", CVAR_ARCHIVE);
#endif

  in_keyboardDebug = ini->Cvar_Get ("in_keyboardDebug", "0", CVAR_ARCHIVE);

  // mouse variables
  in_mouse = ini->Cvar_Get ("in_mouse", "1", CVAR_ARCHIVE);
  in_nograb = ini->Cvar_Get ("in_nograb", "0", CVAR_ARCHIVE);

  SDL_EnableUNICODE (1);
  SDL_EnableKeyRepeat (SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
  keyRepeatEnabled = true;

#ifdef WINRAW_MOUSE
  if ((in_mouse->integer == 2) && !wndproc_init)
    {
      init_WndProc ();
      ini->Printf ("WndProc initialized\n");
      wndproc_init = true;
    }
#endif

  style = in_mouse->integer;
  if (style)
    {
      mouseAvailable = true;
#ifdef EVDEV_MOUSE
      if (style == 2)
	{
	  ret = IN_InitializeEvdevMouse ();

	  if (!ret)
	    {
	      ini->Printf ("Couldn't initialize Evdev mouse, switching back to SDL mouse\n");
	      mouseActive = false;
	      style = 1;
	    }
	  else
	    {
	      mouseActive = true;
	    }
	}
#endif
#ifdef WINRAW_MOUSE
      if (style == 2)
	{
	  ret = IN_InitRawMouse ();

	  if (!ret)
	    {
	      ini->Printf ("Couldn't initialize Raw mouse, back to SDL\n");
	      mouseActive = false;
	      style = 1;
	    }
	  else
	    {
	      mouseActive = true;
	    }
	}
#endif
      if (style == 1)
	{
	  IN_ActivateMouse ();
	}
    }
  else
    {
      IN_DeactivateMouse ();
      mouseAvailable = false;
    }

  ini->DPrintf ("------------------------------------\n");
}

/*
===============
IN_Shutdown
===============
*/
void
IN_Shutdown( void )
{
  // removing commands
  ini->Cmd_RemoveCommand ("in_restart");
#ifdef EVDEV_MOUSE
  ini->Cmd_RemoveCommand ("in_listevdev");
#endif
  IN_DeactivateMouse ();
  mouseAvailable = false;

#ifdef EVDEV_MOUSE
  IN_EvdevMouseShutdown ();
#endif
#ifdef WINRAW_MOUSE
  IN_ShutdownRawMouse ();
#endif
}

/*
===============
IN_Restart
===============
*/
void
IN_Restart( void )
{
  IN_Shutdown ();
  IN_Init ();
}

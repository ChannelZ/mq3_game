/*
===========================================================================
Copyright (C) 2007-2012 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "in_local.h"

static inexport_t ine;
inimport_t * ini;

// in in_input.c
extern cvar_t * in_consoleKeys;

inexport_t *
GetInputLibAPI (inimport_t * inimp)
{
  bool started = false;

  ini = inimp;

  ini->Printf ("-------- Initilizaing Input -------\n");

  // ~ and `, as keys and characters
  in_consoleKeys = ini->Cvar_Get ("in_consoleKeys", "~ ` 0x7e 0x60", CVAR_ARCHIVE);

  ine.Init = IN_Init;
  ine.Frame = IN_Frame;
  ine.Shutdown = IN_Shutdown;
  ine.Restart = IN_Restart;

  return &ine;
}

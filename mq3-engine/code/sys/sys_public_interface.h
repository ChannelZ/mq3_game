/*
  Gabriel Schnoering
  GPLv3 or later


 */

// PUBLIC INTERFACE for the sys/ submodule

// interfaces, list of (specific) functions the lib must be provided
// functions changing from client/serv (print, shutdown etc...)
struct sys_import_s
{
  void (*print) (const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
  void (*dprint) (const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
  void (*error) (int code, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));

  void (*shutdown) (const char *finalmsg);
  bool (*isQcomLoaded) (void);
};
typedef struct sys_import_s sys_import_t;

// this must be called when possible
void Sys_Set_Interface (sys_import_t *sys_import, bool silent);

// initialize the terminal, if silent was false (dedicated), grab a tty
void CON_Init (void);

// ===============================================
// System functions that everyplatform must provide

const char *Sys_DefaultHomePath (void);
const char *Sys_Basename (char *path);
const char *Sys_Dirname (char *path);

const char *Sys_GetCurrentUser (void);
const char *Sys_GetClipboardData (void);
bool Sys_Mkdir (const char *path);
FILE *Sys_Mkfifo (const char *ospath);

const char *Sys_Cwd (void);
void Sys_SetEnv (const char *name, const char *value);

char **Sys_ListFiles (const char *directory, const char *extension,
		      char *filter, int *numfiles, bool wantsubs);
void Sys_FreeFileList (char **list);

void Sys_Sleep (int msec); // block for msec or an stdin input (-1) for stdin only
void Sys_Sleep2 (int msec); // block for msec
int Sys_InputFD (void);

// Sys_Milliseconds should only be used for profiling purposes,
// any game related timing information should come from event timestamps
unsigned int Sys_Milliseconds (void);
bool Sys_RandomBytes (void *string, int len);
bool Sys_LowPhysicalMemory (void);

void Sys_PlatformInit (void);
void Sys_PlatformExit (void);
// ==============================================

// other "sys" functions, some rely on the "platform" functions
void Sys_Init (void);
void Sys_ArgsInit (int argc, char ** argv);

const char *Sys_DefaultInstallPath (void);
const char *Sys_DefaultAppPath (void);

const char *Sys_ConsoleInput (void);
void Sys_Quit (void);

bool Sys_WritePIDFile (void);

// I/O functions to output on tty/term (stdout)
void Sys_Print (const char *msg);
void Sys_Error (const char *error, ...) __attribute__ ((format (printf, 1, 2)));

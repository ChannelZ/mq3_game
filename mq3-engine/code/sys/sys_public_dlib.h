/* Copyright © 2009-2010 Gabriel Schnoering
 *
 * This file is part of MQuake III source code.
 *
 * MQuake III source code is free software based on Quake III Arena source code.
 *
 * Individual portions may be copyright by individual contributors,
 * and are included in this collective work with permission of
 * the copyright owners.
 *
 * This file is licensed under the GPLv3 or later.
 */

enum DlType_e
  {
    MOD,
    CLIENT
  };
typedef enum DlType_e DlType_t;

// this one always looks for mods
void *Sys_DLLoad (const char *name, char *fqpath);
void *Sys_DLLoadType (const char *name, char *fqpath, DlType_t type);
void *Sys_DLLoadType2 (const char *name, char *fqpath, DlType_t type, bool errorfatal);
void *Sys_DLLoadFunction (void *dlHandle, const char *funcname,
			  bool failexit);
void Sys_DLUnload (void *dynHandle);

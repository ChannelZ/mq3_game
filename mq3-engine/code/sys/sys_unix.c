/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/

// including all needed std* libs
#include "qcommon/q_shared.h"
#include "sys_local.h"

/*
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
*/
#include <signal.h>
#include <limits.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/time.h>

#include <dirent.h>
#include <unistd.h>

#include <ctype.h>
#include <errno.h>

#include <pwd.h>
#include <libgen.h>
#include <fcntl.h>

bool stdinIsATTY;

/*
==================
Sys_Mkfifo
==================
*/
FILE *
Sys_Mkfifo (const char *ospath)
{
  FILE *fifo;
  int result;
  int fn;
  struct stat buf;

  // if file already exists AND is a pipefile, remove it
  if (!stat (ospath, &buf) && S_ISFIFO (buf.st_mode))
    remove (ospath);

  result = mkfifo (ospath, 0600);
  if (result != 0)
    return NULL;

  fifo = fopen (ospath, "w+");
  if (fifo)
    {
      fn = fileno (fifo);
      fcntl (fn, F_SETFL, O_NONBLOCK);
    }

  return fifo;
}

/*
==================
Sys_Cwd
==================
*/
const char *
Sys_Cwd (void)
{
  static char cwd[MAX_OSPATH];

  // get current working dir : <unistd.h>
  char *result = getcwd (cwd, sizeof (cwd) - 1);
  if (result != cwd)
    return NULL;

  cwd[MAX_OSPATH-1] = 0;

  return cwd;
}

/*
==================
Sys_DefaultHomePath
==================
*/
const char *
Sys_DefaultHomePath (void)
{
  char *p;

  if (!(*syspath.homePath))
    {
      p = getenv ("HOME");
      if (p != NULL)
	{
	  Q_strncpyz (syspath.homePath, p, sizeof (syspath.homePath));
	  Q_strcat (syspath.homePath, sizeof (syspath.homePath), "/."PRODUCT_NAME);
	}
    }
  return syspath.homePath;
}

/*
================
Sys_TempPath
================
*/
const char *
Sys_TempPath (void)
{
  const char *TMPDIR = getenv ("TMPDIR");

  if ((TMPDIR == NULL) || (TMPDIR[ 0 ] == '\0'))
    return "/tmp";
  else
    return TMPDIR;
}

/*
==================
Sys_Basename
==================
*/
const char *
Sys_Basename (char *path)
{
  // basename : <libgen.h>
  return basename (path);
}

/*
==================
Sys_Dirname
==================
*/
const char *
Sys_Dirname (char *path)
{
  // dirname : <libgen.h>
  return dirname (path);
}

/*
==================
Sys_GetCurrentUser
==================
*/
const char *
Sys_GetCurrentUser (void)
{
  struct passwd *p;

  p = getpwuid (getuid());
  if (p == NULL)
    {
      return "player";
    }
  return p->pw_name;
}

/*
================
Sys_Milliseconds
================
*/
/* base time in seconds, that's our origin
   timeval:tv_sec is an int:
   assuming this wraps every 0x7fffffff -
   ~68 years since the Epoch (1970) - we're safe till 2038

   current time in ms, using sys_timeBase as origin
   NOTE: sys_timeBase*1000 + curtime -> ms since the Epoch
     0x7fffffff ms - ~24 days
   although timeval:tv_usec is a signed long (int32_t)
*/
/*
unsigned int
Sys_Milliseconds (void)
{
  static time_t sys_timeBase = 0;
  int	curtime;
  struct timeval tp;

  // gettimeofday : <sys/time.h>
  gettimeofday (&tp, NULL);

  if (!sys_timeBase)
    {
      sys_timeBase = tp.tv_sec;
      return tp.tv_usec/1000;
    }

  curtime = (tp.tv_sec - sys_timeBase)*1000 + tp.tv_usec/1000;

  return curtime;
}
*/

/*
==================
Sys_RandomBytes
==================
*/
bool
Sys_RandomBytes (void *string, int len)
{
  FILE *fp;

  fp = fopen ("/dev/urandom", "r");
  if (fp == NULL)
    {
      perror ("Sys_RandomBytes : fopen");
      return false;
    }

  if (!fread (string, sizeof (byte), (size_t)len, fp))
    {
      fclose (fp);
      return false;
    }

  fclose (fp);
  return true;
}

/*
==================
Sys_Mkdir
==================
*/
bool
Sys_Mkdir (const char *path)
{
  int result = mkdir (path, 0750);

  if (result != 0)
    return (errno == EEXIST);

  return true;
}

/*
==============================================================

DIRECTORY SCANNING

==============================================================
*/

#define MAX_FOUND_FILES 0x1000

/*
==================
Sys_ListFilteredFiles
==================
*/
static void
Sys_ListFilteredFiles (const char *basedir, char *subdirs, char *filter, char **list, int *numfiles)
{
  char          search[MAX_OSPATH], newsubdirs[MAX_OSPATH];
  char          filename[MAX_OSPATH];
  DIR           *fdir;
  struct dirent *d;
  struct stat   st;

  if (*numfiles >= MAX_FOUND_FILES - 1)
    {
      return;
    }

  if (strlen(subdirs))
    {
      Com_sprintf (search, sizeof(search), "%s/%s", basedir, subdirs);
    }
  else
    {
      Com_sprintf (search, sizeof(search), "%s", basedir);
    }

  // opendir : <dirent.h> <sys/types.h>
  fdir = opendir (search);
  if (fdir == NULL)
    {
      perror ("Sys_ListFilteredFiles : opendir");
      return;
    }

  // readdir : <dirent.h>
  while ((d = readdir (fdir)) != NULL)
    {
      Com_sprintf (filename, sizeof(filename), "%s/%s", search, d->d_name);
      if (stat(filename, &st) == -1)
	{
	  continue;
	}

      if (st.st_mode & S_IFDIR)
	{
	  // !strcmp(".") || !strcmp("..")
	  if (Q_stricmp(d->d_name, ".") && Q_stricmp(d->d_name, ".."))
	    {
	      if (strlen(subdirs))
		{
		  Com_sprintf (newsubdirs, sizeof(newsubdirs),
			       "%s/%s", subdirs, d->d_name);
		}
	      else
		{
		  Com_sprintf (newsubdirs, sizeof(newsubdirs), "%s", d->d_name);
		}
	      Sys_ListFilteredFiles (basedir, newsubdirs, filter, list, numfiles);
	    }
	}
      if (*numfiles >= MAX_FOUND_FILES - 1)
	{
	  break;
	}
      Com_sprintf (filename, sizeof(filename), "%s/%s", subdirs, d->d_name);
      if (!COM_FilterPath (filter, filename, false))
	{
	  continue;
	}
      list[*numfiles] = qce->mem.CopyString (filename);
      (*numfiles)++;
    }
  closedir (fdir);
}

/*
==================
Sys_ListFiles
==================
*/
char **
Sys_ListFiles (const char *directory, const char *extension, char *filter, int *numfiles, bool wantsubs)
{
  struct dirent *d;
  DIR           *fdir;
  bool      dironly = wantsubs;
  char          search[MAX_OSPATH];
  int           nfiles;
  char          **listCopy;
  char          *list[MAX_FOUND_FILES];
  int           i;
  struct stat   st;
  //int           extLen;

  if (filter)
    {
      nfiles = 0;
      Sys_ListFilteredFiles (directory, "", filter, list, &nfiles);

      list[nfiles] = NULL;
      *numfiles = nfiles;

      if (!nfiles)
	return NULL;

      listCopy = (char **)qce->mem.Z_Malloc ((nfiles + 1) * sizeof(*listCopy));
      for (i = 0 ; i < nfiles ; i++)
	{
	  listCopy[i] = list[i];
	}
      listCopy[i] = NULL;
      return listCopy;
    }

  if (!extension)
    extension = "";

  if (extension[0] == '/' && extension[1] == 0)
    {
      extension = "";
      dironly = true;
    }

  //extLen = strlen (extension);

  // search
  nfiles = 0;

  // opendir : <dirent.h> <sys/types.h>
  fdir = opendir(directory);
  if (fdir == NULL)
    {
      *numfiles = 0;
      return NULL;
    }

  // readdir : <dirent.h>
  while ((d = readdir(fdir)) != NULL)
    {
      Com_sprintf (search, sizeof(search), "%s/%s", directory, d->d_name);
      if (stat(search, &st) == -1)
	{
	  continue;
	}
      if ((dironly && !(st.st_mode & S_IFDIR)) ||
	  (!dironly && (st.st_mode & S_IFDIR)))
	{
	  continue;
	}

      if (*extension)
	{
	  // check if extension matches
	  if ((strlen(d->d_name) < strlen(extension)) ||
	      (Q_stricmp (d->d_name + strlen(d->d_name) - strlen(extension), extension)))
	    {
	      continue; // didn't match
	    }
	}

      if (nfiles == MAX_FOUND_FILES - 1)
	break;
      list [nfiles] = qce->mem.CopyString (d->d_name);
      nfiles++;
    }

  list [nfiles] = NULL;

  closedir (fdir);

  // return a copy of the list
  *numfiles = nfiles;

  if (!nfiles)
    {
      return NULL;
    }

  listCopy = qce->mem.Z_Malloc ((nfiles + 1) * sizeof (*listCopy));
  for (i = 0; i < nfiles; i++)
    {
      listCopy[i] = list[i];
    }
  listCopy[i] = NULL;

  return listCopy;
}

/*
==================
Sys_FreeFileList
==================
*/
void
Sys_FreeFileList (char **list)
{
  int i;

  if (!list)
    {
      return;
    }

  for (i = 0 ; list[i] != NULL ; i++)
    {
      qce->mem.Z_Free (list[i]);
    }

  qce->mem.Z_Free (list);
}

/*
==================
Sys_InputFD

return the current input file descriptor if open
==================
*/
int
Sys_InputFD (void)
{
  if (stdinIsATTY)
    {
      return FDIN;
    }

  return -1;
}

/*
==================
Sys_Sleep

Block execution for msec or until input is recieved.
==================
*/
void
Sys_Sleep (int msec)
{
  if (msec == 0)
    return;

  if (stdinIsATTY)
    {
      // FD_ZERO, FD_SET, select : <sys/select.h>
      fd_set fdset;

      FD_ZERO (&fdset);
      FD_SET (FDIN, &fdset);
      if (msec < 0)
	{
	  select ((FDIN + 1), &fdset, NULL, NULL, NULL);
	}
      else
	{
	  struct timeval timeout;

	  timeout.tv_sec = msec / 1000;
	  timeout.tv_usec = (msec % 1000) * 1000;
	  select ((FDIN + 1), &fdset, NULL, NULL, &timeout);
	}
    }
  else
    {
      // With nothing to select() on, we can't wait indefinitely
      if (msec < 0)
	msec = 10;

      usleep (msec * 1000);
    }
}

#define MILLI2NANO 1000000
#define NANO2MILLI 0.000001
unsigned long sys_timeBase2 = 0;
int curtime2;
/*
================
Sys_Milliseconds2
================
*/
unsigned int
Sys_Milliseconds (void)
{
  struct timespec time;

  if (clock_gettime (CLOCK_MONOTONIC, &time) == -1)
    {
      perror ("Sys_MS () : clock_gettime");
      SYS_ERROR (ERR_FATAL, "Sys_Milliseconds failed");
    }

  // could use an init function to avoid this check on every Sys_Milliseconds call
  // but it's not used often so it's ok
  if (!sys_timeBase2)
    {
      sys_timeBase2 = time.tv_sec;
      return time.tv_nsec * NANO2MILLI;
    }

  curtime2 = (time.tv_sec - sys_timeBase2) * 1000 + time.tv_nsec * NANO2MILLI;

  return curtime2;
}

/*
==================
Sys_Sleep2

Block execution for msec
==================
*/
void
Sys_Sleep2 (int msec)
{
  struct timespec sleep;
  div_t conv;

  if (msec == 0)
    return;

  if (msec < 0)
    return;

  // grab quotient and remainder
  conv = div (msec, 1000);

  // fill the sleep structure
  sleep.tv_sec = conv.quot;
  sleep.tv_nsec = conv.rem * MILLI2NANO;
  // sleeping
  if (clock_nanosleep (CLOCK_MONOTONIC, 0, &sleep, NULL) == -1)
    {
      perror ("nanosleep");
      SYS_ERROR (ERR_FATAL, "Sys_Sleep2 failed : nanosleep ()");
    }
}

/*
==============
Sys_SetEnv

set/unset environment variables (empty value removes it)
==============
*/
void
Sys_SetEnv (const char *name, const char *value)
{
  if (value && *value)
    setenv (name, value, 1);
  else
    unsetenv (name);
}

/*
==============
Sys_PlatformInit

Unix specific initialisation
==============
*/
void
Sys_PlatformInit (void)
{
  const char *term = getenv ("TERM");

  // signal : <signal.h>
  signal (SIGHUP, Sys_SigHandler);
  signal (SIGQUIT, Sys_SigHandler);
  signal (SIGTRAP, Sys_SigHandler);
  signal (SIGIOT, Sys_SigHandler);
  signal (SIGBUS, Sys_SigHandler);

  signal (SIGILL, Sys_SigHandler);
  signal (SIGFPE, Sys_SigHandler);
  signal (SIGSEGV, Sys_SigHandler);
  signal (SIGTERM, Sys_SigHandler);
  signal (SIGINT, Sys_SigHandler);

  stdinIsATTY = isatty (FDIN) && !(term && (!strcmp (term, "raw")
					    || !strcmp (term, "dumb")));

}

/*
==============
Sys_PlatformExit

Unix specific deinitialisation
==============
*/
void Sys_PlatformExit( void )
{
}

/*
==============
Sys_ErrorDialog

Display an error message
==============
*/
void
Sys_ErrorDialog (const char *error)
{
  char buffer [1024];
  unsigned int size;
  int f;
  const char *fileName = "crashlog.txt";

  // homePath may not be valid then try binaryPath which may also not be valid..
  char *ospath = Sys_FS_BuildOSPath (syspath.homePath, NULL, fileName);

  if (mkdir (syspath.homePath, 0777) == -1)
    {
      if (errno != EEXIST)
	{
	  Sys_Print (va("Error: Unable to create directory \"%s\", "
			"error is %s(%d)\n"
			"Trying with \"%s\"\n",
			syspath.homePath, strerror (errno), errno,
			syspath.binaryPath));
	}

      //trying agin with binaryPath, this one exists but can we write ?
      ospath = Sys_FS_BuildOSPath (syspath.binaryPath, NULL, fileName);
    }

  Sys_Print (va("Error: %s\n", error));

  /* make sure the write path for the crashlog exists... */
  /*
  if (qce->fs.FS_CreatePath (ospath))
    {
      SYS_PRINT ("ERROR: couldn't create path '%s' for crash log.\n", ospath);
      return;
    }
  */

  /* we might be crashing because we maxed out the Quake MAX_FILE_HANDLES,
     which will come through here, so we don't want to recurse forever by
     calling FS_FOpenFileWrite()...use the Unix system APIs instead. */
  f = open (ospath, O_CREAT | O_TRUNC | O_WRONLY, 0640);
  if (f == -1)
    {
      Sys_Print (va("Error: couldn't open %s - %s\n", fileName, strerror (errno)));
      return;
    }

  /* We're crashing, so we don't care much if write() or close() fails. */
  while ((size = CON_LogRead (buffer, sizeof (buffer))) > 0)
    {
      if (write (f, buffer, size) != (ssize_t)size)
	{
	  Sys_Print (va("ERROR: couldn't fully write to %s\n", fileName));
	  break;
	}
    }
  close(f);
}

/*
==================
Sys_GetClipboardData
==================
*/
const char *Sys_GetClipboardData(void)
{
	return NULL;
}

/*
==================
Sys_LowPhysicalMemory

TODO
==================
*/
bool Sys_LowPhysicalMemory( void )
{
	return false;
}

/*
==============
Sys_PID
==============
*/
int
Sys_PID (void)
{
  return getpid ();
}

/*
==============
Sys_PIDIsRunning
==============
*/
bool
Sys_PIDIsRunning (int pid)
{
  return (kill (pid, 0) == 0);
}

/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include <signal.h>
#include <limits.h>

#include <sys/stat.h>
#include <sys/types.h>
#ifndef _WIN32
#include <sys/mman.h>
#endif
#include <sys/time.h>

#include <dirent.h>
#include <unistd.h>

#include <ctype.h>
#include <errno.h>

#ifndef _WIN32
#include <pwd.h>
#endif
#include <libgen.h>
#include <fcntl.h>

#include "sys_local.h"
#include "sys_loadlib.h"
#include "sys_public_dlib.h"

sys_path_t syspath;

cvar_t * con_ansiColor;

sys_import_t sysi;
bool sysIsSilent; // if sys is silent, don't open tty etc...

/*
======================
Functions needed for each OS :

Sys_DefaultHomePath
Sys_Basename
Sys_Dirname

Sys_GetCurrentUser
Sys_GetClipboardData
Sys_Mkdir
Sys_Cwd
Sys_SetEnv

Sys_ListFilteredFiles
Sys_ListFiles
Sys_FreeFileList
Sys_Sleep
Sys_InputFD
Sys_Milliseconds
Sys_RandomBytes

Sys_PlatformInit
Sys_ErrorDialog
*/

/*
====================
Sys_FS_ReplaceSeparators

Fix things up differently for win/unix/mac
====================
*/
static void
Sys_FS_ReplaceSeparators (char *path)
{
  char	*s;

  for (s = path; *s; s++)
    {
      if (*s == '/' || *s == '\\')
	{
	  // PATH_SEP is defined in q_plateform.h
	  *s = PATH_SEP;
	}
    }
}

/*
===================
Sys_FS_BuildOSPath

Qpath may have either forward or backwards slashes
===================
*/
char *
Sys_FS_BuildOSPath (const char *base, const char *game, const char *qpath)
{
  char		temp[MAX_OSPATH];
  static char	ospath[2][MAX_OSPATH];
  static int	toggle;

  toggle ^= 1;		// flip-flop to allow two returns without clash

  // we may be in the default configuration, try the validgamepath
  // which must be ... valid otherwise just use "BASEGAME"
  if (game != NULL && !game[0])
    {
      if (qce && qce->fs.validGamePath)
	{
	  game = qce->fs.validGamePath ();
	}
      else
	{
	  game = BASEGAME;
	}
    }
  // if we don't want to look for mods, game may be NULL
  else if (game == NULL)
    {
      game = "";
    }

  Com_sprintf (temp, sizeof(temp), "/%s/%s", game, qpath);
  Sys_FS_ReplaceSeparators (temp);
  Com_sprintf (ospath[toggle], sizeof(ospath[0]), "%s%s", base, temp);

  return ospath[toggle];
}

/*
=================
Sys_SigHandler
=================
*/
void
Sys_SigHandler (int signal)
{
  static bool signalcaught = false;

  if (signalcaught)
    {
      fprintf (stderr, "DOUBLE SIGNAL FAULT: Received signal %d, exiting...\n",	signal);
    }
  else
    {
      signalcaught = true;
      fprintf (stderr, "Received signal %d, exiting...\n", signal);

      sysi.shutdown ("Signal caught");

      if (qcomloaded)
	qce->Com_Shutdown ();
    }

  Sys_Exit (0); // Exit with 0 to avoid recursive signals
}

/*
============
Sys_FileTime

returns -1 if not present
============
*/
int
Sys_FileTime (char *path)
{
  struct stat buf;

  // from <sys/stat> <sys/types.h>
  if (stat (path, &buf) == -1)
    {
      perror("Sys_FileTime : stat");
      return -1;
    }

  return buf.st_mtime;
}

/*
=================
Sys_Print
=================
*/
void
Sys_Print (const char *msg)
{
  CON_LogWrite (msg);
  CON_Print (msg);
}

/*
=================
Sys_Error
=================
*/
void
Sys_Error (const char *error, ...)
{
  va_list argptr;
  char    string[1024];

  va_start (argptr,error);
  Q_vsnprintf (string, sizeof(string), error, argptr);
  va_end (argptr);

  Sys_ErrorDialog (string);

  Sys_Exit (1);
}

/*
=================
Sys_Warn
=================
*/
void
Sys_Warn (char *warning, ...)
{
  va_list argptr;
  char    string[1024];

  va_start (argptr,warning);
  Q_vsnprintf (string, sizeof(string), warning, argptr);
  va_end (argptr);

  CON_Print (va("Warning: %s", string));
}

/*
===================================
NOT PLATFORM specific

===================================
*/

/*
=================
Sys_SetBinaryPath
=================
*/
void
Sys_SetBinaryPath (const char *path)
{
  Q_strncpyz (syspath.binaryPath, path, sizeof(syspath.binaryPath));
}

/*
=================
Sys_BinaryPath
=================
*/
const char *
Sys_BinaryPath (void)
{
  return syspath.binaryPath;
}

/*
=================
Sys_SetDefaultInstallPath
=================
*/
void
Sys_SetDefaultInstallPath (const char *path)
{
  Q_strncpyz (syspath.installPath, path, sizeof(syspath.installPath));
}

/*
=================
Sys_DefaultInstallPath
=================
*/
const char *
Sys_DefaultInstallPath(void)
{
  if (*syspath.installPath)
    return syspath.installPath;
  else
    return Sys_Cwd ();
}

/*
=================
Sys_DefaultAppPath
=================
*/
const char *
Sys_DefaultAppPath(void)
{
  return Sys_BinaryPath ();
}

/*
=================
Sys_ConsoleInput

Handle new console input
=================
*/
const char *
Sys_ConsoleInput(void)
{
  return CON_Input ();
}

#define PID_FILENAME PRODUCT_NAME

/*
=================
 Sys_PIDFileName
=================
*/
static char *
Sys_PIDFileName (void)
{
  return va ("%s/%s-%d.pid", Sys_TempPath (), PID_FILENAME, Sys_PID ());
}

/*
=================
 Sys_WritePIDFile

 Return true if there is an existing stale PID file
=================
*/
bool
Sys_WritePIDFile (void)
{
  char *pidFile = Sys_PIDFileName ();
  FILE *f;
  bool stale = false;

  // First, check if the pid file is already there
  if ((f = fopen (pidFile, "r")) != NULL)
    {
      char pidBuffer[64] = {0};
      int pid;

      pid = fread (pidBuffer, sizeof (char), sizeof (pidBuffer) - 1, f);
      fclose (f);

      if (pid > 0)
	{
	  pid = atoi (pidBuffer);
	  if (!Sys_PIDIsRunning (pid))
	    stale = true;
	}
      else
	stale = true;
    }

  if ((f = fopen (pidFile, "w")) != NULL)
    {
      fprintf (f, "%d", Sys_PID ());
      fclose (f);
    }
  else
    SYS_PRINT (S_COLOR_YELLOW "Couldn't write %s.\n", pidFile);

  return stale;
}

/*
=================
Sys_Exit

Single exit point (regular exit or in case of error)
=================
*/
void
Sys_Exit (int ex)
{
  CON_Shutdown ();

  Sys_PlatformExit ();

  if (!ex)
    {
      // Normal exit
      remove (Sys_PIDFileName ());
    }

#ifdef NDEBUG
  exit (ex);
#else

  // Cause a backtrace on error exits
  assert (ex == 0);
  exit (ex);
#endif
}

/*
=================
Sys_Quit
=================
*/
void
Sys_Quit (void)
{
  Sys_Exit (0);
}

#include <locale.h>
/*
=================
Sys_Init
=================
*/
void
Sys_Init (void)
{
  qce->cvar.Set ("arch", OS_STRING " " ARCH_STRING);
  qce->cvar.Set ("username", Sys_GetCurrentUser ());
  con_ansiColor = qce->cvar.Get ("con_ansiColor", "0", CVAR_ARCHIVE);

  // initializing locales
#ifdef _WIN32
  // setlocale doesn't support utf8 on win32
  if (!setlocale (LC_ALL, "C"))
    {
      // error can't set to native locale
      SYS_ERROR (ERR_FATAL, "Failed to set LC_ALL to native locale\n");
    }
#else
  if (!setlocale (LC_ALL, ""))
    {
      // error can't set to native locale
      SYS_ERROR (ERR_FATAL, "Failed to set LC_ALL to native locale: %m\n");
    }
#endif

  // keep the regular "." as decimal separator
#ifdef _WIN32
  if (!setlocale (LC_NUMERIC, "C"))
    {
      // error can't set to posix locale
      SYS_ERROR (ERR_FATAL, "Failed to set LC_NUMERIC to POSIX locale\n");
    }
#else
  if (!setlocale (LC_NUMERIC, "POSIX"))
    {
      // error can't set to posix locale
      SYS_ERROR (ERR_FATAL, "Failed to set LC_NUMERIC to POSIX locale: %m\n");
    }
#endif
}

void
Sys_Set_Interface (sys_import_t *sys_import, bool silent)
{
  sysi = *sys_import;

  sysIsSilent = silent;
}

/*
=================
Sys_ParseArgs
=================
*/
static void
Sys_ParseArgs (int argc, char **argv)
{
  if (argc == 2)
    {
      if (!strcmp (argv[1], "--version") ||
	  !strcmp (argv[1], "-v"))
	{
	  const char* date = __DATE__;
	  fprintf (stdout, Q3_VERSION " dedicated server (%s)\n", date);
	  Sys_Exit(0);
	}
      if (!strcmp (argv[1], "--help") ||
	  !strcmp (argv[1], "-h"))
	{
	  fprintf (stdout, "help page for " Q3_VERSION "\n");
	  Sys_Exit(0);
	}
    }
}

/*
=================
Sys_ArgsInit
=================
*/
void
Sys_ArgsInit (int argc, char ** argv)
{
  Sys_ParseArgs (argc, argv);
  Sys_SetBinaryPath (Sys_Dirname (argv[0]));
  Sys_SetDefaultInstallPath (Sys_BinaryPath());
}

/*
===================================
*/

/* * * * Dynamic Library Functions * * * */

// for these functions we will use server/client output functions
// as this my be echoed in the game console and logged
// but not for early libraries where a "quiet" mode is provied
// and output is only made on tty (this will not be archived)

/*
=================
Sys_DynLibUnload
=================
*/
void
Sys_DLUnload (void *dynHandle)
{
  char *error;
  int ierr;
  if (dynHandle == NULL)
    {
      SYS_PRINT ("Sys_DLUnload (NULL)\n");
      return;
    }

  ierr = Sys_UnloadLibrary (dynHandle);
  error = Sys_LibraryError ();
#ifdef _WIN32
  if (!ierr)
    SYS_ERROR (ERR_DROP, "Couldn't unload library in Sys_DLUnload ()\n");
#else
  if (error != NULL || ierr)
    SYS_ERROR (ERR_DROP, "Couldn't unload library in Sys_DLUnload () : %s\n", error);
#endif
}

/*
==================
Sys_DynLibLoadFunction

Given the libHandle grab the funcname function
==================
*/
void
*Sys_DLLoadFunction (void *dlHandle, const char *funcname,
		     bool failexit)
{
  char *error;
  void *ptr;

  ptr = Sys_LoadFunction (dlHandle, funcname);

  error = Sys_LibraryError ();
  if (error != NULL || ptr == NULL)
    {
      if (failexit == true)
	{
	  SYS_ERROR (ERR_FATAL, "Sys_DLLoadFunction () FATAL on function %s : %s\n", funcname, error);
	}
      else
	{
	  SYS_PRINT ("Sys_DLLoadFunction () IGNORE on function %s : %s\n", funcname, error);
	}
      // clear errors
      Sys_LibraryError ();
    }

  return ptr;
}

/*
=================
Sys_DynLibLoadLibrary

Given the path, try to load the library
=================
*/
static void
*Sys_DLLoadLibrary (const char* base, const char* gamedir, const char* fname,
		    char* fqpath)
{
  void* libHandle;
  char* fn;
  char * error;

  *fqpath = 0;

  fn = Sys_FS_BuildOSPath (base, gamedir, fname);
  SYS_PRINT ("Sys_DLLoadLibrary(%s)... \n", fn);

  libHandle = Sys_LoadLibrary (fn);

  error = Sys_LibraryError ();
  if (error != NULL || libHandle == NULL)
    {
      SYS_PRINT ("Sys_DLLoadLibrary(%s) failed:\n\"%s\"\n", fn, error);
      // clear errors
      Sys_LibraryError ();
      return NULL;
    }

  SYS_PRINT ("Sys_DLLoadLibrary(%s): succeeded ...\n", fn);

  Q_strncpyz (fqpath, fn, MAX_QPATH);

  return libHandle;
}

/*
=================
Sys_DynLibLoad
Load a Dynamic Library
#1 look down current path
#2 look in fs_homepath
#3 look in fs_basepath
=================
*/
void
*Sys_DLLoadType2 (const char *name, char *fqpath, DlType_t type, bool errorfatal)
{
  void  *libHandle = NULL;
  char  fname[MAX_OSPATH];
  char  *basepath;
  char  *homepath;
  const char * pwdpath;
  char  *gamedir;
  char	*(*infofunc) (void);

  assert (name);

  Com_sprintf (fname, sizeof(fname), "%s" ARCH_STRING DLL_EXT, name);

  pwdpath = Sys_Cwd();

  // qce resolved before loading mods
  if (type == MOD)
    {
      if (!qcomloaded)
	{
	  SYS_ERROR (ERR_FATAL, "Sys_DLLoadType () can't load mods without qcommon loaded !\n");
	}
      basepath = qce->cvar.VariableString ("fs_basepath");
      homepath = qce->cvar.VariableString ("fs_homepath");
      gamedir = qce->cvar.VariableString ("fs_game");

      libHandle = Sys_DLLoadLibrary (pwdpath, gamedir, fname, fqpath);

      if (libHandle == NULL && homepath) // homepath == "" is valid
	libHandle = Sys_DLLoadLibrary (homepath, gamedir, fname, fqpath);

      if(libHandle == NULL && basepath) // same here
	libHandle = Sys_DLLoadLibrary (basepath, gamedir, fname, fqpath);

    }
  else if (type == CLIENT)
    {
      gamedir = "lib";

      // must be in the lib/ directory near the binary
      libHandle = Sys_DLLoadLibrary (pwdpath, gamedir, fname, fqpath);
    }

  if (libHandle == NULL)
    {
      if (errorfatal)
	SYS_ERROR (ERR_FATAL, "Sys_DLLoadType (%s) failed : no valid library found\n", name);
      else
	SYS_PRINT (S_COLOR_YELLOW "Sys_DLLoadType (%s) failed : no valid library found\n", name);
    }

  // if we don't find it, don't get an error, this isn't critical
  infofunc = (char *(*)(void))Sys_DLLoadFunction (libHandle, "lib_info", false);

  if (infofunc != NULL)
    {
      SYS_PRINT ("infofunc (%s) : %s\n", name, infofunc ());
    }

  return libHandle;
}

void
*Sys_DLLoadType (const char *name, char *fqpath, DlType_t type)
{
  return Sys_DLLoadType2 (name, fqpath, type, false);
}

void
*Sys_DLLoad (const char *name, char *fqpath)
{
  return Sys_DLLoadType2 (name, fqpath, MOD, false);
}

// PUT THIS IN ANOTHER FILE !
// SET here so it can be shared with con_tty and con_passive
/*
=================
CON_AnsiColorPrint

Transform Q3 colour codes to ANSI escape sequences
=================
*/
#define NB_ANSICOLORS 8
void
CON_AnsiColorPrint (const char *msg)
{
  static char buffer [MAXPRINTMSG];
  int         length = 0;
  static int  q3ToAnsi [NB_ANSICOLORS] =
    {
      30, // COLOR_BLACK
      31, // COLOR_RED
      32, // COLOR_GREEN
      33, // COLOR_YELLOW
      34, // COLOR_BLUE
      36, // COLOR_CYAN
      35, // COLOR_MAGENTA
      0   // COLOR_WHITE
    };

  while (*msg)
    {
      if (Q_IsColorString (msg) || *msg == '\n')
	{
	  // First empty the buffer
	  if (length > 0)
	    {
	      buffer [length] = '\0';
	      fputs (buffer, stdout);
	      length = 0;
	    }

	  if (*msg == '\n')
	    {
	      // Issue a reset and then the newline
	      fputs ("\033[0m\n", stdout);
	      msg++;
	    }
	  else
	    {
	      // Print the color code
	      Com_sprintf (buffer, sizeof (buffer), "\033[%dm",
			   q3ToAnsi [ColorIndex(*(msg + 1))%NB_ANSICOLORS]);
	      fputs (buffer, stdout);
	      msg += 2;
	    }
	}
      else
	{
	  if (length >= MAXPRINTMSG - 1)
	    break;

	  buffer [length] = *msg;
	  length++;
	  msg++;
	}
    }

  // Empty anything still left in the buffer
  if (length > 0)
    {
      buffer [length] = '\0';
      fputs (buffer, stdout);
    }
}

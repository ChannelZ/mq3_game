/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/

#include "qcommon/q_shared.h"
#include "qcommon/qcom_extern.h"
#include "qcommon/qcom_public.h"

#include "sys_public_interface.h"

// everything here is private to sys_ and con_ files

extern qcom_export_t * qce;
extern sys_import_t sysi;
extern bool sysIsSilent;

extern cvar_t * con_ansiColor;

// avoid retyping and substitutions
#define qcomloaded sysi.isQcomLoaded()

#define SYS_PRINT sysi.print
#define SYS_DPRINT sysi.dprint
#define SYS_ERROR sysi.error

struct sys_path_s
{
  char binaryPath [MAX_OSPATH]; // 256
  char installPath [MAX_OSPATH];
  char homePath [MAX_OSPATH];
};
typedef struct sys_path_s sys_path_t;

extern sys_path_t syspath;

//#define FDOUT fileno(stdout), so we can eventually redirect them
#define FDOUT	STDOUT_FILENO
#define FDIN	STDIN_FILENO

// Console
void CON_Shutdown (void);
//void CON_Init (void); // public
const char *CON_Input (void);
void CON_Print (const char *message);

unsigned int CON_LogWrite (const char *in);
unsigned int CON_LogRead (char *out, unsigned int outSize);

void CON_AnsiColorPrint (const char *msg);

char *Sys_FS_BuildOSPath (const char *base, const char *game, const char *qpath);
// this one is static
//void Sys_ListFilteredFiles (const char *basedir, char *subdirs, char *filter,
//			    char **list, int *numfiles)

// Platform dependant
void Sys_ErrorDialog (const char *error);

void Sys_SigHandler (int signal);
void Sys_Exit (int ex);

int Sys_PID (void);
bool Sys_PIDIsRunning (int pid);
const char *Sys_TempPath (void);

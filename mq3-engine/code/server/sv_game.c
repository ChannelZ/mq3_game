/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// sv_game.c -- interface to the game dll

#include "server.h"

void
SV_GameError (const char *string)
{
  SV_Error (ERR_DROP, "%s", string);
}

void
SV_GamePrint (const char *string)
{
  SV_Printf ("%s", string);
}

// these functions must be used instead of pointer arithmetic, because
// the game allocates gentities with private information after the server shared part
int
SV_NumForGentity (sharedEntity_t *ent)
{
  int num;

  num = ((byte *)ent - (byte *)sv.gentities) / sv.gentitySize;

  return num;
}

sharedEntity_t *
SV_GentityNum (int num)
{
  sharedEntity_t *ent;

  ent = (sharedEntity_t *)((byte *)sv.gentities + sv.gentitySize * num);

  return ent;
}

playerState_t *
SV_GameClientNum (int num)
{
  playerState_t	*ps;

  ps = (playerState_t *)((byte *)sv.gameClients + sv.gameClientSize * num);

  return ps;
}

svEntity_t *
SV_SvEntityForGentity (sharedEntity_t *gEnt)
{
  if (gEnt == NULL || gEnt->s.number < 0 || gEnt->s.number >= MAX_GENTITIES)
    {
      SV_Error (ERR_DROP, "SV_SvEntityForGentity: bad gEnt");
    }
  return &sv.svEntities[gEnt->s.number];
}

sharedEntity_t *
SV_GEntityForSvEntity (svEntity_t *svEnt)
{
  int num;

  num = svEnt - sv.svEntities;
  return SV_GentityNum (num);
}

/*
===============
SV_GameSendServerCommand

Sends a command string to a client
===============
*/
void
SV_GameSendServerCommand (int clientNum, const char *text)
{
  if (clientNum == -1)
    {
      SV_SendServerCommand (NULL, "%s", text);
    }
  else
    {
      if (clientNum < 0 || clientNum >= sv_maxclients->integer)
	{
	  return;
	}
      SV_SendServerCommand (svs.clients + clientNum, "%s", text);
    }
}

/*
===============
SV_GameDropClient

Disconnects the client with a message
===============
*/
void
SV_GameDropClient (int clientNum, const char *reason)
{
  if (clientNum < 0 || clientNum >= sv_maxclients->integer)
    {
      return;
    }
  SV_DropClient (svs.clients + clientNum, reason);
}

/*
=================
SV_SetBrushModel

sets mins and maxs for inline bmodels
=================
*/
void
SV_SetBrushModel (sharedEntity_t *ent, const char *name)
{
  clipHandle_t	h;
  vec3_t	mins, maxs;

  if (name == NULL)
    {
      SV_Error (ERR_DROP, "SV_SetBrushModel: NULL");
    }

  if (name[0] != '*')
    {
      SV_Error (ERR_DROP, "SV_SetBrushModel: %s isn't a brush model", name);
    }

  ent->s.modelindex = atoi(name + 1);

  h = qce->cm.InlineModel (ent->s.modelindex);
  qce->cm.ModelBounds (h, mins, maxs);
  VectorCopy (mins, ent->r.mins);
  VectorCopy (maxs, ent->r.maxs);
  ent->r.bmodel = true;

  ent->r.contents = -1;		// we don't know exactly what is in the brushes

  // this should be done explicitely
  //SV_LinkEntity (ent);		// FIXME: remove
}

/*
=================
SV_inPVS

Also checks portalareas so that doors block sight
=================
*/
bool
SV_inPVS (const vec3_t p1, const vec3_t p2)
{
  int	leafnum;
  int	cluster;
  int	area1, area2;
  byte	*mask;

  leafnum = qce->cm.PointLeafnum (p1);
  cluster = qce->cm.LeafCluster (leafnum);
  area1 = qce->cm.LeafArea (leafnum);
  mask = qce->cm.ClusterPVS (cluster);

  leafnum = qce->cm.PointLeafnum (p2);
  cluster = qce->cm.LeafCluster (leafnum);
  area2 = qce->cm.LeafArea (leafnum);

  if (mask && (!(mask[cluster>>3] & (1<<(cluster&7)))))
    return false;
  if (!qce->cm.AreasConnected (area1, area2))
    return false;		// a door blocks sight
  return true;
}

/*
=================
SV_inPVSIgnorePortals

Does NOT check portalareas
=================
*/
bool
SV_inPVSIgnorePortals (const vec3_t p1, const vec3_t p2)
{
  int	leafnum;
  int	cluster;
  byte	*mask;

  leafnum = qce->cm.PointLeafnum (p1);
  cluster = qce->cm.LeafCluster (leafnum);
  mask = qce->cm.ClusterPVS (cluster);

  leafnum = qce->cm.PointLeafnum (p2);
  cluster = qce->cm.LeafCluster (leafnum);

  if (mask && (!(mask[cluster>>3] & (1<<(cluster&7)))))
    return false;

  return true;
}

/*
========================
SV_AdjustAreaPortalState
========================
*/
void
SV_AdjustAreaPortalState (sharedEntity_t *ent, bool open)
{
  svEntity_t *svEnt;

  svEnt = SV_SvEntityForGentity (ent);
  if (svEnt->areanum2 == -1)
    {
      return;
    }
  qce->cm.AdjustAreaPortalState (svEnt->areanum, svEnt->areanum2, open);
}

/*
==================
SV_GameAreaEntities
==================
*/
bool
SV_EntityContact (const vec3_t mins, const vec3_t maxs,
		  const sharedEntity_t *gEnt)
{
  const float * origin;
  clipHandle_t	ch;
  trace_t	trace;

  // check for exact collision
  origin = gEnt->r.currentOrigin;

  ch = SV_ClipHandleForEntity (gEnt);
  qce->cm.TransformedBoxTrace (&trace, vec3_origin, vec3_origin,
			       mins, maxs, ch, origin, -1);

  return trace.startsolid;
}

/*
===============
SV_GetServerinfo

===============
*/
void
SV_GetServerinfo (char *buffer, int bufferSize)
{
  if (bufferSize < 1)
    {
      SV_Error (ERR_DROP, "SV_GetServerinfo: bufferSize == %i", bufferSize);
    }
  Q_strncpyz (buffer, qce->cvar.InfoString(CVAR_SERVERINFO), bufferSize);
}

/*
===============
SV_LocateGameData

===============
*/
// gentities first structure is a sharedEntity_t, so we can use a hack to win memory
// by doing pointers/memory arithmetics
// a table of sharedEntity_t pointers could be a proper solution too, as there aren't
// often more than 200 entities in levels
void
SV_LocateGameData (sharedEntity_t *gEnts, int numGEntities, int sizeofGEntity_t,
		   playerState_t *clients, int sizeofGameClient)
{
  sv.gentities = gEnts;
  sv.gentitySize = sizeofGEntity_t;
  sv.num_entities = numGEntities;

  sv.gameClients = clients;
  sv.gameClientSize = sizeofGameClient;
}

/*
===============
SV_GetUsercmd

===============
*/
void
SV_GetUsercmd (int clientNum, usercmd_t *cmd)
{
  if (clientNum < 0 || clientNum >= sv_maxclients->integer)
    {
      SV_Error (ERR_DROP, "SV_GetUsercmd: bad clientNum:%i", clientNum);
    }
  *cmd = svs.clients[clientNum].lastUsercmd;
}

//==============================================

bool
SV_GetEntityToken (char *buffer, int bufferSize)
{
  const char *s;

  s = COM_Parse (&sv.entityParsePoint);
  Q_strncpyz (buffer, s, bufferSize);
  if (!sv.entityParsePoint && !s[0])
    {
      return false;
    }
  else
    {
      return true;
    }
}

bool
SV_InitEntityString (void)
{
  // start the entity parsing at the beginning
  sv.entityParsePoint = qce->cm.EntityString ();

  return true;
}

/*
==============
GetServerLibAPI (bad name !)
==============
*/
bool
GetServerLibAPI (gamelib_import_t * svlib)
{
  if (svlib == NULL)
    {
      SV_Error (ERR_DROP, "GetServerLibAPI () : argument is NULL\n");
      return false;
    }
  // initialize and fill the structure
  Com_Memset (svlib, 0, sizeof (*svlib));

  svlib->Printf = SV_Printf;
  svlib->Error = SV_Error;
  svlib->Milliseconds = Sys_Milliseconds;
  svlib->Argc = qce->cmd.Argc;
  svlib->Argv = qce->cmd.Argv;
  svlib->ArgvBuffer = qce->cmd.ArgvBuffer;
  svlib->Args = qce->cmd.ArgsBuffer;
  svlib->ArgsFrom = qce->cmd.ArgsFrom;
  svlib->FS_FOpenFile = qce->fs.FOpenFileByMode;
  svlib->FS_Read = qce->fs.Read;
  svlib->FS_Write = qce->fs.Write;
  svlib->FS_FCloseFile = qce->fs.FCloseFile;
  svlib->FS_GetFileList = qce->fs.GetFileList;
  svlib->FS_Seek = qce->fs.Seek;
  svlib->SendConsoleCommand = qce->cmd.Cbuf_ExecuteText;
  svlib->Cvar_Get = qce->cvar.Get;
  svlib->Cvar_Set = qce->cvar.Set;
  svlib->Cvar_SetNew = qce->cvar.SetNew;
  svlib->Cvar_CheckRange = qce->cvar.CheckRange;
  svlib->Cvar_TrackChange = qce->cvar.TrackChange;
  svlib->Cvar_VariableIntegerValue = qce->cvar.VariableIntegerValue;
  svlib->Cvar_VariableValue = qce->cvar.VariableValue;
  svlib->Cvar_VariableString = qce->cvar.VariableString;
  svlib->Cvar_VariableStringBuffer = qce->cvar.VariableStringBuffer;
  svlib->AddCommand = qce->cmd.AddCommand;
  svlib->RemoveCommand = qce->cmd.RemoveCommand;
  svlib->LocateGameData = SV_LocateGameData;
  svlib->DropClient = SV_GameDropClient;
  svlib->SendServerCommand = SV_GameSendServerCommand;
  svlib->SetConfigstring = SV_SetConfigstring;
  svlib->GetConfigstring = SV_GetConfigstring;
  svlib->SetUserinfo = SV_SetUserinfo;
  svlib->GetUserinfo = SV_GetUserinfo;
  svlib->GetServerinfo = SV_GetServerinfo;
  svlib->SetBrushModel = SV_SetBrushModel;
  svlib->Trace = SV_Trace;
  svlib->PointContents = SV_PointContents;
  svlib->InPVS = SV_inPVS;
  svlib->InPVSIgnorePortals = SV_inPVSIgnorePortals;
  svlib->AdjustAreaPortalState = SV_AdjustAreaPortalState;
  svlib->AreasConnected = qce->cm.AreasConnected;
  svlib->LinkEntity = SV_LinkEntity;
  svlib->UnlinkEntity = SV_UnlinkEntity;
  svlib->EntitiesInBox = SV_AreaEntities;
  svlib->EntityContact = SV_EntityContact;
  svlib->GetUsercmd = SV_GetUsercmd;
  svlib->GetEntityToken = SV_GetEntityToken;
  svlib->InitEntityString = SV_InitEntityString;
  svlib->SnapVector = SnapVector;
  svlib->RealTime = qce->Com_RealTime;

  svlib->DebugPolygonCreate = BotImport_DebugPolygonCreate;
  svlib->DebugPolygonDelete = BotImport_DebugPolygonDelete;

  svlib->SV_BotAllocateClient = SV_BotAllocateClient;
  svlib->SV_BotFreeClient = SV_BotFreeClient;

  svlib->SV_BotLibSetup = SV_BotLibSetup;
  svlib->SV_BotLibShutdown = SV_BotLibShutdown;

  svlib->SV_BotGetSnapshotEntity = SV_BotGetSnapshotEntity;
  svlib->SV_BotGetConsoleMessage = SV_BotGetConsoleMessage;
  svlib->SV_BotUserCommand = SV_BotUserCommand;

  svlib->botlib = botlib_export;

  return true;
}

/*
===============
SV_ShutdownGameProgs

Called every time a map changes
===============
*/
void
SV_ShutdownGameProgs (void)
{
  // do we have a gamelib handle ?
  if (gdlib.dynHandle == NULL)
    {
      return;
    }
  // is the extern gamelib existing
  if (glibe == NULL)
    {
      return;
    }
  // is the function valid ? (this one should be ok if the previous one is
  if (glibe->shutdown == NULL)
    {
      return;
    }
  SV_Printf ("SV_ShutdownGameProgs\n");
  glibe->shutdown (false);
  //glibe->shutdown = NULL;
  glibe = NULL;
  Sys_DLUnload (gdlib.dynHandle); // sets dynHandle to NULL
}

/*
==================
SV_InitGameVM

Called for both a full init and a restart
==================
*/
static void
SV_InitGameVM (bool restart)
{
  int i;

  SV_InitEntityString ();

  // clear all gentity pointers that might still be set from
  // a previous level
  //   now done before GAME_INIT call
  for (i = 0; i < sv_maxclients->integer; i++)
    {
      svs.clients[i].gentity = NULL;
    }

  // use the current msec count for a random seed
  // init for this gamestate
  SV_Printf ("sv_game.c : Calling GameInit () from SV_InitGameVM ()\n");
  glibe->init (sv.time, qce->event.Milliseconds(), restart);
}

/*
===================
SV_RestartGameProgs

Called on a map_restart, but not on a normal map change
===================
*/
void
SV_RestartGameProgs (void)
{
  // do we have a gamelib handle ?
  if (gdlib.dynHandle == NULL)
    {
      return;
    }
  // is the extern gamelib existing
  if (glibe == NULL)
    {
      return;
    }
  // is the function valid ? (this one should be ok if the previous one is
  if (glibe->shutdown == NULL)
    {
      return;
    }

  glibe->shutdown (true);

  SV_InitGameVM (true);
}

/*
===============
SV_InitGameProgs

Called on a normal map change, not on a map_restart
===============
*/
void
SV_InitGameProgs (void)
{
  void * Hdl;
  gamelib_export_t *(*GameAPI) (int, gamelib_import_t *);
  cvar_t *var;

  var = qce->cvar.Get ("bot_enable", "1", CVAR_LATCH);
  if (var)
    {
      bot_enable = var->integer;
    }
  else
    {
      bot_enable = 0;
    }

  // free the structure
  Com_Memset (&gdlib, 0, sizeof (gdlib));

  // load the dynamic library
  gdlib.dynHandle = Sys_DLLoad ("qagame", gdlib.fqpath);

  // now the library is loaded resolve functions
  //glibe = NULL;

  Hdl = gdlib.dynHandle;

  // resolve the functions
  GameAPI = Sys_DLLoadFunction (Hdl, "GetGameAPI", true);

  SV_Printf ("SV_InitGameProgs () : Found GameAPI ()\n");

  GetServerLibAPI (&glibi);

  glibe = GameAPI (1, &glibi);

  SV_InitGameVM (false);
}

/*
====================
SV_GameCommand

See if the current console command is claimed by the game
====================
*/
bool
SV_GameCommand (void)
{
  if (sv.state != SS_GAME)
    {
      return false;
    }

  // calling gamemod console_command handler
  return glibe->console_command ();
}

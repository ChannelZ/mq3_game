/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

// g_public.h -- game module information visible to server

#define	GAME_API_VERSION	8

// entity->svFlags
// the server does not know how to interpret most of the values
// in entityStates (level eType), so the game must explicitly flag
// special server behaviors
/// don't send entity to clients, even if it has effects
#define	SVF_NOCLIENT		0x00000001	

// TTimo
// https://zerowing.idsoftware.com/bugzilla/show_bug.cgi?id=551
#define SVF_CLIENTMASK		0x00000002

/// set if the entity is a bot
#define SVF_BOT			0x00000008
/// send to all connected clients
#define	SVF_BROADCAST		0x00000020
/// merge a second pvs at origin2 into snapshots
#define	SVF_PORTAL		0x00000040
/// entity->r.currentOrigin instead of entity->s.origin
/// for link position (missiles and movers)
#define	SVF_USE_CURRENT_ORIGIN	0x00000080
/// only send to a single client (entityShared_t->singleClient)
#define SVF_SINGLECLIENT	0x00000100
/// don't send CS_SERVERINFO updates to this client
/// so that it can be updated for ping tools without
/// lagging clients
#define SVF_NOSERVERINFO	0x00000200
/// send entity to everyone but one client
/// (entityShared_t->singleClient)
#define SVF_NOTSINGLECLIENT	0x00000800



//===============================================================


struct entityShared_s
{
  /// false if not in any good cluster
  bool	linked;	
  int		linkcount;

  /// SVF_NOCLIENT, SVF_BROADCAST, etc
  int		svFlags;

  /// only send to this client when SVF_SINGLECLIENT is set	
  /// if SVF_CLIENTMASK is set, use bitmask for clients to send to (maxclients must be <= 32, up to the mod to enforce this)
  int		singleClient;		

  /// if false, assume an explicit mins / maxs bounding box
  /// only set by trap_SetBrushModel
  bool	bmodel;

  vec3_t	mins, maxs;
  /// CONTENTS_TRIGGER, CONTENTS_SOLID, CONTENTS_BODY, etc
  /// a non-solid entity should set to 0
  int		contents;

  /// derived from mins/maxs and origin + rotation
  vec3_t	absmin, absmax;

  /// currentOrigin will be used for all collision detection and world linking.
  /// it will not necessarily be the same as the trajectory evaluation for the current
  /// time, because each entity must be moved one at a time after time is advanced
  /// to avoid simultanious collision issues
  vec3_t	currentOrigin;
  vec3_t	currentAngles;

  /// when a trace call is made and passEntityNum != ENTITYNUM_NONE,
  /// an ent will be excluded from testing if:
  /// ent->r.number == passEntityNum	(don't interact with self)
  /// ent->r.ownerNum = passEntityNum	(don't interact with your own missiles)
  /// entity[ent->r.ownerNum].ownerNum = passEntityNum	(don't interact with other missiles from owner)
  int		ownerNum;
};
typedef struct entityShared_s entityShared_t;


/// the server looks at a sharedEntity, which is the start of the game's gentity_t structure
struct sharedEntity_s
{
  /// communicated by server to clients
  entityState_t s;
  /// shared by both the server system and game
  entityShared_t r;
};
typedef struct sharedEntity_s sharedEntity_t;

//===============================================================


struct gamelib_export_s;
struct gamelib_import_s;

struct gamelib_export_s
{
  void	(*init) (int levelTime, int randomSeed, int restart);
  void	(*shutdown) (int restart);
  void	(*run_frame) (int levelTime);
  int	(*game_simulation_time) (void);
  char 	*(*client_connect) (int clientNum, bool firstTime, bool isBot);
  void	(*client_disconnect) (int clientNum);
  void	(*client_think) (int clientNum);
  void	(*client_userinfo_changed) (int clientNum);
  void	(*client_begin) (int clientNum);
  void	(*client_command) (int clientNum);
  bool (*console_command) (void);
  int	(*botai_start_frame) (int time);
};
typedef struct gamelib_export_s gamelib_export_t;

struct gamelib_import_s
{
  void	(*Printf) (const char *fmt, ...);
  void	(*Error) (int code, const char *fmt, ...);
  unsigned int	(*Milliseconds) (void);
  int	(*Argc) (void);
  const char *(*Argv) (int arg);
  void	(*ArgvBuffer) (int n, char *buffer, int bufferLength);
  void	(*Args) (char *buffer, int bufferLength);
  char *(*ArgsFrom) (int n);
  int	(*FS_FOpenFile) (const char *qpath, fileHandle_t *f, fsMode_t mode);
  int	(*FS_Read) (const void *buffer, int len, fileHandle_t f);
  int	(*FS_Write) (const void *buffer, int len, fileHandle_t f);
  void	(*FS_FCloseFile) (fileHandle_t f);
  int	(*FS_GetFileList) (const char *path, const char *extension,
			   char *listbuf, int bufsize);
  int	(*FS_Seek) (fileHandle_t f, long offset, int origin);   // fsOrigin_t
  void	(*SendConsoleCommand) (int exec_when, const char *text);
  cvar_t *(*Cvar_Get) (const char *var_name, const char *value, int flags);
  void	(*Cvar_Set) (const char *var_name, const char *value);
  int	(*Cvar_SetNew) (cvar_t *var, const char *value);
  void	(*Cvar_CheckRange) (cvar_t *var, float min, float max, bool integral);
  void	(*Cvar_TrackChange) (cvar_t *var, void (*func) (cvar_t *var));
  int	(*Cvar_VariableIntegerValue) (const char *var_name);
  float	(*Cvar_VariableValue) (const char *var_name);
  char *(*Cvar_VariableString) (const char *var_name);
  void	(*Cvar_VariableStringBuffer) (const char *var_name, char *buffer,
				      int bufsize);
  void	(*AddCommand) (const char *name, xcommand_t func);
  void	(*RemoveCommand) (const char *cmd_name);

  void	(*LocateGameData) (sharedEntity_t *gEnts, int numGEntities,
			   int sizeofGEntity_t, playerState_t *gameClients,
			   int sizeofGameClient);
  void	(*DropClient) (int clientNum, const char *reason);
  void	(*SendServerCommand) (int clientNum, const char *text);
  void	(*SetConfigstring) (int num, const char *string);
  void	(*GetConfigstring) (int num, char *buffer, int bufferSize);
  void	(*GetUserinfo) (int num, char *buffer, int bufferSize);
  void	(*SetUserinfo) (int num, const char *buffer);
  void	(*GetServerinfo) (char *buffer, int bufferSize);
  void	(*SetBrushModel) (sharedEntity_t *ent, const char *name);
  void	(*Trace) (trace_t *results, const vec3_t start, const vec3_t mins,
		  const vec3_t maxs, const vec3_t end,
		  int passEntityNum, int contentmask/*, int capsule*/);
  int	(*PointContents) (const vec3_t point, int passEntityNum);

  bool (*InPVS) (const vec3_t p1, const vec3_t p2);
  bool (*InPVSIgnorePortals) (const vec3_t p1, const vec3_t p2);
  void	(*AdjustAreaPortalState) (sharedEntity_t *ent, bool open);
  bool (*AreasConnected) (int area1, int area2);
  void	(*LinkEntity) (sharedEntity_t *ent);
  void	(*UnlinkEntity) (sharedEntity_t *ent);
  int	(*EntitiesInBox) (const vec3_t mins, const vec3_t maxs,
			  int *entityList, int maxcount);
  bool (*EntityContact) (const vec3_t mins, const vec3_t maxs,
			     const sharedEntity_t *ent/*, int capsule*/);
  void	(*GetUsercmd) (int clientNum, usercmd_t *cmd);
  bool (*GetEntityToken) (char *buffer, int bufferSize);
  bool (*InitEntityString) (void);

  int	(*DebugPolygonCreate) (int color, int numPoints, vec3_t *points);
  void	(*DebugPolygonDelete) (int id);

  void	(*SnapVector) (float *v);

  int	(*RealTime) (qtime_t *qtime);

  int	(*SV_BotAllocateClient) (void);
  void	(*SV_BotFreeClient) (int clientNum);

  int	(*SV_BotLibSetup) (void);
  int	(*SV_BotLibShutdown) (void);

  int	(*SV_BotGetSnapshotEntity) (int client, int sequence);
  int	(*SV_BotGetConsoleMessage) (int client, char *buf, int size);

  void	(*SV_BotUserCommand) (int client, usercmd_t *cmd);

  botlib_export_t *botlib;
};
typedef struct gamelib_import_s gamelib_import_t;

/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// sv_bot.c

#include "server.h"

typedef struct bot_debugpoly_s
{
  int inuse;
  int color;
  int numPoints;
  vec3_t points[128];
} bot_debugpoly_t;

static bot_debugpoly_t *debugpolygons;
int bot_maxdebugpolys;

/*
==================
SV_BotAllocateClient
==================
*/
int
SV_BotAllocateClient (void)
{
  int i;
  client_t *cl;

  // find a client slot
  for (i = 0, cl = svs.clients; i < sv_maxclients->integer; i++, cl++)
    {
      if (cl->state == CS_FREE)
	{
	  break;
	}
    }

  if (i == sv_maxclients->integer)
    {
      return -1;
    }

  cl->gentity = SV_GentityNum (i);
  cl->gentity->s.number = i;
  cl->state = CS_ACTIVE;
  cl->lastPacketTime = svs.time;
  cl->netchan.remoteAddress.type = NA_BOT;
  cl->rate = 16384;

  return i;
}

/*
==================
SV_BotFreeClient
==================
*/
void
SV_BotFreeClient (int clientNum)
{
  client_t *cl;

  if ((clientNum < 0) || (clientNum >= sv_maxclients->integer))
    {
      SV_Error (ERR_DROP, "SV_BotFreeClient: bad clientNum: %i", clientNum);
    }

  cl = &svs.clients[clientNum];
  cl->state = CS_FREE;
  cl->name[0] = 0;
  if (cl->gentity)
    {
      cl->gentity->r.svFlags &= ~SVF_BOT;
    }
}

/*
==================
BotDrawDebugPolygons
==================
*/
void
BotDrawDebugPolygons (void (*drawPoly)
		      (int color, int numPoints, float *points), int value)
{
  static cvar_t *bot_debug = NULL;
  static cvar_t *bot_groundonly = NULL;
  static cvar_t *bot_reachability = NULL;
  static cvar_t *bot_highlightarea = NULL;
  bot_debugpoly_t *poly;
  int i, parm0;

  if (!debugpolygons)
    return;

  //bot debugging
  if (bot_debug == NULL)
    bot_debug = qce->cvar.Get ("bot_debug", "0", 0);
  //
  if (bot_enable && bot_debug->integer)
    {
      //show reachabilities
      if (bot_reachability == NULL)
	bot_reachability = qce->cvar.Get ("bot_reachability", "0", 0);
      //show ground faces only
      if (bot_groundonly == NULL)
	bot_groundonly = qce->cvar.Get ("bot_groundonly", "1", 0);
      //get the hightlight area
      if (bot_highlightarea == NULL)
	bot_highlightarea = qce->cvar.Get ("bot_highlightarea", "0", 0);
      //
      parm0 = 0;
      if (svs.clients[0].lastUsercmd.buttons & BUTTON_ATTACK)
	parm0 |= 1;
      if (bot_reachability->integer)
	parm0 |= 2;
      if (bot_groundonly->integer)
	parm0 |= 4;
      botlib_export->BotLibVarSet ("bot_highlightarea",
				   bot_highlightarea->string);
      botlib_export->Test (parm0, NULL,
			   svs.clients[0].gentity->r.currentOrigin,
			   svs.clients[0].gentity->r.currentAngles);
    }				//end if
  //draw all debug polys
  for (i = 0; i < bot_maxdebugpolys; i++)
    {
      poly = &debugpolygons[i];
      if (!poly->inuse)
	continue;
      drawPoly (poly->color, poly->numPoints, (float *) poly->points);
      //Com_Printf("poly %i, numpoints = %d\n", i, poly->numPoints);
    }
}

/*
==================
BotImport_Print
==================
*/
static void
BotImport_Print (int type, char *fmt, ...)
{
  char str[2048];
  va_list ap;

  va_start (ap, fmt);
  Q_vsnprintf (str, sizeof (str), fmt, ap);
  va_end (ap);

  switch (type)
    {
    case PRT_MESSAGE:
      {
	SV_Printf ("%s", str);
	break;
      }
    case PRT_WARNING:
      {
	SV_Printf (S_COLOR_YELLOW "Warning: %s", str);
	break;
      }
    case PRT_ERROR:
      {
	SV_Printf (S_COLOR_RED "Error: %s", str);
	break;
      }
    case PRT_FATAL:
      {
	SV_Printf (S_COLOR_RED "Fatal: %s", str);
	break;
      }
    case PRT_EXIT:
      {
	SV_Error (ERR_DROP, S_COLOR_RED "Exit: %s", str);
	break;
      }
    default:
      {
	SV_Printf ("unknown print type\n");
	break;
      }
    }
}

/*
==================
BotImport_Trace
==================
*/
static void
BotImport_Trace (bsp_trace_t * bsptrace, vec3_t start, vec3_t mins,
		 vec3_t maxs, vec3_t end, int passent, int contentmask)
{
  trace_t trace;

  SV_Trace (&trace, start, mins, maxs, end, passent, contentmask);
  //copy the trace information
  bsptrace->allsolid = trace.allsolid;
  bsptrace->startsolid = trace.startsolid;
  bsptrace->fraction = trace.fraction;
  VectorCopy (trace.endpos, bsptrace->endpos);
  bsptrace->plane.dist = trace.plane.dist;
  VectorCopy (trace.plane.normal, bsptrace->plane.normal);
  bsptrace->plane.signbits = trace.plane.signbits;
  bsptrace->plane.type = trace.plane.type;
  bsptrace->surface.value = trace.surfaceFlags;
  bsptrace->ent = trace.entityNum;
  bsptrace->exp_dist = 0;
  bsptrace->sidenum = 0;
  bsptrace->contents = 0;
}

/*
==================
BotImport_EntityTrace
==================
*/
static void
BotImport_EntityTrace (bsp_trace_t * bsptrace, vec3_t start, vec3_t mins,
		       vec3_t maxs, vec3_t end, int entnum, int contentmask)
{
  trace_t trace;

  SV_ClipToEntity (&trace, start, mins, maxs, end, entnum, contentmask);
  //copy the trace information
  bsptrace->allsolid = trace.allsolid;
  bsptrace->startsolid = trace.startsolid;
  bsptrace->fraction = trace.fraction;
  VectorCopy (trace.endpos, bsptrace->endpos);
  bsptrace->plane.dist = trace.plane.dist;
  VectorCopy (trace.plane.normal, bsptrace->plane.normal);
  bsptrace->plane.signbits = trace.plane.signbits;
  bsptrace->plane.type = trace.plane.type;
  bsptrace->surface.value = trace.surfaceFlags;
  bsptrace->ent = trace.entityNum;
  bsptrace->exp_dist = 0;
  bsptrace->sidenum = 0;
  bsptrace->contents = 0;
}

/*
==================
BotImport_PointContents
==================
*/
static int
BotImport_PointContents (vec3_t point)
{
  return SV_PointContents (point, -1);
}

/*
==================
BotImport_inPVS
==================
*/
static int
BotImport_inPVS (vec3_t p1, vec3_t p2)
{
  return SV_inPVS (p1, p2);
}

/*
==================
BotImport_BSPEntityData
==================
*/
static char *
BotImport_BSPEntityData (void)
{
  return qce->cm.EntityString ();
}

/*
==================
BotImport_BSPModelMinsMaxsOrigin
==================
*/
static void
BotImport_BSPModelMinsMaxsOrigin (int modelnum, vec3_t angles, vec3_t outmins,
				  vec3_t outmaxs, vec3_t origin)
{
  clipHandle_t h;
  vec3_t mins, maxs;
  float max;
  int i;

  h = qce->cm.InlineModel (modelnum);
  qce->cm.ModelBounds (h, mins, maxs);
  //if the model is rotated
  if ((angles[0] || angles[1] || angles[2]))
    {
      // expand for rotation

      max = RadiusFromBounds (mins, maxs);
      for (i = 0; i < 3; i++)
	{
	  mins[i] = -max;
	  maxs[i] = max;
	}
    }

  if (outmins)
    VectorCopy (mins, outmins);
  if (outmaxs)
    VectorCopy (maxs, outmaxs);
  if (origin)
    VectorClear (origin);
}

/*
==================
BotImport_GetMemory
==================
*/
static void *
BotImport_GetMemory (int size)
{
  void *ptr;

  ptr = qce->mem.Z_TagMalloc (size, TAG_BOTLIB);
  return ptr;
}

/*
==================
BotImport_FreeMemory
==================
*/
static void
BotImport_FreeMemory (void *ptr)
{
  qce->mem.Z_Free (ptr);
}

/*
=================
BotImport_HunkAlloc
=================
*/
static void *
BotImport_HunkAlloc (int size)
{
  if (qce->mem.Hunk_CheckMark ())
    {
      SV_Error (ERR_DROP,
		 "SV_Bot_HunkAlloc: Alloc with marks already set\n");
    }
  return qce->mem.Hunk_Alloc (size, h_high);
}

/*
==================
BotImport_DebugPolygonCreate
==================
*/
int
BotImport_DebugPolygonCreate (int color, int numPoints, vec3_t * points)
{
  bot_debugpoly_t *poly;
  int i;

  if (!debugpolygons)
    return 0;

  for (i = 1; i < bot_maxdebugpolys; i++)
    {
      if (!debugpolygons[i].inuse)
	break;
    }
  if (i >= bot_maxdebugpolys)
    return 0;

  poly = &debugpolygons[i];
  poly->inuse = true;
  poly->color = color;
  poly->numPoints = numPoints;
  Com_Memcpy (poly->points, points, numPoints * sizeof (vec3_t));
  //
  return i;
}

/*
==================
BotImport_DebugPolygonShow
==================
*/
static void
BotImport_DebugPolygonShow (int id, int color, int numPoints, vec3_t * points)
{
  bot_debugpoly_t *poly;

  if (!debugpolygons)
    return;

  poly = &debugpolygons[id];
  poly->inuse = true;
  poly->color = color;
  poly->numPoints = numPoints;
  Com_Memcpy (poly->points, points, numPoints * sizeof (vec3_t));
}

/*
==================
BotImport_DebugPolygonDelete
==================
*/
void
BotImport_DebugPolygonDelete (int id)
{
  if (!debugpolygons)
    return;
  debugpolygons[id].inuse = false;
}

/*
==================
BotImport_DebugLineCreate
==================
*/
static int
BotImport_DebugLineCreate (void)
{
  vec3_t points[1];
  return BotImport_DebugPolygonCreate (0, 0, points);
}

/*
==================
BotImport_DebugLineDelete
==================
*/
static void
BotImport_DebugLineDelete (int line)
{
  BotImport_DebugPolygonDelete (line);
}

/*
==================
BotImport_DebugLineShow
==================
*/
static void
BotImport_DebugLineShow (int line, vec3_t start, vec3_t end, int color)
{
  vec3_t points[4], dir, cross, up = { 0, 0, 1 };
  float dot;

  VectorCopy (start, points[0]);
  VectorCopy (start, points[1]);
  //points[1][2] -= 2;
  VectorCopy (end, points[2]);
  //points[2][2] -= 2;
  VectorCopy (end, points[3]);

  VectorSubtract (end, start, dir);
  VectorNormalize (dir);
  dot = DotProduct (dir, up);

  if ((dot > 0.99) || (dot < -0.99))
    VectorSet (cross, 1, 0, 0);
  else
    CrossProduct (dir, up, cross);

  VectorNormalize (cross);

  VectorMA (points[0], 2, cross, points[0]);
  VectorMA (points[1], -2, cross, points[1]);
  VectorMA (points[2], -2, cross, points[2]);
  VectorMA (points[3], 2, cross, points[3]);

  BotImport_DebugPolygonShow (line, color, 4, points);
}

/*
==================
SV_BotClientCommand
==================
*/
static void
BotClientCommand (int client, char *command)
{
  SV_ExecuteClientCommand (&svs.clients[client], command, true);
}

/*
==================
SV_BotFrame
==================
*/
void
SV_BotFrame (int time)
{
  if (!bot_enable)
    return;
  //NOTE: maybe the game is already shutdown
  //if (!gvm)
  if ((glibe->botai_start_frame == NULL) || (glibe == NULL))
    return;

  glibe->botai_start_frame (time);
  //VM_Call (gvm, BOTAI_START_FRAME, time);
}

/*
===============
SV_BotLibSetup
===============
*/
int
SV_BotLibSetup (void)
{
  if (!bot_enable)
    {
      return 0;
    }

  if (botlib_export == NULL)
    {
      SV_Printf (S_COLOR_RED
		  "Error: SV_BotLibSetup without SV_BotInitBotLib\n");
      return -1;
    }

  return botlib_export->BotLibSetup ();
}

/*
===============
SV_BotLibShutdown

Called when either the entire server is being killed, or
it is changing to a different game directory.
===============
*/
int
SV_BotLibShutdown (void)
{
  if (botlib_export == NULL)
    {
      return -1;
    }

  return botlib_export->BotLibShutdown ();
}

/*
==================
SV_BotInitCvars
==================
*/
void
SV_BotInitCvars (void)
{
  qce->cvar.Get ("bot_enable", "1", 0);		//enable the bot
  qce->cvar.Get ("bot_developer", "0", CVAR_CHEAT);	//bot developer mode
  qce->cvar.Get ("bot_debug", "0", CVAR_CHEAT);	//enable bot debugging
  qce->cvar.Get ("bot_maxdebugpolys", "2", 0);	//maximum number of debug polys
  qce->cvar.Get ("bot_groundonly", "1", 0);		//only show ground faces of areas
  qce->cvar.Get ("bot_reachability", "0", 0);	//show all reachabilities to other areas
  qce->cvar.Get ("bot_visualizejumppads", "0", CVAR_CHEAT);	//show jumppads
  qce->cvar.Get ("bot_forceclustering", "0", 0);	//force cluster calculations
  qce->cvar.Get ("bot_forcereachability", "0", 0);	//force reachability calculations
  qce->cvar.Get ("bot_forcewrite", "0", 0);		//force writing aas file
  qce->cvar.Get ("bot_aasoptimize", "0", 0);		//no aas file optimisation
  qce->cvar.Get ("bot_saveroutingcache", "0", 0);	//save routing cache
  qce->cvar.Get ("bot_thinktime", "100", CVAR_CHEAT);//msec the bots thinks
  qce->cvar.Get ("bot_reloadcharacters", "0", 0);	//reload the bot characters each time
  qce->cvar.Get ("bot_testichat", "0", 0);		//test ichats
  qce->cvar.Get ("bot_testrchat", "0", 0);		//test rchats
  qce->cvar.Get ("bot_testsolid", "0", CVAR_CHEAT);	//test for solid areas
  qce->cvar.Get ("bot_testclusters", "0", CVAR_CHEAT);	//test the AAS clusters
  qce->cvar.Get ("bot_fastchat", "0", 0);		//fast chatting bots
  qce->cvar.Get ("bot_nochat", "0", 0);		//disable chats
  qce->cvar.Get ("bot_pause", "0", CVAR_CHEAT);	//pause the bots thinking
  qce->cvar.Get ("bot_report", "0", CVAR_CHEAT);	//get a full report in ctf
  qce->cvar.Get ("bot_grapple", "0", 0);		//enable grapple
  qce->cvar.Get ("bot_rocketjump", "1", 0);		//enable rocket jumping
  qce->cvar.Get ("bot_challenge", "0", 0);		//challenging bot
  qce->cvar.Get ("bot_minplayers", "0", 0);		//minimum players in a team or the game
  qce->cvar.Get ("bot_interbreedchar", "", CVAR_CHEAT);	//bot character used for interbreeding
  qce->cvar.Get ("bot_interbreedbots", "10", CVAR_CHEAT);	//number of bots used for interbreeding
  qce->cvar.Get ("bot_interbreedcycle", "20", CVAR_CHEAT);	//bot interbreeding cycle
  qce->cvar.Get ("bot_interbreedwrite", "", CVAR_CHEAT);	//write interbreeded bots to this file
}

/*
==================
SV_BotShutdownBotLib
==================
*/
void
SV_BotShutdownBotLib (void)
{
  // do we have a gamelib handle ?
  if (botlib.dynHandle == NULL)
    {
      return;
    }

  SV_Printf ("SV_BotShutdownBotLib\n");

  if (botlib_export != NULL)
    {
      botlib_export->BotLibShutdown ();
      botlib_export = NULL;
    }

  Sys_DLUnload (botlib.dynHandle); // sets dynHandle to NULL
}

/*
==================
SV_BotInitBotLib
==================
*/
void
SV_BotInitBotLib (void)
{
  void * Hdl;
  botlib_export_t *(*BotLibAPI) (int, botlib_import_t *);
  botlib_import_t botlib_import;

  if (!sv_bot->integer)
    {
      qce->cvar.Set ("bot_enable", "0");
      bot_enable = false;

      return;
    }

  // free the structure
  Com_Memset (&botlib, 0, sizeof (botlib));

  // load the dynamic library
  botlib.dynHandle = Sys_DLLoadType ("libq3botlib", botlib.fqpath, CLIENT);

  // now the library is loaded resolve functions
  //glibe = NULL;

  Hdl = botlib.dynHandle;

  // resolve the functions
  BotLibAPI = Sys_DLLoadFunction (Hdl, "GetBotLibAPI", true);

  SV_Printf ("SV_BotInitBotLib () : Found BotLibAPI ()\n");

  if (debugpolygons)
    qce->mem.Z_Free (debugpolygons);

  bot_maxdebugpolys = qce->cvar.VariableIntegerValue ("bot_maxdebugpolys");
  debugpolygons = qce->mem.Z_Malloc (sizeof (bot_debugpoly_t) * bot_maxdebugpolys);

  botlib_import.Print = BotImport_Print;
  botlib_import.Trace = BotImport_Trace;
  botlib_import.EntityTrace = BotImport_EntityTrace;
  botlib_import.PointContents = BotImport_PointContents;
  botlib_import.inPVS = BotImport_inPVS;
  botlib_import.BSPEntityData = BotImport_BSPEntityData;
  botlib_import.BSPModelMinsMaxsOrigin = BotImport_BSPModelMinsMaxsOrigin;
  botlib_import.BotClientCommand = BotClientCommand;

  //memory management
  botlib_import.GetMemory = BotImport_GetMemory;
  botlib_import.FreeMemory = BotImport_FreeMemory;
  botlib_import.AvailableMemory = qce->mem.Z_AvailableMemory;
  botlib_import.HunkAlloc = BotImport_HunkAlloc;

  // file system access
  botlib_import.FS_FOpenFile = qce->fs.FOpenFileByMode;
  botlib_import.FS_Read = qce->fs.Read2;
  botlib_import.FS_Write = qce->fs.Write;
  botlib_import.FS_FCloseFile = qce->fs.FCloseFile;
  botlib_import.FS_Seek = qce->fs.Seek;

  //debug lines
  botlib_import.DebugLineCreate = BotImport_DebugLineCreate;
  botlib_import.DebugLineDelete = BotImport_DebugLineDelete;
  botlib_import.DebugLineShow = BotImport_DebugLineShow;

  //debug polygons
  botlib_import.DebugPolygonCreate = BotImport_DebugPolygonCreate;
  botlib_import.DebugPolygonDelete = BotImport_DebugPolygonDelete;

  botlib_export =
    (botlib_export_t *) BotLibAPI (BOTLIB_API_VERSION, &botlib_import);
  assert (botlib_export);	// somehow we end up with a zero import.
}

//
//  * * * BOT AI CODE IS BELOW THIS POINT * * *
//

/*
==================
SV_BotGetConsoleMessage
==================
*/
int
SV_BotGetConsoleMessage (int client, char *buf, int size)
{
  client_t *cl;
  int index;

  cl = &svs.clients[client];
  cl->lastPacketTime = svs.time;

  if (cl->reliableAcknowledge == cl->reliableSequence)
    {
      return false;
    }

  cl->reliableAcknowledge++;
  index = cl->reliableAcknowledge & (MAX_RELIABLE_COMMANDS - 1);

  if (!cl->reliableCommands[index][0])
    {
      return false;
    }

  Q_strncpyz (buf, cl->reliableCommands[index], size);
  return true;
}

/*
=================
SV_BotUserCommand
=================
*/
void
SV_BotUserCommand (int client, usercmd_t *cmd)
{
  SV_ClientThink (&svs.clients[client], cmd);
}

#if 0
/*
==================
EntityInPVS
==================
*/
int
EntityInPVS (int client, int entityNum)
{
  client_t *cl;
  clientSnapshot_t *frame;
  int i;
  int index;

  cl = &svs.clients[client];
  frame = &cl->frames[cl->netchan.outgoingSequence & PACKET_MASK];
  for (i = 0; i < frame->num_entities; i++)
    {
      index = (frame->first_entity +i) % svs.numSnapshotEntities;
      if (svs.snapshotEntities[index].number == entityNum)
	{
	  return true;
	}
    }
  return false;
}
#endif

/*
==================
SV_BotGetSnapshotEntity
==================
*/
int
SV_BotGetSnapshotEntity (int client, int sequence)
{
  client_t *cl;
  clientSnapshot_t *frame;
  int index;

  cl = &svs.clients[client];
  frame = &cl->frames[cl->netchan.outgoingSequence & PACKET_MASK];
  if ((sequence < 0) || (sequence >= frame->num_entities))
    {
      return -1;
    }

  index = (frame->first_entity + sequence) % svs.numSnapshotEntities;
  return svs.snapshotEntities[index].number;
}

/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// sv_client.c -- server code for dealing with clients

#include "server.h"

/*
=================
SV_GetChallenge

A "getchallenge" OOB command has been received
Returns a challenge number that can be used
in a subsequent connectResponse command.
We do this to prevent denial of service attacks that
flood the server with invalid connection IPs.  With a
challenge, they must give a valid IP address.

If we are authorizing, a challenge request will cause a packet
to be sent to the authorize server.

When an authorizeip is returned, a challenge response will be
sent to that ip.
=================
*/
void
SV_GetChallenge (netadr_t from)
{
  int i;
  int oldest;
  int oldestTime;
  challenge_t * challenge;

  oldest = 0;
  oldestTime = 0x7fffffff;

  // see if we already have a challenge for this ip
  challenge = &svs.challenges[0];
  for (i = 0; i < MAX_CHALLENGES; i++, challenge++)
    {
      if (!challenge->connected && qce->net.CompareAdr (from, challenge->adr))
	{
	  break;
	}
      if (challenge->time < oldestTime)
	{
	  oldestTime = challenge->time;
	  oldest = i;
	}
    }

  if (i == MAX_CHALLENGES)
    {
      // this is the first time this client has asked for a challenge
      challenge = &svs.challenges[oldest];
      challenge->adr = from;
      challenge->firstTime = svs.time;
      challenge->time = svs.time;
      challenge->connected = false;
    }

  // always generate a new challenge number, so the client cannot circumvent sv_maxping
  challenge->challenge = ((rand() << 16) ^ rand()) ^ svs.time;
  challenge->wasrefused = false;

  challenge->pingTime = svs.time;
  qce->net.OutOfBandPrint (NS_SERVER, from, "challengeResponse %i", challenge->challenge);
}

/*
==================
SV_DirectConnect

A "connect" OOB command has been received
==================
*/
void
SV_DirectConnect (netadr_t from)
{
  int i;
  int count;
  int startIndex;
  int version;
  int qport;
  int challenge;
  int clientNum;
  client_t * cl, * newcl;
  char *userinfo;
  char *password;
  char *ip;
  char *denied;
  sharedEntity_t *ent;
  bool firstTime = true;

  SV_DPrintf ("SVC_DirectConnect ()\n");

  userinfo = qce->cmd.Argv (1);

  version = atoi (Info_ValueForKey (userinfo, "protocol"));
  if (version != PROTOCOL_VERSION)
    {
      qce->net.OutOfBandPrint (NS_SERVER, from, "print\nServer uses protocol version %i.\n", PROTOCOL_VERSION);
      SV_DPrintf ("    rejected connect from version %i\n", version);
      return;
    }

  challenge = atoi (Info_ValueForKey (userinfo, "challenge"));
  qport = atoi (Info_ValueForKey (userinfo, "qport"));

  // quick reject
  for (i = 0, cl = svs.clients; i < sv_maxclients->integer; i++,cl++)
    {
      if (cl->state == CS_FREE)
	{
	  continue;
	}
      if (qce->net.CompareBaseAdrMask (from, cl->netchan.remoteAddress, -1)
	  && ((cl->netchan.qport == qport) || (from.port == cl->netchan.remoteAddress.port)))
	{
	  if ((svs.time - cl->lastConnectTime) < (sv_reconnectlimit->integer * 1000))
	    {
	      SV_DPrintf ("%s:reconnect rejected : too soon\n", qce->net.AdrToString (from));
	      return;
	    }
	  break;
	}
    }

  // don't let "ip" overflow userinfo string
  if (qce->net.IsLocalAddress (from))
    ip = "localhost";
  else
    ip = (char *)qce->net.AdrToString (from);
  // userinfo will be used later in this function for copy
  if ((strlen(ip) + strlen(userinfo) + 4) >= MAX_INFO_STRING)
    {
      qce->net.OutOfBandPrint (NS_SERVER, from,
			       "print\nUserinfo string length exceeded.  "
			       "Try removing setu cvars from your config.\n");
      return;
    }
  Info_SetValueForKey (userinfo, "ip", ip);

  // see if the challenge is valid (local clients don't need to challenge)
  if (!qce->net.IsLocalAddress (from))
    {
      int ping;
      challenge_t * challengeptr;

      for (i = 0; i < MAX_CHALLENGES; i++)
	{
	  if (qce->net.CompareAdr (from, svs.challenges[i].adr))
	    {
	      if (challenge == svs.challenges[i].challenge)
		break;
	    }
	}
      if (i == MAX_CHALLENGES)
	{
	  qce->net.OutOfBandPrint (NS_SERVER, from, "print\nNo or bad challenge for your address.\n");
	  return;
	}

      challengeptr = &svs.challenges[i];
      if (challengeptr->wasrefused)
	{
	  // Return silently, so that error messages written by the server keep being displayed.
	  return;
	}

      ping = svs.time - challengeptr->pingTime;

      // never reject a LAN client based on ping
      if (!qce->net.IsLANAddress (from))
	{
	  if (sv_minPing->value && ping < sv_minPing->value)
	    {
	      qce->net.OutOfBandPrint (NS_SERVER, from, "print\nServer is for high pings only\n");
	      SV_DPrintf ("Client %i rejected on a too low ping\n", i);
	      challengeptr->wasrefused = true;
	      return;
	    }
	  if (sv_maxPing->value && ping > sv_maxPing->value)
	    {
	      qce->net.OutOfBandPrint (NS_SERVER, from, "print\nServer is for low pings only\n");
	      SV_DPrintf ("Client %i rejected on a too high ping\n", i);
	      challengeptr->wasrefused = true;
	      return;
	    }
	}
      SV_Printf ("Client %i connecting with %i challenge ping\n", i, ping);
      challengeptr->connected = true;
    }

  // if there is already a slot for this ip, reuse it
  for (i = 0, cl = svs.clients; i < sv_maxclients->integer; i++, cl++)
    {
      if (cl->state == CS_FREE)
	{
	  continue;
	}
      if (qce->net.CompareBaseAdrMask (from, cl->netchan.remoteAddress,-1)
	  && ((cl->netchan.qport == qport) || (from.port == cl->netchan.remoteAddress.port)))
	{
	  SV_Printf ("%s:reconnect\n", qce->net.AdrToString (from));
	  newcl = cl;
	  firstTime = false;

	  goto gotnewcl;
	}
    }

  // find a client slot
  // if "sv_privateClients" is set > 0, then that number
  // of client slots will be reserved for connections that
  // have "password" set to the value of "sv_privatePassword"
  // Info requests will report the maxclients as if the private
  // slots didn't exist, to prevent people from trying to connect
  // to a full server.
  // This is to allow us to reserve a couple slots here on our
  // servers so we can play without having to kick people.

  // check for privateClient password
  password = Info_ValueForKey (userinfo, "password");
  if (!strcmp(password, sv_privatePassword->string))
    {
      startIndex = 0;
    }
  else
    {
      // skip past the reserved slots
      startIndex = sv_privateClients->integer;
    }

  newcl = NULL;
  for (i = startIndex; i < sv_maxclients->integer; i++)
    {
      cl = &svs.clients[i];
      if (cl->state == CS_FREE)
	{
	  newcl = cl;
	  break;
	}
    }

  if (newcl == NULL)
    {
      if (qce->net.IsLocalAddress (from))
	{
	  count = 0;
	  for (i = startIndex; i < sv_maxclients->integer ; i++)
	    {
	      cl = &svs.clients[i];
	      if (cl->netchan.remoteAddress.type == NA_BOT)
		{
		  count++;
		}
	    }
	  // if they're all bots
	  if (count >= (sv_maxclients->integer - startIndex))
	    {
	      SV_DropClient (&svs.clients[sv_maxclients->integer - 1], "only bots on server");
	      newcl = &svs.clients[sv_maxclients->integer - 1];
	    }
	  else
	    {
	      //SV_DropClient (&svs.clients[sv_maxclients->integer - 1], "local got priority");
	      //newcl = &svs.clients[sv_maxclients->integer - 1];
	      SV_Error (ERR_FATAL, "server is full on local connect\n");
	      return;
	    }
	}
      else
	{
	  qce->net.OutOfBandPrint (NS_SERVER, from, "print\nServer is full.\n");
	  SV_DPrintf ("Rejected a connection.\n");
	  return;
	}
    }

 gotnewcl:
  // build a new connection
  // accept the new client
  // this is the only place a client_t is ever initialized

  // clean new client structure (even for a reconnect)
  Com_Memset (newcl, 0, sizeof(*newcl));

  clientNum = newcl - svs.clients;
  ent = SV_GentityNum (clientNum);
  newcl->gentity = ent;

  // save the challenge
  newcl->challenge = challenge;

  // save the address
  qce->net.Netchan_Setup (NS_SERVER, &newcl->netchan , from, qport);
  // init the netchan queue
  newcl->netchan_end_queue = &newcl->netchan_start_queue;

  // save the userinfo
  Q_strncpyz (newcl->userinfo, userinfo, sizeof(newcl->userinfo));

  // get the game a chance to reject this connection or modify the userinfo
  denied = glibe->client_connect (clientNum, firstTime, false);
  if (denied)
    {
      qce->net.OutOfBandPrint (NS_SERVER, from, "print\n%s\n", denied);
      SV_DPrintf ("Game rejected a connection: %s.\n", denied);
      return;
    }

  SV_UserinfoChanged (newcl);

  // send the connect packet to the client
  qce->net.OutOfBandPrint (NS_SERVER, from, "connectResponse");

  SV_DPrintf ("Going from CS_FREE to CS_CONNECTED for %s\n", newcl->name);

  newcl->state = CS_CONNECTED;
  newcl->nextSnapshotTime = svs.time;
  newcl->lastPacketTime = svs.time;
  newcl->lastConnectTime = svs.time;

  // when we receive the first packet from the client, we will
  // notice that it is from a different serverid and that the
  // gamestate message was not just sent, forcing a retransmit
  newcl->gamestateMessageNum = -1;

  // if this was the first client on the server, or the last client
  // the server can hold, send a heartbeat to the master.
  count = 0;
  for (i = 0, cl=svs.clients; i < sv_maxclients->integer; i++, cl++)
    {
      if (svs.clients[i].state >= CS_CONNECTED)
	{
	  count++;
	}
    }
  if (count == 1 || count == sv_maxclients->integer)
    {
      SV_Heartbeat_f ();
    }
}

/*
=====================
SV_DropClient

Called when the player is totally leaving the server, either willingly
or unwillingly.  This is NOT called if the entire server is quiting
or crashing -- SV_FinalMessage() will handle that
=====================
*/
void
SV_DropClient (client_t *drop, const char *reason)
{
  int i;
  challenge_t * challenge;
  const bool isBot = (drop->netchan.remoteAddress.type == NA_BOT);

  if (drop->state == CS_ZOMBIE)
    {
      return;		// already dropped
    }

  if (!isBot)
    {
      // see if we already have a challenge for this ip
      challenge = &svs.challenges[0];

      for (i = 0; i < MAX_CHALLENGES; i++, challenge++)
	{
	  if (qce->net.CompareAdr (drop->netchan.remoteAddress, challenge->adr))
	    {
	      Com_Memset (challenge, 0, sizeof(*challenge));
	      challenge->connected = false;
	      break;
	    }
	}
    }

  // tell everyone why they got dropped
  SV_SendServerCommand (NULL, "print \"%s" S_COLOR_WHITE " %s\n\"", drop->name, reason);

  // call the prog function for removing a client
  // this will remove the body, among other things
  glibe->client_disconnect (drop - svs.clients);

  // add the disconnect command
  SV_SendServerCommand (drop, "disconnect \"%s\"", reason);

  if (isBot)
    {
      SV_BotFreeClient (drop - svs.clients);
    }

  // nuke user info
  SV_SetUserinfo (drop - svs.clients, "");

  if (isBot)
    {
      // bots shouldn't go zombie, as there's no real net connection.
      drop->state = CS_FREE;
    }
  else
    {
      SV_DPrintf ("Going to CS_ZOMBIE for client %i\n", drop - svs.clients);
      drop->state = CS_ZOMBIE;		// become free in a few seconds
    }

  // if this was the last client on the server, send a heartbeat
  // to the master so it is known the server is empty
  // send a heartbeat now so the master will get up to date info
  // if there is already a slot for this ip, reuse it
  for (i = 0; i < sv_maxclients->integer; i++)
    {
      if (svs.clients[i].state >= CS_CONNECTED)
	{
	  break;
	}
    }
  if (i == sv_maxclients->integer)
    {
      SV_Heartbeat_f ();
    }
}

/*
================
SV_SendClientGameState

Sends the first message from the server to a connected client.
This will be sent on the initial connection and upon each new map load.

It will be resent if the client acknowledges a later message but has
the wrong gamestate.
================
*/
static void
SV_SendClientGameState (client_t *client)
{
  int start;
  msg_t msg;
  byte msgBuffer[MAX_MSGLEN];
  entityState_t nullstate;
  entityState_t * base;

  SV_DPrintf ("SV_SendClientGameState() for %s\n", client->name);
  SV_DPrintf ("Going from CS_CONNECTED to CS_PRIMED for %s\n", client->name);
  client->state = CS_PRIMED;

  // when we receive the first packet from the client, we will
  // notice that it is from a different serverid and that the
  // gamestate message was not just sent, forcing a retransmit
  client->gamestateMessageNum = client->netchan.outgoingSequence;

  qce->msg.Init (&msg, msgBuffer, sizeof(msgBuffer));

  // NOTE, MRE: all server->client messages now acknowledge
  // let the client know which reliable clientCommands we have received
  qce->msg.WriteLong (&msg, client->lastClientCommand);

  // send any server commands waiting to be sent first.
  // we have to do this cause we send the client->reliableSequence
  // with a gamestate and it sets the clc.serverCommandSequence at
  // the client side
  SV_UpdateServerCommandsToClient (client, &msg);

  // send the gamestate
  qce->msg.WriteByte (&msg, svc_gamestate);
  qce->msg.WriteLong (&msg, client->reliableSequence);

  // write the configstrings
  for (start = 0; start < MAX_CONFIGSTRINGS; start++)
    {
      if (sv.configstrings[start][0])
	{
	  qce->msg.WriteByte (&msg, svc_configstring);
	  qce->msg.WriteShort (&msg, start);
	  qce->msg.WriteBigString (&msg, sv.configstrings[start]);
	}
    }

  // write the baselines
  Com_Memset (&nullstate, 0, sizeof(nullstate));
  for (start = 0; start < MAX_GENTITIES; start++)
    {
      base = &sv.svEntities[start].baseline;
      if (!base->number)
	{
	  continue;
	}
      qce->msg.WriteByte (&msg, svc_baseline);
      qce->msg.WriteDeltaEntity (&msg, &nullstate, base, true);
    }

  qce->msg.WriteByte (&msg, svc_EOF);

  qce->msg.WriteLong (&msg, client - svs.clients);

  // write the checksum feed
  qce->msg.WriteLong (&msg, sv.checksumFeed);

  // deliver this to the client
  SV_SendMessageToClient (&msg, client);
}

/*
==================
SV_ClientEnterWorld
==================
*/
void
SV_ClientEnterWorld (client_t *client, usercmd_t *cmd)
{
  int clientNum;
  sharedEntity_t * ent;

  SV_DPrintf ("Going from CS_PRIMED to CS_ACTIVE for %s\n", client->name);
  client->state = CS_ACTIVE;

  // resend all configstrings using the cs commands since these are
  // no longer sent when the client is CS_PRIMED
  SV_UpdateConfigstrings (client);

  // set up the entity for the client
  clientNum = client - svs.clients;
  ent = SV_GentityNum (clientNum);
  ent->s.number = clientNum;
  client->gentity = ent;

  client->deltaMessage = -1;
  client->nextSnapshotTime = svs.time;	// generate a snapshot immediately

  if (cmd)
    client->lastUsercmd = *cmd;
  else
    Com_Memset (&client->lastUsercmd, '\0', sizeof (client->lastUsercmd));

  // call the game begin function
  glibe->client_begin (client - svs.clients);
}

/*
============================================================

CLIENT COMMAND EXECUTION

============================================================
*/
/*
=================
SV_Disconnect_f

The client is going to disconnect, so remove the connection immediately  FIXME: move to game?
=================
*/
static void
SV_Disconnect_f (client_t *cl)
{
  SV_DropClient (cl, "disconnected");
}

/*
=================
SV_UserinfoChanged

Pull specific info from a newly changed userinfo string
into a more C friendly form.
Do not give game mod personnal/system informations about the clients
this has to be handled by the server
=================
*/
void
SV_UserinfoChanged (client_t *cl)
{
  int i;
  char *val;

  // name for C code
  Q_strncpyz (cl->name, Info_ValueForKey (cl->userinfo, "name"), sizeof(cl->name));
  // rate command

  // if the client is on the same subnet as the server and we aren't running an
  // internet public server, assume they don't need a rate choke
  if (qce->net.IsLANAddress(cl->netchan.remoteAddress)
      && sv_dedicated->integer != 2 && sv_lanForceRate->integer == 1)
    {
      cl->rate = 99999;	// lans should not rate limit
    }
  else
    {
      val = Info_ValueForKey (cl->userinfo, "rate");
      if (strlen(val))
	{
	  i = atoi(val);
	  cl->rate = i;
	  if (cl->rate < 1000)
	    {
	      cl->rate = 1000;
	    }
	  else if (cl->rate > 90000)
	    {
	      cl->rate = 90000;
	    }
	}
      else
	{
	  cl->rate = 25000; // 3000 for 56k
	}
    }
  // this has nothing to do here...
  val = Info_ValueForKey (cl->userinfo, "handicap");
  if (strlen(val))
    {
      i = atoi(val);
      if (i <= 0 || i > 100 || strlen(val) > 4)
	{
	  Info_SetValueForKey (cl->userinfo, "handicap", "100");
	}
    }

  // snaps command
  val = Info_ValueForKey (cl->userinfo, "snaps");
  if (strlen(val))
    {
      i = atoi(val);
      if (i < 1)
	{
	  i = 1;
	}
      else if (i > sv_fps->integer)
	{
	  i = sv_fps->integer;
	}
      cl->snapshotMsec = 1000/i;
    }
  else
    {
      cl->snapshotMsec = 50;
    }
}

/*
==================
SV_UpdateUserinfo_f
==================
*/
static void
SV_UpdateUserinfo_f (client_t *cl)
{
  Q_strncpyz (cl->userinfo, qce->cmd.Argv(1), sizeof(cl->userinfo));

  SV_UserinfoChanged (cl);
  // call prog code to allow overrides
  glibe->client_userinfo_changed (cl - svs.clients);
}

/*
================
SV_ListPlayers_f
================
*/
void
SV_ListPlayers_f (client_t * client)
{
  int i;
  char buffer[64];
  char string[MAX_STRING_CHARS];
  client_t * cl;
  char *msg;

  // keep space for the ending \"
  Q_strncpyz (string, "print \"", sizeof (string)-2);
  Q_strcat (string, sizeof (string)-2, "id |     name      | ping |  rate  | snaps\n");
  Q_strcat (string, sizeof (string)-2, "------------------------------------------\n");

  for (i = 0, cl = svs.clients; i < sv_maxclients->integer; i++, cl++)
    {
      if (!cl->state)
	continue;

      Q_strncpyz (buffer, cl->name, sizeof(buffer));
      Q_CleanStr (buffer);

      msg = va ("%2d | %-13.13s^7 |  %3d |  %5d |  %3d\n",
		i, buffer, cl->ping,
		cl->rate, cl->snapshotMsec ? 1000/cl->snapshotMsec : 0);

      // we have the players
      Q_strcat (string, sizeof (string)-2, msg);
    }

  // end the string and send it
  Q_strcat (string, sizeof (string), "\"");

  SV_SendServerCommand (client, string);
}

/*
===============
SV_ListMaps_f
===============
*/
// TODO : this function need some rework
//	  cleanings and what if the server gets the command
//	  when qcommon (the fs) is not yet ready ?
void
SV_ListMaps_f (client_t *cl)
{
  char string[MAX_STRING_CHARS];
  size_t len;

  Com_Memset (string, 0, sizeof (string));

  if ((qce->cmd.Argc () != 1) && (qce->cmd.Argc () != 2))
    {
      Q_strcat (string, sizeof (string), "print\"Usage : /list_maps <firstmapname characters>\"");
      SV_SendServerCommand (cl, string);
      return;
    }

  Q_strcat (string, sizeof (string), "print \"");
  Q_strcat (string, sizeof (string), "* * * Map List * * *\n");

  SV_DPrintf ("sv_listmaps () argv %s : len : %d\n",
	      qce->cmd.Argv(1), strlen (qce->cmd.Argv(1)));

  len = strlen (string);

  if (qce->cmd.Argc () == 1)
    // keep space for the ending "\n\""\0
    qce->fs.FilenameListByExt ("maps", "bsp", NULL, string + len, sizeof (string) - len - 4);
  else
    {
      qce->fs.FilenameListByExt ("maps", "bsp", qce->cmd.Argv(1),
				 string + len, sizeof (string) - len - 4);
    }

  Q_strcat (string, sizeof (string), "\n\"");

  SV_SendServerCommand (cl, string);
}

typedef struct ucmd_s
{
  char * name;
  void (*func)(client_t *cl);
} ucmd_t;

static ucmd_t ucmds[] =
  {
    {"disconnect", SV_Disconnect_f},
    {"userinfo", SV_UpdateUserinfo_f},
    {"players", SV_ListPlayers_f},
    {"list_maps", SV_ListMaps_f},

    {NULL, NULL}
};

/*
==================
SV_ExecuteClientCommand

Also called by bot code
==================
*/
void
SV_ExecuteClientCommand (client_t *cl, const char *s, bool clientOK)
{
  bool bProcessed = false;
  ucmd_t *u;

  qce->cmd.TokenizeString (s);

  // if we have a spammer block everything except disconnect or we'll have errors
  if (clientOK || !strcmp (qce->cmd.Argv(0), "disconnect"))
    {
      // see if it is a server level command
      for (u = ucmds; u->name; u++)
	{
	  if (!strcmp (qce->cmd.Argv(0), u->name))
	    {
	      u->func (cl);
	      bProcessed = true;
	      break;
	    }
	}

      // pass unknown strings to the game
      if (!bProcessed && (sv.state == SS_GAME)
	  && ((cl->state == CS_ACTIVE) || (cl->state == CS_PRIMED)))
	{
	  qce->cmd.Args_Sanitize ();
	  glibe->client_command (cl - svs.clients);
	  bProcessed = true;
	}
    }

  if (!bProcessed)
    SV_DPrintf ("client text ignored for %s: %s\n", cl->name, qce->cmd.Argv(0));
}

/*
===============
SV_ClientCommand
===============
*/
static bool
SV_ClientCommand (client_t *cl, msg_t *msg)
{
  int seq;
  bool clientOk = true;
  const char * s;

  seq = qce->msg.ReadLong (msg);
  s = qce->msg.ReadString (msg);

  // see if we have already executed it
  if (cl->lastClientCommand >= seq)
    {
      return true;
    }

  SV_DPrintf ("clientCommand: %s : %i : %s\n", cl->name, seq, s);

  // drop the connection if we have somehow lost commands
  if (seq > cl->lastClientCommand + 1)
    {
      SV_Printf ("Client %s(%i) lost %i clientCommands\n", cl->name,
		 cl - svs.clients, seq - cl->lastClientCommand + 1);
      SV_DropClient (cl, "Lost reliable commands");
      return false;
    }

  // malicious users may try using too many string commands
  // to lag other players.  If we decide that we want to stall
  // the command, we will stop processing the rest of the packet,
  // including the usercmd.  This causes flooders to lag themselves
  // but not other people
  // We don't do this when the client hasn't been active yet since its
  // normal to spam a lot of commands when downloading
  if (cl->state >= CS_ACTIVE &&
      sv_floodProtect->integer &&
      svs.time < cl->nextReliableTime)
    {
      // ignore any other text messages from this client but let them keep playing
      // TTimo - moved the ignored verbose to the actual processing in SV_ExecuteClientCommand, only printing if the core doesn't intercept
      clientOk = false;
    }

  // don't allow another command for one second
  cl->nextReliableTime = svs.time + 500; // 500ms is fine

  SV_ExecuteClientCommand (cl, s, clientOk);

  cl->lastClientCommand = seq;
  Com_sprintf (cl->lastClientCommandString, sizeof(cl->lastClientCommandString), "%s", s);

  return true;		// continue procesing
}

//==================================================================================
void
SV_ClientThink2 (int clientNum, usercmd_t *cmd)
{
  // check clientNum validity ?
  SV_ClientThink (&svs.clients[clientNum], cmd);
}

/*
==================
SV_ClientThink

Also called by bot code
==================
*/
void
SV_ClientThink (client_t *cl, usercmd_t *cmd)
{
  cl->lastUsercmd = *cmd;

  if (cl->state != CS_ACTIVE)
    {
      return;		// may have been kicked during the last usercmd
    }

  glibe->client_think (cl - svs.clients);
}

/*
==================
SV_UserMove

The message usually contains all the movement commands
that were in the last three packets, so that the information
in dropped packets can be recovered.

On very fast clients, there may be multiple usercmd packed into
each of the backup packets.
==================
*/
static void
SV_UserMove (client_t *cl, msg_t *msg, bool delta)
{
  int i;
  int cmdCount;
  usercmd_t nullcmd;
  usercmd_t cmds[MAX_PACKET_USERCMDS];
  usercmd_t *cmd, *oldcmd;

  if (delta)
    {
      cl->deltaMessage = cl->messageAcknowledge;
    }
  else
    {
      cl->deltaMessage = -1;
    }

  cmdCount = qce->msg.ReadByte (msg);

  if (cmdCount < 1)
    {
      SV_Printf ("cmdCount < 1\n");
      return;
    }

  if (cmdCount > MAX_PACKET_USERCMDS)
    {
      SV_Printf ("cmdCount > MAX_PACKET_USERCMDS\n");
      return;
    }
  /*
  // use the checksum feed in the key
  key = sv.checksumFeed;
  // also use the message acknowledge
  key ^= cl->messageAcknowledge;
  // also use the last acknowledged server command in the key
  key ^= qce->Com_HashKey (cl->reliableCommands[ cl->reliableAcknowledge & (MAX_RELIABLE_COMMANDS-1) ], 32);
  */

  Com_Memset (&nullcmd, 0, sizeof(nullcmd));
  oldcmd = &nullcmd;
  for (i = 0; i < cmdCount; i++)
    {
      cmd = &cmds[i];
      qce->msg.ReadDeltaUsercmd (msg, oldcmd, cmd);
      oldcmd = cmd;
    }

  // save time for ping calculation
  cl->frames [cl->messageAcknowledge & PACKET_MASK].messageAcked = svs.time;

  // if this is the first usercmd we have received
  // this gamestate, put the client into the world
  if (cl->state == CS_PRIMED)
    {
      SV_ClientEnterWorld (cl, &cmds[0]);
      // the moves can be processed normaly
    }

  if (cl->state != CS_ACTIVE)
    {
      cl->deltaMessage = -1;
      return;
    }

  // usually, the first couple commands will be duplicates
  // of ones we have previously received, but the servertimes
  // in the commands will cause them to be immediately discarded
  for (i =  0; i < cmdCount; i++)
    {
      // if this is a cmd from before a map_restart ignore it
      if (cmds[i].serverTime > cmds[cmdCount-1].serverTime)
	{
	  continue;
	}
      // extremely lagged or cmd from before a map_restart
      //if ( cmds[i].serverTime > svs.time + 3000 ) {
      //	continue;
      //}
      // don't execute if this is an old cmd which is already executed
      // these old cmds are included when cl_packetdup > 0
      if (cmds[i].serverTime <= cl->lastUsercmd.serverTime)
	{
	  continue;
	}
      SV_ClientThink (cl, &cmds[i]);
    }
}

/*
===========================================================================

USER CMD EXECUTION

===========================================================================
*/
/*
===================
SV_ExecuteClientMessage

Parse a client packet
===================
*/
void
SV_ExecuteClientMessage (client_t *cl, msg_t *msg)
{
  int c;
  int serverId;

  qce->msg.Bitstream (msg);

  serverId = qce->msg.ReadLong (msg);
  cl->messageAcknowledge = qce->msg.ReadLong (msg);

  if (cl->messageAcknowledge < 0)
    {
      // usually only hackers create messages like this
      // it is more annoying for them to let them hanging
#ifndef NDEBUG
      SV_DropClient (cl, "DEBUG: illegible client message");
#endif
      return;
    }

  cl->reliableAcknowledge = qce->msg.ReadLong (msg);

  // NOTE: when the client message is fux0red the acknowledgement numbers
  // can be out of range, this could cause the server to send thousands of server
  // commands which the server thinks are not yet acknowledged in SV_UpdateServerCommandsToClient
  if (cl->reliableAcknowledge < cl->reliableSequence - MAX_RELIABLE_COMMANDS)
    {
      // usually only hackers create messages like this
      // it is more annoying for them to let them hanging
#ifndef NDEBUG
      SV_DropClient (cl, "DEBUG: illegible client message");
#endif
      cl->reliableAcknowledge = cl->reliableSequence;
      return;
    }
  // if this is a usercmd from a previous gamestate,
  // ignore it or retransmit the current gamestate
  if (serverId != sv.serverId)
    {
      if (serverId >= sv.restartedServerId && serverId < sv.serverId)
	{ // TTimo - use a comparison here to catch multiple map_restart
	  // they just haven't caught the map_restart yet
	  SV_DPrintf ("%s : ignoring pre map_restart / outdated client message\n", cl->name);
	  return;
	}
      // if we can tell that the client has dropped the last
      // gamestate we sent them, resend it
      if (cl->messageAcknowledge > cl->gamestateMessageNum)
	{
	  SV_DPrintf ("%s : dropped gamestate, resending\n", cl->name);
	  SV_SendClientGameState (cl);
	}
      return;
    }

  // read optional clientCommand strings
  do
    {
      c = qce->msg.ReadByte (msg);

      if (c == clc_EOF)
	{
	  break;
	}

      if (c != clc_clientCommand)
	{
	  break;
	}

      if (!SV_ClientCommand (cl, msg))
	{
	  return;	// we couldn't execute it because of the flood protection
	}

      if (cl->state == CS_ZOMBIE)
	{
	  return;	// disconnect command
	}
    }
  while (1);

  // read the usercmd_t
  if (c == clc_move)
    {
      SV_UserMove (cl, msg, true);
    }
  else if (c == clc_moveNoDelta)
    {
      SV_UserMove (cl, msg, false);
    }
  else if (c != clc_EOF)
    {
      SV_Printf ("WARNING: bad command byte for client %i\n", (int) (cl - svs.clients));
    }
  //	if ( msg->readcount != msg->cursize ) {
  //		Com_Printf( "WARNING: Junk at end of packet for client %i\n", cl - svs.clients );
  //	}
}

/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "server.h"

extern bool sv_errorEntered;

/*
===============
SV_SendConfigstring

Creates and sends the server command necessary to update the CS index for the
given client
===============
*/
static void
SV_SendConfigstring (client_t *client, int index)
{
  int maxChunkSize = MAX_STRING_CHARS - 24;
  int len;

  len = strlen (sv.configstrings[index]);

  if (len >= maxChunkSize)
    {
      int sent = 0;
      int remaining = len;
      char buf[MAX_STRING_CHARS];
      char * cmd;

      while (remaining > 0)
	{
	  if (sent == 0)
	    {
	      cmd = "bcs0";
	    }
	  else if (remaining < maxChunkSize)
	    {
	      cmd = "bcs2";
	    }
	  else
	    {
	      cmd = "bcs1";
	    }
	  Q_strncpyz (buf, &sv.configstrings[index][sent],
		      maxChunkSize);

	  SV_SendServerCommand (client, "%s %i \"%s\"\n", cmd,
				index, buf);

	  sent += (maxChunkSize - 1);
	  remaining -= (maxChunkSize - 1);
	}
    }
  else
    {
      // standard cs, just send it
      SV_SendServerCommand (client, "cs %i \"%s\"\n", index,
			    sv.configstrings[index]);
    }
}

/*
===============
SV_UpdateConfigstrings

Called when a client goes from CS_PRIMED to CS_ACTIVE.  Updates all
Configstring indexes that have changed while the client was in CS_PRIMED
===============
*/
void
SV_UpdateConfigstrings (client_t *client)
{
  int index;

  for (index = 0; index <= MAX_CONFIGSTRINGS; index++)
    {
      // if the CS hasn't changed since we went to CS_PRIMED, ignore
      if (!client->csUpdated[index])
	continue;

      // do not always send server info to all clients
      if (index == CS_SERVERINFO && client->gentity &&
	  (client->gentity->r.svFlags & SVF_NOSERVERINFO))
	{
	  continue;
	}
      SV_SendConfigstring (client, index);
      client->csUpdated[index] = false;
    }
}

/*
===============
SV_SetConfigstring

===============
*/
void
SV_SetConfigstring (int index, const char *val)
{
  int i;
  client_t * client;

  if (index < 0 || index >= MAX_CONFIGSTRINGS)
    {
      SV_Error (ERR_DROP, "SV_SetConfigstring: bad index %i\n", index);
    }

  if (val == NULL)
    {
      val = "";
    }

  // don't bother broadcasting an update if no change
  if (!Q_strcmp (val, sv.configstrings[index]))
    {
      return;
    }

  // change the string in sv
  qce->mem.Z_Free (sv.configstrings[index]);
  sv.configstrings[index] = qce->mem.CopyString (val);

  // send it to all the clients if we aren't
  // spawning a new server
  if (sv.state == SS_GAME || sv.restarting)
    {
      // send the data to all relevent clients
      for (i = 0, client = svs.clients; i < sv_maxclients->integer; i++, client++)
	{
	  if (client->state < CS_ACTIVE)
	    {
	      if (client->state == CS_PRIMED)
		client->csUpdated[index] = true;
	      continue;
	    }
	  // do not always send server info to all clients
	  if (index == CS_SERVERINFO && client->gentity
	      && (client->gentity->r.svFlags & SVF_NOSERVERINFO))
	    {
	      continue;
	    }
	  SV_SendConfigstring (client, index);
	}
    }
}

/*
===============
SV_GetConfigstring

===============
*/
void
SV_GetConfigstring (int index, char *buffer, int bufferSize)
{
  if (bufferSize < 1)
    {
      SV_Error (ERR_DROP, "SV_GetConfigstring: bufferSize == %i", bufferSize);
    }
  if (index < 0 || index >= MAX_CONFIGSTRINGS)
    {
      SV_Error (ERR_DROP, "SV_GetConfigstring: bad index %i\n", index);
    }
  if (!sv.configstrings[index])
    {
      buffer[0] = 0;
      return;
    }

  Q_strncpyz (buffer, sv.configstrings[index], bufferSize);
}

/*
===============
SV_SetUserinfo

===============
*/
void
SV_SetUserinfo (int index, const char *val)
{
  const char * name;

  if (index < 0 || index >= sv_maxclients->integer)
    {
      SV_Error (ERR_DROP, "SV_SetUserinfo: bad index %i\n", index);
    }

  if (val == NULL)
    {
      val = "";
    }

  Q_strncpyz (svs.clients[index].userinfo, val, sizeof(svs.clients[index].userinfo));
  name = Info_ValueForKey (val, "name");
  Q_strncpyz (svs.clients[index].name, name, sizeof(svs.clients[index].name));
}

/*
===============
SV_GetUserinfo

===============
*/
void
SV_GetUserinfo (int index, char *buffer, int bufferSize)
{
  if (bufferSize < 1)
    {
      SV_Error (ERR_DROP, "SV_GetUserinfo: bufferSize == %i", bufferSize);
    }
  if (index < 0 || index >= sv_maxclients->integer)
    {
      SV_Error (ERR_DROP, "SV_GetUserinfo: bad index %i\n", index);
    }
  Q_strncpyz (buffer, svs.clients[index].userinfo, bufferSize);
}

/*
================
SV_CreateBaseline

Entity baselines are used to compress non-delta messages
to the clients -- only the fields that differ from the
baseline will be transmitted
================
*/
static void
SV_CreateBaseline (void)
{
  int entnum;
  sharedEntity_t * svent;

  for (entnum = 0; entnum < sv.num_entities; entnum++)
    {
      svent = SV_GentityNum (entnum);
      if (!svent->r.linked)
	{
	  continue;
	}

      if (svent->s.number != entnum)
	{
	  SV_DPrintf ("SV_CreateBaseline : Fixing ent->s.number !\n");
	  svent->s.number = entnum;
	}

      //
      // take current state as baseline
      //
      sv.svEntities[entnum].baseline = svent->s;
    }
}

/*
===============
SV_BoundMaxClients

===============
*/
static void
SV_BoundMaxClients (int minimum)
{
  // get the new maxclients value (the latched one)
  qce->cvar.Get ("sv_maxclients", "10", 0);

  sv_maxclients->modified = false;

  if (sv_maxclients->integer < minimum)
    {
      qce->cvar.SetNew (sv_maxclients, va("%i", minimum));
    }
  else if (sv_maxclients->integer > MAX_CLIENTS)
    {
      qce->cvar.SetNew (sv_maxclients, va("%i", MAX_CLIENTS));
    }
}

/*
===============
SV_Startup

Called when a host starts a map when it wasn't running
one before.  Successive map or map_restart commands will
NOT cause this to be called, unless the game is exited to
the menu system first.
===============
*/
static void
SV_Startup (void)
{
  if (svs.initialized)
    {
      SV_Error (ERR_FATAL, "SV_Startup: svs.initialized");
    }

  SV_BoundMaxClients (1);

  svs.clients = qce->mem.Z_Malloc (sizeof(client_t) * sv_maxclients->integer);
  svs.numSnapshotEntities = sv_maxclients->integer * PACKET_BACKUP * 64;

  svs.initialized = true;

  // Don't respect sv_killserver unless a server is actually running
  if (sv_killserver->integer)
    {
      qce->cvar.SetNew (sv_killserver, "0");
    }

  qce->cvar.SetNew (sv_running, "1");

  // Join the ipv6 multicast group now that a map is running
  // so clients can scan for us on the local network.
  qce->net.JoinMulticast6 ();
}

/*
==================
SV_ChangeMaxClients
==================
*/
void
SV_ChangeMaxClients (void)
{
  int		oldMaxClients;
  int		i;
  client_t	*oldClients;
  int		count;

  // get the highest client number in use
  count = 0;
  for (i = 0; i < sv_maxclients->integer; i++)
    {
      if (svs.clients[i].state >= CS_CONNECTED)
	{
	  if (i > count)
	    count = i;
	}
    }
  count++;

  oldMaxClients = sv_maxclients->integer;
  // never go below the highest client number in use
  SV_BoundMaxClients (count);
  // if still the same
  if (sv_maxclients->integer == oldMaxClients)
    {
      return;
    }

  oldClients = qce->mem.Hunk_AllocateTempMemory (count * sizeof(client_t));
  // copy the clients to hunk memory
  for (i = 0; i < count; i++)
    {
      if (svs.clients[i].state >= CS_CONNECTED)
	{
	  oldClients[i] = svs.clients[i];
	}
      else
	{
	  Com_Memset (&oldClients[i], 0, sizeof(client_t));
	}
    }

  // free old clients arrays
  qce->mem.Z_Free (svs.clients);

  // allocate new clients
  svs.clients = qce->mem.Z_Malloc (sv_maxclients->integer * sizeof(client_t));
  Com_Memset (svs.clients, 0, sv_maxclients->integer * sizeof(client_t));

  // copy the clients over
  for (i = 0; i < count; i++)
    {
      if (oldClients[i].state >= CS_CONNECTED)
	{
	  svs.clients[i] = oldClients[i];
	}
    }

  // free the old clients on the hunk
  qce->mem.Hunk_FreeTempMemory (oldClients);

  // allocate new snapshot entities
  svs.numSnapshotEntities = sv_maxclients->integer * PACKET_BACKUP * 64;
}

/*
================
SV_ClearServer
================
*/
static void
SV_ClearServer (void)
{
  int i;

  for (i = 0; i < MAX_CONFIGSTRINGS; i++)
    {
      if (sv.configstrings[i])
	{
	  qce->mem.Z_Free (sv.configstrings[i]);
	}
    }
  Com_Memset (&sv, 0, sizeof(sv));
}

/*
================
SV_SpawnServer

Change the server to a new map, taking all connected
clients along with it.
This is NOT called for map_restart
================
*/
void
SV_SpawnServer (char *server, bool killBots)
{
  int		i;
  int		checksum;
  bool	isBot;
  const char	*p;

  // shut down the existing game if it is running
  SV_ShutdownGameProgs ();

  SV_Printf ("------ Server Initialization ------\n");
  SV_Printf ("Server: %s\n",server);

  // clear the whole hunk because we're (re)loading the server
  qce->mem.Hunk_Clear ();

  // clear collision map data
  qce->cm.ClearMap ();

  // init client structures and svs.numSnapshotEntities
  if (!sv_running->integer)
    {
      SV_Startup();
    }
  else
    {
      // check for maxclients change
      if (sv_maxclients->modified)
	{
	  SV_ChangeMaxClients();
	}
    }

  // allocate the snapshot entities on the hunk
  svs.snapshotEntities =
    qce->mem.Hunk_Alloc (sizeof(entityState_t)*svs.numSnapshotEntities, h_high);
  svs.nextSnapshotEntities = 0;

  // toggle the server bit so clients can detect that a
  // server has changed
  svs.snapFlagServerBit ^= SNAPFLAG_SERVERCOUNT;

  // set nextmap to the same map, but it may be overriden
  // by the game startup or another console command
  qce->cvar.SetNew (sv_nextmap, "map_restart 0");

  // wipe the entire per-level structure (configstrings)
  SV_ClearServer ();
  // we plan to use these again, make them "valid"
  for (i = 0; i < MAX_CONFIGSTRINGS; i++)
    {
      sv.configstrings[i] = qce->mem.CopyString("");
    }

  // get a new checksum feed and restart the file system
  sv.checksumFeed = (((int) rand() << 16) ^ rand()) ^ qce->event.Milliseconds();
  // also clears pakreferences
  qce->fs.Restart (sv.checksumFeed);

  qce->cm.LoadMap (va("maps/%s.bsp", server), false, &checksum);

  // set serverinfo visible name
  qce->cvar.SetNew (sv_mapname, server);

  qce->cvar.SetNew (sv_mapChecksum, va("%i", checksum));

  // serverid should be different each time
  sv.serverId = sv_frametime;
  sv.restartedServerId = sv.serverId; // I suppose the init here is just to be safe
  sv.checksumFeedServerId = sv.serverId;
  qce->cvar.SetNew (sv_serverid, va("%i", sv.serverId));

  // clear physics interaction links
  SV_ClearWorld ();

  // media configstring setting should be done during
  // the loading stage, so connected clients don't have
  // to load during actual gameplay
  sv.state = SS_LOADING;

  // load and spawn all other entities
  SV_InitGameProgs ();

  // run a few frames to allow everything to settle
  for (i = 0; i < 3; i++)
    {
      glibe->run_frame (sv.time);
      SV_BotFrame (sv.time);
      sv.time += 50;
      svs.time += 50;
    }

  // create a baseline for more efficient communications
  SV_CreateBaseline ();

  for (i=0; i < sv_maxclients->integer; i++)
    {
      // send the new gamestate to all connected clients
      if (svs.clients[i].state >= CS_CONNECTED)
	{
	  char * denied;

	  // connected client is a bot
	  if (svs.clients[i].netchan.remoteAddress.type == NA_BOT)
	    {
	      if (killBots)
		{
		  SV_DropClient (&svs.clients[i], "");
		  continue;
		}
	      isBot = true;
	    }
	  else
	    {
	      isBot = false;
	    }

	  // connect the client again
	  denied = glibe->client_connect (i, false, isBot);
	  if (denied)
	    {
	      // this generally shouldn't happen, because the client
	      // was connected before the level change
	      SV_DropClient (&svs.clients[i], denied);
	    }
	  else
	    {
	      if (!isBot)
		{
		  // when we get the next packet from a connected client,
		  // the new gamestate will be sent
		  svs.clients[i].state = CS_CONNECTED;
		}
	      else
		{
		  client_t *client;
		  sharedEntity_t *ent;

		  client = &svs.clients[i];
		  client->state = CS_ACTIVE;
		  ent = SV_GentityNum (i);
		  ent->s.number = i;
		  client->gentity = ent;

		  client->deltaMessage = -1;
		  client->nextSnapshotTime = svs.time;	// generate a snapshot immediately

		  // call the game begin function
		  glibe->client_begin (i);
		}
	    }
	}
    }

  // run another frame to allow things to look at all the players
  glibe->run_frame (sv.time);
  SV_BotFrame (sv.time);
  sv.time += 25;
  svs.time += 25;

  // the server sends these to the clients so they can figure
  // out which pk3s should be auto-downloaded
  p = qce->fs.ReferencedPakChecksums ();
  qce->cvar.SetNew (sv_referencedPaks, p);
  p = qce->fs.ReferencedPakNames ();
  qce->cvar.SetNew (sv_referencedPakNames, p);

  // save systeminfo and serverinfo strings
  // Q_strncpyz (systemInfo, qce->cvar.InfoString_Big(CVAR_SYSTEMINFO), sizeof(systemInfo));
  SV_SetConfigstring (CS_SYSTEMINFO, qce->cvar.InfoString_Big(CVAR_SYSTEMINFO));
  i = qce->cvar.Get_ModifiedFlags ();
  i &= ~CVAR_SYSTEMINFO;
  qce->cvar.Set_ModifiedFlags (i);

  SV_SetConfigstring (CS_SERVERINFO, qce->cvar.InfoString(CVAR_SERVERINFO));
  i = qce->cvar.Get_ModifiedFlags ();
  i &= ~CVAR_SERVERINFO;
  qce->cvar.Set_ModifiedFlags (i);

  // any media configstring setting now should issue a warning
  // and any configstring changes should be reliably transmitted
  // to all clients
  sv.state = SS_GAME;

  // send a heartbeat now so the master will get up to date info
  SV_Heartbeat_f ();

  qce->mem.Hunk_SetMark ();

  SV_Printf ("-----------------------------------\n");
}

/*
===============
SV_Init

Only called at main exe startup, not for each game
===============
*/
void
SV_Init (void)
{
  int index;

  SV_AddOperatorCommands ();

  // send heartbeats to master ?
  sv_dedicated = qce->cvar.Get ("dedicated", "1", CVAR_INIT);
  qce->cvar.CheckRange (sv_dedicated, 1, 2, true);
  // are we running a game mode or sleeping ?
  sv_running = qce->cvar.Get ("sv_running", "0", CVAR_ROM);
  sv_sleepDelay = qce->cvar.Get ("sv_sleepDelay", "120", CVAR_ARCHIVE);
  // we don't want this to change "on the fly"; set the behaviour on startup
  sv_autoWakeUp = qce->cvar.Get ("sv_autoWakeUp", "1", CVAR_INIT);

  // timing and profiling, keep the old name
  sv_speeds = qce->cvar.Get ("sv_speeds", "0", 0);
  sv_dropsim = qce->cvar.Get ("sv_dropsim", "0", CVAR_CHEAT);
  sv_showtrace = qce->cvar.Get ("sv_showtrace", "0", CVAR_CHEAT);
  sv_timescale = qce->cvar.Get ("timescale", "1", CVAR_CHEAT | CVAR_SYSTEMINFO);
  sv_fixedtime = qce->cvar.Get ("fixedtime", "0", CVAR_CHEAT);
  sv_developer = qce->cvar.Get ("developer", "1", CVAR_TEMP);
  sv_buildscript = qce->cvar.Get ("buildScript", "0", 0);

  // serverinfo vars
  qce->cvar.Get ("protocol", va("%i", PROTOCOL_VERSION), CVAR_SERVERINFO | CVAR_ROM);
  sv_mapname = qce->cvar.Get ("mapname", "nomap", CVAR_SERVERINFO | CVAR_ROM);
  sv_privateClients = qce->cvar.Get ("sv_privateClients", "0", CVAR_SERVERINFO);
  sv_hostname = qce->cvar.Get ("sv_hostname", "noname", CVAR_SERVERINFO | CVAR_ARCHIVE);
  sv_maxclients = qce->cvar.Get ("sv_maxclients", "10",
				 (CVAR_SERVERINFO | CVAR_LATCH | CVAR_ARCHIVE));

  sv_minRate = qce->cvar.Get ("sv_minRate", "3000", CVAR_ARCHIVE | CVAR_SERVERINFO);
  sv_maxRate = qce->cvar.Get ("sv_maxRate", "50000", CVAR_ARCHIVE | CVAR_SERVERINFO);
  sv_minPing = qce->cvar.Get ("sv_minPing", "0", CVAR_ARCHIVE | CVAR_SERVERINFO);
  sv_maxPing = qce->cvar.Get ("sv_maxPing", "0", CVAR_ARCHIVE | CVAR_SERVERINFO);
  sv_floodProtect = qce->cvar.Get ("sv_floodProtect", "1", CVAR_ARCHIVE | CVAR_SERVERINFO);

  // systeminfo
  sv_serverid = qce->cvar.Get ("sv_serverid", "0", CVAR_SYSTEMINFO | CVAR_ROM);
  sv_referencedPaks = qce->cvar.Get ("sv_referencedPaks", "", CVAR_SYSTEMINFO | CVAR_ROM);
  sv_referencedPakNames = qce->cvar.Get ("sv_referencedPakNames", "", CVAR_SYSTEMINFO | CVAR_ROM);

  // server vars
  sv_rconPassword = qce->cvar.Get ("rconPassword", "", CVAR_TEMP);
  sv_privatePassword = qce->cvar.Get ("sv_privatePassword", "", CVAR_TEMP);
  sv_fps = qce->cvar.Get ("sv_fps", "50", CVAR_TEMP);
  sv_timeout = qce->cvar.Get ("sv_timeout", "60", CVAR_TEMP);
  sv_zombietime = qce->cvar.Get ("sv_zombietime", "2", CVAR_TEMP);
  sv_nextmap = qce->cvar.Get ("nextmap", "", CVAR_TEMP);

  sv_master[0] = qce->cvar.Get ("sv_master1", /*MASTER_SERVER_NAME*/"", 0);
  for (index = 1; index < MAX_MASTER_SERVERS; index++)
    sv_master[index] = qce->cvar.Get (va ("sv_master%d", index + 1), "", CVAR_ARCHIVE);

  sv_heartbeat = qce->cvar.Get ("sv_heartbeat", HEARTBEAT_FOR_MASTER, CVAR_INIT);
  sv_flatline = qce->cvar.Get ("sv_flatline", FLATLINE_FOR_MASTER, CVAR_INIT);

  sv_reconnectlimit = qce->cvar.Get ("sv_reconnectlimit", "3", 0);
  sv_padPackets = qce->cvar.Get ("sv_padPackets", "0", 0);
  sv_killserver = qce->cvar.Get ("sv_killserver", "0", 0);
  sv_mapChecksum = qce->cvar.Get ("sv_mapChecksum", "", CVAR_ROM);
  sv_lanForceRate = qce->cvar.Get ("sv_lanForceRate", "1", CVAR_ARCHIVE);

  sv_bot = qce->cvar.Get ("sv_bot", "1", CVAR_INIT);

  // initialize bot cvars so they are listed and can be set before loading the botlib
  SV_BotInitCvars();

  // init the botlib here because we need the pre-compiler in the UI
  SV_BotInitBotLib();
}

/*
==================
SV_FinalMessage

Used by SV_Shutdown to send a final message to all
connected clients before the server goes down.  The messages are sent immediately,
not just stuck on the outgoing message list, because the server is going
to totally exit after returning from this function.
==================
*/
void
SV_FinalMessage (const char *message)
{
  int		i, j;
  client_t	*cl;

  // send it twice, ignoring rate
  for (j = 0; j < 2; j++)
    {
      for (i = 0, cl = svs.clients; i < sv_maxclients->integer; i++, cl++)
	{
	  if (cl->state >= CS_CONNECTED)
	    {
	      // don't send a disconnect to a local client
	      if (cl->netchan.remoteAddress.type != NA_LOOPBACK)
		{
		  SV_SendServerCommand (cl, "print \"%s\n\"\n", message);
		  SV_SendServerCommand (cl, "disconnect \"%s\"", message);
		}
	      // force a snapshot to be sent
	      cl->nextSnapshotTime = -1;
	      SV_SendClientSnapshot (cl);
	    }
	}
    }
}

/*
================
SV_Shutdown

Called when each game quits,
before Sys_Quit or Sys_Error
================
*/
void
SV_Shutdown (const char *finalmsg)
{
  if (sv_running == NULL || !sv_running->integer)
    {
      return;
    }

  SV_Printf ("----- Server Shutdown (%s) -----\n", finalmsg);

  //assert (sv_qcomLoaded);
  qce->net.LeaveMulticast6 ();

  if (svs.clients && !sv_errorEntered)
    {
      SV_FinalMessage (finalmsg);
    }

  SV_RemoveOperatorCommands ();
  SV_MasterShutdown ();
  SV_ShutdownGameProgs ();

  // free current level
  SV_ClearServer ();

  // free server static data
  if (svs.clients)
    {
      qce->mem.Z_Free (svs.clients);
    }
  Com_Memset (&svs, 0, sizeof(svs));

  qce->cvar.SetNew (sv_running, "0");

  SV_Printf ("---------------------------\n");
}

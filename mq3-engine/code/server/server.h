/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/
// server.h
// maths and basic manipulations
#include "qcommon/q_shared.h"
// common structures and defines
#include "qcommon/qcom_extern.h"
// interfaces with qcommon
#include "qcommon/qcom_public.h"

// library loading
#include "qcommon/dynlib_local.h"
#include "sys/sys_public_dlib.h"
// sys lib interfaces
#include "sys/sys_public_interface.h"

// botlib
#include "botlib/botlib.h"

// game lib api and structures
#include "server/sv_game_API.h"

//=============================================================================

#define	PERS_SCORE		0		// !!! MUST NOT CHANGE, SERVER AND
						// GAME BOTH REFERENCE !!!

#define	MAX_ENT_CLUSTERS	16
#define	MAX_MASTER_SERVERS	5


typedef struct svEntity_s
{
  struct worldSector_s *worldSector;
  struct svEntity_s *nextEntityInWorldSector;

  entityState_t	baseline;		// for delta compression of initial sighting
  int		numClusters;		// if -1, use headnode instead
  int		clusternums[MAX_ENT_CLUSTERS];
  int		lastCluster;		// if all the clusters don't fit in clusternums
  int		areanum, areanum2;
  int		snapshotCounter;	// used to prevent double adding from portal views
} svEntity_t;

typedef enum
{
  SS_DEAD,			// no map loaded
  SS_LOADING,			// spawning level entities
  SS_GAME			// actively running
} serverState_t;

typedef struct
{
  serverState_t		state;
  bool		restarting;		// if true, send configstring changes during SS_LOADING
  int			serverId;		// changes each server start
  int			restartedServerId;	// serverId before a map_restart

  int			checksumFeed;		// the feed key that we use to compute the pure checksum strings
  // https://zerowing.idsoftware.com/bugzilla/show_bug.cgi?id=475
  // the serverId associated with the current checksumFeed (always <= serverId)
  int			checksumFeedServerId;
  int			snapshotCounter;	// incremented for each snapshot built
  int			timeResidual;		// <= 1000 / sv_frame->value
  int			nextFrameTime;		// when time > nextFrameTime, process world
  struct cmodel_s	*models[MAX_MODELS];
  char			*configstrings[MAX_CONFIGSTRINGS];
  svEntity_t		svEntities[MAX_GENTITIES];

  char			*entityParsePoint;	// used during game VM init

  // the game virtual machine will update these on init and changes
  sharedEntity_t	*gentities;
  int			gentitySize;
  int			num_entities;		// current number, <= MAX_GENTITIES

  playerState_t		*gameClients;
  int			gameClientSize;		// will be > sizeof(playerState_t) due to game private data

  int			restartTime;
  int			time;
  int			simulationTime;
} server_t;



typedef struct
{
  int			areabytes;
  byte			areabits[MAX_MAP_AREA_BYTES];	// portalarea visibility bits
  playerState_t		ps;
  int			num_entities;
  // into the circular sv_packet_entities[]
  // the entities MUST be in increasing state number
  // order, otherwise the delta compression will fail
  int			first_entity;
  int			messageSent;		// time the message was transmitted
  int			messageAcked;		// time the message was acked
  int			messageSize;		// used to rate drop packets
} clientSnapshot_t;

typedef enum
{
  CS_FREE,		// can be reused for a new connection
  CS_ZOMBIE,		// client has been disconnected, but don't reuse
			// connection for a couple seconds
  CS_CONNECTED,		// has been assigned to a client_t, but no gamestate yet
  CS_PRIMED,		// gamestate has been sent, but client hasn't sent a usercmd
  CS_ACTIVE		// client is fully in game
} clientState_t;

typedef struct netchan_buffer_s
{
  msg_t           msg;
  byte            msgBuffer[MAX_MSGLEN];
  struct netchan_buffer_s *next;
} netchan_buffer_t;

typedef struct client_s
{
  clientState_t	state;
  char		userinfo[MAX_INFO_STRING];	// name, etc

  char		reliableCommands[MAX_RELIABLE_COMMANDS][MAX_STRING_CHARS];
  int		reliableSequence;	// last added reliable message, not necesarily sent or acknowledged yet
  int		reliableAcknowledge;	// last acknowledged reliable message
  int		reliableSent;		// last sent reliable message, not necesarily acknowledged yet
  int		messageAcknowledge;

  int		gamestateMessageNum;	// netchan->outgoingSequence of gamestate
  int		challenge;

  usercmd_t	lastUsercmd;
  int		lastMessageNum;		// for delta compression
  int		lastClientCommand;	// reliable client message sequence
  char		lastClientCommandString[MAX_STRING_CHARS];
  sharedEntity_t*gentity;		// SV_GentityNum(clientnum)
  char		name[MAX_NAME_LENGTH];	// extracted from userinfo, high bits masked

  int		deltaMessage;		// frame last client usercmd message
  int		nextReliableTime;	// svs.time when another reliable command will be allowed
  int		lastPacketTime;		// svs.time when packet was last received
  int		lastConnectTime;	// svs.time when connection started
  int		nextSnapshotTime;	// send another snapshot when svs.time >= nextSnapshotTime
  bool	rateDelayed;		// true if nextSnapshotTime was set based on rate instead of snapshotMsec
  int		timeoutCount;		// must timeout a few frames in a row so debugging doesn't break
  clientSnapshot_t	frames[PACKET_BACKUP];	// updates can be delta'd from here
  int		ping;
  int		rate;			// bytes / second
  int		snapshotMsec;		// requests a snapshot every snapshotMsec unless rate choked

  netchan_t	netchan;
  // TTimo
  // queuing outgoing fragmented messages to send them properly, without udp packet bursts
  // in case large fragmented messages are stacking up
  // buffer them into this queue, and hand them out to netchan as needed
  netchan_buffer_t	*netchan_start_queue;
  netchan_buffer_t	**netchan_end_queue;

  bool	csUpdated[MAX_CONFIGSTRINGS+1];
} client_t;

//=============================================================================

// MAX_CHALLENGES is made large to prevent a denial
// of service attack that could cycle all of them
// out before legitimate users connected
#define	MAX_CHALLENGES		1024

#define	AUTHORIZE_TIMEOUT	5000

typedef struct
{
  netadr_t	adr;
  int		challenge;
  int		time;		// time the last packet was sent to the autherize server
  int		pingTime;	// time the challenge response was sent to client
  int		firstTime;	// time the adr was first used, for authorize timeout checks
  bool	wasrefused;
  bool	connected;
} challenge_t;


// this structure will be cleared only when the game dll changes
typedef struct
{
  bool	initialized;			// sv_init has completed

  int		time;				// will be strictly increasing across level changes

  int		snapFlagServerBit;		// ^= SNAPFLAG_SERVERCOUNT every SV_SpawnServer()

  client_t	*clients;			// [sv_maxclients->integer];
  int		numSnapshotEntities;		// sv_maxclients->integer*PACKET_BACKUP*MAX_PACKET_ENTITIES
  int		nextSnapshotEntities;		// next snapshotEntities to use
  entityState_t	*snapshotEntities;		// [numSnapshotEntities]
  int		nextHeartbeatTime;
  challenge_t	challenges[MAX_CHALLENGES];	// to prevent invalid IPs from connecting
  netadr_t	redirectAddress;		// for rcon return messages

  netadr_t	authorizeAddress;		// for rcon return messages
} serverStatic_t;

/*
#define SERVER_MAXBANS	1024
// Structure for managing bans
typedef struct
{
  netadr_t	ip;
  // For a CIDR-Notation type suffix
  int		subnet;

  bool	isexception;
} serverBan_t;
*/

//=============================================================================

extern	serverStatic_t	svs;			// persistant server info across maps
extern	server_t	sv;			// cleared each map

int sv_frametime;

// dynamic lib
// qcommon lib
extern Dlib_t		qclib;
extern qcom_export_t *	qce;
extern qcom_import_t	qimp;
// game mod
extern Dlib_t		gdlib;
extern gamelib_export_t * glibe;
extern gamelib_import_t glibi;

// botlib
extern Dlib_t		botlib;
extern botlib_export_t *botlib_export;
extern int bot_enable;


// this one is true if qce is valid and qcommon has been initialized properly
extern bool sv_qcomLoaded;

extern	cvar_t	*sv_fps;
extern	cvar_t	*sv_timeout;
extern	cvar_t	*sv_zombietime;
extern	cvar_t	*sv_rconPassword;
extern	cvar_t	*sv_privatePassword;
extern	cvar_t	*sv_maxclients;
extern	cvar_t	*sv_dedicated;
extern	cvar_t	*sv_running;
extern	cvar_t	*sv_sleepDelay;
extern	cvar_t	*sv_autoWakeUp;

extern	cvar_t	*sv_privateClients;
extern	cvar_t	*sv_hostname;
extern	cvar_t	*sv_master[MAX_MASTER_SERVERS];
extern	cvar_t	*sv_heartbeat;
extern	cvar_t	*sv_flatline;
extern	cvar_t	*sv_reconnectlimit;
extern	cvar_t	*sv_padPackets;
extern	cvar_t	*sv_killserver;
extern	cvar_t	*sv_mapname;
extern	cvar_t	*sv_mapChecksum;
extern	cvar_t	*sv_serverid;
extern	cvar_t	*sv_minRate;
extern	cvar_t	*sv_maxRate;
extern	cvar_t	*sv_minPing;
extern	cvar_t	*sv_maxPing;
extern	cvar_t	*sv_floodProtect;
extern	cvar_t	*sv_lanForceRate;

extern	cvar_t	*sv_referencedPaks;
extern	cvar_t	*sv_referencedPakNames;

extern	cvar_t	*sv_speeds;
extern	cvar_t	*sv_dropsim;
extern	cvar_t	*sv_showtrace;
extern	cvar_t	*sv_timescale;
extern	cvar_t	*sv_fixedtime;

extern	cvar_t	*sv_nextmap;

extern	cvar_t	*sv_bot;
extern	cvar_t	*sv_developer;
extern	cvar_t	*sv_buildscript;

//extern	serverBan_t serverBans[SERVER_MAXBANS];
//extern	int	serverBansCount;

//===========================================================
//

void SV_Printf (const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
void SV_DPrintf (const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
void SV_Error (int code, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));
void SV_Fatal_Error (int code, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));
// more important server functions
void SV_Init (void);
void SV_Shutdown (const char *finalmsg);
void SV_Frame (/*int msec*/void);
void SV_PacketEvent (netadr_t from, msg_t *msg);
bool SV_GameCommand (void);

//
// sv_main.c
//
void SV_FinalMessage (const char* message);
void SV_SendServerCommand (client_t *cl, const char *fmt, ...);

void SV_AddOperatorCommands (void);
void SV_RemoveOperatorCommands (void);

// shouldn't be shared
//void SV_MasterHeartbeat (void);
void SV_MasterShutdown (void);

void SV_Quit_f (void);
//
// sv_init.c
//
void SV_SetConfigstring (int index, const char *val);
void SV_GetConfigstring (int index, char *buffer, int bufferSize);
void SV_UpdateConfigstrings (client_t *client);

void SV_SetUserinfo (int index, const char *val);
void SV_GetUserinfo (int index, char *buffer, int bufferSize);

void SV_ChangeMaxClients (void);
void SV_SpawnServer (char *server, bool killBots);

//
// sv_client.c
//
void SV_GetChallenge (netadr_t from);

void SV_DirectConnect (netadr_t from);

void SV_ExecuteClientMessage (client_t *cl, msg_t *msg);
void SV_UserinfoChanged (client_t *cl);

void SV_ClientEnterWorld (client_t *client, usercmd_t *cmd);
void SV_DropClient (client_t *drop, const char *reason);

void SV_ExecuteClientCommand (client_t *cl, const char *s, bool clientOK);
void SV_ClientThink (client_t *cl, usercmd_t *cmd);
void SV_ClientThink2 (int clientNum, usercmd_t *cmd);

//
// sv_ccmds.c
//
void SV_Heartbeat_f (void);

//
// sv_snapshot.c
//
void SV_AddServerCommand (client_t *client, const char *cmd);
void SV_UpdateServerCommandsToClient (client_t *client, msg_t *msg);
void SV_WriteFrameToClient (client_t *client, msg_t *msg);
void SV_SendMessageToClient (msg_t *msg, client_t *client);
void SV_SendClientMessages (void);
void SV_SendClientSnapshot (client_t *client);

//
// sv_game.c
//
int		SV_NumForGentity (sharedEntity_t *ent);
sharedEntity_t	*SV_GentityNum (int num);
playerState_t	*SV_GameClientNum (int num);
svEntity_t	*SV_SvEntityForGentity (sharedEntity_t *gEnt);
sharedEntity_t	*SV_GEntityForSvEntity (svEntity_t *svEnt);
void		SV_InitGameProgs (void);
void		SV_ShutdownGameProgs (void);
void		SV_RestartGameProgs (void);
bool	SV_inPVS (const vec3_t p1, const vec3_t p2);

//
// sv_net_chan.c
//
void SV_Netchan_Transmit (client_t *client, msg_t *msg);
void SV_Netchan_TransmitNextFragment (client_t *client);
bool SV_Netchan_Process (client_t *client, msg_t *msg);

//
// sv_bot.c
//
void SV_BotFrame (int time);
int SV_BotAllocateClient (void);
void SV_BotFreeClient (int clientNum);

void SV_BotInitCvars (void);
int SV_BotLibSetup (void);
int SV_BotLibShutdown (void);
int SV_BotGetSnapshotEntity (int client, int ent);
int SV_BotGetConsoleMessage (int client, char *buf, int size);
void SV_BotUserCommand (int client, usercmd_t *cmd);

int BotImport_DebugPolygonCreate (int color, int numPoints, vec3_t *points);
void BotImport_DebugPolygonDelete (int id);

void SV_BotInitBotLib (void);
void SV_BotShutdownBotLib (void);

//============================================================
//
// high level object sorting to reduce interaction tests
//

void SV_ClearWorld (void);
// called after the world model has been loaded, before linking any entities

void SV_UnlinkEntity (sharedEntity_t *ent);
// call before removing an entity, and before trying to move one,
// so it doesn't clip against itself

void SV_LinkEntity (sharedEntity_t *ent);
// Needs to be called any time an entity changes origin, mins, maxs,
// or solid.  Automatically unlinks if needed.
// sets ent->v.absmin and ent->v.absmax
// sets ent->leafnums[] for pvs determination even if the entity
// is not solid

clipHandle_t SV_ClipHandleForEntity (const sharedEntity_t *ent);

void SV_SectorList_f (void);

int SV_AreaEntities (const vec3_t mins, const vec3_t maxs, int *entityList, int maxcount);
// fills in a table of entity numbers with entities that have bounding boxes
// that intersect the given area.  It is possible for a non-axial bmodel
// to be returned that doesn't actually intersect the area on an exact
// test.
// returns the number of pointers filled in
// The world entity is never returned in this list.

int SV_PointContents (const vec3_t p, int passEntityNum);
// returns the CONTENTS_* value from the world and all entities at the given point.

void SV_Trace (trace_t *results, const vec3_t start, const vec3_t mins,
	       const vec3_t maxs, const vec3_t end, int passEntityNum,
	       int contentmask);
// mins and maxs are relative

// if the entire move stays in a solid volume, trace.allsolid will be set,
// trace.startsolid will be set, and trace.fraction will be 0

// if the starting point is in a solid, it will be allowed to move out
// to an open area

// passEntityNum is explicitly excluded from clipping checks (normally ENTITYNUM_NONE)

void SV_ClipToEntity (trace_t *trace, const vec3_t start, const vec3_t mins, const vec3_t maxs,
		      const vec3_t end, int entityNum, int contentmask);
// clip to a specific entity

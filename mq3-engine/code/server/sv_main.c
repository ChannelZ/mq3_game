/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "server.h"

#include <setjmp.h>

serverStatic_t	svs;			// persistant server info
server_t	sv;			// local server

bool	sv_qcomLoaded;		// have we a valid qce structure ?
char		sv_qcomErrorMessage[MAXPRINTMSG];
bool	sv_errorEntered;	// an error happend, don't recurse

// last gameplay related received packet time
// so we can close the serv when nothing happends
int		sv_lastActivePacketTime;

// dynamic librarie
Dlib_t gdlib;
// resolved functions for dyn lib for game
gamelib_export_t * glibe;
gamelib_import_t glibi;

Dlib_t qclib;
qcom_export_t * qce;
qcom_import_t qimp;

// botlib globals
Dlib_t botlib;
botlib_export_t *botlib_export;
int bot_enable;


cvar_t * sv_fps;		// time rate for running non-clients
cvar_t * sv_timeout;		// seconds without any message
cvar_t * sv_zombietime;		// seconds to sink messages after disconnect
cvar_t * sv_rconPassword;	// password for remote server commands
cvar_t * sv_privatePassword;	// password for the privateClient slots
cvar_t * sv_maxclients;
cvar_t * sv_dedicated;
cvar_t * sv_running;
cvar_t * sv_sleepDelay;
cvar_t * sv_autoWakeUp;

cvar_t * sv_privateClients;	// number of clients reserved for password
cvar_t * sv_hostname;
cvar_t * sv_master[MAX_MASTER_SERVERS];	// master server ip address
cvar_t  *sv_heartbeat;			// Heartbeat string that is sent to the master
cvar_t  *sv_flatline;			// If the master server supports it we can send a flatline
cvar_t * sv_reconnectlimit;	// minimum seconds between connect messages
cvar_t * sv_padPackets;		// add nop bytes to messages
cvar_t * sv_killserver;		// menu system can set to 1 to shut server down
cvar_t * sv_mapname;
cvar_t * sv_mapChecksum;
cvar_t * sv_serverid;
cvar_t * sv_minRate;
cvar_t * sv_maxRate;
cvar_t * sv_minPing;
cvar_t * sv_maxPing;
cvar_t * sv_floodProtect;
cvar_t * sv_lanForceRate; // dedicated 1 (LAN) server forces local client rates to 99999

cvar_t * sv_referencedPaks;
cvar_t * sv_referencedPakNames;

cvar_t * sv_speeds;
cvar_t * sv_dropsim;		// 0.0 - 1.0 simulated packet drop
cvar_t * sv_showtrace;
cvar_t * sv_timescale;
cvar_t * sv_fixedtime;

cvar_t * sv_nextmap;

cvar_t * sv_bot;
cvar_t * sv_developer;
cvar_t * sv_buildscript;

//serverBan_t serverBans[SERVER_MAXBANS];
//int serverBansCount = 0;

int	sv_frameNumber;
//int	sv_frameMsec;
jmp_buf	abortframe;		// an ERR_DROP occured, exit the entire frame

/*
=============================================================================

EVENT MESSAGES

=============================================================================
*/
/*
===============
SV_ExpandNewlines

Converts newlines to "\n" so a line prints nicer
===============
*/
static char *
SV_ExpandNewlines (char *in)
{
  static char string[1024];
  unsigned int l;

  l = 0;
  while (*in && l < sizeof(string) - 3)
    {
      if (*in == '\n')
	{
	  string[l++] = '\\';
	  string[l++] = 'n';
	}
      else
	{
	  string[l++] = *in;
	}
      in++;
    }
  string[l] = 0;

  return string;
}

/*
======================
SV_AddServerCommand

The given command will be transmitted to the client, and is guaranteed to
not have future snapshot_t executed before it is executed
======================
*/
void
SV_AddServerCommand (client_t *client, const char *cmd)
{
  int index, i;

  // do not send commands until the gamestate has been sent
  if (client->state < CS_PRIMED)
    return;

  client->reliableSequence++;
  // if we would be losing an old command that hasn't been acknowledged,
  // we must drop the connection
  // we check == instead of >= so a broadcast print added by SV_DropClient()
  // doesn't cause a recursive drop client
  if ((client->reliableSequence - client->reliableAcknowledge) == (MAX_RELIABLE_COMMANDS + 1))
    {
      SV_Printf ("===== pending server commands =====\n");
      for (i = (client->reliableAcknowledge + 1); i <= client->reliableSequence; i++)
	{
	  int index = i & (MAX_RELIABLE_COMMANDS - 1);
	  SV_Printf ("cmd %5d: %s\n", i, client->reliableCommands[index]);
	}
      SV_Printf ("cmd %5d: %s\n", i, cmd);
      SV_DropClient (client, "Server command overflow");
      return;
    }
  index = client->reliableSequence & (MAX_RELIABLE_COMMANDS - 1);
  Q_strncpyz (client->reliableCommands[index], cmd, sizeof(client->reliableCommands[index]));
}

/*
=================
SV_SendServerCommand

Sends a reliable command string to be interpreted by
the client game module: "cp", "print", "chat", etc
A NULL client will broadcast to all clients
=================
*/
void
SV_SendServerCommand (client_t *cl, const char *fmt, ...)
{
  int j;
  va_list argptr;
  byte message[MAX_MSGLEN];
  client_t * client;

  va_start (argptr,fmt);
  Q_vsnprintf ((char *)message, sizeof(message), fmt, argptr);
  va_end (argptr);

  if (cl != NULL)
    {
      SV_AddServerCommand (cl, (char *)message);
      return;
    }

  // hack to echo broadcast prints to console
  if (!Q_strncmp ((char *)message, "print", 5))
    {
      SV_Printf ("broadcast: %s\n", SV_ExpandNewlines ((char *)message));
    }

  // send the data to all relevent clients
  for (j = 0, client = svs.clients; j < sv_maxclients->integer; j++, client++)
    {
      SV_AddServerCommand (client, (char *)message);
    }
}

/*
==============================================================================

MASTER SERVER FUNCTIONS

==============================================================================
*/
/*
================
SV_MasterHeartbeat

Send a message to the masters every few minutes to
let it know we are alive, and log information.
We will also have a heartbeat sent when a server
changes from empty to non-empty, and full to non-full,
but not on every player enter or exit.
================
*/
#define	HEARTBEAT_MSEC	300*1000
void
SV_MasterHeartbeat (const char *message)
{
  // [2] for v4 and v6 adress for the same address string.
  static netadr_t adr[MAX_MASTER_SERVERS][2];
  int i;
  int res;
  int netenabled;

  netenabled = qce->cvar.VariableIntegerValue ("net_enabled");

  // "dedicated 1" is for lan play, "dedicated 2" is for inet public play
  if ((sv_dedicated->integer != 2) || !(netenabled & (NET_ENABLEV4 | NET_ENABLEV6)))
    {
      return;		// only dedicated servers send heartbeats
    }

  // if not time yet, don't send anything
  if (svs.time < svs.nextHeartbeatTime)
    {
      return;
    }
  svs.nextHeartbeatTime = svs.time + HEARTBEAT_MSEC;

  // send to group masters
  for (i = 0; i < MAX_MASTER_SERVERS; i++)
    {
      if (!sv_master[i]->string[0])
	{
	  continue;
	}

      // see if we haven't already resolved the name
      // resolving usually causes hitches on win95, so only
      // do it when needed
      if (sv_master[i]->modified || ((adr[i][0].type == NA_BAD) && (adr[i][1].type == NA_BAD)))
	{
	  sv_master[i]->modified = false;

	  if (netenabled & NET_ENABLEV4)
	    {
	      SV_Printf ("Resolving %s (IPv4)\n", sv_master[i]->string);
	      res = qce->net.StringToAdr (sv_master[i]->string, &adr[i][0], NA_IP);

	      if (res == 2)
		{
		  // if no port was specified, use the default master port
		  adr[i][0].port = BigShort (PORT_MASTER);
		}

	      if (res)
		SV_Printf ("%s resolved to %s\n", sv_master[i]->string, qce->net.AdrToStringwPort(adr[i][0]));
	      else
		SV_Printf ("%s has no IPv4 address.\n", sv_master[i]->string);
	    }

	  if (netenabled & NET_ENABLEV6)
	    {
	      SV_Printf ("Resolving %s (IPv6)\n", sv_master[i]->string);
	      res = qce->net.StringToAdr (sv_master[i]->string, &adr[i][1], NA_IP6);

	      if (res == 2)
		{
		  // if no port was specified, use the default master port
		  adr[i][1].port = BigShort (PORT_MASTER);
		}

	      if (res)
		SV_Printf ("%s resolved to %s\n", sv_master[i]->string, qce->net.AdrToStringwPort(adr[i][1]));
	      else
		SV_Printf ("%s has no IPv6 address.\n", sv_master[i]->string);
	    }

	  if ((adr[i][0].type == NA_BAD) && (adr[i][1].type == NA_BAD))
	    {
	      // if the address failed to resolve, clear it
	      // so we don't take repeated dns hits
	      SV_Printf ("Couldn't resolve address: %s\n", sv_master[i]->string);
	      qce->cvar.SetNew (sv_master[i], "");
	      sv_master[i]->modified = false;
	      continue;
	    }
	}

      SV_Printf ("Sending heartbeat to %s\n", sv_master[i]->string);
      // this command should be changed if the server info / status format
      // ever incompatably changes
      if (adr[i][0].type != NA_BAD)
	qce->net.OutOfBandPrint (NS_SERVER, adr[i][0], "heartbeat %s\n", message);
      if (adr[i][1].type != NA_BAD)
	qce->net.OutOfBandPrint (NS_SERVER, adr[i][1], "heartbeat %s\n", message);
    }
}

/*
=================
SV_MasterShutdown

Informs all masters that this server is going down
=================
*/
void
SV_MasterShutdown (void)
{
  // send a hearbeat right now
  svs.nextHeartbeatTime = -9999;
  SV_MasterHeartbeat (sv_flatline->string);

  // send it again to minimize chance of drops
  svs.nextHeartbeatTime = -9999;
  SV_MasterHeartbeat (sv_flatline->string);

  // when the master tries to poll the server, it won't respond, so
  // it will be removed from the list
}

//===========================================================================
// Stream redirection (output)
//
// Useful for remote commands, remote rcon or remote queries on the server
//
//============================================================================

static char * rd_buffer;
static int    rd_buffersize;
static void   (*rd_flush)(char *buffer);

/*
=================
SV_BeginRedirect
=================
*/
void
SV_BeginRedirect (char *buffer, int buffersize, void (*flush)(char *))
{
  if (!buffer || !buffersize || !flush)
    return;
  rd_buffer = buffer;
  rd_buffersize = buffersize;
  rd_flush = flush;

  *rd_buffer = 0;
}

/*
=================
SV_EndRedirect
=================
*/
void
SV_EndRedirect (void)
{
  if (rd_flush)
    {
      rd_flush (rd_buffer);
    }

  rd_buffer = NULL;
  rd_buffersize = 0;
  rd_flush = NULL;
}

/*
================
SV_FlushRedirect

================
*/
static void
SV_FlushRedirect (char *outputbuf)
{
  qce->net.OutOfBandPrint (NS_SERVER, svs.redirectAddress, "print\n%s", outputbuf);
}

/*
==============================================================================

CONNECTIONLESS COMMANDS

==============================================================================
*/
struct leakyBucket_s
{
  netadrtype_t type;

  union
  {
    byte _4[4];
    byte _6[16];
  } ipv;

  int lastTime;
  signed char burst;

  long hash;

  struct leakyBucket_s *prev, *next;
};
typedef struct leakyBucket_s leakyBucket_t;

// This is deliberately quite large to make it more of an effort to DoS
#define MAX_BUCKETS 16384
#define MAX_HASHES 1024

static leakyBucket_t buckets [MAX_BUCKETS];
static leakyBucket_t *bucketHashes [MAX_HASHES];

/*
================
SVC_HashForAddress
================
*/
static long
SVC_HashForAddress (netadr_t address)
{
  byte *ip;
  size_t size;
  unsigned int i;
  long hash = 0;

  switch (address.type)
    {
    case NA_IP:
      ip = address.ip;
      size = 4;
      break;

    case NA_IP6:
      ip = address.ip6;
      size = 16;
      break;

    default:
      ip = NULL;
      size = 0;
      break;
    }

  for (i = 0; i < size; i++)
    {
      hash += (long)(ip[i]) * (i + 119);
    }

  hash = (hash ^ (hash >> 10) ^ (hash >> 20));
  hash &= (MAX_HASHES - 1);

  return hash;
}

/*
================
SVC_BucketForAddress

Find or allocate a bucket for an address
================
*/
static leakyBucket_t *
SVC_BucketForAddress (netadr_t address, int burst, int period)
{
  leakyBucket_t	*bucket = NULL;
  int i;
  long hash = SVC_HashForAddress (address);
  int now = Sys_Milliseconds ();

  for (bucket = bucketHashes[hash]; bucket; bucket = bucket->next)
    {
      switch (bucket->type)
	{
	case NA_IP:
	  if (memcmp (bucket->ipv._4, address.ip, 4) == 0)
	    {
	      return bucket;
	    }
	  break;

	case NA_IP6:
	  if (memcmp (bucket->ipv._6, address.ip6, 16) == 0)
	    {
	      return bucket;
	    }
	  break;

	default:
	  break;
	}
    }

  for (i = 0; i < MAX_BUCKETS; i++)
    {
      int interval;

      bucket = &buckets[i];
      interval = now - bucket->lastTime;

      // Reclaim expired buckets
      if (((bucket->lastTime > 0) && (interval > (burst * period)))
	  || (interval < 0))
	{
	  if (bucket->prev != NULL)
	    {
	      bucket->prev->next = bucket->next;
	    }
	  else
	    {
	      bucketHashes[bucket->hash] = bucket->next;
	    }

	  if (bucket->next != NULL)
	    {
	      bucket->next->prev = bucket->prev;
	    }

	  Com_Memset (bucket, 0, sizeof(leakyBucket_t));
	}

      if (bucket->type == NA_BAD)
	{
	  bucket->type = address.type;
	  switch (address.type)
	    {
	    case NA_IP:
	      Com_Memcpy (bucket->ipv._4, address.ip, 4);
	      break;

	    case NA_IP6:
	      Com_Memcpy (bucket->ipv._6, address.ip6, 16);
	      break;

	    default:
	      break;
	    }

	  bucket->lastTime = now;
	  bucket->burst = 0;
	  bucket->hash = hash;

	  // Add to the head of the relevant hash chain
	  bucket->next = bucketHashes[hash];
	  if (bucketHashes[hash] != NULL)
	    {
	      bucketHashes[hash]->prev = bucket;
	    }

	  bucket->prev = NULL;
	  bucketHashes[hash] = bucket;

	  return bucket;
	}
    }

  // Couldn't allocate a bucket for this address
  return NULL;
}

/*
================
SVC_RateLimit
================
*/
static bool
SVC_RateLimit (leakyBucket_t *bucket, int burst, int period)
{
  if (bucket != NULL)
    {
      int now = Sys_Milliseconds();
      int interval = now - bucket->lastTime;
      int expired = interval / period;
      int expiredRemainder = interval % period;

      // manage timer wrapping
      if ((expired > bucket->burst) || (expired < 0))
	{
	  bucket->burst = 0;
	  bucket->lastTime = now;
	}
      else
	{
	  bucket->burst -= expired;
	  bucket->lastTime = now - expiredRemainder;
	}

      if (bucket->burst < burst)
	{
	  bucket->burst++;

	  return false;
	}
    }

  return true;
}

/*
================
SVC_RateLimitAddress

Rate limit for a particular address
================
*/
static bool
SVC_RateLimitAddress (netadr_t from, int burst, int period)
{
  leakyBucket_t *bucket = SVC_BucketForAddress (from, burst, period);

  return SVC_RateLimit (bucket, burst, period);
}

/*
================
SVC_Status

Responds with all the info that qplug or qspy can see about the server
and all connected players.  Used for getting detailed information after
the simple info query.
================
*/
static void
SVC_Status (netadr_t from)
{
  int i;
  unsigned int statusLength;
  unsigned int playerLength;
  char status[MAX_MSGLEN];
  char player[1024];
  char infostring[MAX_INFO_STRING];
  client_t *cl;
  playerState_t	*ps;
  static leakyBucket_t bucket;

  // A maximum challenge length of 128 should be more than plenty.
  if (strlen (qce->cmd.Argv(1)) > 128)
    return;

  // Prevent using getstatus as an amplifier
  if (SVC_RateLimitAddress (from, 10, 1000))
    {
      SV_DPrintf ("SVC_Status: rate limit from %s exceeded, dropping request\n",
		   qce->net.AdrToString (from));
      return;
    }

  // Allow getstatus to be DoSed relatively easily, but prevent
  // excess outbound bandwidth usage when being flooded inbound
  if (SVC_RateLimit (&bucket, 10, 100))
    {
      SV_DPrintf ("SVC_Status: rate limit exceeded, dropping request\n");
      return;
    }

  if (!sv_running->integer)
    return;

  strcpy (infostring, qce->cvar.InfoString (CVAR_SERVERINFO));

  // echo back the parameter to status. so master servers can use it as a challenge
  // to prevent timed spoofed reply packets that add ghost servers
  Info_SetValueForKey (infostring, "challenge", qce->cmd.Argv (1));

  status[0] = 0;
  statusLength = 0;

  for (i = 0; i < sv_maxclients->integer; i++)
    {
      cl = &svs.clients[i];
      if (cl->state >= CS_CONNECTED)
	{
	  ps = SV_GameClientNum (i);

	  Com_sprintf (player, sizeof(player), "%i %i \"%s\"\n",
		       ps->persistant[PERS_SCORE], cl->ping, cl->name);

	  playerLength = strlen (player);
	  if ((statusLength + playerLength) >= sizeof(status))
	    {
	      break;		// can't hold any more
	    }

	  strcpy (status + statusLength, player);
	  statusLength += playerLength;
	}
    }

  qce->net.OutOfBandPrint (NS_SERVER, from, "statusResponse\n%s\n%s", infostring, status);
}

/*
================
SVC_Info

Responds with a short info message that should be enough to determine
if a user is interested in a server to do a full status
================
*/
void
SVC_Info (netadr_t from)
{
  int i, count, human;
  char *gamedir;
  char infostring[MAX_INFO_STRING];
  static leakyBucket_t bucket;
  char *msg;

  /*
   * Check whether Cmd_Argv(1) has a sane length. This was not done in the original Quake3 version which led
   * to the Infostring bug discovered by Luigi Auriemma. See http://aluigi.altervista.org/ for the advisory.
   */

  // A maximum challenge length of 128 should be more than plenty.
  if (strlen (qce->cmd.Argv(1)) > 128)
    return;

  // Prevent using getinfo as an amplifier
  if (SVC_RateLimitAddress (from, 10, 1000))
    {
      SV_DPrintf ("SVC_Info: rate limit from %s exceeded, dropping request\n",
		  qce->net.AdrToString (from));
      return;
    }

  // Allow getstatus to be DoSed relatively easily, but prevent
  // excess outbound bandwidth usage when being flooded inbound
  if (SVC_RateLimit (&bucket, 10, 100))
    {
      SV_DPrintf ("SVC_Info: rate limit exceeded, dropping request\n");
      return;
    }

  // if the server is sleeping still provide a few infos
  if (!sv_running->integer)
    {
      Info_SetValueForKey (infostring, "protocol", va("%i", PROTOCOL_VERSION));
      Info_SetValueForKey (infostring, "hostname", sv_hostname->string);

      Info_SetValueForKey (infostring, "sv_running", va("%i", sv_running->integer));
      Info_SetValueForKey (infostring, "sv_autoWakeUp", va("%i", sv_autoWakeUp->integer));

      qce->net.OutOfBandPrint (NS_SERVER, from, "infoResponse\n%s", infostring);
      return;
    }

  // don't count privateclients
  count = human = 0;
  for (i = sv_privateClients->integer; i < sv_maxclients->integer; i++)
    {
      if (svs.clients[i].state >= CS_CONNECTED)
	{
	  count++;
	  if (svs.clients[i].netchan.remoteAddress.type != NA_BOT)
	    {
	      human++;
	    }
	}
    }

  infostring[0] = 0;

  // echo back the parameter to status. so servers can use it as a challenge
  // to prevent timed spoofed reply packets that add ghost servers
  Info_SetValueForKey (infostring, "challenge", qce->cmd.Argv(1));

  Info_SetValueForKey (infostring, "protocol", va("%i", PROTOCOL_VERSION));
  Info_SetValueForKey (infostring, "hostname", sv_hostname->string);
  Info_SetValueForKey (infostring, "mapname", sv_mapname->string);
  Info_SetValueForKey (infostring, "clients", va("%i", count));
  msg = va ("%i", sv_maxclients->integer - sv_privateClients->integer);
  Info_SetValueForKey (infostring, "sv_maxclients", msg);

  // FIXME : move this to game
  //	     make a gameinfo function returning infos from game mod
  msg = va ("%i", qce->cvar.VariableIntegerValue ("g_gametype"));
  Info_SetValueForKey (infostring, "g_gametype", msg);
  msg = va ("%d", qce->cvar.VariableIntegerValue ("g_needpass"));
  Info_SetValueForKey (infostring, "g_needpass", msg);
  Info_SetValueForKey (infostring, "g_humanplayers", va ("%d", human));

  if (sv_minPing->integer)
    {
      Info_SetValueForKey (infostring, "minPing", va("%i", sv_minPing->integer));
    }
  if (sv_maxPing->integer)
    {
      Info_SetValueForKey (infostring, "maxPing", va("%i", sv_maxPing->integer));
    }

  gamedir = qce->cvar.VariableString ("fs_game");
  if (*gamedir)
    {
      Info_SetValueForKey (infostring, "game", gamedir);
    }

  qce->net.OutOfBandPrint (NS_SERVER, from, "infoResponse\n%s", infostring);
}

/*
===============
SVC_RemoteCommand

An rcon packet arrived from the network.
Shift down the remaining args
Redirect all printfs
===============
*/
#define SV_OUTPUTBUF_LENGTH (1024 - 16)
static void
SVC_RemoteCommand (netadr_t from, msg_t *msg)
{
  bool valid;
  char remaining[1024];
  // TTimo - scaled down to accumulate, but not overflow anything network wise, print wise etc.
  // (OOB messages are the bottleneck here)
  char sv_outputbuf[SV_OUTPUTBUF_LENGTH];
  char * cmd_aux;

  // Prevent using rcon as an amplifier and make dictionary attacks impractical
  if (SVC_RateLimitAddress (from, 10, 1000))
    {
      SV_DPrintf ("SVC_RemoteCommand: rate limit from %s exceeded, dropping request\n",
		  qce->net.AdrToString (from));
      return;
    }

  if (!strlen (sv_rconPassword->string)
      || strcmp (qce->cmd.Argv(1), sv_rconPassword->string))
    {
      static leakyBucket_t bucket;

      // Make DoS via rcon impractical
      if (SVC_RateLimit (&bucket, 10, 1000))
	{
	  SV_DPrintf ("SVC_RemoteCommand: rate limit exceeded, dropping request\n");
	  return;
	}

      valid = false;
      SV_Printf ("Bad rcon from %s: %s\n", qce->net.AdrToString (from),
		 qce->cmd.ArgsFrom(2));
    }
  else
    {
      valid = true;
      SV_Printf ("Rcon from %s: %s\n", qce->net.AdrToString (from),
		  qce->cmd.ArgsFrom(2));
    }

  // start redirecting all print outputs to the packet
  svs.redirectAddress = from;
  SV_BeginRedirect (sv_outputbuf, SV_OUTPUTBUF_LENGTH, SV_FlushRedirect);

  if (!strlen (sv_rconPassword->string))
    {
      SV_Printf ("No rconpassword set on the server.\n");
    }
  else if (!valid)
    {
      SV_Printf ("Bad rconpassword.\n");
    }
  else
    {
      remaining[0] = 0;

      // get the command directly, "rcon <pass> <command>" to avoid quoting issues
      // extract the command by walking
      // since the cmd formatting can fuckup (amount of spaces), using a dumb step by step parsing
      cmd_aux = qce->cmd.Cmd ();
      cmd_aux += 4;
      while (cmd_aux[0]==' ')
	cmd_aux++;
      while (cmd_aux[0] && cmd_aux[0]!=' ') // password
	cmd_aux++;
      while (cmd_aux[0]==' ')
	cmd_aux++;

      Q_strcat (remaining, sizeof(remaining), cmd_aux);

      qce->cmd.ExecuteString (remaining);
    }
  SV_EndRedirect ();
}

/*
=================
SV_ConnectionlessPacket

A connectionless packet has four leading 0xff
characters to distinguish it from a game channel.
Clients that are in the game can still send
connectionless packets.
=================
*/
#define WAKE_DEFAULT_MAP "ztn3tourney1"
static void
SV_ConnectionlessPacket (netadr_t from, msg_t *msg)
{
  char *s;
  char *c;

  qce->msg.BeginReadingOOB (msg);
  qce->msg.ReadLong (msg);		// skip the -1 marker

  if (!Q_strncmp ("connect", (char *) &msg->data[4], 7))
    {
      // if the server is sleeping; a player connects and wake is allowed proceed
      if (!sv_running->integer && sv_autoWakeUp->integer)
	{
	  char expanded[MAX_QPATH];

	  if (*sv_nextmap->string)
	    {
	      // make sure the level exists before trying to change, so that
	      // a typo at the server console won't end the game
	      Com_sprintf (expanded, sizeof(expanded), "maps/%s.bsp", sv_nextmap->string);
	      if (qce->fs.ReadFile (expanded, NULL) == -1)
		{
		  SV_Printf ("Can't find map %s. Using default\n", expanded);
		  Q_strncpyz (expanded, WAKE_DEFAULT_MAP, sizeof (expanded));
		}
	      else
		{
		  Q_strncpyz (expanded, sv_nextmap->string, sizeof (expanded));
		}
	    }
	  else
	    {
	      Q_strncpyz (expanded, WAKE_DEFAULT_MAP, sizeof (expanded));
	    }

	  SV_SpawnServer (expanded, true);
	}

      qce->Huff_Decompress (msg, 12);
    }

  s = qce->msg.ReadStringLine (msg);
  qce->cmd.TokenizeString (s);

  c = qce->cmd.Argv (0);
  SV_DPrintf ("SV packet %s : %s\n", qce->net.AdrToString(from), c);

  if (!Q_stricmp (c, "getstatus"))
    {
      SVC_Status (from);
    }
  else if (!Q_stricmp (c, "getinfo"))
    {
      SVC_Info (from);
    }
  else if (!Q_stricmp (c, "getchallenge"))
    {
      SV_GetChallenge (from);
    }
  else if (!Q_stricmp (c, "connect"))
    {
      SV_DirectConnect (from);
    }
  else if (!Q_stricmp (c, "rcon"))
    {
      // don't sleep when configuring remotely
      sv_lastActivePacketTime = Sys_Milliseconds ();

      SVC_RemoteCommand (from, msg);
    }
  else if (!Q_stricmp (c, "disconnect"))
    {
      // if a client starts up a local server, we may see some spurious
      // server disconnect messages when their new server sees our final
      // sequenced messages to the old client
    }
  else
    {
      SV_DPrintf ("bad connectionless packet from %s:\n%s\n",
		   qce->net.AdrToString (from), s);
    }
}

//============================================================================
/*
=================
SV_PacketEvent
=================
*/
void
SV_PacketEvent (netadr_t from, msg_t *msg)
{
  int i;
  int qport;
  client_t * cl;

  // check for connectionless packet (0xffffffff) first
  if ((msg->cursize >= 4) && (*(int *)msg->data == -1))
    {
      SV_ConnectionlessPacket (from, msg);
      return;
    }

  // we don't run a game, we shouldn't receive regular usercmds etc...
  if (!sv_running->integer)
    {
      return;
    }

  // read the qport out of the message so we can fix up
  // stupid address translating routers
  qce->msg.BeginReadingOOB (msg);
  qce->msg.ReadLong (msg);		// sequence number
  qport = qce->msg.ReadShort (msg) & 0xffff;

  // find which client the message is from
  for (i = 0, cl = svs.clients; i < sv_maxclients->integer; i++, cl++)
    {
      if (cl->state == CS_FREE)
	{
	  continue;
	}
      if (!qce->net.CompareBaseAdrMask (from, cl->netchan.remoteAddress, -1))
	{
	  continue;
	}
      // it is possible to have multiple clients from a single IP
      // address, so they are differentiated by the qport variable
      if (cl->netchan.qport != qport)
	{
	  continue;
	}

      // the IP port can't be used to differentiate them, because
      // some address translating routers periodically change UDP
      // port assignments
      if (cl->netchan.remoteAddress.port != from.port)
	{
	  SV_Printf ("SV_PacketEvent: fixing up a translated port\n");
	  cl->netchan.remoteAddress.port = from.port;
	}

      // make sure it is a valid, in sequence packet
      if (SV_Netchan_Process (cl, msg))
	{
	  // zombie clients still need to do the Netchan_Process
	  // to make sure they don't need to retransmit the final
	  // reliable message, but they don't do any other processing
	  if (cl->state != CS_ZOMBIE)
	    {
	      // we have a regular gameplay packet
	      sv_lastActivePacketTime = Sys_Milliseconds ();

	      cl->lastPacketTime = svs.time;	// don't timeout
	      SV_ExecuteClientMessage (cl, msg);
	    }
	}
      return;
    }

  // if we received a sequenced packet from an address we don't recognize,
  // send an out of band disconnect packet to it
  qce->net.OutOfBandPrint (NS_SERVER, from, "disconnect unknown address");
}

/*
===================
SV_UpdateInfoStrings
===================
*/
static void
SV_UpdateInfoStrings (void)
{
  int mflags;

  // update infostrings if anything has been changed
  mflags = qce->cvar.Get_ModifiedFlags ();
  if (mflags & CVAR_SERVERINFO)
    {
      SV_SetConfigstring (CS_SERVERINFO, qce->cvar.InfoString(CVAR_SERVERINFO));
      mflags &= ~CVAR_SERVERINFO;
      qce->cvar.Set_ModifiedFlags (mflags);
    }
  if (mflags & CVAR_SYSTEMINFO)
    {
      SV_SetConfigstring (CS_SYSTEMINFO, qce->cvar.InfoString_Big(CVAR_SYSTEMINFO));
      mflags &= ~CVAR_SYSTEMINFO;
      qce->cvar.Set_ModifiedFlags (mflags);
    }
}

/*
===================
SV_CalcPings

Updates the cl->ping variables
===================
*/
static void
SV_CalcPings (void)
{
  int i, j;
  int total, count;
  int delta;
  client_t * cl;
  playerState_t * ps;

  for (i = 0; i < sv_maxclients->integer; i++)
    {
      cl = &svs.clients[i];
      if (cl->state != CS_ACTIVE)
	{
	  cl->ping = 999;
	  continue;
	}
      if (!cl->gentity)
	{
	  cl->ping = 999;
	  continue;
	}
      if (cl->gentity->r.svFlags & SVF_BOT)
	{
	  cl->ping = 0;
	  continue;
	}

      total = 0;
      count = 0;
      for (j = 0; j < PACKET_BACKUP; j++)
	{
	  if (cl->frames[j].messageAcked <= 0)
	    {
	      continue;
	    }
	  delta = cl->frames[j].messageAcked - cl->frames[j].messageSent;
	  count++;
	  total += delta;
	}
      if (!count)
	{
	  cl->ping = 999;
	}
      else
	{
	  cl->ping = total/count;
	  if (cl->ping > 999)
	    {
	      cl->ping = 999;
	    }
	}

      // let the game dll know about the ping
      ps = SV_GameClientNum (i);
      ps->ping = cl->ping;
    }
}

/*
==================
SV_CheckTimeouts

If a packet has not been received from a client for timeout->integer
seconds, drop the conneciton.  Server time is used instead of
realtime to avoid dropping the local client while debugging.

When a client is normally dropped, the client_t goes into a zombie state
for a few seconds to make sure any final reliable message gets resent
if necessary
==================
*/
static void
SV_CheckTimeouts (void)
{
  int i;
  client_t *cl;
  int droppoint;
  int zombiepoint;

  droppoint = svs.time - 1000 * sv_timeout->integer;
  zombiepoint = svs.time - 1000 * sv_zombietime->integer;

  for (i = 0, cl = svs.clients; i < sv_maxclients->integer; i++,cl++)
    {
      // message times may be wrong across a changelevel
      if (cl->lastPacketTime > svs.time)
	{
	  cl->lastPacketTime = svs.time;
	}

      if ((cl->state == CS_ZOMBIE) && (cl->lastPacketTime < zombiepoint))
	{
	  // using the client id because the cl->name is empty at this point
	  SV_DPrintf ("Going from CS_ZOMBIE to CS_FREE for client %d\n", i);
	  cl->state = CS_FREE;	// can now be reused
	  continue;
	}
      if ((cl->state >= CS_CONNECTED) && (cl->lastPacketTime < droppoint))
	{
	  // wait several frames so a debugger session doesn't
	  // cause a timeout
	  if (++cl->timeoutCount > 5)
	    {
	      SV_DropClient (cl, "timed out");
	      cl->state = CS_FREE;	// don't bother with zombie state
	    }
	}
      else
	{
	  cl->timeoutCount = 0;
	}
    }
}

/*
==================
SV_CheckTimeouts

If the server is empty and it didn't receive
game related packets for sv_sleepDelay->integer seconds
shutdown the game and go to sleep
==================
*/
static void
SV_CheckServerTimeout (void)
{
  int i;
  client_t *cl;

  for (i = 0, cl = svs.clients; i < sv_maxclients->integer; i++,cl++)
    {
      if (cl->state != CS_FREE)
	{
	  break;
	}
    }

  if ((i == sv_maxclients->integer) && (sv_sleepDelay->integer)
      && ((Sys_Milliseconds () - sv_lastActivePacketTime) > sv_sleepDelay->integer * 1000))
    {
      SV_Shutdown ("killserver due to inactivity");
    }
}

/*
=================
Com_RunAndTimeServerPacket
=================
*/
void
Com_RunAndTimeServerPacket (netadr_t *evFrom, msg_t *buf)
{
#ifdef SPEEDS
  int t1, t2, msec;

  if (sv_speeds->integer)
    {
      t1 = Sys_Milliseconds ();
    }
#endif

  SV_PacketEvent (*evFrom, buf);

#ifdef SPEEDS
  if (sv_speeds->integer)
    {
      t2 = Sys_Milliseconds ();
      msec = t2 - t1;

      if (sv_speeds->integer == 3)
	{
	  SV_Printf ("SV_PacketEvent time: %i\n", msec);
	}
    }
#endif
}

/*
=================
SV_EventLoop

Returns last event time
=================
*/
int
SV_EventLoop (void)
{
  sysEvent_t ev;
  netadr_t evFrom;
  msg_t buf;
  byte bufData[MAX_MSGLEN];

  qce->msg.Init (&buf, bufData, sizeof (bufData));

  while (1)
    {
      qce->net.FlushPacketQueue ();

      // get an event
      ev = qce->event.GetEvent ();

      // if no more events are available
      if (ev.evType == SE_NONE)
	{
	  return ev.evTime;
	}

      switch (ev.evType)
	{
	default:
	  SV_Error (ERR_FATAL, "SV_EventLoop: bad event type %i", ev.evType);
	  break;
	  /*
	case SE_NONE:
	  break;
	  */
	  /*
	case SE_CHAR:
	  CL_CharEvent (ev.evValue);
	  break;
	  */
	case SE_CONSOLE:
	  qce->cmd.Cbuf_AddText ((char *)ev.evPtr);
	  qce->cmd.Cbuf_AddText ("\n");

	  // don't sleep when typing on tty
	  sv_lastActivePacketTime = ev.evTime;
	  break;
	case SE_PACKET:
	  // this cvar allows simulation of connections that
	  // drop a lot of packets.  Note that loopback connections
	  // don't go through here at all.
	  if (sv_dropsim->value > 0)
	    {
	      static int seed;

	      if (Q_random (&seed) < sv_dropsim->value)
		{
		  break;	// drop this packet
		}
	    }

	  evFrom = *(netadr_t *)ev.evPtr;
	  buf.cursize = ev.evPtrLength - sizeof (evFrom);

	  // we must copy the contents of the message out, because
	  // the event buffers are only large enough to hold the
	  // exact payload, but channel messages need to be large
	  // enough to hold fragment reassembly
	  if (buf.cursize > buf.maxsize)
	    {
	      SV_Printf ("SV_EventLoop: oversize packet\n");
	      continue;
	    }
	  Com_Memcpy (buf.data, (byte *)((netadr_t *)ev.evPtr + 1), buf.cursize);

	  if (sv_running->integer || (!sv_running->integer && sv_autoWakeUp->integer))
	    {
	      Com_RunAndTimeServerPacket (&evFrom, &buf);
	    }
	  break;
	}
      // free any block data
      qce->event.FreeEvent (&ev);
    }

  // never reached
  SV_Error (ERR_FATAL, "SV_EventLoop ()\n");
  return 0;
}

/*
================
SV_ModifyMsec
================
*/
int
SV_ModifyMsec (int msec)
{
  int clampTime;

  //
  // modify time for debugging values
  //
  if (sv_fixedtime->integer)
    {
      msec = sv_fixedtime->integer;
    }
  else if (sv_timescale->value)
    {
      msec *= sv_timescale->value;
    }

  // don't let it scale below 1 msec
  /*
  if (msec < 1 && sv_timescale->value)
    {
      SV_Printf ("msec was too small, adjusting to 1\n");
      msec = 1;
    }
  */

  // dedicated servers don't want to clamp for a much longer
  // period, because it would mess up all the client's views
  // of time.
  if (sv_running->integer && (msec > 500))
    SV_Printf ("Hitch warning: %i msec frame time\n", msec);

  clampTime = 500;

  if (msec > clampTime)
    {
      msec = clampTime;
    }

  return msec;
}

/*
=================
SV_MinMsec

amount of time we would like to advance this frame
starting from the stacked time and output a delta
to match the fixed framerate
there might be time errors when adding deltas
if the server receives a lot of packets
so also check we aren't too far from the last complete gameframe
=================
*/

// NOTE : we could add modifier depending on some conditions
int
SV_MinMsec (int diff)
{
  int msec;
  int delta;

  msec = (int) ceilf (1000.0 / sv_fps->value);

  delta = (msec - diff);

  if (msec < 0)
    {
      SV_Printf ("Negative msec ! Fix maxfps !\n");
      delta = 0;
    }

  return delta;
}

/*
==================
SV_Frame

Player movement occurs as a result of packet events, which
happen before SV_Frame is called

Structure :

1: read & update inputs (local cmds / user cmds)
check for sleeping a bit of time
break the sleep (by returning to 1:) to handle new inputs if needed
update time stack

2: enought time elapsed (on the stack)
make some check for the server to be in a good state
3: run the game simulation
4: send new snapshots to clients
==================
*/
void
SV_Frame (/*int msec*/)
{
  int frameMsec;

  int stepMsec, diff;
  int msec, minMsec;

  static int lastTime = 0; // keep track of the previous sv_frametime
  // keep track of the last completely handled frame
  static int lastHandledTime = 0;

#ifdef SPEEDS
  // sv_speeds times
  static int time_game;

  int startTime;
  int timeBeforeFirstEvents;
  int timeBeforeServer;
  int timeBeforeEvents;
  int timeBeforeClient;
  int timeAfter;

  timeBeforeFirstEvents =0;
  timeBeforeServer =0;
  timeBeforeEvents =0;
  timeBeforeClient = 0;
  timeAfter = 0;
#endif

  // imported from com_frame
  if (setjmp (abortframe))
    {
      return;		// an ERR_DROP was thrown
    }

  //
  // main event loop
  //
#ifdef SPEEDS
  if (sv_speeds->integer)
    {
      timeBeforeFirstEvents = Sys_Milliseconds ();
    }
#endif

  // grab and run latest events (client move, connection etc...)
  lastTime = sv_frametime;
  sv_frametime = SV_EventLoop ();

  msec = sv_frametime - lastTime;

  // execute command buffer
  qce->cmd.Cbuf_Execute ();

  //
  // server side
  //
#ifdef SPEEDS
  if (sv_speeds->integer)
    {
      timeBeforeServer = Sys_Milliseconds ();
    }
#endif

  // the menu kills the server with this cvar
  if (sv_killserver->integer)
    {
      SV_Shutdown ("Server was killed");
      qce->cvar.SetNew (sv_killserver, "0");
      return;
    }

  if (!sv_running->integer)
    {
      // Running as a server, but no map loaded
      // Block until something interesting happens
      if (sv_autoWakeUp->integer)
	qce->net.Sleep3 (-1, Sys_InputFD ());
      else
	Sys_Sleep (-1);

      return;
    }

  // the server is running here, com_sv_running->integer = 1

  // if it isn't time for the next frame, do nothing
  if (sv_fps->integer < 1)
    {
      qce->cvar.SetNew (sv_fps, "10");
    }

  // timestep we want the simulation to advance
  frameMsec = (int) ceil (1000.0 / sv_fps->value * sv_timescale->value);
  // don't let it scale below 1ms
  if (frameMsec < 1)
    {
      float time;

      time = floorf (sv_fps->value / 1000.0f);
      qce->cvar.SetNew (sv_timescale, va ("%f", time));
      frameMsec = 1;
    }

  msec = SV_ModifyMsec (msec);

  sv.timeResidual += msec;

  diff = (sv_frametime - lastHandledTime);
  minMsec = SV_MinMsec (diff);

  if (minMsec > 0)
    {
      qce->net.Sleep (minMsec);
      return;
    }

  // -----------------------------------------------------
  // we will now try to simulate a game frame

  // if time is about to hit the 32nd bit, kick all clients
  // and clear sv.time, rather
  // than checking for negative time wraparound everywhere.
  // 2giga-milliseconds = 23 days, so it won't be too often
  if (svs.time > 0x70000000)
    {
      SV_Shutdown ("Restarting server due to time wrapping");
      qce->cmd.Cbuf_AddText (va("map %s\n", sv_mapname->string));
      return;
    }

  // this can happen considerably earlier when lots of clients play and the map doesn't change
  if (svs.nextSnapshotEntities >= (0x7FFFFFFE - svs.numSnapshotEntities))
    {
      SV_Shutdown ("Restarting server due to numSnapshotEntities wrapping");
      qce->cmd.Cbuf_AddText (va("map %s\n", sv_mapname->string));
      return;
    }

  if (sv.restartTime && (sv.time >= sv.restartTime))
    {
      sv.restartTime = 0;
      qce->cmd.Cbuf_AddText ("map_restart 0\n");
      return;
    }

#ifdef SPEEDS
  if (sv_speeds->integer)
    {
      startTime = Sys_Milliseconds ();
    }
#endif

  lastHandledTime = sv_frametime;

  // update infostrings if anything has been changed
  SV_UpdateInfoStrings ();

  // update ping based on the all received frames
  SV_CalcPings ();

  // run bots
  SV_BotFrame (sv.time);

  // NOTE : try to calculate the frame in one pass
  // without accumulating time
  // timeResidual should become useless
  // as it's not really accurate btw.
  // ATM the looping code is a bit hybrid (ugly ?)

  // run the game simulation in chunks
  while (sv.timeResidual >= frameMsec)
    {
      // diff can be in the range [frameMsec; 2 * frameMsec]
      // otherwise use frameMsec as stepping time
      if (diff >= (2 * frameMsec))
	stepMsec = frameMsec;
      else if (diff < frameMsec)
	stepMsec = frameMsec;
      else
	stepMsec = diff;

      diff -= stepMsec;

      sv.timeResidual -= stepMsec;
      svs.time += stepMsec;
      sv.time += stepMsec;

      // let everything in the world think and move
      glibe->run_frame (sv.time);

      // once we have update the simulation, let the server know its new time
      sv.simulationTime = glibe->game_simulation_time ();
    }

#ifdef SPEEDS
  if (sv_speeds->integer)
    {
      time_game = Sys_Milliseconds () - startTime;
    }
#endif

  // check timeouts
  SV_CheckTimeouts ();

  // send messages back to the clients
  SV_SendClientMessages ();

  // send a heartbeat to the master if needed
  SV_MasterHeartbeat (sv_heartbeat->string);

#ifdef SPEEDS
  if (sv_speeds->integer)
    {
      timeAfter = Sys_Milliseconds ();
    }
#endif

#ifdef SPEEDS
  //
  // report timing information
  //
  if (sv_speeds->integer)
    {
      int	all, sv, ev, cl;

      all = timeAfter - timeBeforeServer;
      sv = timeBeforeServer - timeBeforeFirstEvents;
      sv -= time_game;

      SV_Printf ("frame:%i all:%3i sv:%3i gm:%3i\n",
		  sv_frameNumber, all, sv, time_game);
    }
#endif
  //
  // trace optimization tracking
  //
  /*
  if (sv_showtrace->integer)
    {

      extern	int	c_traces, c_brush_traces, c_patch_traces;
      extern	int	c_pointcontents;

      SV_Printf ("%4i traces  (%ib %ip) %4i points\n", c_traces,
		  c_brush_traces, c_patch_traces, c_pointcontents);
      c_traces = 0;
      c_brush_traces = 0;
      c_patch_traces = 0;
      c_pointcontents = 0;
    }
  */

  // increase the frame counter
  sv_frameNumber++;

  // we don't want any further frame if there is no activity on the serv
  // put this at the begin of sv_frame () ?
  SV_CheckServerTimeout ();
}

//============================================================================

jmp_buf *
SV_GetJmpFrame (void)
{
  return &abortframe;
}

pg_status
SV_ProgramStatus (void)
{
  if (sv_running && sv_running->integer)
    return running;
  else
    return sleeping;
}

bool
SV_QcomStatus (void)
{
  return sv_qcomLoaded;
}

/*
============
SV_FillQcomLibAPI
============
*/
void
SV_FillQcomLibAPI (qcom_import_t * qcimp)
{
  qcimp->shutdown = SV_Shutdown;
  qcimp->gamecommand = SV_GameCommand; // used by Cmd_ExecuteString ();
  qcimp->get_jmpframe = SV_GetJmpFrame;
  qcimp->get_program_status = SV_ProgramStatus;

  qcimp->Printf = SV_Printf;
  qcimp->DPrintf = SV_DPrintf;
  qcimp->Error = SV_Error;

  qcimp->Sys_DefaultHomePath = Sys_DefaultHomePath;
  qcimp->Sys_DefaultInstallPath = Sys_DefaultInstallPath;
  qcimp->Sys_DefaultAppPath = Sys_DefaultAppPath;

  qcimp->Sys_Milliseconds = Sys_Milliseconds;
  qcimp->Sys_RandomBytes = Sys_RandomBytes;

  qcimp->Sys_Mkdir = Sys_Mkdir;
  qcimp->Sys_Mkfifo = Sys_Mkfifo;

  qcimp->Sys_ConsoleInput = Sys_ConsoleInput;

  qcimp->Sys_ListFiles = Sys_ListFiles;
  qcimp->Sys_FreeFileList = Sys_FreeFileList;

  qcimp->Sys_SetEnv = Sys_SetEnv;
}

/*
=============
SV_LoadCommonLib
=============
*/
void
SV_LoadCommonLib (void)
{
  void *Hdl;
  qcom_export_t *(*QcomAPI) (qcom_import_t * qimp, int isdedicated);

  // free the structure
  Com_Memset (&qclib, 0, sizeof (qclib));

  // load the dynamic library
  qclib.dynHandle = Sys_DLLoadType ("libq3common", qclib.fqpath, CLIENT);

  // resolve export/import structures
  Hdl = qclib.dynHandle;

  QcomAPI = Sys_DLLoadFunction (Hdl, "CommonAPI", true);

  SV_FillQcomLibAPI (&qimp);

  qce = QcomAPI (&qimp, QCFLAG_DEDICATED);

  // stop running the programm and send a proper error message
  if (qce == NULL)
    {
      SV_Error (ERR_FATAL, "SV_LoadCommonLib (): failed to get a valid qce pointer\n");
    }
}

/*
=================
main
=================
*/
int
main (int argc, char **argv)
{
  int i;
  char commandLine[MAX_STRING_CHARS] = { 0 };

  sys_import_t sysimp;

  sysimp.print = SV_Printf;
  sysimp.dprint = SV_DPrintf;
  sysimp.error = SV_Fatal_Error;
  sysimp.shutdown = SV_Shutdown;
  sysimp.isQcomLoaded = SV_QcomStatus;

  Sys_Set_Interface (&sysimp, false);

  // setup the print/error functions for qshared
  q_shared_import_t q_shared_imp;
  q_shared_imp.print = SV_Printf;
  q_shared_imp.error = SV_Fatal_Error;

  Q_Set_Interface (&q_shared_imp);

  Sys_PlatformInit ();

  // Set the initial time base
  Sys_Milliseconds ();

  // check for some arguments and set binary path
  Sys_ArgsInit (argc, argv);

  // Concatenate the command line for passing to Com_Init
  for (i = 1; i < argc; i++)
    {
      const bool containsSpaces = strchr (argv[i], ' ') != NULL;
      if (containsSpaces)
	Q_strcat (commandLine, sizeof (commandLine), "\"");

      Q_strcat (commandLine, sizeof(commandLine), argv[i]);

      if (containsSpaces)
	Q_strcat (commandLine, sizeof (commandLine), "\"");

      Q_strcat (commandLine, sizeof(commandLine), " ");
    }

  // load qcommon module
  SV_LoadCommonLib ();

  qce->Com_Init (commandLine);

  // using qce->... functions is valid from now
  sv_qcomLoaded = true;

  // not sure whether i can put this one late
  // set com_frameTime so that if a map is started on the
  // command line it will still be able to count on com_frameTime
  // being random enough for a serverid
  sv_frametime = qce->event.Milliseconds ();

  SV_Init ();
  // non critical cvar about system (better here than in qcommon)
  Sys_Init ();

  if (Sys_WritePIDFile ())
    {
      const char *message = "The last time " CLIENT_WINDOW_TITLE " ran, "
	"it didn't exit properly. This may be due to inappropriate video "
	"settings.";

      Sys_Error ("%s", message);
    }

  qce->net.Init ();
  // grab the terminal so we can properly output/input
  CON_Init ();

  // initialize packet time
  sv_lastActivePacketTime = Sys_Milliseconds ();

  while (1)
    {
      // start regular work
      SV_Frame ();
    }
  return 0;
}

/*
=============
SV_Quit_f

Both client and server can use this, and it will
do the apropriate things.
=============
*/
void
SV_Quit_f (void)
{
  // don't try to shutdown if we are in a recursive error
  char *p = qce->cmd.Args ();
  if (!sv_errorEntered)
    {
      SV_Shutdown (p[0] ? p : "Server quit");
      SV_BotShutdownBotLib ();
      qce->Com_Shutdown ();
      qce->fs.Shutdown (true);
    }
  Sys_Quit ();
}

/*
=============
SV_Printf

Both client and server can use this, and it will output
to the apropriate place.

A raw string should NEVER be passed as fmt, because of "%f" type crashers.
=============
*/
void
SV_Printf (const char *fmt, ...)
{
  va_list argptr;
  char msg[MAXPRINTMSG];

  va_start (argptr, fmt);
  Q_vsnprintf (msg, sizeof(msg), fmt, argptr);
  va_end (argptr);

  if (rd_buffer)
    {
      if ((strlen (msg) + strlen (rd_buffer)) > (rd_buffersize - 1))
	{
	  rd_flush (rd_buffer);
	  *rd_buffer = 0;
	}
      Q_strcat (rd_buffer, rd_buffersize, msg);
      // TTimo nooo .. that would defeat the purpose
      //rd_flush(rd_buffer);
      //*rd_buffer = 0;
      return;
    }

  // echo to dedicated console and terminal
  Sys_Print (msg);

  // archive in a log file if lib is loaded (no early print)
  if (sv_qcomLoaded)
    qce->Com_PrintLog (msg);
}

/*
================
SV_DPrintf

A SV_Printf that only shows up if the "developer" cvar is set
================
*/
void
SV_DPrintf (const char *fmt, ...)
{
  va_list argptr;
  char msg[MAXPRINTMSG];

  if (!sv_developer || !sv_developer->integer)
    {
      return;		// don't confuse non-developers with techie stuff...
    }

  va_start (argptr,fmt);
  Q_vsnprintf (msg, sizeof(msg), fmt, argptr);
  va_end (argptr);

  SV_Printf ("%s", msg);
}

/*
=============
SV_Error

Both client and server can use this, and it will
do the apropriate things.
=============
*/
void
SV_Error (int code, const char *fmt, ...)
{
  va_list argptr;
  static int lastErrorTime;
  static int errorCount;
  int currentTime;

  // when we are running automated scripts, make sure we
  // know if anything failed
  if (sv_buildscript && sv_buildscript->integer)
    {
      code = ERR_FATAL;
    }

  // if we are getting a solid stream of ERR_DROP, do an ERR_FATAL
  currentTime = Sys_Milliseconds ();
  if ((currentTime - lastErrorTime) < 100)
    {
      if (++errorCount > 3)
	{
	  code = ERR_FATAL;
	}
    }
  else
    {
      errorCount = 0;
    }
  lastErrorTime = currentTime;

  if (sv_errorEntered)
    {
      Sys_Error ("recursive error after: %s", sv_qcomErrorMessage);
    }
  sv_errorEntered = true;

  va_start (argptr,fmt);
  Q_vsnprintf (sv_qcomErrorMessage, sizeof(sv_qcomErrorMessage), fmt, argptr);
  va_end (argptr);

  if ((code == ERR_DISCONNECT) || (code == ERR_SERVERDISCONNECT))
    {
      SV_Shutdown ("Server disconnected");
      // make sure we can get at our local stuff
      // go back to sv_frame ()
      sv_errorEntered = false;
      longjmp (abortframe, -1);
    }
  else if (code == ERR_DROP)
    {
      SV_Printf ("********************\nERROR: %s\n********************\n",
		 sv_qcomErrorMessage);
      SV_Shutdown (va("Server crashed: %s", sv_qcomErrorMessage));
      // go back to sv_frame ()
      sv_errorEntered = false;
      longjmp (abortframe, -1);
    }
  else // err_fatal, end the executable
    {
      SV_Shutdown (va("Server fatal crashed: %s", sv_qcomErrorMessage));
      if (sv_qcomLoaded)
	qce->Com_Shutdown ();
    }

  Sys_Error ("%s", sv_qcomErrorMessage);
}

// Theses Two are here for compatibility with q_shared.c
void SV_Fatal_Error (int level, const char *error, ...)
{
  va_list argptr;
  char text[1024];

  va_start (argptr, error);
  Q_vsnprintf (text, sizeof(text), error, argptr);
  va_end (argptr);

  //SV_Printf ("Error lvl : %i |%s",level, text);
  SV_Error (ERR_FATAL, "QShared - lvl %i |%s\n", level, text);
}

/*
===========================================================================
Copyright (C) 2007-2013 Gabriel Schnoering

This file is part of mQ3 source code.
mQ3 is free software licensed under GNU AGPLv3
or (at your option) any later version.
More informations in the LICENSE file.
===========================================================================
*/

#include "server.h"

/*
===============================================================================

OPERATOR CONSOLE ONLY COMMANDS

These commands can only be entered from stdin or by a remote operator datagram
===============================================================================
*/

/*
==================
SV_GetPlayerByHandle

Returns the player with player id or name from Cmd_Argv(1)
==================
*/
static client_t *
SV_GetPlayerByHandle (void)
{
  client_t	*cl;
  int		i;
  const char	*s;
  char		cleanName[64];

  // make sure server is running
  if (!sv_running->integer)
    {
      return NULL;
    }

  if (qce->cmd.Argc() < 2)
    {
      SV_Printf ("No player specified.\n");
      return NULL;
    }

  s = qce->cmd.Argv (1);

  // Check whether this is a numeric player handle
  for (i = 0; s[i] >= '0' && s[i] <= '9'; i++);

  // arguments are '0' terminated
  if (!s[i])
    {
      int plid = atoi (s);

      // Check for numeric playerid match
      if (plid >= 0 && plid < sv_maxclients->integer)
	{
	  cl = &svs.clients[plid];

	  if(cl->state)
	    return cl;
	}
    }

  // check for a name match
  for (i=0, cl=svs.clients; i < sv_maxclients->integer; i++,cl++)
    {
      if (!cl->state)
	{
	  continue;
	}
      if (!Q_stricmp(cl->name, s))
	{
	  return cl;
	}

      Q_strncpyz (cleanName, cl->name, sizeof(cleanName));
      Q_CleanStr (cleanName);
      if (!Q_stricmp(cleanName, s))
	{
	  return cl;
	}
    }

  SV_Printf ("Player %s is not on the server\n", s);

  return NULL;
}

//=========================================================

/*
==================
SV_Map_f

Restart the server on a different map
==================
*/
static void
SV_Map_f (void)
{
  char		*cmd;
  char		*map;
  bool	killBots, cheat;
  char		expanded[MAX_QPATH];
  char		mapname[MAX_QPATH];

  map = qce->cmd.Argv (1);
  if (!map)
    {
      return;
    }

  // make sure the level exists before trying to change, so that
  // a typo at the server console won't end the game
  Com_sprintf (expanded, sizeof(expanded), "maps/%s.bsp", map);
  if (qce->fs.ReadFile (expanded, NULL) == -1)
    {
      SV_Printf ("Can't find map %s\n", expanded);
      return;
    }

  cmd = qce->cmd.Argv (0);

  if (!Q_stricmp(cmd, "devmap"))
    {
      cheat = true;
      killBots = true;
    }
  else
    {
      cheat = false;
      killBots = false;
    }

  // save the map name here because on a map restart we reload the q3config.cfg
  // and thus nuke the arguments of the map command
  Q_strncpyz (mapname, map, sizeof(mapname));

  // start up the map
  SV_SpawnServer (mapname, killBots);

  // set the cheat value
  // if the level was started with "map <levelname>", then
  // cheats will not be allowed.  If started with "devmap <levelname>"
  // then cheats will be allowed
  if (cheat)
    {
      qce->cvar.Set ("sv_cheats", "1");
    }
  else
    {
      qce->cvar.Set ("sv_cheats", "0");
    }
}

/*
================
SV_MapRestart_f

Completely restarts a level, but doesn't send a new gamestate to the clients.
This allows fair starts with variable load times.
================
*/
static void
SV_MapRestart_f (void)
{
  int		i;
  client_t	*client;
  char		*denied;
  int		delay;
  bool	isBot;

  // make sure we aren't restarting twice in the same frame
  if (sv_frametime == sv.serverId)
    {
      return;
    }

  // make sure server is running
  if (!sv_running->integer)
    {
      SV_Printf ("Server is not running.\n");
      return;
    }

  if (sv.restartTime)
    {
      return;
    }

  if (qce->cmd.Argc() > 1)
    {
      delay = atoi (qce->cmd.Argv(1));
      if (delay < 0)
	delay = 0;
      if (delay > 60)
	delay = 60;
    }
  else
    {
      delay = 5;
    }

  if (delay /*&& !qce->cvar.VariableValue("g_doWarmup")*/)
    {
      sv.restartTime = sv.time + delay * 1000;
      //SV_SetConfigstring (CS_WARMUP, va("%i", sv.restartTime));
      return;
    }

  // check for changes in variables that can't just be restarted
  // check for maxclients and gametype change
  // FIXME : it's up to the client to know if he can handle a gametype change
  //	     (if the ressources are loaded) not to the server to tell him
  //	     it must reload cgame

  if (sv_maxclients->modified)
    {
      char mapname[MAX_QPATH];

      SV_Printf ("SV_MapRestart : maxclients changed -- restarting.\n");

      // restart the map the slow way
      Q_strncpyz (mapname, qce->cvar.VariableString("mapname"), sizeof(mapname));

      SV_SpawnServer (mapname, false);
      return;
    }

  // toggle the server bit so clients can detect that a
  // map_restart has happened
  svs.snapFlagServerBit ^= SNAPFLAG_SERVERCOUNT;

  // generate a new serverid
  // TTimo - don't update restartedserverId there, otherwise we won't deal correctly with multiple map_restart
  sv.serverId = sv_frametime;
  qce->cvar.SetNew (sv_serverid, va("%i", sv.serverId));

  // reset all the vm data in place without changing memory allocation
  // note that we do NOT set sv.state = SS_LOADING, so configstrings that
  // had been changed from their default values will generate broadcast updates
  //sv.state = SS_LOADING;
  sv.restarting = true;

  SV_RestartGameProgs ();

  // run a few frames to allow everything to settle
  for (i = 0; i < 1; i++)
    {
      glibe->run_frame (sv.time);
      sv.time += 25;
      svs.time += 25;
    }

  sv.state = SS_GAME;
  sv.restarting = false;

  // connect and begin all the clients
  for (i=0; i < sv_maxclients->integer; i++)
    {
      client = &svs.clients[i];

      // send the new gamestate to all connected clients
      if (client->state < CS_CONNECTED)
	{
	  continue;
	}

      if (client->netchan.remoteAddress.type == NA_BOT)
	{
	  isBot = true;
	}
      else
	{
	  isBot = false;
	}

      // add the map_restart command
      SV_AddServerCommand (client, "map_restart\n");

      // connect the client again, without the firstTime flag
      denied  = glibe->client_connect (i, false, isBot);
      if (denied)
	{
	  // this generally shouldn't happen, because the client
	  // was connected before the level change
	  SV_DropClient (client, denied);
	  SV_Printf ("SV_MapRestart_f(%d): dropped client %i - denied!\n", delay, i);
	  continue;
	}

      client->state = CS_ACTIVE;

      SV_ClientEnterWorld (client, &client->lastUsercmd);
    }

  // run another frame to allow things to look at all the players
  glibe->run_frame (sv.time);
  sv.time += 25;
  svs.time += 25;
}

//===============================================================

/*
==================
SV_Kick_f

Kick a user off of the server  FIXME: move to game
==================
*/
static void
SV_Kick_f (void)
{
  client_t	*cl;
  int		i;

  // make sure server is running
  if (!sv_running->integer)
    {
      SV_Printf ("Server is not running.\n");
      return;
    }

  if (qce->cmd.Argc() != 2)
    {
      SV_Printf ("Usage: kick <player name>\nkick <player id>\nkick all = kick everyone\n");
      return;
    }

  cl = SV_GetPlayerByHandle ();

  if (cl == NULL)
    {
      // do we kick everyone ?
      if (!Q_stricmp(qce->cmd.Argv(1), "all"))
	{
	  for (i=0, cl=svs.clients; i < sv_maxclients->integer; i++,cl++)
	    {
	      if (!cl->state)
		{
		  continue;
		}
	      if (cl->netchan.remoteAddress.type == NA_LOOPBACK)
		{
		  continue;
		}

	      if (cl->netchan.remoteAddress.type != NA_BOT)
		{
		  continue;
		}

	      SV_DropClient (cl, "was kicked");
	      cl->lastPacketTime = svs.time;	// in case there is a funny zombie
	    }
	}
      return;
    }

  if (cl->netchan.remoteAddress.type == NA_LOOPBACK)
    {
      SV_SendServerCommand (NULL, "print \"%s\"", "Cannot kick host player\n");
      return;
    }

  SV_DropClient (cl, "was kicked");
  cl->lastPacketTime = svs.time;	// in case there is a funny zombie
}

/*
================
SV_Status_f
================
*/
static void
SV_Status_f (void)
{
  int		i, j, l;
  client_t	*cl;
  playerState_t	*ps;
  const char	*s;
  int		ping;
  char		cleanName[64];

  // make sure server is running
  if (!sv_running->integer)
    {
      SV_Printf ("Server is not running.\n");
      return;
    }

  SV_Printf ("map: %s\n", sv_mapname->string);

  SV_Printf ("num score ping name            lastmsg address                qport rate\n");
  SV_Printf ("--- ----- ---- --------------- ------- ---------------------- ----- -----\n");
  for (i=0,cl=svs.clients; i < sv_maxclients->integer; i++,cl++)
    {
      if (!cl->state)
	continue;
      SV_Printf ("%3i ", i);
      ps = SV_GameClientNum (i);
      SV_Printf ("%5i ", ps->persistant[PERS_SCORE]);

      if (cl->state == CS_CONNECTED)
	SV_Printf ("CNCT ");
      else if (cl->state == CS_ZOMBIE)
	SV_Printf ("ZMBI ");
      else
	{
	  ping = cl->ping < 9999 ? cl->ping : 9999;
	  SV_Printf ("%4i ", ping);
	}

      Q_strncpyz (cleanName, cl->name, sizeof (cleanName));
      Q_CleanStr (cleanName);

      SV_Printf ("%.15s", cleanName);

      l = 15 - strlen (cleanName);
      for (j = 0 ; j < l ; j++)
	SV_Printf (" ");

      SV_Printf (" ");

      SV_Printf ("%7i ", svs.time - cl->lastPacketTime);

      s = qce->net.AdrToString (cl->netchan.remoteAddress);
      SV_Printf ("%.22s", s);
      l = 22 - strlen(s);
      for (j = 0 ; j < l ; j++)
	SV_Printf (" ");

      SV_Printf (" ");

      SV_Printf ("%5i", cl->netchan.qport);

      SV_Printf (" %5i", cl->rate);

      SV_Printf ("\n");
    }
  SV_Printf ("\n");
}

/*
==================
SV_ConSay_f
==================
*/
static void
SV_ConSay_f (void)
{
  char	*p;
  char	text[1024];
  const char * msg = "console: ";

  // make sure server is running
  if (!sv_running->integer)
    {
      SV_Printf ("Server is not running.\n");
      return;
    }

  if (qce->cmd.Argc () < 2)
    {
      return;
    }

  Q_strncpyz (text, msg, sizeof(text));
  p = qce->cmd.Args();

  //strcat (text, p);
  Q_strcat (text, sizeof (text), p);

  SV_SendServerCommand (NULL, "chat \"%s\"", text);
}

/*
==================
SV_Heartbeat_f

Also called by SV_DropClient, SV_DirectConnect, and SV_SpawnServer
==================
*/
void
SV_Heartbeat_f (void)
{
  svs.nextHeartbeatTime = -9999999;
}

/*
===========
SV_Serverinfo_f

Examine the serverinfo string
===========
*/
static void
SV_Serverinfo_f (void)
{
  SV_Printf ("Server info settings:\n");
  qce->Info_Print (qce->cvar.InfoString(CVAR_SERVERINFO));
}

/*
===========
SV_Systeminfo_f

Examine or change the serverinfo string
===========
*/
static void
SV_Systeminfo_f (void)
{
  SV_Printf ("System info settings:\n");
  qce->Info_Print (qce->cvar.InfoString_Big(CVAR_SYSTEMINFO));
}

/*
===========
SV_DumpUser_f

Examine all a users info strings FIXME: move to game
===========
*/
static void
SV_DumpUser_f (void)
{
  client_t	*cl;

  // make sure server is running
  if (!sv_running->integer)
    {
      SV_Printf ("Server is not running.\n");
      return;
    }

  if (qce->cmd.Argc() != 2)
    {
      SV_Printf ("Usage: info <userid>\n");
      return;
    }

  cl = SV_GetPlayerByHandle ();
  if (cl == NULL)
    {
      return;
    }

  SV_Printf ("userinfo\n");
  SV_Printf ("--------\n");
  qce->Info_Print (cl->userinfo);
}

/*
=================
SV_KillServer
=================
*/
static void
SV_KillServer_f (void)
{
  SV_Shutdown ("killserver");
}

//===========================================================

/*
==================
SV_CompleteMapName
==================
*/
static void
SV_CompleteMapName (char *args, int argNum)
{
  if (argNum == 2)
    {
      qce->fs.Field_CompleteFilename ("maps", "bsp", true);
    }
}

/*
==================
SV_AddOperatorCommands
==================
*/
void
SV_AddOperatorCommands (void)
{
  static bool opcominitialized;

  if (opcominitialized)
    {
      return;
    }
  opcominitialized = true;

  qce->cmd.AddCommand ("quit", SV_Quit_f);

  qce->cmd.AddCommand ("heartbeat", SV_Heartbeat_f);
  qce->cmd.AddCommand ("kick", SV_Kick_f);
  qce->cmd.AddCommand ("status", SV_Status_f);
  qce->cmd.AddCommand ("serverinfo", SV_Serverinfo_f);
  qce->cmd.AddCommand ("systeminfo", SV_Systeminfo_f);
  qce->cmd.AddCommand ("dumpuser", SV_DumpUser_f);
  qce->cmd.AddCommand ("map_restart", SV_MapRestart_f);
  qce->cmd.AddCommand ("sectorlist", SV_SectorList_f);
  qce->cmd.AddCommand ("map", SV_Map_f);
  qce->cmd.SetCommandCompletionFunc ("map", SV_CompleteMapName);
  qce->cmd.AddCommand ("devmap", SV_Map_f);
  qce->cmd.SetCommandCompletionFunc ("devmap", SV_CompleteMapName);
  qce->cmd.AddCommand ("killserver", SV_KillServer_f);
  qce->cmd.AddCommand ("say", SV_ConSay_f);

  /*
  qce->cmd.AddCommand ("rehashbans", SV_RehashBans_f);
  qce->cmd.AddCommand ("listbans", SV_ListBans_f);
  qce->cmd.AddCommand ("banaddr", SV_BanAddr_f);
  qce->cmd.AddCommand ("exceptaddr", SV_ExceptAddr_f);
  qce->cmd.AddCommand ("bandel", SV_BanDel_f);
  qce->cmd.AddCommand ("exceptdel", SV_ExceptDel_f);
  qce->cmd.AddCommand ("flushbans", SV_FlushBans_f);
  */
}

/*
==================
SV_RemoveOperatorCommands
==================
*/
void
SV_RemoveOperatorCommands (void)
{
#if 0
  // removing these won't let the server start again
  qce->cmd.RemoveCommand ("heartbeat");
  qce->cmd.RemoveCommand ("kick");
  qce->cmd.RemoveCommand ("banUser");
  qce->cmd.RemoveCommand ("banClient");
  qce->cmd.RemoveCommand ("status");
  qce->cmd.RemoveCommand ("serverinfo");
  qce->cmd.RemoveCommand ("systeminfo");
  qce->cmd.RemoveCommand ("dumpuser");
  qce->cmd.RemoveCommand ("map_restart");
  qce->cmd.RemoveCommand ("sectorlist");
  qce->cmd.RemoveCommand ("say");
#endif
}

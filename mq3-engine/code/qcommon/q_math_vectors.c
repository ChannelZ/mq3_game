


#include "q_shared.h"


vec3_t	vec3_origin = {0,0,0};
vec3_t	axisDefault[3] = {{ 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 }};


vec_t
DotProduct (const vec3_t v1, const vec3_t v2)
{
  return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}

void
VectorSubtract (const vec3_t veca, const vec3_t vecb, vec3_t out)
{
  out[0] = veca[0] - vecb[0];
  out[1] = veca[1] - vecb[1];
  out[2] = veca[2] - vecb[2];
}

void
VectorAdd (const vec3_t veca, const vec3_t vecb, vec3_t out)
{
  out[0] = veca[0] + vecb[0];
  out[1] = veca[1] + vecb[1];
  out[2] = veca[2] + vecb[2];
}

void
VectorCopy (const vec3_t in, vec3_t out)
{
  out[0] = in[0];
  out[1] = in[1];
  out[2] = in[2];
}

void
Vector4Copy (const vec4_t in, vec4_t out)
{
  out[0] = in[0];
  out[1] = in[1];
  out[2] = in[2];
  out[3] = in[3];
}

void
VectorScale (const vec3_t in, float scale, vec3_t out)
{
  out[0] = in[0] * scale;
  out[1] = in[1] * scale;
  out[2] = in[2] * scale;
}

void
VectorMA (const vec3_t veca, float scale, const vec3_t vecb, vec3_t vecc)
{
  vecc[0] = veca[0] + vecb[0] * scale;
  vecc[1] = veca[1] + vecb[1] * scale;
  vecc[2] = veca[2] + vecb[2] * scale;
}

void
VectorClear (vec3_t a)
{
  a[0] = a[1] = a[2] = 0.0f;
}

void
VectorNegate (const vec3_t in, vec3_t out)
{
  out[0] = -in[0];
  out[1] = -in[1];
  out[2] = -in[2];
}

void
VectorSet (vec3_t out, const vec_t x, const vec_t y, const vec_t z)
{
  out[0] = x;
  out[1] = y;
  out[2] = z;
}

bool
VectorCompare (const vec3_t v1, const vec3_t v2)
{
  if (v1[0] != v2[0] || v1[1] != v2[1] || v1[2] != v2[2])
    {
      return 0;
    }
  return 1;
}

vec_t
VectorLength (const vec3_t v)
{
  return Q_sqrt (v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

vec_t
VectorLengthSquared (const vec3_t v)
{
  return (v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

vec_t
Distance (const vec3_t p1, const vec3_t p2)
{
  vec3_t	v;

  VectorSubtract (p2, p1, v);
  return VectorLength (v);
}

vec_t
DistanceSquared (const vec3_t p1, const vec3_t p2)
{
  vec3_t	v;

  VectorSubtract (p2, p1, v);
  return v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
}

void
VectorInverse( vec3_t v ){
	v[0] = -v[0];
	v[1] = -v[1];
	v[2] = -v[2];
}

void
CrossProduct (const vec3_t v1, const vec3_t v2, vec3_t cross)
{
  cross[0] = v1[1]*v2[2] - v1[2]*v2[1];
  cross[1] = v1[2]*v2[0] - v1[0]*v2[2];
  cross[2] = v1[0]*v2[1] - v1[1]*v2[0];
}

void
VectorNormalizeFast (vec3_t v)
{
  vec_t ilength;

  ilength = Q_rsqrt (DotProduct (v,v));

  v[0] *= ilength;
  v[1] *= ilength;
  v[2] *= ilength;
}

vec_t
VectorNormalize (vec3_t v)
{
  vec_t	length, ilength;

  length = v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
  length = Q_sqrt (length);

  if (length)
    {
      ilength = 1/length;
      v[0] *= ilength;
      v[1] *= ilength;
      v[2] *= ilength;
    }

  return length;
}

vec_t
VectorNormalize2 (const vec3_t v, vec3_t out)
{
  float	length, ilength;

  length = v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
  length = Q_sqrt (length);

  if (length)
    {
      ilength = 1/length;
      out[0] = v[0]*ilength;
      out[1] = v[1]*ilength;
      out[2] = v[2]*ilength;
    }
  else
    {
      VectorClear (out);
    }

  return length;
}


void
VectorRotate (vec3_t in, vec3_t matrix[3], vec3_t out)
{
	out[0] = DotProduct (in, matrix[0]);
	out[1] = DotProduct (in, matrix[1]);
	out[2] = DotProduct (in, matrix[2]);
}

void
SnapVector (float *v)
{
  /*
	v[0] = rintf(v[0]);
	v[1] = rintf(v[1]);
	v[2] = rintf(v[2]);
  */
	v[0] = nearbyintf (v[0]);
	v[1] = nearbyintf (v[1]);
	v[2] = nearbyintf (v[2]);
}

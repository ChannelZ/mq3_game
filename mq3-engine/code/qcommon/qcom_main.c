/*
   Gabriel Schnoering (c) 2010

    This file is part of mQ3, based on the Quake 3 engine.
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "q_shared.h"
#include "qcommon.h"


qcom_export_t qexp;

char *
lib_info (void)
{
  return "qcommon lib 0.01";
}

int
FillQcomCmd (qcom_cmd_t * qcmd)
{
  if (qcmd == NULL)
    return -1;

  qcmd->AddCommand = Cmd_AddCommand;
  qcmd->RemoveCommand = Cmd_RemoveCommand;

  qcmd->Argc = Cmd_Argc;
  qcmd->Argv = Cmd_Argv;
  qcmd->ArgvBuffer = Cmd_ArgvBuffer;
  qcmd->Args = Cmd_Args;
  qcmd->ArgsFrom = Cmd_ArgsFrom;
  qcmd->ArgsBuffer = Cmd_ArgsBuffer;
  qcmd->TokenizeString = Cmd_TokenizeString;
  qcmd->TokenizeStringIgnoreQuotes = Cmd_TokenizeStringIgnoreQuotes;
  qcmd->Args_Sanitize = Cmd_Args_Sanitize;

  qcmd->Cmd = Cmd_Cmd;
  qcmd->ExecuteString = Cmd_ExecuteString;

  qcmd->SetCommandCompletionFunc = Cmd_SetCommandCompletionFunc;

  qcmd->Cbuf_ExecuteText = Cbuf_ExecuteText;
  qcmd->Cbuf_AddText = Cbuf_AddText;
  qcmd->Cbuf_Execute = Cbuf_Execute;

  return 0;
}


int
FillQcomCvar (qcom_cvar_t * qcva)
{
  if (qcva == NULL)
    return -1;

  qcva->Get = Cvar_Get;
  qcva->Set = Cvar_Set;
  qcva->SetNew = Cvar_SetNew;

  qcva->VariableValue = Cvar_VariableValue;
  qcva->VariableIntegerValue = Cvar_VariableIntegerValue;
  qcva->VariableStringBuffer = Cvar_VariableStringBuffer;
  qcva->VariableString = Cvar_VariableString;
  qcva->Flags = Cvar_Flags;
  qcva->Reset = Cvar_Reset;

  qcva->InfoString = Cvar_InfoString;
  qcva->InfoString_Big = Cvar_InfoString_Big;
  qcva->InfoStringBuffer = Cvar_InfoStringBuffer;

  qcva->CheckRange = Cvar_CheckRange;
  qcva->TrackChange = Cvar_TrackChange;
  qcva->Get_ModifiedFlags = Cvar_GetModifiedFlags;
  qcva->Set_ModifiedFlags = Cvar_SetModifiedFlags;

  qcva->SetCheatState = Cvar_SetCheatState;

  return 0;
}

int
FillQcomMsg (qcom_msg_t * qmsg)
{
  if (qmsg == NULL)
    return -1;

  qmsg->Init = MSG_Init;
  qmsg->WriteData = MSG_WriteData;
  qmsg->Bitstream = MSG_Bitstream;

  qmsg->WriteBits = MSG_WriteBits;
  qmsg->WriteByte = MSG_WriteByte;
  qmsg->WriteShort = MSG_WriteShort;
  qmsg->WriteLong = MSG_WriteLong;
  qmsg->WriteString = MSG_WriteString;
  qmsg->WriteBigString = MSG_WriteBigString;

  qmsg->BeginReading = MSG_BeginReading;
  qmsg->BeginReadingOOB = MSG_BeginReadingOOB;

  qmsg->ReadBits = MSG_ReadBits;
  qmsg->ReadByte = MSG_ReadByte;
  qmsg->ReadShort = MSG_ReadShort;
  qmsg->ReadLong = MSG_ReadLong;

  qmsg->ReadString = MSG_ReadString;
  qmsg->ReadBigString = MSG_ReadBigString;
  qmsg->ReadStringLine = MSG_ReadStringLine;

  qmsg->ReadData = MSG_ReadData;
  qmsg->LookaheadByte = MSG_LookaheadByte;

  qmsg->WriteDeltaUsercmd = MSG_WriteDeltaUsercmd;
  qmsg->ReadDeltaUsercmd = MSG_ReadDeltaUsercmd;

  qmsg->WriteDeltaUsercmdKey = MSG_WriteDeltaUsercmdKey;
  qmsg->ReadDeltaUsercmdKey = MSG_ReadDeltaUsercmdKey;

  qmsg->WriteDeltaEntity = MSG_WriteDeltaEntity;
  qmsg->ReadDeltaEntity = MSG_ReadDeltaEntity;

  qmsg->WriteDeltaPlayerstate = MSG_WriteDeltaPlayerstate;
  qmsg->ReadDeltaPlayerstate = MSG_ReadDeltaPlayerstate;

  qmsg->Copy = MSG_Copy;
  qmsg->Clear = MSG_Clear;

 return 0;
}

int
FillQcomCM (qcom_cm_t * qcm)
{
  if (qcm == NULL)
    return -1;

  qcm->LoadMap = CM_LoadMap;
  qcm->ClearMap = CM_ClearMap;
  qcm->InlineModel = CM_InlineModel;	// 0 = world, 1 + are bmodels
  qcm->TempBoxModel = CM_TempBoxModel;

  qcm->ModelBounds = CM_ModelBounds;

  qcm->NumClusters = CM_NumClusters;
  qcm->NumInlineModels = CM_NumInlineModels;
  qcm->EntityString = CM_EntityString;

  // returns an ORed contents mask
  qcm->PointContents = CM_PointContents;
  qcm->TransformedPointContents = CM_TransformedPointContents;
  qcm->BoxTrace = CM_BoxTrace;
  qcm->TransformedBoxTrace = CM_TransformedBoxTrace;
  qcm->ClusterPVS = CM_ClusterPVS;

  qcm->PointLeafnum = CM_PointLeafnum;

  // only returns non-solid leafs
  // overflow if return listsize and if *lastLeaf != list[listsize-1]
  qcm->BoxLeafnums = CM_BoxLeafnums;
  qcm->LeafCluster = CM_LeafCluster;
  qcm->LeafArea = CM_LeafArea;

  qcm->AdjustAreaPortalState = CM_AdjustAreaPortalState;
  qcm->AreasConnected = CM_AreasConnected;
  qcm->WriteAreaBits = CM_WriteAreaBits;
  //qcm->LerpTag = CM_LerpTag;
  //qcm->MarkFragments = CM_MarkFragments;
  qcm->DrawDebugSurface = CM_DrawDebugSurface;
  return 0;
}

int
FillQcomFS (qcom_fs_t * qfs)
{
  if (qfs == NULL)
    return -1;

  qfs->Initialized = FS_Initialized;
  qfs->Shutdown = FS_Shutdown;
  qfs->Restart = FS_Restart;
  qfs->ConditionalRestart = FS_ConditionalRestart;
  qfs->ListFiles = FS_ListFiles;
  qfs->FreeFileList = FS_FreeFileList;
  qfs->FileExists = FS_FileExists;
  qfs->GetFileList = FS_GetFileList;

  qfs->BuildOSPath = FS_BuildOSPath;
  qfs->CreatePath = FS_CreatePath;

  qfs->FOpenFileByMode = FS_FOpenFileByMode;
  qfs->FOpenFileWrite = FS_FOpenFileWrite;
  qfs->FOpenFileAppend = FS_FOpenFileAppend;
  qfs->FOpenFileRead = FS_FOpenFileRead;

  qfs->SV_FOpenFileWrite = FS_SV_FOpenFileWrite;
  qfs->SV_FOpenFileRead = FS_SV_FOpenFileRead;
  qfs->SV_Rename = FS_Rename;

  qfs->FileIsInPAK = FS_FileIsInPAK;

  qfs->Write = FS_Write;
  qfs->Read = FS_Read;
  qfs->Read2 = FS_Read2;

  qfs->FCloseFile = FS_FCloseFile;
  qfs->ReadFile = FS_ReadFile;
  qfs->FreeFile = FS_FreeFile;
  qfs->ForceFlush = FS_ForceFlush;

  qfs->WriteFile = FS_WriteFile;

  qfs->FTell = FS_FTell;
  qfs->Printf = FS_Printf;

  qfs->Seek = FS_Seek;

  qfs->LoadedPakNames = FS_LoadedPakNames;
  qfs->LoadedPakChecksums = FS_LoadedPakChecksums;
  qfs->ReferencedPakNames = FS_ReferencedPakNames;
  qfs->ReferencedPakChecksums = FS_ReferencedPakChecksums;

  qfs->ClearPakReferences = FS_ClearPakReferences;
  qfs->CheckDirTraversal = FS_CheckDirTraversal;
  qfs->ComparePaks = FS_ComparePaks;
  qfs->FilenameCompare = FS_FilenameCompare;
  qfs->idPak = FS_idPak;
  qfs->FilenameListByExt = FS_FilenameListByExt;

  qfs->validGamePath = FS_validGamePath;

  qfs->Field_Clear = Field_Clear;
  qfs->Field_AutoComplete = Field_AutoComplete;
  qfs->Field_CompleteKeyname = Field_CompleteKeyname;
  qfs->Field_CompleteFilename = Field_CompleteFilename;
  qfs->Field_CompleteCommand = Field_CompleteCommand;

  return 0;
}

int
FillQcomNet (qcom_net_t * qnet)
{
  if (qnet == NULL)
    return -1;

  qnet->SendPacket = NET_SendPacket;

  qnet->Sleep = NET_Sleep;
  qnet->Sleep3 = NET_Sleep3;

  qnet->OutOfBandPrint = NET_OutOfBandPrint;
  qnet->OutOfBandData = NET_OutOfBandData;
 
  qnet->Init = NET_Init;
  qnet->Shutdown = NET_Shutdown;

  qnet->IsLocalAddress = NET_IsLocalAddress;

  qnet->CompareAdr = NET_CompareAdr;
  qnet->CompareBaseAdrMask = NET_CompareBaseAdrMask;
  qnet->AdrToString = NET_AdrToString;
  qnet->AdrToStringwPort = NET_AdrToStringwPort;
  qnet->StringToAdr = NET_StringToAdr;
  qnet->ShowIP = Sys_ShowIP;

  qnet->FlushPacketQueue = NET_FlushPacketQueue;
  qnet->GetLoopPacket = NET_GetLoopPacket;

  qnet->JoinMulticast6 = NET_JoinMulticast6;
  qnet->LeaveMulticast6 = NET_LeaveMulticast6;

  qnet->IsLANAddress = Sys_IsLANAddress;

  qnet->Netchan_Setup = Netchan_Setup;
  qnet->Netchan_Transmit = Netchan_Transmit;
  qnet->Netchan_TransmitNextFragment = Netchan_TransmitNextFragment;

  qnet->Netchan_Process = Netchan_Process;

  return 0;
}

int
FillQcomEvent (qcom_event_t *qevent)
{
  if (qevent == NULL)
    return -1;

  qevent->GetEvent = Com_GetEvent;
  qevent->TagEvents = Com_TagEvents;
  qevent->FreeEvent = Com_FreeEvent;
  qevent->QueueEvent = Com_QueueEvent;
  qevent->Milliseconds = Com_Milliseconds;

  return 0;
}


int
FillQcomMem (qcom_mem_t * qmem)
{
  if (qmem == NULL)
    return -1;

  qmem->TouchMemory = Com_TouchMemory;
  qmem->Hunk_Clear = Hunk_Clear;
  qmem->Hunk_MemoryRemaining = Hunk_MemoryRemaining;
  qmem->Hunk_SetMark = Hunk_SetMark;
  qmem->Hunk_ClearToMark = Hunk_ClearToMark;
  qmem->Hunk_CheckMark = Hunk_CheckMark;
  qmem->Hunk_Alloc = Hunk_Alloc;
  qmem->Hunk_AllocateTempMemory = Hunk_AllocateTempMemory;
  qmem->Hunk_FreeTempMemory = Hunk_FreeTempMemory;
  qmem->Hunk_ClearTempMemory = Hunk_ClearTempMemory;

  qmem->Z_AvailableMemory = Z_AvailableMemoryTot;
  qmem->Z_Free = Z_Free;
  qmem->Z_FreeTags = Z_FreeTags1;
  qmem->Z_TagMalloc = Z_TagMalloc1;
  qmem->Z_Malloc = M_Malloc;
  qmem->S_Malloc = S_Malloc;

  qmem->CopyString = CopyString;

  return 0;
}

qcom_export_t *
CommonAPI (qcom_import_t * qimp, int flags)
{
  if (qimp == NULL)
    {
      // should not happend, report an error
      return NULL;
    }

  qci = qimp;

  // setup the print/error functions for qshared
  q_shared_import_t q_shared_imp;
  q_shared_imp.print = qci->Printf;
  q_shared_imp.error = qci->Error;

  Q_Set_Interface (&q_shared_imp);

  // set the dedicated variable, so memory can be allocated properly
  qcom_flags = flags;

  Com_Memset (&qexp, 0, sizeof (qexp));

  qexp.Com_Init = Com_Init;
  qexp.Com_Shutdown = Com_Shutdown;
  qexp.Com_RealTime = Com_RealTime;
  qexp.Com_HashKey = Com_HashKey;
  qexp.Huff_Decompress = Huff_Decompress;
  qexp.Info_Print = Info_Print;
  qexp.Com_PrintLog = Com_PrintLog;

  FillQcomCmd (&qexp.cmd);
  FillQcomCvar (&qexp.cvar);
  FillQcomMem (&qexp.mem);
  FillQcomMsg (&qexp.msg);
  FillQcomNet (&qexp.net);
  FillQcomFS (&qexp.fs);
  FillQcomCM (&qexp.cm);
  FillQcomEvent (&qexp.event);

  return &qexp;
}

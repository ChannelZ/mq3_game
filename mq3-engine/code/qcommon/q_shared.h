/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
//
#ifndef __Q_SHARED_H
#define __Q_SHARED_H

// q_shared.h -- included first by ALL program modules.
// A user mod should never modify this file

#define PRODUCT_NAME			"mq3"
#define BASEGAME			"base"
#define CLIENT_WINDOW_TITLE		"mQ3"
#define CLIENT_WINDOW_MIN_TITLE		"mq3"

#define Q3_VERSION PRODUCT_NAME " " PRODUCT_VERSION

#define MAX_TEAMNAME 32

//Ignore __attribute__ on non-gcc platforms
#ifndef __GNUC__
#ifndef __attribute__
#define __attribute__(x)
#endif
#endif


#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <limits.h>
#include <stdint.h>
#include <stdbool.h>

#include <wchar.h>

#include "q_platform.h"

//=============================================================

typedef unsigned char	byte;

#ifdef _WIN32
typedef unsigned int uint;
#endif

union floatint_s
{
  float f;
  int i;
  unsigned int ui;
};
typedef union floatint_s floatint_t;

typedef int		qhandle_t;
typedef int		sfxHandle_t;
typedef int		fileHandle_t;
typedef int		clipHandle_t;

#define PAD(x,y) (((x)+(y)-1) & ~((y)-1))

#ifdef __GNUC__
#define QALIGN(x) __attribute__((aligned(x)))
#else
#define QALIGN(x)
#endif

struct q_shared_import_s
{
  void (*print) (const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
  void (*error) (int error, const char * fmt, ...) __attribute__ ((format (printf, 2, 3)));
};
typedef struct q_shared_import_s q_shared_import_t;

extern q_shared_import_t qi;

//void SHD_PRINT (const char * fmt, ...) __attribute__ ((format (printf, 1, 2)));
//void SHD_ERROR (int error, const char * fmt, ...) __attribute__ ((format (printf, 2, 3)));
#define SHD_PRINT qi.print
#define SHD_ERROR qi.error

void Q_Set_Interface (q_shared_import_t *q_shared_imp);

// centralized and cleaned, that's the max string you can send to a Com_Printf / Com_DPrintf (above gets truncated)
#define	MAXPRINTMSG	1024 // 4096


// only used in memhunk.h
#define	STRING(s)	#s
// expand constants before stringifying them
#define	XSTRING(s)	STRING(s)

#define	MAX_QINT	0x7fffffff
#define	MIN_QINT	(-MAX_QINT-1)

#define ARRAY_LEN(x) (sizeof(x) / sizeof(*(x)))

// angle indexes
#define	PITCH		0		// up / down
#define	YAW		1		// left / right
#define	ROLL		2		// fall over

// the game guarantees that no string from the network will ever
// exceed MAX_STRING_CHARS
#define	MAX_STRING_CHARS	1024	// max length of a string passed to Cmd_TokenizeString
#define	MAX_STRING_TOKENS	1024	// max tokens resulting from Cmd_TokenizeString
#define	MAX_TOKEN_CHARS		1024	// max length of an individual token

#define	MAX_INFO_STRING		1024
#define	MAX_INFO_KEY		1024
#define	MAX_INFO_VALUE		1024

#define	BIG_INFO_STRING		8192	// used for system info key only
#define	BIG_INFO_KEY		8192
#define	BIG_INFO_VALUE		8192


#define	MAX_QPATH		64	// max length of a quake game pathname
#define	MAX_OSPATH		256	// max length of a filesystem pathname

#define	MAX_NAME_LENGTH		32	// max length of a client name

#define	MAX_SAY_TEXT		150

// paramters for command buffer stuffing
enum cbufExec_s
  {
    EXEC_NOW,		// don't return until completed, a lib should NEVER use this,
			// because some commands might cause the lib to be unloaded...
    EXEC_INSERT,	// insert at current position, but don't run yet
    EXEC_APPEND		// add to end of the command buffer (normal case)
  };
typedef enum cbufExec_s cbufExec_t;


#define	MAX_MAP_AREA_BYTES	32	// bit vector of area visibility


// print levels from renderer (FIXME: set up for game / cgame?)
enum printParm_s
  {
    PRINT_ALL,
    PRINT_DEVELOPER,	// only print when "developer 1"
    PRINT_CONSOLE,
    PRINT_NOTIFY,
    PRINT_WARNING,
    PRINT_ERROR
  };
typedef enum printParm_s printParm_t;


#ifdef ERR_FATAL
#undef ERR_FATAL	// this is be defined in malloc.h
#endif

// parameters to the main Error routine
enum errorParm_s
  {
    ERR_FATAL,		// exit the entire game with a popup window
    ERR_DROP,		// print to console and disconnect from game
    ERR_SERVERDISCONNECT,	// don't kill server
    ERR_DISCONNECT,	// client disconnected from the server
  };
typedef enum errorParm_s errorParm_t;

/*
#if defined(_DEBUG) && !defined(BSPC)
	#define HUNK_DEBUG
#endif

#ifdef HUNK_DEBUG
#define Hunk_Alloc (size, preference)	Hunk_AllocDebug (size, preference, #size, __FILE__, __LINE__)
void *Hunk_AllocDebug (int size, ha_pref preference, char *label, char *file, int line);
#else
void *Hunk_Alloc (int size, ha_pref preference);
#endif
*/
#define Com_Memset memset
#define Com_Memcpy memcpy


/*
==============================================================

MATHLIB

==============================================================
*/


typedef float vec_t;
typedef vec_t vec2_t[2];
typedef vec_t vec3_t[3];
typedef vec_t vec4_t[4];
typedef vec_t vec5_t[5];


#ifndef M_PI
#define M_PI	3.14159265358979323846f	// matches value in gcc v2 math.h
#endif

#define NUMVERTEXNORMALS	162
extern	vec3_t	bytedirs[NUMVERTEXNORMALS];


extern	vec4_t		colorBlack;
extern	vec4_t		colorRed;
extern	vec4_t		colorGreen;
extern	vec4_t		colorBlue;
extern	vec4_t		colorYellow;
extern	vec4_t		colorMagenta;
extern	vec4_t		colorCyan;
extern	vec4_t		colorWhite;
extern	vec4_t		colorLtGrey;
extern	vec4_t		colorMdGrey;
extern	vec4_t		colorDkGrey;

#define Q_COLOR_ESCAPE	'^'

#define Q_IsColorString(p)	(p && *(p) == Q_COLOR_ESCAPE && *((p)+1) && isalnum(*((p)+1))) // ^[0-9a-zA-Z]

//bool Q_IsColorString (const char * p);

#define COLOR_BLACK		'0'
#define COLOR_RED		'1'
#define COLOR_GREEN		'2'
#define COLOR_YELLOW		'3'
#define COLOR_BLUE		'4'
#define COLOR_CYAN		'5'
#define COLOR_MAGENTA		'6'
#define COLOR_WHITE		'7'

#define ColorIndex(c)	(((c) - '0' ) & 7)

int ColorIndex2(char c);
// takes a number < MAX_DEFINED_COLORS and gives the associated char
char ColorIndexToChar (int i);

#define S_COLOR_BLACK	"^0"
#define S_COLOR_RED	"^1"
#define S_COLOR_GREEN	"^2"
#define S_COLOR_YELLOW	"^3"
#define S_COLOR_BLUE	"^4"
#define S_COLOR_CYAN	"^5"
#define S_COLOR_MAGENTA	"^6"
#define S_COLOR_WHITE	"^7"

// extended colors
#define S_COLOR_ORANGE "^r"
#define S_COLOR_PURPLE "^N"
#define S_COLOR_LIGHTGREEN "^A"
#define S_COLOR_LIGHTPURPLE "^U"
#define S_COLOR_ORANGE2 "^o"
#define S_COLOR_LIGHTBLUE "^G"

#define MAX_DEFINED_COLORS 62
extern vec4_t	g_color_table[MAX_DEFINED_COLORS];

int str16to10 (const char *nptr, char **endptr);

#define	MAKERGB( v, r, g, b ) v[0]=r;v[1]=g;v[2]=b
#define	MAKERGBA( v, r, g, b, a ) v[0]=r;v[1]=g;v[2]=b;v[3]=a

#define DEG2RAD( a ) ( ( (a) * M_PI ) / 180.0F )
#define RAD2DEG( a ) ( ( (a) * 180.0f ) / M_PI )

struct cplane_s;

extern	vec3_t	vec3_origin;
extern	vec3_t	axisDefault[3];

// Tables
extern	const char c16to10[256];

#define	nanmask (255<<23)

#define	IS_NAN(x) (((*(int *)&x)&nanmask)==nanmask)


float	Q_fabs (float f);
float	Q_rsqrt (float f);		// reciprocal square root
// use this so it can be changed if a faster version is available
#define Q_sqrt sqrtf

//inline const float Q_sqrtfast (float f) {return (f * Q_rsqrt(f));}

#define SQRTFAST(x) ((x) * Q_rsqrt (x))
#define Square(x) ((x)*(x))

signed char ClampChar( int i );
signed short ClampShort( int i );


/********** Vector Operations ************/

// this isn't a real cheap function to call!
int	DirToByte (vec3_t dir);
void	ByteToDir (int b, vec3_t dir);

vec_t	DotProduct (const vec3_t v1, const vec3_t v2);
void	VectorSubtract (const vec3_t veca, const vec3_t vecb, vec3_t out);
void	VectorAdd (const vec3_t veca, const vec3_t vecb, vec3_t out);
void	VectorCopy (const vec3_t in, vec3_t out);
void	Vector4Copy (const vec4_t in, vec4_t out);
void	VectorScale (const vec3_t in, float scale, vec3_t out);
void	Vector4Scale (const vec4_t in, vec_t scale, vec4_t out);
void	VectorMA (const vec3_t veca, float scale, const vec3_t vecb, vec3_t vecc);
void	VectorClear (vec3_t a);
void	VectorNegate (const vec3_t in, vec3_t out);
void	VectorSet (vec3_t out, const vec_t x, const vec_t y, const vec_t z);

bool VectorCompare (const vec3_t v1, const vec3_t v2);
vec_t	VectorLength (const vec3_t v);
vec_t	VectorLengthSquared (const vec3_t v);
vec_t	Distance (const vec3_t p1, const vec3_t p2);
vec_t	DistanceSquared (const vec3_t p1, const vec3_t p2);
void	VectorInverse (vec3_t v);
void	CrossProduct (const vec3_t v1, const vec3_t v2, vec3_t cross);

void	VectorNormalizeFast (vec3_t v);
vec_t	VectorNormalize (vec3_t v);		// returns vector length
vec_t	VectorNormalize2 (const vec3_t v, vec3_t out);
void	VectorRotate (vec3_t in, vec3_t matrix[3], vec3_t out);

void	SnapVector (float *v);


unsigned ColorBytes3 (float r, float g, float b);
unsigned ColorBytes4 (float r, float g, float b, float a);

float	NormalizeColor (const vec3_t in, vec3_t out);

float	RadiusFromBounds (const vec3_t mins, const vec3_t maxs);
void	ClearBounds (vec3_t mins, vec3_t maxs);
void	AddPointToBounds (const vec3_t v, vec3_t mins, vec3_t maxs);

int	Q_log2 (int val);

//float	Q_acos(float c);

int	Q_rand (int *seed);
float	Q_random (int *seed);
float	Q_crandom (int *seed);

#define random()	((rand () & 0x7fff) / ((float)0x7fff))
#define crandom()	(2.0 * (random() - 0.5))

void	vectoangles (const vec3_t value1, vec3_t angles);
void	AnglesToAxis (const vec3_t angles, vec3_t axis[3]);

void	AxisClear (vec3_t axis[3]);
void	AxisCopy (vec3_t in[3], vec3_t out[3]);

void	SetPlaneSignbits (struct cplane_s *out);

int	BoxOnPlaneSide (vec3_t emins, vec3_t emaxs, struct cplane_s *plane);

bool BoundsIntersect (const vec3_t mins, const vec3_t maxs,
			  const vec3_t mins2, const vec3_t maxs2);
bool BoundsIntersectSphere (const vec3_t mins, const vec3_t maxs,
				const vec3_t origin, vec_t radius);
bool BoundsIntersectPoint (const vec3_t mins, const vec3_t maxs,
			       const vec3_t origin);

float	AngleMod (float a);
float	LerpAngle (float from, float to, float frac);
float	AngleSubtract (float a1, float a2);
void	AnglesSubtract (vec3_t v1, vec3_t v2, vec3_t v3);

float	AngleNormalize360 (float angle);
float	AngleNormalize180 (float angle);
float	AngleDelta (float angle1, float angle2);

bool PlaneFromPoints (vec4_t plane, const vec3_t a, const vec3_t b, const vec3_t c);
void	ProjectPointOnPlane (vec3_t dst, const vec3_t p, const vec3_t normal);
void	RotatePointAroundVector (vec3_t dst, const vec3_t dir, const vec3_t point, float degrees);
void	RotateAroundDirection (vec3_t axis[3], float yaw);
void	MakeNormalVectors (const vec3_t forward, vec3_t right, vec3_t up);
// perpendicular vector could be replaced by this

//int	PlaneTypeForNormal (vec3_t normal);

void	MatrixMultiply (float in1[3][3], float in2[3][3], float out[3][3]);
void	AngleVectors (const vec3_t angles, vec3_t forward, vec3_t right, vec3_t up);
void	PerpendicularVector (vec3_t dst, const vec3_t src);
int	Q_isnan (float x);


//=============================================

float	Com_Clamp (float min, float max, float value);

char	*COM_SkipPath (char *pathname);
const char *COM_GetExtension (const char *name);
void	COM_StripExtension (const char *in, char *out, int destsize);
void	COM_DefaultExtension (char *path, int maxSize, const char *extension);

void	COM_BeginParseSession (const char *name);
int	COM_GetCurrentParseLine (void);
char	*COM_Parse (char **data_p);
char	*COM_ParseExt (char **data_p, bool allowLineBreak);
int	COM_Compress (char *data_p);
void	COM_ParseError (char *format, ...) __attribute__ ((format (printf, 1, 2)));
void	COM_ParseWarning (char *format, ...) __attribute__ ((format (printf, 1, 2)));
//int		COM_ParseInfos( char *buf, int max, char infos[][MAX_INFO_STRING] );

int	COM_Filter(char *filter, char *name, int casesensitive);
int	COM_FilterPath(char *filter, char *name, int casesensitive);


#define MAX_TOKENLENGTH		1024

#ifndef TT_STRING
//token types
#define TT_STRING		1			// string
#define TT_LITERAL		2			// literal
#define TT_NUMBER		3			// number
#define TT_NAME			4			// name
#define TT_PUNCTUATION		5			// punctuation
#endif

struct pc_token_s
{
  int	type;
  int	subtype;
  int	intvalue;
  float	floatvalue;
  char	string[MAX_TOKENLENGTH];
};
typedef struct pc_token_s pc_token_t;

// data is an in/out parm, returns a parsed out token

void	COM_MatchToken (char**buf_p, char *match);

void	SkipBracedSection (char **program);
void	SkipRestOfLine (char **data);

void	Parse1DMatrix (char **buf_p, int x, float *m);
void	Parse2DMatrix (char **buf_p, int y, int x, float *m);
void	Parse3DMatrix (char **buf_p, int z, int y, int x, float *m);

// from ioq3
int	Com_HexStrToInt (const char *str);
char	Com_IntNumberToChar (int val);

int	Com_sprintf (char *dest, unsigned int size, const char *fmt, ...) __attribute__ ((format (printf, 3, 4)));

char	*Com_SkipTokens (char *s, int numTokens, char *sep);
char	*Com_SkipCharset (char *s, char *sep);

void	Com_RandomBytes (byte *string, int len);

// mode parm for FS_FOpenFile
enum fsMode_s
  {
    FS_READ,
    FS_WRITE,
    FS_APPEND,
    FS_APPEND_SYNC
  };
typedef enum fsMode_s fsMode_t;

enum fsOrigin_s
  {
    FS_SEEK_CUR,
    FS_SEEK_END,
    FS_SEEK_SET
  };
typedef enum fsOrigin_s fsOrigin_t;

//=============================================

int	Q_isprint (int c);
int	Q_islower (int c);
int	Q_isupper (int c);
int	Q_isalpha (int c);
bool Q_isanumber (const char *s);
bool Q_isintegral (float f);

// portable case insensitive compare
int	Q_stricmp (const char *s1, const char *s2);
int	Q_strncmp (const char *s1, const char *s2, int n);
int	Q_stricmpn (const char *s1, const char *s2, int n);
char	*Q_strlwr (char *s1);
char	*Q_strupr (char *s1);
char	*Q_strrchr (const char* string, int c);
const char *Q_stristr (const char *s, const char *find);
#define Q_strcmp strcmp

// buffer size safe library replacements
void	Q_strncpyz (char *dest, const char *src, int destsize);
void	Q_strcat (char *dest, int size, const char *src);

// check is the chunk of memory from src to src + len is empty
bool Q_IsMemNull (void * src, size_t len);

#define Q_vsnprintf vsnprintf

// strlen that discounts Quake color sequences
int	Q_PrintStrlen (const char *string);

// find the max-th printed character, taking care of colors
int Q_PrintStrPos (const char *string, int max);

// ouput a string that will be display at most maxchars (with colors)
void Q_PrintFixedColoredString (const char *in,
				char *out, int outsize,
				int maxchars);

// removes color sequences from string
char	*Q_CleanStr (char *string);
// Count the number of char tocount encountered in string
int	Q_CountChar (const char *string, char tocount);

//=============================================
// UTF8
//=============================================

int Q_UTF8Width (const char *str);
int Q_UTF8Strlen (const char *str);
int Q_UTF8PrintStrlen (const char *str);
bool Q_UTF8ContByte (char c);
unsigned long Q_UTF8CodePoint (const char *str);
char *Q_UTF8Encode (unsigned long codepoint);

//=============================================
/*
short	BigShort(short l);
short	LittleShort(short l);
int	BigLong (int l);
int	LittleLong (int l);
qint64  BigLong64 (qint64 l);
qint64  LittleLong64 (qint64 l);
float	BigFloat (const float *l);
float	LittleFloat (const float *l);

void	Swap_Init (void);
*/
char	* va(char *format, ...) __attribute__ ((format (printf, 1, 2)));

#define TRUNCATE_LENGTH	64
void	 Com_TruncateLongString (char *buffer, const char *s);

//=============================================

//
// key / value info strings
//
char	*Info_ValueForKey (const char *s, const char *key);
void	Info_RemoveKey (char *s, const char *key);
void	Info_RemoveKey_big (char *s, const char *key);
void	Info_SetValueForKey (char *s, const char *key, const char *value);
void	Info_SetValueForKey_Big (char *s, const char *key, const char *value);
bool Info_Validate (const char *s);
void	Info_NextPair (const char **s, char *key, char *value);


/*
==============================================================

COLLISION DETECTION

==============================================================
*/

#include "surfaceflags.h"	// shared with the q3map utility

// plane types are used to speed some tests
// 0-2 are axial planes
#define	PLANE_X			0
#define	PLANE_Y			1
#define	PLANE_Z			2
#define	PLANE_NON_AXIAL		3


/*
=================
PlaneTypeForNormal
=================
*/

#define PlaneTypeForNormal(x) (x[0] == 1.0 ? PLANE_X : (x[1] == 1.0 ? PLANE_Y : (x[2] == 1.0 ? PLANE_Z : PLANE_NON_AXIAL) ) )

// plane_t structure
// !!! if this is changed, it must be changed in asm code too !!!
struct cplane_s
{
  vec3_t	normal;
  float		dist;
  byte		type;		// for fast side tests: 0,1,2 = axial, 3 = nonaxial
  byte		signbits;	// signx + (signy<<1) + (signz<<2), used as lookup during collision
  byte		pad[2];
};
typedef struct cplane_s cplane_t;


// a trace is returned when a box is swept through the world
struct trace_s
{
  bool	allsolid;	// if true, plane is not valid
  bool	startsolid;	// if true, the initial point was in a solid area
  float		fraction;	// time completed, 1.0 = didn't hit anything
  vec3_t	endpos;		// final position
  cplane_t	plane;		// surface normal at impact, transformed to world space
  int		surfaceFlags;	// surface hit
  int		contents;	// contents on other side of surface hit
  int		entityNum;	// entity the contacted sirface is a part of
};
typedef struct trace_s trace_t;

// trace->entityNum can also be 0 to (MAX_GENTITIES-1)
// or ENTITYNUM_NONE, ENTITYNUM_WORLD


// markfragments are returned by CM_MarkFragments()
struct markFragment_s
{
  int		firstPoint;
  int		numPoints;
};
typedef struct markFragment_s markFragment_t;

struct orientation_s
{
  vec3_t	origin;
  vec3_t	axis[3];
};
typedef struct orientation_s orientation_t;

//=====================================================================


// in order from highest priority to lowest
// if none of the catchers are active, bound key strings will be executed
#define	KEYCATCH_CONSOLE	0x0001
#define	KEYCATCH_UI		0x0002
#define	KEYCATCH_MESSAGE	0x0004
#define	KEYCATCH_CGAME		0x0008


// sound channels
// channel 0 never willingly overrides
// other channels will allways override a playing sound on that channel
enum soundChannel_s
  {
    CHAN_AUTO,
    CHAN_LOCAL,		// menu sounds, etc
    CHAN_WEAPON,
    CHAN_VOICE,
    CHAN_ITEM,
    CHAN_BODY,
    CHAN_LOCAL_SOUND,	// chat messages, etc
    CHAN_ANNOUNCER	// announcer voices, etc
  };
typedef enum soundChannel_s soundChannel_t;

/*
========================================================================

  ELEMENTS COMMUNICATED ACROSS THE NET

========================================================================
*/

#define	ANGLE2SHORT(x)	((int)((x)*65536/360) & 65535)
#define	SHORT2ANGLE(x)	((x)*(360.0/65536))

#define	SNAPFLAG_RATE_DELAYED	1
#define	SNAPFLAG_NOT_ACTIVE	2	// snapshot used during connection and for zombies
#define SNAPFLAG_SERVERCOUNT	4	// toggled every map_restart so transitions can be detected

//
// per-level limits
//
#define	MAX_CLIENTS		64		// absolute limit
#define MAX_LOCATIONS		64

#define	GENTITYNUM_BITS		10		// don't need to send any more
#define	MAX_GENTITIES		(1<<GENTITYNUM_BITS)

// entitynums are communicated with GENTITY_BITS, so any reserved
// values that are going to be communcated over the net need to
// also be in this range
#define	ENTITYNUM_NONE		(MAX_GENTITIES-1)
#define	ENTITYNUM_WORLD		(MAX_GENTITIES-2)
#define	ENTITYNUM_MAX_NORMAL	(MAX_GENTITIES-2)


#define	MAX_MODELS		256		// these are sent over the net as 8 bits
#define	MAX_SOUNDS		256		// so they cannot be blindly increased


#define	MAX_CONFIGSTRINGS	1024

// these are the only configstrings that the system reserves, all the
// other ones are strictly for servergame to clientgame communication
#define	CS_SERVERINFO		0		// an info string with all the serverinfo cvars
#define	CS_SYSTEMINFO		1		// an info string for server system to client system configuration (timescale, etc)

#define	RESERVED_CONFIGSTRINGS	2	// game can't modify below this, only the system can

#define	MAX_GAMESTATE_CHARS	16000
struct gameState_s
{
  int		stringOffsets[MAX_CONFIGSTRINGS];
  char		stringData[MAX_GAMESTATE_CHARS];
  int		dataCount;
};
typedef struct gameState_s gameState_t;

//=========================================================

// bit field limits
#define	MAX_STATS		16
#define	MAX_PERSISTANT		16
#define	MAX_POWERUPS		16
#define	MAX_WEAPONS		16

#define	MAX_PS_EVENTS		2

#define PS_PMOVEFRAMECOUNTBITS	6

// playerState_t is the information needed by both the client and server
// to predict player motion and actions
// nothing outside of pmove should modify these, or some degree of prediction error
// will occur

// you can't add anything to this without modifying the code in msg.c

// playerState_t is a full superset of entityState_t as it is used by players,
// so if a playerState_t is transmitted, the entityState_t can be fully derived
// from it.

// bubu : all fields must be 32 bits ?
struct playerState_s
{
  int		commandTime;	// cmd->serverTime of last executed command
  int		pm_type;
  int		bobCycle;	// for view bobbing and footstep generation
  int		pm_flags;	// ducked, jump_held, etc
  int		pm_time;

  vec3_t	origin;
  vec3_t	velocity;
  int		weaponTime;
  int		gravity;
  int		speed;
  int		delta_angles[3];// add to command angles to get view direction
  // changed by spawns, rotating objects, and teleporters

  int		groundEntityNum;// ENTITYNUM_NONE = in air

  int		legsTimer;	// don't change low priority animations until this runs out
  int		legsAnim;	// mask off ANIM_TOGGLEBIT

  int		torsoTimer;	// don't change low priority animations until this runs out
  int		torsoAnim;	// mask off ANIM_TOGGLEBIT

  int		movementDir;	// a number 0 to 7 that represents the reletive angle
				// of movement to the view angle (axial and diagonals)
				// when at rest, the value will remain unchanged
				// used to twist the legs during strafing

  int		eFlags;		// copied to entityState_t->eFlags

  int		eventSequence;	// pmove generated events
  int		events[MAX_PS_EVENTS];
  int		eventParms[MAX_PS_EVENTS];

  int		externalEvent;	// events set on player from another source
  int		externalEventParm;
  int		externalEventTime;

  int		clientNum;	// ranges from 0 to MAX_CLIENTS-1
  int		weapon;		// copied to entityState_t->weapon
  int		weaponstate;

  vec3_t	viewangles;	// for fixed views
  int		viewheight;

  // damage feedback
  int		damageEvent;	// when it changes, latch the other parms
  int		damageYaw;
  int		damagePitch;
  int		damageCount;

  int		stats[MAX_STATS];
  int		persistant[MAX_PERSISTANT];	// stats that aren't cleared on death
  int		powerups[MAX_POWERUPS];	// level.time that the powerup runs out
  int		ammo[MAX_WEAPONS];

  int		generic1;
  int		loopSound;
  int		jumppad_ent;	// jumppad entity hit this frame

  // not communicated over the net at all
  int		ping;		// server to game info for scoreboard
  //int		pmove_framecount;	// FIXME: don't transmit over the network
  //int		jumppad_frame;
  int		entityEventSequence;
};
typedef struct playerState_s playerState_t;


//====================================================================


//
// usercmd_t->button bits, many of which are generated by the client system,
// so they aren't game/cgame only definitions
//
#define	BUTTON_ATTACK		1
#define	BUTTON_TALK		2		// displays talk balloon and disables actions
#define	BUTTON_USE_HOLDABLE	4
#define	BUTTON_GESTURE		8
#define	BUTTON_WALKING		16		// walking can't just be infered from MOVE_RUN
						// because a key pressed late in the frame will
						// only generate a small move value for that frame
						// walking will use different animations and
						// won't generate footsteps
#define BUTTON_AFFIRMATIVE	32
#define	BUTTON_NEGATIVE		64

#define BUTTON_GETFLAG		128
#define BUTTON_GUARDBASE	256
#define BUTTON_PATROL		512
#define BUTTON_FOLLOWME		1024

#define	BUTTON_ANY		2048		// any key whatsoever

#define	MOVE_RUN		120		// if forwardmove or rightmove are >= MOVE_RUN,
						// then BUTTON_WALKING should be set

// usercmd_t is sent to the server each client frame
struct usercmd_s {
  int		serverTime;
  int		angles[3];
  int		buttons;
  byte		weapon;           // weapon
  signed char	forwardmove, rightmove, upmove;
};
typedef struct usercmd_s usercmd_t;

//===================================================================

// if entityState->solid == SOLID_BMODEL, modelindex is an inline model number
#define	SOLID_BMODEL	0xffffff

enum trType_s
  {
    TR_STATIONARY,
    TR_INTERPOLATE,		// non-parametric, but interpolate between snapshots
    TR_LINEAR,
    TR_LINEAR_STOP,
    TR_SINE,		// value = base + sin( time / duration ) * delta
    TR_GRAVITY
  };
typedef enum trType_s trType_t;

struct trajectory_s
{
  trType_t	trType;
  int		trTime;
  int		trDuration;	// if non 0, trTime + trDuration = stop time
  vec3_t	trBase;
  vec3_t	trDelta;	// velocity, etc
};
typedef struct trajectory_s trajectory_t;

// entityState_t is the information conveyed from the server
// in an update message about entities that the client will
// need to render in some way
// Different eTypes may use the information in different ways
// The messages are delta compressed, so it doesn't really matter if
// the structure size is fairly large

// bubu : all fields must be 32 bits ?
struct entityState_s
{
  int		number;		// entity index
  int		eType;		// entityType_t
  int		eFlags;

  trajectory_t	pos;		// for calculating position
  trajectory_t	apos;		// for calculating angles

  int		time;
  int		time2;

  vec3_t	origin;
  vec3_t	origin2;

  vec3_t	angles;
  vec3_t	angles2;

  int		otherEntityNum;	// shotgun sources, etc
  int		otherEntityNum2;

  int		groundEntityNum;// -1 = in air

  int		constantLight;	// r + (g<<8) + (b<<16) + (intensity<<24)
  int		loopSound;	// constantly loop this sound

  int		modelindex;
  int		modelindex2;
  int		clientNum;	// 0 to (MAX_CLIENTS - 1), for players and corpses
  int		frame;

  int		solid;		// for client side prediction, trap_linkentity sets this properly

  int		event;		// impulse events -- muzzle flashes, footsteps, etc
  int		eventParm;

  // for players
  int		powerups;	// bit flags
  int		weapon;		// determines weapon and flash model, etc
  int		legsAnim;	// mask off ANIM_TOGGLEBIT
  int		torsoAnim;	// mask off ANIM_TOGGLEBIT

  int		generic1;
};
typedef struct entityState_s entityState_t;

enum connstate_s
  {
    CA_UNINITIALIZED,
    CA_DISCONNECTED,	// not talking to a server
    CA_AUTHORIZING,		// not used any more, was checking cd key
    CA_CONNECTING,		// sending request packets to the server
    CA_CHALLENGING,		// sending challenge packets to the server
    CA_CONNECTED,		// netchan_t established, getting gamestate
    CA_LOADING,		// only during cgame initialization, never during main loop
    CA_PRIMED,		// got gamestate, waiting for first frame
    CA_ACTIVE,		// game views should be displayed
    //CA_CINEMATIC		// playing a cinematic or a static pic, not connected to a server
};
typedef enum connstate_s connstate_t;


// real time
//=============================================

struct qtime_s
{
  int	tm_sec;		// seconds after the minute - [0,59]
  int	tm_min;		// minutes after the hour - [0,59]
  int	tm_hour;	// hours since midnight - [0,23]
  int	tm_mday;	// day of the month - [1,31]
  int	tm_mon;		// months since January - [0,11]
  int	tm_year;	// years since 1900
  int	tm_wday;	// days since Sunday - [0,6]
  int	tm_yday;	// days since January 1 - [0,365]
  int	tm_isdst;	// daylight savings time flag
};
typedef struct qtime_s qtime_t;

// nothing to do here...
enum _flag_status
  {
    FLAG_ATBASE = 0,
    FLAG_TAKEN,		// CTF
    FLAG_DROPPED
  };
typedef enum _flag_status flagStatus_t;

#define	SAY_ALL		0
#define	SAY_TEAM	1
#define	SAY_TELL	2

#endif	// __Q_SHARED_H

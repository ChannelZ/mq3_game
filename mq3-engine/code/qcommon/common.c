/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
// common.c -- misc functions used in client and server

#include "q_shared.h"
#include "qcommon.h"

#include <setjmp.h>

#ifndef _WIN32
#include <netinet/in.h>
#include <sys/stat.h> // umask
#else
#include <winsock.h>
#endif

#ifdef EVDEV_MOUSE
#include <pthread.h>
#endif

#define MAX_NUM_ARGVS	50

int	com_argc;
char	*com_argv[MAX_NUM_ARGVS+1];

//extern jmp_buf	abortframe;		// an ERR_DROP occured, exit the entire frame

// interface structures
qcom_import_t * qci;
//qcom_export_t qce; // in qcom_main.c

int qcom_flags;

FILE	*debuglogfile;
fileHandle_t	pipefile;
fileHandle_t	logfile;
fileHandle_t	com_journalFile;	// events are written here
fileHandle_t	com_journalDataFile;	// config files are written here

cvar_t	*com_journal;
cvar_t	*com_pipefile;
cvar_t	*com_logfile;		// 1 = buffer log, 2 = flush after each print
cvar_t	*com_version;
//cvar_t	*com_introPlayed;
//cvar_t	*cl_paused;
//cvar_t	*sv_paused;
//cvar_t  *cl_packetdelay;
cvar_t  *sv_packetdelay;
cvar_t	*com_standalone;

cvar_t	*mem_hunk;
cvar_t	*mem_zone;

bool	com_fullyInitialized;

void	Com_WriteConfig_f (void);
void	CIN_CloseAllVideos (void);

/*
============================================================================

COMMAND LINE FUNCTIONS

+ characters seperate the commandLine string into multiple console
command lines.

All of these are valid:

quake3 +set test blah +map test
quake3 set test blah+map test
quake3 set test blah + map test

============================================================================
*/

#define	MAX_CONSOLE_LINES	32
int	com_numConsoleLines;
char	*com_consoleLines[MAX_CONSOLE_LINES];

/*
==================
Com_ParseCommandLine

Break it up into multiple console lines
==================
*/
void
Com_ParseCommandLine (char *commandLine)
{
  int inq = 0;
  com_consoleLines[0] = commandLine;
  com_numConsoleLines = 1;

  while (*commandLine)
    {
      if (*commandLine == '"')
	{
	  inq = !inq;
	}
      // look for a + seperating character
      // if commandLine came from a file, we might have real line seperators
      if ((*commandLine == '+' && !inq) || *commandLine == '\n' || *commandLine == '\r')
	{
	  if (com_numConsoleLines == MAX_CONSOLE_LINES)
	    {
	      return;
	    }
	  com_consoleLines[com_numConsoleLines] = commandLine + 1;
	  com_numConsoleLines++;
	  *commandLine = 0;
	}
      commandLine++;
    }
}


/*
===================
Com_SafeMode

Check for "safe" on the command line, which will
skip loading of q3config.cfg
===================
*/
bool
Com_SafeMode (void)
{
  int	i;

  for (i = 0; i < com_numConsoleLines; i++)
    {
      Cmd_TokenizeString (com_consoleLines[i]);
      if (!Q_stricmp(Cmd_Argv(0), "safe")
	  || !Q_stricmp(Cmd_Argv(0), "cvar_restart"))
	{
	  com_consoleLines[i][0] = 0;
	  return true;
	}
    }
  return false;
}


/*
===============
Com_StartupVariable

Searches for command line parameters that are set commands.
If match is not NULL, only that cvar will be looked for.
That is necessary because cddir and basedir need to be set
before the filesystem is started, but all other sets should
be after execing the config and default.

TODO : create a startup parsed list
       so that when called with "NULL",
       the function wont make the same work twice
       which can conflit with CVAR_INIT
===============
*/
void
Com_StartupVariable (const char *match)
{
  int i;
  char *s;

  for (i = 0; i < com_numConsoleLines; i++)
    {
      Cmd_TokenizeString (com_consoleLines[i]);
      if (strcmp (Cmd_Argv (0), "set"))
	{
	  continue;
	}

      s = Cmd_Argv(1);
      if (match == NULL || !strcmp (s, match))
	{
	  if (Cvar_Flags (s) == CVAR_NONEXISTENT)
	    {
	      Cvar_Get (s, Cmd_Argv (2), CVAR_USER_CREATED);
	    }
	  else
	    {
	      qci->Printf ("Com_StartupVariable (%s)"
			   " : %s already created !\n", match, s);
	      Cvar_Set (s, Cmd_Argv (2));
	    }
	}
    }
}


/*
=================
Com_AddStartupCommands

Adds command line parameters as script statements
Commands are seperated by + signs

Returns true if any late commands were added, which
will keep the demoloop from immediately starting
=================
*/
bool
Com_AddStartupCommands (void)
{
  int i;
  bool added;

  added = false;
  // quote every token, so args with semicolons can work
  for (i = 0; i < com_numConsoleLines; i++)
    {
      if (!com_consoleLines[i] || !com_consoleLines[i][0])
	{
	  continue;
	}

      // dont set commands already added with Com_StartupVariable
      if (!Q_stricmpn (com_consoleLines[i], "set", 3))
	{
	  continue;
	}

      added = true;
      Cbuf_AddText (com_consoleLines[i]);
      Cbuf_AddText ("\n");
    }

  return added;
}


//============================================================================

void
Info_Print (const char *s)
{
  char	key[BIG_INFO_KEY];
  char	value[BIG_INFO_VALUE];
  char	*o;
  int	l;

  if (*s == '\\')
    s++;
  while (*s)
    {
      o = key;
      while (*s && *s != '\\')
	*o++ = *s++;

      l = o - key;
      if (l < 20)
	{
	  Com_Memset (o, ' ', 20-l);
	  key[20] = 0;
	}
      else
	*o = 0;
      qci->Printf ("%s ", key);

      if (!*s)
	{
	  qci->Printf ("MISSING VALUE\n");
	  return;
	}

      o = value;
      s++;
      while (*s && *s != '\\')
	*o++ = *s++;
      *o = 0;

      if (*s)
	s++;
      qci->Printf ("%s\n", value);
    }
}

/*
============
Com_HashKey
============
*/
int
Com_HashKey(char *string, int maxlen)
{
  register int hash, i;

  hash = 0;
  for (i = 0; i < maxlen && string[i] != '\0'; i++)
    {
      hash += string[i] * (119 + i);
    }
  hash = (hash ^ (hash >> 10) ^ (hash >> 20));
  return hash;
}

/*
================
Com_RealTime
================
*/
int
Com_RealTime (qtime_t *qtime)
{
  time_t	t;
  struct tm	*tms;

  t = time (NULL);
  if (!qtime)
    return t;
  tms = localtime (&t);
  if (tms)
    {
      qtime->tm_sec = tms->tm_sec;
      qtime->tm_min = tms->tm_min;
      qtime->tm_hour = tms->tm_hour;
      qtime->tm_mday = tms->tm_mday;
      qtime->tm_mon = tms->tm_mon;
      qtime->tm_year = tms->tm_year;
      qtime->tm_wday = tms->tm_wday;
      qtime->tm_yday = tms->tm_yday;
      qtime->tm_isdst = tms->tm_isdst;
    }
  return t;
}

/*
===================================================================

EVENTS AND JOURNALING

In addition to these events, .cfg files are also copied to the
journaled file
===================================================================
*/

/*
=================
Com_InitJournaling
=================
*/
void
Com_InitJournaling (void)
{
  Com_StartupVariable ("journal");
  com_journal = Cvar_Get ("journal", "0", CVAR_INIT);
  if (!com_journal->integer)
    {
      return;
    }

  if (com_journal->integer == 1)
    {
      qci->Printf ("Journaling events\n");
      com_journalFile = FS_FOpenFileWrite ("journal.dat");
      com_journalDataFile = FS_FOpenFileWrite ("journaldata.dat");
    }
  else if (com_journal->integer == 2)
    {
      qci->Printf ("Replaying journaled events\n");
      FS_FOpenFileRead ("journal.dat", &com_journalFile, true);
      FS_FOpenFileRead ("journaldata.dat", &com_journalDataFile, true);
    }

  if (!com_journalFile || !com_journalDataFile)
    {
      Cvar_Set ("com_journal", "0");
      com_journalFile = 0;
      com_journalDataFile = 0;
      qci->Printf ("Couldn't open journal files\n");
    }
}

/*
========================================================================

EVENT LOOP

========================================================================
*/
/*
  Events work with a stack.
  Use QueueEvent to add a new event.
  Use GrabEvent to get the last event from the stack
  Use FreeEvent to free the last event from the stack
  Be sure to call FreeEvent after EVERY GrabEvent otherwise expect breakage
  The scheme should be Grab - Do the Work - Free in the same function
  QueueEvent should be thread safe, you get event from mouse, network, etc...
  at the same time BUT the processing is not and must be done sequentially
*/

#define MAX_QUEUED_EVENTS  256

static sysEvent_t eventQueue[MAX_QUEUED_EVENTS];
static int eventHead = 0;
static int eventTail = 0;
static byte sys_packetReceived[MAX_MSGLEN];

/*
================
Com_FreeEvent
================
*/
void
Com_FreeEvent (sysEvent_t* ev)
{
  if (ev->evPtr)
    {
      Z_Free (ev->evPtr);
    }
}

/*
================
Com_GrabEvent

this function ASSUME you know there is available events
(like with isNewEventAvailable (), otherwise expect crash)
================
*/
sysEvent_t
Com_GrabEvent (void)
{
  sysEvent_t ev;

  ev = eventQueue[eventTail % MAX_QUEUED_EVENTS];
  eventTail++;

  return ev;
}

/*
================
Com_isNewEventAvailable
================
*/
bool
Com_isNewEventAvailable (void)
{
  if (eventHead > eventTail)
    return true;
  else
    return false;
}

/*
================
Com_QueueEvent

A time of 0 will get the current time
Ptr should either be null, or point to a block of data that can
be freed by the game later.
================
*/
#ifdef EVDEV_MOUSE
pthread_mutex_t mutexQueueEvent = PTHREAD_MUTEX_INITIALIZER;
#endif

void
Com_QueueEvent (int time, sysEventType_t type, int value, int value2, int ptrLength, void *ptr)
{
  sysEvent_t *ev;
  int eventDiff;

  eventDiff = eventHead - eventTail;

#ifdef EVDEV_MOUSE
  // locking to properly add events
  pthread_mutex_lock (&mutexQueueEvent);
#endif

  ev = &eventQueue[eventHead % MAX_QUEUED_EVENTS];

  if (eventDiff > MAX_QUEUED_EVENTS)
    {
      qci->Error (ERR_DROP, "Com_QueueEvent (): stacked too much events without freeing");
    }
  else if (eventDiff == MAX_QUEUED_EVENTS)
    {
      qci->Printf ("Com_QueueEvent: overflow\n");
      // we are discarding an event, but don't leak memory
      Com_FreeEvent (ev);
      // advance the last valid event pointer
      eventTail++;
    }

  if (time == 0)
    {
      time = qci->Sys_Milliseconds ();
    }

  ev->evTime = time;
  ev->evType = type;
  ev->evValue = value;
  ev->evValue2 = value2;
  ev->evPtrLength = ptrLength;
  ev->evPtr = ptr;

  // must be done last as reading functions depend on this one too
  eventHead++;

#ifdef EVDEV_MOUSE
  // unlocking
  pthread_mutex_unlock (&mutexQueueEvent);
#endif
}

/*
===============
Com_ReadFromPipe

Read whatever is in com_pipefile, if anything, and execute it
===============
*/
static void
Com_ReadFromPipe (void)
{
  // put the buffer to 0
  char buffer[MAX_EDIT_LINE] = "";
  bool read;

  if (!pipefile)
    return;

  // FS_Read won't put the ending '\0'
  read = FS_Read (buffer, sizeof (buffer) - 1, pipefile);
  if (read)
    {
      Cbuf_ExecuteText (EXEC_APPEND, buffer);
    }
}

/*
===============
Com_ReadConsoleMessage (void)

===============
*/
static void
Com_ReadConsoleMessage (void)
{
  const char* s;

  s = qci->Sys_ConsoleInput ();
  if (s)
    {
      char* b;
      int len;

      len = strlen (s) + 1;
      b = M_Malloc (len);
      strcpy (b, s);
      Com_QueueEvent (0, SE_CONSOLE, 0, 0, len, b);
    }
}

/*
===============
Com_ReadNetworkMessages

===============
*/
static void
Com_ReadNetworkMessages (void)
{
  netadr_t adr;
  msg_t netmsg;
  bool ret;

  do
    {
      MSG_Init (&netmsg, sys_packetReceived, sizeof (sys_packetReceived));

      ret = NET_GetPacket (&adr, &netmsg);
      if (ret)
	{
	  netadr_t* buf;
	  int len;

	  // copy out to a seperate buffer for qeueing
	  len = sizeof (netadr_t) + netmsg.cursize;
	  buf = M_Malloc (len);
	  *buf = adr;
	  Com_Memcpy (buf + 1, netmsg.data, netmsg.cursize);
	  Com_QueueEvent (0, SE_PACKET, 0, 0, len, buf);
	}
    }
  while (ret);
}

/*
================
Com_GetSystemEvent

================
*/
static sysEvent_t
Com_GetSystemEvent (bool getCurrentTime)
{
  // some uglyness, get a event always available to return the current time
  // and let it be saved in the journal (also a remainder if using pointers)
  static sysEvent_t timeEvent;

  // we want to grab the current time, regardless of stacked events
  if (getCurrentTime)
    {
      // create an empty event to return
      Com_Memset (&timeEvent, 0, sizeof (timeEvent));
      timeEvent.evTime = qci->Sys_Milliseconds ();

      return timeEvent;
    }

  // return if we have data
  if (Com_isNewEventAvailable ())
    {
      return Com_GrabEvent ();
    }

  // grab latest pipe entries; will not stack events
  Com_ReadFromPipe ();

  // check for console commands
  Com_ReadConsoleMessage ();

  // return if we have data
  if (Com_isNewEventAvailable ())
    {
      return Com_GrabEvent ();
    }

  // check for network packets
  Com_ReadNetworkMessages ();

  // return if we have data
  if (Com_isNewEventAvailable ())
    {
      return Com_GrabEvent ();
    }

  // create an empty event to return
  Com_Memset (&timeEvent, 0, sizeof (timeEvent));
  timeEvent.evTime = qci->Sys_Milliseconds ();

  return timeEvent;
}

/*
=================
Com_GetRealEvent
=================
*/
static sysEvent_t
Com_GetRealEvent (bool getCurrentTime)
{
  int r;
  sysEvent_t ev;

  // either get an event from the system or the journal file
  if (com_journal->integer == 2)
    {
      r = FS_Read (&ev, sizeof(ev), com_journalFile);
      if (r != sizeof(ev))
	{
	  qci->Error (ERR_FATAL, "Error reading from journal file");
	}
      if (ev.evPtrLength)
	{
	  ev.evPtr = M_Malloc (ev.evPtrLength);
	  r = FS_Read (ev.evPtr, ev.evPtrLength, com_journalFile);
	  if (r != ev.evPtrLength)
	    {
	      qci->Error (ERR_FATAL, "Error reading from journal file");
	    }
	}
    }
  else
    {
      ev = Com_GetSystemEvent (getCurrentTime);

      // write the journal value out if needed
      if (com_journal->integer == 1)
	{
	  r = FS_Write (&ev, sizeof(ev), com_journalFile);
	  if (r != sizeof(ev))
	    {
	      qci->Error (ERR_FATAL, "Error writing to journal file");
	    }
	  if (ev.evPtrLength)
	    {
	      r = FS_Write (ev.evPtr, ev.evPtrLength, com_journalFile);
	      if (r != ev.evPtrLength)
		{
		  qci->Error (ERR_FATAL, "Error writing to journal file");
		}
	    }
	}
    }

  return ev;
}

/*
=================
Com_GetEvent
=================
*/
sysEvent_t
Com_GetEvent (void)
{
  return Com_GetRealEvent (false);
}

/*
=================
Com_TagEvents

We want to timestamp network message
as they arrive for network computation
=================
*/
void
Com_TagEvents (void)
{
  // check for network packets
  Com_ReadNetworkMessages ();
}

/*
================
Com_Milliseconds

Can be used for profiling, but will be journaled accurately
================
*/
int
Com_Milliseconds (void)
{
  sysEvent_t ev;

  ev = Com_GetRealEvent (true);

  return ev.evTime;
}

//============================================================================

/*
=============
Com_Error_f

Just throw a fatal error to
test error shutdown procedures
=============
*/
static void
Com_Error_f (void)
{
  if (Cmd_Argc() > 1)
    {
      qci->Error (ERR_DROP, "Testing drop error");
    }
  else
    {
      qci->Error (ERR_FATAL, "Testing fatal error");
    }
}


/*
=============
Com_Freeze_f

Just freeze in place for a given number of seconds to test
error recovery
=============
*/
static void
Com_Freeze_f (void)
{
  float	s;
  int	start, now;

  if (Cmd_Argc() != 2)
    {
      qci->Printf ("freeze <seconds>\n");
      return;
    }
  s = atof (Cmd_Argv(1));

  start = Com_Milliseconds ();

  while (1)
    {
      now = Com_Milliseconds ();
      if ((now - start) * 0.001 > s)
	{
	  break;
	}
    }
}

/*
=================
Com_Crash_f

A way to force a bus error for development reasons
=================
*/
static void
Com_Crash_f (void)
{
  * (int *) 0 = 0x12345678;
}

/*
==================
Com_Setenv_f

For controlling environment variables
==================
*/
void Com_Setenv_f(void)
{
	int argc = Cmd_Argc();
	char *arg1 = Cmd_Argv(1);

	if(argc > 2)
	{
		char *arg2 = Cmd_ArgsFrom(2);

		qci->Sys_SetEnv(arg1, arg2);
	}
	else if(argc == 2)
	{
		char *env = getenv(arg1);

		if(env)
			qci->Printf("%s=%s\n", arg1, env);
		else
			qci->Printf("%s undefined\n", arg1);
        }
}
/*
static void
Com_DetectAltivec (void)
{
  // Only detect if user hasn't forcibly disabled it.
  if (com_altivec->integer)
    {
      static bool altivec = false;
      static bool detected = false;
      if (!detected)
	{
	  //altivec = ( Sys_GetProcessorFeatures( ) & CF_ALTIVEC );
	  detected = true;
	}

      if (!altivec)
	{
	  Cvar_Set( "com_altivec", "0" );  // we don't have it! Disable support!
	}
    }
}

*/
/*
=================
Com_InitRand
Seed the random number generator, if possible with an OS supplied random seed.
=================
*/
static void
Com_InitRand (void)
{
  unsigned int seed;

  if (qci->Sys_RandomBytes((byte *) &seed, sizeof(seed)))
    srand (seed);
  else
    srand (time(NULL));
}


/*
=================
Com_Init
=================
*/
void
Com_Init (char *commandLine)
{
  char *s;
  int qport;

  qci->Printf( "%s %s %s\n", Q3_VERSION, PLATFORM_STRING, __DATE__ );

  // Clear queues
  Com_Memset (&eventQueue[0], 0, MAX_QUEUED_EVENTS * sizeof(sysEvent_t));
  Com_Memset (&sys_packetReceived[0], 0, MAX_MSGLEN * sizeof(byte));

  // initialize the weak pseudo-random number generator for use later.
  Com_InitRand();

  Com_InitSmallZoneMemory ();
  Cvar_Init ();

  // prepare enough of the subsystems to handle
  // cvar and command buffer management
  Com_ParseCommandLine (commandLine);

  //	Swap_Init ();
  Cbuf_Init ();

  Com_StartupVariable ("com_zoneMegs");

  // allocate the stack based hunk allocator
  if (qcom_flags & QCFLAG_DEDICATED)
    {
      mem_zone = Cvar_Get ("com_zoneMegs", DEF_COMDEDZONEMEGS_S, CVAR_INIT);
    }
  else
    {
      mem_zone = Cvar_Get ("com_zoneMegs", DEF_COMZONEMEGS_S, CVAR_INIT);
    }

  Com_InitMainZoneMemory (mem_zone->integer);
  Cmd_Init ();

  // get the developer cvar set as early as possible
  Com_StartupVariable ("developer");
  // developer cvar is available early but isn't linked to a variable
  // before qcommon has finished booting
  // use a com_developer cvar ? this would break aliasing

  if (qcom_flags & QCFLAG_CLIENT)
    {
      // done early so bind command exists
      qci->CL_InitKeyCommands();
    }

  FS_InitFilesystem ();

  Com_InitJournaling ();
  // Add some commands here already so users can use them from config files
  Cmd_AddCommand ("setenv", Com_Setenv_f);

  // developer functions
#ifndef NDEBUG
  Cmd_AddCommand ("error", Com_Error_f);
  Cmd_AddCommand ("crash", Com_Crash_f);
  Cmd_AddCommand ("freeze", Com_Freeze_f);
#endif
  //Cmd_AddCommand ("quit", Com_Quit_f);
  //Cmd_AddCommand ("changeVectors", MSG_ReportChangeVectors_f );
  Cmd_AddCommand ("writeconfig", Com_WriteConfig_f );
  Cmd_SetCommandCompletionFunc ("writeconfig", Cmd_CompleteCfgName);

  // Make it execute the configuration files
  Cbuf_AddText ("exec default.cfg\n");

  // skip the q3config.cfg if "safe" is on the command line
  if (!Com_SafeMode())
    {
      if (qcom_flags & QCFLAG_DEDICATED)
	Cbuf_AddText ("exec " Q3CONFIG_CFG_DED "\n");
      else
	Cbuf_AddText ("exec " Q3CONFIG_CFG "\n");
    }

  Cbuf_AddText ("exec autoexec.cfg\n");

  Cbuf_Execute ();

  // override anything from the config files with command line args
  Com_StartupVariable (NULL);

  // allocate the stack based hunk allocator
  if (qcom_flags & QCFLAG_DEDICATED)
    {
      mem_hunk = Cvar_Get ("com_hunkMegs", DEF_COMDEDHUNKMEGS_S, CVAR_LATCH | CVAR_ARCHIVE);
    }
  else
    {
      mem_hunk = Cvar_Get ("com_hunkMegs", DEF_COMHUNKMEGS_S, CVAR_LATCH | CVAR_ARCHIVE);
    }

  Com_InitHunkMemory (mem_hunk->integer,
		      qcom_flags & QCFLAG_DEDICATED);

  // if any archived cvars are modified after this, we will trigger a writing
  // of the config file
  cvar_modifiedFlags &= ~CVAR_ARCHIVE;

  //
  // init commands and vars
  //
  com_pipefile = Cvar_Get ("com_pipefile", "", CVAR_INIT);
  if (com_pipefile->string[0])
    {
      pipefile = FS_FCreateOpenPipeFile (com_pipefile->string);
      qci->Printf ("Opening pipe file: %s [%d]\n", com_pipefile->string, pipefile);
    }

  com_logfile = Cvar_Get ("logfile", "0", CVAR_TEMP);

  sv_packetdelay = Cvar_Get ("sv_packetdelay", "0", CVAR_CHEAT);

  com_standalone = Cvar_Get ("com_standalone", "0", CVAR_INIT);

  //com_introPlayed = Cvar_Get ("com_introplayed", "0", CVAR_ARCHIVE);

  s = va("%s %s %s", Q3_VERSION, PLATFORM_STRING, __DATE__);
  com_version = Cvar_Get ("version", s, CVAR_ROM | CVAR_SERVERINFO);

  //qci->Sys_Init ();

  // Pick a random port value
  Com_RandomBytes ((byte*)&qport, sizeof(int));
  Netchan_Init (qport/* & 0xffff*/); // & is done by netchan_init

  //SV_Init ();

  // add + commands from command line
  if (!Com_AddStartupCommands ())
    {
      // if the user didn't give any commands, run default action
    }

  com_fullyInitialized = true;

  qci->Printf ("--- Common Initialization Complete ---\n");
}

//==================================================================

void
Com_WriteConfigToFile (const char *filename)
{
  fileHandle_t	f;

  f = FS_FOpenFileWrite (filename);
  if (!f)
    {
      qci->Printf ("Couldn't write %s.\n", filename );
      return;
    }

  FS_Printf (f, "// generated by quake, do not modify\n");

  if (qcom_flags & QCFLAG_CLIENT)
    qci->Key_WriteBindings (f);

  Cvar_WriteVariables (f);
  FS_FCloseFile (f);
}


/*
===============
Com_WriteConfiguration

Writes key bindings and archived cvars to config file if modified
===============
*/
void
Com_WriteConfiguration (void)
{
  // if we are quiting without fully initializing, make sure
  // we don't write out anything
  if (!com_fullyInitialized)
    {
      return;
    }

  if (!(cvar_modifiedFlags & CVAR_ARCHIVE))
    {
      return;
    }
  cvar_modifiedFlags &= ~CVAR_ARCHIVE;

  if (qcom_flags & QCFLAG_DEDICATED)
    Com_WriteConfigToFile (Q3CONFIG_CFG_DED);
  else
    Com_WriteConfigToFile (Q3CONFIG_CFG);
}


/*
===============
Com_WriteConfig_f

Write the config file to a specific name
===============
*/
void
Com_WriteConfig_f (void)
{
  char	filename[MAX_QPATH];

  if (Cmd_Argc() != 2)
    {
      qci->Printf ("Usage: writeconfig <filename>\n");
      return;
    }

  Q_strncpyz (filename, Cmd_Argv(1), sizeof(filename));
  COM_DefaultExtension (filename, sizeof(filename), ".cfg");
  qci->Printf ("Writing %s.\n", filename);
  Com_WriteConfigToFile (filename);
}

/*
=================
Com_Shutdown
=================
*/
void
Com_Shutdown (void)
{
  if (logfile)
    {
      FS_FCloseFile (logfile);
      logfile = 0;
    }

  if (com_journalFile)
    {
      FS_FCloseFile (com_journalFile);
      com_journalFile = 0;
    }

  if (pipefile)
    {
      FS_FCloseFile (pipefile);
      FS_HomeRemove (com_pipefile->string);
    }
}

//------------------------------------------------------------------------


/*
=====================
Q_acos

the msvc acos doesn't always return a value between -PI and PI:

int i;
i = 1065353246;
acos(*(float*) &i) == -1.#IND0

	This should go in q_math but it is too late to add new traps
	to game and ui
=====================
*/
/*
float Q_acos(float c) {
	float angle;

	angle = acos(c);

	if (angle > M_PI) {
		return (float)M_PI;
	}
	if (angle < -M_PI) {
		return (float)M_PI;
	}
	return angle;
}
*/

/*
===========================================
command line completion
===========================================
*/

/*
==================
Field_Clear
==================
*/
void
Field_Clear (field_t *edit)
{
  Com_Memset (edit->buffer, 0, MAX_EDIT_LINE);
  edit->cursor = 0;
  edit->scroll = 0;
}

static const char	*completionString;
static char		shortestMatch[MAX_TOKEN_CHARS];
static int		matchCount;
// field we are working on, passed to Field_AutoComplete(&g_consoleCommand for instance)
static field_t		*completionField;

/*
===============
FindMatches

===============
*/
static void
FindMatches (const char *s)
{
  int	i;

  if (Q_stricmpn(s, completionString, strlen(completionString)))
    {
      return;
    }
  matchCount++;
  if (matchCount == 1)
    {
      Q_strncpyz (shortestMatch, s, sizeof(shortestMatch));
      return;
    }

  // cut shortestMatch to the amount common with s
  for (i = 0; shortestMatch[i]; i++)
    {
      if (i >= strlen(s))
	{
	  shortestMatch[i] = 0;
	  break;
	}

      if (tolower(shortestMatch[i]) != tolower(s[i]))
	{
	  shortestMatch[i] = 0;
	}
    }
}

/*
===============
PrintMatches

===============
*/
static void
PrintMatches (const char *s)
{
  if (!Q_stricmpn(s, shortestMatch, strlen(shortestMatch)))
    {
      qci->Printf ("    %s\n", s);
    }
}

/*
===============
PrintCvarMatches

===============
*/
static void
PrintCvarMatches (const char *s)
{
  char value[ TRUNCATE_LENGTH ];

  if (!Q_stricmpn(s, shortestMatch, strlen(shortestMatch)))
    {
      Com_TruncateLongString (value, Cvar_VariableString(s));
      qci->Printf ("    %s = \"%s\"\n", s, value);
    }
}

/*
===============
Field_FindFirstSeparator
===============
*/
static char *
Field_FindFirstSeparator (char *s)
{
  int	i;

  for (i = 0; i < strlen(s); i++)
    {
      if (s[ i ] == ';')
	return &s[i];
    }

  return NULL;
}

/*
===============
Field_Complete
===============
*/
static bool
Field_Complete (void)
{
  int	completionOffset;

  if (matchCount == 0)
    return true;

  completionOffset = strlen(completionField->buffer) - strlen(completionString);

  Q_strncpyz (&completionField->buffer[completionOffset], shortestMatch,
	      sizeof(completionField->buffer) - completionOffset);

  completionField->cursor = strlen(completionField->buffer);

  if (matchCount == 1)
    {
      Q_strcat (completionField->buffer, sizeof(completionField->buffer), " ");
      completionField->cursor++;
      return true;
    }

  qci->Printf ("]%s\n", completionField->buffer);

  return false;
}

/*
===============
Field_CompleteKeyname
===============
*/
void
Field_CompleteKeyname (void)
{
  matchCount = 0;
  shortestMatch[ 0 ] = 0;

  qci->Key_KeynameCompletion (FindMatches);

  if (!Field_Complete ())
    qci->Key_KeynameCompletion (PrintMatches);
}

/*
===============
Field_CompleteFilename
===============
*/
void
Field_CompleteFilename (const char *dir,
			const char *ext, bool stripExt)
{
  matchCount = 0;
  shortestMatch[0] = 0;

  FS_FilenameCompletion (dir, ext, stripExt, FindMatches);

  if (!Field_Complete())
    FS_FilenameCompletion (dir, ext, stripExt, PrintMatches);
}

/*
===============
Field_CompleteCommand
===============
*/
void
Field_CompleteCommand (char *cmd,
		       bool doCommands, bool doCvars)
{
  int	completionArgument = 0;

  // Skip leading whitespace and quotes
  cmd = Com_SkipCharset (cmd, " \"");

  Cmd_TokenizeStringIgnoreQuotes (cmd);
  completionArgument = Cmd_Argc ();

  // If there is trailing whitespace on the cmd
  if (*(cmd + strlen(cmd) - 1) == ' ')
    {
      completionString = "";
      completionArgument++;
    }
  else
    completionString = Cmd_Argv (completionArgument - 1);

  if (qcom_flags & QCFLAG_CLIENT)
    {
      // Unconditionally add a '\' to the start of the buffer
      if ((completionField->buffer[0])
	  && (completionField->buffer[0] != '\\'))
	{
	  if (completionField->buffer[0] != '/')
	    {
	      // Buffer is full, refuse to complete
	      if ((strlen (completionField->buffer) + 1) >=
		  sizeof (completionField->buffer))
		return;

	      memmove (&completionField->buffer[1],
		       &completionField->buffer[0],
		       strlen (completionField->buffer) + 1);
	      completionField->cursor++;
	    }

	  completionField->buffer[0] = '\\';
	}
    }

  if (completionArgument > 1)
    {
      const char *baseCmd = Cmd_Argv (0);
      char	*p;

      //#ifndef DEDICATED
      // This should always be true
      if (baseCmd[0] == '\\' || baseCmd[0] == '/')
	baseCmd++;
      //#endif

      if ((p = Field_FindFirstSeparator (cmd)))
	Field_CompleteCommand (p + 1, true, true); // Compound command
      else
	Cmd_CompleteArgument (baseCmd, cmd, completionArgument);
    }
  else
    {
      if (completionString[0] == '\\' || completionString[0] == '/')
	completionString++;

      matchCount = 0;
      shortestMatch[0] = 0;

      if (strlen(completionString) == 0)
	return;

      if (doCommands)
	Cmd_CommandCompletion (FindMatches);

      if (doCvars)
	Cvar_CommandCompletion (FindMatches);

      if (!Field_Complete())
	{
	  // run through again, printing matches
	  if (doCommands)
	    Cmd_CommandCompletion (PrintMatches);

	  if (doCvars)
	    Cvar_CommandCompletion (PrintCvarMatches);
	}
    }
}

/*
===============
Field_AutoComplete

Perform Tab expansion
===============
*/
void
Field_AutoComplete (field_t *field)
{
  completionField = field;

  Field_CompleteCommand (completionField->buffer, true, true);
}

/*
==================
Com_RandomBytes

fills string array with len radom bytes, peferably from the OS randomizer
==================
*/
void
Com_RandomBytes (byte *string, int len)
{
  int i;

  if (qci->Sys_RandomBytes (string, len))
    return;

  qci->Printf ("Com_RandomBytes: using weak randomization\n");
  for (i = 0; i < len; i++)
    string[i] = (unsigned char)(rand() % 255);
}



void
Com_PrintLog (const char * msg)
{
  static bool opening_qconsole = false;

  // logfile
  if (com_logfile && com_logfile->integer)
    {
      // TTimo: only open the qconsole.log if the filesystem is in an initialized state
      //   also, avoid recursing in the qconsole.log opening (i.e. if fs_debug is on)
      if (!logfile && FS_Initialized() && !opening_qconsole)
	{
	  struct tm	*newtime;
	  time_t	aclock;

	  opening_qconsole = true;

	  time (&aclock);
	  newtime = localtime (&aclock);

	  logfile = FS_FOpenFileWrite ("qconsole.log");

	  if (logfile)
	    {
	      qci->Printf ("logfile opened on %s\n", asctime(newtime));

	      if (com_logfile->integer > 1)
		{
		  // force it to not buffer so we get valid
		  // data even if we are crashing
		  FS_ForceFlush (logfile);
		}
	    }
	  else
	    {
	      qci->Printf ("Opening qconsole.log failed!\n");
	      Cvar_Set ("logfile", "0");
	    }

	  opening_qconsole = false;
	}
      if (logfile && FS_Initialized())
	{
	  FS_Write (msg, strlen(msg), logfile);
	}
    }
}

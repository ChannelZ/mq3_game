/*
Gabriel Schnoering

GPLv3 or later

2010-2011

 */
/*
==============================================================

PROTOCOL

==============================================================
*/
#define DEMOEXT			"dm_"
#define	PROTOCOL_VERSION	68

#define GAMENAME_FOR_MASTER	"iomQ3"	// must NOT contain whitespaces
//#define HEARTBEAT_GAME		"QuakeArena-1"
#define HEARTBEAT_FOR_MASTER	GAMENAME_FOR_MASTER
#define FLATLINE_FOR_MASTER	GAMENAME_FOR_MASTER "dead"

#define	UPDATE_SERVER_NAME	"update.quake3arena.com"
// override on command line, config files etc.
#ifndef MASTER_SERVER_NAME
#define MASTER_SERVER_NAME	"master.quake3arena.com"
#endif

#define	PORT_MASTER		27950
#define	PORT_UPDATE		27951
#define	PORT_SERVER		27960
#define	NUM_SERVER_PORTS	4		// broadcast scan this many ports after


// referenced flags
// these are in loop specific order so don't change the order
#define FS_GENERAL_REF	0x01
#define FS_UI_REF	0x02
#define FS_CGAME_REF	0x04
#define FS_QAGAME_REF	0x08

// cvars
#define	MAX_CVAR_VALUE_STRING	256

#define	CVAR_ARCHIVE		1	// set to cause it to be saved to vars.rc
					// used for system variables, not for player
					// specific configurations
#define	CVAR_USERINFO		2	// sent to server on connect or change
#define	CVAR_SERVERINFO		4	// sent in response to front end requests
#define	CVAR_SYSTEMINFO		8	// these cvars will be duplicated on all clients
#define	CVAR_INIT		16	// don't allow change from console at all,
					// but can be set from the command line
#define	CVAR_LATCH		32	// will only change when C code next does
					// a Cvar_Get(), so it can't be changed
					// without proper initialization.  modified
					// will be set, even though the value hasn't
					// changed yet
#define	CVAR_ROM		64	// display only, cannot be set by user at all
#define	CVAR_USER_CREATED	128	// created by a set command
#define	CVAR_TEMP		256	// can be set even when cheats are disabled, but is not archived
#define	CVAR_CHEAT		512	// can not be changed if cheats are disabled
#define	CVAR_NORESTART		1024	// do not clear when a cvar_restart is issued

#define	CVAR_SERVER_CREATED	2048	// cvar was created by a server the client connected to.
#define	CVAR_NONEXISTENT	0xFFFFFFFF	// Cvar doesn't exist.

// theses are new ones, used as return value from Cvar_Set and Cvar_Get
// gives infos about actions on cvars
#define CVAR_STATUS_NONEXISTENT	0	// cvar doesn't exist
#define CVAR_STATUS_CREATED	1	// cvar was created
#define CVAR_STATUS_MODIFIED	2	// modified an existing cvar
#define CVAR_STATUS_NOTALLOWED	4	// not allowed to modify the cvar

// network (net_ip and net_chan)
#define	MAX_MSGLEN		16384	// max length of a message, which may
					// be fragmented into multiple packets

#define MAX_DOWNLOAD_WINDOW	8	// max of eight download frames
#define MAX_DOWNLOAD_BLKSIZE	2048	// 2048 byte block chunks

#define NET_ENABLEV4		0x01
#define NET_ENABLEV6		0x02
// if this flag is set, always attempt ipv6 connections instead of ipv4 if a v6 address is found.
#define NET_PRIOV6		0x04
// disables ipv6 multicast support if set.
#define NET_DISABLEMCAST	0x08

#define	PACKET_BACKUP		32	// number of old messages that must be kept on client and
					// server for delta comrpession and ping estimation
#define	PACKET_MASK		(PACKET_BACKUP-1)

#define	MAX_PACKET_USERCMDS	32	// max number of usercmd_t in a packet

#define	PORT_ANY		-1

#define	MAX_RELIABLE_COMMANDS	64	// max string commands buffered for restransmit

#define NET_ADDRSTRMAXLEN 48	// maximum length of an IPv6 address string
				// including trailing '\0'

typedef void (*xcommand_t) (void);
typedef void (*completionFunc_t)( char *args, int argNum );

enum netsrc_s
{
  NS_CLIENT,
  NS_SERVER
};
typedef enum netsrc_s netsrc_t;

enum netadrtype_s
  {
    NA_BAD = 0,			// an address lookup failed
    NA_BOT,
    NA_LOOPBACK,
    NA_BROADCAST,
    NA_IP,
    NA_IP6,
    NA_MULTICAST6,
    NA_UNSPEC
  };
typedef enum netadrtype_s netadrtype_t;

struct netadr_s
{
  netadrtype_t	type;

  byte		ip[4];
  byte		ip6[16];

  unsigned short port;
  unsigned long	scope_id;	// Needed for IPv6 link-local addresses
};
typedef struct netadr_s netadr_t;

// (net_chan)

/*
Netchan handles packet fragmentation and out of order / duplicate suppression
*/
struct netchan_s
{
  netsrc_t	sock;

  int		dropped;		// between last packet and previous

  netadr_t	remoteAddress;
  int		qport;			// qport value to write when transmitting

  // sequencing variables
  int		incomingSequence;
  int		outgoingSequence;

  // incoming fragment assembly buffer
  int		fragmentSequence;
  int		fragmentLength;
  byte		fragmentBuffer[MAX_MSGLEN];

  // outgoing fragment buffer
  // we need to space out the sending of large fragmented messages
  bool	unsentFragments;
  int		unsentFragmentStart;
  int		unsentLength;
  byte		unsentBuffer[MAX_MSGLEN];
};
typedef struct netchan_s netchan_t;



struct msg_s
{
  bool	allowoverflow;	// if false, do a Com_Error
  bool	overflowed;	// set to true if the buffer size failed (with allowoverflow set)
  bool	oob;		// set to true if the buffer size failed (with allowoverflow set)
  byte		*data;
  int		maxsize;
  int		cursize;
  int		readcount;
  int		bit;		// for bitwise reads and writes
};
typedef struct msg_s msg_t;


// the svc_strings[] array in cl_parse.c should mirror this
//
// server to client
//
enum svc_ops_e {
	svc_bad,
	svc_nop,
	svc_gamestate,
	svc_configstring,			// [short] [string] only in gamestate messages
	svc_baseline,				// only in gamestate messages
	svc_serverCommand,			// [string] to be executed by client game module
	svc_snapshot,
	svc_EOF,
};

//
// client to server
//
enum clc_ops_e {
	clc_bad,
	clc_nop,
	clc_move,			// [[usercmd_t]
	clc_moveNoDelta,		// [[usercmd_t]
	clc_clientCommand,		// [string] message
	clc_EOF,
};

// memory flags
enum ha_pref_en
{
    h_high,
    h_low,
    h_dontcare
};
typedef enum ha_pref_en ha_pref;

enum memtag_e
{
  TAG_FREE = 0,
  TAG_GENERAL,
  TAG_BOTLIB,
  TAG_RENDERER,
  TAG_SOUND,
  TAG_SMALL,
  TAG_STATIC
};
typedef enum memtag_e memtag_t;


// nothing outside the Cvar_*() functions should modify these fields!
struct cvar_s
{
  char		*name;
  char		*string;
  char		*resetString;		// cvar_restart will reset to this value
  char		*latchedString;		// for CVAR_LATCH vars
  int		flags;
  bool	modified;		// set each time the cvar is changed
  int		modificationCount;	// incremented each time the cvar is changed
  float		value;			// atof( string )
  int		integer;		// atoi( string )
  bool	validate;
  bool	integral;
  float		min;
  float		max;
  bool	trackme;		// call the trackfunc callback on change
  void		(*trackfunc) (struct cvar_s * var);
  struct cvar_s	*next;
  struct cvar_s	*hashNext;
};
typedef struct cvar_s cvar_t;


#define	MAX_EDIT_LINE	256
typedef struct
{
  int		cursor;
  int		scroll;
  int		widthInChars;
  char	buffer[MAX_EDIT_LINE];
} field_t;


#define	SV_ENCODE_START		4
#define SV_DECODE_START		12
#define	CL_ENCODE_START		12
#define CL_DECODE_START		4

// flags for sv_allowDownload and cl_allowDownload
#define DLF_ENABLE	1
#define DLF_NO_REDIRECT 2
#define DLF_NO_UDP	4
#define DLF_NO_DISCONNECT 8

typedef enum
{
  // SE_NONE must be zero
  SE_NONE = 0,	// evTime is still valid
  SE_KEY,	// evValue is a key code, evValue2 is the down flag
  SE_CHAR,	// evValue is an ascii char
  SE_MOUSE,	// evValue and evValue2 are reletive signed x / y moves
  // SE_JOYSTICK_AXIS,	// evValue is an axis number and evValue2 is the current state (-127 to 127)
  SE_CONSOLE,	// evPtr is a char*
  SE_PACKET	// evPtr is a netadr_t followed by data bytes to evPtrLength
} sysEventType_t;

typedef struct
{
  int			evTime;
  sysEventType_t	evType;
  int			evValue, evValue2;
  int			evPtrLength;	// bytes of data pointed to by evPtr, for journaling
  void			*evPtr;		// this must be manually freed if not NULL
} sysEvent_t;

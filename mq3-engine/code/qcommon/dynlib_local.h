/* * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Game library - Gabriel Schnoering. GPLv3
 *
 * Dynamic Library Definitions and Functions
 *
 * * * * * * * * * * * * * * * * * * * * * * * * */


// should not be in this file (temp)
struct Dlib_s
{
  char	libname[MAX_QPATH];
  char	fqpath[MAX_QPATH+1];
  void	*dynHandle;
  void	(*close) (struct Dlib_s self);
};
typedef struct Dlib_s Dlib_t;

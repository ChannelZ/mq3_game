/*







 */

/* Qcommon_public interface headers
 *
 * defines qcom_import_s and qcom_export_s structures
 *
 *
 */

/*
 * struct qcom_import_s
 *
 * What we will need for the library to work
 *
 */

#include <setjmp.h>

enum pg_status_e
  {
    sleeping = 0,
    running = 1
  };
typedef enum pg_status_e pg_status;


struct qcom_import_s
{
  void (*shutdown) (const char *string);
  bool (*gamecommand) (void); // used by Cmd_ExecuteString ()

  jmp_buf *(*get_jmpframe) (void);

  pg_status (*get_program_status) (void);

  // hack : do me properly ?!
  // split com_init in two ? - stage1 - hand back to client/server - stage2
  void (*CL_InitKeyCommands) (void);
  void (*CL_ForwardCommandToServer) (const char *string); // Cmd_ExecuteString ()
  // create a generic way to add completion sources ?
  void (*Key_KeynameCompletion) (void (*callback) (const char *s));
  void (*Key_WriteBindings) (fileHandle_t f);

  void (*Printf) (const char * fmt, ...);
  void (*DPrintf) (const char * fmt, ...);
  void (*Error) (int code, const char * fmt, ...);

  //void (*Sys_Error) (const char *error, ...) __attribute__ ((format (printf, 1, 2)));
  //void (*Sys_Print) (const char *msg);

  //void (*Sys_Quit) (void);

  const char *(*Sys_DefaultHomePath) (void);
  const char *(*Sys_DefaultInstallPath) (void);
  const char *(*Sys_DefaultAppPath) (void);

  unsigned int (*Sys_Milliseconds) (void);
  bool (*Sys_RandomBytes) (void *string, int len);
  bool (*Sys_Mkdir) (const char * path);
  FILE *(*Sys_Mkfifo) (const char *ospath);

  const char *(*Sys_ConsoleInput) (void);

  char **(*Sys_ListFiles) (const char *directory, const char *extension, char *filter, int *numfiles, bool wantsubs);
  void (*Sys_FreeFileList) (char **list);

  void (*Sys_SetEnv) (const char *name, const char *value);
};
typedef struct qcom_import_s qcom_import_t;


/*
 * struct qcom_export_s
 *
 * What we will the library provide
 *
 */

struct qcom_math_s
{
  int (*Q_isprint) (int c);
  int (*Q_islower) (int c);
  int (*Q_isupper) (int c);
  int (*Q_isalpha) (int c);
  bool (*Q_isanumber) (const char *s);
  bool (*Q_isintegral) (float f);

  float (*Q_fabs) (float f);
  float (*Q_sqrt) (float f);
  float (*Q_rsqrt) (float f);

};
typedef struct qcom_math_s qcom_math_t;


struct qcom_mem_s
{
  void (*TouchMemory) (void);
  void (*Hunk_Clear) (void);
  int  (*Hunk_MemoryRemaining) (void);
  void (*Hunk_SetMark) (void);
  void (*Hunk_ClearToMark) (void);
  bool (*Hunk_CheckMark) (void);
  void *(*Hunk_Alloc) (int size, ha_pref preference);
  void *(*Hunk_AllocateTempMemory) (int size);
  void (*Hunk_FreeTempMemory) (void *buf);
  void (*Hunk_ClearTempMemory) (void);

  int (*Z_AvailableMemory) (void);
  void (*Z_Free) (void *ptr);
  void (*Z_FreeTags) (int tag);
  void *(*Z_TagMalloc) (int size, int tag);
  void *(*Z_Malloc) (int size);
  void *(*S_Malloc) (int size);

  char *(*CopyString) (const char *in);
};
typedef struct qcom_mem_s qcom_mem_t;


struct qcom_cvar_s
{
  cvar_t *(*Get) (const char *var_name, const char *value, int flags);

  int (*Set) (const char *var_name, const char *value);
  int (*SetNew) (cvar_t * var, const char * value);

  // avoid using these as much as possible
  float (*VariableValue) (const char *var_name);
  int (*VariableIntegerValue) (const char *var_name);
  char *(*VariableString) (const char *var_name);
  void (*VariableStringBuffer) (const char *var_name, char *buffer, int bufsize);

  unsigned int (*Flags) (const char *var_name);
  void (*Reset) (const char *var_name);

  char *(*InfoString) (int bit);
  char *(*InfoString_Big) (int bit);
  void (*InfoStringBuffer) (int bit, char *buff, int buffsize);

  void (*CheckRange) (cvar_t *cv, float minVal, float maxVal, bool shouldBeIntegral);
  void (*TrackChange) (cvar_t *cv, void (*func)(cvar_t *var));
  int (*Get_ModifiedFlags) (void);
  void (*Set_ModifiedFlags) (int flag);

  void (*SetCheatState) (void);
};
typedef struct qcom_cvar_s qcom_cvar_t;

struct qcom_cmd_s
{
  void (*AddCommand) (const char *cmd_name, xcommand_t function);
  void (*RemoveCommand) (const char *cmd_name);

  int (*Argc) (void);
  char *(*Argv) (int arg);
  void (*ArgvBuffer) (int arg, char *buffer, int bufferLength);
  char *(*Args) (void);
  char *(*ArgsFrom) (int arg);
  void (*ArgsBuffer) (char *buffer, int bufferLength);
  void (*TokenizeString) (const char *text);
  void (*TokenizeStringIgnoreQuotes) (const char *text);
  void (*Args_Sanitize) (void);

  void (*ExecuteString) (const char *text);
  char *(*Cmd) (void);
  void (*SetCommandCompletionFunc) (const char *command, completionFunc_t complete);

  void (*Cbuf_ExecuteText) (int exec_when, const char *text);
  void (*Cbuf_AddText) (const char *text);
  void (*Cbuf_Execute) (void);
};
typedef struct qcom_cmd_s qcom_cmd_t;


struct qcom_net_s
{
  void (*SendPacket) (netsrc_t sock, int length, const void *data, netadr_t to);

  int (*Sleep) (int msec);
  int (*Sleep3) (int msec, int inputfd);

  void (*OutOfBandPrint) (netsrc_t net_socket, netadr_t adr,
			  const char *format, ...) __attribute__ ((format (printf, 3, 4)));
  void (*OutOfBandData) (netsrc_t sock, netadr_t adr, byte *format, int len);

  void (*Init) (void);
  void (*Shutdown) (void);

  bool (*IsLocalAddress) (netadr_t adr);
  bool (*IsLANAddress) (netadr_t adr);

  bool (*CompareAdr) (netadr_t a, netadr_t b);
  bool (*CompareBaseAdrMask) (netadr_t a, netadr_t b, int netmask);

  const char* (*AdrToString) (netadr_t a);
  const char* (*AdrToStringwPort) (netadr_t a);
  int (*StringToAdr) (const char *s, netadr_t *a, netadrtype_t family);
  void (*ShowIP) (void);

  void (*FlushPacketQueue) (void);
  bool (*GetLoopPacket) (netsrc_t sock, netadr_t *net_from, msg_t *net_message);

  void (*JoinMulticast6) (void);
  void (*LeaveMulticast6) (void);

  void (*Netchan_Setup) (netsrc_t sock, netchan_t *chan, netadr_t adr, int qport);
  void (*Netchan_Transmit) (netchan_t *chan, int length, const byte *data);
  void (*Netchan_TransmitNextFragment) (netchan_t *chan);

  bool (*Netchan_Process) (netchan_t *chan, msg_t *msg);
};
typedef struct qcom_net_s qcom_net_t;

struct qcom_cm_s
{
  void (*LoadMap) (const char *name, bool clientload, int *checksum);
  void (*ClearMap) (void);
  clipHandle_t (*InlineModel) (int index);	// 0 = world, 1 + are bmodels
  clipHandle_t (*TempBoxModel) (const vec3_t mins, const vec3_t maxs);

  void (*ModelBounds) (clipHandle_t model, vec3_t mins, vec3_t maxs);

  int (*NumClusters) (void);
  int (*NumInlineModels) (void);
  char *(*EntityString) (void);

  // returns an ORed contents mask
  int (*PointContents) (const vec3_t p, clipHandle_t model);
  int (*TransformedPointContents) (const vec3_t p, clipHandle_t model,
				   const vec3_t origin, const vec3_t angles);

  void (*BoxTrace) (trace_t *results, const vec3_t start, const vec3_t end,
		    const vec3_t mins, const vec3_t maxs,
		    clipHandle_t model, int brushmask);
  void (*TransformedBoxTrace) (trace_t *results,
			       const vec3_t start, const vec3_t end,
			       const vec3_t mins, const vec3_t maxs,
			       clipHandle_t model, const vec3_t origin,
			       int brushmask);

  byte *(*ClusterPVS) (int cluster);

  int (*PointLeafnum) (const vec3_t p);

  // only returns non-solid leafs
  // overflow if return listsize and if *lastLeaf != list[listsize-1]
  int (*BoxLeafnums) (const vec3_t mins, const vec3_t maxs, int *list,
		      int listsize, int *lastLeaf);

  int (*LeafCluster) (int leafnum);
  int (*LeafArea) (int leafnum);

  void (*AdjustAreaPortalState) (int area1, int area2, bool open) ;
  bool (*AreasConnected) (int area1, int area2);

  int (*WriteAreaBits) (byte *buffer, int area);

  int (*LerpTag) (orientation_t *tag, clipHandle_t model,
		  int startFrame, int endFrame,
		  float frac, const char *tagName);


  int (*MarkFragments) (int numPoints, const vec3_t *points,
			const vec3_t projection, int maxPoints,
			vec3_t pointBuffer, int maxFragments,
			markFragment_t *fragmentBuffer);

  void (*DrawDebugSurface) (void (*drawPoly)(int color,
					     int numPoints,
					     float *points));
};
typedef struct qcom_cm_s qcom_cm_t;


struct qcom_msg_s
{
  void (*Init) (msg_t *buf, byte *data, int length);
  void (*WriteData) (msg_t *buf, const void *data, int length);
  void (*Bitstream) (msg_t *buf);

  void (*WriteBits) (msg_t *msg, int value, int bits);
  void (*WriteByte) (msg_t *sb, int c);
  void (*WriteShort) (msg_t *sb, int c);
  void (*WriteLong) (msg_t *sb, int c);
  void (*WriteString) (msg_t *sb, const char *s);
  void (*WriteBigString) (msg_t *sb, const char *s);

  void (*BeginReading) (msg_t *sb);
  void (*BeginReadingOOB) (msg_t *sb);

  int (*ReadBits) (msg_t *msg, int bits);
  int (*ReadByte) (msg_t *sb);
  int (*ReadShort) (msg_t *sb);
  int (*ReadLong) (msg_t *sb);

  char *(*ReadString) (msg_t *sb);
  char *(*ReadBigString) (msg_t *sb);
  char *(*ReadStringLine) (msg_t *sb);

  void (*ReadData) (msg_t *sb, void *buffer, int size);
  int (*LookaheadByte) (msg_t *msg);

  void (*WriteDeltaUsercmd) (msg_t *msg, struct usercmd_s *from,
			     struct usercmd_s *to);
  void (*ReadDeltaUsercmd) (msg_t *msg, struct usercmd_s *from,
			    struct usercmd_s *to);

  void (*WriteDeltaUsercmdKey) (msg_t *msg, int key,
				usercmd_t *from, usercmd_t *to) ;
  void (*ReadDeltaUsercmdKey) (msg_t *msg, int key,
			       usercmd_t *from, usercmd_t *to);

  void (*WriteDeltaEntity) (msg_t *msg, struct entityState_s *from,
			    struct entityState_s *to, bool force );

  void (*ReadDeltaEntity) (msg_t *msg, entityState_t *from,
			   entityState_t *to, int number);

  void (*WriteDeltaPlayerstate) (msg_t *msg, struct playerState_s *from,
				 struct playerState_s *to);
  void (*ReadDeltaPlayerstate) (msg_t *msg, struct playerState_s *from,
				struct playerState_s *to);

  void (*Copy) (msg_t *buf, byte *data, int length, msg_t *src);
  void (*Clear) (msg_t *buf);
};
typedef struct qcom_msg_s qcom_msg_t;


struct qcom_fs_s
{
  bool (*Initialized) (void);
  void (*Shutdown) (bool closemfp);
  void (*Restart) (int checksumFeed);
  bool (*ConditionalRestart) (int checksumFeed);
  char **(*ListFiles) (const char *directory, const char *extension,
		       int *numfiles);
  void (*FreeFileList) (char **list);
  bool (*FileExists) (const char *file);
  int (*GetFileList) (const char *path, const char *extension,
		      char *listbuf, int bufsize);

  char *(*BuildOSPath) (const char *base, const char *game, const char *qpath);

  int (*FOpenFileByMode) (const char *qpath, fileHandle_t *f, fsMode_t mode);
  fileHandle_t (*FOpenFileWrite) (const char *qpath);
  fileHandle_t (*FOpenFileAppend) (const char *filename);
  int (*FOpenFileRead) (const char *qpath, fileHandle_t *file,
			bool uniqueFILE);

  fileHandle_t (*SV_FOpenFileWrite) (const char *filename);
  int (*SV_FOpenFileRead) (const char *filename, fileHandle_t *fp);
  void (*SV_Rename) (const char *from, const char *to);

  int (*FileIsInPAK) (const char *filename, int *pChecksum);

  int (*Write) (const void *buffer, int len, fileHandle_t f);
  int (*Read) (void *buffer, int len, fileHandle_t f);
  int (*Read2) (void *buffer, int len, fileHandle_t f);

  void (*FCloseFile) (fileHandle_t f);
  int (*ReadFile) (const char *qpath, void **buffer);
  void (*FreeFile) (void *buffer);
  void (*ForceFlush) (fileHandle_t f);

  void (*WriteFile) (const char *qpath, const void *buffer, int size);

  int (*FTell) (fileHandle_t f);
  void (*Printf) (fileHandle_t f, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));

  int (*Seek) (fileHandle_t f, long offset, int origin);

  const char *(*LoadedPakNames) (void);
  const char *(*LoadedPakChecksums) (void);
  const char *(*ReferencedPakNames) (void);
  const char *(*ReferencedPakChecksums) (void);

  void (*ClearPakReferences) (int flags);
  bool (*CheckDirTraversal) (const char *checkdir);
  bool (*ComparePaks) (char *neededpaks, int len, bool dlstring);
  bool (*FilenameCompare) (const char *s1, const char *s2);
  bool (*idPak) (char *pak, char *base);
  void (*FilenameListByExt) (const char *dir, const char *ext,
			     const char *start, char *buff, size_t bufsize);
  bool (*CreatePath) (char *OSPath);

  const char * (*validGamePath) (void);

  void (*Field_Clear) (field_t *edit);
  void (*Field_AutoComplete) (field_t *edit);
  void (*Field_CompleteKeyname) (void);
  void (*Field_CompleteFilename) (const char *dir, const char *ext,
				  bool stripExt);
  void (*Field_CompleteCommand) (char *cmd, bool doCommands,
				 bool doCvars);
};
typedef struct qcom_fs_s qcom_fs_t;

struct qcom_event_s
{
  sysEvent_t (*GetEvent) (void);
  void (*TagEvents) (void);
  void (*FreeEvent) (sysEvent_t* ev);
  void (*QueueEvent) (int time, sysEventType_t type, int value,
		      int value2, int ptrLength, void *ptr);
  int (*Milliseconds) (void); // current time based on events
};
typedef struct qcom_event_s qcom_event_t;

struct qcom_export_s
{
  void (*Com_Init) (char *commandLine);
  void (*Com_Shutdown) (void);
  void (*Com_PrintLog) (const char * msg);

  qcom_cmd_t cmd;
  qcom_cvar_t cvar;
  qcom_mem_t mem;
  qcom_msg_t msg;
  qcom_net_t net;
  qcom_fs_t fs;
  qcom_cm_t cm;
  qcom_event_t event;

  void (*Huff_Decompress) (msg_t * msg, int offset);
  int (*Com_RealTime) (qtime_t * time);
  int (*Com_HashKey) (char * string, int maxlen);
  void (*Info_Print) (const char * s);
};
typedef struct qcom_export_s qcom_export_t;

//qcom_export_t * CommonAPI (qcom_import_t * qimp, int flags);

#define QCFLAG_DEDICATED 1
#define QCFLAG_CLIENT 2

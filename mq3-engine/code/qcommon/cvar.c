/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
// cvar.c -- dynamic variable tracking

#include "q_shared.h"
#include "qcommon.h"

// head of the linked list
cvar_t * cvar_vars;
int cvar_modifiedFlags;

#define MAX_CVARS 1024
cvar_t cvar_indexes[MAX_CVARS];
int cvar_numIndexes;

#define FILE_HASH_SIZE 256
static cvar_t * cvarHashTable[FILE_HASH_SIZE];

/*
================
return a hash value for the filename
================
*/
static long
generateHashValue (const char *fname)
{
  int i;
  long hash;
  char letter;

  hash = 0;
  i = 0;
  while (fname[i] != '\0')
    {
      letter = tolower (fname[i]);
      hash += (long)(letter)*(i+119);
      i++;
    }
  hash &= (FILE_HASH_SIZE-1);
  return hash;
}

/*
============
Cvar_ValidateString
============
*/
static bool
Cvar_ValidateString (const char *s)
{
  if (!s)
    {
      return false;
    }
  if (strchr (s, '\\'))
    {
      return false;
    }
  if (strchr (s, '\"'))
    {
      return false;
    }
  if (strchr (s, ';'))
    {
      return false;
    }
  return true;
}

/*
============
Cvar_FindVar
============
*/
static cvar_t *
Cvar_FindVar (const char *var_name)
{
  cvar_t *var;
  long hash;

  hash = generateHashValue (var_name);

  for (var = cvarHashTable[hash]; var; var = var->hashNext)
    {
      if (!Q_stricmp (var_name, var->name))
	{
	  return var;
	}
    }

  return NULL;
}

/*
============
Cvar_VariableValue
============
*/
float
Cvar_VariableValue (const char *var_name)
{
  cvar_t *var;

  var = Cvar_FindVar (var_name);
  if (var == NULL)
    return 0.0f;
  return var->value;
}

/*
============
Cvar_VariableIntegerValue
============
*/
int
Cvar_VariableIntegerValue (const char *var_name)
{
  cvar_t *var;

  var = Cvar_FindVar (var_name);
  if (var == NULL)
    return 0;
  return var->integer;
}

/*
============
Cvar_VariableString
============
*/
char *
Cvar_VariableString (const char *var_name)
{
  cvar_t *var;

  var = Cvar_FindVar (var_name);
  if (var == NULL)
    return "";
  return var->string;
}

/*
============
Cvar_VariableStringBuffer
============
*/
void
Cvar_VariableStringBuffer (const char *var_name, char *buffer, int bufsize)
{
  cvar_t *var;

  var = Cvar_FindVar (var_name);
  if (var == NULL)
    {
      buffer[0] = '\0';
    }
  else
    {
      Q_strncpyz (buffer, var->string, bufsize);
    }
}

/*
============
Cvar_Flags

Unused, but still available to game lib
============
*/
unsigned int
Cvar_Flags (const char *var_name)
{
  cvar_t *var;

  var = Cvar_FindVar (var_name);
  if (var == NULL)
    return CVAR_NONEXISTENT;
  else
    return var->flags;
}

/*
============
Cvar_Validate

To be sure some numerical cvars don't get funny numbers
like "rate abcd"
Also allows boundaries check. min and max allowed value.
============
*/
static const char *
Cvar_Validate (cvar_t *var, const char *value, bool warn)
{
  static char s[MAX_CVAR_VALUE_STRING];
  float valuef;
  bool changed = false;

  if (!var->validate)
    return value;

  if (value == NULL)
    return value;

  if (Q_isanumber (value))
    {
      valuef = atof (value);

      if (var->integral)
	{
	  if (!Q_isintegral (valuef))
	    {
	      if (warn)
		qci->Printf ("WARNING: cvar '%s' must be integral", var->name);

	      valuef = (int)valuef;
	      changed = true;
	    }
	}
    }
  else
    {
      if (warn)
	qci->Printf ("WARNING: cvar '%s' must be numeric", var->name);

      valuef = atof (var->resetString);
      changed = true;
    }

  if (valuef < var->min)
    {
      if (warn)
	{
	  if (changed)
	    qci->Printf (" and is");
	  else
	    qci->Printf ("WARNING: cvar '%s'", var->name);

	  if (Q_isintegral (var->min))
	    qci->Printf (" out of range (min %d)", (int)var->min);
	  else
	    qci->Printf (" out of range (min %f)", var->min);
	}

      valuef = var->min;
      changed = true;
    }
  else if (valuef > var->max)
    {
      if (warn)
	{
	  if (changed)
	    qci->Printf (" and is" );
	  else
	    qci->Printf ("WARNING: cvar '%s'", var->name);

	  if (Q_isintegral (var->max))
	    qci->Printf (" out of range (max %d)", (int)var->max);
	  else
	    qci->Printf (" out of range (max %f)", var->max);
	}

      valuef = var->max;
      changed = true;
    }

  if (changed)
    {
      if (Q_isintegral (valuef))
	{
	  Com_sprintf (s, sizeof (s), "%d", (int)valuef);

	  if (warn)
	    qci->Printf (", setting to %d\n", (int)valuef);
	}
      else
	{
	  Com_sprintf (s, sizeof (s), "%f", valuef);

	  if (warn)
	    qci->Printf (", setting to %f\n", valuef);
	}

      return s;
    }
  else
    return value;
}

/*
============
Cvar_Get

If the variable already exists, the value will not be set unless CVAR_ROM
The flags will be or'ed in if the variable exists.
============
*/
cvar_t *
Cvar_Get (const char *var_name, const char *var_value, int flags)
{
  cvar_t *var;
  long hash;

  if ((var_name == NULL) || (var_value == NULL))
    {
      qci->Error (ERR_FATAL, "Cvar_Get: NULL parameter");
    }

  if (!Cvar_ValidateString (var_name))
    {
      qci->Printf ("invalid cvar name string: %s\n", var_name);
      var_name = "BADNAME";
    }

#if 0	// FIXME: values with backslash happen
  if (!Cvar_ValidateString (var_value))
    {
      qci->Printf ("invalid cvar value string: %s\n", var_value);
      var_value = "BADVALUE";
    }
#endif

  var = Cvar_FindVar (var_name);
  if (var)
    {
      var_value = Cvar_Validate (var, var_value, false);

      // if the C code is now specifying a variable that the user already
      // set a value for, take the new value as the reset value
      if ((var->flags & CVAR_USER_CREATED)
	  && !(flags & CVAR_USER_CREATED) && var_value[0])
	{
	  var->flags &= ~CVAR_USER_CREATED;
	  Z_Free (var->resetString);
	  var->resetString = CopyString (var_value);

	  if (flags & CVAR_ROM)
	    {
	      // this variable was set by the user,
	      // so force it to value given by the engine.

	      if (var->latchedString)
		Z_Free (var->latchedString);

	      var->latchedString = CopyString (var_value);
	    }

	  // ZOID--needs to be set so that cvars the game sets as
	  // SERVERINFO get sent to clients
	  cvar_modifiedFlags |= flags;
	}

      // Make sure servers cannot mark engine-added variables as SERVER_CREATED
      if (var->flags & CVAR_SERVER_CREATED)
	{
	  if (!(flags & CVAR_SERVER_CREATED))
	    var->flags &= ~CVAR_SERVER_CREATED;
	}
      else
	{
	  if (flags & CVAR_SERVER_CREATED)
	    flags &= ~CVAR_SERVER_CREATED;
	}

      var->flags |= flags;

      // only allow one non-empty reset string without a warning
      if (!var->resetString[0])
	{
	  // we don't have a reset string yet
	  Z_Free (var->resetString);
	  var->resetString = CopyString (var_value);

	  // ZOID--needs to be set so that cvars the game sets as
	  // SERVERINFO get sent to clients
	  cvar_modifiedFlags |= flags;
	}
      else if (var_value[0] && Q_strcmp (var->resetString, var_value))
	{
	  qci->DPrintf ("Warning: cvar \"%s\" given initial values: \"%s\" and \"%s\"\n",
			var_name, var->resetString, var_value);
	}
      // if we have a latched string, take that value now
      if (var->latchedString)
	{
	  var->modified = true;
	  var->modificationCount++;

	  qci->DPrintf ("Cvar_Get : %s changing value from %s to %s\n",
			var->name, var->string, var->latchedString);


	  Z_Free (var->string);	// free the old value string

	  var->string = CopyString (var->latchedString);
	  var->value = atof (var->string);
	  var->integer = atoi (var->string);

	  Z_Free (var->latchedString);
	  var->latchedString = NULL;

	  // note what types of cvars have been modified
	  // (userinfo, archive, serverinfo, systeminfo)
	  cvar_modifiedFlags |= var->flags;
	}

      return var;
    }

  //
  // allocate a new cvar
  //
  if (cvar_numIndexes >= MAX_CVARS)
    {
      qci->Error (ERR_FATAL, "MAX_CVARS");
    }
  var = &cvar_indexes[cvar_numIndexes];
  cvar_numIndexes++;
  var->name = CopyString (var_name);
  var->string = CopyString (var_value);
  var->modified = true;
  var->modificationCount = 1;
  var->value = atof (var->string);
  var->integer = atoi (var->string);
  var->resetString = CopyString (var_value);
  //var->latchedString = NULL; // this is done automaticaly, cvar_indexes is static

  var->validate = false;

  // link the variable in
  var->next = cvar_vars;
  cvar_vars = var;

  var->flags = flags;
  // note what types of cvars have been modified (userinfo, archive, serverinfo, systeminfo)
  cvar_modifiedFlags |= var->flags;

  hash = generateHashValue (var_name);
  var->hashNext = cvarHashTable[hash];
  cvarHashTable[hash] = var;

  return var;
}

/*
============
Cvar_SetNew

Assumes all check were performed on var
returns CVAR_STATUS_MODIFIED if everything went fine (even for latched cvars)
============
*/
int
Cvar_SetNew (cvar_t * var, const char * value)
{
  // set to the default value
  if (value == NULL)
    {
      value = var->resetString;
    }

  value = Cvar_Validate (var, value, true);

  // manage latched cvar
  if (var->flags & CVAR_LATCH)
    {
      // if a latched value is already set
      if (var->latchedString)
	{
	  // don't change a thing if it the same
	  if (!Q_strcmp (value, var->latchedString))
	    return CVAR_STATUS_MODIFIED;
	  // otherwise free the memory for the new one
	  else
	    Z_Free (var->latchedString);
	}
      else
	{
	  // value already set, nothing to do
	  if (!Q_strcmp (value, var->string))
	    return CVAR_STATUS_MODIFIED;
	}

      // set the new latched value
      qci->Printf ("%s %s will be changed upon restarting to %s.\n",
		   var->name, var->string, value);
      var->latchedString = CopyString (value);
      var->modified = true;
      var->modificationCount++;

      // call the callback function even if the value is not yet taken
      if (var->trackme)
	{
	  // callback function
	  var->trackfunc (var);
	}

      return CVAR_STATUS_MODIFIED;
    }

  if (!Q_strcmp (value, var->string))
    return CVAR_STATUS_MODIFIED;	// not changed

  var->modified = true;
  var->modificationCount++;

  Z_Free (var->string);	// free the old value string

  var->string = CopyString (value);
  var->value = atof (var->string);
  var->integer = atoi (var->string);

  // note what types of cvars have been modified (userinfo, archive, serverinfo, systeminfo)
  cvar_modifiedFlags |= var->flags;

  // did we track this cvar's changes ? dont call for latched cvars, already done
  if (var->trackme && !(var->flags & CVAR_LATCH))
    {
      // callback function
      var->trackfunc (var);
    }

  return CVAR_STATUS_MODIFIED;
}

/*
============
Cvar_SetByName
============
*/
int
Cvar_SetByName (const char * var_name, const char * value,
		int checkflag, bool create, cvar_t ** varptr)
{
  cvar_t *var;

  if (!Cvar_ValidateString (var_name))
    {
      qci->Printf ("invalid cvar name string: %s\n", var_name);
      var_name = "BADNAME";
    }

  var = Cvar_FindVar (var_name);

  if (var == NULL)
    {
      if (create)
	{
	  qci->Printf ("Cvar_Set created %s : %s\n", var_name, value);
	  // using this the Cvar_FindVar check is redundant fix this
	  var = Cvar_Get (var_name, value, CVAR_USER_CREATED);

	  if (varptr != NULL)
	    *varptr = var;

	  return CVAR_STATUS_CREATED;
	}
      else
	{
	  if (varptr != NULL)
	    *varptr = NULL;

	  return CVAR_STATUS_NONEXISTENT;
	}
    }

  // we have a valid cvar, we might want to get direct acces to it
  if (varptr != NULL)
    *varptr = var;

  // check for specific cases
  if ((checkflag & CVAR_ROM) && (var->flags & CVAR_ROM))
    {
      qci->Printf ("%s is read only (CVAR_ROM).\n", var_name);
      return CVAR_STATUS_NOTALLOWED;
    }

  if ((checkflag & CVAR_INIT) && (var->flags & CVAR_INIT))
    {
      qci->Printf ("%s is write protected (CVAR_INIT).\n", var_name);
      return CVAR_STATUS_NOTALLOWED;
    }

  int cheat;
  cheat = Cvar_VariableIntegerValue ("sv_cheats");

  if ((checkflag & CVAR_CHEAT) && (var->flags & CVAR_CHEAT) && !cheat)
    {
      qci->Printf ("%s is cheat protected (CVAR_CHEAT).\n", var_name);
      return CVAR_STATUS_NOTALLOWED;
    }

  return Cvar_SetNew (var, value);
}

int Cvar_SetUser (const char * var_name, const char * value)
{
  return Cvar_SetByName (var_name, value, CVAR_INIT | CVAR_ROM | CVAR_CHEAT, true, NULL);
}

int Cvar_SetInit (const char * var_name, const char * value)
{
  return Cvar_SetByName (var_name, value, CVAR_INIT, true, NULL);
}

int Cvar_Set (const char * var_name, const char * value)
{
  return Cvar_SetByName (var_name, value, CVAR_INIT | CVAR_CHEAT, true, NULL);
}

/*
============
Cvar_SetValue
============
*/
/*
void
Cvar_SetValue (const char *var_name, float value)
{
  char val[32];

  if (value == (int)value)
    {
      Com_sprintf (val, sizeof (val), "%i",(int)value);
    }
  else
    {
      Com_sprintf (val, sizeof (val), "%f",value);
    }
  Cvar_Set (var_name, val);
}
*/

/*
============
Cvar_Reset
============
*/
void
Cvar_Reset (const char *var_name)
{
  Cvar_Set (var_name, NULL);
}

/*
============
Cvar_SetCheatState

Any testing variables will be reset to the safe values
============
*/
void
Cvar_SetCheatState (void)
{
  cvar_t *var;

  // set all default vars to the safe value
  for (var = cvar_vars; var; var = var->next)
    {
      if (var->flags & CVAR_CHEAT)
	{
	  // the CVAR_LATCHED|CVAR_CHEAT vars might escape the reset here
	  // because of a different var->latchedString
	  if (var->latchedString)
	    {
	      Z_Free (var->latchedString);
	      var->latchedString = NULL;
	    }

	  if (Q_strcmp (var->resetString, var->string))
	    {
	      Cvar_Set (var->name, var->resetString);
	    }
	}
    }
}

/*
============
Cvar_Print

Prints the value, default, and latched string of the given variable
============
*/
void
Cvar_Print (cvar_t *v)
{
  qci->Printf ("\"%s\" is:\"%s" S_COLOR_WHITE "\"",
	       v->name, v->string);

  if (!(v->flags & CVAR_ROM))
    {
      if (!Q_stricmp (v->string, v->resetString))
	{
	  qci->Printf (", the default" );
	}
      else
	{
	  qci->Printf (" default:\"%s" S_COLOR_WHITE "\"", v->resetString);
	}
    }

  qci->Printf ("\n");

  if (v->latchedString)
    {
      qci->Printf ("latched: \"%s\"\n", v->latchedString);
    }
}

/*
============
Cvar_Print_f

Prints the contents of a cvar
(preferred over Cvar_Command where cvar names and commands conflict)
============
*/
void
Cvar_Print_f (void)
{
  char *name;
  cvar_t *cv;

  if (Cmd_Argc() != 2)
    {
      qci->Printf ("usage: print <variable>\n");
      return;
    }

  name = Cmd_Argv (1);

  cv = Cvar_FindVar (name);

  if (cv)
    Cvar_Print (cv);
  else
    qci->Printf ("Cvar %s does not exist.\n", name);
}

/*
============
Cvar_Toggle_f

Toggles a cvar for easy single key binding
============
*/
void
Cvar_Toggle_f (void)
{
  int v;
  int val;

  if ((Cmd_Argc () != 2) && (Cmd_Argc () != 4))
    {
      qci->Printf ("usage: toggle <variable> [mod value]\n");
      return;
    }

  v = Cvar_VariableValue (Cmd_Argv (1));

  if (Cmd_Argc () == 2)
    {
      v ^= 1;
    }
  else
    {
      if (!Q_strcmp (Cmd_Argv (2), "mod"))
	{
	  val = atoi (Cmd_Argv (3));
	  v = (v+1) % val;
	}
      else
	{
	  qci->Printf ("usage: toggle <variable> [mod value]\n");
	  return;
	}
    }

  Cvar_Set (Cmd_Argv (1), va ("%i", v));
}

/*
============
Cvar_Set_f

Allows setting and defining of arbitrary cvars from console, even if they
weren't declared in C code.
============
*/
void
Cvar_Set_f (void)
{
  int c;
  cvar_t *v;
  int cvret;
  const char *cmd;

  c = Cmd_Argc ();
  cmd = Cmd_Argv (0);

  if (c < 2)
    {
      qci->Printf ("usage: %s <variable> <value>\n", cmd);
      return;
    }
  if (c == 2)
    {
      Cvar_Print_f ();
      return;
    }

  // this will add the USER_CREATED flag if the cvar didn't exist
  cvret = Cvar_SetByName (Cmd_Argv (1), Cmd_ArgsFrom (2),
			  (CVAR_INIT | CVAR_ROM | CVAR_CHEAT), true, &v);

  if (v == NULL)
    {
      qci->Error (ERR_FATAL, "Cvar_Set_f : NULL pointer");
    }

  switch (cmd[3])
    {
    case 'a':
      if (!(v->flags & CVAR_ARCHIVE))
	{
	  v->flags |= CVAR_ARCHIVE;
	  cvar_modifiedFlags |= CVAR_ARCHIVE;
	}
      break;
    case 'u':
      if (!(v->flags & CVAR_USERINFO))
	{
	  v->flags |= CVAR_USERINFO;
	  cvar_modifiedFlags |= CVAR_USERINFO;
	}
      break;
    case 's':
      if (!(v->flags & CVAR_SERVERINFO))
	{
	  v->flags |= CVAR_SERVERINFO;
	  cvar_modifiedFlags |= CVAR_SERVERINFO;
	}
      break;
    }
}

/*
============
Cvar_Reset_f
============
*/
void
Cvar_Reset_f (void)
{
  if (Cmd_Argc () != 2)
    {
      qci->Printf ("usage: reset <variable>\n");
      return;
    }

  Cvar_Reset (Cmd_Argv (1));
}

/*
============
Cvar_List_f
============
*/
void
Cvar_List_f (void)
{
  cvar_t *var;
  int i;
  char *match;

  if (Cmd_Argc () > 1)
    {
      match = Cmd_Argv (1);
    }
  else
    {
      match = NULL;
    }

  i = 0;
  for (var = cvar_vars; var; var = var->next, i++)
    {
      if (match && !COM_Filter (match, var->name, false))
	{
	  continue;
	}

      if (var->flags & CVAR_SERVERINFO)
	{
	  qci->Printf ("S");
	}
      else
	{
	  qci->Printf (" ");
	}

      if (var->flags & CVAR_SYSTEMINFO)
	{
	  qci->Printf ("s");
	}
      else
	{
	  qci->Printf (" ");
	}

      if (var->flags & CVAR_USERINFO)
	{
	  qci->Printf ("U");
	}
      else
	{
	  qci->Printf (" ");
	}

      if (var->flags & CVAR_ROM)
	{
	  qci->Printf ("R");
	}
      else
	{
	  qci->Printf (" ");
	}

      if (var->flags & CVAR_INIT)
	{
	  qci->Printf ("I");
	}
      else
	{
	  qci->Printf (" ");
	}

      if (var->flags & CVAR_ARCHIVE)
	{
	  qci->Printf ("A");
	}
      else
	{
	  qci->Printf (" ");
	}

      if (var->flags & CVAR_LATCH)
	{
	  qci->Printf ("L");
	}
      else
	{
	  qci->Printf (" ");
	}

      if (var->flags & CVAR_CHEAT)
	{
	  qci->Printf ("C");
	}
      else
	{
	  qci->Printf (" ");
	}

      if (var->flags & CVAR_USER_CREATED)
	{
	  qci->Printf ("?");
	}
      else
	{
	  qci->Printf (" ");
	}

      qci->Printf (" %s \"%s\"\n", var->name, var->string);
    }

  qci->Printf ("\n%i total cvars\n", i);
  qci->Printf ("%i cvar indexes\n", cvar_numIndexes);
}

/*
=====================
Cvar_InfoString
=====================
*/
char *
Cvar_InfoString (int bit)
{
  static char info[MAX_INFO_STRING];
  cvar_t *var;

  info[0] = '\0';

  for (var = cvar_vars; var; var = var->next)
    {
      if (var->flags & bit)
	{
	  Info_SetValueForKey (info, var->name, var->string);
	}
    }
  return info;
}

/*
=====================
Cvar_InfoString_Big

handles large info strings (CS_SYSTEMINFO)
=====================
*/
char *
Cvar_InfoString_Big (int bit)
{
  static char info[BIG_INFO_STRING];
  cvar_t *var;

  info[0] = '\0';

  for (var = cvar_vars; var; var = var->next)
    {
      if (var->flags & bit)
	{
	  Info_SetValueForKey_Big (info, var->name, var->string);
	}
    }
  return info;
}

/*
=====================
Cvar_InfoStringBuffer
=====================
*/
void
Cvar_InfoStringBuffer (int bit, char* buff, int buffsize)
{
  Q_strncpyz (buff, Cvar_InfoString (bit), buffsize);
}

/*
=====================
Cvar_TrackChange
=====================
*/
void
Cvar_TrackChange (cvar_t *var, void (*func) (cvar_t *var))
{
  if (func == NULL)
    {
      var->trackfunc = NULL;
      var->trackme = false;
    }
  else
    {
      var->trackme = true;
      var->trackfunc = func;
    }
}

/*
=====================
Cvar_CheckRange
=====================
*/
void
Cvar_CheckRange (cvar_t *var, float min, float max, bool integral)
{
  var->validate = true;
  var->min = min;
  var->max = max;
  var->integral = integral;

  // Force an initial range check
  //Cvar_Set (var->name, var->string);
  Cvar_Validate (var, var->string, true);
}

/*
============
Cvar_WriteVariables

Appends lines containing "set variable value" for all variables
with the archive flag set to true.
============
*/
void
Cvar_WriteVariables (fileHandle_t f)
{
  cvar_t *var;
  char buffer[1024];

  for (var = cvar_vars; var; var = var->next)
    {
      if (var->flags & CVAR_ARCHIVE)
	{
	  // write the latched value, even if it hasn't taken effect yet
	  if (var->latchedString)
	    {
	      if ((strlen (var->name) + strlen (var->latchedString) + 10) > sizeof (buffer))
		{
		  qci->Printf (S_COLOR_YELLOW "WARNING: value of variable "
			       "\"%s\" too long to write to file\n", var->name);
		  continue;
		}
	      Com_sprintf (buffer, sizeof (buffer), "seta %s \"%s\"\n",
			   var->name, var->latchedString);
	    }
	  else
	    {
	      if ((strlen (var->name) + strlen (var->string) + 10) > sizeof(buffer))
		{
		  qci->Printf (S_COLOR_YELLOW "WARNING: value of variable "
			       "\"%s\" too long to write to file\n", var->name);
		  continue;
		}
	      Com_sprintf (buffer, sizeof (buffer), "seta %s \"%s\"\n",
			   var->name, var->string);
	    }
	  FS_Write (buffer, strlen (buffer), f);
	}
    }
}

/*
============
Cvar_Command

Handles variable inspection and changing from the console
============
*/
bool
Cvar_Command (void)
{
  cvar_t *v;

  // check variables
  v = Cvar_FindVar (Cmd_Argv(0));
  if (v == NULL)
    {
      return false;
    }

  // perform a variable print or set
  if (Cmd_Argc() == 1)
    {
      Cvar_Print (v);
      return true;
    }

  // set the value if forcing isn't required
  Cvar_SetUser (v->name, Cmd_Args());
  return true;
}

/*
============
Cvar_CommandCompletion
============
*/
void
Cvar_CommandCompletion (void (*callback)(const char *s))
{
  cvar_t *cvar;

  for (cvar = cvar_vars; cvar; cvar = cvar->next)
    {
      callback (cvar->name);
    }
}

/*
==================
Cvar_CompleteCvarName
==================
*/
void
Cvar_CompleteCvarName (char *args, int argNum)
{
  if (argNum == 2)
    {
      // Skip "<cmd> "
      char *p = Com_SkipTokens (args, 1, " ");

      if (p > args)
	Field_CompleteCommand (p, false, true);
    }
}

/*
============
Cvar_Get/SetModifiedFlags
============
*/
int
Cvar_GetModifiedFlags (void)
{
  return cvar_modifiedFlags;
}

void
Cvar_SetModifiedFlags (int flag)
{
  cvar_modifiedFlags = flag;
}
/*
============
Cvar_Init

Reads in all archived cvars
============
*/
void
Cvar_Init (void)
{
  Cmd_AddCommand ("cvar_print", Cvar_Print_f);
  Cmd_SetCommandCompletionFunc ("cvar_print", Cvar_CompleteCvarName);
  Cmd_AddCommand ("cvar_toggle", Cvar_Toggle_f);
  Cmd_SetCommandCompletionFunc ("cvar_toggle", Cvar_CompleteCvarName);
  Cmd_AddCommand ("set", Cvar_Set_f);
  Cmd_SetCommandCompletionFunc ("set", Cvar_CompleteCvarName);
  Cmd_AddCommand ("sets", Cvar_Set_f);
  Cmd_SetCommandCompletionFunc ("sets", Cvar_CompleteCvarName);
  Cmd_AddCommand ("setu", Cvar_Set_f);
  Cmd_SetCommandCompletionFunc ("setu", Cvar_CompleteCvarName);
  Cmd_AddCommand ("seta", Cvar_Set_f);
  Cmd_SetCommandCompletionFunc ("seta", Cvar_CompleteCvarName);
  Cmd_AddCommand ("cvar_reset", Cvar_Reset_f);
  Cmd_SetCommandCompletionFunc ("reset", Cvar_CompleteCvarName);
  //Cmd_AddCommand ("cvar_list", Cvar_List_f);
  Cmd_AddCommand ("list_cvars", Cvar_List_f);
}

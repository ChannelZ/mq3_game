/*
 *	related to memory pool in quake 3
 *
 *
 *
 *
 */

/*
==============================================================================

						ZONE MEMORY ALLOCATION

There is never any space between memblocks, and there will never be two
contiguous free memblocks.

The rover can be left pointing at a non-empty block

The zone calls are pretty much only used for small strings and structures,
all big things are allocated on the hunk.
==============================================================================
*/

#include "q_shared.h"
//#include "mempool.h"
#include "qcommon.h"

/*
========================
Z_Clear
========================
*/
void
Z_Clear (memzone_t *zone, int size)
{
  memblock_t *block;

  // set the entire zone to one free block

  zone->blocklist.next = zone->blocklist.prev = block =
    (memblock_t *)((byte *) zone + sizeof (memzone_t));
  zone->blocklist.tag = TAG_GENERAL;	// in use block
  zone->blocklist.id = 0;
  zone->blocklist.size = 0;
  zone->rover = block;
  zone->size = size;
  zone->used = 0;

  block->prev = block->next = &zone->blocklist;
  block->tag = TAG_FREE;		// free block
  block->id = ZONEID;
  block->size = size - sizeof (memzone_t);
}

/*
========================
Z_AvailableMemory
========================
*/
int
Z_AvailableMemory (memzone_t *zone)
{
  return zone->size - zone->used;
}


/*
========================
Z_Zone
========================
*/
void
Z_Free2 (memzone_t *zone, void *ptr)
{
  memblock_t *block, *other;

  if (!ptr)
    {
      qci->Error (ERR_DROP, "Z_Free: NULL pointer");
    }

  block = (memblock_t *) ((byte *) ptr - sizeof (memblock_t));

  if (block->id != ZONEID)
    {
      qci->Error (ERR_FATAL, "Z_Free: freed a pointer without ZONEID");
    }
  if (block->tag == TAG_FREE)
    {
      qci->Error (ERR_FATAL, "Z_Free: freed a freed pointer");
    }
  // if static memory
  if (block->tag == TAG_STATIC)
    {
      return;
    }

  // check the memory trash tester
  if (*(int *)((byte *) block + block->size - 4) != ZONEID)
    {
      qci->Error (ERR_FATAL, "Z_Free: memory block wrote past end");
    }

  zone->used -= block->size;
  // set the block to something that should cause problems
  // if it is referenced...
  Com_Memset (ptr, 0xaa, block->size - sizeof (*block));

  block->tag = TAG_FREE;	// mark as free

  other = block->prev;
  if (other->tag == TAG_FREE)
    {
      // merge with previous free block
      other->size += block->size;
      other->next = block->next;
      other->next->prev = other;
      if (block == zone->rover)
	{
	  zone->rover = other;
	}
      block = other;
    }

  zone->rover = block;

  other = block->next;
  if (other->tag == TAG_FREE)
    {
      // merge the next free block onto the end
      block->size += other->size;
      block->next = other->next;
      block->next->prev = block;
      if (other == zone->rover)
	{
	  zone->rover = block;
	}
    }
}

/*
================
Z_FreeTags
================
*/
void
Z_FreeTags (memzone_t *zone, int tag)
{
  int count;

  count = 0;
  // use the rover as our pointer, because
  // Z_Free automatically adjusts it
  zone->rover = zone->blocklist.next;
  do
    {
      if (zone->rover->tag == tag)
	{
	  count++;
	  Z_Free2 (zone, (void *) (zone->rover + 1));
	  continue;
	}
      zone->rover = zone->rover->next;
    }
  while (zone->rover != &zone->blocklist);
}

/*
================
Z_TagMalloc
================
*/
void *
Z_TagMalloc (memzone_t *zone, int size, int tag)
{
  int extra;
  memblock_t *start, *rover, *new, *base;

  if (tag == TAG_FREE)
    {
      qci->Error (ERR_FATAL, "Z_TagMalloc: tried to use a TAG_FREE [0] tag");
    }

  //
  // scan through the block list looking for the first free block
  // of sufficient size
  //
  size += sizeof (memblock_t);		// account for size of block header
  size += 4;				// space for memory trash tester
  size = PAD (size, sizeof (intptr_t));	// align to 32/64 bit boundary

  base = rover = zone->rover;
  start = base->prev;

  // looking for a block large enough
  do
    {
      if (rover == start)
	{
	  Z_LogHeap ();

	  // scaned all the way around the list
	  qci->Error (ERR_FATAL, "Z_Malloc: failed on allocation of %i bytes from the %s zone",
		      size, zone == smallzone ? "small" : "main");
	  return NULL;
	}

      if (rover->tag)
	{
	  base = rover = rover->next;
	}
      else
	{
	  rover = rover->next;
	}
    }
  while (base->tag || (base->size < size));

  //
  // found a block big enough
  //
  extra = base->size - size;

  // create a block after the to be created one
  if (extra > MINFRAGMENT)
    {
      // there will be a free fragment after the allocated block
      new = (memblock_t *) ((byte *) base + size);
      new->size = extra;
      new->tag = TAG_FREE;	// free block
      new->prev = base;
      new->id = ZONEID;
      new->next = base->next;
      new->next->prev = new;
      base->next = new;
      base->size = size;
    }

  base->tag = tag;		// no longer a free block

  zone->rover = base->next;	// next allocation will start looking here
  zone->used += base->size;	//

  base->id = ZONEID;

  // marker for memory trash testing
  *(int *)((byte *) base + base->size - 4) = ZONEID;

  return (void *) ((byte *) base + sizeof (memblock_t));
}

/*
========================
Z_Malloc
========================
*/
void *
Z_Malloc (memzone_t *zone, int size)
{
  void *buf;

  Z_CheckHeap (zone);	// DEBUG

  buf = Z_TagMalloc (zone, size, TAG_GENERAL);

  Com_Memset (buf, 0, size);

  return buf;
}

/*
========================
Z_CheckHeap
========================
*/
void
Z_CheckHeap (memzone_t *zone)
{
  memblock_t *block;

  for (block = zone->blocklist.next; ; block = block->next)
    {
      if (block->next == &zone->blocklist)
	{
	  break;			// all blocks have been hit
	}

      if (((byte *) block + block->size) != (byte *) block->next)
	{
	  qci->Error (ERR_FATAL, "Z_CheckHeap: block size does not touch the next block\n" );
	}

      if (block->next->prev != block)
	{
	  qci->Error (ERR_FATAL, "Z_CheckHeap: next block doesn't have proper back link\n");
	}

      if (!block->tag && !block->next->tag)
	{
	  qci->Error (ERR_FATAL, "Z_CheckHeap: two consecutive free blocks\n");
	}
    }
}

/*
========================
Z_LogZoneHeap
========================
*/
extern fileHandle_t logfile;

void
Z_LogZoneHeap (memzone_t *zone, char *name)
{
#ifdef ZONE_DEBUG
   char	dump[32], *ptr;
   int i, j;
#endif
   memblock_t *block;
   char buf[4096];
   int size, allocSize, numBlocks;

   if (!logfile || !FS_Initialized ())
     return;

   size = allocSize = numBlocks = 0;
   Com_sprintf (buf, sizeof (buf),
		"\r\n================\r\n%s log\r\n================\r\n", name);

   FS_Write (buf, strlen (buf), logfile);

   for (block = zone->blocklist.next; block->next != &zone->blocklist; block = block->next)
     {
       if (block->tag)
	 {
#ifdef ZONE_DEBUG
	   ptr = ((char *) block) + sizeof (memblock_t);
	   j = 0;

	   for (i = 0; (i < 20) && (i < block->d.allocSize); i++)
	     {
	       if ((ptr[i] >= 32) && (ptr[i] < 127))
		 {
		   dump[j++] = ptr[i];
		 }
	       else
		 {
		   dump[j++] = '_';
		 }
	     }

	   dump[j] = '\0';
	   Com_sprintf (buf, sizeof (buf), "size = %8d: %s, line: %d (%s) [%s]\r\n",
			block->d.allocSize, block->d.file, block->d.line,
			block->d.label, dump);

	   FS_Write (buf, strlen(buf), logfile);
	   allocSize += block->d.allocSize;
#endif
	   size += block->size;
	   numBlocks++;
	 }
     }
#ifdef ZONE_DEBUG
   // subtract debug memory
   size -= numBlocks * sizeof (zonedebug_t);
#else
   allocSize = numBlocks * sizeof (memblock_t); // + 32 bit alignment
#endif
   Com_sprintf (buf, sizeof (buf), "%d %s memory in %d blocks\r\n", size, name, numBlocks);
   FS_Write (buf, strlen (buf), logfile);
   Com_sprintf (buf, sizeof(buf), "%d %s memory overhead\r\n", size - allocSize, name);
   FS_Write (buf, strlen (buf), logfile);
}

/*
========================
Z_LogHeap
========================
*/
void
Z_LogHeap (void)
{
  Z_LogZoneHeap (mainzone, "MAIN");
  Z_LogZoneHeap (smallzone, "SMALL");
}


memstatic_t emptystring =
  { {(sizeof (memblock_t) + 2 + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'\0', '\0'} };

memstatic_t numberstring[] = {
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'0', '\0'} },
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'1', '\0'} },
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'2', '\0'} },
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'3', '\0'} },
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'4', '\0'} },
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'5', '\0'} },
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'6', '\0'} },
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'7', '\0'} },
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'8', '\0'} },
  { {(sizeof (memstatic_t) + 3) & ~3, TAG_STATIC, NULL, NULL, ZONEID}, {'9', '\0'} }
};

/*
========================
CopyString

NOTE:	never write over the memory CopyString returns because
	memory from a memstatic_t might be returned
========================
*/
char *
CopyString (const char *in)
{
  char *out;

  if (!in[0])
    {
      return ((char *) &emptystring) + sizeof (memblock_t);
    }
  else if (!in[1])
    {
      if ((in[0] >= '0') && (in[0] <= '9'))
	{
	  return ((char *) &numberstring[in[0] - '0']) + sizeof (memblock_t);
	}
    }

  out = S_Malloc (strlen (in) + 1);
  strcpy (out, in);
  return out;
}

// ========================
// Compatibility functions
// ========================

/*
========================
M_Malloc
========================
*/
void *
M_Malloc (int size)
{
  return Z_Malloc (mainzone, size);
}

/*
========================
S_Malloc
========================
*/
void *
S_Malloc (int size)
{
  return Z_TagMalloc (smallzone, size, TAG_SMALL);
}

/*
========================
Z_AvailableMemoryTot

TODO : make on with a list of all "zones"
       and return the sum
========================
*/
int
Z_AvailableMemoryTot (void)
{
  return Z_AvailableMemory (mainzone);
}

/*
========================
Z_Free

CLEARLY TO BE REWORKED !
BETTER : DONT USE THIS ONE
========================
*/
void
Z_Free (void *ptr)
{
  memzone_t *zone;
  memblock_t *block;

  if (!ptr)
    {
      qci->Error (ERR_DROP, "Z_Free: NULL pointer");
    }

  block = (memblock_t *) ((byte *) ptr - sizeof (memblock_t));

  if (block->id != ZONEID)
    {
      qci->Error (ERR_FATAL, "Z_Free: freed a pointer without ZONEID");
    }
  if (block->tag == TAG_FREE)
    {
      qci->Error (ERR_FATAL, "Z_Free: freed a freed pointer");
    }
  // if static memory
  if (block->tag == TAG_STATIC)
    {
      return;
    }

  // check the memory trash tester
  if (*(int *)((byte *) block + block->size - 4) != ZONEID)
    {
      qci->Error (ERR_FATAL, "Z_Free: memory block wrote past end");
    }

  if (block->tag == TAG_SMALL)
    {
      zone = smallzone;
    }
  else
    {
      zone = mainzone;
    }

  Z_Free2 (zone, ptr);
}

/*
================
Z_TagMalloc
================
*/
void *
Z_TagMalloc1 (int size, int tag)
{
  memzone_t *zone;

  if (tag == TAG_SMALL)
    {
      zone = smallzone;
    }
  else
    {
      zone = mainzone;
    }

  return Z_TagMalloc (zone, size, tag);
}


/*
================
Z_FreeTags
================
*/
void
Z_FreeTags1 (int tag)
{
  Z_FreeTags (mainzone, tag);
}

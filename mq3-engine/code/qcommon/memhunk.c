/*
 *
 *
 *
 *
 *
 */

/*
==============================================================================

Goals:
	reproducable without history effects -- no out of memory errors on weird map to map changes
	allow restarting of the client without fragmentation
	minimize total pages in use at run time
	minimize total pages needed during load time

  Single block of memory with stack allocators coming from both ends towards the middle.

  One side is designated the temporary memory allocator.

  Temporary memory can be allocated and freed in any order.

  A highwater mark is kept of the most in use at any time.

  When there is no temporary memory allocated, the permanent and temp sides
  can be switched, allowing the already touched temp memory to be used for
  permanent storage.

  Temp memory must never be allocated on two ends at once, or fragmentation
  could occur.

  If we have any in-use temp memory, additional temp allocations must come from
  that side.

  If not, we can choose to make either side the new temp side and push future
  permanent allocations to the other side.  Permanent allocations should be
  kept on the side that has the current greatest wasted highwater mark.

==============================================================================
*/

#include "q_shared.h"
//#include "mempool.h"
//#include "memhunk.h"
#include "qcommon.h"

#define	HUNK_MAGIC	0x89537892
#define	HUNK_FREE_MAGIC	0x89537893


static memhunk_t mainhunk;

// main zone for all "dynamic" memory allocation
memzone_t *mainzone;
static int s_zoneTotal;

// we also have a small zone for small allocations that would only
// fragment the main zone (think of cvar and cmd strings)
memzone_t *smallzone;
static int s_smallZoneTotal;

/*
============
Com_Meminfo
============
*/
static void
Com_Meminfo (memhunk_t *mainhunk_, memzone_t *mainzone_, memzone_t *smallzone_)
{
  memblock_t *block;
  int zoneBytes, zoneBlocks;
  int smallZoneBytes, smallZoneBlocks;
  int botlibBytes, rendererBytes, soundBytes, taggedBytes;
  int unused;

  zoneBytes = 0;
  zoneBlocks = 0;
  smallZoneBytes = 0;
  smallZoneBlocks = 0;
  botlibBytes = 0;
  rendererBytes = 0;
  soundBytes = 0;
  unused = 0;

  // NOTE
  // TODO : these 2 checks can be generalized for any "memzone" structure

  // finnest stats for mainzone
  for (block = mainzone_->blocklist.next; ; block = block->next)
    {
      if (Cmd_Argc () != 1)
	{
	  qci->Printf ("block:%p    size:%7i    tag:%3i\n",
		       (void *) block, block->size, block->tag);
	}

      if (block->tag != TAG_FREE)
	{
	  zoneBytes += block->size;
	  zoneBlocks++;

	  if (block->tag == TAG_BOTLIB)
	    {
	      botlibBytes += block->size;
	    }
	  else if (block->tag == TAG_RENDERER)
	    {
	      rendererBytes += block->size;
	    }
	  else if (block->tag == TAG_SOUND)
	    {
	      soundBytes += block->size;
	    }
	}

      if (block->next == &mainzone_->blocklist)
	{
	  break;	// all blocks have been hit
	}

      // checkheap tests
      if (((byte *) block + block->size) != (byte *) block->next)
	{
	  qci->Printf ("ERROR (main): block size does not touch the next block\n");
	}

      if (block->next->prev != block)
	{
	  qci->Printf ("ERROR (main): next block doesn't have proper back link\n");
	}

      if (!block->tag && !block->next->tag)
	{
	  qci->Printf ("ERROR (main): two consecutive free blocks\n");
	}
    }

  // finnest stats for smallzone
  for (block = smallzone_->blocklist.next; ; block = block->next)
    {
      if (block->tag != TAG_FREE)
	{
	  smallZoneBytes += block->size;
	  smallZoneBlocks++;
	}

      if (block->next == &smallzone->blocklist)
	{
	  break;	// all blocks have been hit
	}

      // checkheap tests
      if (((byte *) block + block->size) != (byte *) block->next)
	{
	  qci->Printf ("ERROR (small): block size does not touch the next block\n");
	}

      if (block->next->prev != block)
	{
	  qci->Printf ("ERROR (small): next block doesn't have proper back link\n");
	}

      if (!block->tag && !block->next->tag)
	{
	  qci->Printf ("ERROR (small): two consecutive free blocks\n");
	}
    }

  // outputing results
  qci->Printf ("%8i bytes total hunk\n", mainhunk_->hunkTotal);
  qci->Printf ("%8i bytes total zone\n", s_zoneTotal);
  qci->Printf ("%8i bytes total small zone\n", s_smallZoneTotal);
  qci->Printf ("\n" );

  qci->Printf ("----- main hunk -----\n");

  qci->Printf ("%8i low mark\n", mainhunk_->hunk_low.mark);
  qci->Printf ("%8i low permanent\n", mainhunk_->hunk_low.permanent);
  if (mainhunk_->hunk_low.temp != mainhunk_->hunk_low.permanent)
    {
      qci->Printf ("%8i low temp\n", mainhunk_->hunk_low.temp);
    }
  qci->Printf ("%8i low tempHighwater\n", mainhunk_->hunk_low.tempHighwater);
  qci->Printf ("\n");

  qci->Printf ("%8i high mark\n", mainhunk_->hunk_high.mark);
  qci->Printf ("%8i high permanent\n", mainhunk_->hunk_high.permanent);
  if (mainhunk_->hunk_high.temp != mainhunk_->hunk_high.permanent)
    {
      qci->Printf ("%8i high temp\n", mainhunk_->hunk_high.temp);
    }
  qci->Printf ("%8i high tempHighwater\n", mainhunk_->hunk_high.tempHighwater);
  qci->Printf ("\n");

  qci->Printf ("%8i total hunk in use\n", (mainhunk_->hunk_low.permanent
					   + mainhunk_->hunk_high.permanent));

  if (mainhunk_->hunk_low.tempHighwater > mainhunk_->hunk_low.permanent)
    {
      unused += (mainhunk_->hunk_low.tempHighwater - mainhunk_->hunk_low.permanent);
    }
  if (mainhunk_->hunk_high.tempHighwater > mainhunk_->hunk_high.permanent)
    {
      unused += (mainhunk_->hunk_high.tempHighwater - mainhunk_->hunk_high.permanent);
    }
  qci->Printf ("%8i unused highwater\n", unused);
  qci->Printf ("\n");

  qci->Printf ("----- zone mem -----\n");
  taggedBytes = (botlibBytes + rendererBytes + soundBytes);

  qci->Printf ("%8i bytes in %i zone blocks\n", zoneBytes, zoneBlocks);
  qci->Printf ("        %8i bytes in dynamic botlib\n", botlibBytes);
  qci->Printf ("        %8i bytes in dynamic renderer\n", rendererBytes);
  qci->Printf ("        %8i bytes in dynamic sound\n", soundBytes);
  qci->Printf ("        %8i bytes in dynamic other\n", (zoneBytes - taggedBytes));
  qci->Printf ("%8i bytes in %i small Zone blocks\n", smallZoneBytes, smallZoneBlocks);
}

/*
=================
Com_Meminfo_f
=================
*/
static void
Com_Meminfo_f (void)
{
  Com_Meminfo (&mainhunk, mainzone, smallzone);
}

/*
===============
Com_TouchMemory

Touch all known used data to make sure it is paged in
===============
*/
void
Com_TouchMemory (void)
{
  int start, end;
  int i, j;
  int sum;
  memblock_t *block;

  Z_CheckHeap (mainzone);
  Z_CheckHeap (smallzone);

  start = qci->Sys_Milliseconds ();

  sum = 0;

  j = mainhunk.hunk_low.permanent >> 2;
  for (i = 0 ; i < j ; i += 64 )	// only need to touch each page
    {
      sum += ((int *) mainhunk.hunkData)[i];
    }

  i = (mainhunk.hunkTotal - mainhunk.hunk_high.permanent) >> 2;
  j = mainhunk.hunk_high.permanent >> 2;
  for ( ; i < j; i += 64)		// only need to touch each page
    {
      sum += ((int *) mainhunk.hunkData)[i];
    }

  for (block = mainzone->blocklist.next; ; block = block->next)
    {
      if (block->tag)
	{
	  j = block->size >> 2;
	  for (i = 0; i < j; i += 64)	// only need to touch each page
	    {
	      sum += ((int *) block)[i];
	    }
	}
      if (block->next == &mainzone->blocklist)
	{
	  break;	// all blocks have been hit
	}
    }

  end = qci->Sys_Milliseconds ();

  qci->Printf ("Com_TouchMemory: %i msec\n", end - start);
}

/*
===================
Com_InitZoneMemory
===================
*/
void
Com_InitZoneMemory (memzone_t **zone, int size)
{
  *zone = calloc (size, 1);

  if (!*zone)
    {
      qci->Error (ERR_FATAL, "Zone data failed to allocate %3.1f megs",
		  (float) size / (1024 * 1024));
    }

  qci->Printf ("qMem : zone allocated for %3.1f megs\n",
	       (float) size / (1024 * 1024));

  Z_Clear (*zone, size);
}

/*
=================
Com_InitSmallZoneMemory
=================
*/
void
Com_InitSmallZoneMemory (void)
{
  s_smallZoneTotal = 512 * 1024;

  Com_InitZoneMemory (&smallzone, s_smallZoneTotal);
}

/*
=================
Com_InitZoneMemory
=================
*/
void
Com_InitMainZoneMemory (int size)
{
  // if there's a parameter
  if (size)
    {
      if (size < MIN_COMZONEMEGS)
	{
	  size = MIN_COMZONEMEGS;
	}
      if (size > MAX_COMZONEMEGS)
	{
	  size = MAX_COMZONEMEGS;
	}

      s_zoneTotal = 1024 * 1024 * size;
    }
  else
    {
      // default values
      if (qcom_flags & QCFLAG_DEDICATED)
	{
	  s_zoneTotal = 1024 * 1024 * DEF_COMDEDZONEMEGS;
	}
      else
	{
	  s_zoneTotal = 1024 * 1024 * DEF_COMZONEMEGS;
	}
    }

  Com_InitZoneMemory (&mainzone, s_zoneTotal);
}

/*
=================
Com_InitHunkMemory
=================
*/
void
Com_InitHunkMemory (int size, int isdedicated)
{
  int nMinAlloc;
  char *pMsg = NULL;

  // make sure the file system has allocated and "not" freed any temp blocks
  // this allows the config and product id files ( journal files too ) to be loaded
  // by the file system without redunant routines in the file system utilizing different
  // memory systems
  if (FS_LoadStack () != 0)
    {
      qci->Error (ERR_FATAL, "Hunk initialization failed. File system load stack not zero");
    }

  // if we are not dedicated min allocation is 56, otherwise min is 1
  if (isdedicated)
    {
      nMinAlloc = MIN_DEDICATED_COMHUNKMEGS;
      pMsg = "Minimum com_hunkMegs for a dedicated server is %i, allocating %i megs.\n";
    }
  else
    {
      nMinAlloc = MIN_COMHUNKMEGS;
      pMsg = "Minimum com_hunkMegs is %i, allocating %i megs.\n";
    }

  if (size < nMinAlloc)
    {
      mainhunk.hunkTotal = 1024 * 1024 * nMinAlloc;
      qci->Printf (pMsg, nMinAlloc, mainhunk.hunkTotal / (1024 * 1024));
    }
  else
    {
      mainhunk.hunkTotal = size * 1024 * 1024;
    }

  mainhunk.hunkData = calloc (mainhunk.hunkTotal + 31, 1);
  if (!mainhunk.hunkData)
    {
      qci->Error (ERR_FATAL, "Hunk data failed to allocate %3.1f megs",
		  (float)mainhunk.hunkTotal / (1024*1024));
    }

  // cacheline align
  mainhunk.hunkData = (byte *)(((intptr_t) mainhunk.hunkData + 31) & ~31);

  Hunk_Clear ();

  Cmd_AddCommand ("meminfo", Com_Meminfo_f);
#ifdef ZONE_DEBUG
  Cmd_AddCommand ("zonelog", Z_LogHeap);
#endif
}

/*
=======================
Com_ShutdownHunkMemory
=======================
*/
void
Com_ShutdownHunkMemory (void)
{
  Cmd_RemoveCommand ("meminfo");
#ifdef ZONE_DEBUG
  Cmd_RemoveCommand ("zonelog");
#endif
}

/*
====================
Hunk_MemoryRemaining
====================
*/
int
Hunk_MemoryRemaining2 (memhunk_t *hunk)
{
  int low, high;

  low = ((hunk->hunk_low.permanent > hunk->hunk_low.temp) ?
	 hunk->hunk_low.permanent : hunk->hunk_low.temp);

  high = ((hunk->hunk_high.permanent > hunk->hunk_high.temp) ?
	  hunk->hunk_high.permanent : hunk->hunk_high.temp);

  return hunk->hunkTotal - (low + high);
}

int
Hunk_MemoryRemaining (void)
{
  return Hunk_MemoryRemaining2 (&mainhunk);
}

/*
===================
Hunk_SetMark

The server calls this after the level and game VM have been loaded
===================
*/
void
Hunk_SetMark2 (memhunk_t *hunk)
{
  hunk->hunk_low.mark = hunk->hunk_low.permanent;
  hunk->hunk_high.mark = hunk->hunk_high.permanent;
}

void
Hunk_SetMark (void)
{
  Hunk_SetMark2 (&mainhunk);
}

/*
=================
Hunk_ClearToMark

The client calls this before starting a vid_restart or snd_restart
=================
*/
void
Hunk_ClearToMark2 (memhunk_t *hunk)
{
  hunk->hunk_low.permanent = hunk->hunk_low.temp = hunk->hunk_low.mark;
  hunk->hunk_high.permanent = hunk->hunk_high.temp = hunk->hunk_high.mark;
}

void
Hunk_ClearToMark (void)
{
  Hunk_ClearToMark2 (&mainhunk);
}

/*
=================
Hunk_CheckMark
=================
*/
bool
Hunk_CheckMark2 (memhunk_t *hunk)
{
  if (hunk->hunk_low.mark || hunk->hunk_high.mark)
    {
      return true;
    }

  return false;
}

bool
Hunk_CheckMark (void)
{
  return Hunk_CheckMark2 (&mainhunk);
}

/*
=================
Hunk_Clear

The server calls this before shutting down or loading a new map
=================
*/
void
Hunk_Clear2 (memhunk_t *hunk)
{
  hunk->hunk_low.mark = 0;
  hunk->hunk_low.permanent = 0;
  hunk->hunk_low.temp = 0;
  hunk->hunk_low.tempHighwater = 0;

  hunk->hunk_high.mark = 0;
  hunk->hunk_high.permanent = 0;
  hunk->hunk_high.temp = 0;
  hunk->hunk_high.tempHighwater = 0;

  hunk->hunk_permanent = &hunk->hunk_low;
  hunk->hunk_temp = &hunk->hunk_high;

  qci->Printf ("Hunk_Clear: reset the hunk ok\n");
}

void
Hunk_Clear (void)
{
  Hunk_Clear2 (&mainhunk);
}

/*
===============
Hunk_SwapBanks
===============
*/
static void
Hunk_SwapBanks2 (memhunk_t *hunk)
{
  hunkUsed_t *swap;

  // can't swap banks if there is any temp already allocated
  if (hunk->hunk_temp->temp != hunk->hunk_temp->permanent)
    {
      return;
    }

  // if we have a larger highwater mark on this side, start making
  // our permanent allocations here and use the other side for temp
  if ((hunk->hunk_temp->tempHighwater - hunk->hunk_temp->permanent) >
      (hunk->hunk_permanent->tempHighwater - hunk->hunk_permanent->permanent))
    {
      swap = hunk->hunk_temp;
      hunk->hunk_temp = hunk->hunk_permanent;
      hunk->hunk_permanent = swap;
    }
}

/*
=================
Hunk_Alloc

Allocate permanent (until the hunk is cleared) memory
=================
*/
void *
Hunk_Alloc2 (memhunk_t *hunk, int size, ha_pref preference)
{
  void *buf;

  if (hunk->hunkData == NULL)
    {
      qci->Error (ERR_FATAL, "Hunk_Alloc: Hunk memory system not initialized");
    }

  // can't do preference if there is any temp allocated
  if ((preference == h_dontcare) || (hunk->hunk_temp->temp != hunk->hunk_temp->permanent))
    {
      Hunk_SwapBanks2 (hunk);
    }
  else
    {
      if ((preference == h_low) && (hunk->hunk_permanent != &hunk->hunk_low))
	{
	  Hunk_SwapBanks2 (hunk);
	}
      else if ((preference == h_high) && (hunk->hunk_permanent != &hunk->hunk_high))
	{
	  Hunk_SwapBanks2 (hunk);
	}
    }

  // round to cacheline
  size = (size + 31) & ~31;

  if ((hunk->hunk_low.temp + hunk->hunk_high.temp + size) > hunk->hunkTotal)
    {
      qci->Error (ERR_DROP, "Hunk_Alloc failed on %i", size);
    }

  if (hunk->hunk_permanent == &hunk->hunk_low)
    {
      buf = (void *) (hunk->hunkData + hunk->hunk_permanent->permanent);
      hunk->hunk_permanent->permanent += size;
    }
  else
    {
      hunk->hunk_permanent->permanent += size;
      buf = (void *) (hunk->hunkData + hunk->hunkTotal - hunk->hunk_permanent->permanent);
    }

  hunk->hunk_permanent->temp = hunk->hunk_permanent->permanent;

  Com_Memset (buf, 0, size);

  return buf;
}

void *
Hunk_Alloc (int size, ha_pref preference)
{
  return Hunk_Alloc2 (&mainhunk, size, preference);
}

/*
=================
Hunk_AllocateTempMemory

This is used by the file loading system.
Multiple files can be loaded in temporary memory.
When the files-in-use count reaches zero, all temp memory will be deleted
=================
*/
void *
Hunk_AllocateTempMemory2 (memhunk_t *hunk, int size)
{
  void *buf;
  hunkHeader_t *hdr;

  Hunk_SwapBanks2 (hunk);

  size = PAD (size, sizeof (intptr_t)) + sizeof (hunkHeader_t);

  if ((hunk->hunk_temp->temp + hunk->hunk_permanent->permanent + size) > hunk->hunkTotal)
    {
      qci->Error (ERR_DROP, "Hunk_AllocateTempMemory: failed on %i", size);
    }

  if (hunk->hunk_temp == &hunk->hunk_low)
    {
      buf = (void *) (hunk->hunkData + hunk->hunk_temp->temp);
      hunk->hunk_temp->temp += size;
    }
  else
    {
      hunk->hunk_temp->temp += size;
      buf = (void *) (hunk->hunkData + hunk->hunkTotal - hunk->hunk_temp->temp);
    }

  if (hunk->hunk_temp->temp > hunk->hunk_temp->tempHighwater)
    {
      hunk->hunk_temp->tempHighwater = hunk->hunk_temp->temp;
    }

  hdr = (hunkHeader_t *) buf;
  buf = (void *) (hdr + 1);

  hdr->magic = HUNK_MAGIC;
  hdr->size = size;

  // don't bother clearing, because we are going to load a file over it
  return buf;
}

void *
Hunk_AllocateTempMemory (int size)
{
  // return a Z_Malloc'd block if the hunk has not been initialized
  // this allows the config and product id files ( journal files too ) to be loaded
  // by the file system without redunant routines in the file system utilizing different
  // memory systems
  if (mainhunk.hunkData == NULL)
    {
      return Z_Malloc (mainzone, size);
    }

  return Hunk_AllocateTempMemory2 (&mainhunk, size);
}

/*
==================
Hunk_FreeTempMemory
==================
*/
void
Hunk_FreeTempMemory2 (memhunk_t *hunk, void *buf)
{
  hunkHeader_t *hdr;

  hdr = ((hunkHeader_t *) buf) - 1;
  if (hdr->magic != HUNK_MAGIC)
    {
      qci->Error (ERR_FATAL, "Hunk_FreeTempMemory: bad magic");
    }

  hdr->magic = HUNK_FREE_MAGIC;

  // this only works if the files are freed in stack order,
  // otherwise the memory will stay around until Hunk_ClearTempMemory
  if (hunk->hunk_temp == &hunk->hunk_low)
    {
      if (hdr == (void *) (hunk->hunkData + hunk->hunk_temp->temp - hdr->size))
	{
	  hunk->hunk_temp->temp -= hdr->size;
	}
      else
	{
	  qci->Printf ("Hunk_FreeTempMemory: not the final block\n");
	}
    }
  else
    {
      if (hdr == (void *) (hunk->hunkData + hunk->hunkTotal - hunk->hunk_temp->temp))
	{
	  hunk->hunk_temp->temp -= hdr->size;
	}
      else
	{
	  qci->Printf ("Hunk_FreeTempMemory: not the final block\n");
	}
    }
}

void
Hunk_FreeTempMemory (void *buf)
{
  // free with Z_Free if the hunk has not been initialized
  // this allows the config and product id files ( journal files too ) to be loaded
  // by the file system without redunant routines in the file system utilizing different
  // memory systems
  if (mainhunk.hunkData == NULL)
    {
      Z_Free2 (mainzone, buf);
      return;
    }

  Hunk_FreeTempMemory2 (&mainhunk, buf);
}

/*
=================
Hunk_ClearTempMemory

The temp space is no longer needed.  If we have left more
touched but unused memory on this side, have future
permanent allocs use this side.
=================
*/
void
Hunk_ClearTempMemory2 (memhunk_t *hunk)
{
  if (hunk->hunkData != NULL)
    {
      hunk->hunk_temp->temp = hunk->hunk_temp->permanent;
    }
}

void
Hunk_ClearTempMemory (void)
{
  Hunk_ClearTempMemory2 (&mainhunk);
}

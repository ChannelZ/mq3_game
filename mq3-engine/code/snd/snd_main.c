/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.
Copyright (C) 2005 Stuart Dalton (badcdev@gmail.com)

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/

#include "snd_codec.h"
//#include "snd_local.h" // called by snd_codec
#include "snd_public.h"

cvar_t *s_volume;
cvar_t *s_musicVolume;
cvar_t *s_doppler;
cvar_t *s_backend;

cvar_t *s_wav_record;

static soundInterface_t si;
sndimport_t *sndi;
//sndexport_t snde;

/*
=================
S_ValidateInterface
=================
*/
static bool
S_ValidSoundInterface (soundInterface_t * si)
{
  if (!si->Shutdown)
    return false;
  if (!si->StartSound)
    return false;
  if (!si->StartLocalSound)
    return false;
  if (!si->StartBackgroundTrack)
    return false;
  if (!si->StopBackgroundTrack)
    return false;
  if (!si->RawSamples)
    return false;
  if (!si->StopAllSounds)
    return false;
  if (!si->ClearLoopingSounds)
    return false;
  if (!si->AddLoopingSound)
    return false;
  if (!si->AddRealLoopingSound)
    return false;
  if (!si->StopLoopingSound)
    return false;
  if (!si->Respatialize)
    return false;
  if (!si->UpdateEntityPosition)
    return false;
  if (!si->Update)
    return false;
  if (!si->DisableSounds)
    return false;
  if (!si->BeginRegistration)
    return false;
  if (!si->RegisterSound)
    return false;
  if (!si->ClearSoundBuffer)
    return false;
  if (!si->SoundInfo)
    return false;
  if (!si->SoundList)
    return false;

  return true;
}

//=============================================================================

/*
=================
S_SoundInfo
=================
*/
void
S_SoundInfo (void)
{
  if (si.SoundInfo)
    {
      si.SoundInfo ();
    }
}

/*
=================
S_SoundList
=================
*/
void
S_SoundList (void)
{
  if (si.SoundList)
    {
      si.SoundList ();
    }
}

/*
=================
S_StopAllSounds
=================
*/
void
S_StopAllSounds (void)
{
  if (si.StopAllSounds)
    {
      si.StopAllSounds ();
    }
}

/*
=================
S_Play_f
=================
*/
void
S_Play_f (void)
{
  int i;
  sfxHandle_t h;
  char name[256];

  if (!si.RegisterSound || !si.StartLocalSound)
    {
      return;
    }

  i = 1;
  while (i < sndi->Cmd_Argc ())
    {
      if (!Q_strrchr (sndi->Cmd_Argv (i), '.'))
	{
	  Com_sprintf (name, sizeof (name), "%s.wav", sndi->Cmd_Argv (1));
	}
      else
	{
	  Q_strncpyz (name, sndi->Cmd_Argv (i), sizeof (name));
	}
      h = si.RegisterSound (name);
      if (h)
	{
	  si.StartLocalSound (h, CHAN_LOCAL_SOUND);
	}
      i++;
    }
}

/*
=================
S_Music_f
=================
*/
void
S_Music_f (void)
{
  int c;

  if (!si.StartBackgroundTrack)
    {
      return;
    }

  c = sndi->Cmd_Argc ();

  if (c == 2)
    {
      si.StartBackgroundTrack (sndi->Cmd_Argv (1), NULL);
    }
  else if (c == 3)
    {
      si.StartBackgroundTrack (sndi->Cmd_Argv (1), sndi->Cmd_Argv (2));
    }
  else
    {
      sndi->Printf ("music <musicfile> [loopfile]\n");
      return;
    }

}

/*
=================
S_StopMusic_f
=================
*/
void
S_StopMusic_f (void)
{
  if (!si.StopBackgroundTrack)
    return;

  si.StopBackgroundTrack();
}

//=============================================================================

/*
=================
S_Shutdown
=================
*/
void
S_Shutdown (void)
{
  if (si.Shutdown)
    {
      si.Shutdown ();
    }

  Com_Memset (&si, 0, sizeof (soundInterface_t));

  sndi->Cmd_RemoveCommand ("play");
  sndi->Cmd_RemoveCommand ("music");
  sndi->Cmd_RemoveCommand ("stopmusic");
  sndi->Cmd_RemoveCommand ("s_list");
  sndi->Cmd_RemoveCommand ("s_stop");
  sndi->Cmd_RemoveCommand ("s_info");

  S_CodecShutdown ();
}

sndexport_t *
GetSoundLibAPI (sndimport_t * sndimp)
{
  bool started = false;
  cvar_t *cv;
  // functions coming from client
  sndi = sndimp;

  sndi->Printf ("------ Initializing Sound ------\n");

  // setup the print/error functions for qshared
  q_shared_import_t q_shared_imp;
  q_shared_imp.print = sndi->Printf;
  q_shared_imp.error = sndi->Error;

  Q_Set_Interface (&q_shared_imp);

  s_volume = sndi->Cvar_Get ("s_volume", "0.8", CVAR_ARCHIVE);
  s_musicVolume = sndi->Cvar_Get ("s_musicvolume", "0.0", CVAR_ARCHIVE);
  s_doppler = sndi->Cvar_Get ("s_doppler", "1", CVAR_ARCHIVE);
  s_backend = sndi->Cvar_Get ("s_backend", "", CVAR_ROM);
  s_wav_record = sndi->Cvar_Get ("s_wav_record", "0", CVAR_TEMP);

  cv = sndi->Cvar_Get ("s_initsound", "1", 0);
  if (!cv->integer)
    {
      sndi->Printf ("Sound disabled.\n");
      // assume that &si is NULL ?
    }
  else
    {
      S_CodecInit ();

      sndi->Cmd_AddCommand ("play", S_Play_f);
      sndi->Cmd_AddCommand ("music", S_Music_f);
      sndi->Cmd_AddCommand ("stopmusic", S_StopMusic_f);
      sndi->Cmd_AddCommand ("s_list", S_SoundList);
      sndi->Cmd_AddCommand ("s_stop", S_StopAllSounds);
      sndi->Cmd_AddCommand ("s_info", S_SoundInfo);

      if (!started)
	{
	  Com_Memset (&si, 0, sizeof(si));
	  started = S_Base_Init (&si);
	  sndi->Cvar_Set ("s_backend", "base");
	}

      if (started)
	{
	  if (!S_ValidSoundInterface (&si))
	    {
	      sndi->Error (ERR_FATAL, "Sound interface invalid.");
	    }

	  S_SoundInfo ();
	  sndi->Printf ("Sound initialization successful.\n");
	}
      else
	{
	  sndi->Printf ("Sound initialization failed.\n");
	  return NULL;
	}
    }

  sndi->Printf ("--------------------------------\n");

  return &si;
}

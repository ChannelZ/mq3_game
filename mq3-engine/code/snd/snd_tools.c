/* Code under GPLv2, Copyright ID Software
 *
 *
 *
 *
 *
 */
// snd_tools.c what is needed for the module and coming from q_shared as
// qcommon/ is no yet properly structured

#include "snd_local.h"

/*
=============
Q_strncpyz
 
Safe strncpy that ensures a trailing zero
=============
*/
void
Q_strncpyz (char *dest, const char *src, int destsize)
{
  if (!dest)
    {
      sndi->Error (ERR_FATAL, "Q_strncpyz: NULL dest");
    }
  if (!src)
    {
      sndi->Error (ERR_FATAL, "Q_strncpyz: NULL src");
    }
  if (destsize < 1)
    {
      sndi->Error (ERR_FATAL, "Q_strncpyz: destsize < 1");
    }

  strncpy (dest, src, destsize - 1);
  dest[destsize - 1] = 0;
}

int
Q_stricmpn (const char *s1, const char *s2, int n)
{
  int c1, c2;

  if (s1 == NULL)
    {
      if (s2 == NULL)
	return 0;
      else
	return -1;
    }
  else if (s2 == NULL)
    return 1;



  do
    {
      c1 = *s1++;
      c2 = *s2++;

      if (!n--)
	{
	  return 0;		// strings are equal until end point
	}

      if (c1 != c2)
	{
	  if (c1 >= 'a' && c1 <= 'z')
	    {
	      c1 -= ('a' - 'A');
	    }
	  if (c2 >= 'a' && c2 <= 'z')
	    {
	      c2 -= ('a' - 'A');
	    }
	  if (c1 != c2)
	    {
	      return c1 < c2 ? -1 : 1;
	    }
	}
    }
  while (c1);

  return 0;			// strings are equal
}

int
Q_strncmp (const char *s1, const char *s2, int n)
{
  int c1, c2;

  do
    {
      c1 = *s1++;
      c2 = *s2++;

      if (!n--)
	{
	  return 0;		// strings are equal until end point
	}

      if (c1 != c2)
	{
	  return c1 < c2 ? -1 : 1;
	}
    }
  while (c1);

  return 0;			// strings are equal
}

int
Q_stricmp (const char *s1, const char *s2)
{
  return (s1 && s2) ? Q_stricmpn (s1, s2, 99999) : -1;
}



// never goes past bounds or leaves without a terminating 0
void
Q_strcat (char *dest, int size, const char *src)
{
  int l1;

  l1 = strlen (dest);
  if (l1 >= size)
    {
      sndi->Error (ERR_FATAL, "Q_strcat: already overflowed");
    }
  Q_strncpyz (dest + l1, src, size - l1);
}

/*
* Find the first occurrence of find in s.
*/
const char *
Q_stristr (const char *s, const char *find)
{
  char c, sc;
  size_t len;

  if ((c = *find++) != 0)
    {
      if (c >= 'a' && c <= 'z')
	{
	  c -= ('a' - 'A');
	}
      len = strlen (find);
      do
	{
	  do
	    {
	      if ((sc = *s++) == 0)
		return NULL;
	      if (sc >= 'a' && sc <= 'z')
		{
		  sc -= ('a' - 'A');
		}
	    }
	  while (sc != c);
	}
      while (Q_stricmpn (s, find, len) != 0);
      s--;
    }
  return s;
}


int
Q_PrintStrlen (const char *string)
{
  int len;
  const char *p;

  if (!string)
    {
      return 0;
    }

  len = 0;
  p = string;
  while (*p)
    {
      if (Q_IsColorString (p))
	{
	  p += 2;
	  continue;
	}
      p++;
      len++;
    }

  return len;
}


char *
Q_CleanStr (char *string)
{
  char *d;
  char *s;
  int c;

  s = string;
  d = string;
  while ((c = *s) != 0)
    {
      if (Q_IsColorString (s))
	{
	  s++;
	}
      else if (c >= 0x20 && c <= 0x7E)
	{
	  *d++ = c;
	}
      s++;
    }
  *d = '\0';

  return string;
}

int
Q_CountChar (const char *string, char tocount)
{
  int count;

  for (count = 0; *string; string++)
    {
      if (*string == tocount)
	count++;
    }

  return count;
}


char *
Q_strrchr (const char *string, int c)
{
  char cc = c;
  char *s;
  char *sp = (char *) 0;

  s = (char *) string;

  while (*s)
    {
      if (*s == cc)
	sp = s;
      s++;
    }
  if (cc == 0)
    sp = s;

  return sp;
}

/*
==================
COM_DefaultExtension
==================
*/
void
COM_DefaultExtension (char *path, int maxSize, const char *extension)
{
  char oldPath[MAX_QPATH];
  char *src;

//
// if path doesn't have a .EXT, append extension
// (extension should include the .)
//
  src = path + strlen (path) - 1;

  while (*src != '/' && src != path)
    {
      if (*src == '.')
	{
	  return;		// it has an extension
	}
      src--;
    }

  Q_strncpyz (oldPath, path, sizeof (oldPath));
  Com_sprintf (path, maxSize, "%s%s", oldPath, extension);
}

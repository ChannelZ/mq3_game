/*
  This file is part of the mQ3 project

  Copyright Gabriel Schnoering - GPLv3 or later

*/

#include "snd_local.h"

// global variable for the wave file
s_wavrecord_t s_wavrecord;

// fast writing functions
static byte buffer[4];

static void
SafeFS_Write (const void *buf, int len, fileHandle_t f)
{
  int ret;

  ret = sndi->FS_Write (buf, len, f);
  if (ret < len)
    {
      sndi->Error (ERR_DROP, "SafeFS_Write () failed : %d < %d, f : %d\n",
		   ret, len, f);
    }

  s_wavrecord.index += len;
}

static void
fwriteString (const char *s, fileHandle_t f)
{
  int len;

  len = strlen (s);
  SafeFS_Write (s, len, f);
}

static void
fwrite4 (int x, fileHandle_t f)
{
  buffer[0] = (byte)((x >> 0) & 0xFF);
  buffer[1] = (byte)((x >> 8) & 0xFF);
  buffer[2] = (byte)((x >> 16) & 0xFF);
  buffer[3] = (byte)((x >> 24) & 0xFF);

  SafeFS_Write (buffer, 4, f);
}

static void
fwrite2 (int x, fileHandle_t f)
{
  buffer[0] = (byte)((x >> 0) & 0xFF);
  buffer[1] = (byte)((x >> 8) & 0xFF);

  SafeFS_Write (buffer, 2, f);
}

static void
fwrite1 (int x, fileHandle_t f)
{
  buffer[0] = (byte)((x >> 0) & 0xFF);

  SafeFS_Write (buffer, 1, f);
}

/*
===============
S_WaveFilename
===============
*/
static void
S_WaveFilename (int lastNumber, char *checkName)
{
  int a, b, c, d;

  if ((lastNumber < 0) || (lastNumber > MAX_WAV_FILES))
    {
      Com_sprintf (checkName, MAX_OSPATH, "wavs/wav%d.wav", MAX_WAV_FILES);
      return;
    }

  a = lastNumber / 1000;
  lastNumber -= a * 1000;
  b = lastNumber / 100;
  lastNumber -= b * 100;
  c = lastNumber / 10;
  lastNumber -= c * 10;
  d = lastNumber;

  Com_sprintf (checkName, MAX_OSPATH, "wavs/wav%i%i%i%i.wav", a, b, c, d);
}

/*
==================
S_WaveWriteHeader

www.sonicpost.com/guide/wavefiles.html
==================
*/
static void
S_WaveWriteHeader (void)
{
  int size;

  size = 0x7fffffff; // bogus size

  fwriteString ("RIFF", s_wavrecord.handle);
  fwrite4 (size - 8, s_wavrecord.handle);
  fwriteString ("WAVE", s_wavrecord.handle);
  fwriteString ("fmt ", s_wavrecord.handle);
  fwrite4 (16, s_wavrecord.handle);
  fwrite2 (1, s_wavrecord.handle); // WAV_FORMAT_PCM
  fwrite2 (2, s_wavrecord.handle); // wChannels
  fwrite4 (dma.speed, s_wavrecord.handle); // sample rate
  fwrite4 (dma.speed * 2 * (16 / 8), s_wavrecord.handle); // dwAvgBytePerSec
  fwrite2 (2 * (16 / 8), s_wavrecord.handle); // wBlockAlign
  fwrite2 (16, s_wavrecord.handle); // bits per sample
  fwriteString ("data", s_wavrecord.handle);
  fwrite4 (size - 44, s_wavrecord.handle);
}

/*
=================
S_WaveCreateFile
=================
*/
static void
S_WaveCreateFile (void)
{
  char checkName[MAX_OSPATH];
  static int lastNumber = -1;

  // scan for a free filename

  // if we have saved a previous screenshot, don't scan
  // again, because recording demo avis can involve
  // thousands of shots
  if (lastNumber == -1)
    {
      lastNumber = 0;
    }

  // scan for a free wav file
  for (; lastNumber <= MAX_WAV_FILES; lastNumber++)
    {
      // generating file name
      S_WaveFilename (lastNumber, checkName);

      // does the file exist ? if it doesn't take this one
      if (!sndi->FS_FileExists (checkName))
	{
	  break;
	}
    }

  // out of available files
  if (lastNumber >= MAX_WAV_FILES)
    {
      sndi->Printf (S_COLOR_YELLOW "WaveFile: Couldn't create a file. Out of range\n");
      return;
    }

  lastNumber++;

  if (sndi->FS_FOpenFileByMode (checkName, &s_wavrecord.handle, FS_WRITE) == -1)
    {
      // error to create a writable file
      sndi->Printf (S_COLOR_YELLOW "WaveFile : could create file %s\n",
		    checkName);
      return;
    }

  // writing file header and initialize the wavrecord structure
  S_WaveWriteHeader ();

  // copy the filename to the wav structure
  Q_strncpyz (s_wavrecord.name, checkName, MAX_OSPATH);
  sndi->Printf ("recording wav file : %s\n", s_wavrecord.name);
  s_wavrecord.index = 0;
  s_wavrecord.active = true;
}

/*
=================
S_WaveDump_Frame

wav recording must be active, with a valid filehandle
=================
*/
void
S_WaveDump_Frame (const byte * buffer, int size)
{
  static byte s_wavdumpBuffer[WAV_DUMP_MAXBUFFER];
  static int s_wavdumpSize = 0;

  // check for overflow
  if ((s_wavdumpSize + size) > WAV_DUMP_MAXBUFFER)
    {
      sndi->Printf (S_COLOR_YELLOW "WARNING : overflowing outbuffer\n");
      size = WAV_DUMP_MAXBUFFER - s_wavdumpSize;
    }

  // copy from ingame buffer to fill the temporary buffer
  Com_Memcpy (&s_wavdumpBuffer[s_wavdumpSize], buffer, size);
  s_wavdumpSize += size;

  // and dump it to the file when enough datas are available
  if (s_wavdumpSize >= (int)ceil ((float)(dma.speed * dma.samplebits)
				  / (float)WAV_DUMP_FRAMERATE))
    {
      SafeFS_Write ((void *)s_wavdumpBuffer, s_wavdumpSize, s_wavrecord.handle);
      s_wavdumpSize = 0;
    }
}

/*
================
S_WaveDump_Stop
================
*/
void
S_WaveDump_Stop (void)
{
  int pos;

  if (!s_wavrecord.active)
    return;

  // completing the header
  pos = sndi->FS_FTell (s_wavrecord.handle);

  sndi->FS_Seek (s_wavrecord.handle, 4, FS_SEEK_SET);
  fwrite4 (pos - 8, s_wavrecord.handle);

  sndi->FS_Seek (s_wavrecord.handle, 40, FS_SEEK_SET);
  fwrite4 (pos - 44, s_wavrecord.handle);

  // closing the file
  sndi->FS_FCloseFile (s_wavrecord.handle);

  sndi->Printf ("stopped recording wave file : %s (%.2fMB)\n",
		s_wavrecord.name, (float)s_wavrecord.index/1048576.0f);

  s_wavrecord.active = false;
}

/*
=================
S_WaveDump_Check
=================
*/
void
S_WaveDump_Check (void)
{
  if (s_wav_record->integer)
    {
      // we are not yet recording
      // create the file, set the headers
      if (!s_wavrecord.active)
	{
	  S_WaveCreateFile ();
	}
      // we are recording
      else
	{
	  // nothing special to do each frame ?
	  // frame dumping is done elsewhere
	  // (WaveDump_Frame called in snd_mix.c)
	}
    }
  else
    {
      // stop recording, closing file if it was open
      S_WaveDump_Stop ();
    }
}

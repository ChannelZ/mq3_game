/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/

#ifndef SND_PUBLIC_H
#define SND_PUBLIC_H


// Interface between Q3 sound "api" and the sound backend
struct sndexport_s
{
  //void (*Init) (void);
  void (*Shutdown) (void);
  void (*StartSound) (vec3_t origin, int entnum, int entchannel,
		      sfxHandle_t sfx);
  void (*StartLocalSound) (sfxHandle_t sfx, int channelNum);
  void (*StartBackgroundTrack) (const char *intro, const char *loop);
  void (*StopBackgroundTrack) (void);
  void (*RawSamples) (int stream, int samples, int rate, int width,
		      int channels, const byte * data, float volume);
  void (*StopAllSounds) (void);
  void (*ClearLoopingSounds) (bool killall);
  void (*AddLoopingSound) (int entityNum, const vec3_t origin,
			   const vec3_t velocity, sfxHandle_t sfx);
  void (*AddRealLoopingSound) (int entityNum, const vec3_t origin,
			       const vec3_t velocity, sfxHandle_t sfx);
  void (*StopLoopingSound) (int entityNum);
  void (*Respatialize) (int entityNum, const vec3_t origin, vec3_t axis[3],
			int inwater);
  void (*UpdateEntityPosition) (int entityNum, const vec3_t origin);
  void (*Update) (void);
  void (*DisableSounds) (void);
  void (*BeginRegistration) (void);
  sfxHandle_t (*RegisterSound) (const char *sample);
  void (*ClearSoundBuffer) (void);
  void (*SoundInfo) (void);
  void (*SoundList) (void);
};
typedef struct sndexport_s sndexport_t;

struct sndimport_s
{
  void (*Printf) (const char * msg, ...);
  void (*DPrintf) (const char * msg, ...);
  void (*Error) (int lvl, const char * msg, ...);
  int (*Milliseconds) (void);
  int  (*Cmd_Argc) (void);
  char *(*Cmd_Argv) (int i);
  void (*Cmd_AddCommand) (const char *name, void(*cmd)(void));
  void (*Cmd_RemoveCommand) (const char * name);
  cvar_t *(*Cvar_Get) (const char *name, const char *value, int flags);
  void (*Cvar_Set) (const char *name, const char *value);
  int (*Cvar_SetNew) (cvar_t * var, const char * value);
  void *(*Hunk_Alloc) (int size, ha_pref preference);
  void *(*Hunk_AllocateTempMemory) (int size);
  void (*Hunk_FreeTempMemory) (void *block);
  int (*FS_FOpenFileByMode) (const char *qpath, fileHandle_t *f, fsMode_t mode);
  int (*FS_Read) (const void *buffer, int len, fileHandle_t f);
  int (*FS_ReadFile) (const char * qpath, void **buffer);
  int (*FS_Write) (const void *buffer, int len, fileHandle_t f);
  void (*FS_FCloseFile) (fileHandle_t f);
  int (*FS_Seek) (fileHandle_t f, long offset, int origin);
  int (*FS_FTell) (fileHandle_t f);
  bool (*FS_FileExists) (const char *file);

  void *(*Z_Malloc) (int bytes);
  void (*Z_Free) (void *buf);

};
typedef struct sndimport_s sndimport_t;

#endif // SND_PUBLIC_H

/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
// snd_local.h -- private sound definations


#include "qcommon/q_shared.h"
//#include "qcommon/qcommon.h"
#include "qcommon/qcom_extern.h"
#include "snd_public.h"

#define soundInterface_t sndexport_t

extern sndimport_t *sndi;


#define	PAINTBUFFER_SIZE	4096	// this is in samples

#define SND_CHUNK_SIZE		1024	// samples
#define SND_CHUNK_SIZE_FLOAT	(SND_CHUNK_SIZE/2)	// floats
#define SND_CHUNK_SIZE_BYTE	(SND_CHUNK_SIZE*2)	// floats

typedef struct
{
  // the final values will be clamped to +/- 0x00ffff00 and shifted down
  int left;
  int right;
} portable_samplepair_t;

typedef struct sndBuffer_s
{
  short sndChunk[SND_CHUNK_SIZE];
  struct sndBuffer_s *next;
  int size;
} sndBuffer;

typedef struct sfx_s
{
  sndBuffer *soundData;
  bool defaultSound;	// couldn't be loaded, so use buzz
  bool inMemory;		// not in Memory
  int soundCompressionMethod;
  int soundLength;
  char soundName[MAX_QPATH];
  int lastTimeUsed;
  struct sfx_s *next;
} sfx_t;

typedef struct
{
  int channels;
  int samples;			// mono samples in buffer
  int submission_chunk;		// don't mix less than this #
  int samplebits;
  int speed;
  byte *buffer;
} dma_t;

// some infos about the listener
struct snd_listener_s
{
  int number;
  vec3_t origin;
  vec3_t axis[3];
};
typedef struct snd_listener_s snd_listener_t;

#define START_SAMPLE_IMMEDIATE	0x7fffffff

#define MAX_DOPPLER_SCALE 50.0f	//arbitrary

typedef struct loopSound_s
{
  vec3_t origin;
  vec3_t velocity;
  sfx_t *sfx;
  int mergeFrame;
  bool active;
  bool kill;
  bool doppler;
  float dopplerScale;
  int framenum;
} loopSound_t;


typedef struct
{
  int allocTime;
  int startSample;		// START_SAMPLE_IMMEDIATE = set immediately on next mix
  int entnum;			// to allow overriding a specific sound
  int entchannel;		// to allow overriding a specific sound
  int master_vol;		// 0-255 volume before spatialization
  float dopplerScale;
  bool doppler;
  vec3_t origin;		// only use if fixed_origin is set
  bool fixed_origin;	// use origin instead of fetching entnum's origin
  sfx_t *thesfx;		// sfx structure

  int leftvol;		// 0-255 volume after spatialization
  int rightvol;		// 0-255 volume after spatialization

  vec3_t sodrot;
} channel_t;


#define WAV_FORMAT_PCM 1
#define WAV_DUMP_FRAMERATE 50.0f
#define WAV_DUMP_MAXBUFFER 44100 // 1 sec of sound
#define MAX_WAV_FILES 100

typedef struct
{
  int format;
  int rate;
  int width;
  int channels;
  int samples;
  int dataofs;			// chunk starts this many bytes from file start
} wavinfo_t;

struct s_wavrecord_s
{
  bool active;
  int handle;
  char name[MAX_OSPATH];
  int index;
};
typedef struct s_wavrecord_s s_wavrecord_t;

extern s_wavrecord_t s_wavrecord;

/*
====================================================================

  SYSTEM SPECIFIC FUNCTIONS

====================================================================
*/

// initializes cycling through a DMA buffer and returns information on it
bool SNDDMA_Init (void);

// gets the current DMA position
int SNDDMA_GetDMAPos (void);

// shutdown the DMA xfer.
void SNDDMA_Shutdown (void);

void SNDDMA_BeginPainting (void);

void SNDDMA_Submit (void);

//====================================================================

#define MAX_CHANNELS 96 // number of files played simultaneously
#define MAX_RAW_SAMPLES	16384
#define MAX_RAW_STREAMS 128

// dma.c
extern channel_t s_channels[MAX_CHANNELS];
extern channel_t loop_channels[MAX_CHANNELS];
extern int numLoopChannels;
extern dma_t dma;
extern int s_paintedtime;

extern portable_samplepair_t s_rawsamples[MAX_RAW_STREAMS][MAX_RAW_SAMPLES];
extern int s_rawend[MAX_RAW_STREAMS];

extern cvar_t *s_testsound;

// main.c
extern cvar_t *s_volume;
extern cvar_t *s_musicVolume;
extern cvar_t *s_doppler;
extern cvar_t *s_wav_record;

bool S_LoadSound (sfx_t * sfx);

void SND_free (sndBuffer * v);
sndBuffer *SND_malloc (void);
void SND_setup (void);
void SND_shutdown (void);
// requested by SND_malloc
void S_FreeOldestSound (void);

void S_PaintChannels (int endtime);

void S_GetSoundtime (void);
// spatializes a channel
void S_Spatialize (channel_t * ch);

//extern short *sfxScratchBuffer;
//extern sfx_t *sfxScratchPointer;
//extern int sfxScratchIndex;

bool S_Base_Init (soundInterface_t * si);

void S_DisplayFreeMemory (void);

void S_ClearSoundBuffer (void);

void S_UpdateBackgroundTrack (void);

// snd_wav.c
void S_WaveDump_Check (void);
void S_WaveDump_Frame (const byte * buffer, int size);
void S_WaveDump_Stop (void);

#!/bin/sh

# Paths

BASEPATH=~/devel/q3/minq3/
RELPATH=~/devel/q3/releaseminq3
BLIN=$BASEPATH/build/debug-linux-i386
BWIN=$BASEPATH/build/debug-mingw32-x86
MAKE="make debug $*"
MSMAKE="make mods BUILD_MOD_MINISTRIKE=0 $*"
BUILD_WIN=0
BUILD_MODS=1

#MAKEREL="make release $*"
#BLINREL=$BASEPATH/build/release-linux-i386
#ARCH=x86
#EXT=dll

BNAME=mq3
BNAMED=mq3ded

MOD=base
MSMOD=ministrike
# Building client & server

cd $BASEPATH

if [ $BUILD_MODS == 1 ]; then
    $MSMAKE
else
    $MAKE # requested by mods atm
fi

if [ $BUILD_WIN == 1 ]; then
    sh cross-make-mingw.sh mods BUILD_MOD_MINISTRIKE=0 $*
fi

# Copying files

# creating the directories
# TODO : check if they already exist

#mod pathtobuilddire mod
function installmod () {
if [ $# -eq 1 ]; then
    echo "buildmod needs an argument"
else
    install -d $RELPATH/$2
    cp -v $1/qagame$ARCH.$EXT $RELPATH/$2
    cp -v $1/cgame$ARCH.$EXT $RELPATH/$2
fi
}

function installfiles () {

install -d $RELPATH/lib

#client
cp -v $DIR/$BNAME.$ARCH$BIN $RELPATH
cp -v $DIR/lib/libq3renderer$ARCH.$EXT $RELPATH/lib
cp -v $DIR/lib/libq3snd$ARCH.$EXT $RELPATH/lib
cp -v $DIR/lib/libq3input$ARCH.$EXT $RELPATH/lib

#dedicated
cp -v $DIR/$BNAMED.$ARCH$BIN $RELPATH
cp -v $DIR/lib/libq3common$ARCH.$EXT $RELPATH/lib
cp -v $DIR/lib/libq3shared$ARCH.$EXT $RELPATH/lib
cp -v $DIR/lib/libq3botlib$ARCH.$EXT $RELPATH/lib

installmod $DIR/mods/base $MOD
if [ $BUILD_MODS == 1 ];then
    installmod $DIR/mods/ministrike $MSMOD
fi
}

DIR=$BLIN
ARCH=i386
EXT=so
BIN=

installfiles

if [ $BUILD_WIN == 1 ]; then
    DIR=$BWIN
    ARCH=x86
    EXT=dll
    BIN=.exe

    installfiles
fi

#documentation
cp -R $BASEPATH/doc/ $RELPATH/

#custom .pk3
#cp $BASEPATH/pk3/*.pk3 $RELPATH/baseq3

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main (int argc, char ** argv)
{
  FILE * fr;
  FILE * fw;
  char newfile[256];
  char buffer[1024];

  if (argc != 2)
    {
      printf ("the program expected a filename as a parameter :\n"
	      "%s filename\n", argv[0]);
      return 1;
    }

  fr = fopen (argv[1], "r");
  if (fr == NULL)
    {
      // error !
      printf ("can't open filename : %s -- error %m\n", argv[1]);
      return 2;
    }

  // output file
  snprintf (newfile, sizeof (newfile), "%s.table", argv[1]);

  fw = fopen (newfile, "w+");
  if (fw == NULL)
    {
      // error !
      printf ("can't open filename : %s -- error %m\n", argv[1]);
      return 3;
    }

  // start the work
  char name[256];
  unsigned int sk1;
  unsigned int sk2;
  unsigned int sk3;
  unsigned int val1;
  unsigned int val2;
  unsigned int val3;
  unsigned char counter = 'a';

  // loop until we have read the file
  while (fscanf (fr, "%s %x %x %x %u %u %u", name, &sk1, &sk2, &sk3, &val1, &val2, &val3) != EOF)
    {
      // generate the string
      snprintf (buffer, sizeof (buffer), "{%f, %f, %f, 1.0f}, // %c - %s\n",
		(float)val1/(float)255, (float)val2/(float)255, (float)val3/(float)255,
		counter, name);

      // write it
      fwrite (buffer, strlen (buffer), 1, fw);

      // jump the index
      if (counter == 'z')
	counter = 'A';
      else
	counter++;
    }

  // closing files
  fclose (fr);
  fclose (fw);

  return 0;
}

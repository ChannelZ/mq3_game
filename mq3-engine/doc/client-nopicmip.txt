On peut désactiver l'effet de picmip sur certaines textures lorsqu'il est appliqué
sur l'ensemble du monde (r_picmip).
Normalement cette configuration s'effectue dans les fichiers shaders des textures
avec la balise "nopicmip", ce qui est uilisé pour les textures sur lesquelles l'effet
ne doit jamais être appliqué.
Pour les textures normales (ou aucune information n'est précisée) l'effet de picmip
s'applique dessus sauf si dans le code, cela est spécifié au chargment de la texture
(CG_R_RegisterShaderNoMip ()).

On peut choisir de charger certains effets sans picmip via les 2 cvars dédiées
dev_cg_nomip et dev_cg_nomip2 avec les masques binaires suivants

// Nomip flags
#define NM_SHAFT_LIGHTNING	0x00000001 // 1
#define NM_SHAFT_IMPACT		0x00000002 // 2
#define NM_RAIL			0x00000004 // 4
#define NM_PLASMA		0x00000008 // 8
#define NM_ROCKET_EXPLO		0x00000010 // 16
#define NM_GRENADE_EXPLO	0x00000020 // 32
#define NM_SMOKE		0x00000040 // 64
#define NM_BLOOD_IMPACT		0x00000080 // 128
#define NM_SHOTGUN_SMOKE	0x00000100 // 256
#define NM_BULLET_IMPACT	0x00000200 // 512
#define NM_MARKS		0x00000400 // 1024
#define NM_BFG_EXPLO		0x00000800 // 2048
#define NM_SHAFT_MIPMAP		0x00001000 // 4096

#define NM2_ITEMS		0x00000001 // 1
#define NM2_WEAPONS		0x00000002 // 2
#define NM2_PLAYERSKINS		0x00000004 // 4 // nomip on all skins atm
#define NM2_ROCKET_PROJ		0x00000008 // 8
#define NM2_GRENADE_PROJ	0x00000010 // 16
#define NM2_BFG_PROJ		0x00000020 // 32
#define NM2_TELEPORT_EFFECT	0x00000040 // 64

par exemple pour une fumée et un plasma clair on additionne les valeurs :
dev_cg_nomip 72

idem pour les projectiles
dev_cg_nomip2 24

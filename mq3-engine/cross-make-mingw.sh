#!/bin/sh

export CC=i686-mingw32-gcc
export WINDRES=i686-mingw32-windres
export PLATFORM=mingw32
exec make USE_LOCAL_HEADERS=1 USE_INTERNAL_ZLIB=1 USE_INTERNAL_JPEG=1 debug $*
